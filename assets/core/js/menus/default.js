$(function() {
    $('#sidebar > ul > li').on('click', function (e, o) {
        var id = $(this).attr('id');
        var target = e.target;
        var uls = $(this).find('ul');
        sessionStorage.setItem('test', JSON.stringify(uls));
        if(uls.length > 0 && $(uls).has(target).length != 0){
            return;
        }
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).find('ul').slideUp();
        }else{
            $('#sidebar > ul > li').removeClass('active');
            $('#sidebar > ul > li > ul').slideUp();
            $('#sidebar ul li[id="'+id+'"]').addClass('active');
            $(this).find('ul').slideDown();
        }
    });
});