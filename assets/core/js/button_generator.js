$(function(){
    $('body').on('click', 'button, a', function(e){
        $(this).do_action();
    });
});
/**
 * function be used to get information of button to ready for action
 * @param option
 * @returns {*}
 */
$.fn.system_button_info = function(option){
    if(!isset(option)){ option = {}; }

    var arr_data = ['url','text','button','param','more','before','success','fail','title','content', 'dialog', 'async', 'type', 'close_modal', 'loading'];
    $.each(arr_data, function(obj) {
        option[this] = $(obj).get_system_config('action-' + this, option);
    }, $(this));
    if(isset(option.param)){
        var param_json = IsJsonString(option.param);
        if(param_json){
            option.param = param_json;
        }
    }else{
        option.param = {};
    }
    if(isset(option.more)){
        var more_param = IsJsonString(option.more);
        if(more_param){
            option.more = more_param;
        }
    }else{
        option.more = {};
    }
    if(!isset(option.loading)){
        option.loading = 'full';
    }
    return option;
};
/**
 * FUNCTION TO CREATE CONFIRMATION
 * priority Javascript > HTML
 */
$.system_confirm = function(option){
    $(null).system_confirm(option);
};
$.fn.system_confirm = function(option){
    option = $(this).system_button_info(option);
    option.type = 'confirm';
    if (isset(option.url)) {
        $(this).do_success_action(option);
    }
};
/**
 * FUNCTION TO CREATE DIALOG AUTOMATICALLY
 * priority Server > Javascript > HTML
 */
$.system_dialog = function(option){
    $(null).system_dialog(option);
};
$.fn.system_dialog = function(option){
    option = $(this).system_button_info(option);
    option.type = 'dialog';
    option.more.dialog = $("#wf_page_dialog");
    var process_option = $.extend(true, {}, option);
    process_option.success = function(ret, more){
        if(!isset(ret.data.type)){
            option = $.prepare_success_data(option, ret.data);
            more.element.do_success_action(option);
        }
        system_fire_event(option.success, ret, process_option.more);
    };
    $(this).system_process(process_option);
};
/**
 * FUNCTION TO RUN PROCESS AUTOMATICALLY
 * priority Javascript > HTML
 */
$.system_process = function(option){
    $(null).system_process(option);
};
$.fn.system_process = function(option){
    option = $(this).system_button_info(option);
    if(!isset(option.type)){
        option.type = 'process';
    }
    if(isset(option.before)){
        var before_ret = system_fire_event(option.before, option.param, option.more);
        if(isset(before_ret) && typeof before_ret == 'object'){
            option.param = $.extend(option.param, before_ret);
        }else if(before_ret === false){
            return;
        }
    }
    $(this).system_process_post(option);
};

$.fn.do_action = function(action_type, option){
    if(!isset(action_type)){
        action_type = $(this).get_system_config('action-type');
    }
    if(!action_type){
        action_type = "process";
    }
    if (isset(action_type)) {
        switch (action_type){
            case "confirm":
                $(this).system_confirm(option);
                break;
            case "dialog":
                $(this).system_dialog(option);
                break;
            case "process":
                $(this).system_process(option);
                break;
            default :
                system_alert({
                    content : "Wrong action",
                    type    : 'fail'
                });
                break;
        }
    }
};

$.prepare_success_data = function(option, data){
    if(isset(data.option)){
        var buttons = (isset(option.button)) ? option.button : {};
        option = $.extend(option, data.option);
        if(isset(data.option.button)){
            if(isset(data.option.replace_button) && data.option.replace_button){
                buttons = data.option.button;
            }else{
                buttons = $.extend_button(buttons, data.option.button);
            }
            option.button = buttons;
        }
    }
    if(isset(data.content)){
        option.content = data.content;
    }
    return option;
};

$.fn.do_success_action = function(option){
    var action_type = option.type;
    if (isset(action_type)) {
        switch (action_type){
            case "confirm":
                $("#wf_page_confirm").show_confirmation(option);
                break;
            case "dialog":
                $("#wf_page_dialog").show_dialog(option);
                break;
            case "redirect":
                window.location.href = option.url;
                break;
        }
    }
};

$.extend_button = function(current_buttons, new_buttons){
    if(!isset(current_buttons)){ current_buttons = {}; }
    $.each(new_buttons, function(key, value){
        if(isset(current_buttons[key])){
            current_buttons[key] = $.extend(current_buttons[key], value);
        }else{
            current_buttons[key] = value;
        }
    });
    return current_buttons;
};

$.fn.show_confirmation = function(option){
    var confirmation = $(this),
        body = confirmation.find(".modal-dialog .modal-body p"),
        footer = confirmation.find(".modal-dialog .modal-footer");
    if (isset(option.content)) {
        body.html(option.content);
        body.show();
    }else{
        body.html("");
        body.hide();
    }
    if(isset(option.button)){
        footer.generate_modal_button(option.button);
    }else{
        footer.html('<button data-dismiss="modal" class="btn btn-primary">Yes</button><button data-dismiss="modal" class="btn">No</button>');
    }
    footer.find(".btn-primary").attr('action-type', 'process');
    footer.find(".btn-primary").on('click', function(){
        $(this).system_process(option);
    });
    confirmation.modal("show");
};

/**
 * function to show dialog
 * @param option
 */
$.fn.show_dialog = function(option){
    var dialog = $(this),
        header = dialog.find(".modal-dialog .modal-header"),
        body = dialog.find(".modal-dialog .modal-body"),
        footer = dialog.find(".modal-dialog .modal-footer");
    dialog.destroy_modal();
    header.hide();
    footer.hide();
    if(isset(option.title)){
        header.find(".modal-title").html(option.title);
        header.show();
    }
    if(isset(option.button)){
        footer.generate_modal_button(option.button);
        footer.show();
    }
    dialog.dialog_attr(option.dialog);
    body.html(option.content);
    dialog.modal("show");
};

$.fn.dialog_attr = function(option){
    $(this).find('.modal-dialog').removeClass(['modal-lg', 'modal-sm']).addClass("modal-lg");
    if(isset(option)){
        if(isset(option.size)){
            $(this).find('.modal-dialog').removeClass("modal-lg").addClass("modal-" + option.size);
        }
    }
};

$.fn.system_process_post = function(option){
    if(isset(option.url)) {
        if($(this).is('button,a')){
            option.loading = 'inline';
        }
        $(this).start_load(option);
        $.ajax({
            url: option.url,
            method: 'post',
            data: option.param,
            dataType: 'json',
            element: $(this),
            async : isset(option.async)?option.async:true,
            success: function (ret) {
                if (typeof ret === 'object') {
                    var status = ret.status;
                    if (status == REQUEST_SUCCESS) {
                        var message = ret.message;
                        if (message !== "") {
                            system_alert({
                                content : message,
                                type    : 'success'
                            });
                        }
                        option = $.prepare_success_data(option, ret.data);
                        if(!isset(option.close_modal)){
                            if(option.type == 'process'){
                                option.close_modal = 0;
                            }else{
                                option.close_modal = 1;
                            }
                        }
                        if(option.close_modal){
                            close_all_modal();
                        }
                        if (isset(ret.data.type)) {
                            option.type = ret.data.type;
                            this.element.do_success_action(option);
                        }
                        if (!isset(option.more)) {
                            option.more = {};
                        }
                        if (typeof option.more == 'object') {
                            option.more.element = this.element;
                        }
                        if (isset(option.callback)){
                            option.callback();
                        }
                        system_fire_event(option.success, ret, option.more);
                    } else {
                        if(! isset(option.fail)){
                            if (isset(ret.data.validate_error)) {
                                handleErrors(ret.data.validate_error);
                            } else {
                                var message = ret.message;
                                system_alert({
                                    content : message,
                                    type    : 'fail'
                                });
                            }
                        }else{
                            system_fire_event(option.fail, ret, option.more);
                        }
                    }
                } else {
                    system_alert({
                        content : "Something wrong when data return to browser, please contact admin to check!",
                        type    : 'fail'
                    });
                }
                this.element.end_load(option);
            }
        });
    }
};

/**
 * function used to generate buttons
 * @param button
 */
$.fn.generate_modal_button = function(button){
    if(typeof button == "string"){
        button_json = IsJsonString(button);
        if(button_json !== false){
            button = button_json;
        }else{
            $(this).html(button);
        }
    }
    if(typeof button == "object"){
        $(this).html("");
        $.each(button, function(obj){
            $(obj).generate_modal_single_button(this);
        }, $(this));
    }
};
$.fn.generate_modal_single_button = function(value){
    var btn_text = value.text,
        btn_url = value.url,
        btn_class = value.class,
        btn_on_click = value.onClick;
    var btn = $("<button>");
    btn.addClass('btn');
    if(isset(btn_text)){
        btn.html(btn_text);
    }
    btn.button_attr(value);
    if(!isset(btn_url)){
        btn.attr('data-dismiss', "modal");
    }
    if(isset(btn_class)){
        btn.addClass(btn_class);
    }
    if(isset(btn_on_click)){
        btn.on('click', function(){
            system_fire_event(btn_on_click, value.param, value.more);
        });
    }
    $(this).append(btn);
};

$.fn.button_attr = function(option){
    if(isset(option) && typeof option == 'object'){
        var btn = $(this);
        $.each(option, function(key, value){
            btn.data('action-' + key, value);
        });
    }
};

/**
 * get value to run action. This function will be used to define priority
 * @param option
 */
$.fn.get_system_config = function(key, option){
    if(isset(key)){
        if(isset(option)){
            var jsKey = key.replace("action-", "");
            if(isset(option[jsKey])){
                return option[jsKey];
            }
        }

        if(isset($(this).data(key))){
            return $(this).data(key);
        }
        if(isset($(this).attr(key))){
            return $(this).attr(key);
        }
    }
    return null;
};

$.fn.destroy_modal = function(){
    $(this).removeClass('fade').modal('hide').addClass('fade');
};

$.fn.start_load = function(option){
    if(option.loading !== false){
        switch (option.loading){
            case 'inline':
                if($(this).is('button,a')){
                    $(this).attr('data-loading-text','Loading...');
                    $(this).button('loading');
                }else{

                }
                break;
            case 'full':
                $("#wf_page_spinner").show();
                break;
        }
    }
};

$.fn.end_load = function(option){
    if(option.loading !== false){
        switch (option.loading){
            case 'inline':
                if($(this).is('button,a')){
                    $(this).button('reset');
                }else{

                }
                break;
            case 'full':
                $("#wf_page_spinner").hide();
                break;
        }
    }
};

if (typeof system_fire_event != 'function') {
    /**
     * function be used to fire an event
     * @param e
     */
    var system_fire_event = function (e, param, more) {
        if(isset(e)){
            if(typeof e == "function"){
                return e(param, more);
            }else{
                var fn = window[e];
                if(typeof fn === 'function') {
                    return fn(param, more);
                }
            }
        }
        return null;
    };
}
/**
 * DEFAULT ALERT USED FOR SYSTEM
 * @param msg
 */
if (typeof system_alert != 'function') {
    var system_alert = function (options) {
        switch(options['type']){
            case 'success':
                $("#wf_page_alert .modal-dialog .modal-title span.title").html(isset(options['title'])?options['title']:'Success');
                $('#wf_page_alert .modal-dialog .modal-title').attr('class','modal-title green');
                $("#wf_page_alert .modal-dialog .modal-title span[type=icon]").attr('class','glyphicon glyphicon-ok');
                break;
            case 'fail':
                $("#wf_page_alert .modal-dialog .modal-title span.title").html(isset(options['title'])?options['title']:'Fail');
                $('#wf_page_alert .modal-dialog .modal-title').attr('class','modal-title red');
                $("#wf_page_alert .modal-dialog .modal-title span[type=icon]").attr('class','glyphicon glyphicon-remove');
                break;
        }

        $("#wf_page_alert .modal-dialog .modal-body").html(options['content']);
        $('#wf_page_alert').modal("show");
        $('#wt_page_dialog').modal('hide');
    };
}
/**
 * Function to check value exists or not
 * @param value
 * @returns {boolean}
 */
var isset = function (value) {
    if (typeof value !== typeof undefined && value !== null) {
        return true;
    }
    return false;
};

if(typeof IsJsonString != 'function'){
    /**
     *  function to check is Json or not
     * @param str
     * @returns {*}
     * @constructor
     */
    var IsJsonString = function(str) {
        var ret;
        try {
            ret = JSON.parse(str);
        } catch (e) {
            return false;
        }
        return ret;
    }
}


function close_all_modal(){
    $('#wf_page_dialog').destroy_modal();
    $('#wf_page_confirm').destroy_modal();
}