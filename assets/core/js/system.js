/* CREATE BASIC OBJECT FOR SYSTEM */
var UI = function(){};
UI.contentComponent = function(){};

$(function(){
    overrideJquery();
    if($(".datatable").length){
        $(".datatable").transform_datatable();
    }

    $('body').delegate('.cpassword','keyup',function(){
        var target = $(this).attr('target');
        var valPass, valCPass;
        if(target){
            valPass     = $('#' + target).val();
            valCPass    = $(this).val();
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            //Find the confirm message
            if($(this).siblings('.confirmMessage').length == 0){
                var alertNode = document.createElement('span');
                $(alertNode).addClass('confirmMessage');
            }

            if(valPass === valCPass){
                $(this).css('background-color',goodColor);
                $(this).siblings('.confirmMessage').css('color',goodColor);
                $(this).siblings('.confirmMessage').text('Password is matched');
            }else{
                $(this).css('background-color',badColor);
                $(this).siblings('.confirmMessage').css('color',badColor);
                $(this).siblings('.confirmMessage').text('Password is not matched');
            }
        }
    });

    $('table').delegate('.chkbox','click',function(){
        if($(".chkbox").is(':checked')){
            $("#delete").removeClass('disabled');
        }else{
            $("#delete").addClass('disabled');
        }
    });


    $('body').delegate('.checkAll','click',function(){
        if($(this).prop('checked')){
            $(this).parents('table').first().find('.chkbox').prop('checked',1);
        }
        else{
            $(this).parents('table').first().find('.chkbox').prop('checked',0);
        }
    });

});

var overrideJquery = function(){
    $.ajaxSetup({data: {current_system : current_system}});
};

function get_insert_values(){
    var str = {};
    if($(".insert").customValidate()===false){
        return false;
    }

    $(".insert").each(function(){
        if($(this).hasClass('multi-select')){
            var name = $(this).attr('id');
            var val = [];
            $(this).find('input[name="'+name+'[]"]').each(function(){
                val.push($(this).val());
            });
        }else{
            var name = $(this).attr('id');
            var val = $(this).val();
            if(IsJsonString(val)){
                val = JSON.parse(val);
            }
            if($(this).attr('type') == 'checkbox'){
                if(!$(this).prop('checked')){
                    val = $(this).attr('unchecked-value');
                }
                else{
                    val = 1;
                }
            }
        }
        str[name] = val;
    });
    str = custom_get_insert_values(str);
    if(str === false){
        return false;
    }
    return {field_post:str};
}
var custom_get_insert_values = function(root){
    return root;
};
var transform_select = function(parent){

    var objs;
    if(isset(parent)){
        objs = $(parent).find('select[data-source]');
    }else{
        objs = $('select[data-source]');
    }

    objs.each(function(){
        var select = $(this),
            value = $(select).attr('data-value'),
            url = $(this).attr('data-source'),
            param = $(this).attr('data-param'),
            multiple = $(this).attr('multiple'),
            default_param = {term:"",minimal:true};
        param = IsJsonString(param);

        if(!isset(value)){
            value = [];
        }else{
            if(IsJsonString(value)){
                value = JSON.parse(value);
            }
            if(!is_array(value)){
                value = [value];
            }
            for(var i = 0; i < value.length; i++){
                value[i] = value[i].toString();
            }
        }


        if(param !== false){
            $.extend(true,default_param,param);
        }
        //$(select).select2({width:'style'});
        //if(isset(url)){
        //    $(select).select2({
        //        ajax: {
        //            url : url,
        //            dataType: 'json',
        //            delay: 250,
        //            data: function(params){
        //                return {
        //                    term : params.term
        //                }
        //            },
        //            processResults: function(data,page){
        //                var new_data = [];
        //                $.each(data,function(){
        //                    new_data.push({
        //                        id   : this.value,
        //                        text : this.label
        //                    })
        //                });
        //                return {
        //                    results : new_data
        //                }
        //            }
        //        }
        //    })
        //}
        $.post(url,default_param,function(data){
            data = JSON.parse(data);
            for(i = 0; i < data.length; i++){
                if(_.indexOf(value,data[i].value) != -1){
                    $(select).append($('<option>',{
                        text: data[i].label,
                        value: data[i].value,
                        selected: true
                    }));
                }else{
                    $(select).append($('<option>',{
                        text: data[i].label,
                        value: data[i].value
                    }));
                }
            }
            $(select).select2({width:'style'});

        });
    });
};
$.fn.get_option = function(opt, df, prefix){
    if(!isset(prefix)){
        prefix = '';
    }
    if(!isset(opt)){
        opt = {};
    }
    var element = $(this);
    $.each(df, function(key, value){
        if(!isset(opt[key])){
            var v = element.attr(prefix+key);
            if(isset(v)){
                opt[key] = v;
            }else{
                opt[key] = value;
            }
        }
    });
    return opt;
};
var transform_datepicker = function(parent,option){
    var df_option = {
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        minDate: new Date(1970,1,1),
        maxDate: new Date(2100,1,1)
    };
    var elems;
    if(isset(parent)){
        elems = $(parent).find('.datepicker');
    }else {
        elems = $('.datepicker');
    }

    elems.each(function(){
        var temp_option = $(this).get_option(option, df_option,'datepicker');

        var max_month= $(this).attr("max_month");

        if(isset(max_month)){
            $(this).datepicker(temp_option);
            var id = $(this).attr('id');
            var main_elem = $(this);
            var depended_elems = $('[object_time='+id+']');
            $(this).datepicker('option','onClose',function(){
                depended_elems.each(function(){
                    var depended_date = $(main_elem).val();
                    var limit_date=moment(new Date(depended_date)).add(max_month, 'months');
                    $(this).datepicker('option','minDate', new Date(depended_date));
                    $(this).datepicker('option','maxDate', new Date(limit_date));
                });
            });
        }
        else{
            $(this).datepicker(temp_option);
        }
    });
};

var transform_datetimepicker = function(parent, option){
    var df_option = {
        format:'H:i',
        timepicker:true,
        datepicker:false,
        minTime: '00:00',
        maxTime: '23:59',
        minDate: '1970/01/02',
        formatDate:'Y-m-d H:i:s',
        maxDate: '2100/01/01',
        step: 30
    };
    if(isset(parent)){
        var elems = $(parent).find('input.time-picker');
        elems.each(function(){
            var temp_option = $(this).get_option(option, df_option);
            $(this).datetimepicker(temp_option);
        });
    }else {
        var elems = $('input.time-picker');
        elems.each(function(){
            var temp_option = $(this).get_option(option, df_option);
            $(this).datetimepicker(temp_option);
        });
    }
};
var render_form = function(parent){
    transform_select(parent);
    transform_multiselect(parent);
    transform_autocomplete(parent);
    transform_datetimepicker(parent);
    transform_datepicker(parent);
    custom_form_render(parent);
};

function IsJsonString(str) {
    var ret;
    try {
        ret = JSON.parse(str);
    } catch (e) {
        return false;
    }
    return ret;
}

function isset(value){
    if(typeof value !== typeof undefined && value !== false && value !== null){
        return true;
    }
    return false;
}

function is_array(value){
    if(isset(value) && value.constructor === Array){
        return true;
    }
    return false;
}

var transform_multiselect = function(parent){
    var objs;
    if(isset(parent)){
        objs = $(parent).find('.multi-select');
    }else{
        objs = $('.multi-select');
    }
    objs.each(function(){
        var data = $(this).attr('raw-data');
        var id = $(this).attr('id');
        if(isset(data)){
            data = JSON.parse(data);
            var selected = [];
            data.select.forEach(function(entry){
                selected.push(entry.value);
            });
            if(selected){
                $(this).attr('value',JSON.stringify(selected));
            }
            var magic = $(this).magicSuggest({
                allowFreeEntries: false,
                placeholder: 'Input tags',
                data: data.total,
                valueField: 'value',
                cls : 'insert multi-select',
                renderer: function(data){
                    return data.name;
                },
                resultAsString: true
            });

        }
        else{
            var data_source = $(this).attr('data-source');
            if(isset(data_source)){
                magic = $(this).magicSuggest({
                    allowFreeEntries: false,
                    placeholder: 'Input tags',
                    data: data_source,
                    valueField: 'value',
                    cls : 'insert multi-select',
                    displayField : 'label',
                    resultAsString: true
                });
            }else{
                magic = $(this).magicSuggest({
                    allowFreeEntries: true,
                    placeholder: 'Input tags',
                    cls : 'insert multi-select',
                    displayField : 'label',
                    resultAsString: true
                });
            }
        }
        $('#'+id).data('magic',magic);
        $(magic).on('selectionchange', function(){
            var current_magic = this;
            $.each(this.getSelection(),function(){
                if(this.value == 'disabled'){
                    current_magic.removeFromSelection(this, true);
                }
            });
        });
    });
};

var transform_autocomplete = function(parent){
    var objs;
    if(isset(parent)){
        objs = $(parent).find('input.autocomplete');
    }else{
        objs = $('input.autocomplete');
    }
    objs.each(function(obj){
        var dataSource = $(this).attr('data-source');
        var target = $(this).attr('target');
        var dataValue = isset($(this).attr('data-value'))?JSON.parse($(this).attr('data-value')):null;
        if(isset(dataSource)){
            if($.type(dataSource) === 'function' || $.type(dataSource) === "string"){
                var opt = {};
                if(IsJsonString(dataSource)){
                    dataSource = JSON.parse(dataSource);
                    opt['local'] = true;
                }

                opt['source'] = dataSource;
                if(target){
                    opt['target'] = target;
                }
                if(dataValue){
                    opt['value'] = dataValue;
                }else{
                    opt['value'] = {value : 0, label : ''};
                }
                $(this).render_autocomplete(opt);
            }
        }
    });
};

$.fn.render_autocomplete = function(option){
    if(isset(option)){
        if(isset(option['target'])){
            option['select'] = function (event, ui) {
                $(this).val(ui.item.label);
                $('#'+option['target']).val(ui.item.value);
                return false;
            }
        }

        var default_option = {
            minLength : 0
        };
        if(!isset(option.param)){
            option.param = $(this).data('autocomplete-param');
            if(!isset(option.param)){
                var json_string = IsJsonString($(this).attr('autocomplete-param'));
                if(json_string !== false){
                    option.param = json_string;
                }
            }
        }
        $.extend(true, default_option, option);
        var url = default_option.source;
        if(option['local']){
            default_option.source = url;
        }else{
            default_option.source = function(request, response) {
                $.extend(true, request, default_option.param);
                $.getJSON(url, request, response);
            };
        }
        $(this).autocomplete(default_option).focus(function(){
            $(this).autocomplete('search', $(this).val());
        });
        if(isset(option.value)){
            $(this).val(option.value.label);
            if(isset(option['target'])){
                setTimeout(function(){
                    $('#'+option['target']).val(option.value.value);
                },1000)
            }
        }


        $(this).autocomplete('option','select');
        if(isset($(this).data("ui-autocomplete"))) {
            $(this).data("ui-autocomplete")._renderItem = function (ul, item) {
                if (item.value == 'disabled') {
                    return $('<li class="disabled">')
                        .append(item.label)
                        .appendTo(ul).click(function () {
                            return false;
                        });
                } else {
                    return $("<li>")
                        .append(item.label)
                        .appendTo(ul);
                }
            };
        }
    }
    return $(this);
};

var custom_form_render = function(parent){};

function handleErrors(object){
    var mess = '';
    var popup = 0;
    $.each(object, function(k,v){
        $("#" + k).focus().addAlert({text:v});
        mess += ' - ' + v+'<br>';
        if($('#'+ k).length ==0 && popup == 0){
            popup = 1;
        }
    });
    if(popup == 1) {
        $.msgBox({
            title: "Error",
            type: "error",
            content: mess,
            button: [{value: 'Cancel'}]
        });
    }
}

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

var extensionMethods = {
    getValue : function(){
        var ids = [];
        var name = $(this).attr('name');
        $(this).find('input[name="'+name+'[]"]').each(function(){
            ids.push($(this).val());
        });
        return ids;
    }
};

var extensionMethodsWithNoBody = {
    generateUrl: function(url,params){
        if(_.isUndefined(params)){
            params = {};
        }
        var params_string = $.param(params);
        return url  + params_string!=''?('?'+params_string):'';
    }
};

$.extend(true, $.fn, extensionMethods);
$.extend(true, $, extensionMethodsWithNoBody);
function to_two_digit(n){
    return n > 9 ? "" + n: "0" + n;
}

$('#wf_page_dialog').delegate('input','keypress',function(e){
    if (e.keyCode == 13) {
        $('#wf_page_dialog .btn-primary:first').click();
    }
});

function prompt_dialog(content,type){
    var head_text = '';
    if(!isset(type)){
        type = 'warning';
    }
    switch(type){
        case 'error':
            head_text = 'Error';
            break;
        case 'success':
            head_text = 'Success';
            break;
        case 'warning':
            head_text = 'Warning';
            break;
    }

    $.msgBox({
        title: head_text,
        type: type,
        content: content
    })
}