$(function(){
    $('#review_rating').rating({
        theme: 'krajee-svg',
        size: 'xs',
        starCaptions : function(val) {
            if(val < 2){
                return val + ' star';
            }
            else{
                return val + ' stars';
            }
        }
    });

    $('#review_submit').on('click',function(){
        var review_rating = $('#review_rating').val();
        var review_author_name = $('#review_author_name').val();
        var review_title = $('#review_title').val();
        var review_body = $('#review_body').val();
        if(review_author_name == ''){
            alert('Author name should not be empty.'); return false;
        }
        if(review_title == ''){
            alert('Review title should have minimum 10 characters.'); return false;
        }
        if(review_body.length < 10 ){
            alert('Review body should have minimum 10 characters.'); return false;
        }

        $.ajax({
            url : url + 'booking/customer_reviews/save_customer_reviews',
            type: 'POST',
            data: {
                postData : {
                    review_rating : review_rating,
                    review_title : review_title,
                    review_body : review_body,
                    review_author : review_author_name,
                    customer_id : customerId,
                    bill_id : billId
                }
            },
            success: function(res){
                res = $.parseJSON(res);
                console.log(res);
                if(res.status){
                    console.log(res);
                    customerId = '';
                    billId = '';
                    window.location.href = url+'booking/book';
                }
            }
        });

    });
});