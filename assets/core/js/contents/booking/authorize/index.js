/**
 * Created by Vu Huy on 12/23/2014.
 */

$(function (){
    $('#login_form_main').submit(function (e) {
        e.preventDefault();
        var $this = $(e.currentTarget),
            inputs = {};
        // Send all form's inputs
        var check = true;
        $.each($this.find('input'), function (i, item) {
            var $item = $(item);
            if($item.val() == ""){
                var html = 'Please fill '+ $item.attr('name');
                $('#login_form .login-warning').html(html).show();
                check = false;
                return false;
            }
            else{
                inputs[$item.attr('name')] = $item.val();
            }
        });

        // Send form into ajax
        if(check == true){
            $.system_process({
                url : $this.attr('action'),
                param: inputs,
                success: function(ret,more){
                    location.href = url + 'booking/home';
                },
                fail: function(ret,more){
                    $('#login_form .login-warning').html("Username or password is incorrect").show();
                }
            });
        }
    });
});