/**
 * Created by Vu Huy on 12/13/2014.
 */
$(function (){
    $('#submit-btn').click(function (e) {
        e.preventDefault();
        var $this = $(e.currentTarget),
            inputs = {};
        // Send all form's inputs
        var form_password_current = $('#form_password_current').val();
        var form_password_first = $('#form_password_first').val();
        var form_password_second = $('#form_password_second').val();
        if(form_password_first != form_password_second){
            $.msgBox({
                title:"Message",
                type:"alert",
                content:"Password and repassword is not the same",
                buttons:[{value:"Cancel"}]
            });
            return false;
        }
        else
        {
            // Send form into ajax
            $('#gento_loading').slideDown(100);

            $.system_process({
                url : url + 'booking/authorize/change_password',
                param : { old_password : form_password_current,
                        new_password : form_password_first },
                success: function (res) {
                    setTimeout(function () {
                        window.location.href = url + 'booking/book';
                    }, 1900);
                }
            });
        }
    });
});