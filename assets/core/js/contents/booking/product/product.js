/**
 * Created by Vu Huy on 12/15/2014.
 */
$(function(){

});

function productAddToCartForm(code){
    val = $('#qty').val();
    if(val == 0){
        $.msgBox({
            title: "Message",
            type: "warning",
            content: 'Please add at least 1 item in quantity quantity',
            buttons: [{value: "Cancel"}]
        });
        return;
    }
    $.ajax({
        url : url + 'booking/cart/insert_item',
        type : 'POST',
        data : {code : code, qty:val},
        success: function(data){
            if(data == 1)
            $.msgBox({
                title: "Message",
                type: "info",
                content: "The item has been added to your cart",
                buttons: [{value: "Cancel"}],
                success : function(data){
                    window.location.reload();
                }
            });
            else{
                $.msgBox({
                    title: "Message",
                    type: "error",
                    content: data,
                    buttons: [{value: "Cancel"}]
                });
            }
        }
    });
}