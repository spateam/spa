/**
 * Created by Vu Huy on 12/1/2014.
 */
var control = "reports/";
$('.parent-list a').click(function(e){
    e.preventDefault();
    $('.parent-list a').removeClass('active');
    $(this).addClass('active');
    var currentClass='.child-list .'+ $(this).attr("id");
    $('.child-list .page-header').html($(this).html());
    $('.child-list .list-group').addClass('hidden');
    $(currentClass).removeClass('hidden');

    $('html, body').animate({
        scrollTop: $("#report_selection").offset().top
    }, 500);
});

$('#csv').on('click',function(){
    $.redirectWithData('post',url + system + control + 'show_detail/' + report_name + '/csv', post_back);
});
$('#excel').on('click',function(){
    $.redirectWithData('post',url + system + control  + 'show_detail/' + report_name + '/excel', post_back);
});

$(document).on('click', '#print', function() {
    $(this).hide();
    $('#mainBlock').addClass('printLayout');
    window.print();
    $(this).show();
    $('#mainBlock').removeClass('printLayout');
});

$(function(){
    render_form();
    //$('[table-toggle]').trigger('click');
    //$('[row-toggle]').trigger('click');

});
//$('body').delegate('[table-toggle]','click',function(){
//    var toggle = $(this).attr('table-toggle');
//    if(toggle == 'off'){
//        $(this).closest('table').find('tr').hide();
//        $(this).show();
//        $(this).attr('table-toggle','on');
//    }else{
//        $(this).closest('table').find('tr').show();
//        $(this).attr('table-toggle','off');
//    }
//});
//
//$('body').delegate('[row-toggle]','click',function(){
//    var toggle = $(this).attr('row-toggle');
//    if(toggle == 'off'){
//        $(this).nextUntil('[row-toggle]').hide();
//        $(this).attr('row-toggle','on');
//    }else{
//        $(this).nextUntil('[row-toggle]').show();
//        $(this).attr('row-toggle','off');
//    }
//});

$("#condition_filter input").click(function(){
    var value = $(this).val();
    if(value == 'item' || value == 'monthly-branch-total'){
        $("#condition_filter_summary").hide();
    }
    else
        $("#condition_filter_summary").show();
})

var current_system = $("#current_system").val();
var init_data = {};
if(current_system == "admin")
    var current_url = url + 'admin/branch/new_suggest_by_branch_group';
else
    var current_url = url + 'staff/branch/new_suggest_by_branch_group';
$.ajax({
    url : current_url,
    type : 'POST',
    async : false,
    data : {branch_group_id : 0,minimal: true},
    success: function(result){
        result = JSON.parse(result);
        init_data = result;
    }
});
var magic = $("#ec_branch_id").magicSuggest({
    allowFreeEntries: true,
    displayField : 'label',
    valueField: 'value',
    data: init_data,
    placeholder: 'Input tags',
    cls : 'insert multi-select',
    resultAsString: true
});

$("#ec_branch_group_id").change(function(){
    var branch_group_id = $(this).val();
    $.ajax({
        url : current_url,
        type : 'POST',
        async : false,
        data : {branch_group_id : branch_group_id,minimal: true},
        success: function(result){
            result = JSON.parse(result);
            magic.clear(true);
            magic.setData(result);
        }
    });
});