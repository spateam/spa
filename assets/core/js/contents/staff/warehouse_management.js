/**
 * Created by Vu Huy on 2/11/2015.
 */


var custom_form_render = function(){
    $('#item_autocomplete').autocomplete({
        select: function(ev,ui){
            ev.preventDefault();
            $('#item_autocomplete').val(ui.item.label);
        }
    });

};


var add_product_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    $.system_dialog({
        url : url + 'add_product_form/'+id,
        more :table_data
    });
};

var add_quantity_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_data_before_add_quantity', success:'after_success', more:table_data}};
    $.system_dialog({
        url : url + 'add_quantity_form/'+id,
        more :table_data,
        button: buttons
    });
};

var remove_quantity_form = function(id,url,table_id){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_data_before_remove_quantity', success:'after_success', more:table_data}};
    $.system_dialog({
        url : url + 'remove_quantity_form/'+id,
        more :table_data,
        button: buttons
    });
};

$('#wf_page_dialog').delegate('#item_autocomplete','autocompleteselect',function(ev,ui){
    $('#item_id').val(ui.item.value);

    var warehouse_id = $('#warehouse_id').val();

    $.system_process({
        url : url + 'staff/warehouse_management/item_info/' + ui.item.value + '/' + warehouse_id,
        close_modal : 0,
        success : function(ret,more){
            $('#item_change').removeAttr('disabled');
            $('#quantity').val(ret.data['quantity']);
        }
    });
});


var get_data_before_remove_quantity = function(){
    var id = $('#id').val();
    var new_amount = Number($('#quantity').val()) - Number($('#item_change').val());
    $('#quantity').val(new_amount);
    $('#item_change').val(0);
    var data = get_insert_values();
    data.id = id;
    data.field_post['log_data'] = {log_type : 'import_pro'};
    return data;
};

var get_data_before_add_quantity = function(){
    var id = $('#id').val();
    var new_amount = Number($('#quantity').val()) + Number($('#item_change').val());
    $('#quantity').val(new_amount);
    $('#item_change').val(0);
    var data = get_insert_values();
    data.id = id;
    data.field_post['log_data'] = {log_type : 'import_pro'};
    return data;
};