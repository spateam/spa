$(document).ready(function(){

    var tables = $('#inbox-appointments').DataTable({
        ajax          : getDataURL,
        deferRender   : true,
        aoColumns : [
            { mData : "bookingDate" },
            { mData : "start_time" },
            { mData : "end_time" },
            { mData : renderName },
            { mData : "code" },
            { mData : "mobile_number" },
            { mData : "email" },
            { mData : "itemName" }
        ],
        fnRowCallback: function(nRow, aData, iDisplayIndex,iDisplayIndexFull){
            if(aData.is_new == 1){
                $('td', nRow).css('background-color', '#ADD8E6'); // light blue
            }
            else{
                $('td', nRow).css('background-color', '#FFFFE0'); // light yellow
            }
        },
        paging        : true,
        ordering      : true,
        info          : true
    });

    function renderName(data,type,dataToSet){
        return '<a href="' + url + 'staff/scheduler?date=' + moment(data.start_time).format('YYYY-MM-DD') + '&is_full_screen=0&dp_mode=0" target="_blank">' + data.name + '</a>';
    }

    $('#reload-table').on('click', function(){
        tables.ajax.reload();
    });
});