/*
 * Created by Vu Huy on 1/6/2015.
 */
var marked_time = marked_time;
var historyTable = '';
var byPass = 0;
var employees = employees;
var bookings = bookings;
var bookings_comment = bookings_comment;
var employee_section = [];
var data = [];
// Save editting block -- Thao Tran Minh 2015/25/08
var editblock_array = [];
var edit_form = $('#my_form');
var list_pending_block = new Array();
var ondrag_employee_id = 0;

//Some helper
var html = function(id) { return document.getElementById(id); };
var standard_time_string = scheduler.date.date_to_str("%H:%i");
var standard_date_time_string = scheduler.date.date_to_str("%Y-%m-%d %H:%i:%s");
var to_standard_date_time = scheduler.date.str_to_date('%Y-%m-%d %H:%i:%s');
sessionStorage.setItem('request_pin',0);

/*--- function ----*/
var reorder;
$(function(){
    render_form();
    ready_data();
    init();
    $('#submit-btn').on('click',function(){
        var id = $('#employee_chosen_id').val();
        window.location.href = url + 'staff/scheduler/index/employee/' + id;
    });
    $('body').delegate('.dropdown-menu','click',function(e){
        e.stopPropagation();
    });
    if(dp_mode == 0){
        $('#change_dp_mode').html('View Cancel Scheduler');
    }
    else{
        $('#change_dp_mode').html('View Booking Scheduler');
    }
});
/**
 * Created by Vu Huy on 1/12/2015.
 */

var init = function() {
    scheduler.locale.labels.unit_tab = "Show All";
    scheduler.locale.labels.grid_tab = "Grid";
    scheduler.config.display_marked_timespans = true;
    scheduler.config.all_timed = true;
    scheduler.config.multi_day = true;
    scheduler.config.multisection = true;
    scheduler.config.event_duration = 35;
    scheduler.config.details_on_create = true;
    scheduler.config.details_on_dblclick = true;
    scheduler.config.dblclick_create = true;
    scheduler.config.drag_create  = true;
    scheduler.config.auto_end_date = true;
    scheduler.config.hour_size_px = 132;
    scheduler.config.time_step = 5;
    scheduler.config.default_date = "%Y-%m-%d, %D";
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";
    scheduler.config.limit_time_select = true;
    scheduler.config.first_hour = 10;
    scheduler.config.last_hour = Math.ceil(store_open_hour[1]);

    scheduler.config.check_limits = false;

    scheduler.config.icons_select = [
        "icon_details"
    ];

    if(!dp_mode){
        scheduler.config.icons_select.push("icon_reserve");
        scheduler.locale.labels.icon_reserve = 'Cancel';
    }
    else{
        scheduler.config.dblclick_create = false;
        scheduler.config.drag_create  = false;
        scheduler.config.drag_move = false;
        scheduler.config.icons_select.pop('icon_details');
        scheduler.config.icons_select.pop("icon_reserve");
    }

    if( right['d']){
        scheduler.config.icons_select.push("icon_custom_delete");
        scheduler.locale.labels.icon_custom_delete = 'Delete';
    //    scheduler.config.icons_select.push("icon_delete");
    }
    scheduler.config.drag_resize = false;
    scheduler.config.mark_now = false;
    if(! right['i']){
        scheduler.config.drag_create = false;
        scheduler.config.dblclick_create = false;
    }

    if(right['i'] && right['e'] && right['d']){
        scheduler.config.icons_select.push('icon_create_bill');
        scheduler.locale.labels.icon_create_bill = 'Create Bill';
    }

    var sections = employee_section;
    scheduler.createUnitsView({
        name:"unit",
        property:"section_id",
        list: sections,
        size:employees.length,
        step:15
    });
    scheduler.createGridView({
        name:"grid",
        fields:[    // defines columns of the grid
            {id:"created_date",   label:'<span class="grid_header_col_style">Created Time</span>', width:180,sort:'date',css:'grid_col_style'},
            {id:"start_date",   label:'<span class="grid_header_col_style">Start Time</span>', width:150,sort:'date',css:'grid_col_style'},
            {id:"end_date",   label:'<span class="grid_header_col_style">End Time</span>', width:150,sort:'date',css:'grid_col_style'},
            {id:"customer_name",   label:'<span class="grid_header_col_style">Customer</span>',width:200,sort:'str',css:'grid_col_style'},
            {id:"employee_name",   label:'<span class="grid_header_col_style">Employee</span>',width:200,sort:'str',css:'grid_col_style'},
            {id:"service_name",   label:'<span class="grid_header_col_style">Service</span>',width:200,sort:'str',css:'grid_col_style'},
            {id:"room_name",   label:'<span class="grid_header_col_style">Room</span>',width:160,sort:'str',css:'grid_col_style'},
            {id:"comment", label:'<span class="grid_header_col_style">Comment</span>',sort:'date',align:"left", width:'*',css:'grid_col_style'}
        ]
    });

    scheduler.templates.event_class = function(start,end,event){
        var css = "";
        switch(Number(event.status)){
            case 1:{
                if(Number(event.type) == 1){
                    event.color = '#64DF3D'; // green
                }
                else if(Number(event.type) == 2){
                    event.color = '#B9E7F7'; // blue
                }
            } break;
            case 2:{
                event.color = '#64DF3D'; // green of wait to confirm
            } break;
            case 3:{
                event.color = 'gray';
            } break;
            case 4:{
                event.color = '#EED8BA'; // brown
            } break;
            case 8: {
                if(Number(event.type) == 1){
                    event.color = '#9e70a2' // dark purple
                }
                else{
                    event.color = '#F1D2F4';
                }
            } break;
            case 9: {
                event.color = '#FF85D4'; // not really light pink
            } break;
            default :{
                event.color = '#EED8BA';
            }
        }

        return css; // default return
    };

    scheduler.templates.unit_scale_text = function(key, label, unit) {
        var html = "<div style='font-size:18px; text-overflow: ellipsis; float:left; width:80%; height:100%; overflow: hidden'>" +
            "<a href='{0}'>{1}</a></div>";
        var index_space = label.split(' ');
        label = index_space[0]+' '+index_space[1];
        html = html.format(url + 'staff/scheduler/index/employee/' + key+ '?is_full_screen='+(is_full_screen?1:0), label);
        var order_html = $('#dropdown-order-sample').html().format(unit.key);
        return html + order_html;
    };

    addnewcustomer = function(){
        $.system_process({
            url : url + 'staff/scheduler/getTest',
            success: function(data){
                $(".modal-lg").attr("id","current_eventid");
                $("#current_eventid").hide();
                $("#wf_page_dialog").append(data.data);
            }
        });
    };

    savenewcustomer = function(){
        var firstname = $("#customer_simple_form #first_name").val();
        var lastname = $("#customer_simple_form #last_name").val();
        var mobilenumber = $("#customer_simple_form #mobile_number").val();
        var nric           = $("#customer_simple_form #nric").val();
        var gender         = $("#customer_simple_form #gender").val();
        var email          = $("#customer_simple_form #email").val();
        var recovery_email = $("#customer_simple_form #recovery_email").val();
        var phone_number   = $("#customer_simple_form #phone_number").val();
        var customer_type  = $("#customer_simple_form #customer_type").val();
        var birthday       = $("#customer_simple_form #birthday").val();
        var pin            = $("#customer_simple_form #pincode").val();
        var cpin            = $("#customer_simple_form #cpincode").val();

        if(!firstname || firstname == ''){
            alert('First Name should not be blank.'); return false;
        }
        if(!mobilenumber || mobilenumber == ''){
            alert('Mobile Number should not be blank.'); return false;
        }
        if(customer_type == 2){
            if(!email || email == ''){
                alert('Primary Email should not be blank.');return false;
            }
            if(pin == ''){
                alert('The PIN can not be empty.');return false;
            }
            else{
                if(cpin == ''){
                    alert('Please type the Confirm PIN.');return false;
                }
            }
            if(pin !== cpin){
                alert('The PIN and confirm PIN does not match.');return false;
            }
        }
        $.system_process({
            url : url + 'customers/editSimple/',
            async: false,
            param: {
                first_name: firstname,
                last_name: lastname,
                mobile_number: mobilenumber,
                nric: nric,
                gender: gender,
                email: email,
                recovery_email: recovery_email,
                phone_number: phone_number,
                customer_type : customer_type,
                birthday : birthday,
                pin : pin
            },
            success: function(data){
                    $("#current_eventid").show();
                    $("#customer_simple_form").remove();
                    sessionStorage.setItem('request_pin', 1);
                    $('#customer_id').val(data.data[0].id);
                    $('#customer_id_typeahead').val(data.data[0].first_name + ' ' + data.data[0].last_name + ' ' + data.data[0].code + ' - ' + data.data[0].mobile_number);
            }
        });
    };

    closenewcustomer = function(){
        $("#current_eventid").show();
        $("#customer_simple_form").remove();
    };

    reorder = function(employee_id,element){
        var order = $(element).parent().find('input').val();
        if(!order){
            $.msgBox({
                type : 'error',
                content : 'Please input a number'
            });
        }
        $.system_process({
            url : url + 'staff/scheduler/reorder/' + employee_id + '/' + order,
            success: function(){
                refresh_scheduler();
            }
        });
    };

    set_online_offline = function(employee_id){
        var first_hour = scheduler.config.first_hour;
        var last_hour = scheduler.config.last_hour;
        var date = scheduler._date;
        date = moment(date).format('YYYY-MM-DD');
        $.msgBox({
            title: "Set online/offline",
            type: "confirm",
            content: "Are you sure to set online/offline this employee?",
            buttons: [{value: "Yes"}, {value: "No"}],
            success: function(result){
                if(result == 'Yes') {
                    $.system_process({
                        url : url + 'staff/scheduler/set_online_offline',
                        param: {employee_id: employee_id,date: date,first_hour: first_hour,last_hour: last_hour},
                        success: function(){
                            refresh_scheduler();
                        }
                    });
                }
                else{
                    return false;
                }
            }
        });
    };
    var format = scheduler.date.date_to_str("%h %i %A");
    scheduler.templates.hour_scale = function(date){
        return format(date);
    };
    scheduler.templates.quick_info_content = function(start, end, ev){
        return event_to_text(ev);
    };

    /*--------------------------------*/
    scheduler.checkCollisionCustom = function(ev){
        var events = scheduler.getEvents(ev.start_date,ev.end_date);
        for(var i = 0; i < events.length; i++){
            if(events[i].section_id == ev.section_id && events[i].id != ev.id){
                return false;
            }
        }
        return true;
    };

    scheduler.attachEvent('onViewChange',function(new_mode , new_date){
        get_fifteen_blocks(bookings_comment);
    });

    scheduler.attachEvent('onEventChanged',function(){
        refresh_scheduler();
    });

    scheduler.attachEvent("onBeforeDrag", function (id, mode, e){
        ondrag_event = scheduler.getEvent(id);
        if(ondrag_event != undefined && ondrag_event.employee_id != undefined) {
            ondrag_employee_id = ondrag_event.employee_id;
        }
        return true;
    });

    scheduler.attachEvent("onTemplatesReady", function() {
        var highlight_step = 60; // we are going to highlight 30 minutes timespan

        var highlight_html = "";
        var hours = scheduler.config.last_hour - scheduler.config.first_hour; // e.g. 24-8=16
        var times = hours*60/highlight_step; // number of highlighted section we should add
        var height = scheduler.config.hour_size_px;//*(highlight_step/60);
        for (var i=0; i<times; i++) {
            highlight_html += "<div class='highlighted_timespan' style='height: "+(height-1)+"px;'></div>"
        }

        /*---- Set blocked date ----*/
        branch_data.branch_offline_data.forEach(function(obj){
            scheduler.addMarkedTimespan({
                start_date : to_standard_date_time(obj.start_time),
                end_date   : to_standard_date_time(obj.end_time),
                type       :  "dhx_time_block"
            });

            var start_date = to_standard_date_time(obj.start_time);
            start_date = moment(start_date);
            start_date = start_date.format('YYYY-MM-DD');
            scheduler.addMarkedTimespan({
                start_date : to_standard_date_time(start_date + ' 00:00:00'),
                end_date   : to_standard_date_time(start_date + ' 23:59:59'),
                html    : highlight_html,
                type    : 'highlight'
            });
        });
        if(isset(current_employee_id)){ // nếu chọn xem chi tiết nhân viên
            var employee = employee_map[current_employee_id];
            // Block open/close time for scheduler
            scheduler.addMarkedTimespan({
                days:  "fullweek",
                zones: [0*60, store_open_hour[0]*60],
                css: "open_store_time",
                type:  "dhx_time_block" //the hardcoded value
            });

            var employee = employee_map[current_employee_id];
            for(key in employee.blocked_calendar_date){
                var blocked_date = employee.blocked_calendar_date[key];
                if(blocked_date.length){
                    blocked_date[0] = store_open_hour[0]*60;
                    if(blocked_date[1] < store_open_hour[0]*60){
                        blocked_date[1] = store_open_hour[0]*60;
                    }
                    if(blocked_date[2] > store_open_hour[1]*60){
                        blocked_date[2] = store_open_hour[1]*60;
                    }
                    scheduler.addMarkedTimespan({
                        days : key,
                        zones : blocked_date,
                        type:  "dhx_time_block"
                    });

                    // Fix for dhtmlx blocked time priority on scheduler week view
                    employee.blocked_absent_date.forEach(function(obj){
                        scheduler.addMarkedTimespan({
                            days       : moment(obj.start_time,'YYYY-MM-DD').toDate(),
                            zones      : blocked_date,
                            type       :  "dhx_time_block"
                        });

                        scheduler.addMarkedTimespan({
                            days:  moment(obj.start_time,'YYYY-MM-DD').toDate(),
                            zones: [0*60, store_open_hour[0]*60],
                            css: "open_store_time",
                            type:  "dhx_time_block" //the hardcoded value
                        });
                    });
                }else{
                    scheduler.addMarkedTimespan({
                        days : key,
                        zones : 'fullday',
                        type:  "dhx_time_block"
                    });
                }
            }
            //Set absent on calendar
            employee.blocked_absent_date.forEach(function(obj){
                var obj_des = obj.description;
                obj_des = obj_des.replace(/(\r\n|\n|\r)/gm, '<br/>');
                scheduler.addMarkedTimespan({
                    start_date : to_standard_date_time(obj.start_time),
                    end_date   : to_standard_date_time(obj.end_time),
                    type       :  "dhx_time_block",
                    html       :  '<input type="hidden" value="'+obj.id+'"><div style="padding-left:10%;" class="row dhx_time_block_inner_html '+obj.type.label+'">'+obj_des+'</div>'
                });
                var start_date = to_standard_date_time(obj.start_time);
                start_date = moment(start_date);
                start_date = start_date.format('YYYY-MM-DD');
                scheduler.addMarkedTimespan({
                    start_date : to_standard_date_time(start_date + ' 00:00:00'),
                    end_date   : to_standard_date_time(start_date + ' 23:59:59'),
                    html    : highlight_html,
                    type    : 'highlight'
                });
            });
        }else{
            // Ngoài menu chính
            for(order_key in employees){
                var employee = employees[order_key];
                var employee_key = employee.id;
                var loop_flag = 1;

                scheduler.addMarkedTimespan({
                    days:  "fullweek",
                    zones: [0*60, store_open_hour[0]*60],
                    css: "open_store_time",
                    sections : {unit:employee_key},
                    type:  "dhx_time_block" //the hardcoded value
                });
                for(key in employee.blocked_calendar_date){

                    var blocked_date = employee.blocked_calendar_date[key];
                    if(blocked_date.length){
                        // set all block on employee (block, away ...)
                        blocked_date[0] = store_open_hour[0]*60;
                        if(blocked_date[1] < store_open_hour[0]*60){
                            blocked_date[1] = store_open_hour[0]*60;
                        }
                        if(blocked_date[2] > store_open_hour[1]*60){
                            blocked_date[2] = store_open_hour[1]*60;
                        }

                        scheduler.addMarkedTimespan({
                            days : key,
                            zones : blocked_date,
                            sections : {unit:employee_key},
                            type: 'dhx_time_block'
                        });

                        // Fix for dhtmlx blocked time priority on scheduler unit view
                        employee.blocked_absent_date.forEach(function(obj){
                            scheduler.addMarkedTimespan({
                                days       : moment(obj.start_time,'YYYY-MM-DD').toDate(),
                                zones      : blocked_date,
                                sections   : {unit:employee_key,week:employee_key},
                                type       :  "dhx_time_block"
                            });
                        });
                    }else{
                        scheduler.addMarkedTimespan({
                            days : key,
                            zones : 'fullday',
                            sections : {unit:employee_key},
                            type:  "dhx_time_block"
                        });
                    }
                }
                //Set absent on calendar
                employee.blocked_absent_date.forEach(function(obj){
                    var s_H = moment(obj.start_time).format('HH');
                    var e_H = moment(obj.end_time).format('HH');
                    var s_M = moment(obj.start_time).format('mm');
                    var e_M = moment(obj.end_time).format('mm');
                    var obj_des = obj.description;
                    obj_des = obj_des.replace(/(\r\n|\n|\r)/gm, '<br/>');
                    scheduler.addMarkedTimespan({
                        days       : moment(obj.start_time,'YYYY-MM-DD').toDate(),
                        zones      : [(s_H*60)+(1*s_M),(e_H*60)+(1*e_M)],
                        sections   : {unit:employee_key,week:employee_key},
                        type       :  "dhx_time_block",
                        class      :  "dhx_time_block",
                        html       :  '<input type="hidden" value="'+obj.id+'"><div style="padding-top:15px;" class="row dhx_time_block_inner_html '+obj.type.label+'">'+obj_des+'</div>'
                    });
                });
            }

        }
        scheduler.addMarkedTimespan({
            days    : "fullweek",
            zones   : "fullday",
            html    : highlight_html,
            type    : 'highlight'
        });
    });

    scheduler.attachEvent("onBeforeEventChanged", function(ev, e, is_new, original){
        if(/*ev.status == 8 ||*/ ev.status == 9){
            $.msgBox({
                type : 'error',
                title: 'Message',
                content: "You can't move the appointment has been create HOLD BILL"
            });
            return false;
        }
        /*if(ev.status == 2){
            $.msgBox({
                type : 'error',
                title: 'Message',
                content: "You can't move the booking online when you havent confirm customer yet"
            });
            return false;
        }*/
        if(dp_mode){
            return false;
        }
        if(! isset(ev.is_old)){
            if(! right['i']){
                no_permission_message('You don\'t have permission to insert new booking');
                ev.client_delete = true;
                scheduler.deleteEvent(ev.id);
                return false;
            }
        }else{
            if(! right['e']){
                no_permission_message('You don\'t have permission to edit a booking');
                return false;
            }
        }

        // If moving absent block
        if(isset(ev.is_old)) {
            if(ev.is_old == "unblocking") {
                check_collision({event:ev});

                var id = ev.section_id;//blocked_id
                var data = {
                    date : moment(ev.start_date).format('YYYY-MM-DD HH:mm:ss'),
                    absent_from : moment(ev.start_date).format('YYYY-MM-DD HH:mm:ss'),
                    absent_to   : moment(ev.end_date).format('YYYY-MM-DD HH:mm:ss'),
                    absent_whole_day : 0,
                    type             : ev.type,
                    description      : ev.text,
                };
                $.system_process({
                    url: url + 'employees/change_absent',
                    param: {id: id,data: data},
                    success: function(ret, more){
                        refresh_scheduler();
                    }
                });

                $.system_process({
                    url: url + 'employees/delete_absent_date',
                    param: {id:ev.blocked_id},
                    success: function(result){
                        scheduler.config.check_limits = true;
                        //scheduler.config.drag_resize= false;
                        refresh_scheduler();
                    }
                });

                return true;
            }
        }
        if(is_new)
            return true;
        try{
            if(ev.bundle_id){
                var data = {
                    customer_id : ev.customer_id,
                    room_id     : ev.room_id,
                    bundle_id   : ev.bundle_id,
                    comment     : ev.comment,
                    is_old      : ev.is_old,
                    booking_id  : ev.booking_id,
                    no_preference: ev.no_preference,
                    is_bypass : ev.is_bypass,
                    sub_service : {}
                };
                data['sub_service'][ev.service.id] = {
                    employee_id: ev.section_id,
                    room_id    : ev.room.id,
                    start_time : standard_date_time_string(ev.start_date)
                };

                var diff = moment(ev.start_date).diff(moment(original.start_date));
                var evs = scheduler.getEvents();
                $.each(evs,function(order,object){
                    if(object.booking_id == ev.booking_id && ev.id != object.id){
                        object.start_date = moment(object.start_date).add(diff).toDate();
                        object.end_date = moment(object.end_date).add(diff).toDate();
                        check_collision({event : object});
                        data['sub_service'][object['service'].id] = {
                            employee_id : object['employee'].id,
                            room_id     : object['room'].id,
                            start_time  : standard_date_time_string(object.start_date)
                        };
                    }
                });

                $.system_process({
                    url : url + 'staff/scheduler/save_event',
                    more: {event_id : ev.id},
                    param: {field_post : data},
                    success: 'after_save_event',
                    fail: function(){
                        system_alert({
                            type : 'fail',
                            content: result.message
                        });
                        ev.client_delete = 1;
                        scheduler.deleteEvent(ev.id);
                        scheduler.addEvent({
                            created_date    : ev.created_date ,
                            customer_id     : ev.customer_id  ,
                            customer_name   : ev.customer_name,
                            customer        : ev.customer     ,
                            code            : ev.code         ,
                            booking_id      : ev.booking_id   ,
                            comment         : ev.comment      ,
                            is_old          : ev.is_old,
                            start_date      : ev.start_date ,
                            end_date        : ev.end_date   ,
                            employee        : ev.employee   ,
                            room            : ev.room       ,
                            service         : ev.service    ,
                            employee_id     : ev.employee_id,
                            employee_name   : ev.employee_name,
                            service_name    : ev.service_name,
                            room_name       : ev.room_name  ,
                            text            : ev.text       ,
                            bundle_id       : ev.bundle_id  ,
                            section_id      : ev.section_id,
                            is_bypass       : ev.is_bypass,
                            color           : 'red'
                        });
                    }
                });
            }
            else{
                var post_data = '';
                if(ev.status != 4 && ev.status != 5 && ev.status != 6 && ev.status != 7) {
                    check_collision({event: ev});
                    post_data = {
                        field_post: {
                            customer_id: ev.customer_id,
                            room_id: ev.room_id,
                            employee_id: ev.section_id,
                            start_time: standard_date_time_string(ev.start_date),
                            comment: ev.comment,
                            service_id: ev.service.id,
                            is_old: ev.is_old,
                            hidden_end_time: standard_date_time_string(ev.end_date),
                            booking_id: ev.booking_id,
                            booking_status: ev.status,
                            no_preference: ev.no_preference,
                            is_bypass : ev.is_bypass
                        }
                    };

                    if(ondrag_employee_id != ev.section_id && ev.no_preference == 0){
                        $.msgBox({
                            title: 'Message',
                            content: 'This is a Preferred. Are you sure you want to change therapist of this booking?',
                            type: 'alert',
                            buttons: [{value: "Yes"}, {value: "Cancel"}],
                            success: function (result) {
                                if (result == 'Yes') {
                                    post_data.field_post.scheduler = 1;
                                    post_data.field_post.byPass = 0;
                                    $.ajax({
                                        url : url + 'staff/scheduler/check_room_quantity_bed',
                                        type: 'POST',
                                        data: post_data,
                                        success: function(res){
                                            res = $.parseJSON(res);
                                            if (!res.status) {
                                                $.msgBox({
                                                    title: 'Message',
                                                    content: 'No more available Room. Are you sure you want to bypass the room checking validation and move this appointment here?',
                                                    type: 'alert',
                                                    buttons: [{value: "Yes"}, {value: "Cancel"}],
                                                    success: function (res) {
                                                        if(res == 'Yes') {
                                                            post_data.field_post.byPass = 1;
                                                            processEvent(ev.id, post_data);
                                                        }
                                                    }
                                                });
                                            }
                                            else{
                                                post_data.field_post.byPass = 0;
                                                post_data.field_post.is_bypass = 0;
                                                post_data.field_post.room_id = res.data;
                                                processEvent(ev.id,post_data);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
                else{
                    check_collision({event: ev});
                    post_data = {
                        field_post: {
                            employee_id: ev.section_id,
                            start_time: standard_date_time_string(ev.start_date),
                            room_id: (ev.room ? ev.room.id : ''),
                            comment: ev.text,
                            booking_id: ev.booking_id,
                            hidden_end_time: standard_date_time_string(ev.end_date),
                            service_id: 0,
                            customer_id: 0,
                            booking_status : ev.status,
                            no_preference: ev.no_preference,
                            is_bypass : ev.is_bypass
                        }
                    };
                }

                if(ev.room) {
                    post_data.field_post.scheduler = 1;
                    post_data.field_post.byPass = ev.is_bypass;
                    $.ajax({
                        url : url + 'staff/scheduler/check_room_quantity_bed',
                        type: 'POST',
                        data: post_data,
                        success: function(res){
                            res = $.parseJSON(res);
                            if (!res.status) {
                                $.msgBox({
                                    title: 'Message',
                                    content: 'No more available Room. Are you sure you want to bypass the room checking validation and move this appointment here?',
                                    type: 'alert',
                                    buttons: [{value: "Yes"}, {value: "Cancel"}],
                                    success: function (res) {
                                        if(res == 'Yes') {
                                            post_data.field_post.byPass = 1;
                                            post_data.field_post.is_bypass = 1;
                                            processEvent(ev.id, post_data);
                                        }
                                    }
                                });
                            }
                            else{
                                post_data.field_post.byPass = 0;
                                post_data.field_post.is_bypass = 0;
                                post_data.field_post.room_id = res.data;
                                processEvent(ev.id,post_data);
                            }
                        }
                    })
                }
                else{
                    processEvent(ev.id, post_data);
                }
            }
          //  return true;
        }
        catch(msg){
            $.msgBox({
                type : 'error',
                title: 'message',
                content: msg
            });
            return false;
        }
    });

    scheduler.attachEvent('onBeforeLightBox',function(id){
        if(dp_mode){
            return false;
        }
        var ev = scheduler.getEvent(id);
        if(! isset(ev.is_old)){
            if(! right['i']){
                no_permission_message('You don\'t have permission to insert new booking');
                ev.client_delete = true;
                scheduler.deleteEvent(id);
                return false;
            }
        }else{
            if(! right['e']){
                no_permission_message('You don\'t have permission to edit a booking');
                return false;
            }
        }
        if(!isset(ev.section_id)){
            ev.section_id = current_employee_id;
        }
        popUpFormReady(ev);
        edit_form.modal('show');
    });

    //scheduler.attachEvent("onClick", function (id, e){
    //    //any custom logic here
    //    var ev = scheduler.getEvent(id);
    //    scheduler.deleteMarkedTimespan({
    //        start_date: ev.start_date,
    //        end_date: ev.end_date,
    //        sections: {week:ev.section_id,unit:ev.section_id}
    //    });
    //    return true;
    //});

    scheduler.attachEvent('onBeforeEventDelete',function(id,e){
        var ev = scheduler.getEvent(id);
        if(ev.client_delete){
            return true;
        }
        if(isset(ev.is_old)){
            if(! right['d']){
                no_permission_message("You don't have permission to delete this booking");
                return false;
            }
        }

        $.system_process({
            url : url + 'staff/scheduler/delete_event',
            param: {field_post: {
                booking_id: ev.booking_id
            }},
            more: {event_id : ev.id},
            success: 'after_delete_event'
        });
        return false;
    });

    scheduler._click.buttons.create_bill = function(id){
        sessionStorage.removeItem('pre_bill_data');
        var ev = scheduler.getEvent(id);
        if(ev.status == 1) {
            var pre_bill_data = {
                'booking_id' : ev.booking_id,
                'customer_id': ev.customer_id,
                'item': ev.service.id,
                'employee_id': ev.employee_id,
                'comment' : ev.comment
            };
            $.system_process({
                url: url + 'sales/savePreSessionBill',
                param: {field_post: pre_bill_data},
                success: function (res) {
                    sessionStorage.setItem('pre_bill_data', JSON.stringify(pre_bill_data));
                    window.open(url + "sales", '_blank');
                }
            });
        }
        else if(ev.status == 2){
            alert('You need to confirm the customer before create bill on this appointment.');
        }
        else{
            alert('You can not create bill from this type of block');
        }
        //window.location.replace(url + "sales");
    };

    scheduler._click.buttons.reserve = function(id){
        var ev = scheduler.getEvent(id);
        if(ev.status == 1 || ev.status == 2) {
            $.msgBox({
                title: 'Message',
                content: 'Reason for cancellation<br>Reason - Time/Date - Your Name<br><br><textarea id="reserve-comment" cols="35" placeholder="Type your comment"></textarea>',
                type: 'alert',
                buttons: [{value: "Yes"}, {value: "Cancel"}],
                success: function (result) {
                    if (result == 'Yes') {
                        $.system_process({
                            url: url + 'staff/scheduler/reserve_event',
                            param: {
                                field_post: {
                                    booking_id: ev.booking_id,
                                    reserve_comment: $('#reserve-comment').val()
                                }
                            },
                            success: 'refresh_scheduler'
                        });
                    }
                }
            });
        }
        else{
            alert('You can not reserve this booking from this type of block');
        }
    };

    scheduler._click.buttons.custom_delete = function(id){
        var ev = scheduler.getEvent(id);

        if(ev.status == 8 || ev.status == 9){
            $.msgBox({
                title  : 'Message',
                content: "You can't delete the appointment has been paid or hold",
                type : 'alert',
            });
            return false;
        }

        if(ev.client_delete){
            return true;
        }
        if(isset(ev.is_old)){
            if(! right['d']){
                no_permission_message("You don't have permission to delete this booking");
                return false;
            }
        }
        if(ev.recurring_type != null){
            $.msgBox({
                title  : 'Message',
                content: 'Would you like to cancel this appointment only, or the current and future appointments in the series?',
                type : 'alert',
                buttons:[{value:"This only"},{value:"This and future"},{value:'Cancel'}],
                success: function(result){
                    if(result=='This only'){
                        $.system_process({
                            url : url + 'staff/scheduler/delete_event',
                            param: {field_post: {
                                booking_id: ev.booking_id
                            }},
                            more: {event_id : ev.id},
                            success: 'after_delete_event'
                        });
                    }
                    else if(result == 'This and future'){
                        $.system_process({
                            url : url + 'staff/scheduler/delete_event',
                            param: {field_post: {
                                booking_id: ev.booking_id,
                                delete_recurring: true
                            }},
                            more: {event_id : ev.id},
                            success: 'after_delete_event'
                        });
                    }
                }
            });
            return false;
        }
        else{
            var delete_mess = 'Event will be deleted permanently, are you sure?';
            if(ev.status == 4 || ev.status == 5 || ev.status == 6 || ev.status == 7)
                delete_mess = 'This block will be deleted permanently, are you sure?';
            $.msgBox({
                title  : 'Message',
                content: delete_mess,
                type : 'alert',
                buttons:[{value:"OK"},{value:'Cancel'}],
                success: function(result){
                    if(result=='OK'){
                        $.system_process({
                            url : url + 'staff/scheduler/delete_event',
                            param: {field_post: {
                                booking_id: ev.booking_id
                            }},
                            more: {event_id : ev.id},
                            success: 'after_delete_event'
                        });
                    }
                }
            });
        }
    }

    //Handling mouse event
    scheduler.config.check_limits = false;
    scheduler.config.drag_resize= true;
    scheduler.attachEvent("onMouseMove", function(id, ev){
        var active_event = scheduler.getEvent(id);
        if(active_event != undefined){
            if(active_event.status == 4 || active_event.status == 5 || active_event.status == 6 || active_event.status == 7)
                scheduler.config.drag_resize= true;
            else
               scheduler.config.drag_resize= false;
        }

        if(dp_mode){
            return false;
        }
        var obj = scheduler.getActionData(ev);
        var date = moment(obj.date);
        date = date.format('YYYY-MM-DD HH:mm:ss');
        var element = ev.target;
        if($(element).hasClass('highlighted_timespan')){
            if( $(element).attr('show-button') != '1'){
                var employee_id = isset(obj.section)?obj.section:current_employee_id;

                $('.dhx_time_block').attr('show-button' , 0);
                $('.dhx_time_block .absent-btn').remove();
                $('.highlighted_timespan').attr('show-button' , 0);
                $('.highlighted_timespan .absent-btn').remove();
                $(element).attr('show-button',1);

                var element_style = "font-size: 9px;width:100%; height:25%; line-height:0px; padding-left:5px; padding-top:5px;";
                var element_class = "fleft absent-btn";

                $(['Away','Offline','Block']).each(function(i){
                    $(element).prepend($('<div>',{
                        class   :   element_class,
                        style   :   element_style
                    }).append($('<b>', {
                        onclick : 'create_away_form("'+employee_id+'", "'+date+'", "'+this+'")',
                        style   : 'cursor: pointer;',
                        text    : this
                    })));
                });
            }
        }else if($(element).hasClass('dhx_time_block') || $(element).hasClass('dhx_time_block_inner_html')){
            if($(element).hasClass('dhx_time_block_inner_html')){
                element = $(element).parent();
            }
            if( $(element).attr('show-button') != '1'){
                var blocked_id = $(element).find('input').val();
                var employee_id = isset(obj.section)?obj.section:current_employee_id;
                if(isset(blocked_id)){
                    $('.dhx_time_block').attr('show-button' , 0);
                    $('.dhx_time_block .absent-btn').remove();
                    $('.highlighted_timespan').attr('show-button' , 0);
                    $('.highlighted_timespan .absent-btn').remove();
                    $(element).attr('show-button',1);
                    // Thao Tran Minh -- Edited moving blocking 2015/08/25
                    $(element).prepend($('<div>',{
                        class   : 'fleft absent-btn',
                        style   : "padding-right: 10px;color:blue;font-size: 11px;" // Change style to prevent be blocked by open time hour -- Tran Minh Thao editted
                    }).append($('<div>', {
                        onclick : 'delete_absent_date('+blocked_id+')',
                        text    : 'Remove'
                    }),$('<div>', {
                        onclick : 'edit_absent_date('+blocked_id+')',
                        text    : 'Edit'
                    }),$('<div>', {
                        onclick : 'unblock_absent_date('+blocked_id+',this)',
                        text    : 'Move'
                    })));
                }
            }
        }
    });

    if(date){
        date = to_standard_date_time(date);
    }else{
        date = new Date();
    }
    if(isset(current_employee_id))
        scheduler.init('scheduler_here', date, "week");
    else{
        scheduler.init('scheduler_here', date, "unit");
    }
    scheduler.parse(data,"json");
    $i = 1;

    $.each(bookings_comment, function(i,item){
        bookings_comment[i].id = parseInt(moment().format('x'))+$i;
        if(item.status == 4) {
            var temp_comment = bookings_comment[i].text.replace(/(\r\n|\n|\r)/gm, '<br/>');
            bookings_comment[i].text = temp_comment;
        }
        if(item.status == 5) {
            var temp_comment = bookings_comment[i].text.replace(/(\r\n|\n|\r)/gm, '<br/>');
            bookings_comment[i].text = temp_comment;
        }
        else if(item.status == 6){
            var temp_comment = bookings_comment[i].text.replace(/(\r\n|\n|\r)/gm, '<br/>');
            bookings_comment[i].text = temp_comment;
        }
        else if(item.status == 7){
            var temp_comment = bookings_comment[i].text.replace(/(\r\n|\n|\r)/gm, '<br/>');
            bookings_comment[i].text = temp_comment;
        }
        $i-= 1000;
    });
    scheduler.parse(bookings_comment,"json");
    get_fifteen_blocks(bookings_comment);
};

function processEvent(evID,post_data){
    $.system_process({
        url : url + 'staff/scheduler/save_event',
        more: {event_id : evID},
        param: post_data,
        success: 'after_save_event',
        fail: function(result){
            system_alert({
                type : 'fail',
                content: result.message
            });
        }
    });
}

function get_fifteen_blocks(bookings_comment){
    var fifteen_block = [];
    for(var i=0;i<bookings_comment.length;i++){
        var obj = bookings_comment[i];
        var start = obj.start_time;
        var end = obj.end_time;
        var d2 = new Date(end);
        var d1 = new Date(start);
        var seconds =  (d2- d1)/1000;
        if(seconds <= 900)
            fifteen_block.push(obj.id);
    }

    for(var i=0;i<fifteen_block.length;i++){
        $('div[event_id='+fifteen_block[i]+']').find('.dhx_body').css("height","3px");
        $('div[event_id='+fifteen_block[i]+']').css({"height":"30px"});
    }
}


function ready_data(){
    var key;
    for(key in employees){
        employee_section.push({
            key : employees[key].id,
            label : employees[key].first_name+' '
        });
    }
    for(key in bookings){
        var obj = bookings[key];

        var last_name = obj['customer_detail'].last_name;
        if (last_name === undefined || last_name === null) {
            last_name = '';
        }
        var booking_data = {
            created_date    : obj['created_date'],
            customer_id     : obj['customer_id'],
            customer_name   : obj['customer_detail'].first_name + ' ' + last_name,
            customer        : obj['customer_detail'],
            code            : obj['code'],
            booking_id      : obj['id'],
            status          : obj['status'],
            type            : obj['type'],
            comment         : obj['comment'],
            reserve_comment : obj['reserve_comment'],
            is_old          : 1,
            recurring_type  : obj['recurring_type'],
            no_preference   : obj['no_preference'],
            bill_code       : obj['bill_code']
        };

        if(isset(obj['services'])){
            $.each(obj['services'],function(){
                var each_booking_data = $.extend(true,{},booking_data);
                //var staff_name = obj['employee_detail'].first_name + '<br>';
                var staff_name = '';
                var bill_code = '';
                var is_new = '';
                var room = ''
                if(obj['is_new'] == 1 && obj['status'] == 2) is_new = '<b style="color: red;">NEW</b><br>';
                if(obj['no_preference'] == 0) staff_name = '<b>Request</b><br>';
                if(obj['bill_code'] != '' && obj['bill_code'] != undefined)
                    bill_code = '<br><b>'+obj['bill_code']+'</b>';
                else
                    bill_code = '';
                if(obj['room_name']) room = obj['room_name'];
                if(obj['is_bypass'] == 1) room += '<b style="color: red;">*</b>';
                each_booking_data = $.extend(true,each_booking_data,{
                    start_date      : this['start_time'],
                    end_date        : this['end_time'],
                    employee        : this['employee_detail'],
                    room            : this['room_detail'],
                    service         : this['service_detail'],
                    employee_id     : this['employee_detail'].id,
                    employee_name   : this['employee_detail'].first_name + ' ' + this['employee_detail'].last_name,
                    service_name    : this['service_detail'].name,
                    room_id         : this['room_id'],
                    room_name       : this['room_detail'].name,
                    text            : is_new + staff_name + room + '<br>' + obj['customer_detail'].first_name + ' ' + last_name + '<br>' + obj['customer_detail'].mobile_number + '<br>' + obj['service_detail'].name + '<br>' +obj['comment'] + bill_code,
                    bundle_id       : obj['bundle_id'],
                    section_id      : this['employee_detail'].id,
                    no_preference   : this['no_preference'],
                    bill_code       : obj['bill_code']
                });
                data.push(each_booking_data);
            });
        }else{
            var each_booking_data = $.extend(true,{},booking_data);
            //var staff_name = obj['employee_detail'].first_name + '<br>';
            var staff_name = '';
            var bill_code = '';
            var is_new = '';
            var room = '';

            if(obj['is_new'] == 1 && obj['status'] == 2) is_new = '<b style="color: red;">NEW</b><br>';
            if(obj['no_preference'] == 0) staff_name = '<b>Request</b><br>';
            if(obj['bill_code'] != '' && obj['bill_code'] != undefined)
                bill_code = '<br><b>'+obj['bill_code']+'</b>';
            else
                bill_code = '';
            if(obj['room_name']) room = obj['room_name'];
            if(obj['is_bypass'] == 1) room += '<b style="color: red;">*</b>';
            each_booking_data = $.extend(each_booking_data,{
                start_date      : obj['start_time'],
                end_date        : obj['end_time'],
                employee        : obj['employee_detail'],
                room            : obj['room_detail'],
                room_id         : obj['room_id'],
                service         : obj['service_detail'],
                employee_id     : obj['employee_detail'].id,
                employee_name   : obj['employee_detail'].first_name + ' ' + obj['employee_detail'].last_name,
                service_name    : obj['service_detail'].name,
                room_name       : obj['room_detail'].name,
                text            : is_new + staff_name + room +'<br>'+ obj['customer_detail'].first_name + ' ' + last_name + '<br>' + obj['customer_detail'].mobile_number + '<br>' + obj['service_detail'].name + '<br>' +(obj['comment']==''?'':obj['comment'])+ bill_code,
                bundle_id       : 0,
                section_id      : obj['employee_detail'].id,
                bill_code       : obj['bill_code'],
                is_bypass       : obj['is_bypass']
            });
            data.push(each_booking_data);
        }
    }
}

function show_minical(){
    if (scheduler.isCalendarVisible())
        scheduler.destroyCalendar();
    else{
        scheduler.renderCalendar({
            position:"dhx_minical_icon",
            date:scheduler._date,
            navigation:true,
            handler:function(date,calendar){
                var date_new = moment(date).format('YYYY-MM-DD');
                 var url = $.generateUrl(current_url,
                 {
                 date : date_new,
                 is_full_screen : is_full_screen?"1":'0',
                 dp_mode: dp_mode?"1":"0"
                 });
                 window.location.href = url;
                scheduler.destroyCalendar()
            }
        });
    }
}

function check_recurring_booking(){
    var recurring_type = parseInt($('#repeat_type').val());
    var return_data;
    var radio_repeat = parseInt($('input[name="radio_repeat"]:checked').val());
    if(recurring_type == 0){
        return '';
    }
    else if(recurring_type == 1){
        return_data = {
            recurring_type : recurring_type,
            repeat_time  : $('#daily_option').val(),
            repeat_type  : radio_repeat,
            until_date   : radio_repeat==1 ? $('#ultil-date').val() : '',
            number_times : radio_repeat==2 ? $('#number-of-times').val() : ''
        };
    }
    else if(recurring_type == 2){
        var list_dow = new Array();
        $('.dow_select').each(function(i,item){
            if(item.checked){
                list_dow.push($(item).val());
            }
        });
        return_data = {
            recurring_type : recurring_type,
            repeat_time  : $('#week_option').val(),
            repeat_type  : radio_repeat,
            until_date   : radio_repeat==1 ? $('#ultil-date').val() : '',
            number_times : radio_repeat==2 ? $('#number-of-times').val() : '',
            repeat_on    : list_dow
        };
    }
    else if(recurring_type == 3){
        return_data = {
            recurring_type : recurring_type,
            repeat_time  : $('#monthly_option').val(),
            repeat_type  : radio_repeat,
            until_date   : radio_repeat==1 ? $('#ultil-date').val() : '',
            number_times : radio_repeat==2 ? $('#number-of-times').val() : '',
            repeat_on    : parseInt($('input[name="radio_month_repeat"]:checked').val())
        };
    }
    else if(recurring_type == 4){
        return_data = '';
    }
    return return_data;
}

var popUpFormReady = function(ev){
    try{
        check_collision({event:ev});
    }
    catch(msg){
        $.msgBox({
            type : 'error',
            title: 'message',
            content: msg,
            afterClose: function(){
                refresh_scheduler();
            }
        });
        return false;
    }
    if(ev.status == 5 || ev.status == 6 || ev.status == 7){
        var _date = {
            'start_time' : moment(ev.start_date).format('YYYY-MM-DD HH:mm:ss'),
            'end_time'   : moment(ev.end_date).format('YYYY-MM-DD HH:mm:ss')
        };
        create_away_form(ev.section_id,_date,ev.status,ev.text,'edit',ev.booking_id);
    }
    else{
        ev.text = ev.text.replace(/<br\s*\/?>/mg,"\n");
        $('').system_dialog({
            url     : url + 'staff/scheduler/getForm/' + ev.booking_id,
            async: false,
            param   : {
                employee_id : ev.section_id,
                start_time : standard_date_time_string(ev.start_date),
                end_time: standard_date_time_string(ev.end_date),
                event_id : ev.id,
                event_status: ev.status,
                comment: ev.text,
                booking_id: ev.booking_id,
                room_id: (ev.room ? ev.room.id : '')
            },
            type    : 'dialog'
        });

        $('#customer_id_typeahead').render_autocomplete({
            source: url + 'customers/suggest?all=true&extend=true',
            select: function (event, ui) {
                var str = '<div>'+ui.item.label+'</div>';
                $(this).val($(str).text());
                var id = ui.item.value;
                set_customer(id);
                return false;
            }
        }).focus(function(){
            $(this).autocomplete('search', $(this).val());
        });

        var already = JSON.parse($('#customer_id_typeahead').attr('data-value'));
        if(already != ''){
            $('#customer_id_typeahead').val(already.label);
            $('#customer_id').val(already.value);
        }

         $('#category_id').change(function(){
            var category_id = $(this).val();
            if(isset(category_id)){
                $.system_process({
                    url : url + 'admin/items/suggestByCategory/'+category_id+'/'+ev.section_id,
                    param: {minimal : true},
                    success:function(ret){
                        $('#service_id').html('');
                        var select = false;
                        $.each(ret['data'],function(key,obj){
                            if(ev.hasOwnProperty("service") && (obj.value == ev.service.id)){
                                select = true;
                            }
                            else{
                                select = false;
                            }
                            $('#service_id').append($('<option>',{
                                text  : obj.label,
                                value : obj.value,
                                selected: select
                            }));
                        });
                        $('#service_id').prepend($('<option>',{
                            text  : 'Choose a service',
                            value : 0
                        }));
                    }
                });
            }
        });
        setTimeout(function(){
            $('#category_id').trigger('change');
            setTimeout(function(){
                if(ev.hasOwnProperty("service")){
                    $('#category_id').select2('val',ev.service.categories[0].id);
                }
                if(!ev.hasOwnProperty("room")){
                    $('#service_id').trigger('change')
                }
            },500);
        },1000);
    }
};

$.fn.set_room = function(rooms, backupRooms){
    var inputRoom = $(this);
    var value, suggestion;
    if(Object.keys(rooms).length){
        value = {value : 0, label : ''};
        suggestion = [];
        for(key in rooms){
            suggestion.push({value : key, label : rooms[key].name});
        }
        $(this).render_autocomplete({
            value : value,
            source : suggestion,
            target : $(this).attr('target'),
            local : true
        });
    }else{
        if(check_room_process){
            $.msgBox({
                title: 'Message',
                content: 'No more available Room. Are you sure you want to bypass the room checking validation and move this appointment here?',
                type: 'alert',
                buttons: [{value: "Yes"}, {value: "Cancel"}],
                success: function (res) {
                    if(res == 'Yes') {
                        value = {value : 0, label : ''};
                        suggestion = [];
                        for(var key in backupRooms){
                           suggestion.push({value : backupRooms[key].id, label : backupRooms[key].name});
                        }
                        inputRoom.render_autocomplete({
                            value : value,
                            source : suggestion,
                            target : inputRoom.attr('target'),
                            local : true
                        });
                        byPass = 1;
                    }
                }
            });

            check_room_process = false;
        }
    }
};

$.fn.set_employee = function(employees){
    var value, suggestion;
    if(Object.keys(employees).length){
        var section_id = $('#section_id').val();
        value = {value : 0, label : ''};
        suggestion = [];
        for(key in employees){
            if(key == section_id){
                value = {value : $('#section_id').val(), label : employees[key].name};
            }
            suggestion.push({value : key, label : employees[key].name});
        }
        $(this).render_autocomplete({
            value : value,
            source : suggestion,
            target : $(this).attr('target'),
            local : true
        });
    }else{
        if(check_employee_process){
            prompt_dialog('No employee available','error');
            check_employee_process = false;
        }
        value = {value : 0, label : 'No employee available for this time and service'};
        suggestion = [{value : 0, label : 'No employee available for this time and service'}];
        $(this).render_autocomplete({
            value : value,
            source : suggestion,
            target : $(this).attr('target'),
            local : true
        });
    }
};

/*------ Edit Form Handler Functions ------*/
var check_employee_process;
var check_room_process;
$(function(){
    var clone_sub_item_sample = function(data,is_multiple){
        var element                 = $('#sub_item_sample').clone();
        var employee_type_elem      = $(element).find('.employee_id_typeahead:first');
        var employee_value_elem     = $(element).find('.employee_id_value:first');
        var room_type_elem          = $(element).find('.room_id_typeahead:first');
        var room_value_elem         = $(element).find('.room_id_value:first');
        var duration_value_elem     = $(element).find('.duration:first');

        var target_id = "";
        var prefix_name = "";
        if(is_multiple){
            prefix_name = "sub_service["+data['id']+"]";
            target_id = "_"+data['id'];
        }
        employee_type_elem.attr('target','employee_id'+target_id);
        employee_value_elem.attr('id','employee_id'+target_id).attr('name',prefix_name+'[employee_id]').addClass('required');
        $(employee_type_elem).set_employee(data['list_employee']);

        room_type_elem.attr('target','room_id'+target_id);
        room_value_elem.attr('id','room_id'+target_id).attr('name',prefix_name+'[room_id]').addClass('required');
        $(room_type_elem).set_room(data['list_room'],data['service']['rooms']);
        $(element).find('.service_name').html(data['name']);
        $(duration_value_elem).attr('name',prefix_name+'[duration]').val(data['duration']);
        return element;
    };

    $('#wf_page_dialog').delegate('#service_id','change',function(){
        //check_employee_process = true;
        check_employee_process = false;
        check_room_process = true;
        if($(this).val() == 0){
            return;
        }
        var id = $('#id').val();
        if(!isset(id)) id = 0;
        check_valid_service_item($(this).val());
        $.system_process({
            url : url + 'staff/scheduler/data_for_time_change',
            param : {id : id, service_id:$(this).val(),start_time : ($('#start_time').val())},
            success: function(ret,more){
                $('#employee_room_list').html('');
                var data = isset(ret['data'])?ret['data']:false;
                if(!data){
                    return;
                }
                $('#duration_span').html(data['total_duration']);
                $('#duration').val(data['total_duration']);
                if(data['is_bundle'] == '1'){
                    $.each(data.service,function(){
                        var row = clone_sub_item_sample(this,true);
                        $('#employee_room_list').append(row);
                        row.show();
                    });
                }else{
                    var row = clone_sub_item_sample(data.service,false);
                    $('#employee_room_list').append(row);
                    row.show();
                }
            }
        });
    });

    $('#wf_page_dialog').delegate('#start_time','change',function(){
        var start_time = $(this).val();
        var service_id = $('#service_id').val();
        var id = $('#id').val();
        $.system_process({
            url : url + 'staff/scheduler/data_for_time_change',
            param : {id : id, service_id : service_id, start_time: start_time},
            success: function(ret,more){
                var data = isset(ret['data'])?ret['data']:false;
                if(!data){
                    return;
                }
                $('#duration_span').html(data['total_duration']);
                $('#duration').val(data['total_duration']);
                $('#hidden_end_time').val(moment.unix(moment($('#start_time').val()).unix() + $('#duration').val() * 60).format('YYYY-MM-DD HH:mm:ss'));
                $('#employee_room_list').html('');
                if(data['is_bundle'] == '1'){
                    $.each(data.service,function(){
                        var row = clone_sub_item_sample(this,true);
                        $('#employee_room_list').append(row);
                        row.show();
                    });
                }else{
                    var row = clone_sub_item_sample(data.service,false);
                    $('#employee_room_list').append(row);
                    row.show();
                }
            }
        });
    });
});
/*-----------------------------------------*/

var check_valid_service_item = function(id){
    $.system_process({
        url: url + 'staff/scheduler/check_valid_service_item',
        param: {service_id:id},
        success: function(result){
            try{
                if(result.data[0].status == 4){
                    $("#category_id").change();
                    throw 'Item disabled';
                }
                if(result.data[0].type != 1){
                    $("#category_id").change();
                   throw 'Invalid item service';
                }
            }
            catch(msg){
                $.msgBox({
                    type : 'error',
                    title: 'message',
                    content: msg
                });
                return false;
            }
        }
    });
};

var check_collision = function(data){
    if(isset(data['event'])){
        var ev = data['event'];
    }else{
        var ev = scheduler.getEvent(data.event_id);
        var st = to_standard_date_time(data.start_time);
        var et = moment(st);
        et = et.add(data.duration,'m').toDate();
        ev.section_id   = data.employee_id;
        ev.employee_id  = data.employee_id;
        ev.start_date   = st; ev.end_date = et;
    }
    var isCollisionWithAnotherEvent = scheduler.checkCollisionCustom(ev);
    if(!isCollisionWithAnotherEvent){
        throw 'This employee is not available for this service at this time. Please choose another time'
    }

    var isLimitViolation = scheduler.checkLimitViolation(ev);
    if(!isLimitViolation){
        throw 'This employee is not available for this service at this time. Please choose another time';
    }
    return et;
};

var before_save_event = function(param,more){
    if( ! get_insert_values()){
        return false;
    }
    var hidden_end_time = $('#hidden_end_time').val();
    var hidden_employee_id = $('#hidden_employee_id').val();
    var booking_status = $('#booking_status').val();
    var data        = $('#employee_form').serializeJSON();
    var start_time  = $('#start_time').val();
    var comment     = $('#comment').val();
    var is_old      = $('#is_old').val();
    var no_preference = ($('#no_preference').is(':checked')? 0 : 1);
    var customer_id = data['customer_id'];
    var customer_temp_id = data['customer_temp_id'];
    var service_id  = data['service_id'];
    var booking_id     = $('#id').val();
    var room_id,employee_id,duration;
    var recurring_data = check_recurring_booking();
    try{
        var check_date = "2015-01-01 "+Math.floor(store_open_hour[0])+":00:00";
        check_date = moment(check_date).format('HH:mm:ss');
        event_date = moment(start_time).format('HH:mm:ss');
        if(event_date < check_date){
            if(service_id != 0 || customer_id != 0)
                throw "Cannot make appointment at this time";
        }
        if(isset(data['sub_service'])){
            var marked_time = start_time;
            if(booking_id){
                var ev = scheduler.getEvent(more.event_id);
                var evs = scheduler.getEvents();
                $.each(evs,function(order,object){
                    if(object.booking_id == ev.booking_id && ev.id != object.id){
                        object.client_delete = 1;
                        scheduler.deleteEvent(object.id);
                    }
                });
            }
            $.each(data['sub_service'],function(key,value){
                data['sub_service'][key]['start_time'] = marked_time;
                room_id     = value['room_id'];
                employee_id = value['employee_id'];
                duration    = value['duration'];
                marked_time = check_collision({
                    event_id    : more.event_id,
                    start_time  : marked_time,
                    employee_id : employee_id,
                    duration    : duration
                });
                marked_time = moment(marked_time).format('YYYY-MM-DD HH:mm:ss');
            });
            return {field_post: {
                customer_id: customer_id, sub_service : data['sub_service'],
                comment : comment, bundle_id : service_id, booking_status:booking_status,
                is_old: is_old, booking_id : booking_id, recurring_data : recurring_data,
                hidden_employee_id: hidden_employee_id,hidden_end_time:hidden_end_time,
                no_preference: no_preference
            }};
        }
        else{
            room_id     = (isset(data['room_id']) ? data['room_id'] : $('#room_id').val());
            employee_id = data['employee_id'];
            duration    = data['duration'];
            check_collision({
                event_id    : more.event_id,
                start_time  : start_time,
                employee_id : employee_id,
                duration    : duration
            });
            return {field_post: {
                customer_id: customer_id,
                customer_temp_id: customer_temp_id,
                room_id : room_id,
                employee_id : employee_id,
                start_time: start_time,
                comment : comment,
                service_id : service_id,
                booking_status:booking_status,
                is_old: is_old,
                booking_id : booking_id,
                recurring_data : recurring_data,
                hidden_employee_id:hidden_employee_id,
                hidden_end_time:hidden_end_time,
                no_preference: no_preference,
                type : '2',
                byPass : byPass
            }};
            byPass = 0;
        }
    }catch(msg){
        $.msgBox({
            type : 'error',
            title: 'message',
            content: msg,
            afterClose: function(){
                refresh_scheduler();
            }
        });
        return false;
    }
};

var after_save_event = function(post_back,more){
    refresh_scheduler();
};

var delete_new_event = function(param,more){
    var ev = scheduler.getEvent(more['event_id']);
    if(ev.status == null || ev.status == undefined || ev.status == '') {
        if (ev && typeof ev.is_old == 'undefined' && !ev.is_old) {
            ev.client_delete = 1;
            scheduler.deleteEvent(more['event_id']);
        }
    }
    else{
        if(more['is_new'] == 1 && ev.status == 2)
            refresh_scheduler();
    }
};

var after_delete_event = function(param,more){
    //var ev = scheduler.getEvent(more['event_id']);
    //ev.client_delete = 1;
    //scheduler.deleteEvent(more['event_id']);
    refresh_scheduler();
};

var event_to_text = function(ev){
    if(ev.status == 1 || ev.status == 2 || ev.status == 8 || ev.status == 9) {
        return '<div>' + ev.service.name
            + '<br>' + 'Customer: ' + (ev.customer.full_name != ''? ev.customer.full_name: '')
            + '<br>' + 'Room: ' + (ev.room.name!=''? ev.room.name:'')
            + '<br>' + 'Comment: ' + ev.comment + '</div>';
    }
    else if(ev.status == 3){
        return ev.service.name
            + '<br>' + 'Customer: ' + (ev.customer.full_name != ''? ev.customer.full_name: '')
            + '<br>' + 'Room: ' + (ev.room.name!=''? ev.room.name:'')
            + '<br>' + 'Comment: ' + ev.comment
            + '<br>' + 'Reserve Comment: ' + ev.reserve_comment;
    }
    else{
        return (ev.comment ? ev.comment : '');
    }
};

var no_permission_message = function(msg){
    if(!isset(msg)){
        msg = "You don't have permission";
    }
    $.msgBox({
        title : 'No Permission',
        type : 'error',
        content : msg
    });
};

var create_away_form = function(employee_id, date, type, str, status, booking_id){
    str = str || "";
    status = status || "";
    booking_id = booking_id || "";
    var obj = date;
    if(status=="edit"){
        date = obj.start_time;
    }

    $.system_dialog({
        url : url + 'employees/get_absent',
        param : {id : employee_id, date : date, type : type},
        success: function(){
            var dialog = $('#wf_page_dialog');
            render_form(dialog);
            $('#type_absent').val(status);
            if(status=="edit"){
                $('#employee_id').val(employee_id);
                $('#booking_id').val(booking_id);
                dialog.find('#absent_from').datetimepicker({
                    datepicker: true,
                    timepicker: true,
                    value: obj.start_time,
                    step: 15,
                    format:'Y-m-d H:i:s'
                });
                dialog.find('#absent_to').datetimepicker({
                    datepicker: true,
                    timepicker: true,
                    value: obj.end_time,
                    step: 15,
                    format:'Y-m-d H:i:s'
                });
            }
            else{
                dialog.find('#absent_from').datetimepicker({
                    datepicker: true,
                    timepicker: true,
                    step: 15,
                    format:'Y-m-d H:i:s'
                });
                dialog.find('#absent_to').datetimepicker({
                    datepicker: true,
                    timepicker: true,
                    step: 15,
                    format:'Y-m-d H:i:s'
                });
            }

            /*var index = str.indexOf('<br>');
            str = str.substring(index+4);*/
            str = str.replace(/<br\s*\/?>/mg,"\n");
            dialog.find('#description').html(str);
            dialog.find('#type').val(type);
         //   dialog.find('#type').select2('val',type);
        }
    });
};

var full_screen = function(){
    is_full_screen = !is_full_screen;
    refresh_scheduler();
};

var change_dp_mode = function(){
    dp_mode = !dp_mode;
    refresh_scheduler();
}

var absent_add_click = function(){
    var id = $('#employee_id').val();
    var data = {
        start_time     : $('#absent_from').val(),
        end_time       : $('#absent_to').val(),
        type           : $('#absent_type_list').val(),
        description    : $('#description').val(),
        action         : $('#type_absent').val(),
        booking_id     : $('#booking_id').val(),
        recurring_data : check_recurring_booking()
    };
    var starttime = $('#absent_from').val().split(" ");
    var endtime = $('#absent_to').val().split(" ");

    if(endtime[1] <= starttime[1]){
        alert('The To time must be bigger than From time');
        return false;
    }

    if((moment(data.start_time).format('YYYY-MM-DD') < moment(data.end_time).format('YYYY-MM-DD')) && data.recurring_data != ''){
        alert("Can't create recurring here. The recurring appointment only can create when the From-To date is the same date");
        return false;
    }


    if(data.start_time && data.end_time){
        $.system_process({
            url: url + 'staff/scheduler/save_BlockAwayOffline',
            param: {id: id,data: data},
            success: function(ret, more){
                refresh_scheduler();
            }
        });
    }else{
        $.msgBox({
            title: "Message",
            type: "error",
            content: 'Missing information. Please choose date and time',
            buttons: [{value: "Cancel"}]
        });
    }

};

var delete_absent_date = function(id){
    var employee_id = $('#employee_id').val();
    $.system_process({
        url: url + 'employees/delete_absent_date',
        param: {id:id},
        success: function(result){
            refresh_scheduler();
        }
    });
};

var unblock_absent_date = function(id,element){
    $(".removeblock").remove();
    scheduler.config.check_limits = false;
    //scheduler.config.drag_resize= true;
    scheduler.deleteMarkedTimespan(id);
    $(element).parent().parent().remove();

    $.system_process({
        url: url + 'employees/get_current_absent',
        param: {id:id},
        success: function(result){
            var absent_data = result.data[0];
            var testconfig = {
                start_date: absent_data.start_time,
                end_date:   absent_data.end_time,
                text: absent_data.description,
                type: absent_data.type,
                is_old: "unblocking",
                section_id: absent_data.employee_id,
                blocked_id: absent_data.id // for example
            };

            var eventid = scheduler.addEvent(testconfig);
            $("div[event_id='"+eventid+"']").addClass('removeblock');
        }
    });
};

var edit_absent_date = function(id){
    editblock_array.push(id);
    $.system_process({
        url: url + 'employees/get_current_absent',
        param: {id:id},
        success: function(result){
            var absent_data = result.data[0];
            create_away_form(absent_data.employee_id,absent_data,1,absent_data.description,"edit",'');
        }
    });
};

var check_double_absent = function(){
    var id = $('#employee_id').val();
    var start_date = $('#absent_from').val();
    var end_date = $('#absent_to').val();
    var booking_id = $('#booking_id').val();
    
    if(start_date >= end_date){
        $.msgBox({
            type : 'error',
            title: 'message',
            content: 'Invalid Date. The To Date must be greater than From Date',
            afterClose: function(){
                refresh_scheduler();
            }
        });
        return false;
    }
    absent_add_click();
  /*  $.system_process({
        url : url + 'staff/scheduler/block_check_collision',
        param : {employee_id : id,start_time : start_date,end_time : end_date, booking_id : booking_id },
        success: function(data){
            if(data.data == 0)
                absent_add_click();
            else{
                $.msgBox({
                    type : 'error',
                    title: 'message',
                    content: 'Double block. Please choose another date and time',
                    afterClose: function(){
                        refresh_scheduler();
                    }
                });
            }
        }
    });
    */
};

$('body').delegate('#absent_whole_day','click',function(e){
    if($(this).prop('checked')){
        $('[key=checkbox-relate]').attr('disabled','disabled');
    }else{
        $('[key=checkbox-relate]').removeAttr('disabled');
    }
});

$('body').delegate('#absent_add','click',function(e){
    e.preventDefault();
    check_double_absent();
    //absent_add_click();
});

var create_branch_offline_form = function(){
    $.system_dialog({
        url : url + 'config/get_branch_offline_form',
        success: function(){
            var dialog = $('#wf_page_dialog');
            render_form(dialog);
            dialog.find('#offline_date').datetimepicker({
                datepicker: true,
                timepicker: false,
                format:'Y-m-d'
            });
            dialog.find('#offline_from, #offline_to').datetimepicker({
                datepicker: false,
                timepicker: true,
                format:'H:i'
            });
        }
    });
};

var getExportCancelledApptData = function(){
    var data = validateData();
    if(data){
        window.location = url + 'staff/scheduler/exportCancelledAppts?branchId='+data.branchId+'&startDate='+data.startDate+'&endDate='+data.endDate;
    }
    else{
        return false;
    }
}

var exportCancelledApptsForm = function(){
    $.system_dialog({
        url : url + 'staff/scheduler/getExportCancelledApptsForm',
        success: function(){
            var dialog = $('#wf_page_dialog');
            render_form(dialog);
            dialog.find('#startDate,#endDate').datetimepicker({
                datepicker: true,
                timepicker: false,
                format:'Y-m-d'
            });
        }
    });
}

var validateData = function(){
    var startDate = $('#startDate').val();
    if(startDate == '' || startDate == undefined || !startDate){
        alert('please check again the "Start Date" input');
        return false;
    }

    var endDate = $('#endDate').val();
    if(endDate == '' || endDate == undefined || !endDate){
        alert('please check again the "Start Date" input');
        return false;
    }

    if(startDate > endDate){
        alert('Start Date and End Date have a conflict about time.');
        return false;
    }

    var branchId = $('#branchId').val();
    var postData = {
        startDate : startDate,
        endDate : endDate,
        branchId : branchId
    };
    return postData;
}

var delete_branch_offline_date = function(id){
    $.system_process({
        url: url + 'config/delete_branch_offline_date',
        param: {id:id},
        success: function(result){
            refresh_scheduler();
        }
    });
};

var add_branch_offline_date = function(){
    var id = $('#branch_id').val();
    var data = {
        date : $('#offline_date').val(),
        offline_from : $('#offline_from').val(),
        offline_to   : $('#offline_to').val(),
        offline_whole_day : ($('#offline_whole_day').prop('checked')) ? 1 : 0
    };
    if(data.date && ((data.offline_from && data.offline_to) || data.offline_whole_day)){
        $.system_process({
            url: url + 'config/change_offline',
            param: {id: id,data: data},
            success: function(ret, more){
                refresh_scheduler();
            }
        });
    }else{
        $.msgBox({
            title: "Message",
            type: "error",
            content: 'Missing information. Please choose date and time',
            buttons: [{value: "Cancel"}]
        });
    }
};

var branch_offline_date_checkbox_click = function(element){
    if($(element).prop('checked')){
        $('[key=checkbox-relate]').attr('disabled','disabled');
    }else{
        $('[key=checkbox-relate]').removeAttr('disabled');
    }
};

var refresh_scheduler = function(){
    sessionStorage.setItem('request_pin',0);
    setTimeout(function(){
        var date_new = moment($('.dhx_cal_date').text().substr(0,10)).format('YYYY-MM-DD');
        window.location.href = $.generateUrl(current_url,
            {
                date : date_new,
                is_full_screen : is_full_screen?"1":'0',
                dp_mode: dp_mode?"1":"0"
            }
        )
    },500);
};

$('.dhx_cal_prev_button').click(function(){
    if(scheduler._mode == 'unit'){
        var date_new = moment($('.dhx_cal_date').text().substr(0,10)).subtract(1, 'days').format('YYYY-MM-DD');
    }
    else if(scheduler._mode == 'week'){
        var date_new = moment($('.dhx_cal_date').text().substr(0,10)).subtract(1, 'weeks').format('YYYY-MM-DD');
    }
    var url = $.generateUrl(current_url,
        {
            date : date_new,
            is_full_screen : is_full_screen?"1":'0',
            dp_mode: dp_mode?"1":"0"
        });
    window.location.href = url;
});

$('.dhx_cal_next_button').click(function(){
    if(scheduler._mode == 'unit'){
        var date_new = moment($('.dhx_cal_date').text().substr(0,10)).add(1, 'days').format('YYYY-MM-DD');
    }
    else if(scheduler._mode == 'week'){
        var date_new = moment($('.dhx_cal_date').text().substr(0,10)).add(1, 'weeks').format('YYYY-MM-DD');
    }
    var url = $.generateUrl(current_url,
        {
            date : date_new,
            is_full_screen : is_full_screen?"1":'0',
            dp_mode: dp_mode?"1":"0"
        });
    window.location.href = url;
});

$('.dhx_cal_today_button').click(function(){
    var date_new = moment().format('YYYY-MM-DD');
    var url = $.generateUrl(current_url,
        {
            date : date_new,
            is_full_screen : is_full_screen?"1":'0',
            dp_mode: dp_mode?"1":"0"
        });
    window.location.href = url;
});

$('#search-submit').on('click',function(){
    console.log($('#search-submit').attr('clicked'));
    if($('#search-submit').attr('clicked') == 'true') {
        $('#search-submit').attr('clicked','false');
        $('#search-submit').text('Search');
    }
    else {
        if(($('#search_filter').val()).length <= 5){
            alert("Please type full Mobile number or Booking code reference.");
            return false;
        }
        else {
            $.ajax({
                url: url + 'staff/scheduler/searchHistoryBooking',
                type: 'POST',
                async: false,
                data: {
                    filter: $('#search_filter').val()
                },
                success: function (res) {
                    res = $.parseJSON(res);
                    historyTable.destroy();
                    historyTable = $('#history_data').DataTable({
                        data    : res.data,
                        order   : [[ 0, "desc" ]],
                        columns : [
                            { data: "created_date" },
                            { data: "bookingCode" },
                            { data: "branch_name" },
                            { data: "customerCode" },
                            { data: "itemName" },
                            { data: "start_time" },
                            { data: "end_time" },
                            { data: "comment" },
                            { data: "reserve_comment" },
                            { data: "status" }
                        ],
                        paging        : true,
                        ordering      : true
                    });
                //    $('#search_content').html(res.data);
                    $('#search-submit').attr('clicked', 'true');
                    $('#search-submit').text('Close');
                }
            });
        }
    }
    $('#search_content').slideToggle('slow');
});

var get_updated_data = function(){
    $.ajax({
        method: "POST",
        url : url + 'staff/scheduler/get_updated_data',
        data: {marked_time : marked_time},
        async: false,
        dataType : 'json'
    }).done(function( ret ) {
        var new_bookings = ret.new_bookings;
        var list_pending = ret.list_pending;
        if(new_bookings != undefined && new_bookings.length){
            $.each(new_bookings,function(){
                var booking_data = this;
                /* Thao Tran Minh - Edited 2015-08-18 **/
                var temp_begin = booking_data.start_time.split(" ");
                var temp_end = booking_data.end_time.split(" ");
                var day_in_week = moment(temp_begin[0],'YYYY-MM-DD').toDate();

                var begin_time = temp_begin[1].split(":");
                var end_time = temp_end[1].split(":");
                var begin_slot = begin_time[0]*60+begin_time[1]*1;
                var end_slot = end_time[0]*60+end_time[1]*1;
                var no_preference = "";
                var is_new = '';
                if(booking_data.is_new == 1) is_new = '<b style="color: red;">NEW</b><br>';
                if(booking_data.no_preference == 0)
                    no_preference = "<div><strong>Request</strong></div>";
                var html_slot = "<div class='booking_slot'>"+
                                    "<div class='booking_slot_head'>"+temp_begin[1].substring(0,5)+' - '+temp_end[1].substring(0,5)+"</div>"+
                                    "<div class='booking_slot_content'>"+
                                        is_new+
                                        no_preference+
                                        "<div>"+booking_data.customer_first_name+' '+booking_data.customer_last_name+" - "+booking_data.customer_mobile_number+"</div>"+
                                        "<div>"+booking_data.comment+"</div>"+
                                    "<div>"+
                                "</div>";
                // If inside scheduler week view
                if(isset(current_employee_id)) {
                    scheduler.addMarkedTimespan({
                        days       : day_in_week,
                        zones      : [begin_slot,end_slot],
                        html       : html_slot,
                        css        :   "new_booking_style",
                        type       :  "dhx_time_block"
                    });
                }
                // If inside scheduler unit view
                else {
                    scheduler.addMarkedTimespan({
                        days       : day_in_week,
                        zones      : [begin_slot,end_slot],
                        sections   : {unit:booking_data.employee_id,week:5},
                        html       : html_slot,
                        css        :   "new_booking_style",
                        type       :  "dhx_time_block"
                    });
                }
                /* --------- End ---------- */

                // Fix for dhtmlx blocked time priority on dynamic booking
                var employee = employee_map[booking_data.employee_id];
                for(key in employee.blocked_calendar_date){
                    var blocked_date = employee.blocked_calendar_date[key];
                    if(blocked_date.length){
                        scheduler.addMarkedTimespan({
                            days       : day_in_week,
                            zones      : blocked_date,
                            type       :  "dhx_time_block"
                        });
                        scheduler.addMarkedTimespan({
                            days:  day_in_week,
                            zones: [0*60, store_open_hour[0]*60],
                            css: "open_store_time",
                            type:  "dhx_time_block" //the hardcoded value
                        });
                    }
                }
            });

            // Add new
            scheduler.setCurrentView()
        }
        if(list_pending_block.length > 0) {
            $.each(list_pending_block, function (i, it) {
                scheduler.deleteMarkedTimespan(it);
                list_pending_block = list_pending_block.slice(it);
            });
            scheduler.setCurrentView();
        }
        if(list_pending != undefined && list_pending.length){
            var from_time = 0;
            var to_time = 0;
            var block_id = 0;
            $.each(list_pending,function(i,item){
                from_time = (parseInt(moment(item.date_time).format('HH'))*60) + parseInt(moment(item.date_time).format('mm'));
                to_time = from_time + parseInt(item.duration);
                block_id = scheduler.addMarkedTimespan({
                    days       : new Date(moment(item.date_time).format('YYYY-MM-DD')),
                    zones      : [from_time,to_time],
                    sections   : {unit:item.staff_id,week:item.staff_id},
                    html       : "<div style='text-align: center;'>Pending Appointment...</div>",
                    css:   "pending_block",
                    type:  "dhx_time_block"
                });

                // Fix for dhtmlx blocked time priority on pending appointment
                var employee = employee_map[item.staff_id];
                for(key in employee.blocked_calendar_date){
                    var blocked_date = employee.blocked_calendar_date[key];
                    if(blocked_date.length){
                        scheduler.addMarkedTimespan({
                            days       : new Date(moment(item.date_time).format('YYYY-MM-DD')),
                            zones      : blocked_date,
                            type       :  "dhx_time_block"
                        });
                        scheduler.addMarkedTimespan({
                            days:  new Date(moment(item.date_time).format('YYYY-MM-DD')),
                            zones: [0*60, store_open_hour[0]*60],
                            css: "open_store_time",
                            type:  "dhx_time_block" //the hardcoded value
                        });
                    }
                }
            });
            list_pending_block.push(block_id);
            scheduler.setCurrentView();
        }

        if(is_full_screen == 0){
           $.notify({
               message: 'You have <b>'+ret.number_new_appointment+'</b> new unprocessed appointment'
           },{
               type : 'danger',
               allow_dismiss: true,
               placement: {
                   from: "top",
                   align: "right"
               },
               position: 'fixed',
               delay: 14000,
               timer: 1000,
               offset : 55
           });
        }
        else{
           $.notify({
               message: 'You have <b>'+ret.number_new_appointment+'</b> new unprocessed appointment'
           },{
               type : 'danger',
               allow_dismiss: true,
               placement: {
                   from: "top",
                   align: "right"
               },
               position: 'fixed',
               delay: 14000,
               timer: 1000
           });
        }
    });

    if(localStorage.getItem('reload') == 1){
        localStorage.setItem('reload',0);
        refresh_scheduler();
    }

};
$(document).on('keyup',function(e){
    e.stopPropagation();
});
$(document).on('keydown',function(e){
    e.stopPropagation();
});

window.onbeforeunload = function() {
    sessionStorage.setItem('scheduler_top',Math.abs($('.dhx_scale_holder').position().top));
    sessionStorage.setItem('document_top',$(document).scrollTop());
    sessionStorage.setItem('document_left',$(document).scrollLeft());
}

$(document).ready(function(){
    $(document).scrollTop(sessionStorage.getItem('document_top'));
    $(document).scrollLeft(sessionStorage.getItem('document_left'));
    $('.dhx_cal_data.dhx_resize_denied').scrollTop(sessionStorage.getItem('scheduler_top'));

    $('.dhx_cal_tab.dhx_cal_tab_standalone[name="unit_tab"]').on('click',function(){
        window.location.href = url + 'staff/scheduler?is_full_screen='+(is_full_screen?1:0)+'&dp_mode='+(dp_mode?1:0);
        //refresh_scheduler();
    });
    $('.dhx_scale_m').parent().css('position','relative');
    $('.dhx_scale_m').css({'position':'absolute','top':'-10px','right':'3px'});
    $('.dhx_scale_h').css({'position':'absolute','top':'-10px','right':'16px'});
    $('.dhx_scale_m').parent().append('<span style="position: absolute;top: 7%;right: 3px;font-size: 14px;">15</span>');
    $('.dhx_scale_m').parent().append('<span style="position: absolute;top: 31%;right: 3px;font-size: 14px;">30</span>');
    $('.dhx_scale_m').parent().append('<span style="position: absolute;top: 57%;right: 3px;font-size: 14px;">45</span>');

    $("#fullscreen_notify_content").hide();

    historyTable = $('#history_data').DataTable({
        data          : [],
        columns       : [
            { data: "created_date" },
            { data: "bookingCode" },
            { data: "branch_name"},
            { data: "customerCode" },
            { data: "itemName" },
            { data: "start_time" },
            { data: "end_time" },
            { data: "comment" },
            { data: "reserve_comment" },
            { data: "status"}
        ],
        paging        : true,
        ordering      : true
    });

    setTimeout(function(){
        if(current_url.search('employee')){
            var employeeId = current_url.split('/').reverse();
            employeeId = employeeId[0];
            if(!is_full_screen) {
                $('#employee_chosen_id').select2().select2('val', employeeId);
            }
            else{
                $('.staff_name').html(employee_map[employeeId].first_name + employee_map[employeeId].last_name);
            }
        }
    }, 1000);
});

var get_avaiable_room = function(){
    $.ajax({
        url : url + 'staff/scheduler/notify_rooms_available',
        data: "",
        type: 'POST',
        error: function() {;},
        success: function(data) {
            data = JSON.parse(data);
            $("#notify_content").html(data.data["content"]);
            $("#fullscreen_notify_content").html(data.data["content"]);

            if(is_full_screen){
                $("#fullscreen_notify_content").show();
                $("#not_available_rooms").css({"right":"-1163px","top":"14px"});
                $(".dhx_cal_navline div[name=unit_tab]").css({"left":"50px","height":"34px"});
            }
            else
                $("#notify_content").html(data.data["content"]);
        }
    });
};

var export_PDF = function(){
    scheduler.exportToPDF({
        format:'A2',
        orientation:'landscape',
        zoom: 0.9,
        header:'<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/libraries/bootstrap/css/bootstrap.min.css">'+
               '<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/libraries/dhtmlx/scheduler/dhtmlxscheduler_flat.css">'+
               '<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/css/system.css">'+
               '<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/css/awesome_font.css">'+
               '<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/css/layouts/default.css">'+
               '<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/css/menus/default.css">'+
               '<link rel="stylesheet" href="http://pos.healingtouchspa.com/assets/core/css/contents/staff/scheduler.css">'
    });
}

var set_customer = function(id){
    if(sessionStorage.getItem('request_pin_time') == null){
        sessionStorage.setItem('request_pin_time',1);
    }
    else{
        sessionStorage.setItem('request_pin_time',parseInt(sessionStorage.getItem('request_pin_time')) + 1);
    }
    if(sessionStorage.getItem('request_pin') != 1 && sessionStorage.getItem('request_pin_time') >= 1){
        $.system_process({
            url : url + 'sales/set_customer_scheduler',
            param : {customer_id : id},
            success : function(ret, more){
                var data = ret['data'];
                if(data['process_type'] == 1){
                    $('#customer_id').val(id);
                    sessionStorage.setItem('request_pin',0);
                }else {
                    $.msgBox({
                        title: "Enter Customer Mobile Number",
                        type: "confirm",
                        content: "<input id='customer_mobile_number'> </input>",
                        buttons: [{value: "Submit"}, {value: "Cancel"}],
                        success: function(result){
                            if(result == 'Submit') {
                                if ($('#customer_mobile_number').val() != '') {
                                    $.system_process({
                                        url: url + 'sales/set_customer_scheduler',
                                        param: {customer_id: id, mobile_number: $('#customer_mobile_number').val()},
                                        success: function (res) {
                                            $('#customer_id').val(id);
                                            sessionStorage.setItem('request_pin',0);
                                        },
                                        fail: function (res) {
                                            $.msgBox({
                                                type: 'error',
                                                content: res.message
                                            });
                                            $('#customer_id_typeahead').val('');
                                            $('#customer_id').val('');
                                            sessionStorage.setItem('request_pin',0);
                                        }
                                    });
                                }
                                else {
                                    alert('Please type your information');
                                    $('#customer_id_typeahead').val('');
                                    $('#customer_id').val('');
                                    sessionStorage.setItem('request_pin',0);
                                }
                            }
                            else{
                                $('#customer_id_typeahead').val('');
                                $('#customer_id').val('');
                                sessionStorage.setItem('request_pin',0);
                            }
                        }
                    });
                    $("#customer_mobile_number").focus();
                    sessionStorage.setItem('request_pin',0);
                }
            }
        });
    }
    else{
        sessionStorage.setItem('request_pin',0);
    }
};
setInterval(get_updated_data,15000);
get_avaiable_room();
setInterval(get_avaiable_room,300000); // Five minutes per time

function popup_cancellation_records(){

}