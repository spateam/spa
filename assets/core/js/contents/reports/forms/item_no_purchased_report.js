/**
 * Created by Vu Huy on 2/2/2015.
 */
$(function(){
    // $('body').delegate('#category_id','change',function(){
    //     var category_id = $(this).val();
    //     if(isset(category_id)){
    //         $.system_process({
    //             url : url + 'admin/items/suggestByCategory/1',
    //             param: {minimal : true},
    //             success:function(ret,more){
    //                 $('#item_id').html('');
    //                 $.each(ret['data'],function(key,obj){
    //                     $('#item_id').append($('<option>',{
    //                         text  : obj.label,
    //                         value : obj.value
    //                     }));
    //                 });
    //                 $('#item_id').prepend($('<option>',{
    //                     text  : 'Entire Category',
    //                     value : 0,
    //                     selected: true,
    //                 }));
    //             }
    //         });
    //     }
    // });
    var ms = $('#category_id').data('magic');
    $(ms).on('selectionchange', function(e,m){
        var category_ids = this.getValue();
        // if(isset(branch_ids)){
        //     branch_ids = isset(branch_ids)?branch_ids.getValue():[];
        // }else{
        //     branch_ids = IsJsonString($('#branch_id').val())?JSON.parse($('#branch_id').val()):[];
        // }

        $('#item_id').html('');

        if(isset(category_ids)){
            $.system_process({
                url : url + 'admin/items/suggestByCategoryList',
                param: {minimal:true,category_ids:category_ids},
                success:function(ret,more){
                    $('#item_id').html('');
                    $.each(ret['data'],function(key,obj){
                        $('#item_id').append($('<option>',{
                            text  : obj.label,
                            value : obj.value
                        }));
                    });
                    $('#item_id').prepend($('<option>',{
                        text  : 'Entire Category',
                        value : 0,
                        selected: true,
                    }));
                }
            });
        }

    });
    // setTimeout(function(){
    //     $('#category_id').data('magic').trigger('selectionchange');
    // },1000);

});
