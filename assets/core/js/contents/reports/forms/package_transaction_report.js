/**
 * Created by Vu Huy on 3/4/2015.
 */
var source = $('#customer_autocomplete').attr('data-source');
$('#customer_autocomplete').render_autocomplete({
    source: source,
    select: function (event, ui) {
        $(this).val(ui.item.label);
        $('#customer_id').val(ui.item.value);
        return false;
    }
}).focus(function(){
    $(this).autocomplete('search', $(this).val());
});