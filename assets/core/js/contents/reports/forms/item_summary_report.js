/**
 * Created by Vu Huy on 3/5/2015.
 */
/**
 * Created by Vu Huy on 2/2/2015.
 */
$(function(){
    $('body').delegate('#category_id','change',function(){
        var category_id = $(this).val();
        if(isset(category_id)){
            $.system_process({
                url : url + 'items/suggestByCategory/'+category_id,
                param: {minimal : true},
                success:function(ret,more){
                    $('#item_id').html('');
                    $.each(ret['data'],function(key,obj){
                        $('#item_id').append($('<option>',{
                            text  : obj.label,
                            value : obj.value
                        }));
                    });
                }
            });
        }
    });
    setTimeout(function(){
        $('#category_id').trigger('change');
    },500);

});
