$(document).ready(function(){

    function format ( d ) {
        var row = '';
        $.each(d,function(i,item){
            row += '<tr>'+
                    '<td>'+item.billCode+'</td>'+
                    '<td>'+item.before_trans+'</td>'+
                    '<td>'+item.trans+'</td>'+
                    '<td>'+item.after_trans+'</td>'+
                    '<td>'+item.created_date+'</td>'+
                   '</tr>';
        });
        var html = '<table class="table dataTable" cellspacing="0" width="100%" role="grid" style="width: 100%; text-align:center;">'+
                    '<thead>' +
                        '<tr role="row">' +
                            '<th>Bill Code</th>' +
                            '<th>Before Transaction</th>' +
                            '<th>Transaction</th>' +
                            '<th>After Transaction</th>' +
                            '<th>Created Date</th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>'+
                        row
                    '</tbody>'+
                '</table>';
        return html;
    }

    var tables = $('#gift-card-table').DataTable({
        ajax          : site_url,
        deferRender   : true,
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data"     :  null,
                "defaultContent": ''
            },
            { data : "code" },
            { data : "itemName" },
            { data : "customerCode"},
            { data : "customerName" },
            { data : "start_value" },
            { data : "current_value" }
        ],
        paging        : true,
        ordering      : true
    });

    $('#gift-card-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tables.row( tr );
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            tr.addClass('shown');
            $.ajax({
                url: site_url_detail,
                type: 'POST',
                data: {
                    'giftcard_code' : row.data().code
                },
                success: function(res){
                    res = JSON.parse(res);
                    if(res){
                        row.child(format(res.data)).show();
                    }
                    else{

                    }
                }
            });
        }
    });

    $('#reload-table').on('click', function(){
        tables.ajax.reload();
    });
});