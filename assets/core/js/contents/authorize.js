var control = 'authorize/';
$(function () {

    $('form[name="login_form"]').submit(function (event) {
        var username = $("#account").val();
        var password = $("#password").val();
        var branch_id = $('#branch_id').val();
        $("#messageBox").removeClass('messagebox_error');
        var runAlert = function (msg) {
            $("#messageBox").addClass('messagebox_error');
            $("#messageBox").html(msg).fadeIn().fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
        };

        if (username == "") {
            runAlert('Blank Username. Plesae Enter Username');
            $("#account").focus();
            return false;
        }
        if (password == "") {
            runAlert('Blank password. Plesae Enter Password');
            $("#password").focus();
            return false;
        }

        $.system_process({
            url : BASE_URL + 'authorize/login',
            param: {Username:username, password: password, branch_id:branch_id},
            success: function(ret,more){
                var data = ret['data'];
                if(data == 1){
                    window.location.href = BASE_URL+'home';
                } else{
                    runAlert(data);
                    return false;
                }
            }
        });

        event.preventDefault();
    });

    $('#content').delegate('#branch_group_id','change', function(){
        var branch_group_id = $(this).val();

        $.system_process({
            url : url + control + 'getBranches',
            param : {branch_group_id : branch_group_id},
            success: function(ret,more){
                var data = ret['data'];
                var html = "";
                data.forEach(function(obj){
                    html += '<option value="'+obj.id+'">'+obj.name+'</option>';
                });

                $('#branch_id').html(html);
            }
        });
    });
});