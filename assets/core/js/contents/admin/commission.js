$(function(){

});

var custom_form_render = function(){
    $('#commission_item').on("autocompleteselect", function(event, ui){
        addItemCommission(ui.item.value);
        return false;
    });
    $('#commission_search_item').on("autocompleteselect", function(event, ui){
        if($(this).val() == ''){
            $('tr[id*="commission_item_"]').show();
            $('tr[id*="commission_item_"] tr').show();
            $('tr[id*="commission_item_sample"]').hide();
        }
        addSearchItemCommission(ui.item.value,ui.item.data);
        return false;
    });
    $('#commission_search_item').on('keyup', function(){
        if($(this).val() == ''){
            $('tr[id*="commission_item_"]').show();
            $('tr[id*="commission_item_"] tr').show();
            $('tr[id*="commission_item_sample"]').hide();
        }
    });
    $('#commission_category').on("autocompleteselect", function(event, ui){
        addCategoryCommission(ui.item.value);
        return false;
    });
    $('#commission_item_list, #commission_category_list').delegate('a.delete-item', 'click', function(){
        var tr = $(this).closest('tr');
        tr.remove();
    });
    $('#commission_category_list').delegate('a.add-rule', 'click', function(){
        addCategoryCommissionRule($(this));
    });
    $('#commission_category_list').delegate('a.delete-rule', 'click', function(){
        var obj = $(this).closest('div.commission_category_level');
        obj.remove();
    });

    $('#commission_item_list').delegate('.commission_item_type_bundle','change',function(){
        var type = $(this).val();
        if(type == '3'){
            $(this).closest('tr').find('.commission_bundle_sub_item_list').addClass('hidden');
        }else{
            $(this).closest('tr').find('.commission_bundle_sub_item_list').removeClass('hidden');
        }
    });

    $('.commission_item_type_bundle').trigger('change');
};

var custom_get_insert_values = function(root){
    var commission_categories = [];
    $('#commission_category_list tr').each(function (){
        if(isset($(this).attr('id'))){
            var category_id = $($(this).attr('id').split("_")).get(-1);
            $(this).find('div.commission_category_level').each(function (){
                var from = $(this).find('.commission_category_from').first().val(),
                    to = $(this).find('.commission_category_to').first().val(),
                    value = $(this).find('.commission_category_value').first().val(),
                    type = $(this).find('.commission_category_type').first().val();
                commission_categories.push({category_id: category_id, from: from, to: to, value: value, type: type});
            });
        }
    });
    root.commission_categories = commission_categories;

    var commission_items = [];
    var is_pass = true;
    $('#commission_item_list > tr').each(function (){
        var item_type = $(this).attr('item-type');
        if(isset($(this).attr('id'))){
            if(item_type == 'bundle'){
                var item_id = $($(this).attr('id').split("_")).get(-1),
                    value = Number($(this).find('.commission_item_value').first().val()),
                    type = $(this).find('.commission_item_type_bundle').first().val(),
                    price = Number($(this).attr('item-price'));
                var sub_items = [];
                $(this).find('.commission_bundle_sub_item_list tr').each(function(){
                    var sub_item_id = $($(this).attr('id').split("_")).get(-1),
                        sub_item_value = $(this).find('.commission_bundle_sub_item_value').first().val();
                    sub_items.push({id : sub_item_id, value: Number(sub_item_value)});
                });
                if(check_sub_item_total(price,type,value,sub_items)){
                    commission_items.push({item_id: item_id, value : value, type: type, sub_items : sub_items});
                }else{
                    is_pass = false;
                }
            }else{
                var item_id = $($(this).attr('id').split("_")).get(-1),
                    value = $(this).find('.commission_item_value').first().val(),
                    type = $(this).find('.commission_item_type').first().val();
                commission_items.push({item_id: item_id, value: value, type: type});
            }
        }
    });
    if(is_pass == false){
        return is_pass;
    }
    root.commission_items = commission_items;
    return root;
};

var check_sub_item_total = function(item_price,commission_type,commission_value,sub_items){

    var sub_total = 0;
    $.each(sub_items,function(){
        sub_total += this.value;
    });

    try{
        switch(commission_type){
            case '2':
                if(sub_total != 100){
                    throw 'The total commission of sub-items does not equal 100%'
                }
                return true;
                break;
            case '1':
                var limit = commission_value;
                if(limit != sub_total){
                    throw 'The total commission of sub-items does not equal to commission of bundle item'
                }
                return true;
                break;
            default:
                return true;
        }
    }catch(msg){
        $.msgBox({
            type : 'error',
            content : msg
        });
        return false;
    }

    return false;
};

var addCategoryCommission = function(id){
    var categoryList = $('#commission_category_list');
    if(categoryList.find('#commission_category_' + id).length > 0){
        $.msgBox({
            title:"Message",
            type:"alert",
            content:'You have already added this category.',
            buttons:[{value:'cancel'}]
        });
        return;
    }
    $.system_process({
        url : url + 'admin/categories/getByID/' + id,
        success: function(ret,more){
            var result = ret['data'];
            var row = $('#commission_category_sample').clone();
            row.attr('id', 'commission_category_' + result.id);
            row.find('.commission_category_name').first().html(result.name);
            categoryList.append(row);
            row.show();
        }
    });
};

var addCategoryCommissionRule = function(obj){
    var sample = $('#commission_category_level_sample').clone();
    sample.removeAttr('id');
    sample.addClass('commission_category_level');
    obj.before(sample);
    sample.show();
};

var addSearchItemCommission = function(id, result){
    var itemList = $('#commission_item_list');
    if(itemList.find('#commission_item_' + id).length > 0){
        $('#commission_item_list tr').hide();
        $('#commission_item_'+id).show();
        $('#commission_item_'+id+' tr').show();
    }
}

var addItemCommission = function(id){
    var itemList = $('#commission_item_list');
    if(itemList.find('#commission_item_' + id).length > 0){
        $.msgBox({
            title:"Message",
            type:"alert",
            content:'You have already added this item.',
            buttons:[{value:'cancel'}]
        });
        return;
    }
    $.system_process({
        url : url + 'admin/items/getItem/' + id,
        success: function(ret,more){
            var result = ret['data'];
            if(result.type == 4){
                var row = $('#commission_bundle_item_sample').clone();
                row.attr('id','commission_item_' + result.id);
                row.find('.commission_item_name').first().html(result.name);
                var sub_item_list = row.find('.commission_bundle_sub_item_list tbody');
                if(result.bundle.sub_items){
                    $.each(result.bundle.sub_items,function(){
                        var sub_item_row = $('#commission_sub_bundle_item_sample').clone();
                        sub_item_row.attr('id','commission_bundle_sub_item_'+this.id);
                        sub_item_row.find('.commission_bundle_sub_item_name').first().html(this.name);
                        sub_item_list.append(sub_item_row);
                        sub_item_row.show();
                    });
                }
                itemList.prepend(row);
                row.show();
            }else{
                var row = $('#commission_item_sample').clone();
                row.attr('id', 'commission_item_' + result.id);
                row.find('.commission_item_name').first().html(result.name);
                itemList.prepend(row);
                row.show();
            }
        }
    });
};
