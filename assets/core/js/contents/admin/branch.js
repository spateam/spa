/**
 * Created by Vu Huy on 2/6/2015.
 */
var get_data_before_edit = function(){
    var checkPass = get_insert_values();
    if(checkPass == false){
        return false;
    }
    var id = $('#id').val();
    var branch_data = $('#wf_page_dialog form').serializeJSON();
    return {id:id,field_post:branch_data};
};

function get_form_success(){
    render_form($('#wf_page_dialog'));

    $('#wf_page_dialog input[type=checkbox]').on('click',function(){
        var target = $(this).attr('target');
        var id = $('#branch_group_id').val();
        if($(this).prop('checked')){
            $.system_process({
                url : url + 'admin/branch/get_config_inherit_value/' + target +'/' + id,
                success: function(ret,more){
                    $('#' + target).attr('old-data',$('#' + target).val());
                    $('#' + target).val(ret['data']);
                }
            });

            $('#' + target).attr('disabled','disabled');
        }else{
            $('#' + target).removeAttr('disabled','');
            $('#' + target).val($('#' + target).attr('old-data'));
        }
    });

    $('#wf_page_dialog input[type=checkbox]').each(function(){
        var target = $(this).attr('target');
        if($(this).prop('checked')){
            $('#' + target).attr('disabled','disabled');
        }else{
            $('#' + target).removeAttr('disabled','');
        }
    });

}

function get_permission_form(id,url,table_id,element){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {};
    buttons['success'] = {
        text: 'Save',
        url: url + 'edit_permissions/' + id,
        class : 'btn-primary',
        close_modal : 1,
        before: function (param, more) {
            param.permissions = [];
            param.removePermissions = [];
            $('input[name="permission-data"]').each(function (index, el) {
                if ($(this).prop('checked') && $(this).val() != "")
                    param.permissions.push($(this).val());
                else if ($(this).prop('checked') == false && $(this).val() != "") {
                    param.removePermissions.push($(this).val());
                }
            });
            return param;
        },
        success: function (ret, more) {
            get_list(1, url, table_id);
        }
    };
    buttons['cancel'] = {'text': 'Close'};

    $.system_dialog({
        url : url + 'get_permission_form/' +id ,
        more: table_data,
        button: buttons,
        success: function () {
            $('#tree').bonsai({
                checkboxes: true, // depends on jquery.qubit plugin
                createCheckboxes: true // takes values from data-name and data-value, and data-name is inherited
            }).bonsai('collapseAll');
            $('li.container[disabled]').parent().siblings('input[type=checkbox][name=permission-data]').attr('disabled', 'disabled');
            $('li.container[disabled]').each(function () {
                $(this).children("input").each(function () {
                    $(this).attr('disabled', 'disabled').attr('name', 'global');
                });
            });
        },
        close_modal : 1
    });
}

$('#wf_page_dialog').delegate('input[name="permission-data"]', 'click', function () {
    var permit_type = $(this).parent().attr('permit-type');
    if (permit_type == 'v') {
        if ($(this).prop('checked')) {

        } else {
            var siblings = $(this).parent().siblings();
            $.each(siblings, function (index, el) {
                if ($(this).children('input[name="permission-data"]').prop('checked')) {
                    $(this).children('input[name="permission-data"]').prop('checked', false);
                }
            });
        }
    }
    else {
        if ($(this).prop('checked')) {
            var siblings = $(this).parent().siblings();
            $.each(siblings, function (index, el) {
                if (!$(this).children('input[name="permission-data"]').prop('checked') && $(this).attr('permit-type') == 'v') {
                    $(this).children('input[name="permission-data"]').prop('checked', true);
                }
            });
        }
    }
});