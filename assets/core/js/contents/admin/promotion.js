$(document).ready(function(){
    $('#category_id').change(function(){
        var category_id = $(this).val();
        if(isset(category_id)){
            $.system_process({
                url : url + 'admin/items/suggestByCategory/'+category_id+'/'+ev.section_id,
                param: {minimal : true},
                success:function(ret){
                    $('#item_id').html('');
                    var select = false;
                    $.each(ret['data'],function(key,obj){
                        if(ev.hasOwnProperty("service") && (obj.value == ev.service.id)){
                            select = true;
                        }
                        else{
                            select = false;
                        }
                        $('#service_id').append($('<option>',{
                            text  : obj.label,
                            value : obj.value,
                            selected: select
                        }));
                    });
                    $('#service_id').prepend($('<option>',{
                        text  : 'Choose a service',
                        value : 0
                    }));
                }
            });
        }
    });
});