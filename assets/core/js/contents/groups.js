/**
 * Created by Vu Huy on 11/4/2014.
 */

var control = "groups/";
$(function () {
    $('#dialog').delegate('input[name="permission-data"]', 'click', function () {
        permit_type = $(this).parent().attr('permit-type');
        if (permit_type == 'v') {
            if ($(this).prop('checked')) {

            } else {
                var siblings = $(this).parent().siblings();
                $.each(siblings, function (index, el) {
                    if ($(this).children('input[name="permission-data"]').prop('checked')) {
                        $(this).children('input[name="permission-data"]').prop('checked', false);
                    }
                });
            }
        }
        else {
            if ($(this).prop('checked')) {
                var siblings = $(this).parent().siblings();
                $.each(siblings, function (index, el) {
                    if (!$(this).children('input[name="permission-data"]').prop('checked') && $(this).attr('permit-type') == 'v') {
                        $(this).children('input[name="permission-data"]').prop('checked', true);
                    }
                });
            }
        }
    });
});

function update_permission(id, url, table_id) {
    var buttons = {};
    if (right.ep) {
        buttons['success'] = {
            text: 'Save',
            url: url + 'edit_permissions',
            class : 'btn-primary',
            param: {id: id},
            before: function (param, more) {
                param.permissions = [];
                param.removePermissions = [];
                $('input[name="permission-data"]').each(function (index, el) {
                    if ($(this).prop('checked') && $(this).val() != "")
                        param.permissions.push($(this).val());
                    else if ($(this).prop('checked') == false && $(this).val() != "") {
                        param.removePermissions.push($(this).val());
                    }
                });
                return param;
            },
            success: function (ret, more) {
                get_list(1, url, table_id);
            }
        }
    }
    ;
    buttons['cancel'] = {'text': 'Close'};
    $(this).system_dialog({
        url: url + 'getPermissionsForm/' + id,
        button: buttons,
        success: function () {
            $('#tree').bonsai({
                checkboxes: true, // depends on jquery.qubit plugin
                createCheckboxes: true // takes values from data-name and data-value, and data-name is inherited
            }).bonsai('collapseAll');
            $('li.container[disabled]').parent().siblings('input[type=checkbox][name=permission-data]').attr('disabled', 'disabled');
            $('li.container[disabled]').each(function () {
                $(this).children("input").each(function () {
                    $(this).attr('disabled', 'disabled').attr('name', 'global');
                });
            });
        }
    });
    return false;
}

function update_report_permission(id, url, table_id) {
    var buttons = {};
    if (right.ep) {
        buttons['success'] = {
            text: 'Save',
            url: url + 'edit_report_permission',
            class : 'btn-primary',
            param: {id: id},
            before: function (param, more) {
                report_permission = $('#report_permission_form').serializeJSON();
                param.report_permission = report_permission.report_permission_data;
                return param;
            },
            success: function (ret, more) {
                get_list(1, url, table_id);
            }
        }
    };
    buttons['cancel'] = {'text': 'Close'};
    $(this).system_dialog({
        url: url + 'get_report_permission_form/' + id,
        button: buttons,
        success: function () {
            $('#tree').bonsai({
                checkboxes: true, // depends on jquery.qubit plugin
                createCheckboxes: true // takes values from data-name and data-value, and data-name is inherited
            }).bonsai('collapseAll');
        }
    });
    return false;
}