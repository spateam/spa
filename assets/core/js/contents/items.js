$(document).ready(function(){
    $('#wf_page_dialog').delegate('#type','change',function(){
        $("#package").addClass('hidden');
        $("#service").addClass('hidden');
        $("#bundle").addClass('hidden');
        $('#product').addClass('hidden');
        $('#credit_value').removeClass('required');
        $('#duration').removeClass('required');
        $('#commission_type').removeClass('required');
        $('#unit_cost').removeClass('required');
        switch($('#type').val()){
            case '1':
                $("#service").removeClass('hidden');
                $('#duration').addClass('required');
                $('.no_fullcash').removeClass('hidden');
                $('.show_price_in_ui').removeClass('hidden');
                $('.show_in_ui').removeClass('hidden');
                break;
            case '2':
                $("#package").removeClass('hidden');
                $('#credit_value').addClass('required');
                $('.no_fullcash').removeClass('hidden');
                $('.show_price_in_ui').removeClass('hidden');
                $('.show_in_ui').removeClass('hidden');
                break;
            case '3':
                $('#product').removeClass('hidden');
                $('#unit_cost').addClass('required');
                $('.no_fullcash').removeClass('hidden');
                $('.show_price_in_ui').removeClass('hidden');
                $('.show_in_ui').removeClass('hidden');
                break;
            case '4':
                $("#bundle").removeClass('hidden');
                $('#commission_type').addClass('required');
                $('.no_fullcash').removeClass('hidden');
                $('.show_price_in_ui').removeClass('hidden');
                $('.show_in_ui').removeClass('hidden');
                break;
            case '5':
                $('.promotion').addClass('hidden');
                $('.duration').addClass('hidden');
                $('.show_price_in_ui').addClass('hidden');
                $('.show_in_ui').addClass('hidden');
                $('.no_fullcash').addClass('hidden');
                break;
            default:
                break;
        }
    });
});

var custom_form_render = function(){
    $('#item_kit').on("autocompleteselect", function(event, ui){
        addItemToPackage(ui.item.value);
        return false;
    });
    $('#room').on("autocompleteselect", function(event, ui){
        addRoomToService(ui.item.value);
        return false;
    });
    $('#item_bundle_item').on("autocompleteselect", function(event, ui){
        addSubItemToBundle(ui.item.value);
        return false;
    });
    $('#item_kit_select').on("change",function(){
        addItemToPackage($(this).val());
    })

    $("#type").trigger('change');
};

function addItemToPackage(id){
    var data = $('#item_kit_ids').val();
    data = JSON.parse(data);
    if(data.indexOf(id) != -1){
        $.msgBox({
            title:"Message",
            type:"alert",
                content:'You have already added this item to the package.',
            buttons:[{value:'Cancel'}]
        });
        return;
    }

    $.system_process({
        url : url + control + 'getItem/' + id,
        success: function(ret,more){
            var result = ret['data'];
            var html = "";
            html += '<td> <a onclick="return deleteItemKitRow(this, '+result.id+');"> <i class="fa fa-trash-o fa fa-2x text-error"></i> </a></td>';
            html += '<td class="modelname">'+result.name+'</td>';
            html += '<td class="modelname">'+result.price+'</td>';
            var row = document.createElement('tr');
            $('#grid_item').append(row);
            $(row).html(html);
            data.push(id);
            $('#item_kit_ids').val(JSON.stringify(data));
            $('#item_kit').val('');
        },
        close_modal : false
    });
}


function deleteItemKitRow(element,id){
    id = id.toString();

    $(element).parent().parent().remove();
    data = JSON.parse($('#item_kit_ids').val());

    var i = data.indexOf(id);
    if(i!=-1){
        data.splice(i,1);
    }
    $('#item_kit_ids').val(JSON.stringify(data));
    $('#category_add').select2('val',0);
}

function addRoomToService(id){
    var data = $('#room_ids').val();
    data = JSON.parse(data);
    if(data.indexOf(id) != -1){
        $.msgBox({
            title:"Message",
            type:"alert",
            content:'You have already added this room to the service.',
            buttons:[{value:'Cancel'}]
        });
        return;
    }

    $.system_process({
        url : url + 'staff/rooms/getItem/' + id,
        success: function(ret,more){
            var result = ret['data'];
            var html = "";
            html += '<td> <a onclick="return deleteRoomService(this, '+result.id+');"> <i class="fa fa-trash-o fa fa-2x text-error"></i> </a></td>';
            html += '<td class="modelname">'+result.name+'</td>';
            html += '<td class="modelname">'+result.code+'</td>';
            var row = document.createElement('tr');
            $('#grid_item_service').append(row);
            $(row).html(html);
            data.push(id);
            $('#room_ids').val(JSON.stringify(data));
            $('#room').val('');
        },
        close_modal: false
    });
}

function deleteRoomService(element,id){
    id = id.toString();

    $(element).parent().parent().remove();
    data = JSON.parse($('#room_ids').val());

    var i = data.indexOf(id);
    if(i!=-1){
        data.splice(i,1);
    }
    $('#room_ids').val(JSON.stringify(data));
}

function addSubItemToBundle(id){
    var data = $('#item_bundle_item_ids').val();
    data = JSON.parse(data);
    if(data.indexOf(id) != -1){
        $.msgBox({
            title:"Message",
            type:"alert",
            content:'You have already added this item to the bundle.',
            buttons:[{value:'Cancel'}]
        });
        return;
    }

    $.system_process({
        url : url + control + 'getItem/' + id,
        success: function(ret,more){
            var result = ret['data'];
            var html = "";
            html += '<td> <a onclick="return deleteBundleSubItem(this, '+result.id+');"> <i class="fa fa-trash-o fa fa-2x text-error"></i> </a></td>';
            html += '<td class="modelname">'+result.name+'</td>';
            html += '<td class="modelname">'+result.price+'</td>';
            var row = document.createElement('tr');
            $('#grid_item_bundle').append(row);
            $(row).html(html);
            data.push(id);
            $('#item_bundle_item_ids').val(JSON.stringify(data));
            $('#item_bundle_item').val('');
        },
        close_modal: false
    });
}

function deleteBundleSubItem(element,id){
    id = id.toString();

    $(element).parent().parent().remove();
    data = JSON.parse($('#item_bundle_item_ids').val());

    var i = data.indexOf(id);
    if(i!=-1){
        data.splice(i,1);
    }
    $('#item_bundle_item_ids').val(JSON.stringify(data));
}


function disable_item(id,url,table_id, element){

    var table_data = {
        table_id : table_id,
        url : url
    };
    $.system_process({
        url : url + 'disable_item/' + id,
        success: after_success,
        more: table_data
    });
}

function enable_item(id,url,table_id, element){
    var table_data = {
        table_id : table_id,
        url : url
    };
    $.system_process({
        url : url + 'enable_item/' + id,
        success: after_success,
        more: table_data
    });
}


$('body').delegate('#category_add','change',function(){
    var category_id = $(this).val();
    var current_data = $('#item_kit_ids').val();
    current_data = JSON.parse(current_data);
    if(isset(category_id)){
        $.system_process({
            url : url + 'admin/items/suggestByCategory/'+category_id,
            param: {minimal : true},
            success:function(ret,more){
                $('#item_id').html('');
                $.each(ret['data'],function(key,obj){
                    if ($.inArray(obj.value, current_data) != -1){}
                    else{
                        $('#item_id').append($('<option>',{
                            text  : obj.label,
                            value : obj.value
                        }));
                    }
                });
                /*$('#item_id').prepend($('<option>',{
                    text  : 'Entire Category',
                    value : 0,
                    selected: true,
                }));*/
            }
        });
    }
});