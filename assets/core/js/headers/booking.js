/**
 * Created by Vu Huy on 12/11/2014.
 */

$(function (){
    $('#login_form').submit(function (e) {
        e.preventDefault();
        var $this = $(e.currentTarget),
            inputs = {};
        // Send all form's inputs
        var check = true;
        $.each($this.find('input'), function (i, item) {
            var $item = $(item);
            if($item.val() == ""){
                var html = 'Please fill '+ $item.attr('name');
                $('#login_form .login-warning').html(html).show();
                check = false;
                return false;
            }
            else{
                inputs[$item.attr('name')] = $item.val();
            }
        });

        // Send form into ajax
        if(check == true){
            $.system_process({
                url : $this.attr('action'),
                param: inputs,
                success: function(ret,more){
                    if(ret.data.valid_email != null || ret.data.valid_email != undefined){
                        location.href = ret.data.valid_email;
                    }
                    else{
                        location.href = url + 'booking/home';
                    }
                },
                fail: function(ret,more){
                    $('#login_form .login-warning').html("Username or password is incorrect").show();
                }
            });
        }
    });

    $('#update_submit_btn').click(function (e) {
        e.preventDefault();
        var $this = $(e.currentTarget),
            inputs = {};
        // Send all form's inputs
        var form_password_current = $('#form_password_current').val();
        var form_password_first = $('#form_password_first').val();
        var form_password_second = $('#form_password_second').val();
        var form_uid = $('#form_uid').val();
        if(form_password_first != form_password_second){
            $.msgBox({
                title:"Message",
                type:"alert",
                content:"New Password and Confirm New Password is not the same",
                buttons:[{value:"Cancel"}]
            });
            return false;
        }
        else
        {
            // Send form into ajax
            $('#gento_loading').slideDown(100);

            $.system_process({
                url : url + 'booking/authorize/update_password/'+form_uid+'_'+form_password_current,
                param : { old_password : form_password_current,
                        new_password : form_password_first },
                success: function (res) {
                    setTimeout(function () {
                        window.location.href = url + 'booking/book';
                    }, 1900);
                }
            });
        }
    });
});

function getRegistrationForm(){
    var html = '<div class="container-fluid"><p>Please enter your email for existing customers:</p>' +
               '<input id="check_register_email" type="email"ustomer class="form-control input required" style="with:50%" /><br>'+
               '<input type="checkbox" name="agreement" id="agreement_reg" class="policy_checkbox">'+
               'I have read and accepted your spa policy (<a target="_blank" style="text-decoration: none;" href="http://www.healingtouchspa.com/policy">http://www.healingtouchspa.com/policy</a>)<br>'+
               '<span class="error_return" style="color:red;"></span>' +
               '<br><p><b>New customer clicks <a id="loadRegisterForm" href="#">HERE</a> to register a new account</b></p>'+
               '<a id="help_button" href="http://www.healingtouchspa.com/frequently-asked-questions/" target="_blank" class="btn pull-left green-color" >Help</a> '+
               '<button id="check_valid_email" class="btn pull-right green-color" type="button">Continue</button></div>';
    bootbox.dialog({
        title: 'Registration Requirement',
        message: html,
        onEscape: function() {},backdrop: true
    });

    $('#check_valid_email').on('click',function() {
        if (!$('#agreement_reg').is(':checked')) {
            $('.error_return').text('Please acknowledge that you have read and accepted our spa policy.');
        }
        else {
            $.ajax({
                url: checking_email_url,
                type: 'POST',
                async: false,
                data: {
                    'email': $('#check_register_email').val()
                },
                success: function (res) {
                    res = $.parseJSON(res);
                    if (!res.status) {
                        $('.error_return').text(res.message);
                    }
                    else {
                        $('.error_return').text('');
                        bootbox.hideAll();
                        bootbox.dialog({
                            title: 'Result',
                            message: res.message,
                            onEscape: function () {
                            }, backdrop: true
                        })
                    }
                }
            });
        }
    });
}

function register(){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var intRegex = /^[0-9]+$/;
    var reg_name=$('#reg_name').val();
    var email_res=$('#email_res').val();
    var password=$('#password').val();
    var re_password=$('#re_password').val();
    var mobilenum=$('#mobilenum').val();
    var recovery_email=$('#recovery_email').val();
    var policy = $('#agreement_reg').is(':checked');
    var callingCountryCode = $('#callingCountryCode').val();
    
    if(reg_name == ""){
        bootbox.dialog({
            title : 'Notice',
            message: 'Please enter your name',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(re.test(email_res)==false){
        bootbox.dialog({
            title : 'Notice',
            message: 'Your email is invalid',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(password != re_password){
        bootbox.dialog({
            title : 'Notice',
            message: 'Confirm password is not match',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(password == ""){
        bootbox.dialog({
            title : 'Notice',
            message: 'The password must not be empty',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(callingCountryCode == ''){
        bootbox.dialog({
            title : 'Notice',
            message: 'Please input your calling country code',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(mobilenum == ""){
        bootbox.dialog({
            title : 'Notice',
            message: 'Please input your mobile number',
            onEscape: function () {}, backdrop: true
        });
    }
    else if((mobilenum.length < 8) || (mobilenum.length > 15) || (intRegex.test(mobilenum) == false)){
        bootbox.dialog({
            title : 'Notice',
            message: 'Invalid mobile number',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(recovery_email == email_res){
        bootbox.dialog({
            title : 'Notice',
            message: 'Your recovery email must not be the same as your login email',
            onEscape: function () {}, backdrop: true
        });
    }
    else if(!policy){
        bootbox.dialog({
            title : 'Notice',
            message: 'Please acknowledge that you have read and accepted our spa policy before registering.',
            onEscape: function () {}, backdrop: true
        });
    }
    else {
        $.ajax({
            url : url + 'booking/authorize/register',
            data: {
                reg_name:reg_name,
                email_res:email_res,
                password:password,
                country_calling_code: callingCountryCode,
                mobilenum:mobilenum,
                recovery_email:recovery_email
            },
            success: function(res){
                res = $.parseJSON(res);
                if(res.status) {
                    bootbox.hideAll();
                    bootbox.dialog({
                        title: 'Notice',
                        message: res.message+'<br>Website will auto redirect after 3 seconds.'
                    });
                    setInterval(function(){
                        window.location.href = url + 'booking/book';
                    }, 3000);
                }
                else{
                    bootbox.dialog({
                        title: 'Error',
                        message: res.message,
                        onEscape: function () {}, backdrop: true
                    })
                }
            }
        });
    }
}

$('body').delegate('#loadRegisterForm','click',function(event){
    bootbox.hideAll();
    $.ajax({
        url: registerForm,
        success: function(res){
            res = $.parseJSON(res);
            var registerBootbox = bootbox.dialog({
                title : 'Register Form',
                message: res.data.html,
                buttons: {
                    'Help' : {
                        className: "btn-success pull-left",
                        callback: function(){
                            window.open('http://www.healingtouchspa.com/frequently-asked-questions/');
                            return false;
                        }
                    },
                    'Back' : {
                        className: "btn-success",
                        callback: function(){
                            bootbox.hideAll();
                            getRegistrationForm();
                            return false;
                        }
                    },
                    'Register' : {
                        className: "btn-success",
                        callback: function(){
                            register();
                            return false;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-cancel',
                        callback: function(){
                            bootbox.hideAll();
                        }
                    }
                }
            });
        }
    });
});

/*$("#info_update_form a").hide();
$("#logout_menu_item").show();*/

function firsttime_login(){
    if ($('input#agreement').is(':checked')){
        $("#survey_form").submit();
    }
    else{
        $.msgBox({
            title: "Message",
            type: "error",
            content: 'Please check I have read the spa policy',
            buttons: [{value: "Cancel"}]
        });
        return false;
    }
}

$('.dropdown-menu').click(function (e) {
    e.stopPropagation();
});

$('#verify_new_pwd').on('click',function(){
    $.ajax({
        url : valid_new_pwd,
        type: 'POST',
        data: {
            'cop' : $('#cop').val(),
            'pwd1' : $('#pwd1').val(),
            'pwd2' : $('#pwd2').val()
        },
        success: function(res){
            res = $.parseJSON(res);
            if(res.status){
                // do something
                location.href = res.data.valid_email;
            }
            else{
                bootbox.dialog({
                    title: 'ERROR',
                    message: res.message,
                    onEscape: function () {}, backdrop: true
                });
            }
        }
    })
});

$('#verify_mobile_number').on('click',function(){
    var cop = $('#cop').val();
    $.ajax({
        url: valid_mobile_number,
        data: {
            'cop' : cop,
            'mobile_number' : $('#mobile_field').val()
        },
        success: function(res){
            res = $.parseJSON(res);
            if(res.status == 'S'){
                onVerifySuccessful();
            }
            else{
                if(res.data.is_check_previous != null && res.data.is_check_previous != undefined){
                    bootbox.confirm({
                        title: 'Notice',
                        message: res.message+'<br><br><span id="err_message" style="color:red;"></span><br><div><table><tr><td>Previous Mobile No   </td><td><input class="form-control" type="number" id="is_previous_no" /></td></tr><tr style="height: 40px"><td>Current Mobile No </td><td>'+$('#mobile_field').val()+'</td></tr></table></div>',
                        callback: function(result){
                            if(result){
                                if(!$('#is_previous_no').val()){
                                    $('#err_message').html('Previous Mobile No should not be blank.');
                                    return false;
                                }
                                else {
                                    $.ajax({
                                        url: valid_previous_number,
                                        data: {
                                            'cop': cop,
                                            'previous_number': $('#is_previous_no').val()
                                        },
                                        success: function (res) {
                                            res = $.parseJSON(res);
                                            if (res.status == 'S') {
                                                var previous = $('#is_previous_no').val();
                                                var current = $('#mobile_field').val();
                                                bootbox.hideAll();
                                                bootbox.confirm({
                                                    title: 'Confirm',
                                                    message: '<p>You have successfully verified your previous mobile number, please select one mobile number from the listing to become your primary contact.</p>' +
                                                    '<input type="radio" name="selected_mobile_number" value="' + previous + '">' + previous + '<br>' +
                                                    '<input type="radio" name="selected_mobile_number" value="' + current + '">' + current + '<br>',
                                                    callback: function () {
                                                        var selectedNumber = $('input[type="radio"][name="selected_mobile_number"]:checked').val();
                                                        if (selectedNumber) {
                                                            $.ajax({
                                                                url: finish_verify_mobile_number,
                                                                data: {
                                                                    'cop': cop,
                                                                    'selected_number': selectedNumber
                                                                },
                                                                success: function (res) {
                                                                    onVerifySuccessful();
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                            else {
                                                bootbox.alert({
                                                    title: 'Notice',
                                                    message: '<p>The mobile number you have provided is incorrect, please contact us or visit one of our branches for more info.</p>'
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
                else{
                    bootbox.dialog({
                        title: 'Notice',
                        message: res.message,
                        onEscape: function(){}, backdrop:true
                    });
                }
            }
        }
    });
});

function onVerifySuccessful(){
    bootbox.dialog({
        title: 'Notice',
        message: 'Your mobile was verify successfuly.',
        onEscape: function(){}, backdrop:true
    });
    setInterval(function(){
        location.href = url + 'booking/book';
    }, 2000);
}
