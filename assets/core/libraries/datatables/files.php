<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 1/6/2015
 * Time: 2:42 PM
 */

$libCssFiles = ['media/css/jquery.dataTables.min'];
$libJsFiles  = [
    'media/js/jquery.dataTables.min',
    'media/js/dataTables.bootstrap.min'
];