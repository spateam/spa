/**
 * Created by Vu Huy on 11/29/2014.
 * params:
 *  data : object
 *   {
 *      page : object {
 *          current_page
 *          total_page
 *          item_per_page
 *      },
 *      total_rows
 *      dataset : array
 *   }
 */

$.fn.transform_datatable = function(command,data,option) {
    /* --- Data Functions Prototype ---- */
    $.fn.create_datatable_row = function (data_row,headers) {
        $.fn.transform_format = function(data_text){
            var data_format = $(this).get_datacell_config('data-format',option);
            if(isset(data_format)){
                var str = dataset_fire_event(data_format,data_text,data_row);
                if(isset(str)){
                    data_text = str;
                }
            }
            return data_text
        };

        $.fn.create_text_cell = function (data_row) {
            var data_column = data_row[$(this).get_datacell_config('data-column',option)];
            data_column = $(this).transform_format(data_column);

            var td_cell = $('<td>', {html :data_column!=""&&data_column!=null?data_column:" "});
            return(td_cell);
        };

        $.fn.create_text_link_cell = function(data_row){
            var data_function           = $(this).get_datacell_config('data-function',option);
            var data_limited_function   = $(this).get_datacell_config('data-function-limited',option);
            var data_params             = $(this).get_datacell_config('data-function-params',option);
            var data_text               = data_row[$(this).get_datacell_config('data-column',option)];
            var data_function_allow_key = $(this).get_datacell_config('data-function-allow-key',option);
            /*---- Transform format -----*/
            var data_text = $(this).transform_format(data_text);

            if(! isset(data_function_allow_key) || (isset(data_function_allow_key) && data_row[data_function_allow_key])){
                var function_html = "";
                if(row_limited && data_limited_function){
                    function_html = data_limited_function  + '(';
                }else{
                    function_html = data_function + '(';
                }
                data_params.split(',').forEach(function(obj){
                    function_html += data_row[obj] + ',';
                });
                function_html = function_html.substr(0,function_html.length-1) + ',"'+url+'","'+table_id+'", this)';
                if(typeof data_text == 'undefined')
                    data_text = data_row[$(this).attr('data-column')];
                var td_cell = $('<td>',
                    {
                    }).append($('<a>', {
                        html    : data_text && data_text.length>0?data_text:" ",
                        onclick : function_html
                    }));
                return(td_cell);
            }
            return $(this).create_text_cell(data_row);
        };

        $.fn.create_checkbox_cell = function(data_row){
            var data_column = data_row[$(this).attr('data-column')];
            var td_cell = $('<td>',
                {
                }).append($('<input>', {
                    type  : 'checkbox',
                    name  : "keys[]",
                    class : 'chkbox',
                    value : data_row[data_key]
                }));
            return(td_cell);

        };

        $.fn.create_auto_increment_cell = function(){
            var td_cell = $('<td>', {text :start_row_id++});
            return(td_cell);
        };

        $.fn.create_buttons_cell = function(data_row){
            var data_class              = $(this).get_datacell_config('data-class',option);
            var data_function           = $(this).get_datacell_config('data-function',option);
            var data_function_params    = $(this).get_datacell_config('data-function-params',option);
            var data_allow_key          = $(this).get_datacell_config('data-allow-key',option);
            var data_align              = $(this).get_datacell_config('data-align',option);
            var data_title              = $(this).get_datacell_config('data-title',option)?$(this).get_datacell_config('data-title',option):[];
            var data_tag                = $(this).get_datacell_config('data-tag',option)?$(this).get_datacell_config('data-tag',option):[];
            var td_cell = $('<td>',{
                style : "text-align:" + data_align + ";"
            });
            if( Object.prototype.toString.call( data_class ) === '[object Array]' ) {
                for(i = 0; i < data_class.length; i++){
                    var row_class           = data_class[i];
                    var row_function        = data_function[i];
                    var row_function_params = data_function_params[i];
                    var row_tag = isset(data_tag[i])?data_tag[i]:'<span>';
                    if(data_allow_key){
                        var row_allow_key       = data_allow_key[i];
                    }else{
                        var row_allow_key = null;
                    }
                    var function_html       = row_function + '(';
                    row_function_params.split(',').forEach(function(obj){

                        function_html += data_row[obj] + ',';
                    });

                    function_html = function_html.substr(0,function_html.length-1) + ',"'+url+'","'+table_id+'", this)';
                    if(row_allow_key && ! data_row[row_allow_key]){
                        continue;
                    }
                    var button = $('<a>', {
                            href    : '#',
                            onclick : function_html
                        }
                    ).append($('<span>', {
                            title   : (data_title[i] != null ? data_title[i]:''),
                            class   : row_class
                        }));
                    td_cell.append(button);
                }
            }
            else{

                var function_html = data_function + '(';
                data_function_params.split(',').forEach(function(obj){
                    function_html += data_row[obj] + ',';
                });
                function_html = function_html.substr(0,function_html.length-1) + ',"'+url+'","'+table_id+'")';
                if(data_allow_key && data_row[data_allow_key]){
                    var button = $('<a>', {
                            href    : '#',
                            onclick : function_html
                        }
                    ).append($('<span>', {
                            class   : data_class
                        }));
                    td_cell.append(button);
                }
            }
            return td_cell;
        };

        $.fn.create_link_cell = function(data_row){
            var url     = $(this).get_datacell_config('data-url',option);
            var text = data_row[$(this).get_datacell_config('data-column',option)];
            text = $(this).transform_format(text);
            var element_class = $(this).get_datacell_config('data-class',option)?$(this).get_datacell_config('data-class',option):'';
            if(!text){
                var text    = $(this).get_datacell_config('data-text',option);
                if(!text){
                    text = '';
                }
            }
            var new_window = $(this).get_datacell_config('data-new-window',option);
            var params = $(this).get_datacell_config('data-param',option);
            if(params){
                var params_text = "";
                params.split(',').forEach(function(obj){
                    params_text += data_row[obj] + '/';
                });
                params = params_text.substr(0,params_text.length-1);
            }else{
                params = data_row[data_key];
            }
            var config = {
                href    : url + params
            };
            config['target'] = '_blank';
            var td_cell = $('<td>',
                {
                }).append($('<a>', config).append($('<span>',{text: text,class:element_class})));
            return(td_cell);
        };

        $.fn.create_text_button_cell = function(data_row){
            var _text = $(this).get_datacell_config('data-button-text',option);
            var _else_text = $(this).get_datacell_config('data-else-button-text',option);
            var _function = $(this).get_datacell_config('data-function',option);
            var _else_function = $(this).get_datacell_config('data-else-function',option);
            var _params = $(this).get_datacell_config('data-function-params',option);
            var _data_allow_key = $(this).get_datacell_config('data-function-allow-key',option);
            var td_cell = ($('<td>'));
            if(!isset(_data_allow_key) || (isset(data_row[_data_allow_key]) && data_row[_data_allow_key])){
                var function_html = _function + '(';
                _params.split(',').forEach(function(obj){
                    function_html += data_row[obj] + ',';
                });
                function_html = function_html.substr(0,function_html.length-1) + ',"'+url+'","'+table_id+'",this)';

                td_cell = $('<td>',
                    {
                    }).append($('<a>', {
                        text    : _text,
                        onclick : function_html
                    }));
            }else if(_else_function){
                var function_html = _else_function + '(';
                _params.split(',').forEach(function(obj){
                    function_html += data_row[obj] + ',';
                });
                function_html = function_html.substr(0,function_html.length-1) + ',"'+url+'","'+table_id+'",this)';

                td_cell = $('<td>',
                    {
                    }).append($('<a>', {
                        text    : _else_text,
                        onclick : function_html
                    }));
            }
            return(td_cell);
        };
        /*------------------------------------------------------*/
        var row_limited = limited=='true'||limited==true?true:false;
        var row = $('<tr>',{});
        var i = 1;
        var count = headers.length;

        if (allow_checkbox) {
            var cell = $(this).create_checkbox_cell(data_row);
            $(row).append(cell);
        } if (auto_increment) {
            var cell = $(this).create_auto_increment_cell();
            $(row).append(cell);
        }

        count = 0;
        if(limited){
            limited_keys.forEach(function(obj){
                if(typeof data_row[obj] == 'undefined'){
                    count++;
                    return true;
                }
                if( data_row[obj] == true || data_row[obj] > 0){
                    row_limited = false;
                }
            });
            if(count==limited_keys.length)
                row_limited = false;
        }
        var stop = false;
        var cell_id = 0;
        $(headers).each(function () {
            if(stop == true){
                return;
            }
            var data_type = $(this).get_datacell_config('data-type',option);
            if(row_limited == true && Number(i) > Number(limited_start)){
                var cell =$('<td>',{
                    text : "You dont have permission on this row",
                    colspan: headers.length - limited_start,
                    style: "text-align:center"
                });
                $(row).append(cell);
                stop = true;
            }else{
                $(this).data('cell_id',cell_id);
                switch (data_type){
                    case 'text':
                        cell = $(this).create_text_cell(data_row);
                        break;
                    case 'text-link':
                        cell = $(this).create_text_link_cell(data_row);
                        break;
                    case 'buttons':
                        cell = $(this).create_buttons_cell(data_row);
                        break;
                    case 'link':
                        cell = $(this).create_link_cell(data_row);
                        break;
                    case 'text-button':
                        cell = $(this).create_text_button_cell(data_row);
                        break;
                }
                if(isset(cell)){
                    $(row).append(cell);
                }
            }
            i++;
        });

        return row;
    };

    /* --- End of Data Functions Prototype ---- */

    /* --- Initialize Table and Component ---- */
    var table           = $(this);
    var thead           = $(this).children('thead');
    var url             = $(this).get_dataset_config('data-url',option);
    var get_list_url    = join_address(url,$(this).get_dataset_config('data-list-url',option),'getList');
    var get_entity_url  = join_address(url,$(this).get_dataset_config('data-get-entity-url',option),'get');
    var table_id        = $(this).get_dataset_config('id',option);
    var hash_tag_table  = null;
    if(isset(query_params)){
        query_string = to_query_string(query_params);
        window.location.hash = query_string;
        hash_tag_table = get_hash_value('tables');
        hash_tag_table = hash_tag_table&&isset(hash_tag_table[table_id])?hash_tag_table[table_id]:null;
    }
    if(! data){
        if(hash_tag_table && isset(hash_tag_table['entity_id'])){
            $.system_process({
                url     : get_entity_url + '/' + hash_tag_table['entity_id'],
                async   : false,
                success : function(ret, more){
                    data = ret['data'];
                    $.extend(true,option,data['dataset_option']);
                }
            });
        }else{
            var page = 1;
            if(hash_tag_table && isset(hash_tag_table['page'])){
                page = hash_tag_table['page'];
            }
            $.system_process({
                url     : get_list_url + '/' + page,
                async   : false,
                success : function(ret, param){
                    data = ret['data'];
                    $.extend(true,option,data['dataset_option']);
                }
            });
        }

    }

    var allow_checkbox = $(this).get_dataset_config('data-checkbox',option,true);
    var auto_increment = $(this).get_dataset_config('data-checkbox',option,true);
    if(command != 'refresh'){
        $('[target='+table_id+']').each(function(obj){
            switch ($(this).attr('btn-type')){
                case 'new':
                    update_item(0,url,table_id, $(this));
                    break;
                case 'delete':
                    delete_dialog($(this),table_id,url);
                    break;
                case 'search':
                    var suggest_url = join_address(url, $(this).attr('data-suggest-url'),'suggest');
                    $(this).render_autocomplete({
                        source: suggest_url,
                        select: function (event, ui) {
                            getSearch(ui.item.value,get_entity_url,table_id);
                            $(this).val(ui.item.label);
                            return false;
                        }
                    });
                    break;
                case 'clear':
                    $(this).click(function(){
                        clear_search(get_list_url,table_id);
                    });
                    break;
            }
        });

        if(auto_increment){
            $(thead).children('tr').prepend($('<th>',{
                'text' : 'No.'
            }).append($('', {})));
        }
        if(allow_checkbox) {
            $(thead).children('tr').prepend($('<th>', {
                'width': 20
            }).append($('<input>', {
                'class': 'checkAll',
                'type': 'checkbox'
            })));
        }
    }


    /* -------------------------------- */
    var limited         = $(this).get_dataset_config('data-limited',option);
    var limited_start   = $(this).get_dataset_config('data-limited-start-column',option);
    var limited_keys    = $(this).get_dataset_config('data-limited-key',option);
    if((isset(limited_keys ) && typeof limited_keys != 'Array') && limited_keys.length > 0 ){
        limited_keys = limited_keys.split(',');
    }
    var tbody = $(this).children('tbody');
    var data_key = $(this).children('thead').get_dataset_config('data-key'); data_key = data_key!=null&&data_key.length>0?data_key:'id';

    var start_row_id = (data.page.current_page - 1) * data.page.item_per_page + 1;
    var headers = $(this).find('thead tr th');



    $(tbody).html('');
    if(data.dataset.length==0){
        cell = $('<tr>').append($('<td>',{
            colspan: headers.length,
            text:'There is no record',
            style:'text-align:center'
        }));
        $(tbody).append(cell);
    }else{
        data.dataset.forEach(function (obj) {
            var data_row = obj;
            var row = $(this).create_datatable_row(data_row,headers);
            $(tbody).append(row);
        });
    }

    /* Transform Paging */
    if(data.page.total_page > 0){
        $(".paging[target='"+table_id+"']").paginate({
            count 		: data.page.total_page,
            start 		: data.page.current_page,
            display     : data.page.item_per_page,
            border					: false,
            text_color  			: '#888',
            background_color    	: '#EEE',
            text_hover_color  		: 'black',
            background_hover_color	: '#CFCFCF',
            onChange     			: function(page){
                if(! isset(query_params['tables'])){
                    query_params['tables'] = {};
                }
                query_params['tables'][table_id] = {
                    page : page
                };
                get_list(page,get_list_url,table_id);
            }
        });
    }
    /* Set Total Items */
    $('#total_rows').text(data.total_rows);
};

function update_item(id,url,table_id, element){
    var table_data = {
        table_id : table_id,
        url : url
    };
    var buttons = {'success': {before:'get_data_before_edit', success:'after_success', more:table_data}};
    if(id == 0){
        $(element).data('action-url', url + 'getForm');
    }else{
        $(element).data('action-url', url + 'getForm/' + id);
    }
    $(element).data('action-button', buttons);
}


function refresh_table(data_return,table_data){
    $('#'+table_data['table_id']).transform_datatable('refresh');
}


function delete_dialog(e,table_id,url){
    var table_data = {
        table_id : table_id,
        url : url
    };
    $(e).data('action-type','confirm')
        .data('action-content','Are you sure want to delete this record?')
        .data('action-url',url+'delete')
        .data('action-before','get_data_before_delete')
        .data('action-success','refresh_table')
        .data('action-more',table_data);
}



function get_list(page,get_list_url,table_id){
    if(!page) page = "";
    if(! isset(query_params['tables'])){
        query_params['tables'] = {};
    }
    query_params['tables'][table_id] = {
        page : page
    };
    $('#'+table_id).transform_datatable('refresh');
}

function getSearch(id,get_entity_url,table_id){
    $.system_process({
        url : get_entity_url  + '/' + id,
        success : function(ret, more){
            var data = ret['data'];
            if(! isset(query_params['tables'])){
                query_params['tables'] = {};
            }
            query_params['tables'][table_id] = {
                entity_id : id
            };
            window.location.hash = to_query_string(query_params);
            $('#'+table_id).transform_datatable('refresh',data);
        }
    });
}

function before_clear_search(url,table_id){
    return true;
}

function clear_search(url,table_id){
    if(before_clear_search(url,table_id)){
        get_list(1,url,table_id);
    }

}

/**
 * Processing the dialog after loaded (for #wf_page_dialog)
 */
function get_form_success(){
    render_form($('#wf_page_dialog'));
}

/**
 * Gathering the dialog data before post them to server to add or edit
 * @returns {*}
 */

var get_data_before_edit = function(){
    var id = $('#id').val();
    var data = get_insert_values();
    data.id = id;
    return data;
};


/**
 * Gathering the delete data before post them to server to delete
 * @returns {{ids: *}}
 */
function get_data_before_delete()
{
    var ids = [];
    $("[name='keys[]']").each(function() {
        if ($(this).is(":checked"))
        {
            ids.push($(this).val());
        }
    });

    return {ids:JSON.stringify(ids)};
}


function after_success(data_return,table_data){
    if(isset(data_return.validate_error)){
        handleErrors(data_return.validate_error);
    }
    refresh_table(data_return,table_data);
}


/**
 *
 * @param key
 * @param option
 * @param $default
 * @returns {*}
 */
$.fn.get_dataset_config = function(key, option, $default){

    if(isset(key)){
        if(isset(option)){
            var jsKey = key.replace(/-/g, "_");
            if(isset(option[jsKey])){
                return option[jsKey];
            }
        }

        if(isset($(this).data(key))){
            return $(this).data(key);
        }
        if(isset($(this).attr(key))){
            return $(this).attr(key);
        }
    }

    if(isset($default)){
        return $default;
    }
    return null;
};


/**
 *
 * @param key
 * @param option
 * @param $default
 * @returns {*}
 */
$.fn.get_datacell_config = function(key, option, $default){
    if(isset(key)){
        if(isset(option)){
            var jsKey = key.replace(/-/g, "_");
            var hKey = $(this).attr('data-cell-id')?$(this).attr('data-cell-id'):$(this).attr('data-column');
            if(isset(hKey) && isset(option['headers'][hKey]) && isset(option['headers'][hKey][jsKey])){
                $(this).data(jsKey,option['headers'][hKey][jsKey]);
                return option['headers'][hKey][jsKey];
            }
        }

        if(isset($(this).data(key))){
            return $(this).data(key);
        }
        if(isset($(this).attr(key))){
            if(IsJsonString($(this).attr(key))){
                return JSON.parse($(this).attr(key));
            }
            return $(this).attr(key);
        }
    }

    if(isset($default)){
        return $default;
    }
    return null;
};


/**
 *
 * @param e
 * @param data_text
 * @param data_row
 * @returns {*}
 */
var dataset_fire_event = function(e, data_text, data_row){
    if(isset(e)){
        if(typeof e == "function"){
            return e(data_text, data_row);
        }else{
            var fn = window[e];
            if(typeof fn === 'function') {
                return fn(data_text, data_row);
            }else{
                return e;
            }
        }
    }
    return null;
};


var get_choose_branch_form = function(id, url, table_id){
    $.system_dialog({
        url : url + 'get_choose_branch_form/' + id + '/' + table_id,
        button : [{
            text : 'Cancel'
        }]
    });
};


var join_address = function(a1,a2,ad){
    var ret_add = "";
    if(isset(a2)){
        ret_add = a2.indexOf('.')?a2:a1+a2;
    }else{
        ret_add = a1 + ad;
    }
    return ret_add;
};

var to_query_string = function(obj, prefix) {
    var str = [];
    for(var p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
            str.push(typeof v == "object" ?
                to_query_string(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
};



var get_hash_value = function(key,url) {
    //if(location.hash){
    //    var result =  location.hash.match(new RegExp(key+'=([^&]*)'));
    //    console.log(result,key);
    //    if(!result) return null;
    //    if(result.length > 1) return result[1];
    //}
    //return null;

    if (!url) url = current_url + location.hash;
    return purl(url).fparam(key);
};

