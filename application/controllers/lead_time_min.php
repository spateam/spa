<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Lead_time_min extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "lead_time_min";
        $this->pageCode = "lead_time_min";
        $this->pageName = "lead_time_min";
        $this->breadcrumbs[] = array('url' => 'lead_time_min', 'text' => 'lead_time_min');
    }
}