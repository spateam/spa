<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Questionaire extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "questionaire";
        $this->pageCode = "questionaire";
        $this->pageName = "Questionaire";
        $this->breadcrumbs[] = array('url' => 'questionaire', 'text' => 'Questions');
    }
}