<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "config";
        $this->pageCode = "config";
        $this->pageName = "Config";
        $this->breadcrumbs[] = array('url' => 'config', 'text' => 'Config');
    }

    public function index(){
        if($this->user_permission->checkPermission('v',$this->pageCode) == false){
            return;
        }
        $data = $this->load->controller_model('config')->getDataForView($this->user_check->get('login_as_type'));
        $data->pageCode = $this->pageCode;
        $this->page($this->moduleName, $data);
    }

    public function save(){
        $message = 'Save Successfully';
        $type = 'success';
        try{
            $config_controller_model = $this->load->controller_model('config');
            $config_values = $this->input->post('value');
            $branch_values = $this->input->post('branch_data');
            $config_controller_model->update($config_values, $branch_values);

        }catch(Exception $e){
            $message = $e->getMessage();
            $type = 'fail';
        }
        $data = $config_controller_model->getDataForView($this->user_check->get('login_as_type'));
        $data->messageBox = new stdClass();
        $data->messageBox->type = $type;
        $data->messageBox->message = $message;

        $this->page($this->moduleName, $data);
    }

    function get_config_inherit_value($id){
        $value = $this->load->controller_model('config')->get_config_inherit_value($id);
        $this->ajax_content(REQUEST_SUCCESS,'',$value);
    }

    function get_branch_offline_form(){
        $data['offline_data']   = $this->load->controller_model('config')->get_branch_offline_data();
        $data['branch_id']      = $this->user_check->get('branch_id');
        $data['default']        = array();
        $ret = array();
        $ret['content'] = $this->content('config/offline_form',$data,true);
        $ret['option'] = array(
            'title' => "Branch Offline"
        );
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function change_offline(){
        $data       = (object) $this->input->post('data');
        $branch_id  = $this->input->post('id');
        $result = $this->load->controller_model($this->moduleName)->change_offline($branch_id,$data);
        if($result){
            return $this->ajax_content(REQUEST_SUCCESS);
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Can't save absent");
        }
    }

    function delete_branch_offline_date(){
        $id     =   $this->input->post('id');
        $this->load->controller_model($this->moduleName)->delete_offline($id);
        return $this->ajax_content(REQUEST_SUCCESS);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */