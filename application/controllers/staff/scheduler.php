<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Scheduler extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "scheduler";
        $this->pageCode = "scheduler";
        $this->pageName = "Scheduler";
        $this->breadcrumbs[] = array('url' => 'staff/scheduler', 'text' => 'Scheduler');
    }

    function index($type = "all",$id = 0){
        $this->assets->loadLibrary('dhtmlx');
        $condition = $id?array("booking_service" => array("{$type}_id" => $id)):array();
        $condition['filter_date'] = ($this->input->get('date') ? $this->input->get('date') : date('Y-m-d'));
        $dp_mode = to_b($this->input->get('dp_mode'));

        $data = $this->load->controller_model('scheduler')->getDataForCalendar($type,$condition,$dp_mode);

        if($type == 'all' || $type == 'employee'){
            $data = array_merge(array('employee_id' => $id),$data);
            $is_full_screen = to_b($this->input->get('is_full_screen'));
            $data['is_full_screen'] = $is_full_screen;

            if($is_full_screen){
                $data['pageCode'] = 'scheduler';
                $this->page($this->moduleName.'/index',$data,'raw');
            }else{
                $this->page($this->moduleName.'/index',$data);
            }

        }
    }

    function getForm($id = 0){
        $employee_id    = $this->input->post('employee_id');
        $start_time     = $this->input->post('start_time');
        $end_time       = $this->input->post('end_time');
        $event_id       = $this->input->post('event_id');
        $room_id        = $this->input->post('room_id');
        $event_status   = $this->input->post('event_status');

        if(isset($event_status) && $event_status == 4){
            $var = new stdClass();
            $var->id = $this->input->post('booking_id');
            $var->comment = $this->input->post('comment');
            $var->status = $event_status;
            $data['book'] = $var;
        }
        else{
            $modelName      = $this->load->controller_model($this->moduleName);
            $data = $modelName->getDataForViewForm($id,$employee_id,$room_id);
        }

        // Mark appointment as read
        $is_new = 0;
        if(isset($data['book']->is_new) && $data['book']->is_new == 1){
            $is_new = 1;
            $modelName->mark_as_read_appointment($id);
        }
        
        $data['employee_id'] = $employee_id;
        $data['start_time'] = $start_time;
        $data['end_time'] = $end_time;
        $data['employee'] = $this->load->table_model('employee')->getByID($employee_id);
        $data = $this->content($this->moduleName.'/edit_form',$data, true);

        $submit_url = site_url($this->dir.'/save_event');
        $buttons = array(
            'success' => array(
                'url'       => $submit_url,
                'text'      => 'Submit',
                'class'     => "btn-primary submit_edit_create_form",
                'before'    => 'before_save_event',
                'success'   => 'after_save_event',
                'close_modal' => true,
                'more'      => array('event_id' => $event_id)
            ),
            'cancel' => array(
                'text'      => 'Cancel',
                'class'     => 'lightbox-close',
                'onClick'   => 'delete_new_event',
                'more'      => array('event_id' => $event_id,'is_new' => $is_new)
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button'  => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }

    function data_for_time_change(){
        $booking_id = $this->input->post('id');
        $service_id = $this->input->post('service_id');
        if(!$service_id){
            return $this->ajax_content(REQUEST_SUCCESS,'');
        }

        $start_time = $this->input->post('start_time');
        $data = $this->load->controller_model('scheduler')->getDataForTimeChange($service_id, $start_time, $booking_id, false ,array(),'scheduler');
        $this->ajax_content(REQUEST_SUCCESS,'',$data);
    }

    function save_BlockAwayOffline($type= "employee"){
        $posts = $this->input->post();
        if(isset($posts['data']['recurring_data'])){
            $recurring = $posts['data']['recurring_data'];
            unset($posts['data']['recurring_data']);
        }
        else{
            $recurring = '';
        }

        if($this->check_employee_time_slot(array('employee_id' => $posts['id'],
                                                 'start_time' => $posts['data']['start_time'],
                                                 'hidden_end_time' => $posts['data']['end_time'],
                                                 'booking_id' => $posts['data']['booking_id']))){
            $this->ajax_content(REQUEST_FAIL,"You can't setup a block/away/offline to this employee, a conflict appointment has been found.");
            exit;
        }

        if(!isset($posts['data']['action']) || (isset($posts['data']['action']) && $posts['data']['action'] == '')){
            $this->load->controller_model('scheduler')->insert_bao_booking($posts);
        }
        if(isset($posts['data']['action']) && $posts['data']['action'] == 'edit'){
            $this->load->controller_model('scheduler')->edit_bao_booking($posts);
        }
        if(!empty($recurring) && $recurring['recurring_type'] > 0){
            if($recurring['recurring_type'] == 1){
                if($recurring['repeat_type'] == 1){
                    $temp_begin = strtotime($posts['data']['start_time'].' +'.$recurring['repeat_time'].' days');
                    $temp_end   = strtotime($posts['data']['end_time'].' +'.$recurring['repeat_time'].' days');
                    while($temp_begin <= strtotime($recurring['until_date'])){
                        $temp_begin = date('Y-m-d H:i:s',$temp_begin);
                        $temp_end   = date('Y-m-d H:i:s',$temp_end);
                        $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $temp_begin, 'end_time' => $temp_end, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'],'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                        $temp_begin = strtotime($temp_begin.' +'.$recurring['repeat_time'].' days');
                        $temp_end = strtotime($temp_end.' +'.$recurring['repeat_time'].' days');
                    }
                }
                else if($recurring['repeat_type'] == 2){
                    for($i=1;$i<intval($recurring['number_times']);$i++){
                        $temp_begin = date('Y-m-d H:i:s',strtotime($posts['data']['start_time'].' +'.$recurring['repeat_time']*$i.' days'));
                        $temp_end = date('Y-m-d H:i:s',strtotime($posts['data']['end_time'].' +'.$recurring['repeat_time']*$i.' days'));
                        $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $temp_begin, 'end_time' => $temp_end, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'],'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                    }
                }
            }
            else if($recurring['recurring_type'] == 2){
                if($recurring['repeat_type'] == 1){
                    $repeat_on = $recurring['repeat_on'];
                    foreach($repeat_on as $dow){
                        $new_start_time = '';
                        $new_end_time = '';
                        if($dow == 1)
                            $new_start_time = date('Y-m-d',strtotime('this monday'));
                        elseif($dow == 2)
                            $new_start_time = date('Y-m-d',strtotime('this tuesday'));
                        elseif($dow == 3)
                            $new_start_time = date('Y-m-d',strtotime('this wednesday'));
                        elseif($dow == 4)
                            $new_start_time = date('Y-m-d',strtotime('this thursday'));
                        elseif($dow == 5)
                            $new_start_time = date('Y-m-d',strtotime('this friday'));
                        elseif($dow == 6)
                            $new_start_time = date('Y-m-d',strtotime('this saturday'));
                        elseif($dow == 7)
                            $new_start_time = date('Y-m-d',strtotime('next sunday'));
                        if(strtotime($new_start_time) > strtotime(date('Y-m-d'))){
                            $a = $new_start_time.' '.date('H:i:s',strtotime($posts['data']['start_time']));
                            $b = $new_start_time.' '.date('H:i:s',strtotime($posts['data']['end_time']));
                            $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $a, 'end_time' => $b, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'],'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                        }
                        $c = $new_start_time;
                        if(strtotime($new_start_time) > strtotime(date('Y-m-d'))){
                            $new_start_time = $c.' '.date('H:i',strtotime($posts['data']['start_time']));
                            $new_end_time = $c.' '.date('H:i',strtotime($posts['data']['end_time']));
                            $temp_begin = strtotime($new_start_time.' +'.$recurring['repeat_time'].' weeks');
                            $temp_end = strtotime($new_end_time.' +'.$recurring['repeat_time'].' weeks');
                            while($temp_begin < strtotime($recurring['until_date'])){
                                $new_start_time = date('Y-m-d H:i:s',$temp_begin);
                                $new_end_time = date('Y-m-d H:i:s',$temp_end);
                                $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $new_start_time, 'end_time' => $new_end_time, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'],'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                                $temp_begin = strtotime($new_start_time.' +'.$recurring['repeat_time'].' weeks');
                                $temp_end = strtotime($new_end_time.' +'.$recurring['repeat_time'].' weeks');
                            }
                        }
                    }
                }
                else if($recurring['repeat_type'] == 2){
                    $repeat_on = $recurring['repeat_on'];
                    foreach($repeat_on as $dow) {
                        $new_start_time = '';
                        if ($dow == 1)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' monday'));
                        elseif ($dow == 2)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' tuesday'));
                        elseif ($dow == 3)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' wednesday'));
                        elseif ($dow == 4)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' thursday'));
                        elseif ($dow == 5)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' friday'));
                        elseif ($dow == 6)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' saturday'));
                        elseif ($dow == 7)
                            $new_start_time = date('Y-m-d', strtotime($posts['data']['start_time'].' sunday'));

                        if (strtotime($new_start_time) > strtotime(date('Y-m-d'))) {
                            $a = $new_start_time . ' ' . date('H:i:s', strtotime($posts['data']['start_time']));
                            $b = $new_start_time . ' ' . date('H:i:s', strtotime($posts['data']['end_time']));
                            $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $a, 'end_time' => $b, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'], 'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                        }
                        $c = $new_start_time;
                        $new_start_time = $c . ' ' . date('H:i', strtotime($posts['data']['start_time']));
                        $new_end_time = $c . ' ' . date('H:i', strtotime($posts['data']['end_time']));
                        $temp_begin = strtotime($new_start_time . ' +' . $recurring['repeat_time'] . ' weeks');
                        $temp_end = strtotime($new_end_time . ' +' . $recurring['repeat_time'] . ' weeks');
                        for ($i = 1; $i < intval($recurring['number_times']); $i++) {
                            if (strtotime($new_start_time) >= strtotime($posts['data']['start_time'])) {
                                $new_start_time = date('Y-m-d H:i:s', $temp_begin);
                                $new_end_time = date('Y-m-d H:i:s', $temp_end);
                                $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $new_start_time, 'end_time' => $new_end_time, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'], 'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                                $temp_begin = strtotime($new_start_time . ' +' . $recurring['repeat_time'] . ' weeks');
                                $temp_end = strtotime($new_end_time . ' +' . $recurring['repeat_time'] . ' weeks');
                            }
                            else{
                                $new_start_time = strtotime($new_start_time . ' +' . $recurring['repeat_time'] . ' weeks');
                                $i++;
                            }
                        }
                    }
                }
            }
            else if($recurring['recurring_type'] == 3){
                $new_start_time = strtotime($posts['data']['start_time'].' +'.$recurring['repeat_time'].' months');
                $new_end_time   = strtotime($posts['data']['end_time'].' +'.$recurring['repeat_time'].' months');
                if($recurring['repeat_type'] == 1) { // until a date
                    while ($new_start_time <= strtotime($recurring['until_date'])) {
                        if (is_numeric($new_start_time)) {
                            $new_start_time = date('Y-m-d H:i:s', $new_start_time);
                            $new_end_time = date('Y-m-d H:i:s', $new_end_time);
                        }
                        $repeat_booking['start_time'] = date('Y-m-d H:i:s', strtotime($new_start_time));
                        $repeat_booking['end_time'] = date('Y-m-d H:i:s', strtotime($new_end_time));
                        $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $new_start_time, 'end_time' => $new_end_time, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'],'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                        $new_start_time = strtotime($new_start_time . ' +' . $recurring['repeat_time'] . ' months');
                        $new_end_time   = strtotime($new_end_time   . ' +' . $recurring['repeat_time'] . ' months');
                    }
                }
                else if($recurring['repeat_type'] == 2){
                    for($i=1;$i<intval($recurring['number_times']);$i++){
                        if (is_numeric($new_start_time)) {
                            $new_start_time = date('Y-m-d H:i:s', $new_start_time);
                            $new_end_time = date('Y-m-d H:i:s', $new_end_time);
                        }
                        $repeat_booking['start_time'] = date('Y-m-d H:i:s', strtotime($new_start_time));
                        $repeat_booking['end_time'] = date('Y-m-d H:i:s', strtotime($new_end_time));
                        $insert_array[] = array('id' => $posts['id'], 'data' => array('start_time' => $new_start_time, 'end_time' => $new_end_time, 'type' => $posts['data']['type'], 'description' => $posts['data']['description'],'action' => $posts['data']['action'], 'booking_id' => $posts['data']['booking_id']));
                        $new_start_time = strtotime($new_start_time . ' +' . $recurring['repeat_time'] . ' months');
                        $new_end_time   = strtotime($new_end_time   . ' +' . $recurring['repeat_time'] . ' months');
                    }
                }
            }

            foreach($insert_array as $item){
                if($this->check_employee_time_slot(array('employee_id' => $item['id'],'start_time' => $item['data']['start_time'],
                    'hidden_end_time' => $item['data']['end_time'],'booking_id' => $item['data']['booking_id']))){
                    $this->ajax_content(REQUEST_FAIL,"You can't setup a block/away/offline to this employee, a conflict appointment has been found.");
                    exit;
                }
            }

            foreach($insert_array as $item){
                $this->load->controller_model('scheduler')->insert_bao_booking($item);
            }
        }

        $message = "You have updated a block comment";
        $this->ajax_content(REQUEST_SUCCESS,$message);
    }

    function save_event($type = "employee"){
        $posts = $this->input->post('field_post');
        if($this->check_employee_time_slot($posts)){
            $this->ajax_content(REQUEST_FAIL,"You can't move the appointment to the time slot has been full.");
            exit;
        }

        if($this->check_employee_pending_appointment($posts)){
            $this->ajax_content(REQUEST_FAIL,"You can't move the appointment to the time slot has been full.");
            exit;
        }

        if(isset($posts['service_id']) && $posts['service_id'] != 0 && $posts['service_id'] != ''){
            if((!isset($posts['customer_id']) || $posts['customer_id'] == 0) && isset($posts['booking_status']) && $posts['booking_status'] != BOOKING_STATUS('Hold')){
                $this->ajax_content(REQUEST_FAIL,"You haven't choosen customer from the list.");
                exit;
            }

            if(!isset($posts['employee_id']) || $posts['employee_id'] == 0){
                $this->ajax_content(REQUEST_FAIL,"You haven't choosen employee or this employee does not assign with this service");
                exit;
            }
        }
        else{
            $posts['service_id'] = 0;
        }
        if(isset($posts['booking_status']) && ($posts['booking_status'] == BOOKING_STATUS('HoldBill') /*|| $posts['booking_status'] == BOOKING_STATUS('Payment')*/)){
            $this->ajax_content(REQUEST_FAIL,"You can't move the appointment has been create HOLD BILL");//  or PAID BILL
            exit;
        }

        if(isset($posts['booking_status']) && $posts['booking_status'] == 4){
            if(isset($posts['booking_id']) && $posts['booking_id'] != 0){
                $result = $this->load->controller_model('scheduler')->update_comment_booking($posts);
                $message = "You have updated a block comment";
            }
            else{
                $result = $this->load->controller_model('scheduler')->insert_comment_booking($posts);
                $message = "You have added a new block comment";
            }
            $this->ajax_content(REQUEST_SUCCESS,$message);
        }
        else{
            if($posts['service_id'] == 0 && isset($posts['customer_id']) && $posts['customer_id'] == 0){
                if(isset($posts['booking_id']) && $posts['booking_id'] != 0){
                    $result = $this->load->controller_model('scheduler')->update_comment_booking($posts);
                    $message = "You have updated a block comment";
                }
                else{
                    $result = $this->load->controller_model('scheduler')->insert_comment_booking($posts);
                    $message = "You have added a new block comment";
                }
                $this->ajax_content(REQUEST_SUCCESS,$message);
            }
            else{
                if($posts['booking_status'] != 8) {
                    if (!$this->check_employee_service($posts['service_id'], $posts['employee_id'])){
                        $this->ajax_content(REQUEST_FAIL, "This staff haven't assigned to this service appointment, please check again.");
                        exit;
                    }
                    if ((!isset($posts['room_id']) || $posts['room_id'] == '' || $posts['room_id'] == 0) && $posts['is_bypass'] == 0) {
                        $this->ajax_content(REQUEST_FAIL, "You haven't choosen room name.");
                        exit;
                    }
                }
                $insert_array = array();
                $main_book = '';

                if(!$posts['is_old']){
                    $result = $this->load->controller_model('scheduler')->insert_booking($posts,null,'scheduler');
                    // LOG INSERT APPOINTMENT
                    $logData = $this->load->controller_model('scheduler')->getPrepareData($result);
                    $this->load->controller_model('audit_trails')->log(array(),array(),
                        array('action' => 'Create Appointment',
                            'object' => $logData['customer'],
                            'objectDetail' => $logData['branch'].' - '.$logData['item'].' - From '.get_user_date($logData['start_time'],'','Y-m-d H:i').': To '.get_user_date($logData['end_time'],'','Y-m-d H:i').' - '.$logData['room'],
                            'module' => 'staff/'.$this->moduleName));
                    // END LOG
                    $main_book = $result;
                }else{
                    $result = $this->load->controller_model('scheduler')->update_booking($posts,'scheduler');
                    // LOG INSERT APPOINTMENT
                    $logData = $this->load->controller_model('scheduler')->getPrepareData($result);
                    $this->load->controller_model('audit_trails')->log(array(),array(),
                        array('action' => 'Update Appointment',
                            'object' => $logData['customer'],
                            'objectDetail' => $logData['branch'].' - '.$logData['item'].' - From '.get_user_date($logData['start_time'],'','Y-m-d H:i').': To '.get_user_date($logData['end_time'],'','Y-m-d H:i').' - '.$logData['room'],
                            'module' => 'staff/'.$this->moduleName));
                    // END LOG
                }

                if(isset($posts['recurring_data']) && $posts['recurring_data'] != ''){
                    $recurring = $posts['recurring_data'];
                    $recurring['parent_booking_id'] = $result;
                    $this->save_booking_recurring($result,$recurring);
                }
                if(isset($posts['recurring_data']) && $posts['booking_status'] != 8){
                    $recurring_data = $posts['recurring_data'];
                    $recurring_data['parent_booking_id'] = $result;
                    $repeat_booking = array(
                        'customer_id' => $posts['customer_id'],
                        'room_id'     => $posts['room_id'],
                        'employee_id' => $posts['employee_id'],
                        'start_time'  => '',
                        'comment'     => $posts['comment'],
                        'service_id'  => $posts['service_id']
                    );
                    if(isset($recurring_data['recurring_type']) && intval($recurring_data['recurring_type']) == 1){
                        if(intval($recurring_data['repeat_type']) == 1){ // until a date
                            $temp_begin = strtotime($posts['start_time'].' +'.$recurring_data['repeat_time'].' days');
                            while($temp_begin <= strtotime($recurring_data['until_date'])){
                                $repeat_booking['start_time'] = date('Y-m-d H:i:s',$temp_begin);
                                $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                $temp_begin = strtotime($repeat_booking['start_time'].' +'.$recurring_data['repeat_time'].' days');
                            }
                        }
                        else if($recurring_data['repeat_type'] == 2){ // a number of times
                            for($i=1;$i<intval($recurring_data['number_times']);$i++){
                                $repeat_booking['start_time'] = date('Y-m-d H:i:s',strtotime($posts['start_time'].' +'.$recurring_data['repeat_time']*$i.' days'));
                                $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                            }
                        }
                    }
                    else if(isset($recurring_data['recurring_type']) && $recurring_data['recurring_type'] == 2){
                        if(intval($recurring_data['repeat_type']) ==  1) { // until a date
                            $repeat_on = $recurring_data['repeat_on'];
                            foreach($repeat_on as $dow){
                                $new_start_time = '';
                                if($dow == 1)
                                    $new_start_time = date('Y-m-d',strtotime('this monday'));
                                elseif($dow == 2)
                                    $new_start_time = date('Y-m-d',strtotime('this tuesday'));
                                elseif($dow == 3)
                                    $new_start_time = date('Y-m-d',strtotime('this wednesday'));
                                elseif($dow == 4)
                                    $new_start_time = date('Y-m-d',strtotime('this thursday'));
                                elseif($dow == 5)
                                    $new_start_time = date('Y-m-d',strtotime('this friday'));
                                elseif($dow == 6)
                                    $new_start_time = date('Y-m-d',strtotime('this saturday'));
                                elseif($dow == 7)
                                    $new_start_time = date('Y-m-d',strtotime('next sunday'));
                                if(strtotime($new_start_time) > strtotime(date('Y-m-d'))){
                                    $repeat_booking['start_time'] = $new_start_time.' '.date('H:i:s',strtotime($posts['start_time']));
                                    $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                }
                                if(strtotime($new_start_time) >= strtotime(date('Y-m-d'))){
                                    $new_start_time = $new_start_time.' '.date('H:i',strtotime($posts['start_time']));
                                    $temp_begin = strtotime($new_start_time.' +'.$recurring_data['repeat_time'].' weeks');
                                    while($temp_begin <= strtotime($recurring_data['until_date'])){
                                        $repeat_booking['start_time'] = date('Y-m-d H:i:s',$temp_begin);
                                        $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                        $temp_begin = strtotime($repeat_booking['start_time'].' +'.$recurring_data['repeat_time'].' weeks');
                                    }
                                }
                            }
                        }
                        else if($recurring_data['repeat_type'] == 2){ // a number of times
                            $repeat_on = $recurring_data['repeat_on'];
                            foreach($repeat_on as $dow){
                                $new_start_time = '';
                                if($dow == 1)
                                    $new_start_time = date('Y-m-d',strtotime('this monday'));
                                elseif($dow == 2)
                                    $new_start_time = date('Y-m-d',strtotime('this tuesday'));
                                elseif($dow == 3)
                                    $new_start_time = date('Y-m-d',strtotime('this wednesday'));
                                elseif($dow == 4)
                                    $new_start_time = date('Y-m-d',strtotime('this thursday'));
                                elseif($dow == 5)
                                    $new_start_time = date('Y-m-d',strtotime('this friday'));
                                elseif($dow == 6)
                                    $new_start_time = date('Y-m-d',strtotime('this saturday'));
                                elseif($dow == 7)
                                    $new_start_time = date('Y-m-d',strtotime('next sunday'));
                                if(strtotime($new_start_time) > strtotime(date('Y-m-d'))){
                                    $repeat_booking['start_time'] = $new_start_time.' '.date('H:i:s',strtotime($posts['start_time']));
                                    $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                }
                                if(strtotime($new_start_time) >= strtotime(date('Y-m-d'))){
                                    $new_start_time = $new_start_time.' '.date('H:i',strtotime($posts['start_time']));
                                    for($i=1;$i<intval($recurring_data['number_times']);$i++){
                                        $repeat_booking['start_time'] = date('Y-m-d H:i:s',strtotime($new_start_time.' +'.$recurring_data['repeat_time']*$i.' weeks'));
                                        $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                    }
                                }
                            }
                        }
                    }
                    // MONTHLY RECURRING
                    else if(isset($recurring_data['recurring_type']) && $recurring_data['recurring_type'] == 3) {
                        $new_start_time = strtotime($posts['start_time'].' +'.$recurring_data['repeat_time'].' months');
                        if(intval($recurring_data['repeat_type']) ==  1) { // until a date
                            while($new_start_time <= strtotime($recurring_data['until_date'])){
                                if(is_numeric($new_start_time)){
                                    $new_start_time = date('Y-m-d H:i:s',$new_start_time);
                                }
                                $repeat_booking['start_time'] = date('Y-m-d H:i:s',strtotime($new_start_time));
                                $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                $new_start_time = strtotime($repeat_booking['start_time'].' +'.$recurring_data['repeat_time'].' months');
                            }
                        }
                        elseif(intval($recurring_data['repeat_type']) ==  2){ // a number of times
                            for($i=1;$i<intval($recurring_data['number_times']);$i++){
                                if(is_numeric($new_start_time)){
                                    $new_start_time = date('Y-m-d H:i:s',$new_start_time);
                                }
                                $repeat_booking['start_time'] = date('Y-m-d H:i:s',strtotime($new_start_time));
                                $insert_array[] = array('repeat_booking' => $repeat_booking,'recurring_data' => $recurring_data);
                                $new_start_time = strtotime($repeat_booking['start_time'].' +'.$recurring_data['repeat_time'].' months');
                            }
                        }
                    }
                }
                foreach($insert_array as $item){
                    $check = $this->load->controller_model('scheduler')->_check_each_service($item['repeat_booking'],null,null,TRUE);
                    if($check != '' && !is_array($check) && $main_book != ''){
                        $this->load->controller_model('scheduler')->delete_booking($main_book);
                        if(isset($posts['recurring_data']) && $posts['recurring_data'] != ''){
                            $list_booking = $this->load->controller_model('scheduler')->get_list_recurring_booking_id($main_book);
                            $this->load->controller_model('scheduler')->delete_recurring_booking($list_booking);
                        }
                        $this->ajax_content(REQUEST_FAIL,$check);
                        exit;
                    }
                }

                foreach($insert_array as $item){
                    $booking_id = $this->load->controller_model('scheduler')->insert_booking($item['repeat_booking'],null,'scheduler');
                    if(isset($posts['recurring_data'])  && $posts['recurring_data'] != ''){
                        $this->save_booking_recurring($booking_id,$item['recurring_data']);
                    }
                }
                //    $booking_data = $this->load->controller_model('scheduler')->getBookingData($type,array('id' => $result));
                $message = isset($posts['booking_id'])?"You have saved the booking":"You have added a new booking";
                $this->ajax_content(REQUEST_SUCCESS,$message);
            }
        }
    }

    function save_booking_recurring($booking_id,$recurring_data){
        if(!empty($recurring_data)){
            $result = $this->load->controller_model('scheduler')->save_booking_recurring($booking_id,$recurring_data);
            if($result){
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
        else{
            return FALSE;
        }
    }

    function delete_event(){
        $posts = $this->input->post('field_post');
        $result = $this->load->controller_model('scheduler')->delete_booking($posts['booking_id']);
        if(isset($posts['delete_recurring']) && $posts['delete_recurring'] == 'true'){
            $list_booking = $this->load->controller_model('scheduler')->get_list_recurring_booking_id($posts['booking_id']);
            $result = $this->load->controller_model('scheduler')->delete_recurring_booking($list_booking);
        }
        if($result){
            // LOG DELETE APPOINTMENT
            $logData = $this->load->controller_model('scheduler')->getPrepareData($posts['booking_id']);
            if($logData){
                $object = $logData['customer'];
                $objectDetail = $logData['branch'].' - '.$logData['item'].' - From '.get_user_date($logData['start_time'],'','Y-m-d H:i').': To '.get_user_date($logData['end_time'],'','Y-m-d H:i').' - '.$logData['room'];
            }
            else{
                $object = 'ID Booking to trace: '.$posts['booking_id'];
                $objectDetail = '';
            }
            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Delete Block Appointment',
                    'object' => $object,
                    'objectDetail' => $objectDetail,
                    'module' => 'staff/'.$this->moduleName));
            // END LOG
            $this->ajax_content(REQUEST_SUCCESS,"You have delete a booking",array());
        }else{
            $this->ajax_content(REQUEST_FAIL,$result);
        }
    }

    function reserve_event(){
        $posts = $this->input->post('field_post');
        $result = $this->load->controller_model('scheduler')->reserve_booking($posts['booking_id'],$posts['reserve_comment']);
        if($result){
            // LOG INSERT APPOINTMENT
            $logData = $this->load->controller_model('scheduler')->getPrepareData($posts['booking_id']);
            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Reserve Appointment',
                    'object' => $logData['customer'],
                    'objectDetail' => $logData['branch'].' - '.$logData['item'].' - From '.get_user_date($logData['start_time'],'','Y-m-d H:i').': To '.get_user_date($logData['end_time'],'','Y-m-d H:i').' - '.$logData['room'].' - Reserve Comment: '.$posts['reserve_comment'],
                    'module' => 'staff/'.$this->moduleName));
            // END LOG
            $this->ajax_content(REQUEST_SUCCESS,"You have reserve a booking",array());
        }else{
            $this->ajax_content(REQUEST_FAIL,$result);
        }
    }

    function reorder($employee_id,$order_number){
        if(!isset($employee_id) || ! isset($order_number)){
            return $this->ajax_content(REQUEST_FAIL,"Incorrect employee or order_number");
        }
        $this->load->controller_model('scheduler')->update_order($employee_id,$order_number);
        return $this->ajax_content(REQUEST_SUCCESS,"");
    }

    function set_online_offline(){
        $employee_id = $this->input->post('employee_id');
        $date = $this->input->post('date');
        $first_hour = $this->input->post('first_hour');
        $last_hour = $this->input->post('last_hour');
        if(!isset($employee_id)){
            return $this->ajax_content(REQUEST_FAIL,"Incorrect employee");
        }
        $result = $this->load->controller_model('scheduler')->update_online_offline($employee_id,$date,$first_hour,$last_hour);
        if($result == 0)
            return $this->ajax_content(REQUEST_FAIL,"Cannot block. This employee have some booking.");
        return $this->ajax_content(REQUEST_SUCCESS,"");
    }

    function get_updated_data(){
        $marked_time = $this->input->post('marked_time');
        if(!$marked_time){
            return $this->ajax_content(REQUEST_FAIL);
        }
        // GET NEW BOOKING DATA HOLDING
        $booking_model = $this->load->table_model('booking');
        $bookings = $booking_model->get_detail(array('booking'=>array('status' => BOOKING_STATUS('Hold'),"booking.created_date > '$marked_time'",'branch_id' => $this->user_check->get('branch_id'))),2,1);

        foreach($bookings as $booking){
            if(isset($booking->bundle_id)){
                $booking->start_time = '9999-00-00 00:00:00';
                $booking->end_time = '0000-00-00 00:00:00';
                foreach($booking->services as $service){
                    if($service->start_time < $booking->start_time){
                        $booking->start_time = $service->start_time;
                    }
                    if($service->end_time > $booking->end_time){
                        $booking->end_time = $service->end_time;
                    }
                }
            }
            $booking->start_time = get_user_date($booking->start_time,'','Y-m-d H:i:s');
            $booking->end_time = get_user_date($booking->end_time,'','Y-m-d H:i:s');
        }
        // END GET NEW BOOKING DATA HOLDING

        date_default_timezone_set('Asia/Singapore');
        $now = date('Y-m-d H:i:s');

        // DELETE OVER FIVE MINUTE BOOKING
        $list_pending = $this->load->table_model('booking_pending_appointment')->get_list_pending(array('branch_id' => $this->user_check->get('branch_id')));
        foreach($list_pending as $item) {
            $now = strtotime(get_user_date('', '', 'Y-m-d H:i:s'));
            $compare = strtotime($item->start_time);
            if (($now - $compare) > 300) {
                $this->load->table_model('booking_pending_appointment')->delete_list_pending($item->unique_id);
            }
        }
        // END DELETE OVER FIVE MINUTE BOOKING

        // GET BOOKING PENDING APPOINTMENT
            $list_pending = $this->load->table_model('booking_pending_appointment')->get_list_pending(array('branch_id' => $this->user_check->get('branch_id')));
        // END GET BOOKING PENDING APPOINTMENT
        // CHECK WRONG TAGGING BILL_ID TO BOOKING APPOINTMENT
        $minute = date('i');
        $hour = date('H');
        if($minute == 30 && in_array($hour, array(12,14,16,18,20))){
            $booking_model->check_wrong_bill_tagging();
        }
        // END CHECK WRONG TAGGING BILL ID TO BOOKING APPOINTMENT

        // GET NUMBER OF NEW APPOINTMENT
        $news_appointment = $booking_model->get_number_of_new_appointment(array('branch_id' => $this->user_check->get('branch_id')));
        // END GET NUMBER OF NEW APPOINTMENT
        echo json_encode(array('number_new_appointment' => $news_appointment,'marked_time' => get_database_date(), 'new_bookings' => $bookings, 'list_pending' => $list_pending));
        exit;
    }

    function edit_blocktime($type = "employee"){
        $posts = $this->input->post('field_post');

        if(!$posts['is_old']){
            $result = $this->load->controller_model('scheduler')->insert_booking($posts,null,'scheduler');
        }else{
            $result = $this->load->controller_model('scheduler')->update_booking($posts,'scheduler');
        }
        $booking_data = $this->load->controller_model('scheduler')->getBookingData($type,array('id' => $result));
        $message = isset($posts['booking_id'])?"You have saved the booking":"You have added a new booking";
        $this->ajax_content(REQUEST_SUCCESS,$message);
    }

    function notify_rooms_available(){
        // Get all employee of this branch
        $sql = $this->load->table_model('employee')->get();
        $available_employee = array();
        foreach($sql as $value){
            array_push($available_employee, $value->id);
        }

        $timezone = +8;
        $current_hour = date("Y-m-d H:i:s", time() - ($timezone*3600)); // Get current date by timezone
        $next_hour = date('Y-m-d H:i:s', strtotime($current_hour) + 3600);
        
        // Get all booking in current day
        $booking_service = $this->load->controller_model('scheduler')->getDataForRoomNotify($available_employee);
        $not_avail_room = array();
        
        foreach($booking_service as $value){
            /*echo $value->start_time."---".$value->end_time."\n";
            echo $current_hour."---".$next_hour."\n";
            echo "\n\n";*/
            if($value->start_time < $current_hour && $value->end_time > $current_hour)
                array_push($not_avail_room, $value->room_id);

            elseif($value->start_time < $next_hour && $value->end_time > $next_hour)
                array_push($not_avail_room, $value->room_id);

            elseif($current_hour == $value->start_time && $next_hour == $value->end_time)
                array_push($not_avail_room, $value->room_id);

            elseif($current_hour < $value->start_time && $next_hour > $value->end_time)
                array_push($not_avail_room, $value->room_id);

            elseif($current_hour > $value->start_time && $next_hour < $value->end_time)
                array_push($not_avail_room, $value->room_id);

            elseif($current_hour == $value->start_time && $next_hour < $value->end_time)
                array_push($not_avail_room, $value->room_id);

            elseif($current_hour > $value->start_time && $next_hour == $value->end_time)
                array_push($not_avail_room, $value->room_id);
        }

        // Get room type list and avaiable room from current time to next 1 hour
        $result = $this->load->table_model('room_type')->get();
        foreach($result as $value){
            $temp = array();
            $room_list = $this->load->table_model('room')->get(array('room_type_id' => $value->id));
            foreach($room_list as $room){
                if(!in_array($room->id, $not_avail_room))
                    array_push($temp, $room->id);
            }
            $value->room_list = $temp;
        }
        
        $temp["data"] = $result;
        $data = $this->content($this->moduleName.'/room_notify',$temp, true);
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data));
    }

    function getSimpleCustomerForm(){
        $status = REQUEST_SUCCESS;
        $message = "";
        $replace_button = 0;
        $success = "get_form_success";
        $buttons = array(
            'success' => array(
                'text'      => 'Save',
                'class'     => "btn-primary",
            ),
            'cancel' => array(
                'text'  => 'Cancel'
            )
        );
        $success = "";
        $content = $this->content('staff/scheduler/simple_customer_form','', true);

        $option = array(
            'button' => $buttons,
            'replace_button' => $replace_button
        );
        if($success !== ""){
            $option["success"] = $success;
        }
        $this->ajax_content($status,$message,array('type' => 'process','content' =>$content, 'option' => $option));
    }

    function getTest(){
        $status = REQUEST_SUCCESS;
        $message = "";
        $content = $this->content('staff/scheduler/simple_customer_form','', true);
        $this->ajax_content($status,$message,$content);
    }

    function block_check_collision(){
        $employee_id = $this->input->post('employee_id');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $booking_id = $this->input->post('booking_id');
        if($booking_id == ''){
            $booking_id = 0;
        }
        $is_double = $this->load->controller_model('scheduler')->check_double_comment($start_time,$end_time,$employee_id,$booking_id);
        $status = REQUEST_SUCCESS;
        $message = "";
        $this->ajax_content($status,$message,$is_double);
    }

    function check_valid_service_item(){
        $service_id = $this->input->post('service_id');
        $result = $this->load->table_model('item')->get(array('id' => $service_id));
        $employee_service = $this->load->table_model('employee_service')->get(array('item_id' => $result[0]->id));
        $employee = array();
        foreach($employee_service as $item){
            $employee[] = $item->employee_id;
        }
        $result[0]->employee = $employee;
        $status = REQUEST_SUCCESS;
        $message = "";
        if(!count($result)){
            $result[0] = new stdClass();
            $result[0]->status = 4;
        }
        $this->ajax_content($status,$message,$result);
    }

    function check_employee_service($service_id,$employee_id){
        $employee_service = $this->load->table_model('employee_service')->get(array('item_id' => $service_id));
        $employee = array();
        foreach($employee_service as $item){
            $employee[] = $item->employee_id;
        }
        if(in_array($employee_id,$employee)){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    function check_employee_time_slot($data){
        $check_timeslot = $this->load->controller_model('scheduler')->check_timeslot(array(
            'employee_id' => $data['employee_id'],
            'start_time' => $data['start_time'],
            'end_time' => $data['hidden_end_time'],
            'booking_id' => $data['booking_id']
        ));
        return $check_timeslot;
    }

    function check_employee_pending_appointment($data){
        $check = $this->load->controller_model('scheduler')->check_pending_appointment(array(
            'employee_id' => $data['employee_id'],
            'start_time' => $data['start_time'],
            'end_time' => $data['hidden_end_time']
        ));
        return $check;
    }

    function searchHistoryBooking(){
        $searchText = trim($this->input->get_post('filter', true));
        $searchResult = $this->load->controller_model('scheduler')->getHistoryBooking($searchText);

        if(empty($searchResult)){
            echo json_encode(array('result' => TRUE, 'data' => array()));
        }
        else{
            echo json_encode(array('result' => TRUE, 'data' => $searchResult));
        }
    }

    function check_room_quantity_bed($data = false){
        if(!$data){
            $data = $this->input->get_post('field_post', true);
        }

        $check = $this->load->controller_model('scheduler')
            ->_getAvailRoom(strtotime($data['start_time']), $data['service_id'], $data['booking_id']);

        if(count($check) >0){
            $result = FALSE;
            $temp = '';
            foreach($check as $item){
                if($item->bed_quantity > 0){
                    $temp = $item->id;
                    if($item->id == $data['room_id']){
                        $result = $item->id;
                        break;
                    }
                }
            }
            if(!$result){
                $result = $temp;
            }
        }
        else{
            $result = FALSE;
        }

        if(isset($data['scheduler']) && $data['scheduler'] == 1){
            if ($result) {
                $this->ajax_content(TRUE,'',$result);
            } else {
                $this->ajax_content(FALSE,'','');
            }
        }
        else {
            if ($result) {
                return array('result' => TRUE, 'data' => $data);
            } else {
                return array('result' => FALSE);
            }
        }
    }

    function getExportCancelledApptsForm(){
        $data['branch_id']      = $this->user_check->get('branch_id');
        $data['default']        = array();
        $ret = array();
        $ret['content'] = $this->content('staff/scheduler/export_cancelled_appts_form',$data,true);

//        $buttons = array(
//            'success' => array(
//            //    'url'       => 'scheduler/exportCancelledAppts',
//                'id' => 'submit_form',
//                'text'      => 'Export',
//                'class'     => "btn-primary",
//                'before'    => 'validateData',
//            //    'success'   => 'exportData',
//                'close_modal' => true,
//            //    'more'      => array('branch_id' => $data['branch_id'])
//            ),
//            'cancel' => array(
//                'text'      => 'Cancel',
//                'class'     => 'lightbox-close',
//                'close_modal' => true,
//            )
//        );
        $ret['option'] = array(
            'title' => "Export Cancelled Data",
//            'button'  => $buttons
        );
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function exportCancelledAppts(){
        $postData = array(
            'branchId' => $this->input->get('branchId',true),
            'startDate' => $this->input->get('startDate',true),
            'endDate' => $this->input->get('endDate',true)
        );
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $field_data = array(
            'Code', 'Booking Date', 'Comment','Reserve Comment', 'Branch',
            'Customer Code', 'Customer Name', 'Customer Phone Number' ,'Customer Email',
            'Start Time', 'End Time', 'Service', 'Employee'
        );
        $cols = 0;
        foreach ($field_data as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cols, 1, $field);
            $cols++;
        }
        $row = 2;
        $data = $this->load->controller_model($this->moduleName)->getExcelCancelledApptsData($postData);

        foreach ($data as $key => $record) {
            $col = 0;
            $sheet = $objPHPExcel->getActiveSheet();
            foreach ($field_data as $key => $field) {
                $sheet->setCellValueByColumnAndRow($col, $row, $record[$field]);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Cancelled Appointment Data');
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="cancelled_appointment_data_'.date('Y_m_d_H_i_s',strtotime('NOW')).'.xls"');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}