<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Rooms extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "rooms";
        $this->pageCode = "rooms";
        $this->pageName = "Rooms";
        $this->breadcrumbs[] = array('url' => 'staff/rooms', 'text' => 'Rooms');
    }

    function getForm($id = 0){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getRoom($id);
        if(!empty($data)){
            $data = $data[0];
        }
        $data = $this->content($this->moduleName.'/edit_form',array('item' => $data), true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }


    function suggestItem(){
        $keyword = $this->input->get('term');
        $modelName = $this->load->controller_model($this->moduleName);

        $suggest = $modelName->getItemSuggestion($keyword);

        echo json_encode($suggest);
    }

    function getItem($id){
        $modelName = $this->load->controller_model($this->moduleName);

        $item = $modelName->getItem($id);
        $this->ajax_content(REQUEST_SUCCESS,"",$item);
    }

    function disable_room($id){
        try {
            $this->load->controller_model($this->moduleName)->disable_room($id);
            $this->ajax_content(REQUEST_SUCCESS,'The room has been disabled');
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }

    function enable_room($id){
        try {
            $this->load->controller_model($this->moduleName)->enable_room($id);
            $this->ajax_content(REQUEST_SUCCESS,'The room has been enabled');
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }
    }

}