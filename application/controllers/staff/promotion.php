<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Promotion extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "promotion";
        $this->pageCode = "promotion";
        $this->pageName = "Promotion";
        $this->breadcrumbs[] = array('url' => 'staff/promotion', 'text' => 'Promotion');
    }

    function getForm($id = 0){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getPromotion($id);
        $data = $modelName->getPromotionDetail($id,$data);
        $data = $this->content($this->moduleName.'/edit_form',array('item' => $data), true);
        $buttons = array(
            'success' => array(
                'url'       => site_url($this->dir . '/edit'),
                'text'      => 'Save',
                'class'     => "btn-primary"
            ),
            'cancel' => array(
                'text'  => 'Cancel',
            )
        );
        $option = array(
            'success' => 'get_form_success',
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content'=>$data, 'type' => 'dialog', 'option' => $option));
    }


    function suggestItem(){
        $keyword = $this->input->get('term');
        $modelName = $this->load->controller_model($this->moduleName);

        $suggest = $modelName->getItemSuggestion($keyword);

        echo json_encode($suggest);
    }

    function getItem($id){
        $modelName = $this->load->controller_model($this->moduleName);

        $item = $modelName->getItem($id);
        $this->ajax_content(REQUEST_SUCCESS,"",$item);
    }

}