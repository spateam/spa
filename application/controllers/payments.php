<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Payments extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "payments";
        $this->pageCode = "payments";
        $this->pageName = "Payments";
        $this->breadcrumbs[] = array('url' => 'payments', 'text' => 'Payments');
    }


}