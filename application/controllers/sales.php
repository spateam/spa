<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "sales";
        $this->pageCode = "sales";
        $this->pageName = "Sales";
        $this->breadcrumbs[] = array('url' => 'sales', 'text' => 'Sales');
        $this->load->library('cart','email');
    }

    private function init($type = 'index'){
        /*---- Set current payment ------*/
        $sale_session = $this->session->userdata('sale');
        if($sale_session){
            $sale_session->current_payment = isset($sale_session->current_payment)?$sale_session->current_payment:$this->system_config->get('cash');
        }else{
            $sale_session = (object)array('current_payment' => $this->system_config->get('cash'));
        }
        if($type != 'retrieve' && $type != 'suspend') {
            $preBillData = $this->session->userdata('preBillData');
            if ($preBillData) {
                $sale_session->booking_id = $preBillData['booking_id'];
                $sale_session->comment = $preBillData['comment'];
                $modelName = $this->load->controller_model($this->moduleName);
                if (isset($preBillData['item']) && $preBillData['item'] != '') {
                    $modelName->insertItemToCart($preBillData['item']);
                    $preBillData['item'] = '';
                }
                $cart_content = $this->session->userdata('cart_contents');
                if (count($cart_content) == 3) {
                    foreach ($cart_content as $key => $item) {
                        if (is_array($item)) {
                            $cart_content[$key]['options']['choice_staff'] = array($preBillData['employee_id']);
                        }
                    }
                    $this->session->set_userdata('cart_contents', $cart_content);
                }
            } else {
                $this->session->unset_userdata('preBillData');
                $this->session->unset_userdata('sale');
            }
            $this->session->set_userdata('preBillData', $preBillData);
        }
        $this->session->set_userdata('sale',$sale_session);
    }

    function index(){

        if($this->user_permission->checkPermission('i','bills') == false){
            echo "You dont have permission"; exit;
        }
        $this->init('index');
        $data = new stdClass();
        $data->pageCode = $this->pageCode;
        $this->page($this->moduleName, $data);
    }

    function savePreSessionBill(){
        $this->session->unset_userdata('sale');
        $this->session->unset_userdata('cart_contents');
        $data = $this->input->post('field_post');
        $this->session->set_userdata('preBillData',$data);

        // LOG CREATE BILL
        $this->load->controller_model('audit_trails')->log(array(),array(),
            array('action' => 'Create Bill From Appointment',
                'object' => $data['booking_id'],
                'objectDetail' => 'Create Bill from appointment : '.$data['booking_id'],
                'module' => 'staff/'.$this->moduleName));
        // END LOG

        return $this->ajax_content(REQUEST_SUCCESS,'');
    }

    function categories(){
        $modelName = $this->load->controller_model($this->moduleName);

        //Check credit in future

        $data = $modelName->getCategories();
        $this->ajax_content(REQUEST_SUCCESS,'',$data);
    }

    function items(){
        $category_id = $this->input->post('category');
        $modelName = $this->load->controller_model($this->moduleName);

        //Check credit in future
        $data = $modelName->getChildCategories($category_id);
        $dataItem = $modelName->getItems($category_id);
        foreach($dataItem as $item){
            $data[] = $item;
        }
        $this->ajax_content(REQUEST_SUCCESS,'',$data);
    }

    function getList(){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForCartView();
        $json = new stdClass();
        $json->content = $this->content($this->moduleName.'/cart', $data);
        $this->ajax_content(REQUEST_SUCCESS,'',$json);
    }

    function insert_row($item = ''){
        $modelName = $this->load->controller_model($this->moduleName);
        if($this->input->post('item')){
            $item_id = $this->input->post('item');
            $category_id = ($this->input->post('category'))?$this->input->post('category'):0;

            $result = $modelName->insertItemToCart($item_id, $category_id);
            if($result !== true){
                return $this->ajax_content(REQUEST_FAIL,$result);
            }
        }
        $this->getList();
    }

    function update_row(){
        $modelName = $this->load->controller_model($this->moduleName);
        if($this->input->post('rowid')){
            $rowid = $this->input->post('rowid');
            $row_values = $this->input->post('row_values');
            $modelName->updateItemToCart($rowid,json_decode($row_values));
        }

        $this->getList();
    }

    function delete_row(){
        $modelName = $this->load->controller_model($this->moduleName);

        if($this->input->post('rowid')){
            $rowid = $this->input->post('rowid');
            $modelName->deleteItemFromCart($rowid);
        }

        $this->getList();

    }

    function right_panel(){
        $modelName = $this->load->controller_model($this->moduleName);

        $json = new stdClass();
        $data = $modelName->getDataForRightPanel();

        $json->content = $this->content($this->moduleName.'/right_panel',$data);
        $this->ajax_content(REQUEST_SUCCESS,'',$json);
    }

    function set_customer(){
        try{
            $sale_session = $this->session->userdata('sale');
            if(isset($sale_session->customer_id)){
                unset($sale_session->customer_id);
                if(isset($sale_session->booking_id)){
                    unset($sale_session->booking_id);
                    $this->session->unset_userdata('preBillData');
                }
            }
            $modelName = $this->load->controller_model($this->moduleName);
            $customer_id = $this->input->post('customer_id');
            $result = $modelName->setCustomer($customer_id);
            if($result != 1){
                return $this->ajax_content(REQUEST_SUCCESS,"",array('process_type' => $result));
            }
            $result = $modelName->customerChange();
            $this->ajax_content(REQUEST_SUCCESS,"",array('process_type' => $result));
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage(),array());
        }
    }

    function set_customer_scheduler(){
        try{
            $modelName = $this->load->controller_model($this->moduleName);
            $customer_id = $this->input->post('customer_id');
            $customer_number = $this->input->post('mobile_number');
            $result = $modelName->setCustomerScheduler($customer_id,$customer_number);
            if($result != 1){
                return $this->ajax_content(REQUEST_SUCCESS,"",array('process_type' => $result));
            }
            $this->ajax_content(REQUEST_SUCCESS,"",array('process_type' => $result));
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage(),array());
        }
    }

    function check_customer_pin(){
        try{
            $modelName = $this->load->controller_model($this->moduleName);
            $sale = $this->session->userdata('sale');
            $customer_id = $sale->customer_id;
            $customer_pin = $this->input->post('pin');
            $result = $modelName->setCustomerSale($customer_id,$customer_pin);
            if($result != 1){
                return $this->ajax_content(REQUEST_SUCCESS,"",array('process_type' => $result));
            }
            $this->ajax_content(REQUEST_SUCCESS,"",array('process_type' => $result));
        }catch(Exception $e){
            $this->ajax_content(REQUEST_FAIL,$e->getMessage(),array());
        }
    }

    function detach_customer(){
        $modelName = $this->load->controller_model($this->moduleName);
        $result = $modelName->detachCustomer();
        if($result != 1){
            echo $result;
        }
        $result = $modelName->customerChange();
        if(is_numeric($result)){
            $this->ajax_content(REQUEST_SUCCESS);
        }else{
            $this->ajax_content(REQUEST_FAIL,"Somerthing wrong then detach customer");
        }
    }

    function add_payment(){
        $payment_values = $this->input->post('payment_values');
        $modelName = $this->load->controller_model($this->moduleName);

        $result = $modelName->addPayment(json_decode($payment_values));
        if($result == 1){
            $this->ajax_content(REQUEST_SUCCESS);
        }else{
            $this->ajax_content(REQUEST_FAIL,'Can not add payment. Please contact to administrator');
        }
    }

    function cancel_sale(){
        $this->cart->destroy();
        $this->session->unset_userdata('sale');
        $this->init();
        $this->ajax_content(REQUEST_SUCCESS);
    }

    function delete_payment(){
        $id = $this->input->post('id');
        $modelName = $this->load->controller_model($this->moduleName);
        $result = $modelName->removePayment($id);
        if($result == 1){
            $this->ajax_content(REQUEST_SUCCESS);
        }else{
            $this->ajax_content(REQUEST_FAIL,'Can not delete payment. Please contact administrator');
        }
    }

    function update_right_panel(){
        $data = json_decode($this->input->post('data'));
        $sale = $this->session->userdata('sale');
        $sale->comment = $data->comment;
        $this->session->set_userdata($sale);
        $this->ajax_content(REQUEST_SUCCESS, "");
    }

    function complete(){
        if($this->user_permission->checkPermission('i','bills') == false){
            echo "You don't have this permission"; exit;
        }

        if(!isset($this->session->userdata('sale')->customer_id)){
            $this->ajax_content(REQUEST_FAIL,'Please enter a customer');
            return;
        }
        $cancel_update_appointment = 0;
        if(isset($this->session->userdata('sale')->booking_id)){
            $booking_customer = $this->load->table_model('booking')->getByID($this->session->userdata('sale')->booking_id);
            if(isset($booking_customer->customer_id) && $booking_customer->customer_id != $this->session->userdata('sale')->customer_id){
                $cancel_update_appointment = 1;
            }
        }
        $comment =$this->input->post('comment');
        $modelName = $this->load->controller_model($this->moduleName);
        $modelName->checkDataBeforeSubmit();
        $bill_id = $modelName->insertBill($comment);

        /*------- After add the bill, send sms --------------------*/

        $this->load->library('Smslib');
        $this->smslib->sendBillSms($this->session->userdata('sale')->customer_id);

        /*------- Checking unsubscribe status of the customer ---------------*/
        $customer = $this->load->table_model('customer')->get(array('id' => $this->session->userdata('sale')->customer_id));
        $customer = $customer[0];
        if(isset($customer->is_subscribe) && $customer->is_subscribe == 1){
            $branch_info = $this->load->table_model('branch')->get(array('name' => $this->session->all_userdata()['staff']['login']->branch_name));
            $emailto = $customer->email;
            if (isset($emailto) && $emailto != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $this->email->set_newline("\r\n");
                $this->email->from($branch_info[0]->email,"Healing Touch Spa");
                $this->email->to($emailto);
                $this->email->subject('Please Feedback or Review Healing Touch Spa,'. $branch_info[0]->name);
                $body = $this->load->email_content('customer_reviews', $customer);
                $this->email->message($body);
                if($this->email->send()){
                    $this->load->table_model('bill')->updateEmailReviewBill($bill_id);
                }
                else{
                    echo $this->email->print_debugger();
                }
            }
        }

        /*------- After add the bill, destroy the sale session -----*/
        if(is_numeric($bill_id)){
            // LOG CREATE BILL
            $billcode = $modelName->getBillCode($bill_id);
            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Create Bill',
                    'object' => $billcode,
                    'objectDetail' => 'Create Bill with code : '.$billcode,
                    'module' => 'staff/'.$this->moduleName));
            // END LOG

            $booking_id = isset($this->session->userdata('sale')->booking_id) ? $this->session->userdata('sale')->booking_id : '';
            if($booking_id == ''){
                $pre = $this->session->userdata('preBillData');
                if($pre && isset($pre['booking_id']) && $pre['booking_id'] > 0){
                    $booking_id = $pre['booking_id'];
                }
            }

            $this->cart->destroy();
            $this->session->unset_userdata('sale');
            $this->session->unset_userdata('preBillData');
            $reload = 0;
            if($booking_id != '') {
                if(isset($cancel_update_appointment)) {
                    if ($cancel_update_appointment == 1) {
                        $this->load->table_model('booking')->update($booking_id, array('bill_id' => NULL));
                    } else {
                        $this->load->table_model('booking')->update($booking_id, array('status' => BOOKING_STATUS('Payment'), 'bill_id' => $bill_id));
                    }
                    $reload = 1;
                }
            }
            $this->ajax_content(REQUEST_SUCCESS,"",array('bill_id' => $bill_id,'reload' => $reload));
        }else{
            if($bill_id == ''){
                return;
            }
            $this->ajax_content(REQUEST_FAIL,$bill_id);
        };
    }

    function receipt($id){
        if($this->user_permission->checkPermission('v','bills') == false){
            echo "You don't have this permission"; exit;
        }

        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForBill($id, array(BILL_STATUS('Void'),BILL_STATUS('Complete')));

        $data->pageCode = $this->pageCode;

        $this->page($this->moduleName."/receipt", $data);

    }

    function recent_bills(){
        if($this->user_permission->checkPermission('v','bills') == false){
            echo "You don't have this permission"; exit;
        }
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForRecentBills();
        $json = new stdClass();
        $json->content = $this->content($this->moduleName.'/recent_bills',$data);
        return $this->ajax_content(REQUEST_SUCCESS, "", $json);
    }

    function suspend(){
        $modelName = $this->load->controller_model($this->moduleName);
        $modelName->checkDataBeforeSubmit();
        $bill_id = $modelName->suspendSale();
        /*------- After add the bill, destroy the sale session -----*/
        $this->cart->destroy();
        $this->session->unset_userdata('sale');
        $this->init('suspend');
        if(is_numeric($bill_id)){
            // LOG CREATE BILL
            $bill = $modelName->getBillData($bill_id);

            $detail = 'Description: '.$bill[0]['description'].', discount: '.$bill[0]['discount'].'<br>';
            $detail .= '<ul>';
            foreach($bill as $item){
                $detail .= '<li>'.$item['item_name'].' - quantity: '.$item['quantity'].' - discount: '.$item['discount_value'].(($item['discount_type']==1)? '$': '%').'</li>';
            }
            $detail .= '</ul>';

            $this->load->controller_model('audit_trails')->log(array(),array(),
                array('action' => 'Hold Bill',
                    'object' => $bill[0]['customer'],
                    'objectDetail' => $detail,
                    'module' => 'staff/'.$this->moduleName));
            // END LOG

            // check appointment
            $booking_id = isset($this->session->userdata('sale')->booking_id) ? $this->session->userdata('sale')->booking_id : '';
            if($booking_id == ''){
                $booking_id = isset($this->session->userdata('preBillData')['booking_id']) ? $this->session->userdata('preBillData')['booking_id'] : '';
            }

            $reload = 0;
            if($booking_id != '') {
                $old_status = $this->load->table_model('booking')->get(array('id' => $booking_id));
                if(count($old_status) > 0) {
                    $this->load->table_model('booking')->update($booking_id, array('status' => BOOKING_STATUS('HoldBill'), 'bill_id' => $bill_id, 'old_status' => $old_status[0]->status));
                    $reload = 1;
                }
            }
            $this->ajax_content(REQUEST_SUCCESS,"",array('reload' => $reload));
        }else{
            $this->ajax_content(REQUEST_FAIL, "Something wrong when suspend this sale. Please contact to administrator");
        }
    }

    function retrieve_bill_form(){
        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getSuspendedBills();
        $json = new stdClass();
        $json->content = $this->content($this->moduleName.'/suspended_bills',$data);
        $json->option = array(
            'title' => 'Suspended Bills'
        );
        $json->type = 'dialog';
        $this->ajax_content(REQUEST_SUCCESS, "", $json);
    }

    function delete_suspended_bill($id, $isAjax = true){
        // LOG DELETE SUSPENDED BILL
        $modelName = $this->load->controller_model($this->moduleName);
        $bill = $modelName->getBillData($id);

        $detail = 'Description: '.$bill[0]['description'].', discount: '.$bill[0]['discount'].'<br>';
        $detail .= '<ul>';
        foreach($bill as $item){
            $detail .= '<li>'.$item['item_name'].' - quantity: '.$item['quantity'].' - discount: '.$item['discount_value'].(($item['discount_type']==1)? '$': '%').'</li>';
        }
        $detail .= '</ul>';
        $this->load->controller_model('audit_trails')->log(array(),array(),
            array('action' => 'Delete Suspended Bill',
                'object' => $bill[0]['customer'],
                'objectDetail' => $detail,
                'module' => 'staff/'.$this->moduleName));
        // END LOG

        $this->load->controller_model($this->moduleName)->deleteBillDirectly($id);

        if($isAjax) {
            $appointment = $this->load->table_model('booking')->get(array('bill_id' => $id, 'status' => array(BOOKING_STATUS('HoldBill'))));
            if (count($appointment) == 1) {
                $this->load->table_model('booking')->update($appointment[0]->id, array('status' => $appointment[0]->old_status, 'bill_id' => '', 'old_status' => $appointment[0]->status));
                $reload = 1;
            } else {
                $reload = 0;
            }
        }
        if($isAjax){
            return $this->ajax_content(REQUEST_SUCCESS,"", array('reload' => $reload));
        }
    }

    function retrieve_bill($id){
        $modelName = $this->load->controller_model($this->moduleName);
        $modelName->retrieveBill($id);

        // LOG RETRIEVE BILL
        $bill = $modelName->getBillData($id);

        $detail = 'Description: '.$bill[0]['description'].', discount: '.$bill[0]['discount'].'<br>';
        $detail .= '<ul>';
        foreach($bill as $item){
            $detail .= '<li>'.$item['item_name'].' - quantity: '.$item['quantity'].' - discount: '.$item['discount_value'].(($item['discount_type']==1)? '$': '%').'</li>';
        }
        $detail .= '</ul>';
        $this->load->controller_model('audit_trails')->log(array(),array(),
            array('action' => 'Retrieve Bill',
                'object' => $bill[0]['customer'],
                'objectDetail' => $detail,
                'module' => 'staff/'.$this->moduleName));
        // END LOG

        $this->delete_suspended_bill($id, false);
        $this->init('retrieve');
        $appointment = $this->load->table_model('booking')->get(array('bill_id' => $id, 'status' => BOOKING_STATUS('HoldBill')));

        if(count($appointment) >= 1){
            $detail_appointment = $this->load->table_model('booking_service')->get(array('booking_id' => $appointment[0]->id));
            $this->session->set_userdata('preBillData',array(
                'booking_id' => $appointment[0]->id,
                'employee_id' => $detail_appointment[0]->employee_id,
                'comment' => $bill[0]['description']));
            $this->load->table_model('booking')->update($appointment[0]->id, array('status' => $appointment[0]->old_status, 'bill_id' => '', 'old_status' => ''));
            $reload = 1;
        }
        else{
            $reload = 0;
        }
        return $this->ajax_content(REQUEST_SUCCESS, "", array('reload' => $reload));
    }

    function barcode(){
        echo $this->content($this->moduleName.'/barcode');
    }

    function receipt_minimal($id,$status = 1){
        if($this->user_permission->checkPermission('v','bills') == false){
            echo "You don't have this permission"; exit;
        }

        $modelName = $this->load->controller_model($this->moduleName);
        $data = $modelName->getDataForBill($id, $status);
        $data->pageCode = $this->pageCode;

        $this->content($this->moduleName."/receipt_minimal", $data, false);
    }

    function check_need_pin(){
        $pin = $this->input->post('pin');
        if($pin){
            $result = $this->load->controller_model($this->moduleName)->checkPin($pin);
            if($result && !is_string($result)){
                $this->ajax_content(REQUEST_SUCCESS);
            }else{
                $this->ajax_content(REQUEST_FAIL,$result);
            }
        }else{
            $result = $this->load->controller_model($this->moduleName)->checkPin();
            if($result && !is_string($result)){
                $this->ajax_content(REQUEST_SUCCESS);
            }else{
                $this->ajax_content(REQUEST_FAIL,$result);
            }
        }
    }

    function clear_discount(){
        $sale = $this->session->userdata('sale');
        if($sale){
            if(isset($sale->voucher)){
                $sale->voucher = 0;
                $this->session->set_userdata('sale',$sale);
                return $this->ajax_content(REQUEST_SUCCESS);
            }
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Sale session is not available");
        }
    }

    function package_change(){
        $rowid = $this->input->post('rowid');
        $type =  $this->input->post('type');
        $package_id = $this->input->post('package_id');
        $row_values = $this->input->post('row_values');
        $this->load->controller_model($this->moduleName)->package_change($rowid,$package_id,$row_values,$type);
        $this->getList();
    }

    function payment_type_change(){
        $sale = $this->session->userdata('sale');
        if(isset($sale)){
            $sale->current_payment = $this->input->post('payment_id');
        }else{
            $sale = (object)array('current_payment' => $this->input->post('payment_id'));
        }
        $this->session->set_userdata('sale',$sale);
        $this->ajax_content(REQUEST_SUCCESS);
    }

}