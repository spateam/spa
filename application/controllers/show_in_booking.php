<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Show_in_booking extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "show_in_booking";
        $this->pageCode = "show_in_booking";
        $this->pageName = "show_in_booking";
        $this->breadcrumbs[] = array('url' => 'show_in_booking', 'text' => 'show_in_booking');
    }
}