<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "config";
        $this->pageCode = "config";
        $this->pageName = "Config";
        $this->breadcrumbs[] = array('url' => 'config', 'text' => 'Config');
    }

    public function index(){
        if($this->user_permission->check_level(Permission_Value::ADMIN) == false){
            echo "You don't have permission to access this content"; return;
        }

        $data = $this->load->controller_model('config')->getDataForView($this->user_check->get('login_as_type'));
        $data->pageCode = $this->pageCode;
        $this->page($this->moduleName, $data);
    }

    public function save(){
        $config_controller_model = $this->load->controller_model('config');
        $values = $this->input->post('value');
        $company_values = $this->input->post('company_data');
        $config_controller_model->update($values, null,$company_values);

        $data = $config_controller_model->getDataForView($this->user_check->get('login_as_type'));

        $data->messageBox = new stdClass();
        $data->messageBox->type = 'success';
        $data->messageBox->message = "success_save_config";

        $this->page($this->moduleName, $data);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */