<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Reports extends Admin_Controller
{

    private $reportFormDir;
    private $reportDetailDir;

    function __construct()
    {
        parent::__construct();
        $this->moduleName = "reports";
        $this->pageCode = "reports";
        $this->pageName = "Reports";
        $this->breadcrumbs[] = array('url' => 'admin/reports', 'text' => 'Reports');
        $this->reportFormDir = "forms/";
        $this->reportDetailDir = "details/";
        $this->reportExportDir = "export/";
    }

    function index()
    {
        if ($this->user_permission->checkPermission('v', $this->pageCode) == false) {
            return;
        }
        $data = new stdClass();
        $data->pageCode = $this->pageCode;
        $data->reports = $this->user_permission->get_available_report();

        $this->page($this->moduleName, $data);
    }

    public function load_report_form($name)
    {
        if ($this->user_permission->checkPermission('v', $this->pageCode) == false) {
            throw new Exception("You don't have this permission");
        }
        if ($this->user_permission->checkReportPermission($name) == false) {
            throw new Exception("You don't have this permission to view the report");
        }
        $report = REPORT($name);
        if(isset($report['condition_form']) && $report['condition_form'] == 0){
            $this->show_detail($name);
        }else{
            $this->pageName = $report['name'];
            $this->breadcrumbs[] = array('text' => $report['name']);
            $model = $this->load->controller_model($this->moduleName);
            $data = $model->get_report_form_data($name);
            $url = admin_url($this->moduleName . '/show_detail/' . $name);

            $this->page($this->moduleName . "/" . $this->reportFormDir . $name, array('url' => $url, 'data' => $data));
        }

    }

    public function show_detail($name,$format = "")
    {
        ini_set('memory_limit', '1024M');
        $this->load->library('excel');
        if ($this->user_permission->checkPermission('v', $this->pageCode) == false) {
            throw new Exception("You don't have this permission");
        }
        if ($this->user_permission->checkReportPermission($name) == false) {
            throw new Exception("You don't have this permission to view the report");
        }
        $report = REPORT($name);
        $post_data = $this->input->post();
        /* Set session for report */
        $data_input = $this->session->userdata('report');
        $data = new stdClass();
        $data->post_back = $post_data;
        if ($data_input == null) {
            $data_input = new stdClass();
        }
        if (isset($post_data['start_date'])) {
            $data_input->start_date = $post_data['start_date'];
            $data->start_date =$post_data['start_date'];
            if(isset($post_data['start_time'])){
                $time = $post_data['start_time'];
            }else{
                $time = " 00:00:00";
            }
            $post_data['start_date'] = get_database_date($post_data['start_date'] . " {$time}");
        }
        if (isset($post_data['end_date'])) {
            $data_input->end_date = $post_data['end_date'];
            $data->end_date = $post_data['end_date'];
            if(isset($post_data['end_time'])){
                $time = $post_data['end_time'];
            }else{
                $time = " 23:59:59";
            }
            $post_data['end_date'] = get_database_date($post_data['end_date'] . " {$time}");
        }

        if(isset($post_data['all_time'])){
            $data->start_time = 'All Time';
            $data->end_time = '';
        }

        $this->session->set_userdata('report', $data_input);
        $data->name = $report['name'];
        $data->key = $name;
        if(isset($post_data['branch_group_id']) && $post_data['branch_group_id'] != 0){
            $branch_groups = $this->load->table_model('branch_group')->getTableMap('','name',array('id' => $post_data['branch_group_id']));
            $data->branch_name = implode(' - ',$branch_groups);
        }
        else if(isset($post_data['branch_id'])){
            $branch_id = $this->user_check->get_branch_id($post_data['branch_id']);
            $branch = $this->load->table_model('branch')->get(array('id' => $branch_id));
            $branch = convert_to_array($branch,'','name');
            $data->branch_name = implode(' - ',$branch);
        }
        else{
            $data->branch_name = "";
        }

        if(isset($post_data['department_id']) && $post_data['department_id'] > 0) {
            $department = $this->load->table_model('department')->get(array('id' => $post_data['department_id']));
            $data->department_name = $department[0]->name;
        }
        else {
            $data->department_name = "";
        }

        $data_content = $this->load->controller_model($this->moduleName)->get_report_detail_data($name, $post_data);

        $this->load->library('export_original_data');

        if($format != ''){
            $data_array = $this->load->controller_model($this->moduleName)->to_original_format($name, $data_content, 'export');
            $this->export($name, $format, $data_array, $data);
        }else{
            $data_array = $this->load->controller_model($this->moduleName)->to_original_format($name, $data_content);
            $data_array= $this->export_original_data->parse_html($data_array);
            $data->data_content = $this->content($this->moduleName . "/" . $this->reportDetailDir . 'html_view', array('data_content' => $data_array), true);
            $this->page($this->moduleName . "/report_layout", $data, 'raw');
        }

    }

    public function export($name, $format, $data_content, $data){
        if(isset($format) && $format != ""){
            $export_data = $this->load->controller_model($this->moduleName)->get_report_export_data($name, $data_content,$data);
            $this->excel->setActiveSheetIndex(0)->fromArray($export_data);
//            $date = date("Y_m_d_H_i");

            $date = date('Y_m_d_H_i', strtotime(get_user_date("","","Y-m-d H:i:s")));
            // $report_name = $data->branch_name . '_' . $name . '_' . $date;
            $report_name = $name . '_' . $date;
            if($format == 'excel'){
                $report_name = $report_name . ".xlsx";
                $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
            }else{
                $report_name = $report_name . ".csv";
                $objWriter = new PHPExcel_Writer_CSV($this->excel);
                $objWriter->setDelimiter(",");
            }
            ob_start();
            $objWriter->save('php://output');
            $excelOutput = ob_get_clean();
            force_download($report_name,$excelOutput);
        }
    }
}