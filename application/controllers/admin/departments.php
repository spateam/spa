<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:53 PM
 */

class Departments extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "departments";
        $this->pageCode = "departments";
        $this->pageName = "Employee Role";
        $this->breadcrumbs[] = array('url' => 'admin/departments', 'text' => 'Employee Role');
    }

    function suggest_by_department_group(){
        $keyword = $this->input->get_post('term');
        /*$branch_group_id = $this->input->get_post('branch_group_id');
        if(!$branch_group_id){
            $branch_group_id = $this->user_check->get('branch_group_id');
        }*/
        $isMinimal = $this->input->get_post('minimal');
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion(trim($keyword));
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion(trim($keyword));
        }
        echo json_encode($suggest);
    }
}