<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit_Trails extends Admin_Controller {
    function __construct(){
        parent::__construct();
        $this->moduleName = "audit_trails";
        $this->pageCode = "audit_trails";
        $this->pageName = "Audit Trails";
        $this->breadcrumbs[] = array('url' => 'audit_trails', 'text' => 'Audit Trails');
    }

    function getData(){
        $data = $this->load->table_model('audit_trails')->getData();
        foreach($data as $key => $item){
            $data[$key]['createAt'] = get_user_date($item['createAt'], '', 'Y-m-d H:i:s');
        }
        echo json_encode(array('data' => $data));
    }
}