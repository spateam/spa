<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Warehouse extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "warehouse";
        $this->pageCode = "warehouse";
        $this->pageName = "Warehouse";
        $this->breadcrumbs[] = array('url' => 'admin/warehouse', 'text' => 'WareHouse');
    }

    function get_branch_have_warehouse(){
        $branch_id = $this->load->table_model('warehouse_branch')->get();
        $this->ajax_content(REQUEST_SUCCESS,'',convert_to_array($branch_id,'','branch_id'));
    }

    function get_warehouse_distribution($warehouse_id){
        $warehouse_item_data = $this->load->controller_model($this->moduleName)->get_warehouse_item_data($warehouse_id);
        $data = array('warehouse_id' => $warehouse_id, 'warehouse_item_data' => $warehouse_item_data);
        $content = $this->content($this->moduleName.'/warehouse_distribute_item',$data,true);
        $buttons = array(
            'success' => array(
                'text'   => 'Save',
                'url'    => site_url($this->dir . '/update_warehouse_distribution/'. $warehouse_id),
                'before' => 'before_post_warehouse_distribution',
                'class' => 'btn-primary',
                'close_modal' => 1
            ),
            'cancel' => array(
                'text'   => 'Close'
            )
        );
        $option = array(
            'button' => $buttons
        );
        $this->ajax_content(REQUEST_SUCCESS,'',array('content' => $content,'option' => $option));
    }

    function update_warehouse_distribution($warehouse_id){
        if($this->user_permission->checkPermission('e',$this->pageCode) == false){
            throw new Exception('You dont have permission for this action');
        }
        $data = $this->input->post('field_post');
        $this->load->controller_model($this->moduleName)->update_warehouse_distribution($warehouse_id,$data);
        return $this->ajax_content(REQUEST_SUCCESS,'Item was distributed succsesfully');
    }

    function suggest_item(){
        $term = $this->input->get_post('term',true);
        $id = $this->input->get_post('id',true);

        $list_data = $this->load->controller_model($this->moduleName)->suggest_item($term,$id);
        echo json_encode($list_data);
    }

}