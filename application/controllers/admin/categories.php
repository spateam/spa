<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Admin_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "categories";
        $this->pageCode = "categories";
        $this->pageName = "Categories";
        $this->breadcrumbs[] = array('url' => 'admin/categories', 'text' => 'Categories');
    }

    function getByID($id){
        return $this->ajax_content(REQUEST_SUCCESS, '', $this->load->table_model('category')->getByID($id));
    }

    function edit(){
        try{
            $id = $this->input->post('id');
            $fields = $this->input->post('field_post');
            $modelName = $this->load->controller_model($this->moduleName);
            if($id == 0){
                //Insert to Database
                if($this->user_permission->checkPermission('i',$this->pageCode) == false){
                    return;
                }
                $result = $modelName->insert($fields);
            }else{
                if($this->user_permission->checkPermission('e',$this->pageCode) == false){
                    return;
                }
                //Edit a record
                $result = $modelName->update($id,$fields);
            }
            $data = new stdClass();
            $data->option = array('close_modal' => 1);
            if(is_numeric($result) || $result === true){
                $branch_id = $fields['branch_id'];
                $get_cat_branch = $this->load->table_model('category_booking')->Remove_Cat_Branch($id,$branch_id);
                return $this->ajax_content(REQUEST_SUCCESS,'The record has been saved', $data);
            }else{
                return $this->ajax_content(REQUEST_FAIL, $result, $data);
            }
        }catch(Exception $e){
            return $this->ajax_content(REQUEST_FAIL,$e->getMessage());
        }

    }

}