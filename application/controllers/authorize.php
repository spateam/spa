<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authorize extends Staff_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->moduleName = "authorize";
    }

    public function index($branch_group_id = 0)
    {
        /*
         * Check login
         */
        $User = checkLoggedInWithoutRedirect();
        if ($User != false) {
            redirect(site_url('home'));
        }

        $branch_model = $this->load->table_model('branch');
        if ($branch_group_id == 0) {
            $branch_groups = $branch_model->select(
                array(
                    'select' => '*',
                    'from' =>
                        array(
                            'branch_group' => array('table' => 'branch_group'))
                ))->result();
            $data['branch_groups'] = $branch_groups;
            $data['branches'] = $branch_model->get(array('branch_group_id' => $branch_groups[0]->id));
        } else {
            $data['branches'] = $branch_model->get(array('branch_group_id' => $branch_group_id));
        }

        $this->session->unset_userdata('login');
        $this->page($this->moduleName, $data, 'pos_login');

    }

    function getBranches()
    {
        $branch_group_id = $this->input->post('branch_group_id');
        $branch_groups = array();

        if ($branch_group_id != 0) {
            $branch_groups = $this->load->table_model('branch_group')->get_branch_and_branch_group($branch_group_id);
        }
        $this->ajax_content(REQUEST_SUCCESS,'',$branch_groups);
    }

    public function login()
    {
        $username = trim($this->input->post("Username"));
        $password = $this->input->post("password");
        $branch_id = $this->input->post("branch_id");

        $employees_model = $this->load->table_model('employee');
        $branch_model = $this->load->table_model('branch');
        $employees = $employees_model->get(array(
            'username' => $username,
            'password' => md5($password),
        ));

        if (count($employees)) {
            $branch = $branch_model->getByID($branch_id);
            $employee = $employees[0];
            //check branch
            $arrPermission  = array(
                'branch_group_id' => $branch->branch_group_id,
                'branch_id' => $branch_id
            );
            $employee_temp = $employees_model->getByID($employee->id, $arrPermission, $employee->type);
                if ($employee_temp !== null && ($employee_temp->type == Permission_Value::ADMIN || (isset($employee_temp->employee_permission) && $employee_temp->employee_permission) ||
                    (isset($employee_temp->global) && $employee_temp->global))) {
                $department_group_model = $this->load->table_model('department_group');
                $groups = $department_group_model->get_group(array('department_id' => $employee->department_id));
                $branch = $this->load->table_model('branch')->get(array('id' => $branch_id))[0];
                $userSession = new stdClass();
                $userSession->user_id = $employee->id;
                $userSession->Username = $employee->username;
                $userSession->Email = $employee->email;
                $userSession->FirstName = $employee->first_name;
                $userSession->LastName = $employee->last_name;
                $userSession->Phone = $employee->phone_number;
                $userSession->branch_id = $branch_id;
                $userSession->branch_name = $branch->name;
                $userSession->branch_group_id = $branch->branch_group_id;
                $userSession->type = $employee->type;
                $userSession->login_as_type = $employee->type > 2 ? 2 : $employee->type;
                $userSession->group_id = $groups;
                $this->session->set_userdata('login', $userSession);
                $userSession->timezone = $this->system_config->get('timezone');
                $userSession->permissions = $this->user_permission->getUserPermissions();
                $this->session->set_userdata('login',$userSession);
                $this->ajax_content(REQUEST_SUCCESS,"",1);
            } else {
                $User = new stdClass();
                $User->login_as_type = 4;
                $this->session->set_userdata('login', $User);
                $this->ajax_content(REQUEST_SUCCESS,"","You do not have permission on this branch");
                exit;
            }
        } else {
            $this->ajax_content(REQUEST_SUCCESS,"","Wrong Username or Password!");
            exit;
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('sale');
        redirect(site_url());
    }

}