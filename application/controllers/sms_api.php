<?php

class SMS_API extends POS_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    // SEND SMS REMINDER TO BOOKING CUSTOMER BEFORE 1 DAY
    public function sendSMSReminder(){
        $this->load->library('Smslib');
        $this->smslib->sendSMSReminder();
    }
}
