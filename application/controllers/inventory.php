<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "Inventory";
        $this->pageName = "Inventory";
        $this->breadcrumbs[] = array('url' => 'inventory', 'text' => 'Inventory');
    }

    public function index()
    {
        $menus_model = $this->load->table_model('menu');
        $menu = $menus_model->getMenuToShow(array('title' => $this->moduleName));
        if(count($menu)){
            $menu = $menu[0];
            $childMenus = $menus_model->getMenuToShow(array('parent_id' => $menu->id));
            redirect($childMenus[0]->link);
        }
    }

}