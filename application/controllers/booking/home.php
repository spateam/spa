<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "book";
        redirect('/booking/book');
    }
}