<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "product";
        $this->breadcrumbs[] = array('url' => 'category', 'text' => 'Category');
    }

    function index(){
        $code = $this->input->get('code');
        if($code == false){
            $code = '';
        }
        $model = $this->load->table_model('item_model');
        $item = $model->get(array('code' => $code))[0];
        $category = $model->select(array(
            'select' => array('category'=>array('id','code','name')),
            'from'   => array(
                'category'      => array('table'=>'category'),
                'category_item' => array('table'=>'category_item','condition'=>'category_item.category_id = category.id'),
                'item'          => array('table'=>'item','condition'=>'item.id = category_item.item_id')
            ),
            'where' => array('item.id' => $item->id)
        ))->result()[0];
        $items_related = $model->select(array(
                'select' => array('item'=>array('code','name','price','description')),
                'from'   => array(
                    'category'      => array('table'=>'category'),
                    'category_item' => array('table'=>'category_item','condition'=>'category_item.category_id = category.id'),
                    'item'          => array('table'=>'item','condition'=>'item.id = category_item.item_id')
                ),
                'where' => array('category.id' => $category->id)
            ))->result();

        $this->breadcrumbs[] = array('url' => "category?code={$category->code}", 'text' => $category->name);
        $this->breadcrumbs[] = array('url' => "product?code={$item->code}", 'text' => $item->name);

        $data = array();
        $data['item'] = $item;
        $data['category'] = $category;
        $data['items_related'] = $items_related;
        $this->page('product',$data);
    }

}