<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_Reviews extends Booking_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "customer_reviews";
        $this->breadcrumbs[] = array('url' => 'customer_reviews', 'text' => 'customer_reviews');
    }

    function index(){
        $customerId = trim($this->input->get('cid'));
        $billId = trim($this->input->get('bid'));
        $customerInfo = $this->load->table_model('customer')->get(array('id' => $customerId));
        if(count($customerInfo) > 0){
            $customerInfo = $customerInfo[0];
        }
        else{
            $customerInfo = array();
        }
        $branchInfo = $this->load->table_model('bill_branch')->getBranchIdNumber($billId);
        $this->page('customer_reviews/customer_reviews',array('branchId' => $branchInfo, 'customerInfo' => $customerInfo, 'billId' => $billId));
    }

    function save_customer_reviews(){
        $data = $this->input->get_post('postData',true);
        foreach($data as $key => $item){
            $data[$key] = trim($item);
        }
        $timezone = $this->system_config->get('timezone', array());
        $data['createAt'] = get_database_date(date('Y-m-d H:i:s', strtotime('NOW')),$timezone);
        $this->load->table_model('customer_reviews')->insert($data);
        $this->ajax_content(TRUE);
    }
}