<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_Validation extends Booking_Controller {

    function __construct(){
        parent::__construct();
    }

    /**
     * Function to unsubscribe the email sending to customer. (currently only work on the review email)
     */
    function unsubscribe($customerId,$hashKey)
    {
        // check customer exist and unsubscribe or not
        $customer = $this->load->table_model('customer')->getById($customerId);

        // setup view name to load to front-end
        $viewName = 'notify/unsubscribe';
        if (empty($customer)) {
            $this->page($viewName, array('error_msg' => 'Customer does not exist in our system, pls contact us for more information.'));
            return;
        }

        // check hashKey
        $compareHashKey = md5($customerId . $customer->code);
        if($hashKey !== $compareHashKey){
            $this->page($viewName, array('error_msg' => 'HashKey is not match, please contact us for more information.'));
            return;
        }

        // pass all the validation, processing to update subscibe status of customer.
        $this->load->table_model('customer')->update($customerId, array('is_subscribe' => 0));
        $this->page($viewName, array('error_msg' => 'Unsubscribe successful.'));
        return;
    }
}