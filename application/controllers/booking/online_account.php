<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_Account extends Booking_Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $logged_user = $this->session->userdata('login');
        $logged_user = $this->db->get_where('customer', array('id' => $logged_user->id))->row();
        $nextAppt = $this->load->controller_model('online_account')->getNextAppt($logged_user->id);
        if($logged_user){
            $this->page('online_account/index',array('data' => $logged_user,'nextApp' => $nextAppt));
        }
        else{
            redirect(base_url().'booking/book');
        }
    }

    function loadSectionBookingData(){
        $customerData = $this->session->userdata('login');
        $customerData = $this->db->get_where('customer', array('id' => $customerData->id))->row();
        $data = $this->load->controller_model('online_account')->getSectionBookingData(array('id' => $customerData->id));

        foreach($data as $key => $item){
            $item['start_time'] = get_user_date($item['start_time'], '', 'd-m-Y H:i');
            $item['allow_edit'] = 0;
            $data[$key] = $item;
            if($item['status'] == 0){
                $data[$key]['status'] = 'Re-Scheduled';
            }
            elseif($item['status'] == 1){
                $data[$key]['status'] = 'Confirmed';
                if(strtotime(get_user_date(date('d-m-Y H:i'), '', 'd-m-Y H:i')) < strtotime(get_user_date($item['start_time'],'','d-m-Y H:i'))){
                    $data[$key]['allow_edit'] = 1;
                }
            }
            elseif($item['status'] == 2){
                $data[$key]['status'] = 'Not Confirmed';
            }
            elseif($item['status'] == 3){
                $data[$key]['status'] = 'Cancelled';
            }
            elseif($item['status'] == 8){
                $data[$key]['status'] = 'Payment';
            }
            elseif($item['status'] == 9){
                $data[$key]['status'] = 'Hold Bill';
            }
        }
        echo json_encode(array('data' => $data));
    }

    function loadSectionCreditHistoryData(){
        $pin = $this->input->get_post('pin',true);

        $customerData = $this->session->userdata('login');
        $customerData = $this->db->get_where('customer', array('id' => $customerData->id))->row();
        if(empty($customerData->pin)){
            echo json_encode(array('status' => FALSE, 'message' => 'Your NRIC or PIN was not exist in our system, please contact us or visit one of our branches for more info'));
            exit;
        }

        if(empty($pin)){
            echo json_encode(array('status' => FALSE, 'message' => 'Your NRIC or PIN is empty, please key in your PIN.'));
            exit;
        }

        if($customerData->pin != $pin && $customerData->nric != $pin){
            echo json_encode(array('status' => FALSE, 'message' => 'The NRIC or Pin that you have entered is incorrect, please contact us or visit one of our branches for more info'));
            exit;
        }

        $data = new stdClass();
        $data->customer_name = to_full_name($customerData->first_name, $customerData->last_name);
        $data->customer_code = $customerData->code;
        $data->customer_credit = $this->load->controller_model('customers')->getCustomerCreditHistory($customerData->id);
        $data->content = $this->content('customers/online_account_credit_history', $data, true);

        echo json_encode(array('status' => TRUE, 'message' => $data->content));
    }

}