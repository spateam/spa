<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:53 PM
 */

class Employees extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "employees";
        $this->pageCode = "employees";
        $this->pageName = "Employees";
        $this->breadcrumbs[] = array('url' => 'employees', 'text' => 'Employees');
    }

    function insert_to_branch(){
        $user = $this->session->userdata('login');
        $employee_id = $this->input->post('id');
        $branch_id = $user->branch_id;

        $result = $this->load->controller_model('employees')->insert_to_branch($employee_id);
        if(is_numeric($result)){
            $result = $this->send_transfer_branch_mail($employee_id, $branch_id);
        }
        if($result == 1){
            $this->ajax_content(REQUEST_SUCCESS,'The email has been sent to the employee');
        }else{
            $this->ajax_content(REQUEST_FAIL, $result);
        }

}

    function send_transfer_branch_mail($employee_id, $branch_id){

        $data = new stdClass();
        $data->employee = $this->load->table_model('employee')->get(array('id' => $employee_id))[0];
        $data->branch = $this->load->table_model('branch')->get(array('id' => $branch_id))[0];
        $data->com_name = $this->load->table_model('config')->get(array('code'=>'com_name'))[0]->value;
        if(isset($data->employee) && $data->employee !== null) {
            $to = $data->employee->email;
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "{$data->com_name}.You have been added to a branch.";
                $body = $this->load->email_content('employee_to_branch', $data);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return 1;
                }
            }
        }
        return "Fail to send mail";
    }

    function delete(){
        if($this->user_permission->checkPermission('d',$this->pageCode) == false){
            return;
        }
        $user = $this->session->userdata('login');
        $branch_id = $user->branch_id;
        $ids = $this->input->post('ids');
        $ids = json_decode($ids);

        $modelName = $this->load->controller_model($this->moduleName);

        $result = $modelName->delete($ids);
        if($result){
            foreach($ids as $id){
                $this->send_employee_remove_email($id,$branch_id);
            }

        }
        if(is_numeric($result) || $result === true){
            $this->ajax_content(REQUEST_SUCCESS,'The record has been deleted', array());
        }else{
            $this->ajax_content(REQUEST_FAIL,$result, array());
        }
    }

    function send_employee_remove_email($employee_id,$branch_id){
        $data = new stdClass();
        $user = $this->session->userdata('login');
        $data->employee = $this->load->table_model('employee')->get(array('id' => $employee_id))[0];
        $data->branch = $this->load->table_model('branch')->get(array('id' => $user->branch_id))[0];
        $data->com_name = $this->load->table_model('config')->get(array('code'=>'com_name'))[0]->value;
        if(isset($data->employee) && $data->employee !== null) {
            $to = $data->employee->email;
            if (isset($to) && $to != "") {
                $this->load->library('email');
                $this->email->clear(true);
                $subject = "{$data->com_name}.You have been remove from {$data->branch->name} branch.";
                $body = $this->load->email_content('employee_out_branch', $data);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    return 1;
                }
            }
        }
        return "Fail to send mail";
    }

    function get_calendar($id = 0){
        $user = $this->session->userdata('login');
        $custom_string = "";
        $disabled_dates = "";
        $data = $this->load->controller_model($this->moduleName)->getDataForCalendar($id);
        $data->id = $id;
        $days = $this->load->table_model('employee_calendar_customize')->get(array('employee_id' => $id,'branch_id' => $user->branch_id));
        if(count($days)){
            foreach($days as $day){
                $dayofweek = date('w', strtotime(trim($day->date)));
                if(isset($data->item[$dayofweek])){
                    $custom_string .= $day->date."_".$day->start_time."_".$day->end_time."_".$day->working."|";
                }
            }
        }
        for($i=0;$i<7;$i++){
            if(!isset($data->item[$i]))
                $disabled_dates .= $i.",";
        }
        $data->custom_string = $custom_string;
        $data->disabled_dates = $disabled_dates;
        $this->ajax_content(REQUEST_SUCCESS,'',array('content' => $this->content('employees/calendar',$data,true)));
    }

    function get_absent(){
        $id     = $this->input->post('id');
        $date   = $this->input->post('date');
        $type   = $this->input->post('type');

        if(!isset($type) || !$type){
            $type = 6;
        }
        if(!isset($date) || !$date){
            $date = date('Y-m-d H:i:s');
        }
        $default_date = array();
        if($date){
            $date = date('Y-m-d H:00:00',strtotime($date));
            $start = new DateTime($date);
            $end = new DateTime($date);
            $end = $end->add(new DateInterval('PT1H'));

            $default_date = array(
                'from_date_time'  => $start->format('Y-m-d H:i:s'),
                'to_date_time'    => $end->format('Y-m-d H:i:s'),
            );
            if($type){
                if(! is_numeric($type)){
                    $type = BOOKING_STATUS($type);
                }
                $default_date['type'] = $type;
            }
            $absent_type = BOOKING_STATUS();
            $filter_array = array(5,6,7);
            foreach($absent_type as $key=>$value){
                if(!in_array($key, $filter_array))
                    unset($absent_type[$key]);
            }
            $default_date['absent_type'] = $absent_type;
        }
        $default_date['absent_type'] = $absent_type;
        $data = (object) ($this->load->controller_model($this->moduleName)->getDataForAbsent($id));
        $data->id = $id;
        $data->default_date = $default_date;
        $data->start_time = $date;
        $ret = new stdClass();
        $ret->content = $this->content('employees/absent',(array)$data,true);
        $ret->option = array(
            'title' => "Block Dates"
        );
        if($date) {
            if ($default_date['type'] == 6)
                $ret->option = array(
                    'title' => "Away Dates"
                );
            if ($default_date['type'] == 7)
                $ret->option = array(
                    'title' => "Offline Dates"
                );
        }
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function get_current_absent(){
        $id     = $this->input->post('id');
        $data = ($this->load->controller_model($this->moduleName)->getCurrentAbsent($id));
        return $this->ajax_content(REQUEST_SUCCESS, "", $data);
    }

    function change_calendar($id = 0){
        $data = $this->input->post('calendar');
        $custom_days = $this->input->post('custom_days');
        if(!empty($data)){
            foreach($data as $item){
                $regex = '~^[0-9]{2}:[0-9]{2}$~';
                if(!preg_match($regex,$item)){
                    $this->ajax_content(REQUEST_FAIL,'Please check the input format time. "'.$item.'" is not a correct format.');
                    exit;
                }
            }
        }
        $check_exist_appointment = $this->load->controller_model($this->moduleName)->checkEmployeeAppointment($id,$data,$custom_days);
        if(!is_string($check_exist_appointment)){
            $autoFixEmployeeTime = $this->load->controller_model($this->moduleName)->autoFixEmployeeTime($id);

            $result = $this->load->controller_model($this->moduleName)->changeCalendar($id,$data,$custom_days);
            if(is_numeric($result) || $result == true){
                $this->ajax_content(REQUEST_SUCCESS,'The calendar has been updated');
            }
        }
        else{
            $this->ajax_content(REQUEST_FAIL,$check_exist_appointment);
            exit;
        }
    }

    function change_absent(){
        $data = (object) $this->input->post('data');
        $id = $this->input->post('id');
        $result = $this->load->controller_model($this->moduleName)->changeAbsent($id,$data);
        if($result){
            return $this->ajax_content(REQUEST_SUCCESS);
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Can't save absent");
        }
    }

    function delete_absent_date(){
        $id     =   $this->input->post('id');
        $result = $this->load->controller_model($this->moduleName)->deleteAbsentDate($id);
        if($result){
            return $this->ajax_content(REQUEST_SUCCESS);
        }else{
            return $this->ajax_content(REQUEST_FAIL, "Can't delete absent");
        }
    }

    function suggest_therapist(){
        $keyword = $this->input->get_post('term');
        $isMinimal  = to_b($this->input->get_post('minimal'));
        $isFull     = to_b($this->input->get_post('full'));
        $isAll      = to_b($this->input->get_post('all'));
        $isExtend   = to_b($this->input->get_post('extend'));
        $condition = array('department_id' => $this->load->table_model('department')->get_department_serve_booking());
        if(isset($isMinimal) && $isMinimal){
            $suggest = $this->load->controller_model($this->moduleName)->getMinimalSuggestion($keyword,$condition,$isFull,$isExtend,$isAll);
        }else{
            $suggest = $this->load->controller_model($this->moduleName)->getSuggestion($keyword, $condition,$isFull,$isExtend,$isAll);
        }
        echo json_encode($suggest);
    }

    function suggest_absent_type(){
        $absent_type = BOOKING_STATUS();
        $suggest = array();
        foreach($absent_type as $value=>$label){
            if($value == 5 || $value == 6 || $value == 7){
                $suggest[] = array(
                    'label' => $label,
                    'value' => $value
                );
            }
        }
        echo json_encode($suggest);
    }

    function get_assigned_service_form($id){
        $data = (object)$this->load->controller_model($this->moduleName)->get_assigned_service_form_data($id);
        $data->id = $id;
        $ret = new stdClass();
        $ret->content = $this->content('employees/assigned_service',(array)$data,true);
        $ret->option = array(
            'title' => "Assigned Service"
        );
        $buttons = array(
            'success' => array(
                'url'           => site_url($this->dir . '/save_assigned_service/' . $id),
                'text'          => 'Save',
                'class'         => "btn-primary",
                'before'        => 'get_assigned_service_before_edit',
                'close_modal'   => 1
            ),
            'cancel' => array(
                'text'  => 'Cancel'
            )
        );
        $ret->option['button'] = $buttons;
        return $this->ajax_content(REQUEST_SUCCESS, "", $ret);
    }

    function save_assigned_service($employee_id){
        $service_ids = $this->input->post('service_id');
        $this->load->controller_model($this->moduleName)->assign_service($employee_id,$service_ids);
        $this->ajax_content(REQUEST_SUCCESS,'Successfully assign services');
    }
}