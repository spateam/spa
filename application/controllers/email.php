<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {
    function __construct()
    {
        parent::__construct();
    }

    function sendEmailBirthday()
    {
        $customers = $this->load->table_model('customer')->getCustomerBirthdays();
        if (empty($customers)) {
            return false;
        }
        $this->load->library('email');
        foreach ($customers as $customer)
        {
            if($customer->branch_group_name == 'Healing Touch Global'){
                $from = 'yishun@healingtouchspa.com';
            }
            elseif($customer->branch_group_name == 'Healing Touch'){
                $from = 'tampines@healingtouchspa.com';
            }
            elseif($customer->branch_group_name == 'Spa Affinity Pte Ltd'){
                $from = 'orchard@healingtouchspa.com';
            }
            else{
                $from = 'tampines@healingtouchspa.com';
            }
            $emailto = strtolower(trim($customer->email));
            if (isset($emailto) && $emailto != "") {
                $this->email->clear(true);
                $this->email->set_newline("\r\n");
                $this->email->from($from, "Healing Touch Spa");
                $this->email->to($emailto);
                $this->email->subject($customer->branch_group_name.' Birthday Discount E-Voucher');
                $body = $this->load->email_content('customer_birthday', $customer);
                $this->email->message($body);

                if(!$this->email->send()){
                    echo $this->email->print_debugger();
                }
            }
            exit;
        }
    }

    function sendEmailExpiryThreeMonths()
    {
        $customers = $this->load->table_model('customer')->getCustomerExpiryThreeMonths();
        if (empty($customers)) {
            return false;
        }

        $this->load->library('email');
        foreach ($customers as $customer)
        {
            $emailto = strtolower(trim($customer->email));
            if (isset($emailto) && $emailto != "") {
                $this->email->clear(true);
                $this->email->set_newline("\r\n");
                $this->email->from('noreply@healingtouchspa.com',"Healing Touch Spa");
                $this->email->to($emailto);
                $this->email->subject('Expiry Package 3 Months');
                $this->email->message('This is test Expiry Package alert for 3 months to go... ' . $customer->first_name . ' .');
                if($this->email->send()){
                    $this->load->table_model('credit')->updateEmailExpiryThreeMonths($customer->credit_id);
                }
            }
        }
    }

    function sendEmailExpiryOneMonth()
    {
        $customers = $this->load->table_model('customer')->getCustomerExpiryOneMonth();
        if (empty($customers)) {
            return false;
        }
        $this->load->library('email');
        foreach ($customers as $customer)
        {
            $emailto = strtolower(trim($customer->email));
            if (isset($emailto) && $emailto != "") {
                $this->email->clear(true);
                $this->email->set_newline("\r\n");
                $this->email->from('noreply@healingtouchspa.com',"Healing Touch Spa");
                $this->email->to($emailto);
                $this->email->subject('Expiry Package');
                $this->email->message('This is test Expiry Package for one month ' . $customer->first_name . '... .');
                if($this->email->send()){
                    $this->load->table_model('credit')->updateEmailExpiryOneMonth($customer->credit_id);
                }
            }
        }
    }

    function sendEmailInviteReview()
    {
        $customers = $this->load->table_model('customer')->sendEmailInviteReview();
        if (empty($customers)) {
            return false;
        }

        $this->load->library('email');
        foreach ($customers as $customer)
        {
            $customer->created_date = get_user_date($customer->created_date,'','Y-m-d');
            $branch_info = $this->load->table_model('branch')->get(array('name' => $customer->branch_name));
            $emailto = strtolower(trim($customer['email']));
            if (isset($emailto) && $emailto != "") {
                $this->email->clear(true);
                $this->email->set_newline("\r\n");
                $this->email->from($branch_info[0]->email,"Healing Touch Spa");
                $this->email->to($emailto);
                $this->email->subject('Please Feedback or Review '.$customer->branch_group_name.','.$customer->branch_name);
                $body = $this->load->email_content('customer_reviews', $customer);
                $this->email->message($body);
                if($this->email->send()){
                    $this->load->table_model('bill')->updateEmailReviewBill($customer->bid);
                }
                else{
                    echo $this->email->print_debugger();
                }
            }
        }
    }

    function sendEmailReminder12()
    {
        $customers = $this->load->table_model('customer')->getCustomerListReminder12();

        if (empty($customers)) {
            return false;
        }
        $this->load->library('email');

        foreach ($customers as $customer)
        {
            $emailto = strtolower(trim($customer->email));
            if (isset($emailto) && $emailto != "") {
                $this->email->clear(true);
                $this->email->set_newline("\r\n");
                $this->email->from('noreply@healingtouchspa.com',"Healing Touch Spa");
                $this->email->to($emailto);
                $this->email->subject('notification before 12h');
                $this->email->message('This is notification to customers before 12h of customer : ' . $customer->first_name . '... .');
                if($this->email->send()){
                    // send sms also
                    $this->load->table_model('booking')->updateEmailReminder12($customer->booking_id);
                }
            }
        }
    }
}