<?php

class API extends POS_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    function sql_creator_history_bill($customer_id = 760){
        echo $this->load->controller_model('customers','staff')->sql_creator(array(
            'select' => array(
                'bill'                  => array('bill_code' => 'code', 'bill_created_date' => 'created_date'),
                'item'                  => array('item_name' => 'name'),
                'employee'              => array('employee_first_name' => 'first_name','employee_last_name' => 'last_name'),
                'bill_item_credit'      => array('credit_used' => 'credit_value'),
                'credit'                => array('credit_id' => 'id')
//                'payment_type'      => array('payment_name' => 'name'),
//                'bill_payment' => array('payment_amount' => 'amount')
            ),
            'from'   => array(
                'bill'              => array('table' => 'bill'),
                'bill_item'         => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id'),
                'item'              => array('table' => 'item', 'condition' => 'item.id = bill_item.item_id'),
                'bill_item_credit'  => array('table' => 'bill_item_credit', 'condition' => 'bill_item_credit.bill_item_id = bill_item.id', 'type' => 'left'),
                'bill_employee'     => array('table' => 'bill_employee', 'condition' => 'bill_employee.bill_item_id = bill_item.id'),
                'employee'          => array('table' => 'employee', 'condition' => 'bill_employee.employee_id = employee.id'),
                'credit'            => array('table' => 'bill_item_credit', 'condition' => 'credit.id = bill_item_credit.credit_id', 'type' => 'left'),
//                'bill_payment'      => array('table' => 'bill_payment', 'condition' => 'bill_payment.bill_id = bill.id'),
//                'payment_type'           => array('table' => 'payment_type', 'condition' => 'payment_type.id = bill_payment.payment_id')
            ),
            'where' => array(
                'bill.customer_id' => $customer_id,
            ),
            'group' => 'bill.id,bill_item.id,bill_employee.employee_id',
            'order' => 'bill.created_date desc',
        ));
    }

    public function navigator($cliAction = '')
    {
        $returnValues = array();

        $action = "";
        if ($cliAction != "") {
            $action = $cliAction;
        } else if ($this->input->post('action', true)) {
            $action = $this->input->post('action', true);
        }

        if ($action !== "") {
            switch ($action) {
                case 'customer':
                    $this->second_branch_customer();
                    return;
                    break;
                case 'import_customer_restriction':
                    $this->import_customer_promotion_restriction();
                    return;
                    break;
                case 'import_category':
                    $this->import_category_by_excel();
                    return;
                    break;
                case 'import_item':
                    $this->import_item_by_excel();
                    return;
                    break;
                case 'import_commission':
                    $this->import_commission_by_excel();
                    return;
                    break;
                case 'backup_db':
                    $this->backup_db('127.0.0.1','root','','pos2');
                    return;
                    break;
                case 'enable_all_employee_working_hour':
                    $this->enable_all_employee_working_hour();
                    return;
                    break;
                default :
                    $returnValues["status"] = REQUEST_FAIL;
                    $returnValues["message"] = "Unsupported action";
                    break;
            }
        } else {
            $returnValues["status"] = REQUEST_FAIL;
            $returnValues["message"] = "Unsupported Method";
        }
        echo(json_encode($returnValues));
    }

    private function backup_db($host,$user,$password,$database){
        try{
            $command = "C://xampp/mysql/bin/mysqldump.exe -h$host -u$user ". (($password)?"-p$password":'') ." $database > database/".date('YmdHis').".sql";
            $result = exec("$command");
            echo "The database has been backup";
        }
        catch(Exception $e){
            echo $e->getMessage();
        }
    }

    private function second_branch_customer(){
        $branch_id = 2;
        $branch_group_id = 2;
        $customer_model = $this->load->table_model('customer');
        $item_model = $this->load->table_model('item');
        $customer_model->setFieldListRequirement(array());
        set_time_limit(0);
        $db = mysqli_connect("gentospa.clypmak1usxf.ap-southeast-1.rds.amazonaws.com", "gentospa", "303gentospa?", "basicposdb");
        $sql_member = "SELECT
	memberid AS `code`,
	`name` AS first_name,
nric,
pwd as pin,
phone1 as phone_number,
hphone as mobile_number,
email,
addr1 as address,
DATE(dob) as birthday,
IF(sex IN ('M', 'm'), 1, IF(sex IN ('F', 'f'), 2, null)) as gender,
promogroup,
(point + pointbf) as credit,
point,
pointbf,
expdate as end_date
FROM
	member
WHERE (point+pointbf) > 0";
        $sql_member .= " AND (promogroup = '' OR promogroup IS NULL)";
        $query = $db->query($sql_member);
        $ret = array();
        $customer_map = $customer_model->getTableMap('code');
        $item_convert_map = array(
            '' => 31,
            'body' => 94,
            'slimming' => 98,
            'slimmin' => 98,
            'beauty' => 96,
            'sfacial' => 97
        );
        foreach($item_convert_map as $key => $value){
            $item_by_code = $item_model->get(array('code' => $value));
            if(count($item_by_code) > 0){
                $item_id = $item_by_code[0]->id;
                $item_convert_map[$key] = $item_id;
            }
        }
        $package_exists = array();
        $counter = 0;
        while($row = $query->fetch_assoc()){
            $code = $row['code'];
            echo "Start import $code <br/>";
            if($row['credit'] > 0 && !isset($customer_map[$code])){
                $promo_group = trim(strtolower($row['promogroup']));
                if(isset($item_convert_map[$promo_group]) && $item_convert_map[$promo_group] != 0){
                    $first_name = isset($row['first_name']) ? $row['first_name'] : "";
                    $nric = $row['nric'];
                    $pin = $row['pin'];
                    $phone_number = $row['phone_number'];
                    $mobile_number = $row['mobile_number'];
                    $email = $row['email'];
                    $address = isset($row['address']) ? $row['address'] : "";
                    $birthday = $row['birthday'];
                    $gender = isset($row['gender']) ? $row['gender'] : 'null';

                    $item_id = $item_convert_map[$promo_group];
                    $start_date = get_database_date();
                    $end_date = $row['end_date'];
                    $credit = $row['credit'];
                    $credit_original = $row['credit'];
                    $customer_data = array(
                        'code' => $code,
                        'first_name' => $first_name,
                        'nric' => $nric,
                        'pin' => $pin,
                        'phone_number' => $phone_number,
                        'mobile_number' => $mobile_number,
                        'email' => $email,
                        'address' => $address,
                        'birthday' => $birthday,
                        'gender' => $gender,
                        'customer_type' => 2
                    );
                    $customer_id = $customer_model->insert($customer_data, 1, array('branch_group_id' => $branch_group_id));
                    $credit_data = array(
                        'customer_id' => $customer_id,
                        'item_id' => $item_id,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'credit' => $credit,
                        'credit_original' => $credit_original
                    );
                    $credit_id = $this->load->table_model('credit')->insert($credit_data, 1, array('branch_group_id' => $branch_group_id));
                    ++$counter;
                }
                if(isset($package_exists[$promo_group])){
                    $package_exists[$promo_group]++;
                }else{
                    $package_exists[$promo_group] = 1;
                }
                echo "Imported $code <br/>";
            }
            echo "End import $code <br/><br/>";
        }
        echo "<br/><br/><br/><br/>";
        echo $counter;
//        foreach($package_exists as $package => $num_c){
//            echo $package . '('. $num_c .")<br/>";
//        }
        return $ret;
    }

    function import_guest_customer(){
        $branch_id = 2;
        $branch_group_id = 2;
        $customer_model = $this->load->table_model('customer');
        $item_model = $this->load->table_model('item');
        $customer_model->setFieldListRequirement(array());
        set_time_limit(0);
        $db = mysqli_connect("gentospa.clypmak1usxf.ap-southeast-1.rds.amazonaws.com", "gentospa", "303gentospa?", "basicposdb");
        $sql_member = "SELECT
	memberid AS `code`,
	`name` AS first_name,
nric,
pwd as pin,
phone1 as phone_number,
hphone as mobile_number,
email,
addr1 as address,
DATE(dob) as birthday,
IF(sex IN ('M', 'm'), 1, IF(sex IN ('F', 'f'), 2, null)) as gender,
expdate as end_date
FROM
	member
WHERE (point+pointbf) = 0  AND (promogroup != '' AND promogroup IS NOT NULL) AND (nric != '' AND nric IS NOT NULL)";
        $query = $db->query($sql_member);
        $ret = array();
        $customers          = $customer_model->get(array("nric is not null and nric != ''"));
        $customer_map       = convert_to_array($customers,'code');
        $customer_nric_map  = convert_to_array($customers,'nric');
        $counter = 0;
        while($row = $query->fetch_assoc()){
            $code = $row['code'];
            echo "Start import $code <br/>";
            if(!isset($customer_map[$code]) && !isset($customer_nric_map[$row['nric']])){
                $first_name = isset($row['first_name']) ? $row['first_name'] : "";
                $nric = $row['nric'];
                $pin = $row['pin'];
                $phone_number = $row['phone_number'];
                $mobile_number = $row['mobile_number'];
                $email = $row['email'];
                $address = isset($row['address']) ? $row['address'] : "";
                $birthday = $row['birthday'];
                $gender = isset($row['gender']) ? $row['gender'] : 'null';
                $customer_data = array(
                    'code' => $code,
                    'first_name' => $first_name,
                    'nric' => $nric,
                    'pin' => $pin,
                    'phone_number' => $phone_number,
                    'mobile_number' => $mobile_number,
                    'email' => $email,
                    'address' => $address,
                    'birthday' => $birthday,
                    'gender' => $gender,
                    'customer_type' => 1
                );
                $customer_id = $customer_model->insert($customer_data, 1, array('branch_group_id' => $branch_group_id));
                $customer_map['nric'] = true;
                ++$counter;
                echo "Imported $code <br/>";
            }
            echo "End import $code <br/><br/>";
        }
        echo "<br/><br/><br/><br/>";
        echo $counter;
        return $ret;
    }

    function import_category_by_excel(){
        $category_model = $this->load->table_model('category');
        $category_model->setFieldListRequirement(array());
        $file = FCPATH."import/import_category.xlsx";
        $data = $this->get_data_from_excel($file);
        foreach($data as $i => $row){
            $insert_data = array();
            $code = $row['code'];
            $categoryByCode = $category_model->get(array('code' => $code));
            if(isset($categoryByCode[0])){
                $insert_data['id'] = $categoryByCode[0]->id;
            }else{
                $insert_data['code'] = $code;
                $insert_data['name'] = $row['name'];
            }
            echo $i;
            echo "<br/>";
            $insert_data['id'] = $category_model->insert($insert_data, 4);
            echo "insert";
            if(isset($row['branch_group'])){
                $branch_groups = explode(',', $row['branch_group']);
                if(isset($row['branch'])){
                    $branches = explode(',', $row['branch']);
                    foreach($branch_groups as $branch_group){
                        foreach($branches as $branch){
                            $permission = array('branch_id' => $branch, 'branch_group_id' => $branch_group);
                            $insert_data['id'] = $category_model->insert($insert_data, 1, $permission);
                            echo "to";
                            echo "<br/><br/>";
                        }
                    }
                }
            }
        }
    }

    function import_item_by_excel(){
        $item_model = $this->load->table_model('item');
        $item_model->setFieldListRequirement(array());
        $category_model = $this->load->table_model('category');
        $category_item_model = $this->load->table_model('category_item');
        $package_item_model = $this->load->table_model('package_item');
        $file = FCPATH."import/import_item.xlsx";
        $data = $this->get_data_from_excel($file);
        foreach($data as $i => $row){
            $insert_data = array();
            $code = "";
            if(isset($row['code'])){
                $code = $row['code'];
                $itemByCode = $item_model->get(array('code' => $code));
                if(isset($itemByCode[0])){
                    $insert_data['id'] = $itemByCode[0]->id;
                }
            }
            $insert_data['code'] = $code;
            $insert_data['name'] = $row['name'];
            $insert_data['description'] = isset($row['description']) ? $row['description'] : "";
            $insert_data['price'] = $row['price'];
            $insert_data['promotion'] = $row['is_promotion'];
            $insert_data['type'] = ITEM_TYPE(ucfirst($row['type']));
            echo "<hr/>";
            echo $i;
            echo "<br/>";
            $insert_data['id'] = $item_model->insert($insert_data, 4);
            if(isset($row['category'])){
                $categories = explode(',', $row['category']);
                foreach($categories as $category){
                    $categoryByCode = $category_model->get(array('code' => $category));
                    if(isset($categoryByCode[0])){
                        $category_id = $categoryByCode[0]->id;
                        $insert_category_data = array(
                            'item_id' => $insert_data['id'],
                            'category_id' => $category_id
                        );
                        $category_item_checker = $category_item_model->get($insert_category_data);
                        if(count($category_item_checker) == 0){
                            $category_item_model->insert($insert_category_data);
                            echo "<br/>Insert to category $category<br/><br/>";
                        }
                    }
                }
            }
            if(isset($row['package_item'])){
                $package_items = explode(',', $row['package_item']);
                foreach($package_items as $package_item){
                    $package_item_by_code = $item_model->get(array('code' => $package_item));
                    if(isset($package_item_by_code[0])){
                        $package_item_id = $package_item_by_code[0]->id;
                        $insert_package_data = array(
                            'package_id' => $insert_data['id'],
                            'item_id' => $package_item_id
                        );
                        $package_item_checker = $package_item_model->get($insert_package_data);
                        if(count($package_item_checker) == 0) {
                            $package_item_model->insert($insert_package_data);
                            echo "Insert item $package_item to this item<br/>";
                        }
                    }
                }
                echo "<br/>";
            }
            echo "insert";
            if(isset($row['branch_group'])){
                $branch_groups = explode(',', $row['branch_group']);
                if(isset($row['branch'])){
                    $branches = explode(',', $row['branch']);
                    foreach($branch_groups as $branch_group){
                        foreach($branches as $branch){
                            $permission = array('branch_id' => $branch, 'branch_group_id' => $branch_group);
                            $insert_data['id'] = $item_model->insert($insert_data, 1, $permission);
                            echo "to";
                            var_dump($permission);
                            echo "<br/><br/>";
                        }
                    }
                }
            }
        }
        echo "DONE";
    }

    function get_data_from_excel($file){
        $this->load->library('excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $active_sheet = $objPHPExcel->setActiveSheetIndex(0);
        $cell_collection = $active_sheet->getCellCollection();
        $data = array();
        $key_arr = array();
        foreach ($cell_collection as $cell) {
            $column = $active_sheet->getCell($cell)->getColumn();
            $row = $active_sheet->getCell($cell)->getRow();
            $cell_obj = $active_sheet->getCell($cell);
            $data_value = $cell_obj->getValue();
            if(PHPExcel_Shared_Date::isDateTime($cell_obj)) {
                $data_value = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($data_value));
            }
            if($row == 1){
                $key_arr[$column] = strtolower($data_value);
            }else{
                $data[$row][$key_arr[$column]] = $data_value;
            }
        }
        return $data;
    }

    function import_commission_by_excel(){
        $commission_model = $this->load->table_model('commission');
        $commission_item_model = $this->load->table_model('commission_item');
        $item_model = $this->load->table_model('item');
        $file = FCPATH."import/import_commission.xlsx";
        $data = $this->get_data_from_excel($file);
        $commission_id = 0;
        foreach($data as $row){
            $will_insert = array();
            if(isset($row['name'])){
                $will_insert['name'] = $row['name'];
            }
            if(isset($row['id'])){
                $commission_by_id = $commission_model->get(array('id' => $row['id']));
                if(count($commission_by_id) > 0){
                    echo "<br/>";
                    echo "change commission from $commission_id";
                    $commission_id = $commission_by_id[0]->id;
                    echo " to $commission_id";
                    echo " by current commission on database";
                    echo "<br/>";
                }else{
                    $will_insert['id'] = $row['id'];
                    echo "<br/>";
                    echo "change commission from $commission_id";
                    $commission_id = $commission_model->insert($will_insert, "", array(), true);
                    echo " to $commission_id";
                    echo " by insert new commission with ID";
                    echo "<br/>";
                }
            }else{
                if(count($will_insert) > 0){
                    echo "<br/>";
                    echo "change commission from $commission_id";
                    $commission_id = $commission_model->insert($will_insert);
                    echo " to $commission_id";
                    echo " by insert new commission";
                    echo "<br/>";
                }
            }
            if(isset($commission_id) && $commission_id != 0){
                if(isset($row['item'])){
                    $item_code = $row['item'];
                    $type = 1;
                    if(isset($row['item_type'])){
                        $type = COMMISSION_TYPE($row['item_type']);
                        if(!isset($type)){
                            $type = 1;
                        }
                    }
                    if(isset($row['item_value'])){
                        $item_by_code = $item_model->get(array('code' => $item_code),"","","","",array(),true);
                        if(count($item_by_code) > 0){
                            $item_id = (int) $item_by_code[0]->id;
                            $commission_item_data = array(
                                'commission_id' => $commission_id,
                                'item_id' => $item_id,
                                'value' => $row['item_value'],
                                'type' => $type
                            );
                            $commission_item_model->insert($commission_item_data);
                            echo "insert item $item_id to $commission_id<br/>";
                        }
                    }
                }
            }
        }
    }

    function import_customer_promotion_restriction(){
        $this->db->trans_start();
        $this->db->delete('bill',array('status' => BILL_STATUS('Invisible')));
        $customer_model = $this->load->table_model('customer');
        $item_model = $this->load->table_model('item');
        $item_promo_map = array(
            '46Promo' => 46,
            'SIGFTRIAL' => 101,
            'DST' => 43,
            'PP' => 32
        );
        $item_price_map = array();
        foreach($item_promo_map as $key => $value){
            $item_by_code = $item_model->get(array('code' => $value));
            if(count($item_by_code) > 0){
                $item_id = $item_by_code[0]->id;
                $item_price = $item_by_code[0]->price;
                $item_promo_map[$key] = $item_id;
                $item_promo_map[strtolower($key)] = $item_id;
                $item_price_map[$item_id] = $item_price;
            }
        }
        $insert_data = array();
        $import_promo_folder = FCPATH."import/customer_promo/";
        foreach(glob($import_promo_folder . "*.xls") as $file_name){
            $data = $this->get_data_from_excel($file_name);
            foreach($data as $row){
                $customer_id = 0;
                $item_id = 0;
                $date = null;
                if(isset($row['member'])){
                    $customer_by_code = $customer_model->get(array('code' => $row['member']));
                    if(count($customer_by_code) > 0){
                        $customer_id = $customer_by_code[0]->id;
                    }
                }
                if(isset($row['item no'])){
                    $promo_item_key = strtolower($row['item no']);
                    if(isset($item_promo_map[$promo_item_key])){
                        $item_id = $item_promo_map[$promo_item_key];
                    }
                }
                if(isset($row['date'])){
                    $date = $row['date'];
                }
                if($customer_id != 0 && $item_id != 0){
                    $insert_data[] = array(
                        'customer_id' => $customer_id,
                        'item_id' => $item_id,
                        'created_date' => $date
                    );
                }
            }
        }
        $bill_model = $this->load->table_model('bill');
        $bill_item_model = $this->load->table_model('bill_item');
        foreach($insert_data as $row){
            $bill_data = array(
                'code' => 0,
                'customer_id' => $row['customer_id'],
                'description' => 'Bill import to check customer promotion item history of old system',
                'gst' => 7,
                'status' => 4,
                'global' => 1,
                'created_date' => $row['created_date']
            );
            $bill_id = $bill_model->insert($bill_data);
            $bill_item_data = array(
                'bill_id' => $bill_id,
                'item_id' => $row['item_id'],
                'quantity' => 1,
                'price' => $item_price_map[$row['item_id']],
                'discount_type' => 1,
                'discount_value' => 100
            );
            $bill_item_model->insert($bill_item_data);
        }
        echo "Done!";
        $this->db->trans_complete();
    }

    function merge_duplicate_customer()
    {
        $this->db->trans_start();
        $customers = $this->db->get('customer')->result();
        foreach($customers as $customer){
            $customer->nric = trim($customer->nric);
            $this->db->update('customer',$customer,array('id' => $customer->id));
        }
        $customers = $this->db->query('SELECT
*

FROM
 `customer`
WHERE
 customer.`nric` IN (
  SELECT
  customer.`nric`
  FROM
   `customer`
	WHERE customer.status = 1
  GROUP BY
   customer.`nric`
  HAVING
   COUNT(customer.nric) > 1
 ) AND customer.nric != "" AND customer.status = 1
ORDER BY customer.nric;')->result();

        $customers = convert_to_array($customers,'nric','',false);
        $credits = $this->load->table_model('credit')->getTableMap('customer_id', '', '', false);
        foreach ($customers as $nric => $customer_rows) {
            if ($nric == 'PROMO DONE' || $nric == 'S') {
                continue;
            }
            if (count($customer_rows) < 2) {
                continue;
            }
            $main_customer_information = $customer_rows[0];
            $temp = isset($credits[$main_customer_information->id]) ? $credits[$main_customer_information->id] : array();
            $main_customer_credits = array();
            foreach ($temp as $row) {
                $main_customer_credits[$row->item_id] = $row;
            }

            unset($customer_rows[0]);
            foreach ($customer_rows as $customer_row) {
                if ($main_customer_information->customer_type != 2 && $customer_row->customer_type == 2) {
                    $main_customer_information->customer_type = $customer_row->customer_type;
                    if (!$main_customer_information->pin && $customer_row->pin) {
                        $main_customer_information->pin = $customer_row->pin;
                    }
                }
                if(!$main_customer_information->email){
                    $main_customer_information->email = $customer_row->email;
                }

                if(!$main_customer_information->mobile_number){
                    $main_customer_information->mobile_number = $customer_row->mobile_number;
                }
                if(!$main_customer_information->gender){
                    $main_customer_information->gender = $customer_row->gender;
                }

                $this->db->update('bill', array('customer_id' => $main_customer_information->id), array('customer_id' => $customer_row->id));
                $sub_customer_credits = isset($credits[$customer_row->id]) ? $credits[$customer_row->id] : array();
                foreach ($sub_customer_credits as $credit) {
                    if (isset($main_customer_credits[$credit->item_id])) {
                        $main_credit_row = $main_customer_credits[$credit->item_id];
                        $main_credit_row->credit += $credit->credit;
                        $main_credit_row->credit_original += $credit->credit_original;
                        if ($main_credit_row->end_date < $credit->end_date)
                            $main_credit_row->end_date = $credit->end_date;
                        if ($main_credit_row->start_date > $credit->start_date)
                            $main_credit_row->start_date = $credit->start_date;
                        if ($main_credit_row->created_date < $credit->created_date)
                            $main_credit_row->created_date = $credit->created_date;
                        if ($main_credit_row->updated_date < $credit->updated_date)
                            $main_credit_row->updated_date = $credit->updated_date;

                        $this->db->update('credit', $main_credit_row, array('id' => $main_credit_row->id));
                        $this->db->update('credit', array('status' => 0), array('id' => $credit->id));
                        $this->db->update('credit_log', array('id' => $main_credit_row->id), array('id' => $credit->id));
                    } else {
                        $this->db->update('credit', array('customer_id' => $main_customer_information->id), array('id' => $credit->id));
                    }
                }
                $this->db->update('customer', array('status' => 0), array('id' => $customer_row->id));
                $this->db->update('bill',array('customer_id' => $main_customer_information->id),array('customer_id' => $customer_row->id));
                $this->db->update('bill_log',array('customer_id' => $main_customer_information->id),array('customer_id' => $customer_row->id));
            }
            $this->db->update('customer', $main_customer_information, array('id' => $main_customer_information->id));

        }
        $this->fix_merge_customer();
        $this->fix_merge_customer_2();
        $this->db->trans_complete();
        echo "Done!";
    }

    function fix_merge_customer(){
        $this->db->trans_start();

        $customer_active = $this->db->query("select *
from customer
where customer.nric IN (
	select customer.nric FROM
	credit JOIN customer on credit.customer_id = customer.id
	where credit.status = 0
)
and customer.status = 1
order by customer.nric")->result_array();

        $customer_deleted = $this->db->query("select *
from customer
where customer.status = 0
order by customer.nric")->result_array();;
        $customer_deleted = convert_to_array($customer_deleted,'nric','',false);
        $main_customer_id = convert_to_array($customer_active,'','id',false);
        $customer_active = convert_to_array($customer_active,'nric','',true);
        $credit_model = $this->load->table_model('credit');
        $active_credits = $credit_model->getTableMap('customer_id','',array('customer_id' => $main_customer_id),false);

        foreach($active_credits as $customer_id=>&$row){
            $row = convert_to_array($row,'item_id','');
        }

        foreach($customer_active as $nric=>$main_fields){
            if(isset($active_credits[$main_fields['id']])){
                $active_credit = $active_credits[$main_fields['id']];
                if(isset($customer_deleted[$nric])){
                    foreach($customer_deleted[$nric] as $row){
                        $deleted_sub_credits = $this->db->get_where('credit',array('customer_id' => $row['id']))->result_array();
                        foreach($deleted_sub_credits as $deleted_credit){
                            if(isset($active_credit[$deleted_credit['item_id']])){
                                $sub_credit_id = $deleted_credit['id'];
                                $active_credit_id = $active_credit[$deleted_credit['item_id']]->id;
                                $this->db->update('bill_item_credit',array('credit_id' => $active_credit_id),array('credit_id' => $sub_credit_id));
                                echo "Change from {$sub_credit_id} => {$active_credit_id} <br>";
                            }
                        }
                    }
                }
            }

        }

        $this->db->trans_complete();

        echo "Successfully fixed (Hope so)";
    }

    function fix_merge_customer_2()
    {
        $this->db->trans_start();

        $customer_active = $this->db->query("select *
from customer
where customer.nric IN (
	select customer.nric FROM
	customer
	where customer.status = 0
)
and customer.status = 1
order by customer.nric")->result_array();

        $customer_deleted = $this->db->query("select *
from customer
where customer.nric IN (
	select customer.nric FROM
	customer
	where customer.status = 0
)
and customer.status = 0
order by customer.nric")->result_array();;
        $customer_deleted = convert_to_array($customer_deleted, 'nric', '', false);
        $customer_active = convert_to_array($customer_active, 'nric', '', true);

        foreach($customer_active as $nric=>$main_field){
            foreach($customer_deleted[$nric] as $sub_field){
                $result = $this->db->get_where('customer_branch',array('customer_id' => $sub_field['id']))->result_array();
                foreach($result as $row){
                    if(!$this->db->get_where('customer_branch',array('customer_id' => $main_field['id'],'branch_id' => $row['branch_id']))->num_rows()){
                        echo "{$nric} : Branch ID = {$row['branch_id']}; from {$sub_field['id']} to {$main_field['id']}<br>";
                        $this->db->update('customer_branch',array('customer_id' => $main_field['id']),array('customer_id' => $sub_field['id'],'branch_id' => $row['branch_id']));
                    }
                }

                $result = $this->db->get_where('customer_branch_group',array('customer_id' => $sub_field['id']))->result_array();
                foreach($result as $row){
                    if(!$this->db->get_where('customer_branch_group',array('customer_id' => $main_field['id'],'branch_group_id' => $row['branch_group_id']))->num_rows()){
                        echo "{$nric} : Branch Group ID = {$row['branch_group_id']}; from {$sub_field['id']} to {$main_field['id']}<br>";
                        $this->db->update('customer_branch_group',array('customer_id' => $main_field['id']),array('customer_id' => $sub_field['id'],'branch_group_id' => $row['branch_group_id']));
                    }
                }
            }
        }
        $this->db->trans_complete();
    }

    function enable_all_employee_working_hour(){
        $therapist_id = 8;
        $working_days = WORKING_DAYS();
        $start_hour = "10:00";
        $end_hour = "23:00";

        $this->db->trans_start();
        $therapist = $this->db->get_where('employee',array('department_id' => $therapist_id))->result_array();

        foreach($therapist as $each_therapist){
            $employee_branches = $this->db->get_where('employee_branch',array('employee_id' => $each_therapist['id']))->result_array();
            foreach($employee_branches as $each_branch){
                $is_set = $this->db->get_where('employee_calendar',array('branch_id' => $each_branch['branch_id'], 'employee_id' => $each_therapist['id']))->num_rows();
                if(! $is_set){
                    foreach($working_days as $key=>$label){
                        $this->db->insert('employee_calendar',array(
                            'day_id' => $key,
                            'employee_id' => $each_therapist['id'],
                            'start_time' => $start_hour,
                            'end_time'   => $end_hour,
                            'branch_id' => $each_branch['branch_id']
                        ));
                        echo "Insert working time for {$each_therapist['first_name']} {$each_therapist['last_name']} on $label at branch id :{$each_branch['branch_id']}";
                    }

                }
            }
        }

        $this->db->trans_complete();

        echo "Process is completed";
    }

    function set_up_room(){
        $rooms = array(
            '1' => array(
                'Massage room'  => 9,
                'Slimming room' => 4,
                'Sauna room'    => 0,
                'Facial room'   => 3,
                'Shower room'   => 3
            ),
            '2' => array(
                'Massage room'  => 11,
                'Slimming room' => 1,
                'Sauna room'    => 1,
                'Facial room'   => 0,
                'Shower room'   => 2
            ),
            '3' => array(
                'Massage room'  => 10,
                'Slimming room' => 1,
                'Sauna room'    => 0,
                'Facial room'   => 1,
                'Shower room'   => 2
            ),
            '4' => array(
                'Massage room'  => 8,
                'Slimming room' => 1,
                'Sauna room'    => 0,
                'Facial room'   => 0,
                'Couple room'   => 1,
                'Shower room'   => 2
            ),
            '5' => array(
                'Massage room'  => 11,
                'Slimming room' => 1,
                'Sauna room'    => 0,
                'Facial room'   => 1,
                'Shower room'   => 2
            ),
            '6' => array(
                'Massage room'  => 13,
                'Slimming room' => 2,
                'Sauna room'    => 1,
                'Facial room'   => 1,
                'Shower room'   => 3
            ),
        );

        $room_codes = array(
            'Massage room'  => 'MR',
            'Slimming room' => 'SLR',
            'Sauna room'    => 'SAR',
            'Facial room'   => 'BR',
            'Couple room'   => 'CR',
            'Shower room'   => 'SHR'
        );
        $this->db->trans_complete();
        $branch = $this->load->table_model('branch')->getTableMap('id','','',true,Permission_Value::ADMIN);
        $this->db->empty_table('room');
        $this->db->empty_table('room_branch');
        $this->db->empty_table('booking_service');
        $this->db->empty_table('booking');
        $this->db->query('ALTER TABLE room AUTO_INCREMENT = 1;');
        $room_type_map = $this->db->get('room_type')->result_array();
        $room_type_map = convert_to_array($room_type_map,'code');
        foreach($rooms as $branch_id=>$branch_rooms){

            foreach($branch_rooms as $name=>$room_quantity){
                for($i = 1; $i <= $room_quantity; $i++){
                    $room_type = isset($room_type_map[$room_codes[$name]])?$room_type_map[$room_codes[$name]]:null;
                    $info = array(
                        'code'          =>  $branch[$branch_id]->sale_id_prefix . '_' . $room_codes[$name] . '_' . $i,
                        'bed_quantity'  => 1,
                        'name'          => $name . ' ' . $i,
                        'status'        => 1,
                        'room_type_id'  => null
                    );
                    if($room_type){
                        $info['room_type_id'] = $room_type['id'];
                    }
                    $this->db->insert('room',$info);
                    $insert_id = $this->db->insert_id();
                    $this->db->insert('room_branch',array('room_id' => $insert_id,'branch_id' => $branch_id));
                    echo 'Inserted room : ' .  $branch[$branch_id]->sale_id_prefix . '_' . $room_codes[$name] . '_' . $i . '<br>';
                }
            }
        }
        echo "Operation completed";
        $this->db->trans_complete();
    }

    function merge_customer(){
        $customer_id_1 = $this->input->get('c1',true);
        $customer_id_2 = $this->input->get('c2',true);
        $this->db->trans_start();
        $customer_1 = $this->load->table_model('customer')->getByID($customer_id_1);
        $customer_2 = $this->load->table_model('customer')->getByID($customer_id_2);

        $credit_id_1 = $this->load->table_model('credit')->getTableMap('item_id','',array('customer_id' => $customer_1->id),true);
        $credit_id_2 = $this->load->table_model('credit')->getTableMap('item_id','',array('customer_id' => $customer_2->id),true);
        if(!$customer_1 || !$customer_2){
            echo "one of two customer doesn't exists"; return;
        }

        $check_array = array('nric','email','mobile_number');

        $customer_update_field = array();
        foreach($check_array as $checking_field){
            if(!isset($customer_1->$checking_field) && isset($customer_2->$checking_field)){
                $customer_update_field['$checking_field'] = trim($customer_2->$checking_field);
                echo "Update Customer $checking_field: ".$customer_2->$checking_field.'<br>';
            }
        }

        $this->load->table_model('customer')->update($customer_1->id, $customer_update_field, false);
        $this->load->table_model('customer')->update($customer_2->id, array('status' => 0),false);

        /* ---- Move Credit ------ */
        foreach($credit_id_2 as $item_id=>$credit_field){
            echo "Start Process Credit ID:".$credit_field->id.'<br>';
            $duplicate_credit = isset($credit_id_1[$item_id])?$credit_id_1[$item_id]:null;

            if($duplicate_credit){
                $credit_update_field = new stdClass();
                $credit_update_field->credit = $duplicate_credit->credit + $credit_field->credit;
                $credit_update_field->credit_original = $duplicate_credit->credit_original + $credit_field->credit_original;
                if($duplicate_credit->end_date < $credit_field->end_date){
                    $credit_update_field->end_date = $credit_field->end_date;
                }

                if($duplicate_credit->start_date > $credit_field->start_date){
                    $credit_update_field->start_date = $credit_field->start_date;
                }
                $this->load->table_model('credit')->update($credit_field->id,array('status' => 0),false);
                $this->load->table_model('credit')->update($duplicate_credit->id, $credit_update_field,false);

                $this->db->update('credit_adjustment_log',array('credit_id' => $duplicate_credit->id),array('credit_id' => $credit_field->id));
                $this->db->update('credit_adjustment_log',array('credit_id_transfer' => $duplicate_credit->id),array('credit_id_transfer' => $credit_field->id));

                $this->db->update('bill',array('customer_id' => $customer_id_1),array('customer_id' => $customer_id_2));
                $this->db->update('bill_log',array('customer_id' => $customer_id_1),array('customer_id' => $customer_id_2));
                $this->db->update('bill_item_credit',array('credit_id' => $duplicate_credit->id),array('credit_id' => $credit_field->id));
            }
            else{
                $this->db->update('credit', array('customer_id' => $customer_id_1), array('id' => $credit_field->id));
                $this->db->insert('merged_credit_history', array('credit_id' => $credit_field->id, 'customer_id_from' => $customer_id_2, 'customer_id_to' => $customer_id_1, 'log_time' => get_database_date()));
                $mergedCreditId = $this->db->insert_id();

                $result = $this->db->get_where('credit_log', array('customer_id' => $customer_id_2,'id' => $credit_field->id))->result();
                $this->db->update('credit_log',array('customer_id' => $customer_id_1),array('customer_id' => $customer_id_2, 'id' => $credit_field->id));
                foreach($result as $row){
                    $this->db->insert('merged_credit_log_history', array('merged_credit_id' => $mergedCreditId , 'credit_log_id' => $row->inc_id , 'customer_id_from' => $customer_id_2, 'customer_id_to' => $customer_id_1, 'log_time' => get_database_date()));
                }
            }
        }

        $result = $this->db->get_where('bill', array('customer_id' => $customer_id_2))->result();
        $this->db->update('bill',array('customer_id' => $customer_id_1),array('customer_id' => $customer_id_2));
        foreach($result as $row){
            $this->db->insert('merged_bill_history', array('bill_id' => $row->id, 'customer_id_from' => $customer_id_2, 'customer_id_to' => $customer_id_1, 'log_time' => get_database_date()));
        }

        $result = $this->db->get_where('bill_log', array('customer_id' => $customer_id_2))->result();
        $this->db->update('bill_log',array('customer_id' => $customer_id_1),array('customer_id' => $customer_id_2));
        foreach($result as $row){
            $this->db->insert('merged_bill_log_history', array('bill_log_id' => $row->id, 'customer_id_from' => $customer_id_2, 'customer_id_to' => $customer_id_1, 'log_time' => get_database_date()));
        }

        /*----- Move Branch Group ------- */
        $customer_2_branch_group = convert_to_array($this->db->get_where('customer_branch_group',array('customer_id' => $customer_id_2))->result_array(),'','branch_group_id');
        foreach($customer_2_branch_group as $branch_group_id){
            $is_exists = $this->db->get_where('customer_branch_group',array('customer_id' => $customer_id_1,'branch_group_id' => $branch_group_id))->result_array();
            if(!count($is_exists)){
                $this->db->insert('customer_branch_group',array('customer_id' => $customer_id_1,'branch_group_id' => $branch_group_id));
                echo "Add to branch group: $branch_group_id<br>";
            }
        }
        echo '<br>FINISHED';
        $this->db->trans_complete();
    }

    function clear_session(){
        $this->session->sess_destroy();
    }

    /**
     * Function to update item_booking table and category_booking table
     */
    function update_show_in_booking(){
        $branch_list = $this->db->get_where('branch',array('status' => 1))->result_array();
        $item_list = $this->db->get_where('item',array('status' => 1))->result_array();
        $category_list = $this->db->get_where('category',array('status' => 1))->result_array();

        $item_booking = $this->db->get_where('item_booking')->result_array();
        $category_booking = $this->db->get_where('category_booking')->result_array();

        // Update item_booking table
        $temp_array = array();
        foreach($item_booking as $item){
            $temp = $item["item_id"]."@".$item["branch_id"];
            $temp_array[] = $temp;
        }

        $data_array = array();
        foreach($item_list as $item){
            foreach($branch_list as $branch) {
                $temp = $item["id"]."@".$branch["id"];
                if(in_array($temp, $temp_array));
                else{
                    $data = array('item_id' => $item["id"],
                                  'branch_id' => $branch["id"],
                                  'is_show' => 1,
                                  'is_price_show' => 0);
                    $data_array[] = $data;
                }
            }
        }

        if(!empty($data_array)){
            $this->db->insert_batch('item_booking',$data_array);
            echo count($data_array)." rows of item_booking inserted<br>";
        }
        else{
            echo "0 row of item_booking inserted<br>";
        }


        // Update category_booking table
        $temp_array = array();
        foreach($category_booking as $cat){
            $temp = $cat["category_id"]."@".$cat["branch_id"];
            $temp_array[] = $temp;
        }

        $data_array = array();
        foreach($category_list as $cat){
            foreach($branch_list as $branch) {
                $temp = $cat["id"]."@".$branch["id"];
                if(in_array($temp, $temp_array));
                else{
                    $data = array('category_id' => $cat["id"],
                                  'branch_id' => $branch["id"],
                                  'is_show' => 1,
                                  'is_price_show' => 0);
                    $data_array[] = $data;
                }
            }
        }

        if(!empty($data_array)){
            $this->db->insert_batch('category_booking',$data_array);
            echo count($data_array)." rows of category_booking inserted<br>";
        }
        else{
            echo "0 row of category_booking inserted<br>";
        }
    }

    function show_php(){
        echo phpinfo();
    }

    function fix_duplicate_customer_code(){
        $list_duplicate = $this->db->query("SELECT id, code, count(id)
                                            FROM gentopos2.customer
                                            WHERE code != ''
                                            GROUP BY code
                                            HAVING count(id) > 1")->result_array();
        if(count($list_duplicate) > 0){
            foreach($list_duplicate as $item){
                $i = 1;
                while($i < 10){
                    $new_code = 'SPA' . mt_rand(100001, 900000);
                    $check_new_code = $this->db->query('SELECT id
                                                        FROM gentopos2.customer
                                                        WHERE code = "'.$new_code.'"')->result();
                    if(count($check_new_code) == 0){
                        $this->db->query('UPDATE gentopos2.customer SET code = "'.$new_code.'" WHERE id = '.$item['id']);
                        break;
                    }
                    $i++;
                }
            }
        }
    }

    function test(){
        echo '<pre>';
        var_dump($this->session->all_userdata()['staff']['login']->branch_group_id);
        echo '</pre>';
    }

    function test_res_api(){
        $user = json_decode(
            file_get_contents('http://localhost/spa/rest_api/user/id/1/format/json')
        );

        var_dump($user);

        //echo $user->name;
    }

    function send_email_test(){
        $this->load->library('email');
        $this->email->clear(true);
        $subject = "test send paypal email";
        $body = $this->load->email_content('customer_notify_remain_payment');
        $this->email->to('lhoanghieu@gmail.com');
        $this->email->subject($subject);
        $this->email->message($body);
        if ($this->email->send()) {
            return 1;
        }
        return "Fail to send mail";
    }


    // CANCEL APPOINTMENT
    function cancelAppointment(){
        $bookingId = $this->input->get_post('id',true);
        if(!empty($bookingId)){
            $st_now = $this->load->table_model('booking')->get_detail(array('booking'=>array('id'=>$bookingId)));
            $st_now = count($st_now)?(array)$st_now[0]:array();
            $result = $this->load->table_model('booking')->update($bookingId,array(
                'status' => BOOKING_STATUS('Reserve'),
                'reserve_comment' => 'Cancel by customer from Transactions.',
                'old_status' => $st_now['status']
            ));
            if($result){
                echo json_encode(array('result' => TRUE));
            }
            else{
                echo json_encode(array('result' => FALSE, 'messsage' => 'There is a problem when cancel an appointment. Please contact us for more information.'));
            }
        }
        else{
            echo json_encode(array('result' => FALSE, 'message' => 'Booking ID is empty. Please contact us for more information.'));
        }
    }
}
