<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 2:53 PM
 */

class Branchs extends Staff_Controller {

    function __construct(){
        parent::__construct();
        $this->moduleName = "branchs";
        $this->pageCode   = "branchs";
        $this->pageName = "Branches";
        $this->breadcrumbs[] = array('url' => 'branchs', 'text' => 'Branches');
    }

    function getForm(){
        $id = $this->input->post('id');
        $branch = $this->load->controller_model('branchs');
        $data = $branch->getDataForViewForm($id);
        $this->content('branchs/edit_form',$data, false);
    }


}