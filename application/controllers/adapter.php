<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adapter extends POS_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function to_branch($branch_id = 0, $url = 'config'){
        try{
            $user_login = $this->session->userdata('admin',true);
            if(!$user_login || ! isset($user_login['login'])){
                throw new Exception("You don't have permission to access this branch");
            }
            $user_login = $user_login['login'];
            $employee = $this->load->table_model('employee')->getByID($user_login->user_id);
            $branch = $this->load->table_model('branch')->getByID($branch_id);
            $groups = $this->load->table_model('department_group')->get_group(array('department_id' => $employee->department_id));
            /*------- Set user session -----------*/
            $userSession = new stdClass();
            $userSession->user_id = $employee->id;
            $userSession->Username = $employee->username;
            $userSession->Email = $employee->email;
            $userSession->FirstName = $employee->first_name;
            $userSession->LastName = $employee->last_name;
            $userSession->Phone = $employee->phone_number;
            $userSession->branch_id = $branch_id;
            $userSession->branch_name = $branch->name;
            $userSession->branch_group_id = $branch->branch_group_id;
            $userSession->type = $employee->type;
            $userSession->login_as_type = $employee->type > 2 ? 2 : $employee->type;
            $userSession->group_id = $groups;
            $userSession->timezone = $this->system_config->get('timezone');
            $userSession->permissions = $this->user_permission->getUserPermissions(array(
                'permission' => array('branch_id' => $branch_id),
                'user_level' => 2,
            ));
            $this->session->set_userdata('login',$userSession,'staff');
            redirect(site_url(urldecode($url)));
        }
        catch(Exception $e){
            echo $e->getMessage();
        }

    }

}