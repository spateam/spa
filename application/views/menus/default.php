<div id="sidebar" class="hidden-print">
    <ul class="nav" style="display:block">
        <?php
        foreach($menus[0] as $menu) :
            $hasChild = false;
            $childContent = "";
            $parent_active = false;
            if(isset($menus[$menu->id])){
                if(count($menus[$menu->id]) == 0){ continue; }
                $hasChild = true;
                foreach($menus[$menu->id] as $child_menu){
                    $childLink = site_url($child_menu->link);
                    $class_active = "";
                    if(strtolower($child_menu->title)==strtolower($pageName)){
                        $class_active = " class='active'";
                        $parent_active = true;
                    }
                    $childContent .= "<li{$class_active}><a href='{$childLink}'><i class='icon fa fa-{$child_menu->link}'></i><span class='hidden-minibar'>{$child_menu->title}</span></a></li>";
                }
            }
            ?>
            <li id="<?php echo $menu->id; ?>" class="<?php echo (strtolower($menu->title)==strtolower($pageName) || $parent_active) ? "active" : ""; ?>">
                <?php if($hasChild): ?>
                    <a href="#">
                        <i class="icon fa fa-<?php echo $menu->link?>"></i>
                        <span class="hidden-minibar"><?php echo $menu->title?></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav" <?php echo ($parent_active) ? "style='display: block;'" : ""; ?>>
                    <?php echo $childContent; ?>
                    </ul>
                <?php else: ?>
                    <?php if(isset($menu->link) && $menu->link != ""): ?>
                    <a href="<?php echo site_url($menu->link) ?>">
                        <i class="icon fa fa-<?php echo $menu->link?>"></i>
                        <span class="hidden-minibar"><?php echo $menu->title?></span>
                    </a>
                    <?php endif; ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
        <li>
            <a href="<?php if($this->config->item('current_system') == 'staff'){
                echo site_url('authorize/logout');
            }else if($this->config->item('current_system') == 'admin'){
                echo admin_url('authorize/logout');
            }?>">
                <i class="icon fa fa-power-off"></i>
                <span class="hidden-minibar">Logout</span>
            </a>
        </li>
    </ul>
</div>