<i class="fa fa-home"></i>
<?php foreach($breadcrumbs as $key => $value): ?>
    <a href="<?php echo isset($value['url']) ? site_url($value['url']) : ""; ?>"><?php echo $value['text']; ?></a>
<?php endforeach; ?>