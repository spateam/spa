<?php
$user = $this->session->userdata('login');
?>
<div class="fleft" style="width: 175px">
    <h1>
        <a href="#">
            <img src="<?= $this->assets->releaseImage('header_logo.png'); ?>" class="hidden-print header-log" id="header-logo" alt="">
        </a>
    </h1>
</div>
<div class="fleft" style="height:100%;  padding-top: 15px; padding-left:15px">
    <span class="text" style="color: #2d2b2b; float:left;vertical-align: middle; font-size:15px; font-weight:bolder"><?php
        if($user->login_as_type <= Permission_Value::BRANCH ){
            if(isset($user->branch_name))
                echo $user->branch_name;
        }else{
            if($user->login_as_type == Permission_Value::BRANCH_GROUP){
                echo "You're logging as Territory Manager";
            }else{
                echo "You're logging as Admin";
            }
        }
        ?></span>
</div>
<div class="fleft">
    <a id="menu-trigger" href="#"><i class="fa fa-bars fa fa-2x"></i></a>
    <div class="clear"></div>
    <div id="user-nav" class="hidden-print hidden-xs">
        <ul class="btn-group" style="width: auto; margin: 0px;">
            <?php if($user->Username):?>
            <li class="btn  hidden-xs disabled"><a onclick="return false;"><i class="icon fa fa-user fa-2x"></i> <span class="text">  Welcome <b><?php echo $user->Username?></b></span></a></li>
            <?php endif;?>
            <li class="btn  "><a href="<?php if($this->config->item('current_system') == 'staff'){
                    echo site_url('authorize/logout');
                }else if($this->config->item('current_system') == 'admin'){
                    echo admin_url('authorize/logout');
                }?>"><i class="fa fa-power-off"></i><span class="text">Logout</span></a></li>
        </ul>
    </div>
</div>
