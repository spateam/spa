<p>
    Dear <?php echo ucwords($first_name . ' ' . $last_name); ?>
</p>
<p>

    Thank you for your recent visit.
</p>
<p>
    We invite you to review your recent experience on https://www.facebook.com/healingtouchsg/ or TripAdvisor https://goo.gl/4MOQwe and you will stand a chance to win 1hr massage voucher worth $55.
</p>
<p>
    Thank you and we look forward to serving you again.
</p>
<p>
    Yours faithfully, <br>
    Customer Service Team <br>
    Healing Touch <br>
</p>

<p>
    <a href="<?php echo base_url().'booking/email_validation/unsubscribe/'.$id.'/'.md5($id.$code); ?>">Please click here if you wish to unsubscribe</a>
</p>