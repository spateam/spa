<p>
    Hi <?php echo $email; ?>,
</p>
<p>
    Thank you for signing up an account with us.<br/>
    Please click link below to verify your email address with us.<br/>
    Verification link: <a href="<?php echo site_url("booking/authorize/confirm/{$code}") ?>"><?php echo site_url("booking/authorize/confirm/{$code}") ?></a><br/>
    Your ID : "<?php echo $code ?>"<br/>
    <?php if(!empty($otp)): ?>
        Your OTP : <?php echo $otp; ?>
    <?php endif ?>
</p>
<p>
    Best Regards,<br/>
    Branch Group
</p>

<p>
    <a href="<?php echo base_url().'booking/email_validation/unsubscribe/'.$id.'/'.md5($id.$code); ?>">Please click here if you wish to unsubscribe</a>
</p>