<p>Dear <?php echo ucwords(strtolower($customer_name)); ?></p>
<p>This is an automated message to verify your email, please click on the link below.</p>

<?php if(isset($isDuplicateEmail) && isset($isWronglyMobile) && $isDuplicateEmail && $isWronglyMobile){ ?>
    <p>Your email address <b><?php echo $email; ?></b> is already registered with another mobile number, please provide us your previous mobile number to be verified</p>
    <p>
        Click on the link below to verify your mobile number.<br>
        <a href="<?php echo base_url(); ?>booking/authorize/verify_email?cid=<?php echo $id ?>">Verify Mobile</a>
    </p>
<?php } else { ?>
    <p>
        Click on the link below to verify your email address..<br>
        <a href="<?php echo base_url(); ?>booking/authorize/verify_email?cid=<?php echo $id ?>">Verify Email</a>
    </p>
<?php } ?>

<p>Best Regards</p>
<?php
if(isset($branch_group) && $branch_group != ''){
    echo $branch_group;
}
else{
    echo 'Healing Touch Spa';
}
?>