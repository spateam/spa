<?php $str_pass = $userid."_".md5($password) ?>
<p>
    Your new login password is <strong><?php echo $password; ?></strong>.
</p>
<p>
    You may login to <a style="text-decoration: none;" href="http://pos.healingtouchspa.com/booking/authorize/update_password/<?php echo $str_pass ?>">http://pos.healingtouchspa.com/booking/authorize/update_password</a> to reset your password.
</p>
<p>
	Thank you<br>
	Customer Service<br>
	Healing Touch Global Pte Ltd​
</p>
<p>
	CONFIDENTIALITY NOTICE: This communication contains information that is intended for the individual or entity named as recipient. Such information may be confidential and/or protected by legal professional privilege. If you are not the intended recipient, please take notice that any disclosure, copying, distribution or use of any part of the contents of this communication is strictly prohibited by law. If you have received this communication in error, please notify us immediately by return email info@healingtouchspa.com and delete this communication wholly from any storage media.
</p>