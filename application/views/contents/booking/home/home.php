<div style="margin: 33px 0 20px 0;" class="index-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="index_block">
                    <div class="header_slider">
                        <div class="fluid_container">
                            <div class="camera_wrap camera_orange_skin" id="camera_wrap"
                                 style="display: block; height: 446px;">
                                <div class="camera_fakehover">
                                    <div class="camera_src camerastarted paused">
                                        <div data-link="fitness-wellness.html"
                                             data-src="http://vn.gentotech.net/source/projects/gentospa/magentospa/skin/frontend/default/theme289k/images/slider_pic1.jpg">
                                            &nbsp;</div>
                                        <div data-link="herbs-cleansing.html"
                                             data-src="http://vn.gentotech.net/source/projects/gentospa/magentospa/skin/frontend/default/theme289k/images/slider_pic2.jpg">
                                            &nbsp;</div>
                                        <div data-link="sports-nutrition.html"
                                             data-src="http://vn.gentotech.net/source/projects/gentospa/magentospa/skin/frontend/default/theme289k/images/slider_pic3.jpg">
                                            &nbsp;</div>
                                    </div>
                                    <div class="camera_target">
                                        <div class="cameraCont">
                                            <div class="cameraSlide cameraSlide_0"
                                                 style="visibility: visible; display: none; z-index: 1;"><img
                                                    width="868" height="446"
                                                    src="http://vn.gentotech.net/source/projects/gentospa/magentospa/skin/frontend/default/theme289k/images/slider_pic1.jpg?1418282533322"
                                                    class="imgLoaded"
                                                    style="visibility: visible; height: 446px; margin-left: 0px; margin-top: 0px; position: absolute; width: 868px;"
                                                    data-alignment="" data-portrait="">

                                                <div class="camerarelative" style="width: 868px; height: 446px;"></div>
                                            </div>
                                            <div class="cameraSlide cameraSlide_1 cameracurrent"
                                                 style="display: block; z-index: 999;"><img width="868" height="446"
                                                                                            src="http://vn.gentotech.net/source/projects/gentospa/magentospa/skin/frontend/default/theme289k/images/slider_pic2.jpg?1418282533881"
                                                                                            class="imgLoaded"
                                                                                            style="visibility: visible; height: 446px; margin-left: 0px; margin-top: 0px; position: absolute; width: 868px;"
                                                                                            data-alignment=""
                                                                                            data-portrait="">

                                                <div class="camerarelative" style="width: 868px; height: 446px;"></div>
                                            </div>
                                            <div class="cameraSlide cameraSlide_2 cameranext"
                                                 style="display: none; z-index: 1;"><img width="868" height="446"
                                                                                         src="http://vn.gentotech.net/source/projects/gentospa/magentospa/skin/frontend/default/theme289k/images/slider_pic3.jpg?1418282551029"
                                                                                         class="imgLoaded"
                                                                                         style="visibility: visible; height: 446px; margin-left: 0px; margin-top: 0px; position: absolute; width: 868px;"
                                                                                         data-alignment=""
                                                                                         data-portrait="">

                                                <div class="camerarelative" style="width: 868px; height: 446px;"></div>
                                            </div>
                                            <div class="cameraSlide cameraSlide_3 cameranext"
                                                 style="z-index: 1; display: none;">
                                                <div class="camerarelative" style="width: 868px; height: 446px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="camera_overlayer"></div>
                                    <div class="camera_target_content">
                                        <div class="cameraContents">
                                            <div class="cameraContent" style="display: none;"><a target=""
                                                                                                 href="fitness-wellness.html"
                                                                                                 class="camera_link"></a>
                                            </div>
                                            <div class="cameraContent cameracurrent" style="display: block;"><a
                                                    target="" href="herbs-cleansing.html" class="camera_link"></a></div>
                                            <div class="cameraContent" style="display: none;"><a target=""
                                                                                                 href="sports-nutrition.html"
                                                                                                 class="camera_link"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: none; top: auto; height: 7px;" class="camera_bar"><span
                                            class="camera_bar_cont"
                                            style="opacity: 0.8; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; background-color: rgb(34, 34, 34);"><span
                                                id="pie_0"
                                                style="opacity: 0.8; position: absolute; background-color: rgb(238, 238, 238); left: 0px; right: 0px; top: 2px; bottom: 2px; display: none;"></span></span>
                                    </div>
                                    <div class="camera_prev"><span></span></div>
                                    <div class="camera_next"><span></span></div>
                                </div>
                                <div class="camera_loader" style="display: none; visibility: visible;"></div>
                            </div>
                            <div class="clear">&nbsp;</div>
                        </div>
                    </div>
                    <div class="banners_col">
                        <div class="banner">
                            <div class="ban_wrap">
                                <h2>SALE</h2>

                                <h3>-50%</h3>

                                <p>Lorem ipsum dolor sitcodipiscing elit. Integer dui ante molestie et blandit eget
                                    vulput eget orci.</p>
                                <a class="shop_now" href="#">Shop Now!</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-left col-md-8">
            <div class="col-md-6">LEFT PANEL</div>
            <div class="col-md-6">
                <div title="Sen Spa" class="box-name name-shop">
                    <a title="Healing Touch" href="#">
                        <!-- title spa -->
                        Healing Touch
                    </a>
                </div>
                <p class="box-desc">
                    Healing Touch is Singapore’s most recommended spa on
                    Facebook to get a great massage. Visit us today with your
                    stressed and aching bodies...
                </p>

                <p class="box-desc address-shop">
                    <!-- address -->
                    70 Thomson Road, Singapore 307588, Singapore, AK 99688
                </p>

                <div class="box-info">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <span>02-07-2014</span>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <!--<a href="#" style="background: #00C0BE; padding: 2px 5px;"></i>&nbsp;-->
                            1 Like
                            <!--</a>-->
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <a style="background: #00C0BE; padding: 2px 5px; color: #fff;"
                               href="<?php echo site_url('booking/book') ?>">
                                Book online
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
        <!-- right -->
        <div class="col-right col-md-4">
            RIGHT PANEL
        </div>
    </div>
</div>