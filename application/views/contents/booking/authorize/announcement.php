<div style="margin: 33px 0 20px 0;" class="index-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="index_block">
<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/11/2014
 * Time: 5:01 PM
 */
    if($message === 1){
        echo "Your account has been activated. You will redirect to main page in 10 seconds";
    }else{
        echo $message;
    }
?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    <?php if($message == 1): ?>
    document.addEventListener("DOMContentLoaded", function(event) {
        //do work
        setTimeout(function () {
            window.location.href = "<?php echo site_url('booking/home')?>";
        }, 10000);
    });

    <?php endif; ?>

</script>