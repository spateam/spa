<?php $user = $this->session->userdata('login'); ?>

<center>
    <div style="height: 120px; width: 300px; margin: 50px 0px 200px 0px;">
        <p style="color:#3c2313;font-size: 25px;font-weight: 500">Verify Mobile Number</p>
        <div id="show_finish">
            <p class="login-warning" colspan="2" style="color: #3c2313"><?php echo $message; ?></p>
            <p>
                <input id="mobile_field" type="input" class="form-control input"/>
            </p>
            <button id="verify_mobile_number" type="submit" class="btn pull-right">Verify Mobile Number</button>
            <input type="hidden" id="cop" value="<?php echo $id; ?>">
        </div>
    </div>
</center>
