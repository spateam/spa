
<div class="container">
    <span>Proceed booking without registration</span>
    <hr class="hr-format"/>
    <div class="row">
        <div class="col-md-9">
            <div class="head-block">
                <table style="width:100%" class="progress-table">
                    <tr>
                        <?php foreach($steps as $key=>$step): ?>
                            <td class="menu-step text-center process passed" value="<?php echo $key?>" style="width:14.3%"><?php echo $step['text']?></td>
                        <?php endforeach; ?>
                    </tr>
                </table>
                <div id="progressbar"></div>
            </div>
            <div id="load_backward" style="width: 100%"></div>
            <div class="content">
                <?php
                if($update_app == ""){
                    foreach($steps as $key=>$step): ?>
                        <div step="<?php echo $key?>" class="row section-show hidden">

                        </div>
                    <?php 
                    endforeach;
                }
                else
                    echo $update_app; ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="head-block head-block-pad">Booking Information</div>

            <table>
                <tbody id="business_info">
                <tr>
                    <p>1. Only available date/time slots will be visible for booking</p>
                    <p>2. Only one appointment slot can be booked for each customer at each time. If you need 2 appointment slots, you need to book twice.</p>
                    <p>3. Call us at 6715 1515 or reply us by the confirmation email should you need to cancel your appointment. Customers who give us less than 12 hours cancellation notice (more than 3 times in a 6 months period) will be required to prepay to secure future advance bookings</p>
                    <p>4. Couple room booking is only available through phone booking.</p>
                    <p>5. Please call us if you need assistance. Our staff will be able to answer your calls or comments from your online bookings between 11am - 9pm.</p>
                </tr>
                    <tr>
                        <td>Operationg Hours</td>
                        <td style="padding-left: 5px;">Daily 11am - 10:30pm</td>
                    </tr>
                    <tr>
                        <td>Last appointment</td>
                        <td style="padding-left: 5px;">9pm</td>
                    </tr>
                    <tr>
                        <td>Hotline</td>
                        <td style="padding-left: 5px;">6715 1515</td>
                    </tr>
                    <tr>
                        <td>Website</td>
                        <td style="padding-left: 5px;"><a href="http://healingtouchspa.com">www.healingtouchspa.com</a></td>
                    </tr>
                </tbody>
            </table>
            <div style="clear: both;"></div>
            <div style="height: 5px;"></div>
        </div>
    </div>
    <input name="branch-chosen" id="branch-chosen" type="hidden">
    <input name="service-chosen" id="service-chosen" type="hidden">
    <hr class="hr-format"/>
</div>
<script>
    promotion = '';//<?php echo '';// $booking_data->promotion_url; ?>;
    br_id_url = '<?php echo (isset($booking_data->branch_id) && $booking_data->branch_id != '' ? $booking_data->branch_id : false); ?>';
    sv_id_url = '<?php echo (isset($booking_data->service_id) && $booking_data->service_id != '' ? $booking_data->service_id : true); ?>';
    sv_nm_url = '';<?php echo '';//$sv_nm_url; ?>
    br_nm_url = '';<?php echo '';//$br_nm_url; ?>

    sessionStorage.setItem('prepareData', '<?php echo json_encode($prepareData); ?>');
    message = "<?php echo str_replace("\n", '<br />',$config); ?>";
</script>