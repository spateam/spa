<div class="col-md-12">
    <div class="title-step" style="text-align: center; width: 100%; padding: 20px; font-size: 17px;">Are you sure you want to cancel this appointment?</div>
    <div style="width: 100%; display: inline-block;">
        <div style="float:left;  font-size: 16px; color: #000; margin-bottom: 5px;">
            Appointment details
        </div>        
    </div>
        <table style="font-size: 18px;width: 100%;display: table;">
        <tbody>
        <tr>
            <td style="height: 30px;" colspan="3"></td>
        </tr>
        <tr>
            <td valign="top" style="width: 150px; padding-top:10px;font-size: 14px;"><b>Your Detail</b></td>
            <td valign="top" style="padding-top:10px;">
                <div>
                    <p style="font-weight: bold;color: #b91e22;font-size: 16px;line-height: 30px;">
                    <?php date_default_timezone_set('Asia/Singapore');
                    $this->session->set_userdata(array('cancel_app_id' => $app_details[0]->bid));
                    $date_start = new DateTime(get_user_date($app_details[0]->start_time,'','Y-m-d H:i:s'));
                        echo $date_start->format('h:i A')?>, <?php echo $date_start->format('M')?> <?php echo $date_start->format('d')?>, <?php echo $date_start->format('Y')?>
                    </p>
                </div>
                <div style="font-size: 12px;color: #3c2313">
                    <div class="review_detail_ctm"><span><?php echo $app_details[0]->cfirstname.' '.$app_details[0]->clastname; ?></span></div>
                    <div class="review_detail_ctm"><span><?php echo $app_details[0]->branch_name; ?></span></div>
                    <div class="review_detail_ctm"><span>With <?php echo $app_details[0]->employeename; ?></span></div>
                    <?php $upper_code = strtoupper($app_details[0]->code); ?>
                    <div><span style="font-weight: normal">Reference #</span> <?php echo $upper_code ?></div>
                    <div>
                        <img class="img_barcode" src="<?php echo $baseurl."ulti/barcode?barcode={$upper_code}&text={$upper_code}&width=300&height=50"; ?>"></img>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 150px; padding-top:10px;font-size: 14px;"><b>Your Comment</b></td>
            <td valign="top" style="padding-top:10px;">
                <div style="font-size: 12px;color: #3c2313">
                    <div class="review_detail_ctm"><span><?php echo $app_details[0]->comment; ?></span></div>
                </div>
            </td>
        </tr>
        </tbody></table>

    <div style="margin-top: 30px;clear: both;"></div>
    <div align="right">
        <table width="150px;">
            <tr>
                <td>
                    <button onclick="cancelapp(<?php //echo $app_details[0]->bid.' '.$app_details[0]->clastname; ?>);" title="Cancel this appointment" type="button" style="font-size: 15px; margin-top: 15px;" class="btn pull-right button-color">Yes</button>
                </td>
                <td>
                    <a href="<?php echo $baseurl ?>booking/book">
                        <button type="button" style="font-size: 15px; margin-top: 15px;" class="btn pull-right button-color">No</button>
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <div style="height: 20px;"></div>
</div>
