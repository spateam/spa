<?php $reschedule = $this->session->userdata('is_reschedule');
      $is_re = empty($reschedule)?0:1;
?>
<div class="col-md-12">
    <?php if(empty($reschedule)) { ?>
            <div class="title-step" style="text-align: center; width: 100%; padding: 20px; font-size: 20px;">Please review and confirm your appointment.</div>
        <?php }
    else {?>
            <div class="title-step" style="text-align: center; width: 100%; padding: 20px; font-size: 20px;">Are you sure you want to reschedule this appointment?</div>
        <?php } ?>
    <div style="width: 100%; display: inline-block;">
        <div style="float:left;  font-size: 16px; color: #000; margin-bottom: 5px;">
            Appointment details
        </div>
        <?php if(empty($reschedule)) { ?>
            <div class="time"><h6 style="color: #000;">Slot held for</h6><div class="timeleft"> <span class="minsremaining">0:00</span></div></div>
        <?php } ?>
    </div>
    <?php $i=1; ?>
    <?php foreach($appointments as $key => $appointment): ?>
    <table style="font-size: 18px;width: 100%;display: table;">
        <tbody>
        <tr colspan="3"><b>Appointment <?php echo $i; ?></b></tr>
        <tr>
            <td valign="bottom" style="width: 150px;font-size: 14px; padding-left:20px;">Service</td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"> <?php echo $appointment['service_name']; ?> </td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(2,'<?php echo $key; ?>', <?php echo $is_re; ?>);" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="1">Edit Service</button>
            </td>
        </tr>
        <tr>
            <td valign="bottom" style="width: 150px;font-size: 14px;  padding-left:20px;">Branch</td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"> <?php echo $appointment['branch_name']; ?> </td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(1,'<?php echo $key; ?>', <?php echo $is_re; ?>);" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="2" >Edit Branch</button>
            </td>
        </tr>
        <tr>
            <td valign="bottom" style="width: 150px; font-size: 14px; padding-left:20px;">Staff</td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"><?php echo $appointment['staff_name']; ?></td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(3,'<?php echo $key; ?>',<?php echo $is_re; ?>);" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="3">Edit Staff</button>
            </td>
        </tr>
        <tr>
            <td valign="bottom" style="width: 150px; font-size: 14px; padding-left:20px;">Date Time</td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"><?php echo $appointment['date_time']; ?></td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(3,'<?php echo $key; ?>', <?php echo $is_re; ?>);" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="3">Edit Date &amp; Time</button>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;" colspan="3"></td>
        </tr>
        </tbody>
    </table>
        <?php $i++; ?>
    <?php endforeach; ?>
    <table style="font-size: 18px;width: 100%;display: table;">
        <tr>
            <td valign="top" style="width: 150px; padding-top:10px;font-size: 14px;"><b>Your Detail</b></td>
            <td valign="top" style="padding-top:10px;">
                <div style="font-size: 12px;color: #3c2313">
                    <div class="review_detail_ctm"><span><strong>Name: </strong></span><span><?php echo $ctm_information->firstname.' '.$ctm_information->lastname; ?></span></div>
                    <div class="review_detail_ctm"><span><strong>Contact Number: </strong></span><span><?php echo $ctm_information->phone; ?></span></div>
                    <div class="review_detail_ctm"><span><strong>Email: </strong></span><span><?php echo $ctm_information->email; ?></span></div>
                    <div class="review_detail_ctm"><span><strong>Comment: </strong></span><span><?php echo $ctm_information->comment; ?></span></div>
                </div>
            </td>
            <td valign="top" style="width: 150px;">
                <button style="margin-top:5px; margin-bottom: 5px;margin-left: 0px;min-width:28px; width:150px; font-size:15px" class="btn btn-primary button-color" onclick="choose_staff_indate('MORE')">Add Booking</button>

                <button onclick="temp_load_step(4,'<?php echo $key; ?>', <? echo $is_re; ?>);" type="button" style="width: 150px;" class="btn back-process button-color" value="4">Edit details</button>
            </td>
        </tr>
    </table>
    <div style="margin-top: 30px;clear: both;"></div>
    <div align="right">
        <input type="checkbox" name="agreement" id="agreement" class="policy_checkbox">
        I have read and accepted your spa policy ( <a target="_blank" style="text-decoration: none;" href="http://www.healingtouchspa.com/policy">http://www.healingtouchspa.com/policy</a> )
    </div>
    <div style="margin-top: 5px;" align="right">
        <table>
            <tr>
                <?php if(!empty($reschedule)) { ?>
                    <td style="padding-right: 5px;">
                        <div>
                            <button onclick="canceleditapp()" type="button" style="font-size: 15px; margin-top: 15px; margin-bottom: 10px;" class="btn pull-right button-color">
                                Cancel Reschedule
                            </button>
                        </div>
                    </td>
                    <?php } ?>
                <td>
                    <div>
                        <button onclick="finish()" type="button" style="font-size: 15px; margin-top: 15px; margin-bottom: 10px;" class="btn pull-right button-color">
                            Confirm Appointment
                        </button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div style="height: 10px;"></div>
</div>


