<div id="dlg_checkPIN" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Checking PIN</h4>
            </div>
            <div class="modal-body">
                <p>Enter your PIN: </p>
                <input id="input_checkPIN" type="password" />
            </div>
            <div class="modal-footer">
                <button id="btn_conf_checkingPIN" type="button" class="btn btn-default" onclick="check_Pin();">Confirm</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>