<form id="employee-chosen">
    <div class="col-md-7" id="boder-right" style="padding-left:0;">
        <div class="show-staff-datetime" style="padding:10px 0;">
        </div>
        <div id="service_employee_sample" style="display:none;">
            <div class="col-md-5 service_label"></div>
            <div class="col-md-7 employee_chosen_zone">
                <label class="service_label" for="">Choose Employee</label>
                <div class="employee_dpl">
                    <select id="" name="" class="choose_employee_select"></select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5" id="boder-left">
        <div style="padding-bottom: 5px;" class="booking-choose-date-ms"></div>
        <div id="no_slot_time" style="display:none;">No available slots now.</div>
        <input name="time-chosen" id="time-chosen" type="hidden">
        <div class="booking-next-btn" style="margin-bottom: 15px;">
            <div style="margin-top:5px; margin-left: 0px;min-width:28px; font-size:15px" class="btn btn-primary button-color" onclick="load_step(2);">Back</div>
        </div>
    </div>

</form>

<script>
    var current_time = '<?php echo $current_date ?>';
    var branch_offline_data;
    var chosen_day;
    var chosen_date;
    var chosen_time;
    var service_duration;
    var allowTimes = new Array();
    $(function(){
    });

    function reload_time_slot(dateText,branch_id,service_id,employee_id){
        if(service_id == ''){
            service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
        }
        if(branch_id == ''){
            branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
        }

        var date_string = moment(dateText).format('YYYY-MM-DD HH:mm');
        $.system_process({
            url     : booking_url + '/book/get_available_timeslot',
            param   : {date_string:date_string ,local_timezone: moment.parseZone(moment()).utcOffset(), service_id:service_id , branch_id:branch_id , employee_id:employee_id},
            async   : true,
            success : function(ret,more){
                if(ret['data']){
                    $('#no_slot_time').hide();
                    $('#time-chosen').datetimepicker({
                        value : moment(date_string).format('YYYY-MM-DD'),
                        minDate     : ret['data']['minDate'],
                        maxDate     : ret['data']['maxDate'],
                        disabledDates: ret['data']['disabledDates'],
                        allowTimes:ret['data']['allowTimes']
                    });
                    if (ret['data']['allowTimes'].length != 0) {
                        setTimeout(function () {
                            $(".xdsoft_time_variant").find("div:contains('"+ret['data']['allowTimes'][0]+"')").addClass('xdsoft_current');
                        }, 700);
                    }
                }
                else{
                    $('#no_slot_time').show();
                    $('.xdsoft_datetimepicker.xdsoft_.xdsoft_noselect.xdsoft_inline').remove()
                }
            }
        });
    }

    function onChangeTime(dateText){
        var date_string = moment(dateText).format(moment_date_format);
        $('#time-chosen').val(date_string);
        sessionStorage.setItem('select_time',1);
    }

    function get_avail_employee(){
        var service_id = $('#service-chosen').val();
        if(service_id == ''){
            service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
        }
        var branch_id = $('#branch-chosen').val();
        if(branch_id == ''){
            branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
        }

        $.system_process({
            url     : booking_url + '/book/get_avail_employee',
            param   : {service_id:service_id,branch_id:branch_id},
            async   : false,
            success : function(ret,more){
                var data = ret['data'];
                $('.show-staff-datetime').html('');
                if(data['msg']){
                    var elem = $('#service_employee_sample').clone().show();
                    elem.find('.employee_chosen_zone').html(data['msg']);
                    $('.show-staff-datetime').append(elem);
                }else{
                        var row = clone_service_employee_chosen(data,'employee_id');
                        $('.show-staff-datetime').append(row);
                    $('.show-staff-datetime').append($('<div>',{
                        class : 'row'
                    }));
                    $(".booking-next-btn").append($('<div>',{
                        style   : "margin-top:5px; margin-left: 0px;min-width:28px; font-size:15px",
                        class   : 'btn btn-primary button-color',
                        text    : 'Next',
                        onclick : 'choose_staff_indate()'
                    }));
                    $('.booking-more-btn').append($('<div>',{
                        style   : "margin-top:5px; margin-left: 0px;min-width:28px; font-size:15px",
                        class   : 'btn btn-primary button-color',
                        text    : 'Add New Booking',
                        onclick : 'choose_staff_indate("MORE")'
                    }));
                }
            }
        });
    }

    function clone_service_employee_chosen(data,name){
        var row_container = $('#service_employee_sample').clone().show();
        row_container.attr('id','');
        row_container.find('.service_label:first').html(sessionStorage.getItem('service_name'));

        var select = row_container.find('select:first'); select.attr('name',name).attr('id','employee_id_choosen');
        var label  = row_container.find('label:first'); label.attr('target','employee_id_'+data.id);
        var option = $('<option/>',{
            value: -1,
            text : 'No preference'
        });
        var option1 = $('<option/>',{
            value: -2,
            text: 'Please choose...'
        });
        select.append(option1);
        select.append(option);
        $.each(data,function(key,value){
            var option = $('<option/>',{
                value : value['id'],
                text  : value['first_name']
            });
            select.append(option);
        });
        return row_container;
    }

    function filter_time(options){
        var default_options = {
            time_range  : {start : '00:00', end: '23:59'},
            time_exclude: []
        };

        $.extend(default_options, options);

        var time_range = default_options['time_range'];
        time_range['start']=time_range['start'].replace(':','.');

        var _hour = Math.round(time_range['start']);
        var _10minute = Math.round(parseInt(time_range['start'].substr(3,2)));

        var filter_array = [];
        var i = 0;
        while(to_time_format(_hour + ':' + _10minute) < time_range['end']){
            if(_10minute == 60){
                _hour += 1;
                _10minute = 0;
            }
            var _isPass = true;
            var time_format = to_time_format(_hour+":"+_10minute);
            //Check if it is in time exclude
            options.time_exclude.forEach(function(obj){
                if(_isPass == false){
                    return;
                }
                if(obj['start'] <= time_format && time_format <= obj['end']){
                    _isPass = false;
                }
            });
            if(_isPass){
                filter_array.push(time_format);
            }
            _10minute += 10;
        }
        return filter_array;
    }

    get_avail_employee();

    $('#employee_id_choosen').change(function(){
        sessionStorage.removeItem('select_time');
        var service_id = $('#service-chosen').val();
        if(service_id == ''){
            service_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].service_id;
        }
        var branch_id = $('#branch-chosen').val();
        if(branch_id == ''){
            branch_id = JSON.parse(sessionStorage.getItem('session'))[JSON.parse(sessionStorage.getItem('currentUniqueId'))].branch_id;
        }
        var employee_id = $('#employee_id_choosen').val();
        var date_string = moment().format('YYYY-MM-DD HH:mm');
        if(employee_id == -2){
            $('.xdsoft_datetimepicker.xdsoft_.xdsoft_noselect.xdsoft_inline').remove();
            var old_element = document.getElementById("time-chosen");
            var new_element = old_element.cloneNode(true);
            old_element.parentNode.replaceChild(new_element, old_element);
            $(".booking-choose-date-ms").html("");
            return false;
        }
        $.system_process({
            url     : booking_url + '/book/get_available_timeslot',
            param   : {date_string:date_string,local_timezone: moment.parseZone(moment()).utcOffset(),service_id:service_id,branch_id:branch_id,employee_id:employee_id},
            async   : true,
            success : function(ret,more){
                console.log(ret['data']);
                if(ret['data']) {
                    if(!ret['data']['default_date']){
                        $('#no_slot_time').show();
                        $('.xdsoft_datetimepicker.xdsoft_.xdsoft_noselect.xdsoft_inline').remove()
                    }
                    else {
                        $('#no_slot_time').hide();
                        $('.xdsoft_datetimepicker.xdsoft_.xdsoft_noselect.xdsoft_inline').remove();
                        var old_element = document.getElementById("time-chosen");
                        var new_element = old_element.cloneNode(true);
                        old_element.parentNode.replaceChild(new_element, old_element);
                        branch_offline_data = '<?php json_encode($branch_offline_data)?>';
                        service_duration = '<?php $service_duration ?>';
                        $('#time-chosen').datetimepicker({
                            format: 'Y-m-d',
                            value: moment(ret['data']['default_date']).format('YYYY-MM-DD'),
                            formatDate: 'Y-m-d',
                            minDate: ret['data']['minDate'],
                            maxDate: ret['data']['maxDate'],
                            formatTime: 'H:i',
                            inline: true,
                            allowTimes: ret['data']['allowTimes'],
                            disabledDates: ret['data']['disabledDates'],
                            step: 30,
                            beforeShowDay: function (date) {
                                var timestamp_string = moment(date).format('YYYY-MM-DD HH:mm:ss');
                                var date_string = moment(date).format('YYYY-MM-DD');
                                if (timestamp_string < current_time || _.contains(branch_offline_data, date_string)) {
                                    return [false, ""]
                                } else {
                                    return [true, ""]
                                }
                            },
                            onSelectDate: function (dateText) {
                                reload_time_slot(dateText, branch_id, service_id, employee_id);
                            },
                            onChangeMonth: function(dateText){
//                                var limitDate = new Date(dateText);
//                                var maxDate = new Date(ret['data']['maxDate'] + ' 00:00:00');
//                                var minDate = new Date(ret['data']['minDate'] + ' 00:00:00');
//                                if(limitDate > maxDate || limitDate < minDate){
//                                    $('#no_slot_time').show();
//                                    $('.xdsoft_timepicker.active').hide();
//                                }
//                                else{
//                                    $('#no_slot_time').hide();
//                                    $('.xdsoft_timepicker.active').show();
//                                }
                            },
                            onSelectTime: function (dateText) {
                                onChangeTime(dateText, branch_id, service_id, employee_id);
                            }
                        });
                        $(".booking-choose-date-ms").html("Choose date and time");
                        if (ret['data']['allowTimes'].length != 0) {
                            setTimeout(function () {
                                $(".xdsoft_time_variant").find("div:contains('" + ret['data']['allowTimes'][0] + "')").addClass('xdsoft_current');
                            }, 700);
                        }
                    }
                }
                else{
                    $('#no_slot_time').show();
                    $('.xdsoft_datetimepicker.xdsoft_.xdsoft_noselect.xdsoft_inline').remove()
                }
            }
        });
    });

</script>