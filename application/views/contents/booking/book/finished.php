<div style="margin: 25px 15px 10px 15px;" id="showprocess" class="row">
    <div class="col-md-12" style="padding: 0px;margin-right: 15px;">

        <div class="title-step" style="text-align: center; width: 100%; padding: 20px; font-size: 16px;color: #593729">
            Your appointment is confirmed. Please check for confirmation email.<br>
            Please save this confirmation to your device and present it to our staff upon arrival. <br>
            Thank you and we look forward to seeing you soon! <br>
            <p>You will return to the homepage in <span id="reload_countdown">60</span>s or click <a href="http://www.healingtouchspa.com" style="color: #444444;">here</a> now</p>
        </div>
        <div style="width: 100%; border-bottom:1px solid #593729; font-size: 16px;color: #593729; margin-bottom: 5px;">
            Booking details
        </div>
        <table style="font-size: 12px;width: 100%;">
            <tbody>
            <tr>
                <td valign="bottom" style="width: 150px;font-size: 14px;"><b>Your Name</b></td>
                <td valign="bottom" style="color: #3c2313;"><?php echo $appointments[0]['customer_name']; ?> </td>
            </tr>
            <?php $i=1; ?>
            <?php foreach($appointments as $appointment): ?>
                <tr>
                    <td valign="bottom" style="width: 150px; padding-left:20px;"><b>Appointment <?php echo $i; ?></b></td>
                    <td valign="bottom" style="color: #3c2313;"><?php echo  $appointment['branch_name']; ?> </td>
                </tr>
                <tr>
                    <td valign="bottom" style="width: 150px;"></td>
                    <td valign="bottom" style="color: #3c2313;"><?php echo $appointment['service_name']; ?> </td>
                </tr>
                <tr>
                    <td valign="bottom" style="width: 150px;"></td>
                    <?php if($appointment['no_preference'] == 1)
                             $staff_name = 'No Preference';
                          else
                             $staff_name = $appointment['staff_name']; ?>
                    <td valign="bottom" style="color: #3c2313;"><?php echo $staff_name; ?></td>
                </tr>

                <tr>
                    <td valign="bottom" style="width: 150px;"></td>
                    <td valign="bottom" style="color: #3c2313;"><?php echo $appointment['date_time']; ?></td>
                </tr>

                <tr>
                    <td valign="bottom" style="width: 150px;"></td>
                    <td valign="bottom" style="color: #3c2313;">
                        Reference #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i><?php echo strtoupper($appointment['code']) ?></i>
                    </td>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>

            <tr>
                <td style="height: 30px;" colspan="3"></td>
            </tr>

            <tr style="border-top:#593729 1px solid;">
                <td valign="top" style="width: 150px;font-size: 14px;">Your comment</td>
                <td valign="top"><?php echo $ctm_information->comment; ?></td>
            </tr>
            </tbody>
        </table>

        <div style="margin-top: 30px;">
            <?php $i=1; foreach($appointments as $item): ?>
            <button onclick="Save_booking('<?php echo $item['code']; ?>')" type="button" style="font-size: 15px;margin-left: 10px; color:#2d2b2b; font-weight: bold;" class="btn pull-right">
                Save to Device Appointment <?php echo $i; ?>
            </button>
            <?php $i++; endforeach; ?>
        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-7">
            </div>
        </div>
    </div>
</div>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}
,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-53866229-1', 'auto');
ga('send', 'pageview');

(function(){
     setInterval(function(){
         if(parseInt($('#reload_countdown').text()) === 0){
             location.href = BASE_URL + 'booking';
         }
         else{
             $('#reload_countdown').text(parseInt($('#reload_countdown').text()) - 1);
         }
     },1000);
})();
</script>
