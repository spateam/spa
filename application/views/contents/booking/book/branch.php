<div class="col-md-8" id="boder-right">
    <h3 class="title-step">Please choose a branch</h3>
    <div class="items">
        <?php foreach($branches as $branch): ?>
        	<?php if($branch->active == 1) { ?>
	            <div class="child-item branch_id_<?php echo $branch->id; ?>" <?php if($branch->active == 1) echo 'onclick="choose_branch('. $branch->id .',this)"'; ?> branch-id="<?php echo $branch->id?>">
	                <div class="sub-title-content"><strong><?php echo ucfirst($branch->name) ?></strong></div>
	                <div><?php echo $branch->address?> - <?php echo $branch->phone_number?></div>
	            </div>
            <?php } ?>
        <?php endforeach; ?>
    </div>
</div>

