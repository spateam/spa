<div class="col-md-12">
    <div class="title-step" style="text-align: center; width: 100%; padding: 20px; font-size: 20px;">Are you sure you want to reschedule this appointment?</div>
    <div style="width: 100%; display: inline-block;">
        <div style="float:left;  font-size: 16px; color: #000; margin-bottom: 5px;">
            Appointment details
        </div>
    </div>
    <?php
    /*echo "<pre>";
    print_r($app_details);
    echo "</pre>";*/
    ?>
    <table style="font-size: 18px;width: 100%;display: table;">
        <tbody>
        <tr>
            <td valign="bottom" style="width: 150px;font-size: 14px;"><b>Service</b></td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"> <?php echo $app_details[0]->service_name; ?> </td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(2,'<?php echo $key; ?>');" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="1">Edit Service</button>
            </td>
        </tr>
        <tr>
            <td valign="bottom" style="width: 150px;font-size: 14px;"><b>Branch</b></td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"> <?php echo $app_details[0]->branch_name; ?> </td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(1,'<?php echo $key; ?>');" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="2" >Edit Branch</button>
            </td>
        </tr>
        <tr>
            <td valign="bottom" style="width: 150px;"></td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"><?php echo (!empty($staff['staff_real']) && ($staff['staff_real'] == -1) ? 'No preference' :$app_details[0]->employeename); ?></td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(3,'<?php echo $key; ?>');" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="3">Edit Staff</button>
            </td>
        </tr>
        <tr>
            <td valign="bottom" style="width: 150px;"></td>
            <td valign="bottom" style="font-size: 12px;color: #3c2313"><?php echo $app_details[0]->start_time; ?></td>
            <td style="width: 150px;">
                <button onclick="temp_load_step(3,'<?php echo $key; ?>');" type="button" style="width: 150px; margin-top: 5px;" class="btn back-process button-color" value="3">Edit Date &amp; Time</button>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;" colspan="3"></td>
        </tr>
        <tr>
            <td valign="top" style="width: 150px; padding-top:10px;font-size: 14px;"><b>Your Detail</b></td>
            <td valign="top" style="padding-top:10px;">
                <div style="font-size: 12px;color: #3c2313">
                    <div class="review_detail_ctm"><span><strong>Name: </strong></span><span><?php echo $app_details[0]->cfirstname.' '.$app_details[0]->clastname; ?></span></div>
                    <div class="review_detail_ctm"><span><strong>Contact Number: </strong></span><span><?php echo $app_details[0]->cnumber; ?></span></div>
                    <div class="review_detail_ctm"><span><strong>Email: </strong></span><span><?php echo $app_details[0]->cemail; ?></span></div>
                    <div class="review_detail_ctm"><span><strong>Comment: </strong></span><span><?php echo $app_details[0]->comment; ?></span></div>
                </div>
            </td>
            <td valign="top" style="width: 150px;">
                <button onclick="load_step(4);" type="button" style="width: 150px;" class="btn back-process button-color" value="4">Edit details</button>
            </td>
        </tr>
        </tbody></table>

    <div style="margin-top: 30px;clear: both;"></div>
    <div align="right">
        <input type="checkbox" name="agreement" id="agreement" class="policy_checkbox">
        I have read and accepted your spa policy ( <a target="_blank" style="text-decoration: none;" href="http://www.healingtouchspa.com/policy">http://www.healingtouchspa.com/policy</a> )
    </div>
    <div style="margin-top: 5px;" align="right">
        <table>
            <tr>
                <td style="padding-right: 5px;">
                    <div>
                        <button onclick="canceleditapp()" type="button" style="font-size: 15px; margin-top: 15px; margin-bottom: 10px;" class="btn pull-right button-color">
                            Cancel Reschedule
                        </button>
                    </div>
                </td>
                <td>
                    <div>
                        <button onclick="finish()" type="button" style="font-size: 15px; margin-top: 15px; margin-bottom: 10px;" class="btn pull-right button-color">
                            Confirm Appointment
                        </button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div style="height: 10px;"></div>
</div>