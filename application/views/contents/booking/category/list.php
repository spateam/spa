<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/13/2014
 * Time: 2:05 PM
 */
?>
<div class="container">
    <div class="page-title category-title">
        <h2><?php echo $category_name ?></h2>
    </div>
    <div class="row">
<?php foreach($items as $item): ?>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail product-box">
                <a href="<?php echo booking_url('product?code='.$item->code)?>"><img src="<?php echo $this->assets->releaseProductImage('default.png') ?>" alt="..." class="product"></a>
                <div class="caption" style="float:left;">
                    <a class="product-title" title="<?php echo $item->name ?>" href="<?php echo booking_url('product?code='.$item->code)?>"><?php echo $item->name ?></a>
                    <p class="product-description"><?php echo $item->description ?></p>
                    <div class="price-box">
                        <?php echo to_currency($item->price)?>
                    </div>
                    <div class="button-group">
                        <a href="#add_cart" onclick="add_cart('<?php echo $item->code ?>')" class="btn btn-cart btn-product" role="button"><span class="fa fa-shopping-cart"></span></a>
                        <a href="<?php echo booking_url('product?code='.$item->code)?>" class="btn btn-product" role="button">Detail</a>
                    </div>
                </div>
            </div>
        </div>
<?php endforeach; ?>
    </div>
</div>