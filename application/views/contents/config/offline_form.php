<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/24/2014
 * Time: 2:19 PM
 */
?>
<input type="hidden" name="id" value="<?php echo isset($branch_id)?$branch_id:'';?>" class="insert form-control form-inps" id="branch_id"  />
<form class="form-horizontal">
    <div class="widget-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="offline_date" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Date:</label>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <input name="offline_date" value="<?= isset($default['date'])?$default['date']:'' ?>" class="insert form-control form-inps" id="offline_date"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="offline_from" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Time:</label>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <input key="checkbox-relate" name="offline_from" value="<?= isset($default['start_time'])?$default['start_time']:'' ?>" class="insert form-control form-inps" id="offline_from"  />
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <input key="checkbox-relate" name="offline_to" value="<?= isset($default['end_time'])?$default['end_time']:'' ?>" class="insert form-control form-inps" id="offline_to"  />
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <input onclick="branch_offline_date_checkbox_click(this)" type="checkbox" id="offline_whole_day"/> <label for="offline_whole_day">Whole day</label>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <div id="offline_add" class="btn btn-primary" onclick="add_branch_offline_date()"><span class="fa fa-plus"</span></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="form-group">
        <table id="Offline Dates" class="table table-bordered table-striped table-hover text-success text-center">
            <tr>
                <th>Delete</th>
                <th>Date</th>
                <th>From</th>
                <th>To</th>
            </tr>
            <tbody id="grid_offline">
            <?php foreach($offline_data as $row): ?>
                <tr>
                    <td><span class="btn fa fa-trash-o" onclick="delete_branch_offline_date(<?php echo $row->id ?>)"></span></td>
                    <td><?php echo get_user_date($row->start_time,'',BASE_DATE_FORMAT)?></td>
                    <td><?php echo get_user_date($row->start_time,$this->system_config->get('timezone'),'H:i')?></td>
                    <td><?php echo get_user_date($row->end_time,$this->system_config->get('timezone'),'H:i')?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</form>