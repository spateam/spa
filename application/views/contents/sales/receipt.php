<div id="content" class="clearfix sales_content_minibar">
    <div id="receipt_wrapper">
        <div id="receipt_header">
            <div id="company_name"><?php echo $branch_info->company_name ?></div>
            <div id="GST_no"> GST No.<?php if($branch_info->bill_branch_group_id == 1) echo GST_NUMBER('HTG'); else if($branch_info->bill_branch_group_id == 2) echo GST_NUMBER('HT'); else if($branch_info->bill_branch_group_id == 3) echo GST_NUMBER('SAP'); ?></div>
            <div id="location_branch_group_name"><?php echo $branch_info->branch_group_name; ?></div>
            <div id="location_name"><?php echo $branch_info->name; ?></div>
            <div id="location_address"><?php echo $branch_info->address; ?></div>
            <div id="location_phone"><?php echo (isset($branch_info->phone_number) &&!empty($branch_info->phone_number)) ? 'Tel: '.$branch_info->phone_number : ""; ?></div>
            <div id="sale_receipt">Sales Receipt</div>
            <div id="sale_time"><?php echo get_user_date($bill->created_date, '', '', true);?></div>
            <div class="pull-right">
                <button class="btn btn-primary text-white hidden-print" id="print_button" onclick="print_receipt(<?php echo $bill->id?>,<?php echo $bill->status?>)"> <?php echo "Print"?> </button>
                <br>
                <button class="btn btn-primary text-white hidden-print" id="new_sale_button_1" onclick="window.location='<?php echo site_url('sales') ?>'"> <?php echo "New Sale" ?> </button>
            </div>
        </div>
        <div id="receipt_general_info">
            <?php
            if(isset($customer)):?>
                <div><?php echo "Member ID".": ".$customer->code; ?></div>
                <div><?php echo "Customer Name".": ".to_full_name($customer->first_name,$customer->last_name); ?></div>
            <?php
            endif;
            $bill_code = strtoupper($bill->code);
            ?>
            <div><?php echo 'Sale ID: '.$bill_code; ?></div>
        </div>
        <table id="receipt_items">
            <tbody>
            <tr>
                <th class="text-center"><?php echo "Item Name"?></th>
                <th style="width:205px;" class="text-center"><?php echo "Employee" ?></th>
                <th style="width:100px;" class="text-center"><?php echo "Price"?></th>
                <th style="width:100px;" class="text-center"><?php echo "Qty" ?></th>
                <th style="width:100px;" class="text-center"><?php echo "Discount" ?></th>
                <th style="width: 200px;" class="text-center"><?php echo "Package Used"?></th>
                <th style="width:100px;" class="text-right"> <?php echo "Total"?></th>

            </tr>
            <?php
            $subtotal = 0;
            $total_item_discount = 0;
            $credit_before_first = array();
            $credit_after_last = array();
            $item_bundle = array();
            foreach($bill_items as $item):
                $serviceEmployeeString = "";
                if($item->detail->type == ITEM_TYPE('Bundle')){
                    foreach($item->employees as $sub_item_id=>$employee_list){
                        $serviceEmployeeString .= $item_map[$sub_item_id]->name. ': ';
                        $employee_array = array();
                        foreach($employee_list as $employee){
                            $employee_array[] = $employee->first_name;
                        }
                        $serviceEmployeeString .= implode(' ,',$employee_array).'<br>';
                        /**
                         * Comment out to fix bug: bundle has 3 childs item so when discount i think it discount on : main item + 3 child items it's make the 4 times discount on the bill
                         * Bug happen when we not choose employee for sub item
                         */
                        // $item_bundle[] = $item_map[$sub_item_id]->code;
                    }
                    /**
                     * Fix bug: bundle has 3 childs item so when discount i think it discount on : main item + 3 child items it's make the 4 times discount on the bill
                     * Bug happen when we not choose employee for sub item
                     */
                    foreach($item->detail->bundle->sub_items as $code => $value){
                        $item_bundle[] = $value->code;    
                    }
                }else{
                    foreach($item->employees as $employee){
                        $serviceEmployeeString .= $employee->first_name . ', ';
                    }
                }

                if(!in_array($item->detail->code, $item_bundle)){
                    $total = $item->price * $item->quantity;
                    if($item->discount_type == 1){
                        $total_item_discount += $total * $item->discount_value / 100;
                        $total = $total - $total * $item->discount_value / 100;
                    }

                    else{
                        $total = $total - $item->discount_value;
                        $total_item_discount += $item->discount_value;
                    }

                    $total = $total - $item->credit_value;
                    $subtotal += $total;
                }

                //Caculate credit before and after
                $credit_before = array();
                $credit_after = array();
                if(isset($bill_items_credits[$item->id])){
                    $bill_item_credits = $bill_items_credits[$item->id];
                    if($bill->status == BILL_STATUS('Complete')){
                        foreach($bill_item_credits as $key=>$credit_value){
                            $used = isset($credit_used[$key])?$credit_used[$key]:0;
                            $add = isset($credit_add[$key])?$credit_add[$key]:0;
                            $credit_before[$key] = $item->current_credit[$key] + $used - $add;
                            $credit_after[$key]  = $credit_before[$key] - $credit_value;
                            if(!isset($credit_before_first[$key])){
                                $credit_before_first[$key] = $credit_before[$key];
                            }
                            $credit_after_last[$key] = $credit_after[$key];
                        }
                    }else{
                        $credit_after = $credit_before;
                    }
                }else if($item->detail->type == 2){
                    if(!isset($credits[$item->item_id])){
                        $used = 0; $add = 0;
                    }else{
                        $used = isset($credit_used[$credits[$item->item_id]->id])?$credit_used[$credits[$item->item_id]->id]:0;
                        $add = isset($credit_add[$credits[$item->item_id]->id])?$credit_add[$credits[$item->item_id]->id]:0;
                    }
                    $credit_before = $item->current_credit + $used - $add;
                    if($bill->status == BILL_STATUS('Complete')){
                        $credit_after = $credit_before + $item->credit_add;
                    }else{
                        $credit_after = $credit_before;
                    }
                    if(isset($credits[$item->item_id]->id) && !isset($credit_before_first[$credits[$item->item_id]->id])){
                        $credit_before_first[$credits[$item->item_id]->id] = $credit_before;
                    }
                    if(isset($credits[$item->item_id]->id))
                        $credit_after_last[$credits[$item->item_id]->id] = $credit_after;
                }
                ?>
                <?php if($item->belong_bundle == 0){ ?>
                <tr>
                    <td ><?php echo $item->detail->name;?></td>
                    <td class="text-center"><?php echo $serviceEmployeeString;?></td>
                    <td class="text-center"><?php echo $item->price;?></td>
                    <td class="text-center"><?php echo $item->quantity;?></td>
                    <td class="text-center"><?php echo $item->discount_value . (($item->discount_type == 1) ? '%' : '$' );?></td>
                    <td class="text-center">
                        <?php if(isset($bill_item_credits)): ?>
                        <?php foreach($bill_item_credits as $credit_id=>$credit_value):?>
                            <?php if(isset($item->package_detail) && is_array($item->package_detail)):?>
                            <span><?php echo $item->package_detail[$credit_id]->name . " ( {$credit_value})" ?>&nbsp; &nbsp;</span>
                        <?php endif; endforeach; ?>
                        <?php endif; ?>
                    </td>

                    <td class="text-right"><?php echo to_currency($total);?></td>
                    </tr>
                <?php } ?>
            <?php
                if($bill->status == BILL_STATUS('Complete')){
                    if(isset($bill_items_credits[$item->id])) {
                        $bill_item_credits = $bill_items_credits[$item->id];
                        foreach($bill_item_credits as $credit_id=>$credit_value){
                            $credit_used[$credit_id] -= $credit_value;
                        }
                    }else if($item->detail->type == 2){
                        $credit_add[$credits[$item->item_id]->id]  -= $item->credit_add;
                    }
                }
            endforeach;
            $total_temp = ($subtotal - $bill->discount)>0?$subtotal-$bill->discount:0;
            $gst_value =  $total_temp * $bill->gst/100;
            $total = $total_temp + $gst_value;
            foreach($bill_payments as $payment){
                if($payment->payment_id == $this->system_config->get('cash')){
                    $total = sale_total_round($total);
                    break;
                }
            }

            ?>
            <tr style="border-top: 2px solid #000000;">
                <td colspan="6" class="text-right"><?php echo 'Sub Total'; ?></td>
                <td class="text-right"><?php echo to_currency($subtotal);?></td>
            </tr>

            <?php if(isset($total_item_discount) && $total_item_discount > 0){ ?>
                <tr>
                    <td colspan="6" class="text-right">Total Item Discount: </td>
                    <td class="text-right"><?php echo to_currency($total_item_discount); ?></td>
                </tr>
            <?php } ?>
            
            <?php if(isset($bill->discount) && $bill->discount > 0){ ?>
                <tr>
                    <td colspan="6" class="text-right">Voucher Discount: </td>
                    <td class="text-right"><?php echo to_currency( $bill->discount); ?></td>
                </tr>
            <?php } ?>

            <tr>
                <td colspan="6" class="text-right">GST <?php echo $bill->gst ?>%</td>
                <td class="text-right"><?php echo to_currency($gst_value);?></td>
            </tr>


            <tr>
                <td colspan="6" class="text-right"><?php echo "Total"; ?></td>
                <td class="text-right"><?php echo to_currency($total);?></td>
            </tr>
            <tr><td colspan="7">&nbsp;</td></tr>
            <?php
            $curr =0;
            foreach($bill_payments as $bill_payment){
                ?>
                <tr>
                    <td colspan="5" class="text-right"><?php echo "Payment"; ?></td>
                    <td class="text-right"><?php echo $bill_payment->detail->name ?> </td>
                    <td class="text-right"><?php echo to_currency( $bill_payment->amount ); ?>  </td>
                </tr>
            <?php } ?>
            <tr><td colspan="7">&nbsp;</td></tr>
            <tr>
                <td colspan="6" class="text-right"><?php echo "Change Due" ?></td>
                <td class="text-right"><?php echo to_currency($bill->amount_due); ?></td>
            </tr>
            <tr>
                <td colspan="5" class="text-right" style="vertical-align: top"><?php echo "Comment"; ?> </td>
                <td colspan="2" class="text-right"><?php echo $bill->description; ?> </td>
            </tr>
            </tbody>
        </table>
        <?php if($bill->status == BILL_STATUS('Complete')): ?>
        <div class="text-right row nomargin">
            <table class="fright" style="width: 450px;">
                <thead>
                <tr>
                    <th class="text-center">Customer Package</th>
                    <th class="text-center">Before</th>
                    <th class="text-center">After</th>
                    <th class="text-center">Expiry</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($credit_data as $credit_id=>$credit_fields):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $credit_fields->item_name?></td>
                        <?php if(isset($credit_before_first[$credit_id])):
                            if($credit_after_last[$credit_id] < 0 && $credit_after_last[$credit_id] > -0.001){
                                $credit_after_last[$credit_id] = 0;
                            }
                            ?>
                        <td class="text-center"><?php echo $credit_before_first[$credit_id]>0 ? number_format($credit_before_first[$credit_id],2):0?></td>
                        <td class="text-center"><?php echo $credit_after_last[$credit_id]<1? 0 : number_format($credit_after_last[$credit_id],2)?></td>
                        <?php else:
                            $at_current_bill_credit = $credit_fields->credit;
                            if(isset($credit_used_before[$credit_id])){
                                $at_current_bill_credit += $credit_used_before[$credit_id];
                            }if(isset($credit_add_before[$credit_id])){
                                $at_current_bill_credit -= $credit_add_before[$credit_id];
                            }

                            ?>
                            <td class="text-center"><?php  echo number_format($at_current_bill_credit,2); ?></td>
                            <td class="text-center"><?php  echo number_format($at_current_bill_credit,2); ?></td>
                        <?php endif; ?>
                        <td><?php echo get_user_date($credit_fields->end_date)?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>
        <div class="text-right row nomargin">
            <?php if(count($giftcard_data)): ?>
            <table class="fright" style="width: 450px; text-align:center;">
                <thead>
                <tr>
                    <th class="text-center">Gift Card</th>
                    <th class="text-center">Before</th>
                    <th class="text-center">After</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($giftcard_data as $giftcard): ?>
                    <tr>
                        <td><?php echo $giftcard->giftcard_code; ?></td>
                        <td><?php echo $giftcard->before_trans; ?></td>
                        <td><?php echo $giftcard->after_trans; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <div id="barcode">
            <image src="<?php echo site_url("sales/barcode?barcode={$bill_code}&text={$bill_code}&width=250&height=50") ?>"></image>
        </div>
    </div>

</div>
