<html>
<head>
    <title>POS System</title>
    <meta charset="UTF-8">
    <?php echo $this->assets->releaseSingleCss('system'); ?>
</head>
<body onload="window.print();">
    <div style="width:272px; padding-bottom:50px; font-size:12px; padding-left:10px;">
        <div style="font-size:20px; font-weight: bold; text-align:center"><?php echo $branch_info->company_name ?></div>
        <div style="font-weight: bold; text-align:center"> GST No.<?php if($branch_info->bill_branch_group_id == 1) echo GST_NUMBER('HTG'); else if($branch_info->bill_branch_group_id == 2) echo GST_NUMBER('HT'); else if($branch_info->bill_branch_group_id == 3) echo GST_NUMBER('SAP'); ?></div>
        <div style="font-weight: bold; text-align:center"><?php echo $branch_info->branch_group_name; ?></div>
        <div style="font-weight: bold; text-align:center"><?php echo $branch_info->name; ?></div>
        <div style="text-align:center"><?php echo $branch_info->address; ?></div>
        <div style="text-align:center"><?php echo (isset($branch_info->phone_number) && !empty($branch_info->phone_number)) ? 'Tel: '.$branch_info->phone_number : ""; ?></div>
        <div style="text-align:center">Sale Receipt</div>
        <div style="text-align:center"><?php echo get_user_date($bill->created_date, '', '', true); ?></div>
        <div>&nbsp</div>
        <?php if(isset($customer)):?>
            <div>Member ID:<?php echo $customer->code ?></div>
            <div>Customer Name: <?php echo to_full_name($customer->first_name,$customer->last_name);?></div>
        <?php endif; ?>
        <div>Sale ID: <?php echo $bill->code?></div>
        <div>Cashier: <?php echo to_full_name($cashier->first_name,$cashier->last_name)?></div>
        <div>&nbsp</div>
        <table style="font-size:16px">
            <thead>
            <th width="40%">Name</th>
            <th width="10%">Q</th>
            <th width="15%">-C</th>
            <th width="15%">D</th>
            <th width="20%" style="text-align:right">Total</th>
            </thead>
            <tbody>
            <?php
            $subtotal = 0;
            $credit_before_first = array();
            $credit_after_last = array();
            $total_item_discount = 0;
            foreach($bill_items as $item):
                if($item->belong_bundle == 1) {
                    $item->price = 0;
                }
                    $serviceEmployeeString = "";
                    if ($item->detail->type == ITEM_TYPE('Bundle')) {
                        foreach ($item->employees as $sub_item_id => $employee_list) {
                            $serviceEmployeeString .= $item_map[$sub_item_id]->name . ': ';
                            $employee_array = array();
                            foreach ($employee_list as $employee) {
                                $employee_array[] = $employee->first_name;
                            }
                            $serviceEmployeeString .= implode(' ,', $employee_array) . '<br>';
                        }
                    } else {
                        foreach ($item->employees as $employee) {
                            $serviceEmployeeString .= $employee->first_name . ', ';
                        }
                    }

                    $total = $item->price * $item->quantity;
                    if ($item->discount_type == 1) {
                        $total_item_discount += $total * $item->discount_value / 100;
                        $total = $total - $total * $item->discount_value / 100;
                    } else {
                        $total = $total - $item->discount_value;
                        $total_item_discount += $item->discount_value;
                    }
                    $total = $total - $item->credit_value;

                    $subtotal += $total;

                    //Caculate credit before and after
                    $credit_before = array();
                    $credit_after = array();

                    if (isset($bill_items_credits[$item->id])) {
                        $bill_item_credits = $bill_items_credits[$item->id];
                        if ($bill->status == BILL_STATUS('Complete')) {
                            foreach ($bill_item_credits as $key => $credit_value) {
                                $used = isset($credit_used[$key]) ? $credit_used[$key] : 0;
                                $add = isset($credit_add[$key]) ? $credit_add[$key] : 0;
                                $credit_before[$key] = $item->current_credit[$key] + $used - $add;
                                $credit_after[$key] = $credit_before[$key] - $credit_value;
                                if (!isset($credit_before_first[$key])) {
                                    $credit_before_first[$key] = $credit_before[$key];
                                }
                                $credit_after_last[$key] = $credit_after[$key];
                            }
                        } else {
                            $credit_after = $credit_before;
                        }


                    } else if ($item->detail->type == 2) {
                        if (!isset($credits[$item->item_id])) {
                            $used = 0;
                            $add = 0;
                        } else {
                            $used = isset($credit_used[$credits[$item->item_id]->id]) ? $credit_used[$credits[$item->item_id]->id] : 0;
                            $add = isset($credit_add[$credits[$item->item_id]->id]) ? $credit_add[$credits[$item->item_id]->id] : 0;
                        }

                        $credit_before = $item->current_credit + $used - $add;
                        if ($bill->status == BILL_STATUS('Complete')) {
                            $credit_after = $credit_before + $item->credit_add;
                        } else {
                            $credit_after = $credit_before;
                        }
                        if (isset($credits[$item->item_id]->id) && !isset($credit_before_first[$credits[$item->item_id]->id])) {
                            $credit_before_first[$credits[$item->item_id]->id] = $credit_before;
                        }
                        if (isset($credits[$item->item_id]->id))
                            $credit_after_last[$credits[$item->item_id]->id] = $credit_after;
                    }
                    ?>
                    <tr>
                        <td style="overflow:hidden;text-overflow:ellipsis;"><?php echo $item->detail->name; ?></td>
                        <td><?php echo $item->quantity; ?></td>
                        <td>
                            <?php $total_credit_item = 0;
                            if (isset($bill_item_credits)) {
                                foreach ($bill_item_credits as $credit_id => $credit_value)
                                    if (isset($item->package_detail) && is_array($item->package_detail)) {
                                        $total_credit_item += $credit_value;
                                    }
                            }
                            echo number_format($total_credit_item, 2) ?>

                        </td>
                        <td><?php $discount = $item->discount_value;
                            if ($item->discount_type == 1) {
                                $discount = number_format($discount) . '%';
                            } else {
                                $discount = to_currency($discount);
                            }
                            echo $discount ?></td>
                        <td style="text-align: right"><?php echo to_currency($total); ?></td>
                    </tr>
                    <?php
                    if ($bill->status == BILL_STATUS('Complete')) {
                        if (isset($bill_items_credits[$item->id])) {
                            $bill_item_credits = $bill_items_credits[$item->id];
                            foreach ($bill_item_credits as $credit_id => $credit_value) {
                                $credit_used[$credit_id] -= $credit_value;
                            }
                        } else if ($item->detail->type == 2) {
                            $credit_add[$credits[$item->item_id]->id] -= $item->credit_add;
                        }
                    }
            //    }
            endforeach;
            $total_temp = ($subtotal - $bill->discount)>0?$subtotal-$bill->discount:0;
            $gst_value =  $total_temp * $bill->gst/100;
            $total = $total_temp + $gst_value;
            foreach($bill_payments as $payment){
                if($payment->payment_id == $this->system_config->get('cash')){
                    $total = sale_total_round($total);
                    break;
                }
            }
            ?>
            <tr><td colspan="5" style="border-bottom:1px solid #555555; ">&nbsp;</td></tr>
            <tr><td colspan="3" style="text-align:right;">Sub Total</td><td colspan="2" style="text-align:right;"><?php echo to_currency($subtotal); ?></td></tr>
            <tr><td colspan="3" style="text-align:right;">Total Item Discount</td><td colspan="2" style="text-align:right;"><?php echo to_currency($total_item_discount); ?></td></tr>
            <tr><td colspan="3" style="text-align:right;">Voucher Dis</td><td colspan="2" style="text-align:right;"><?php echo to_currency( $bill->discount); ?></td></tr>
            <tr><td colspan="3" style="text-align:right;">GST <?php echo $bill->gst ?>%</td><td colspan="2" style="text-align:right;"><?php echo to_currency($gst_value); ?></td></tr>
            <tr><td colspan="3" style="text-align:right;">Total</td><td colspan="2" style="text-align:right;"><?php echo to_currency($total);?></td></tr>

            <?php
            foreach($bill_payments as $bill_payment){
                ?>
                <tr>
                    <td colspan="1" style=""><?php echo "Payment"; ?></td>
                    <td colspan="2" style="text-align: right"><?php echo $bill_payment->detail->name ?> </td>
                    <td colspan="2" style="text-align: right"><?php echo to_currency( $bill_payment->amount ); ?>  </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2" style="text-align: right;">Change Due</td>
                <td colspan="3" style="text-align: right;"><?php echo to_currency($bill->amount_due); ?></td>
            </tr>
            <?php if($bill->status == BILL_STATUS('Complete')): ?>
                <tr> <td colspan="5" style="border-bottom:1px solid #555555; ">&nbsp;</td></tr>
                <tr>
                    <th colspan="3">Package</th>
                    <th style="text-align: right;">B</th>
                    <th style="text-align: right;">A</th>
                </tr>
                <?php foreach($credit_data as $credit_id=>$credit_fields): ?>
                    <tr>
                        <td colspan="3" style="text-align: right;"><?php echo $credit_fields->item_name?></td>
                        <?php if(isset($credit_before_first[$credit_id])):
                            if($credit_after_last[$credit_id] < 0 && $credit_after_last[$credit_id] > -0.001){
                                $credit_after_last[$credit_id] = 0;
                            }
                            ?>
                            <td style="text-align: right;"><?php echo $credit_before_first[$credit_id]>0?number_format($credit_before_first[$credit_id],2):0?></td>
                            <td style="text-align: right;"><?php echo number_format($credit_after_last[$credit_id],2); ?></td>
                        <?php else:
                            $at_current_bill_credit = $credit_fields->credit;
                            if(isset($credit_used_before[$credit_id])){
                                $at_current_bill_credit += $credit_used_before[$credit_id];
                            }if(isset($credit_add_before[$credit_id])){
                                $at_current_bill_credit -= isset($credit_used_before[$credit_id]) ? $credit_used_before[$credit_id] : $at_current_bill_credit;
                            }
                            ?>
                            <td style="text-align: right;"><?php echo number_format($at_current_bill_credit,2); ?></td>
                            <td style="text-align: right;"><?php echo number_format($at_current_bill_credit,2); ?></td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right;">Expiry</td>
                        <td colspan="2"><?php echo get_user_date($credit_data[$credit_id]->end_date)?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th colspan="3">GiftCard</th>
                    <th style="text-align: right;">B</th>
                    <th style="text-align: right;">A</th>
                </tr>
                <?php foreach($giftcard_data as $giftcard): ?>
                    <tr>
                        <td colspan="3"><?php echo $giftcard->giftcard_code; ?></td>
                        <td style="text-align: right;"><?php echo $giftcard->before_trans; ?></td>
                        <td style="text-align: right;"><?php echo $giftcard->after_trans; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($bill->status == BILL_STATUS('Void')): ?>
                <tr><td colspan="5" style="border-bottom:1px solid #555555; ">&nbsp;</td></tr>
                <tr>
                    <td colspan="1">Void Reason:</td>
                    <td colspan="3"><?= $bill->description ?></td>
                </tr>
            <?php endif; ?>
            <?php if($bill->description != '' && $bill->status != BILL_STATUS('Void')): ?>
                <tr>
                    <td colspan="1" class="text-right" style="vertical-align: top"><?php echo "Comment"; ?> </td>
                    <td colspan="3" class="text-right"><?php echo $bill->description; ?> </td>
                </tr>
            <?php endif; ?>
            <tr><td>&nbsp</td></tr>
            </tbody>
        </table>
    </div>
</body>