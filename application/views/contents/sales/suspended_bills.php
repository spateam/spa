<div class="widget-content">
    <div class="table-responsive">
        <div id="dTable_wrapper" class="dataTables_wrapper" role="grid">
            <table class="table table-bordered table-striped table-hover data-table dataTable" id="dTable" aria-describedby="dTable_info">
                <thead>	<tr role="row">
                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Hold Bill ID: activate to sort column descending" style="width: 59px;">Hold Bill ID</th>
                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dTable" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 98px;">Date</th>
                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dTable" rowspan="1" colspan="1" aria-label="Customer: activate to sort column ascending" style="width: 46px;">Customer</th>
                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dTable" rowspan="1" colspan="1" aria-label="Comments: activate to sort column ascending" style="width: 461px;">Comments</th>
                    <th >Location</th>
                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dTable" rowspan="1" colspan="1" aria-label="Retrieve: activate to sort column ascending" style="width: 96px;">Retrieve</th>

                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="dTable" rowspan="1" colspan="1" aria-label="Delete: activate to sort column ascending" style="width: 66px;">Delete</th>
                </tr>
                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php if($bills != null){
                    foreach($bills as $bill){
                        ?>
                        <tr class="even">
                            <td style="width:10%"><?php echo $bill->id?></td>
                            <td style="width:10%"><?php echo ($bill->created_date)?></td>
                            <td style="width:20%"><?php echo isset($bill->customer_name)?$bill->customer_name:"";?></td>
                            <td style="width:20%"><?php echo $bill->description?></td>
                            <td style="width:20%"><?php echo $bill->name?></td>
                            <td style="width:10%">
                                <div class="retrieve_bill_btn btn btn-primary" bill-id="<?= $bill->id ?>">Retrieve Bill</div>
                            </td>
                            <td style="width:10%">
                                <div class="delete_bill_btn btn btn-danger" bill-id="<?= $bill->id ?>">Delete Bill</div>
                            </td>
                        </tr>
                    <?php }?>
                <?php }else{?>
                    <tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">There is no holding bill</td></tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>