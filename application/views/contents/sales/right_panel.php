<?php
$sale_session = $this->session->userdata('sale');
if($sale_session){
    $current_payment_id = isset($sale_session->current_payment) ? $sale_session->current_payment : 1;
}

?>
<ul class="list-group">
<li class="list-group-item no-padding" id="cancel_show">
    <!-- Cancel and suspend buttons -->
    <div class="sale_form_main">
        <?php if($this->cart->total()):?>
            <input type="button" class="btn btn-danger button_dangers" id="cancel_sale_button" value="Cancel bill">
            <input type="button" class="btn btn-warning warning-buttons" id="suspend_sale_button" value="Hold bill">
        <?php endif ?>
    </div>
</li>
<input type="hidden" id="error_cus" name="error_cus" value="<?php echo (isset($error) && isset($customer)== null)?$error:'';?>">
<input type="hidden" id="customer_view_id"  value="<?php echo isset($customer->id)?$customer->id:'';?>">

<?php if(isset($customer)){
    ?>
    <!-- Customer info starts here-->
    <li class="list-group-item item_tier" id="select_customer">
        <strong class="lab_cus">Customer</strong>
        <div class="row nomargin">
            <div class="clearfix" id="customer_info_shell">
                <div id="customer-info" class=" full_width_imporant">
                    <div class="stl-select">

                        <div class="cname row">
                            <div class="col-md-4 no-padding">
                                <strong class="code">Type:</strong>
                            </div>
                            <div class="col-md-4 no-padding"><span><?php echo  CUSTOMER_TYPE($customer->customer_type) ?></span></div>
                            <?php if(CUSTOMER_TYPE($customer->customer_type) == 'Guest'): ?>
                            <div class="col-md-2 no-padding"><a onclick="convert_customer(<?php echo $customer->id?>)"><i class="fa fa-user"></a></i></div>
                            <?php endif;?>
                        </div>
                        <div class="cname row">
                            <div class="col-md-4 no-padding"><strong class="code">Member ID:</strong></div>
                            <div class="col-md-8 no-padding"><span><?php echo  $customer->code ?></span></div>
                        </div>
                        <div class="cname row">
                            <div class="col-md-4 no-padding"><strong class="customer_name">Name:</strong></div>
                            <div class="col-md-8 no-padding"><span><?php echo  $customer->first_name.' '.$customer->last_name ?></span></div>
                        </div>
                        <?php if(isset($customer_credits) && count($customer_credits)):?>
                            <div>
                                <table style="width:100%">
                                    <tr>
                                        <th style="width:60%">Credit Name</th>
                                        <th>Before</th>
                                        <th>After</th>
                                    </tr>
                                    <?php foreach($customer_credits as $credit): ?>
                                        <tr>
                                            <td style="width:60%"><?= $credit->name?></td>
                                            <td><?= number_format($credit->credit,2)?></td>
                                            <td><?= number_format($credit->credit_after,2)?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php endif;?>
                        <?php if(isset($customer_giftcard) && count($customer_giftcard)):?>
                            <div>
                                <table style="width:100%">
                                    <tr>
                                        <th style="width:60%">GiftCard Code</th>
                                        <th>Before</th>
                                        <th>After</th>
                                    </tr>
                                    <?php foreach($customer_giftcard as $giftcard): ?>
                                        <tr>
                                            <td style="width:60%"><?= $giftcard->code?></td>
                                            <td><?= to_currency(round($giftcard->current_value,2)) ?></td>
                                            <td><?= to_currency(round($giftcard->giftcard_after,2)) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php endif;?>
                    </div>
                    <div id="customer-info" class=" full_width_imporant">
                        <?php if($customer->customer_permission): ?>
                        <a  onclick="update_customer(<?= $customer->id?>)" id="edit_customer" class="none btn-sm btn-primary edit_customer" title="View Customer">Detail</a>
                        <?php endif; ?>
                        <a  id="delete_customer" class="btn-sm btn-warning delete_customer">Detach</a>
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php }else{?>
    <li class="list-group-item item_tier" id="select_customer">
        <strong class="lab_cus">Select a Customer:</strong>
        <h5 class="customer-basic-information"></h5>
        <div class="row nomargin">
            <div class="clearfix" id="customer_info_shell">
                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                <input type="text" name="customer" id="customer" size="30" placeholder="Type customer name..." accesskey="c" class="ui-autocomplete-input" autocomplete="off">
                <div id="add_customer_info">
                    <div id="common_or" class="common_or">
                        OR
                        <a href="<?= site_url('customers') ?>" class="btn btn-primary none" title="New Customer" id="new-customer">New Customer</a>
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php } ?>
<li class="list-group-item no-padding" >
    <div id='sale_details'>
        <table id="sales_items" class="table">
            <tr class="warning">
                <td class="left">Items In Cart:</td>
                <td class="right"><?= $cart_data->total_items ?></td>
            </tr>

            <tr>
                <td class="left">Sub Total:</td>
                <td class="right"> <?= to_currency($cart_data->subtotal,2) ?></td>
            </tr>

            <?php if(isset($cart_data->voucher_discount) && $cart_data->voucher_discount != 0): ?>
                <tr>
                    <td class="left">Voucher Discount:</td>
                    <td class="right"> <?= to_currency($cart_data->voucher_discount,2) ?><span onclick="clear_discount()" style="cursor:pointer" class="fa fa-trash-o">&nbsp</span></td>
                </tr>
            <?php endif;?>

            <tr id="gst">
                <td class="left">GST <?= $cart_data->gst ?>%:</td>
                <td class="right"><?= to_currency($cart_data->gst_value,2) ?></td>
            </tr>
            <tr class="success">
                <td ><h3 class="sales_totals">Total:</h3></td>
                <td ><h3 class="currency_totals"><label id="total"><?= to_currency($cart_data->total,2) ?></label></h3></td>
            </tr>
        </table>
    </div>
</li>
<?php if(isset($payment_data) && $payment_data != null){?>
    <li class="list-group-item spacing"></li>
    <li class="list-group-item no-padding">
        <div id="Payment_Types">
            <?php if(count($payment_data->payments) > 0){?>
                <table class="table">
                    <thead>
                    <tr>
                        <th id="pt_delete"></th>
                        <th id="pt_type">Type</th>
                        <th id="pt_amount">Amount</th>
                    </tr>
                    </thead>
                    <?php
                    foreach($payment_data->payments as $payment){?>
                        <tbody>
                        <tr>
                            <td id="delete_payment" value="<?php echo $payment->payment_type?>" class="delete_payment">[Delete]</td>
                            <td><?= $payment->name; ?> </td>
                            <td><?= to_currency( (isset($payment->amount_paid) && $payment->amount_paid != "") ? $payment->amount_paid : 0 ); ?></td>
                        </tr>
                        </tbody>
                    <?php }?>
                </table>
            <?php }?>
            <table id="amount_due" class="table">
                <tbody><tr class="success">
                    <td>
                        <h4 class="sales_amount_due">Amount Due:</h4>
                    </td>
                    <td>
                        <h3 class="amount_dues"><label id="amount_total_credit"><?= to_currency($payment_data->amount_due) ;?></label></h3>
                        <input type="hidden" id="amount_dues" value="<?= to_currency($payment_data->amount_due) ;?>">
                    </td>
                </tr>
                </tbody>
            </table>
            <div id="make_payment">
                <form action="<?php echo site_url('sales/add_payment'); ?>" method="post" accept-charset="utf-8" id="add_payment_form" autocomplete="off">
                    <table id="make_payment_table" class="table">
                        <tbody>
                        <tr>
                            <td>Using Gift Card Code:<div class="input-append"></td>
                            <td>
                                <select name="giftcard_list_redeem" id="giftcard_list_redeem" class="w-select">
                                    <option value="">Select...</option>
                                    <?php
                                    foreach($customer_giftcard as $giftcard){?>
                                        <option value="<?php echo $giftcard->code;?>"><?php echo $giftcard->code;?></option>
                                    <?php }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Amount Redeem:<div class="input-append"></td>
                            <td><input type="text" name="giftcard_values_redeem" id="giftcard_values_redeem" class="w-select"/></td>
                        </tr>
                        <tr>
                            <td>Voucher Discount:<div class="input-append"></td>
                            <td>
                                <input type="text" name="voucher" value="" id="voucher" class="w-select">
                            </td>
                        </tr>
                        <tr>
                            <td>Promotion Code:<div class="input-append"></td>
                            <td>
                                <input type="text" name="promotion_code" value="" id="promotion_code" class="w-select">
                            </td>
                        </tr>

                        <tr id="mpt_top">
                            <td id="add_payment_text">
                                Add Payment:
                            </td>
                            <td>
                                <select name="payment_type" id="payment_type" class="w-select">
                                    <?php
                                    foreach($payment_data->payment_types as $type){?>
                                        <option value="<?php echo $type->id;?>" <?php echo $type->id==$current_payment_id?'selected':''?>><?php echo $type->name;?></option>
                                    <?php }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td id="amount_tendered_label">Amount Paid:</td>
                            <td>
                                <input type="text" name="amount_paid" value="" id="amount_paid" accesskey="p" class="w-select">
                            </td>
                        </tr>

                        <tr id="mpt_bottom">
                            <td id="tender" colspan="2">
                                <div class="input-append">
                                    <div type="submit" class="btn btn-primary" id="add_payment_button"> Add Payment</div>
                                </div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </li>
<?php }?>
<li class="list-group-item">
    <label id="comment_label" for="comment">Comments:</label><br>
    <textarea name="comment" cols="40" rows="4" id="comment" accesskey="o" class="r-select"><?= isset($comment)?$comment:"";?></textarea>
    <?php
    $totalPayment = 0;
    if(isset($payment_data->payments) && count($payment_data->payments) > 0){
        foreach($payment_data->payments as $item){
            $totalPayment += floatval($item->amount_paid);
        }
    } else{
        $totalPayment = -1;
    } ?>

    <?php if(isset($payment_data)
          && isset($cart_data)
          && isset($payment_data->amount_due)
          && (($this->cart->total() > 0 && round($payment_data->amount_due,2) <= 0)
               || ($this->cart->total() > 0 && isset($payment_data->minimum_deposit) && $payment_data->minimum_deposit && $totalPayment >= $payment_data->minimum_deposit))){?>
        <div id="finish_sale">
            <div class="btn btn-success btn-large btn-block" id="finish_sale_btn" type="submit"> Complete Bill </div>
        </div>
    <?php }?>

</li>

<li class="list-group-item spacing"></li>
</ul>
<script>
    var condition = '<?php echo ((isset($payment) && isset($cart_data) && isset($payment_data->amount_due) && ($this->cart->total() > 0 && isset($payment_data->minimum_deposit) && $payment_data->minimum_deposit && $totalPayment >= $payment_data->minimum_deposit)) ? 1 : 0) ?>';
    var due = '<?php echo ((isset($payment_data) && isset($cart_data) && isset($payment_data->amount_due) && (($this->cart->total() > 0 && round($payment_data->amount_due,2) <= 0))) ? 1 : 0); ?>';
    if(due == 1){
        sessionStorage.setItem('is_deposit', 0);
    }
    else{
        sessionStorage.setItem('is_deposit', condition);
    }
</script>
<script>
    $a = '<?php echo isset($error) ? $error : ''; ?>';
    if($a != ''){
        alert($a);
    }
</script>
