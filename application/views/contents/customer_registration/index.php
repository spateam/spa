<!DOCTYPE html>
<html>
<head>
    <title>POS System</title>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link rel="shortcut icon" type="image/x-icon" href="http://localhost/spa/public/images/en/icon.ico">
    <meta name="description" content="Amazing site">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <link rel="stylesheet" href="http://localhost/spa/assets/core/libraries/jquery/jquery.datetimepicker.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/libraries/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/libraries/jPaginate/css/style.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/libraries/validator/validator.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/libraries/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/css/system.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/css/awesome_font.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/css/layouts/booking.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/css/headers/booking.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/css/breadcrumbs/booking.css">
    <link rel="stylesheet" href="http://localhost/spa/assets/core/css/contents/booking/book/index.css">
</head>
<body>
<div id="wf_page">
    <header>
        <div style="height: 4px; background: #593729;"></div>
        <div id="header">
            <div id="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 topbar-language">
                            <a href="http://localhost/spa/booking/home">
                                <img src="http://localhost/spa/assets/core/images/logo.png" style="width: 360px; height: 90px; margin-left: -20px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="page-wrapper" class="page-wrapper">
        <div class="main-container" style="padding-top:30px; padding-bottom:30px; min-height:760px">
            <div class="container">
                <hr class="hr-format"/>

                <input name="branch-chosen" id="branch-chosen" type="hidden">
                <input name="service-chosen" id="service-chosen" type="hidden">
                <hr class="hr-format"/>
            </div>
        </div>
    </div>

    <footer>

        <div class="page-footer">
            <div class="container">
                <div style="color: #2d2b2b;">Copyright © 2016 Healing Touch. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
</div>
<script type="text/javascript">
    var BASE_URL = 'http://localhost/spa/';
</script>
<script src="http://localhost/spa/assets/core/libraries/jquery/jquery.min.js"></script>
<script src="http://localhost/spa/assets/core/libraries/jquery/jquery.datetimepicker.js"></script>
<script src="http://localhost/spa/assets/core/libraries/jquery-ui/jquery-ui.min.js"></script>
<script src="http://localhost/spa/assets/core/libraries/validator/validator.js"></script>
<script src="http://localhost/spa/assets/core/libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="http://localhost/spa/assets/core/libraries/bootbox/bootbox.min.js"></script>
<script src="http://localhost/spa/assets/core/js/moment.js"></script>
</body>
</html>
