
<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="department_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:''; ?>"/>
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="groupname" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Name</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" id="name" name="name" value="<?php echo isset($item->name)?$item->name:'';?>" class="insert form-inps role_name" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="group" class="required col-sm-3 col-md-3 col-lg-2 control-label">Role Name</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input id="group_ids" name="group_ids" class="insert multi-select" raw-data='<?php
                        $data = array("total"=>array(),"select"=>array());
                        foreach($groups as $group){
                            $row = array();
                            $row["value"] = $group->id;
                            $row["name"] = $group->name;
                            $data["total"][] = $row;
                            if(isset($item) && in_array($group->id, $item->groups)) {
                                $data["select"][] = $row;
                            }
                        }
                        echo json_encode($data);
                        ?>'">
                    </div>
                </div>

                <?= Form_Generator::CREATE_FORM_GROUP(array(
                    'label' => array(
                        'text' => 'Serve Booking',
                    ),
                    'control' => array(
                        'id'   => 'is_serve_booking',
                        'type' => 'checkbox',
                        'attribute' => array(
                            'class' => 'insert',
                            'value' => isset($item->is_serve_booking)&&to_b($item->is_serve_booking)?1:0,
                            'unchecked-value' => 0,
                        )
                    ),
                )) ?>

                <?= Form_Generator::CREATE_FORM_GROUP(array(
                    'label' => array(
                        'text' => 'Require Account',
                    ),
                    'control' => array(
                        'id'   => 'is_account_required',
                        'type' => 'checkbox',
                        'attribute' => array(
                            'class' => 'insert',
                            'value' => isset($item->is_account_required)&&to_b($item->is_account_required)?1:0,
                            'unchecked-value' => 0,
                        )
                    ),
                )) ?>

            </div>
        </div>
    </form>
</div>
