<div class="">
    <div class="row">
        <label id="message" class="hide" style="color: rgb(36, 142, 226); margin-left:13px;display: inline-block;"><?php echo isset($message)?$message:''?></label>
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="fa fa-align-justify"></i> </span>
                    <h5>Credit Adjustment</h5>
                </div>
                <form method="post" accept-charset="utf-8" id="row_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
                    <table class="table table-responsive table-bordered">
                        <thead><tr>
                            <td>Package Name</td>
                            <td>Available Credit</td>
                            <td>Expiry Date</td>
                        </tr></thead>
                        <tbody>
                        <?php $i = 0; foreach($customer_credits as $item_id=>$credit_fields): ?>
                            <tr>
                                <td><?php echo $credit_fields->item_detail->name?></td>
                                <td><input name="credit_data[<?php echo $credit_fields->id?>][credit]" value="<?php echo $credit_fields->credit?>" disabled class="credit_value form-control" target="<?php echo $i?>"></td>
                                <td><input disabled type="hidden" class="credit_expiry" target="<?php echo $i?>" name="credit_data[<?php echo $credit_fields->id?>][expiry]"> <input target="<?php echo $i ?>" class="datepicker" value="<?php echo get_user_date($credit_fields->end_date,'','Y-m-d')?>"></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.datepicker').datepicker({dateFormat:'yy-mm-dd'});

        $('#wf_page_dialog').delegate('.datepicker','change',function(){
            try{
                var rowid           = $(this).attr('target');
                var date    = $(this).val();
                $('.credit_expiry[target='+rowid+']').val(date);
                $('.credit_expiry[target='+rowid+']').removeAttr('disabled');
            }catch(msg){
                $.msgBox({
                    type : 'error',
                    title: 'Message',
                    content: msg
                })
            }
        });

    });

</script>