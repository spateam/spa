<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="employee_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:''; ?>"/>
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="code" class="required col-sm-3 col-md-3 col-lg-2 control-label">Member ID:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" disabled name="code" value="<?php echo isset($item->code)?$item->code:'Auto Generate';?>" class="insert form-inps" id="code"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">First Name:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="first_name" value="<?php echo isset($item->first_name)?$item->first_name:'';?>" class="insert form-inps required" id="first_name"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_name" class="col-sm-3 col-md-3 col-lg-2 control-label ">Last Name:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="last_name" value="<?php echo isset($item->last_name)? $item->last_name:'';?>" class="insert form-inps" id="last_name"  />
                    </div>
                </div><div class="form-group">
                    <label for="nric" class="col-sm-3 col-md-3 col-lg-2 control-label">NRIC:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="nric" id="nric" value="<?php echo isset($item->nric)?$item->nric:'';?>" class="insert form-inps"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="gender" class="col-sm-3 col-md-3 col-lg-2 control-label ">Gender:</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="gender" class="insert drop-style" >
                            <option value="1" <?= isset($item->gender)&&$item->gender=="1"?"selected":""?>>Male</option>
                            <option value="2" <?= isset($item->gender)&&$item->gender=="2"?"selected":""?>>Female</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_name" class=" col-sm-3 col-md-3 col-lg-2 control-label ">DOB:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" id="birthday" name="birthday" class="insert" value="<?php echo isset($item->birthday)&&$item->birthday !=''? $item->birthday: date('Y-m-d',time());?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 col-md-3 col-lg-2 control-label">Primary E-Mail:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="email" value="<?php echo isset($item->email)? $item->email:'';?>" class="insert form-inps email-validate" id="email"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="recovery_email" class="col-sm-3 col-md-3 col-lg-2 control-label">Secondary E-Mail:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="recovery_email" value="<?php echo isset($item->email)? $item->recovery_email:'';?>" class="insert form-inps email-validate" id="recovery_email"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile_number" class="col-sm-3 col-md-3 col-lg-2 control-label required">Mobile Number:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="mobile_number" value="<?php echo isset($item->mobile_number)?$item->mobile_number:'';?>" class="insert form-inps required" id="mobile_number"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone_number" class="col-sm-3 col-md-3 col-lg-2 control-label ">Phone Number:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="phone_number" value="<?php echo isset($item->phone_number)?$item->phone_number:'';?>" class="insert form-inps" id="phone_number"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-3 col-md-3 col-lg-2 control-label ">Address:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="address" value="<?php echo isset($item->address)?$item->address:''; ?>" class="insert form-control form-inps" id="address"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="address_1" class="col-sm-3 col-md-3 col-lg-2 control-label ">Customer type:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                        <select class="insert drop-style" id="customer_type" old-value="<?= isset($item->customer_type)?$item->customer_type:0?>">
                            <option <?php echo isset($item->customer_type)&&$item->customer_type==1?'selected':'';?> value="1">Guest</option>
                            <option <?php echo isset($item->customer_type)&&$item->customer_type==2?'selected':'';?> value="2">Member</option>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    <input type="hidden" id="firstname_sale" name="firstname_sale">
    <input type="hidden" id="lastname_sale" name="lastname_sale">
    </form>
</div>


<script>
    $('#customer_type').trigger('change');
</script>
