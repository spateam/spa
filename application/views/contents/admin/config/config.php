<div class="break_line"></div>
<div class="">
    <div class="row">
        <label id="message" class="hide" style="color: rgb(36, 142, 226); margin-left:13px;display: inline-block;"><?php echo isset($message)?$message:''?></label>
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="fa fa-align-justify"></i> </span>
                    <h5>Store Configuration Information</h5>
                </div>

                <form action="<?php echo admin_url('config/save')?>" method="post" accept-charset="utf-8" id="row_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
                    <div class="row">
                        <h4 class="col-sm-3 col-md-3 col-lg-2 control-label required">Company Information</h4>
                    </div>

                    <?php foreach($company_data as $row): ?>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label required"><?php echo $row->name ?></label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_name" name="company_data[<?php echo $row->id ?>]" class="form-inps form-control" value="<?php echo $row->value?>">
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <hr class="divider">
                    <div class="row">
                        <h4 class="col-sm-3 col-md-3 col-lg-2 control-label required">Company Config</h4>
                    </div>
                    <?php foreach($data as $row) : ?>
                        <?= generate_form_control($row,false) ?>
                    <?php endforeach; ?>

                    <hr class="divider">
                    <div class="row">
                        <h4 class="col-sm-3 col-md-3 col-lg-2 control-label required">SMS Configuration</h4>
                    </div>
                    <?php foreach($sms as $row) : ?>
                        <?= generate_form_control($row,false) ?>
                    <?php endforeach; ?>

                    <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                        <div class="form-actions">
                            <button name="submitf" value="Save" id="submitf" class="submit_button btn btn-primary float_right">Submit</button>
                        </div>
                    <?php endif; ?>

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var controller = "config/";
</script>
