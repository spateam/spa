<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Customers" target="customer_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="customer_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('ex', $pageCode)): ?>
                <a class="btn btn-medium btn-primary tip-bottom" title="Export Data" id="export_customer_data" btn-type="new">
                    <span class="visible-lg">Export Data</span>
                    <i title="Export Data" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('im', $pageCode)): ?>
                <a class="btn btn-medium btn-primary tip-bottom" title="Import Data" id="import_customer_data" btn-type="new">
                    <span class="visible-lg">Import Data</span>
                    <i title="Import Data" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search customers" class="search ui-autocomplete-input" autocomplete="off" target="customer_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="customer_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="customer_list" btn-type="clear">clear search</a>
            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table id="customer_list" data-url="<?php echo admin_url('customers') ?>/" class="tablesorter table table-bordered  table-hover datatable" data-limited="true" data-limited-key="customer_global,customer_permission" data-limited-start-column="5">
                    <thead data-key="id">
                    <tr>
                        <th data-type="text-link" data-column="code" data-function="update_item" data-function-limited="insert_to_branch" data-function-params="id">Member ID</th>
                        <th data-type="text-link" data-column="first_name" data-function="update_item" data-function-limited="insert_to_branch" data-function-params="id">First name</th>
                        <th data-type="text-link" data-column="last_name" data-function="update_item" data-function-limited="insert_to_branch" data-function-params="id">Last name</th>
                        <th data-type="text" data-column="mobile_number">Mobile phone</th>
                        <th data-type="text" data-column="nric">Nric</th>
                        <th data-type="text" data-column="email">Primary Email</th>
                        <th data-type="text" data-column="recovery_email">Secondary Email</th>
                        <th data-type="text" data-column="customer_type">Type</th>
                        <th data-type="text" data-column="created_date">Created Date</th>
                        <th data-type="text" data-column="updated_date">Updated Date</th>
                        <th data-type="buttons" data-class='["fleft icon_view","fleft icon-print-img","fleft icon_action icon-edit","fleft icon_action v_survey"
                        <?=  $this->user_permission->checkPermission('epin',$pageCode)==true?',"fleft icon_action icon-pin"':'' ?>
                        <?=  $this->user_permission->checkPermission('vpin',$pageCode)==true?',"fleft icon_action icon-eye"':'' ?>
                        <?=  $this->user_permission->check_level(Permission_Value::ADMIN)==true?',"fleft  fa fa-credit-card"':'' ?>
                        <?=  $this->user_permission->check_level(Permission_Value::ADMIN)==true?',"fleft  fa fa-refresh"':'' ?>
                        <?=  $this->user_permission->check_level(Permission_Value::ADMIN)==true?',"fleft  fa icon_sms"':'' ?>]'
                            data-function='["credit_history","printCustomerCreditHistory","reset_password","view_survey","view_pin","view_pin_only","get_credit_form","get_transfer_form","send_sms_onetime_pwd"]'
                            data-element='["","","","","","","span","span",""]'
                            data-function-params='["id","id","id","id","id","id","id","id","id"]'
                            data-allow-key='["isMember","isMember","isMember","","isMember","isMember","isMember","isMember","isMember"]'
                            data-align="center">Action</th>
                        <th data-type="buttons" data-class='["fa fa-forward"]'
                            data-function='["get_choose_branch_form"]' data-function-params='["id"]' data-allow-key='[""]' data-align="center" data-tag='["<i>"]'>Branch</th>
                    </tr>
                    </thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
