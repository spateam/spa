<div class="">
    <div class="row">
        <label id="message" class="hide" style="color: rgb(36, 142, 226); margin-left:13px;display: inline-block;"><?php echo isset($message)?$message:''?></label>
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="fa fa-align-justify"></i> </span>
                    <h5>Credit Adjustment</h5>
                </div>
                <form method="post" accept-charset="utf-8" id="row_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
                    <table class="table table-responsive table-bordered">
                        <thead><tr>
                            <td>Package Name</td>
                            <td>Available Credit</td>
                            <td>Adjust</td>
                            <td>&nbsp;</td>
                            <td>Expiry Date</td>
                        </tr></thead>
                        <tbody>
                        <?php $i = 0; foreach($customer_credits as $item_id=>$credit_fields): ?>
                            <tr>
                                <td><?php echo $credit_fields->item_detail->name?></td>
                                <td><input name="credit_data[<?php echo $credit_fields->id?>][credit]" value="<?php echo $credit_fields->credit?>" disabled class="credit_value form-control" target="<?php echo $i?>"></td>
                                <td><input type="number" class="credit_adjustment form-control" target="<?php echo $i?>" value="0"></td>
                                <td><a class="credit_adjustment_submit" target="<?php echo $i?>"><i class="fa fa-2x fa-plus-circle"></i></a></td>
                                <td><input disabled type="hidden" class="credit_expiry" target="<?php echo $i?>" name="credit_data[<?php echo $credit_fields->id?>][expiry]"> <input target="<?php echo $i ?>" class="datepicker" value="<?php echo get_user_date($credit_fields->end_date,'','Y-m-d')?>"></td>
                            </tr>
                        <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.datepicker').datepicker({dateFormat:'yy-mm-dd'});

        $('#wf_page_dialog').delegate('.datepicker','change',function(){
            try{
                var rowid           = $(this).attr('target');
                var date    = $(this).val();
                $('.credit_expiry[target='+rowid+']').val(date);
                $('.credit_expiry[target='+rowid+']').removeAttr('disabled');
            }catch(msg){
                $.msgBox({
                    type : 'error',
                    title: 'Message',
                    content: msg
                })
            }
        });

        $('#wf_page_dialog').delegate('.credit_adjustment_submit','click',function(){
            try{
                var rowid           = $(this).attr('target');
                var adjust_value    = Number($('.credit_adjustment[target='+rowid+']').val());
                var credit_value    = Number($('.credit_value[target='+rowid+']').val());

                var new_credit_value = credit_value + adjust_value;
                if(new_credit_value < 0){
                    throw 'The credit value cant be lower than 0';
                }

                $('.credit_value[target='+rowid+']').val(new_credit_value);
                $('.credit_adjustment[target='+rowid+']').val(0);
            }catch(msg){
                $.msgBox({
                    type : 'error',
                    title: 'Message',
                    content: msg
                })
            }
        });
    });

</script>