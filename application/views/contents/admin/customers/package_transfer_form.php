<div class="widget-box" style="padding:15px;">
    <div class="row" style="font-size: 16px; font-weight: bold;">
        Customer Name: <?php echo to_full_name($customer->first_name,$customer->last_name); ?> <br/>
        Customer ID: <?php echo $customer->code; ?> <br/>
    </div>

    <div class="form-group">
        <form id="package_transfer">
            <input type="hidden" name="customer_id" value="<?= $customer->id ?>">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td>From Package</td>
                    <td>
                        <select  id="from_package_id" name="from_package_id" class="form-control">
                            <option value="0">Please choose a package</option>
                            <?php foreach($customer_credits as $credit_field): ?>
                                <option value="<?= $credit_field->id ?>"><?= $credit_field->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td><input readonly id="from_credit_value" name="from_credit_value" type="number" class="form-control"></td>
                </tr>
                <tr>
                    <td>To Package</td>
                    <td id="to_credit_id">
                        <select id="to_package_id" name="to_package_id" class="form-control">
                            <option value="0">Please choose a package</option>
                            <?php foreach($total_package as $package_field): ?>
                                <option value="<?= $package_field->id?>"><?= $package_field->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td><input id="to_credit_value" name="to_credit_value" type="number" class="form-control"></td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>



<script>
    var customer_credits = <?= json_encode($customer_credits) ?>;

    $('#package_transfer').delegate('#from_package_id','change',function(){
        var credit_id = $(this).val();
        $('#from_credit_value').val(customer_credits[credit_id]['credit']);
    });

    var get_credit_transfer_data = function(data){
        data['credit_transfer_data'] = $('#package_transfer').serializeJSON();
        return data;
    }

</script>