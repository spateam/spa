<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="master_warehouse_item" id="master_warehouse_item_form" class="form-horizontal">
        <input name="item_id" value="<?= $item->id ?>" type="hidden">
        <div class="form-group">
            <label class="col-sm-3 col-md-3 col-lg-2 control-label">Quantity Remaning</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="master_quantity" class="form-control" value="<?= $item->product->master_quantity ?>" readonly/>
            </div>
        </div>
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <td style="width:40%">Warehouse</td>
                <td style="width:20%">Quantity</td>
                <td style="width:20%">Add</td>
                <td style="width:20%">Remove</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach($warehouse_list as $warehouse): ?>
                <tr>
                    <td><?= $warehouse->name ?></td>
                    <td><input class="form-control" id="warehouse_quantity_<?= $warehouse->id ?>" value="<?= $warehouse->quantity ?>" readonly/></td>
                    <td><input min="0" class="form-control plus_item" target="warehouse_quantity_<?= $warehouse->id ?>" class="add_to_warehouse" type="number" name="item_change[<?= $warehouse->id ?>][plus]" previous_value="0" value="0"/></td>
                    <td><input min="0" class="form-control minus_item" target="warehouse_quantity_<?= $warehouse->id ?>" class="remove_from_warehouse" type="number" name="item_change[<?= $warehouse->id ?>][minus]" previous_value="0" value="0"/></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </form>
</div>

<script>
    var dialog = $('#wf_page_dialog');
    $(dialog).delegate('.plus_item','change',function(){
        var plus_value      = Number($(this).val());
        var previous_value  = Number($(this).attr('previous_value'));
        var plus_change     = plus_value - previous_value;
        var master_quantity_elem = $('#master_quantity');
        var master_quantity = Number(master_quantity_elem.val());
        var warehouse_quantity_elem = $('#'+$(this).attr('target'));
        var warehouse_quantity = Number(warehouse_quantity_elem.val());
        if(warehouse_quantity + plus_change >= 0){
            warehouse_quantity_elem.val(warehouse_quantity + plus_change);
            master_quantity_elem.val(master_quantity - plus_change);
            $(this).attr('previous_value',plus_value);
        }else{
            $(this).val(previous_value);
        }

    });


    $(dialog).delegate('.minus_item','change',function(){
        var minus_value      = Number($(this).val());
        var previous_value   = Number($(this).attr('previous_value'));
        var minus_change     = minus_value - previous_value;

        var master_quantity_elem = $('#master_quantity');
        var master_quantity = Number(master_quantity_elem.val());

        var warehouse_quantity_elem = $('#'+$(this).attr('target'));
        var warehouse_quantity = Number(warehouse_quantity_elem.val());
        if(warehouse_quantity - minus_change >= 0){
            warehouse_quantity_elem.val(warehouse_quantity - minus_change);
            master_quantity_elem.val(master_quantity + minus_change);
            $(this).attr('previous_value',minus_value);
        }else{
            $(this).val(previous_value);
        }
    });

    var before_spread_item = function(){
        try{
            var master_quantity_elem = $('#master_quantity');
            if(master_quantity_elem.val() < 0){
                throw "Master Quantity can't be lower than 0";
            }
            var data = $('#master_warehouse_item_form').serializeJSON();
            return {field_post : data};
        }catch(msg){
            $.msgBox({
                type : 'error',
                content : msg
            });
            return false;
        }
    }
</script>