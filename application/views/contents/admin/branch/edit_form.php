<div class="">
    <div class="row">
        <label id="message" class="hide" style="color: rgb(36, 142, 226); margin-left:13px;display: inline-block;"><?php echo isset($message)?$message:''?></label>
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="fa fa-align-justify"></i> </span>
                    <h5>Branch Configuration Information</h5>
                </div>

                <form method="post" accept-charset="utf-8" id="row_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
                    <input id="id" type="hidden" name="id" value="<?php echo $branch_data->id ?>">
                    <div class="row">
                        <h6 class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Info</h6>
                    </div>
                    <div class="form-group">
                        <label for="branch_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Name:</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="branch_data[name]" class="insert form-inps required" id="name" value="<?php echo $branch_data->name ?>"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-3 col-md-3 col-lg-2 control-label ">Belong To:</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <select name="branch_data[branch_group_id]" class="insert form-inps" id="branch_group_id" data-source="<?php echo admin_url('branch_group/suggest')?>" data-value="<?php echo $branch_data->branch_group_id ?>">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-3 col-md-3 col-lg-2 control-label ">Address:</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="branch_data[address]" class="insert form-inps" id="address" value="<?php echo $branch_data->address?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone_number" class="col-sm-3 col-md-3 col-lg-2 control-label ">Phone Number:</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="branch_data[phone_number]" class="insert form-inps" id="phone_number" value="<?php echo $branch_data->phone_number?>"   />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-3 col-md-3 col-lg-2 control-label">E-Mail:</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="branch_data[email]"  class="insert form-inps" id="email" value="<?php echo $branch_data->email?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fax" class="col-sm-3 col-md-3 col-lg-2 control-label ">Fax:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="branch_data[fax]" class="insert form-control form-inps" id="fax"  value="<?php echo $branch_data->fax?>"/>	</div>
                    </div>
                    <div class="form-group">
                        <label for="fax" class="col-sm-3 col-md-3 col-lg-2 control-label ">Branch ID Prefix:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="branch_data[sale_id_prefix]" value="<?php echo isset($branch_data->sale_id_prefix)?$branch_data->sale_id_prefix:''; ?>" class="insert form-inps" id="sale_id_prefix"  />	</div>
                    </div>
                    <?php if($this->user_permission->check_level(Permission_Value::BRANCH_GROUP)): ?>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-lg-2 control-label required">Employees</label>
                            <div class="col-sm-9 col-md-9 col-lg-10">
                                <input name="branch_data[employees]" class="form-inps form-control insert multi-select" value='<?php echo isset($branch_data)?json_encode($branch_data->employees):'[]'?>'
                                       data-source="<?php echo admin_url('branch/get_suitable_employees')?>">
                            </div>
                        </div>
                    <?php endif; ?>
                    <hr class="divider">
                    <div class="row">
                        <h6 class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Config</h6>
                    </div>
                    <?php foreach($branch_config as $row) : ?>
                        <?= generate_form_control($row,true,$branch_data->id) ?>
                    <?php endforeach; ?>
                </form>
            </div>
        </div>
    </div>
</div>