<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('i', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="item_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->checkPermission('d', $pageCode)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="item_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search customers" class="search ui-autocomplete-input" autocomplete="off" target="item_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="item_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="item_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo admin_url('items') ?>/" id="item_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id">
                    <tr>
                        <th data-column="code"  data-type="text-link" data-function="update_item" data-function-params="id">UPC/EAN/ISBN</th>
                        <th data-column="name"  data-type="text-link" data-function="update_item" data-function-params="id">Name</th>
                        <th data-column="price" data-type="text">Price</th>
                        <th data-column="description"  data-type="text">Description</th>
                        <th data-type="text-button" data-button-text="Disable" data-else-button-text="Enable" data-function="disable_item" data-else-function="enable_item" data-function-params="id" data-function-allow-key="is_not_disable">&nbsp</th>
                        <th data-type="buttons" data-class='["fa fa-forward"]'
                            data-function='["get_choose_branch_form"]' data-function-params='["id"]' data-allow-key='[""]' data-align="center" data-tag='["<i>"]'>Branch</th>
                    </tr>
                    </thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var control = "items/";
    var empty_required_field = "<?php echo "empty_required_field" ?>";
    var update_successfully = "<?php echo "edit_confirm_password_null" ?>";
    var add_successfully = "<?php echo "edit_confirm_password_null" ?>";
    var code_exists = "<?php echo "UPC/EAN/ISBN is exist" ?>";
    var edit_item_number_null = "<?php echo "edit_item_number_null" ?>";
</script>