<div class="break_line"></div>
<div class="form-row">
    <button type="button" id="reload-table" class="btn btn-primary submit_button btn-large pull-right" style="margin-right: 52px; margin-bottom: 5px;">Reload</button>
</div>
<div class="row">
    <table id="customer-reviews" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th style="width:0.1%;">ID</th>
            <th style="width:15% ;">Created At</th>
            <th style="width:15%">Review Author</th>
            <th style="width:15%;">Review Title</th>
            <th style="width:35%;">Review Body</th>
            <th style="width:5%;">Review Rating</th>
            <th style="width:10%;">Bill Code</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Created At</th>
            <th>Review Author</th>
            <th>Review Title</th>
            <th>Review Body</th>
            <th>Review Rating</th>
            <th>Bill Code</th>
        </tr>
        </tfoot>

    </table>
</div>
<script type="text/javascript">
    var admin_site_url = "<?php echo admin_url('customer_reviews/getData') ?>";
</script>