<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 9:10 AM
 */
?>

<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="item_kit_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:''; ?>"/>
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>

        <div class="form-group">
            <label for="group_name" class="col-sm-3 col-md-3 col-lg-2 control-label">Role Name :</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="name" value="<?php echo isset($item->name)?$item->name:'';?>" class="insert required form-control" id="name"  />
            </div>
        </div>

    </form>
</div>
