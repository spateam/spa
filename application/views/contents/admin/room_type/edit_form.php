<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 10:22 AM
 */
?>
<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="room_type_Form" id="room_type_Form" class="form-horizontal">
        <div class="row">
            <legend>Room Type Form</legend>
            <div class="col-md-12">
            <?= Form_Generator::CREATE_FORM_GROUP(array(
                'label'     => array('text' => 'Code', 'class' => 'required'),
                'control'   => array('type' => 'input','class' => 'required insert','value' => $item->code,'id' => 'code')
            )) ?>
            <?= Form_Generator::CREATE_FORM_GROUP(array(
                'label'     => array('text' => 'Name', 'class' => 'required'),
                'control'   => array('type' => 'input','class' => 'required insert','value' => $item->name,'id' => 'name')
            )) ?>
            <?= Form_Generator::CREATE_FORM_GROUP(array(
                'label'     => array('text' => 'Categories', 'class' => 'required'),
                'control'   => array('type' => 'select','class' => 'required insert','value' => isset($item->category_list)?convert_to_array($item->category_list,'','id'):array(),'id' => 'category_id_list','attribute' => array(
                    'data-source' => site_url('categories/suggest?full=1'),
                    'multiple'    => 'multiple'
                ))
            )) ?>
            </div>
        </div>
        <input id="id" name="id" value="<?php echo isset($item->id)?$item->id:''?>" type="hidden">
    </form>
</div>
