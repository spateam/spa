<div id="box">
    <div class="box_title">Healing Touch</div>
    <div class="pos_login_form">
        <div class="login_form_header" style="">LOGIN</div>
        <form name="login_form">
            <div class="form_row">
                <div class="form_title">User Name:</div>
                <div class="form_input">
                    <input tabindex="1" type="text" id="username" name="username"/>
                </div>
            </div>
            <div class="form_row">
                <div class="form_title">Password:</div>
                <div class="form_input">
                    <input tabindex="2" type="password" id="password" name="password"/>
                </div>
            </div>
            <div class="form_row">
                <div class="form_title">&nbsp;</div>
                <div class="form_input">
                    <button type="submit" id="submit">Login</button>
                </div>
            </div>
            <div class="clear"></div>
        </form>
    </div>
</div>
