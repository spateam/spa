<div class="">
    <div class="row">
        <label id="message" class="hide" style="color: rgb(36, 142, 226); margin-left:13px;display: inline-block;"><?php echo isset($message)?$message:''?></label>
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="fa fa-align-justify"></i> </span>
                    <h5>Branch Group Configuration Information</h5>
                </div>

                <form method="post" accept-charset="utf-8" id="row_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
                    <div class="row">
                        <h6 class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Group Info</h6>
                    </div>
                    <input type="hidden" id="id" name="id" value="<?php echo isset($branch_group_data->id)?$branch_group_data->id:0?>">
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label required">Name</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_name" name="branch_group_data[name]" class="form-inps form-control insert required" value="<?php echo isset($branch_group_data)?$branch_group_data->name:'' ?>">
                        </div>
                    </div>
                    <?php if($this->user_permission->check_level(Permission_Value::ADMIN)): ?>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 col-lg-2 control-label required">Employees</label>
                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input id="branch_name" name="branch_group_data[employees]" class="form-inps form-control insert multi-select" value='<?php echo isset($branch_group_data)?json_encode($branch_group_data->employees):'[]'?>'
                                data-source="<?php echo admin_url('branch_group/get_suitable_employees')?>">
                        </div>
                    </div>
                    <?php endif; ?>
                    <hr class="divider">
                    <div class="row">
                        <h6 class="col-sm-3 col-md-3 col-lg-2 control-label required">Branch Group Config</h6>
                    </div>
                    <?php foreach($branch_group_config as $row) : ?>
                        <?= generate_form_control($row,true) ?>
                    <?php endforeach; ?>
                </form>
            </div>
        </div>
    </div>
</div>