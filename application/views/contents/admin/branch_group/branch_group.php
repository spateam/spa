<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->check_level(Permission_Value::ADMIN)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="item_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
            <?php if($this->user_permission->check_level(Permission_Value::ADMIN)):?>
                <a id="delete" class="btn btn-danger tip-bottom disabled" title="Delete" target="item_list" btn-type="delete"><i title="Delete" class="fa fa-trash-o tip-bottom hidden-lg fa fa-2x"></i><span class="visible-lg">Delete</span></a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search customers" class="search ui-autocomplete-input" autocomplete="off" target="item_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="item_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="item_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo admin_url('branch_group') ?>/" id="item_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id">
                    <tr>
                        <th data-column="name"                  data-type="text-link"  data-function="update_item" data-function-params="id">Name</th>
                        <th data-column="created_date"               data-type="text-link"  data-function="update_item" data-function-params="id">Created Date</th>
                        <th data-align="center" data-type="buttons" data-class='["glyphicon glyphicon-cog"]' data-function='["get_permission_form"]' data-function-params='["id"]'>Permission</th>
                    </tr>
                    </thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>