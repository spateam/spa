<h4><?php echo $branch_group_data->name ?></h4>
<input type="hidden" id="branch_group_id" value="<?=isset($branch_group_data)?$branch_group_data->id:'0' ?>">
<ol id='tree' data-name='permission-data'>
    <?php foreach($available_permission as $module_name=>$rows): ?>
        <li class='expanded'><?= $module_name?>
            <ol>
                <?php foreach($rows as $row):?>
                    <li class="container" permit-type='<?= $row->permission_code ?>' data-value='<?= $row->id?>' <?= isset($branch_group_permission)&&in_array($row->id,$branch_group_permission)?"data-checked='1'":""?>
                        <?php
                        if($row->global == 1){
                            echo "disabled";
                        }
                        ?>
                        ><?=$row->name?></li>
                <?php endforeach;?>
            </ol>
        </li>
    <?php endforeach; ?>
</ol>

