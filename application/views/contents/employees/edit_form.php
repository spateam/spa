<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="employee_form" id="employee_form" class="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">First Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="first_name" value="<?php echo isset($item->first_name)?$item->first_name:'';?>" class="insert form-inps required" id="first_name"  />			</div>
                </div>
                <div class="form-group">
                    <label for="last_name" class=" col-sm-3 col-md-3 col-lg-2 control-label ">Last Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="last_name" value="<?php echo isset($item->last_name)? $item->last_name:'';?>" class="insert form-inps" id="last_name"  />			</div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 col-md-3 col-lg-2 control-label required">E-Mail:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="email" value="<?php echo isset($item->email)? $item->email:'';?>" class="insert form-inps required email-validate" id="email"  />			</div>
                </div>
                <div class="form-group">
                    <label for="phone_number" class="col-sm-3 col-md-3 col-lg-2 control-label ">Phone Number:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="phone_number" value="<?php echo isset($item->phone_number)?$item->phone_number:'';?>" class="insert form-inps" id="phone_number"  />			</div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-3 col-md-3 col-lg-2 control-label ">Address 1:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="address" value="<?php echo isset($item->address)?$item->address:''; ?>" class="insert form-control form-inps" id="address"  />	</div>
                </div>

                <div class="form-group">
                    <label for="comments" class="col-sm-3 col-md-3 col-lg-2 control-label ">Comments:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                        <textarea name="comments" cols="17" rows="5"  id="comments" class="insert" ><?php echo isset($item->comments)?$item->comments:''; ?></textarea>	</div>
                </div>
            </div>
        </div>
        <div id="login_section">
            <input type="hidden" id="therapist_id" value="<?php echo implode(',',$this->load->table_model('department')->get_department_with_account_not_require()) ?>">
            <legend class="page-header text-info"> &nbsp; &nbsp; Employee Login Info</legend>
            <div class="form-group">
                <label for="username" class="col-sm-3 col-md-3 col-lg-2 control-label required">Username:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" name="username" value="<?php echo isset($item->username)?$item->username:''; ?>" id="username" class="insert form-control required"  />
                    <span id="confirmExist" class="confirmExist"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="<?php echo (isset($item->id)&&$item->id != null)?'':'required' ?> col-sm-3 col-md-3 col-lg-2 control-label">Password:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input for="password" type="password" name="password" value="" id="password" class="insert form-control cpassword-validate" target="cpassword"  />
                </div>
            </div>

            <div class="form-group">
                <label for="repeat_password" class="<?php echo (isset($item->id)&&$item->id != null)?'':'required' ?> col-sm-3 col-md-3 col-lg-2 control-label">Password Again:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input for="confirmpassword" type="password" name="cpassword" value="" id="cpassword" class="form-control cpassword" target="password" />
                </div>
            </div>
        </div>


        <div class="form-group">
            <label for="department" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Employee Role:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">

                <select id="department_id" name="department_id" class="insert drop-style get-list-manager">
                    <?php
                    foreach($departments as $department):
                        if(isset($item->department_id)&&$department->id==$item->department_id):
                            echo "<option value={$department->id} selected='selected'>{$department->name}</option>";
                        else:
                            echo "<option value={$department->id}>{$department->name}</option>";
                        endif;
                    endforeach;?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="department" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Commission Rule:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <?php echo generate_control($commissions,isset($item->commission_id) ? $item->commission_id: "", "select",array('class' => 'form-control insert', 'id' => 'commission_id', 'name' => 'commission_id'))?>
            </div>
        </div>

        <?php if(!isset($item->id) || !$item->id):
            $WORKING_DAYS = WORKING_DAYS();
            ?>
        <div class="widget-content">
            <table class="table table-bordered text-center">
                <thead>
                <tr>
                    <th width="20px">&nbsp</th>
                    <?php foreach($WORKING_DAYS as $key=>$day): ?>
                        <th width="30px">
                            <input checked id="header_date<?php echo $key ?>" header-id="<?php echo $key?>" type="checkbox" class="date-header" style="margin-right:5px;">
                            <label checked for="header_date<?php echo $key ?>"><?php echo $day?></label></th>
                    <?php endforeach ?>
                </tr>
                </thead>
                <tbody id="commission_category_list">
                <tr id="start_time_row">
                    <td>Start</td>
                    <?php foreach($WORKING_DAYS as $key=>$day): ?>
                        <td><input type="text" refer="<?php echo $key?>" name="<?php echo $key?>" value="<?php echo $this->system_config->get('start_time');?>" class="insert form-inps time-picker" id="<?php echo $key?>_start"  /></td>
                    <?php endforeach; ?>
                </tr>
                <tr id="end_time_row">
                    <td>End</td>
                    <?php foreach($WORKING_DAYS as $key=>$day): ?>
                        <td><input type="text" refer="<?php echo $key?>" name="<?php echo $key?>" value="<?php echo $this->system_config->get('end_time');?>" class="insert form-inps time-picker" id="<?php echo $key?>_end"  /></td>
                    <?php endforeach; ?>
                </tr>
                </tbody>
            </table>
        </div>
        <?php endif; ?>
        <input id="id" name="id" value="<?php echo isset($item->id)?$item->id:''?>" type="hidden">
        <input id="current_user" name="current_user" value="<?php echo isset($item->username)?$item->username:''?>" type="hidden">
    </form>
</div>

<script>
    $(function(){
        $('#department_id').trigger('change');
    });
</script>