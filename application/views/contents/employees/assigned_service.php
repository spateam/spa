<input type="hidden" name="id" value="<?php echo isset($id)?$id:'';?>" class="insert form-control form-inps" id="employee_id"  />
<form class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category</label>

        <div class="col-sm-9 col-md-9 col-lg-10">
            <select data-source="<?php echo admin_url('categories/suggest')?>?full=1" name="category_id" id="category_id"></select>
        </div>
    </div>
    <div class="form-group">
        <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label"> Add item relate </label>
        <div class="col-sm-9 col-md-9 col-lg-9">
            <select name="item_id" id="item_id" style="margin-top: 5px;"></select>
            <input type="button" class="btn" id="btn_add_all_services" value="Add all list service" style="margin-left: 15px;">
        </div>
        <div class="col-sm-9 col-md-9 col-lg-10" style="float: right;">
            <div class="widget-box align-top">
                <div class="widget-title">
						<span class="icon">
							<i class="fa fa-th"></i>
						</span>
                    <h5>Add Service</h5>
                </div>
                <div class="widget-content no-padding scrollable" style="height:500px;">
                    <input type="hidden" id="service_ids" class="insert" value='<?php
                    $json = array();
                    if(isset($assigned_service_detail))
                        foreach($assigned_service_detail as $service){
                            $json[] = $service->id;
                        }
                    echo json_encode($json);
                    ?>'/>
                    <table id="employee_service_list" class="table table-bordered table-striped table-hover text-success text-center" >
                        <tr>
                            <th>Delete</th>
                            <th>Item name</th>
                            <th>Duration</th>
                        </tr>
                        <tbody id="grid_item">
                        <?php
                        if(isset($assigned_service_detail)):
                            foreach($assigned_service_detail as $service){
                                ?>
                                <tr>
                                    <td><a onclick='return deleteAssignedService(this, <?= $service->id ?>);' ><i class="fa fa-trash-o fa fa-2x text-error"></i></a></td>
                                    <td class="modelname"><?php echo $service->name;?></td>
                                    <td class="modelname"><?php echo $service->service->duration;?></td>
                                </tr>
                            <?php }
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="application/javascript">
    $('#btn_add_all_services').click(function(){
        var get_list_service = sessionStorage.getItem('list_service_item').split(',');
        var data = $('#service_ids').val();
        data = JSON.parse(data);
        $.each(get_list_service,function(i,item){
            if(item > 0) {
                $.system_process({
                    url: url + 'items/' + 'getItem/' + item,
                    async: false,
                    success: function (ret, more) {
                        var result = ret['data'];
                        var html = "";
                        try {
                            if (result.type == 1) {
                                if (result.status != 4) {
                                    html += '<td> <a onclick="return deleteAssignedService(this, ' + result.id + ');"> <i class="fa fa-trash-o fa fa-2x text-error"></i> </a></td>';
                                    html += '<td class="modelname">' + result.name + '</td>';
                                    html += '<td class="modelname">' + result.service.duration + '</td>';
                                    var row = document.createElement('tr');
                                    $('#grid_item').append(row);
                                    $(row).html(html);
                                    data.push(item);
                                    $('#service_ids').val(JSON.stringify(data));
                                    $('#service_suggest').val('');
                                }
                            }
                        }
                        catch (msg) {
                            return false;
                        }
                    }
                });
            }
        });
    });
</script>
