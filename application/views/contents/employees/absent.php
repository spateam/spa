<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/24/2014
 * Time: 2:19 PM
 */
?>
<input type="hidden" name="id" value="<?php echo isset($id)?$id:'';?>" class="insert form-control form-inps" id="employee_id"  />
<form class="form-horizontal">
    <div class="widget-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="absent_date" class="col-sm-3 col-md-3 col-lg-2 control-label required ">From:</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <input name="absent_date" value="<?= isset($default_date['from_date_time'])?$default_date['from_date_time']:'' ?>" class="insert form-control form-inps" id="absent_from"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="absent_from" class="col-sm-3 col-md-3 col-lg-2 control-label required ">To:</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <input key="checkbox-relate" name="absent_to" value="<?= isset($default_date['to_date_time'])?$default_date['to_date_time']:'' ?>" class="insert form-control form-inps" id="absent_to"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="absent_from" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Type:</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <select class="form-control" class="insert" id="absent_type_list" name="type">
                            <?php
                            foreach($default_date['absent_type'] as $key=>$value){
                                if(isset($default_date['type'])){
                                    if($key == $default_date['type'])
                                        $select = ' selected="selected" ';
                                    else $select = '';
                                }
                                else $select = '';

                                echo '<option '.$select.' value="'.$key.'">'.$value.'</option>';
                            }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="absent_from" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Description:</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <textarea class="form-control" class="insert" id="description" name="description"><?= isset($default_date['description'])?$default_date['description']:'' ?></textarea>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <div id="absent_add" class="btn btn-primary"><span class="fa fa-plus"></span></div>
                    </div>
                </div>


                <div class="form-group ">
                    <label for="recurring" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Recurring:</label>
                    <div id="recurring_section" class="col-sm-8 col-md-8 col-lg-8">
                        <select class="form-control" id="repeat_type" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                            <option value="0" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==0?'selected':''); ?>>none</option>
                            <option value="1" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==1?'selected':''); ?>>Daily</option>
                            <option value="2" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==2?'selected':''); ?>>Weekly</option>
                            <option value="3" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==3?'selected':''); ?>>Monthly</option>
                            <!--<option value="4" >Specific Days</option> -->
                        </select>
                    </div>
                </div>

                <div id="recurring-option-section-1" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat every</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <select class="form-control" id="daily_option">
                            <?php for($i=1;$i<=6;$i++):?>
                                <option value="<?php echo $i; ?>" <?php if(isset($book) && $book->recurring_type==1) echo ($book->repeat_times==$i?'selected':''); ?>><?php if($i==1) echo $i.' day'; else echo $i.' days'; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div id="recurring-option-section-2" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat every</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <select class="form-control" id="week_option">
                            <?php for($i=1;$i<=6;$i++):?>
                                <option value="<?php echo $i; ?>" <?php if(isset($book) && $book->recurring_type==2) echo ($book->repeat_times==$i?'selected':''); ?>><?php if($i==1) echo $i.' week'; else echo $i.' weeks'; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div id="repeat-of-week" class="form-group hide">
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat on</label>
                    <div class="col-sm-3 col-md-3 col-lg-7">
                        <?php if(isset($book) && $book->recurring_type==2) $dow = explode(',',$book->repeat_on); else $dow=array(); ?>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="1" <?php if(in_array(1,$dow)) echo 'checked'; ?>>Mon</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="2" <?php if(in_array(2,$dow)) echo 'checked'; ?>>Tue</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="3" <?php if(in_array(3,$dow)) echo 'checked'; ?>>Wed</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="4" <?php if(in_array(4,$dow)) echo 'checked'; ?>>Thu</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="5" <?php if(in_array(5,$dow)) echo 'checked'; ?>>Fri</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="6" <?php if(in_array(6,$dow)) echo 'checked'; ?>>Sat</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="7" <?php if(in_array(7,$dow)) echo 'checked'; ?>>Sun</label>
                    </div>
                </div>
                <div id="recurring-option-section-3" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat every</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <select class="form-control" id="monthly_option">
                            <?php for($i=1;$i<=6;$i++):?>
                                <option value="<?php echo $i; ?>" <?php if(isset($book) && $book->recurring_type==3) echo ($book->repeat_times==$i?'selected':''); ?>><?php if($i==1) echo $i.' month'; else echo $i.' months'; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div id="repeat-of-month" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat on</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <div class="radio">
                            <label><input type="radio" name="radio_month_repeat" value="1" <?php if(isset($book) && $book->recurring_type==3) echo ($book->repeat_on==1?'checked':''); ?>>3rd Thursday of the month</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="radio_month_repeat" value="2" <?php if(isset($book) && $book->recurring_type==3) echo ($book->repeat_on==2?'checked':''); ?>>20th day of the month</label>
                        </div>
                    </div>
                </div>
                <div id="ultil-recurring-section" class="form-group hide">
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-3 control-label">Repeats</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <div class="radio">
                            <label><input type="radio" name="radio_repeat" value="1" <?php if(isset($book)) echo ($book->repeat_to==1?'checked':''); ?>>Until a date</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="radio_repeat" value="2" <?php if(isset($book)) echo ($book->repeat_to==2?'checked':''); ?> >A number of times</label>
                        </div>
                    </div>
                    <div id="ultil-recurring-time-1" class="col-sm-2 col-md-2 col-lg-3 hide">
                        <input id="ultil-date" class="form-control" value="<?php echo $start_time?>">
                    </div>
                    <div id="ultil-recurring-time-2" class="col-sm-2 col-md-2 col-lg-3 hide">
                        <div style="width: 150px;" class="col-sm-3 col-md-3 col-lg-4">
                            <select class="form-control" id="number-of-times">
                                <?php for($i=2;$i<=12;$i++):?>
                                    <option value="<?php echo $i; ?>" <?php if(isset($book)) echo ($book->repeat_to_data==$i?'selected':''); ?>><?php if($i==2) echo ' twice'; else echo $i.' times'; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <input class="hide" id="type_absent" type="input" value="">
        <input class="hide" id="booking_id" type="input" value="">
        <table id="Absent Dates" class="table table-bordered table-striped table-hover text-success text-center">
            <tr>
                <th>Delete</th>
                <th>Date & Time From</th>
                <th>Date & Time To</th>
                <th>Type</th>
            </tr>
            <tbody id="grid_absent">
            <?php foreach($absent_data as $row): ?>
            <tr>
                <td><span class="btn fa fa-trash-o" onclick="delete_absent_date(<?php echo $row->id ?>)"></span></td>
                <td><?php echo get_user_date($row->start_time,$this->system_config->get('timezone'),'Y-m-d H:i:s')?></td>
                <td><?php echo get_user_date($row->end_time,$this->system_config->get('timezone'),'Y-m-d H:i:s')?></td>
                <td><?php echo ABSENT_TYPE($row->type)?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</form>


<script>
    function change_recurring_section(){
        var select_value = $('#recurring_section select').val();
        $('div[id*="recurring-option-section-"]').addClass('hide');
        $('#repeat-of-week').addClass('hide');
        if(select_value == 0) {
            $('#ultil-recurring-section').addClass('hide');
        }
        else if(select_value == 1) {
            $('#ultil-recurring-section').removeClass('hide');
        }
        else if(select_value == 2){
            $('#repeat-of-week').removeClass('hide');
            $('#ultil-recurring-section').removeClass('hide');
        }
        else if(select_value == 3){
            $('#ultil-recurring-section').removeClass('hide');
        }
        else if(select_value == 4){
            $('#ultil-recurring-section').removeClass('hide');
        }
        if($('#ultil-recurring-section').hasClass('hide')){
            $('#ultil_recurring_time_picker').addClass('hide');
        }
        else {
            if($('input[name=radio_repeat]:checked').val() == 1){
                $('#ultil-recurring-time-1').removeClass('hide');
                $('#ultil-recurring-time-2').addClass('hide');
            }
            else if($('input[name=radio_repeat]:checked').val() == 2) {
                $('#ultil-recurring-time-1').addClass('hide');
                $('#ultil-recurring-time-2').removeClass('hide');
            }
        }
        $('#recurring-option-section-'+select_value).removeClass('hide');
    }

    $(document).ready(function(){
        var current_date = '<?php echo $start_time?>';
        var recurring_to_date = '<?php if(isset($book->repeat_to_data)) echo (strlen($book->repeat_to_data) > 5 ? $book->repeat_to_data:'') ?>';

        $('#start_time').datetimepicker({
            timepicker: true,
            datepicker: true,
            format: 'Y-m-d H:i:s',
            startDate : current_date
        });
        $('#ultil-date').datetimepicker({
            datepicker: true,
            format: 'Y-m-d H:i:s',
            value : (recurring_to_date==''?current_date:recurring_to_date)
        });
        $('#recurring_section select').change(function(){
            change_recurring_section();
        });
        $('input[name=radio_repeat]').bind('change',function(){
            if($('input[name=radio_repeat]:checked').val() == 1){
                $('#ultil-recurring-time-1').removeClass('hide');
                $('#ultil-recurring-time-2').addClass('hide');
            }
            else if($('input[name=radio_repeat]:checked').val() == 2) {
                $('#ultil-recurring-time-1').addClass('hide');
                $('#ultil-recurring-time-2').removeClass('hide');
            }
        });

        $('.dow_select[value="'+moment().format('d')+'"]').attr('checked','checked');
        $('.dow_select[value="'+moment().format('d')+'"]').attr('disabled','disabled');
        change_recurring_section();
        if(!<?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 0: 1; else echo 1; ?>){
            $('.submit_edit_create_form').addClass('hide');
        }
    });
</script>