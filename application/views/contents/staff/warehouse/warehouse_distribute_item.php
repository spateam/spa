<div class="widget-content">
    <form  method="post" accept-charset="utf-8" name="warehouse_item" id="warehouse_item_form" class="form-horizontal">
        <input name="warehouse_id" value="<?= $warehouse_id ?>" type="hidden">
        <p>Search: <input name="search_item" id="search_item" type="text" value="" class="form-control autocomplete" data-source="<?php echo site_url('staff/warehouse/suggest_item?id='.$warehouse_id) ?>"></p>
        <div class="scrollable">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th style="width:40%">Item</th>
                    <th style="width:10%">Current Quantity</th>
                    <th style="width:10%">Master Warehouse Quantity</th>
                    <th style="width:20%">Add</th>
                    <th style="width:20%">Remove</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($warehouse_item_data as $item_id => $item_fields): ?>
                    <tr id="item_num_<?= $item_id ?>">
                        <td><?= $item_fields['item_name'] ?></td>
                        <td><input class="form-control" id="warehouse_quantity_<?= $item_id ?>" value="<?= $item_fields['current_quantity'] ?>" readonly/></td>
                        <td><input class="form-control" id="master_warehouse_quantity_<?= $item_id?>" value="<?= $item_fields['master_quantity'] ?>" readonly/></td>
                        <td><input target_id="<?= $item_id ?>" min="0" class="form-control plus_item"  type="number" name="item_change[<?= $item_id ?>][plus]" previous_value="0" value="0"/></td>
                        <td><input target_id="<?= $item_id ?>" min="0" class="form-control minus_item" type="number" name="item_change[<?= $item_id ?>][minus]" previous_value="0" value="0"/></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </form>
</div>

<script>
    var dialog = $('#wf_page_dialog');
    $(dialog).delegate('.plus_item','change',function(){
        var plus_value      = Number($(this).val());
        var previous_value  = Number($(this).attr('previous_value'));
        var plus_change     = plus_value - previous_value;
        var master_quantity_elem = $('#master_warehouse_quantity_'+$(this).attr('target_id'));
        var master_quantity = Number(master_quantity_elem.val());
        var warehouse_quantity_elem = $('#warehouse_quantity_'+$(this).attr('target_id'));
        var warehouse_quantity = Number(warehouse_quantity_elem.val());
        if(warehouse_quantity + plus_change >= 0){
            warehouse_quantity_elem.val(warehouse_quantity + plus_change);
            master_quantity_elem.val(master_quantity - plus_change);
            $(this).attr('previous_value',plus_value);
        }else{
            $(this).val(previous_value);
        }
    });


    $(dialog).delegate('.minus_item','change',function(){
        var minus_value      = Number($(this).val());
        var previous_value   = Number($(this).attr('previous_value'));
        var minus_change     = minus_value - previous_value;

        var master_quantity_elem = $('#master_warehouse_quantity_'+$(this).attr('target_id'));
        var master_quantity = Number(master_quantity_elem.val());

        var warehouse_quantity_elem = $('#warehouse_quantity_'+$(this).attr('target_id'));
        var warehouse_quantity = Number(warehouse_quantity_elem.val());
        if(warehouse_quantity - minus_change >= 0){
            warehouse_quantity_elem.val(warehouse_quantity - minus_change);
            master_quantity_elem.val(master_quantity + minus_change);
            $(this).attr('previous_value',minus_value);
        }else{
            $(this).val(previous_value);
        }
    });

    var before_post_warehouse_distribution = function(){
        try{
            var master_quantity_elems = $('[id^=master_warehouse_quantity_]');
            $(master_quantity_elems).each(function(){
                if($(this).val() < 0){
                    throw "Master Quantity can't be lower than 0";
                }
            });
            var data = $('#warehouse_item_form').serializeJSON();
            return {field_post : data};
        }catch(msg){
            $.msgBox({
                type : 'error',
                content : msg
            });
            return false;
        }
    };

    $('#search_item').on('keyup keypress', function(e) {
        var code = e.keyCode || e.which;
        if($('#search_item').val() == ''){
            $('tr[id*="item_num_"]').show();
        }

        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#search_item').autocomplete({
        source: $('#search_item').attr('data-source'),
        select: function( event, ui ) {
            $('tr[id*="item_num_"]').hide();
            $('#item_num_'+ui.item.id).show();
        }
    });

</script>