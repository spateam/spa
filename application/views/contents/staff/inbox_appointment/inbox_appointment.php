<div class="break_line"></div>
<div class="form-row">
    <button type="button" id="reload-table" class="btn btn-primary submit_button btn-large pull-right" style="margin-right: 52px; margin-bottom: 5px;">Reload</button>
</div>
<div class="row">
    <table id="inbox-appointments" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="width:12%;">Date of Booking</th>
                <th style="width:10% ;">App From</th>
                <th style="width:10% ;">App To</th>
                <th style="width:10%">Name</th>
                <th style="width:10%;">ID</th>
                <th style="width:12.5%;">Mobile</th>
                <th style="width:12.5%;">Email</th>
                <th style="width:20%;">Treatment</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
            <tr>
                <th>Date of Booking</th>
                <th>App From</th>
                <th>App To</th>
                <th>Name</th>
                <th>ID</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Treatment</th>
            </tr>
        </tfoot>

    </table>
</div>
<script type="text/javascript">
    var getDataURL = "<?php echo staff_url('inbox_appointment/getData') ?>";
</script>