<?php
function to_suggestion_list($book,$list_name,$book_default,$default, $label, $value)
{
    $suggestion = array();
    $val = array('label' => '', 'value' => 0);
    if (isset($book)) {
        if((isset($book->service_list->$list_name))&&count($book->service_list->$list_name)){
            foreach ($book->service_list->$list_name as $key => $row) {
                if (isset($book->id) && $book->employee_id == $key) {
                    $val = array('label' => $row->$label, 'value' => $row->$value);
                }
                $suggestion[] = array('label' => $row->$label, 'value' => $row->$value);
            }
            if($val['value'] == 0 && isset($book_default)){
                $suggestion[] = array('label' => (isset($book_default->$label)? $book_default->$label : $book_default->first_name.' '.$book_default->last_name), 'value' => (isset($book_default->$value) ? $book_default->$value : $book_default->id));
                $val = array('label' => (isset($book_default->$label)? $book_default->$label : $book_default->first_name.' '.$book_default->last_name), 'value' => (isset($book_default->$value) ? $book_default->$value : $book_default->id));
            }
        }else if(isset($book_default)){
               $suggestion[] = array('label' => (isset($book_default->$label)? $book_default->$label : $book_default->first_name.' '.$book_default->last_name), 'value' => (isset($book_default->$value) ? $book_default->$value : $book_default->id));
               $val = array('label' => (isset($book_default->$label)? $book_default->$label : $book_default->first_name.' '.$book_default->last_name), 'value' => (isset($book_default->$value) ? $book_default->$value : $book_default->id));
        }

    } elseif (isset($default)) {
        $suggestion[] = array('label' => $default->$label, 'value' => $default->$value);
        $val = array('label' => $default->$label, 'value' => $default->$value);
    }
    return array($suggestion, $val);
}
?>

<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="employee_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($book->id)?$book->id:''; ?>"/>
        <input type="hidden" id="section_id" value="<?php echo isset($employee)?$employee->id:(isset($room->id)?$room->id:0) ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select style="width:50%" data-source="<?php echo admin_url('categories/suggest')?>?full=1" name="category_id" id="category_id" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>></select>
                    </div>
                </div>
                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Service</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select style="width:50%" id="service_id" class="insert form-control" name="service_id" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'readonly': ''; ?>></select>
                    </div>
                </div>
                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <input type="hidden" value="<?php echo isset($book->start_time)?get_user_date($book->start_time,'','Y-m-d H:i:s'):$start_time?>">
                    <label for="start_time" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Time:</label>
                    <div id="start_time_section" class="col-sm-4 col-md-4 col-lg-5">
                        <input id="start_time" class="form-control" value="<?php echo isset($book->start_time)?get_user_date($book->start_time,'','Y-m-d H:i:s'):$start_time?>" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                    </div>
                    <label for="duration" class="col-sm-2 col-md-2 col-lg-2 control-label ">Duration: <span id="duration_span"><?php echo isset($book->service_duration)?$book->service_duration:0 ?></span> minutes</label>
                    <input id="duration" class="insert required" type="hidden" value=<?php echo isset($book->service_duration)?$book->service_duration:0 ?>   >
                </div>
                <?php if(isset($book) && $book->status == 2) : ?>
                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <label for="address" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Customer booking:</label>
                    <div id="customer_temp_section" class="col-sm-8 col-md-8 col-lg-8">
                        <?php
                        $customer_temp_val = array('label' => '','value' => 0);
                        if(isset($book->customer)):
                            $customer_temp_val = array('label' => to_full_name($book->customer->first_name,$book->customer->last_name.' - '.$book->customer->mobile_number.' - '.$book->customer->email),'value' => $book->customer->id);
                        endif; ?>
                        <input disabled="disabled" id="customer_temp_id_typeahead" class="full_width_imporant autocomplete" target="customer_temp_id" data-value='<?php echo json_encode($customer_temp_val)?>' data-source=''>
                        <input id="customer_temp_id" minLength="0" class="required insert" type="hidden" name="customer_temp_id">
                    </div>
                </div>
                <?php endif ?>
                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <label for="address" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Customer confirm:</label>
                    <div id="customer_section" class="col-sm-8 col-md-8 col-lg-8">
                        <?php
                        $customer_val = array('label' => '','value' => 0);
                        if(isset($book) && $book->status != 2) :
                            if(isset($book->customer)):
                                $customer_val = array('label' => to_full_name($book->customer->first_name,$book->customer->last_name),'value' => $book->customer->id);
                            endif;
                        endif;
                        ?>
                        <input placeholder="Type customer name" id="customer_id_typeahead" class="required full_width_imporant autocomplete" target="customer_id" data-value='<?php echo json_encode($customer_val)?>' <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                        <input id="customer_id" minLength="0" class="required insert" type="hidden" name="customer_id">
                    </div>
                    <div class="col-sm-1 col-md-1 col-lg-1">
                        <a class="glyphicon glyphicon-plus <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'hide': ''; ?>" onclick="addnewcustomer()" ></a>
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Customer Information: </label>
                    <div id="customer_detail_information" class="col-sm-8 col-md-8 col-lg-8">
                        <?php if(isset($book->customer)){
                            $str = '';
                            if(isset($book->customer->email))
                                $str .= 'Email: '. $book->customer->email;
                            if(isset($book->customer->mobile_number))
                                $str .= ' - Mobile Number: '. $book->customer->mobile_number;
                            echo $str;
                        }
                        else{
                            echo '';
                        }?>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="comment" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Comment:</label>
                    <div id="description_section" class="col-sm-8 col-md-8 col-lg-8">
                        <textarea id="comment" name="comment" class="insert form-control full_width_imporant" cols="50" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>><?php echo isset($book)?$book->comment:''?></textarea>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="comment" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Avaiable comments:</label>
                    <div id="description_section" class="col-sm-8 col-md-8 col-lg-8">
                        <select class="form-control" id="avaiable_comments" onchange="update_comment(this)">
                            <option value="">Choose a comment</option>
                            <option value="Cancel Within 30 Min">Cancel Within 30 Min</option>
                            <option value="No Show">No Show</option>
                        </select>
                    </div>
                </div>

                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <table class="table table-bordered table-responsive table-sales">
                        <thead>
                        <tr>
                            <td><label class="required">Service Name</label></td>
                            <td><label class="required">Employee ID</label</td>
                            <td><label class="required">Room ID</label</td>
                        </tr>
                        <tr id="sub_item_sample" style="display:none">
                            <td class="service_name"></td>
                            <td>
                                <input placeholder="Type Employee Name" class="employee_id_typeahead full_width_imporant" data-value="" data-source="" target="" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                                <input id="" class="insert employee_id_value" type="hidden" name="">
                            </td>
                            <td>
                                <input placeholder="Type Room Name" class="room_id_typeahead full_width_imporant" data-value="" data-source="" target="" <?php if(isset($book->status)) echo  ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill')) ? 'readonly' : ''; ?>>
                                <input id="" class="insert room_id_value" type="hidden" name="">
                            </td>
                            <input class="duration" type="hidden">
                        </tr>
                        </thead>
                        <tbody id="employee_room_list">
                        <?php if(isset($book->service)):?>
                            <?php if($book->service->type == ITEM_TYPE('Bundle')) :?>
                                    <?php foreach($book->services as $service): ?>
                                    <tr>
                                        <td><?= $service->name ?></td>
                                        <td><?php
                                            if(isset($employee)){
                                                $employee->name = to_full_name($employee->first_name,$employee->last_name);
                                            }
                                            if(isset($service->employee)){
                                                $service->employee->name = to_full_name($service->employee->first_name,$service->employee->last_name);
                                            }
                                            ?>
                                            <?php list($employee_suggestion,$employee_val) = to_suggestion_list(
                                                $service,
                                                'list_employee',
                                                $service->employee,
                                                isset($employee)?$employee:null,'name','id') ?>
                                            <input <?php echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?> placeholder="Type Employee Name"  <?php echo isset($service)?'':'disabled' ?> class="employee_id_typeahead required full_width_imporant autocomplete" data-value='<?php echo json_encode($employee_val)?>' data-source='<?php echo json_encode($employee_suggestion)?>' target="employee_id_<?= $service->service_id ?>">
                                            <input <?php echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?> class="required insert" type="hidden" id="employee_id_<?= $service->service_id ?>" name="sub_service[<?= $service->service_id ?>][employee_id]"></td>
                                        <td><?php list($room_suggestion,$room_val) = to_suggestion_list(
                                                $service,
                                                'list_room',
                                                $service->room,
                                                isset($room)?$room:null,'name','id') ?>
                                            <input <?php echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'readonly': ''; ?> placeholder="Type Room Name" <?php echo isset($service)?'':'disabled' ?> class="room_id_typeahead required full_width_imporant autocomplete" data-value='<?php echo json_encode($room_val)?>' data-source='<?php echo json_encode($room_suggestion)?>' target="room_id_<?= $service->service_id ?>">
                                            <input class="required insert" type="hidden" id="room_id_<?= $service->service_id ?>" name="sub_service[<?= $service->service_id ?>][room_id]"></td>
                                        <input class="duration" type="hidden" value="<?= $service->duration ?>" name="sub_service[<?= $service->service_id ?>][duration]">
                                    </tr>
                                    <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td><?= $book->service->name ?></td>
                                    <td><?php
                                        if(isset($employee)){
                                            $employee->name = to_full_name($employee->first_name,$employee->last_name);
                                        }
                                        ?>
                                        <?php list($employee_suggestion,$employee_val) = to_suggestion_list(
                                            isset($book)?$book:array(),
                                            'list_employee',
                                            isset($book)?$book->employee:null,
                                            isset($employee)?$employee:null,'name','id') ?>
                                        <input  <?php echo isset($book)?'':'disabled' ?> class="employee_id_typeahead required full_width_imporant autocomplete" data-value='<?php echo json_encode($employee_val)?>' data-source='<?php echo json_encode($employee_suggestion)?>' target="employee_id" <?php echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                                        <input placeholder="Type Employee Name" class="required insert" type="hidden" name="employee_id" id="employee_id"></td>
                                    <td><?php list($room_suggestion,$room_val) = to_suggestion_list(
                                            isset($book)?$book:array(),
                                            'list_room',
                                            isset($book)?$book->room:null,
                                            isset($room)?$room:null,'name','id') ?>
                                        <input placeholder="Type Room Name" <?php echo isset($book)?'':'disabled' ?> class="room_id_typeahead required full_width_imporant autocomplete" data-value='<?php echo json_encode($room_val)?>' data-source='<?php echo json_encode($room_suggestion)?>' target="room_id" <?php echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                                        <input class="required insert" type="hidden" name="room_id" id="room_id" <?php echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>></td>
                                    <input class="duration" type="hidden" value="<?= $book->service_duration ?>" name="duration">
                                </tr>
                            <?php endif; ?>
                        <?php else :?>
                            <tr>
                                <td>Please select a service</td>
                                <td>
                                    <input <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?> placeholder="Type Employee Name" class="employee_id_typeahead full_width_imporant ui-autocomplete-input" data-value="" value="<?php echo to_full_name($employee->first_name,$employee->last_name); ?>" data-source="" target="employee_id" autocomplete="off">
                                    <input id="employee_id" class="insert employee_id_value required" type="hidden" name="[employee_id]" value="<?php echo $employee_id; ?>">
                                </td>
                                <td>
                                    <input <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?> placeholder="Type Room Name" class="room_id_typeahead full_width_imporant ui-autocomplete-input" data-value="" data-source="" target="room_id" autocomplete="off">
                                    <input id="room_id" class="insert room_id_value required" type="hidden" name="[room_id]" value="0">
                                </td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <label for="no_preference" class="required col-sm-3 col-md-3 col-lg-2 control-label">Request: </label>
                    <div id="no_preference_section" class="col-sm-8 col-md-8 col-lg-8">
                        <input <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?> id="no_preference" type="checkbox" <?php if(isset($book->no_preference) && $book->no_preference == 0) echo 'checked'; ?>/>
                    </div>
                </div>

                <div class="form-group <?php if(isset($book->status)){ echo $book->status == BOOKING_STATUS('Comment') ? 'hide' : ''; } ?>">
                    <label for="recurring" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Recurring:</label>
                    <div id="recurring_section" class="col-sm-8 col-md-8 col-lg-8">
                        <select class="form-control" id="repeat_type" <?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('Payment') || $book->status == BOOKING_STATUS('HoldBill'))? 'disabled': ''; ?>>
                            <option value="0" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==0?'selected':''); ?>>none</option>
                            <option value="1" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==1?'selected':''); ?>>Daily</option>
                            <option value="2" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==2?'selected':''); ?>>Weekly</option>
                            <option value="3" <?php if(isset($book->recurring_type)) echo ($book->recurring_type==3?'selected':''); ?>>Monthly</option>
                            <!--<option value="4" >Specific Days</option> -->
                        </select>
                    </div>
                </div>
                <div id="recurring-option-section-1" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat every</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <select class="form-control" id="daily_option">
                            <?php for($i=1;$i<=6;$i++):?>
                                <option value="<?php echo $i; ?>" <?php if(isset($book) && $book->recurring_type==1) echo ($book->repeat_times==$i?'selected':''); ?>><?php if($i==1) echo $i.' day'; else echo $i.' days'; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div id="recurring-option-section-2" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat every</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <select class="form-control" id="week_option">
                            <?php for($i=1;$i<=6;$i++):?>
                                <option value="<?php echo $i; ?>" <?php if(isset($book) && $book->recurring_type==2) echo ($book->repeat_times==$i?'selected':''); ?>><?php if($i==1) echo $i.' week'; else echo $i.' weeks'; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div id="repeat-of-week" class="form-group hide">
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat on</label>
                    <div class="col-sm-3 col-md-3 col-lg-7">
                        <?php if(isset($book) && $book->recurring_type==2) $dow = explode(',',$book->repeat_on); else $dow=array(); ?>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="1" <?php if(in_array(1,$dow)) echo 'checked'; ?>>Mon</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="2" <?php if(in_array(2,$dow)) echo 'checked'; ?>>Tue</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="3" <?php if(in_array(3,$dow)) echo 'checked'; ?>>Wed</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="4" <?php if(in_array(4,$dow)) echo 'checked'; ?>>Thu</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="5" <?php if(in_array(5,$dow)) echo 'checked'; ?>>Fri</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="6" <?php if(in_array(6,$dow)) echo 'checked'; ?>>Sat</label>
                        <label class="checkbox-inline"><input type="checkbox" class="dow_select" value="7" <?php if(in_array(7,$dow)) echo 'checked'; ?>>Sun</label>
                    </div>
                </div>
                <div id="recurring-option-section-3" class="form-group hide">
                    <!-- repeat option -->
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-2 control-label">Repeat every</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <select class="form-control" id="monthly_option">
                            <?php for($i=1;$i<=6;$i++):?>
                                <option value="<?php echo $i; ?>" <?php if(isset($book) && $book->recurring_type==3) echo ($book->repeat_times==$i?'selected':''); ?>><?php if($i==1) echo $i.' month'; else echo $i.' months'; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div id="ultil-recurring-section" class="form-group hide">
                    <label for="ultil-recurring" class="col-sm-3 col-md-3 col-lg-3 control-label">Repeats</label>
                    <div class="col-sm-3 col-md-3 col-lg-4">
                        <div class="radio">
                            <label><input type="radio" name="radio_repeat" value="1" <?php if(isset($book)) echo ($book->repeat_to==1?'checked':''); ?>>Until a date</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="radio_repeat" value="2" <?php if(isset($book)) echo ($book->repeat_to==2?'checked':''); ?> >A number of times</label>
                        </div>
                    </div>
                    <div id="ultil-recurring-time-1" class="col-sm-2 col-md-2 col-lg-3 hide">
                        <input id="ultil-date" class="form-control" value="<?php echo $start_time?>">
                    </div>
                    <div id="ultil-recurring-time-2" class="col-sm-2 col-md-2 col-lg-3 hide">
                        <div style="width: 150px;" class="col-sm-3 col-md-3 col-lg-4">
                            <select class="form-control" id="number-of-times">
                                <?php for($i=2;$i<=12;$i++):?>
                                    <option value="<?php echo $i; ?>" <?php if(isset($book)) echo ($book->repeat_to_data==$i?'selected':''); ?>><?php if($i==2) echo ' twice'; else echo $i.' times'; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <input name="status" id="booking_status" type="hidden" value="<?php echo isset($book->status)?$book->status:0?>"/>
                <input id="is_old" type="hidden" value="<?php echo isset($book)?1:0 ?>">
                <input id="hidden_employee_id" type="hidden" value="<?php echo $employee_id; ?>">
                <input id="hidden_end_time" type="hidden" value="<?php echo $end_time; ?>">
            </div>
        </div>
    </form>
</div>

<script>

    var test = function(){
        $('#repeat_type').change();
    };

    function change_recurring_section(){
        var select_value = $('#recurring_section select').val();
        $('div[id*="recurring-option-section-"]').addClass('hide');
        $('#repeat-of-week').addClass('hide');
        if(select_value == 0) {
            $('#ultil-recurring-section').addClass('hide');
        }
        else if(select_value == 1) {
            $('#ultil-recurring-section').removeClass('hide');
        }
        else if(select_value == 2){
            $('#repeat-of-week').removeClass('hide');
            $('#ultil-recurring-section').removeClass('hide');
        }
        else if(select_value == 3){
            $('#ultil-recurring-section').removeClass('hide');
        }
        else if(select_value == 4){
            $('#ultil-recurring-section').removeClass('hide');
        }
        if($('#ultil-recurring-section').hasClass('hide')){
            $('#ultil_recurring_time_picker').addClass('hide');
        }
        else {
            if($('input[name=radio_repeat]:checked').val() == 1){
                $('#ultil-recurring-time-1').removeClass('hide');
                $('#ultil-recurring-time-2').addClass('hide');
            }
            else if($('input[name=radio_repeat]:checked').val() == 2) {
                $('#ultil-recurring-time-1').addClass('hide');
                $('#ultil-recurring-time-2').removeClass('hide');
            }
        }
        $('#recurring-option-section-'+select_value).removeClass('hide');
    }

     $(document).ready(function(){
        var current_date = to_standard_date_time('<?php echo $start_time?>');
        var recurring_to_date = '<?php if(isset($book->repeat_to_data)) echo (strlen($book->repeat_to_data) > 5 ? $book->repeat_to_data:'') ?>';

         $('#start_time').datetimepicker({
            timepicker: true,
            datepicker: true,
            format: 'Y-m-d H:i:s',
            startDate : current_date
        });
        $('#ultil-date').datetimepicker({
            datepicker: true,
            format: 'Y-m-d H:i:s',
            value : (recurring_to_date==''?current_date:recurring_to_date)
        });
        $('#recurring_section select').change(function(){
            change_recurring_section();
        });
        $('input[name=radio_repeat]').bind('change',function(){
            if($('input[name=radio_repeat]:checked').val() == 1){
                $('#ultil-recurring-time-1').removeClass('hide');
                $('#ultil-recurring-time-2').addClass('hide');
            }
            else if($('input[name=radio_repeat]:checked').val() == 2) {
                $('#ultil-recurring-time-1').addClass('hide');
                $('#ultil-recurring-time-2').removeClass('hide');
            }
        });

        $('.dow_select[value="'+moment().format('d')+'"]').attr('checked','checked');
        $('.dow_select[value="'+moment().format('d')+'"]').attr('disabled','disabled');
         change_recurring_section();
         if(!<?php if(isset($book->status)) echo ($book->status == BOOKING_STATUS('HoldBill'))? 0: 1; else echo 1; ?>){
            $('.submit_edit_create_form').addClass('hide');
        }
    });

    function update_comment(classname){
        var self = $(classname);
        var comment = self.val();
        var current_comment = $("#comment").text();
        if($.trim(current_comment) == "")
            current_comment = comment;
        else
            current_comment = current_comment +'\n'+ comment;
        $("#comment").text(current_comment);
    }
</script>