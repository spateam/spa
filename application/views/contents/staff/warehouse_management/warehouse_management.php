<div class="break_line"></div>
<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right">
            <?php if($this->user_permission->checkPermission('e', $pageCode)):?>
                <a class="btn btn-medium btn-primary tip-bottom" title="New Category" target="room_list" btn-type="new">
                    <span class="visible-lg">New</span>
                    <i title="New Customer" class="fa fa-pencil tip-bottom hidden-lg fa fa-2x"></i>
                </a>
            <?php endif; ?>
        </div>
        <input type="text" id="name" value="" placeholder="Search warehouse product" class="search ui-autocomplete-input" autocomplete="off" target="room_list" btn-type="search">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <div class="paging" target="room_list"></div>
                <span id="total_rows" class="label label-info"></span>
                <a class="btn btn-info btn-sm clear-state pull-right" target="room_list" btn-type="clear">clear search</a>

            </div>
            <div class="widget-content no-padding table_holder table-responsive" >
                <table data-url="<?php echo site_url('staff/'.$pageCode) ?>/" id="room_list" class="tablesorter table table-bordered  table-hover datatable">
                    <thead data-key="id" >
                    <tr>
                        <th data-type="text" data-column="item_name">Item Name</th>
                        <th data-type="text" data-column="item_code">Item Code</th>
                        <th data-type="text" data-column="quantity">Quantity</th>
                        <th data-type="text" data-column="updated_date">Last Transaction</th>
                    </tr></thead>
                    <tbody id="grid-rows">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>