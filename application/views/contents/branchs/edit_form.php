
<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="employee_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:''; ?>"/>
        <div class="row">
            <div class="col-md-12">
                <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
                <div class="form-group">
                    <label for="branch_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Name:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="branch_name" value="<?php echo isset($item->name)?$item->name:'';?>" class="insert form-inps required" id="branch_name"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Address:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="address" value="<?php echo isset($item->address)? $item->address:'';?>" class="insert form-inps required" id="address"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone_number" class="col-sm-3 col-md-3 col-lg-2 control-label ">Phone Number:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="phone_number" value="<?php echo isset($item->phone_number)?$item->phone_number:'';?>" class="insert form-inps" id="phone_number"  />
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-3 col-md-3 col-lg-2 control-label">E-Mail:</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="email" value="<?php echo isset($item->email)? $item->email:'';?>" class="insert form-inps" id="email"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="fax" class="col-sm-3 col-md-3 col-lg-2 control-label ">Fax:</label>	<div class="col-sm-9 col-md-9 col-lg-10">
                        <input type="text" name="fax" value="<?php echo isset($item->fax)?$item->fax:''; ?>" class="insert form-inps" id="fax"  />	</div>
                </div>
            </div>
        </div>
    </form>
</div>
