<div class="break_line"></div>
<div class="form-row">
    <button type="button" id="reload-table" class="btn btn-primary submit_button btn-large pull-right" style="margin-right: 52px; margin-bottom: 5px;">Reload</button>
</div>
<div class="row">
    <table id="gift-card-table" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="width:0.1%"></th>
                <th style="width:10%;">Code</th>
                <th style="width:25%;">Gift Card Name</th>
                <th style="width:10%;">Owner Code</th>
                <th style="width:24% ;">Owner</th>
                <th style="width:15%">Original Values</th>
                <th style="width:15%;">Current Values</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
            <tr>
                <th></th>
                <th>Code</th>
                <th>Gift Card Name</th>
                <th>Owner Code</th>
                <th>Owner Name</th>
                <th>Original Values</th>
                <th>Current Values</th>
            </tr>
        </tfoot>

    </table>
</div>
<script type="text/javascript">
    var site_url = "<?php echo site_url('gift_card/getData') ?>";
    var site_url_detail = "<?php echo site_url('gift_card/getDetailData') ?>";
</script>