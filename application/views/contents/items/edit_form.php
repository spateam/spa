<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="item_kit_form" class="form-horizontal">
        <input type="hidden" id="id" value="<?php echo isset($item->id)?$item->id:''; ?>"/>
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
        <div class="form-group">
            <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label">Type:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <?php echo generate_control(ITEM_TYPE(),isset($item->type)?$item->type:'', 'select',array('class' => 'insert select_commission', 'id' => 'type'))?>
            </div>
        </div>
        <div class="form-group">
            <label for="code" class="col-sm-3 col-md-3 col-lg-2 control-label required">UPC/EAN/ISBN:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="code" value="<?php echo isset($item->code)?$item->code:'';?>" class="form-control form-inps required insert" id="code"  />
            </div>
        </div>
        <div class="form-group">
            <label for="item_name" class="col-sm-3 col-md-3 col-lg-2 control-label  required">Item Name:</label>			<div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="name" value="<?php echo isset($item->name)?$item->name:'';?>" class="insert form-control form-inps required" id="name"  />
            </div>
        </div>
        <div class="form-group">
            <label for="category" class="col-sm-3 col-md-3 col-lg-2 control-label  required wide">Category:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="category_id" name="category_id" class="insert multi-select" value='<?= $item->id&&isset($item->categories)?json_encode(convert_to_array($item->categories,'','id')):json_encode(array()) ?>' data-source="<?= site_url('categories/suggest') ?>?full=1">
            </div>
        </div>

        <div class="form-group" id="unit_price_display">
            <label for="unit_price" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Unit Price:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="price" value="<?php echo isset($item->price)?$item->price:'';?>" class="insert form-control form-inps required" id="price"  />
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-3 col-md-3 col-lg-2 control-label">Description:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <textarea name="description" cols="17" rows="5" id="description" class="insert form-textarea" ><?php echo isset($item->description)?$item->description:'';?></textarea>
            </div>
        </div>

        <div class="form-group no_fullcash">

        <!-- DEPOSIT PAYMENT -->
        <div class="form-group">
            <label for="minimum_deposit" class="col-sm-3 col-md-3 col-lg-2 control-label">Minimum Deposit</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="minimum_deposit" id="minimum_deposit" class="insert form-control form-inps" value="<?php echo isset($item->min_cash_payment)?$item->min_cash_payment:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="remain_payment_time" class="col-sm-3 col-md-3 col-lg-2 control-label">Remain Payment Time (Months): </label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="remain_payment_time" id="remain_payment_time" class="insert form-control form-inps" value="<?php echo isset($item->payment_time)?$item->payment_time:''; ?>" />
            </div>
        </div>
        <!-- END DEPOSIT PAYMENT -->

        <div class="form-group">
            <label for="no_fullcash" class="col-sm-3 col-md-3 col-lg-2 control-label">No cash payment:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="checkbox" name="no_fullcash" id="no_fullcash" class="insert chbox" unchecked-value="0" <?php echo (isset($item->no_fullcash) && ($item->no_fullcash == 1) ? 'checked' : '');?> />
            </div>
        </div>
        <div  id="service" class="">
            <div class="form-group">
                <label for="last_appointment" class="col-sm-3 col-md-3 col-lg-2 control-label">Last Appointment:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <?php
                    if(isset($item->last_appointment) && ($item->last_appointment['branch_last_appointment'] != $item->last_appointment['item_last_appointment'])){ ?>
                        <input type="text" name="last_appointment" value='<?php echo $item->last_appointment["item_last_appointment"]; ?>' class="insert form-control form-inps" id="last_appointment" />
                        <input id="last_appointment_ckb" type="checkbox">&nbsp;Inherited
                    <?php 
                    }
                    else{?>
                        <input type="text" disabled="disable" name="last_appointment" value='<?php echo $item->last_appointment["item_last_appointment"]; ?>' class="insert form-control form-inps" id="last_appointment" />
                        <input id="last_appointment_ckb" type="checkbox" checked="checked">&nbsp;Inherited
                    <?php
                    }?>
                </div>
            </div>
        </div>
        <div class="form-group show_price_in_ui">
            <label for="show_price_in_ui" class="col-sm-3 col-md-3 col-lg-2 control-label">Show Price In UI</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="show_price_in_ui" name="show_price_in_ui" data-source="<?php echo admin_url('branch/suggest')?>" value='<?= isset($item->show_price_in_ui)?json_encode(convert_to_array($item->show_price_in_ui,'','id')):json_encode(array()) ?>' class="insert form-inps multi-select"/>
            </div>
        </div>
        <div class="form-group show_in_ui">
            <label for="show_in_ui" class="col-sm-3 col-md-3 col-lg-2 control-label">Show In UI</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input id="show_in_ui" name="show_in_ui" data-source="<?php echo admin_url('branch/suggest')?>" value='<?= isset($item->show_in_ui)?json_encode(convert_to_array($item->show_in_ui,'','id')):json_encode(array()) ?>' class="insert form-inps multi-select"/>
            </div>
        </div>
        <div class="form-group">
            <label for="show_in_ui" class="col-sm-3 col-md-3 col-lg-2 control-label">Show From Date</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" id="show_date_from" name="show_date_from" class="insert report_date datepicker" value="<?= isset($item->show_date_from)?$item->show_date_from:'' ?>">
                &nbsp;Time
                <input type="text" id="show_time_from" name="show_time_from" class="insert time-picker" value="<?= isset($item->show_time_from)?$item->show_time_from:'' ?>">
                &nbsp;&nbsp;
                <input type="checkbox" name="show_date_loop" id="show_date_loop" class="insert chbox" unchecked-value="0" <?php echo (isset($item->show_date_loop) && ($item->show_date_loop == 1) ? 'checked' : '');?> />&nbsp;Loop every week
            </div>
        </div>
        <div class="form-group">
            <label for="show_in_ui" class="col-sm-3 col-md-3 col-lg-2 control-label">Show To Date</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" id="show_date_to" name="show_date_to" class="insert report_date datepicker" value="<?= isset($item->show_date_to)?$item->show_date_to:'' ?>">
                &nbsp;Time
                <input type="text" id="show_time_to" name="show_time_to" class="insert time-picker" value="<?= isset($item->show_time_to)?$item->show_time_to:'' ?>">
            </div>
        </div>

        <div class="form-group promotion">
            <label for="promotion" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Promotion:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="number" min="0" max="50" name="promotion" value="<?php echo isset($item->promotion)&&!empty($item->promotion)?$item->promotion:'0'?>" id="promotion" class="insert form-control form-inps">
            </div>
        </div>

        <div  id="package" class="">
            <div class="form-group credit_value">
                <label for="credit_value" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Credit Value:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="credit_value" value="<?php echo isset($item->credit_value)?$item->credit_value:'';?>" class="insert form-control form-inps" id="credit_value"  />
                </div>
            </div>

            <div class="form-group expire_length">
                <label for="credit_value" class="col-sm-3 col-md-3 col-lg-2 control-label  ">Expiry Length:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                     <input type="number" name="expire_length" value="<?php echo isset($item->package->expire_length)?$item->package->expire_length:'';?>" class="insert form-control form-inps" id="expire_length"  />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category item relate</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select data-source="<?php echo admin_url('categories/suggest')?>?full=1" name="category_add" id="category_add">
                        <option value="0"> Select Category </option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label"></label>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <select name="item_id" id="item_id" style="margin-top: 5px;"></select>
                    <input type="button" class="btn" id="btn_add_all_items" value="Add all list items" style="margin-left: 15px;">
                </div>
            </div>

            <div class="form-group">
                <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label">Add Single Item</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <div style="clear: both">
                        <select id="item_kit_select" data-source="<?php echo site_url('items/suggest') ?>" ></select>
                    </div>
                    <div class="widget-box align-top">
                        <div class="widget-title">
						<span class="icon">
							<i class="fa fa-th"></i>
						</span>
                            <h5>Add Item</h5>
                        </div>
                        <div class="widget-content no-padding">
                            <input type="hidden" id="item_kit_ids" class="insert" value='<?php
                            $json = array();
                            if(isset($item->package)) {
                                foreach ($item->package->kit_items as $kit_item) {
                                    $json[] = $kit_item->id;
                                }
                            }
                            echo json_encode($json);
                            ?>'/>
                            <table id="item_kit_items" class="table table-bordered table-striped table-hover text-success text-center">
                                <tr>
                                    <th>Delete</th>
                                    <th>Item name</th>
                                    <th>Unit price</th>
                                </tr>
                                <tbody id="grid_item">
                                <?php
                                $kitdataitem = isset($item->package) ? $item->package : '';

                                if($kitdataitem != ''):
                                foreach($kitdataitem->kit_items as $kit_item){
                                    ?>
                                    <tr>
                                        <td><a onclick='return deleteItemKitRow(this, <?= $kit_item->id ?>);' ><i class="fa fa-trash-o fa fa-2x text-error"></i></a></td>
                                        <td class="modelname"><?php echo $kit_item->name;?></td>
                                        <td class="modelname"><?php echo $kit_item->price;?></td>
                                    </tr>
                                <?php }
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="product" class="">
            <div class="form-group">
                <label for="unit_cost" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Unit Cost:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="unit_cost" value="<?php echo isset($item->product)?$item->product->unit_cost:'';?>" class="insert form-control form-inps" id="unit_cost"  />
                </div>
            </div>
        </div>

        <div  id="service" class="">
            <div class="form-group duration">
                <label for="duration" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Duration:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="number" name="duration" value="<?php echo isset($item->service)?$item->service->duration:'';?>" class="insert form-control form-inps" id="duration"  />
                </div>
            </div>

            <div class="form-group">
                <label for="promotion_time" class="col-sm-3 col-md-3 col-lg-2 control-label required ">Retrict Promo time:</label>
                <div class="col-sm-9 col-md-9 col-lg-10" id="promotion_time">
                    <div id="promotionKit_<?php echo strtotime('NOW'); ?>" style="margin-top: 1px; margin-bottom: 1px;">
                        <span>Start Date: </span><input name="restrict_start_date" id="restrict_start_date" type="text" class="start_date insert" style="width:100px;" value="<?php echo isset($item->restrict_datetime)?$item->restrict_datetime->restrict_from_date:''; ?>">
                        <span>  End Date: </span><input name="restrict_end_date" id="restrict_end_date" type="text" class="end_date insert" style="width:100px;" value="<?php echo isset($item->restrict_datetime)?$item->restrict_datetime->restrict_to_date:''; ?>">
                        <span>  Start Time: </span><input name="restrict_start_time" id="restrict_start_time" type="text" class="start_time insert" style="width:70px;" value="<?php echo isset($item->restrict_datetime)?$item->restrict_datetime->restrict_from_time:''; ?>">
                        <span>  End Time: </span><input name="restrict_end_time" id="restrict_end_time" type="text" class="end_time insert" style="width:70px;" value="<?php echo isset($item->restrict_datetime)?$item->restrict_datetime->restrict_to_time:''; ?>">
<!--                        <span><input type="button" value="+" class="addmore" style="width: 26px;"/></span>-->
                    </div>
                </div>
            </div>

        <!--    <div class="form-group">
                <label for="fullcash_payment" class="col-sm-3 col-md-3 col-lg-2 control-label">Full Cash Payment:</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="checkbox" name="fullcash_payment" <?php if(isset($item->fullcash_payment) && $item->fullcash_payment) echo 'checked'; ?> class="insert form-control form-inps" id="fullcash_payment" />
                </div>
            </div>
            -->
        </div>

        <div id="bundle" class="">
            <div class="form-group">
                <label for="item" class="col-sm-3 col-md-3 col-lg-2 control-label"> Sub-Items: </label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" name="item_bundle_item" value="" class="form-control form-inps autocomplete" data-source="<?php echo site_url('items/suggest') ?>" id="item_bundle_item"  />
                    <div class="widget-box align-top">
                        <div class="widget-title">
                            <span class="icon">
                                <i class="fa fa-th"></i>
                            </span>
                            <h5>Add Sub Item</h5>
                        </div>
                        <div class="widget-content no-padding">
                            <input type="hidden" id="item_bundle_item_ids" class="insert" value='<?php
                            echo isset($item->bundle->sub_items)?json_encode(convert_to_array($item->bundle->sub_items,'','id',true)):"[]";
                            ?>'/>
                            <table class="table table-bordered table-striped table-hover text-success text-center">
                                <tr>
                                    <th>Delete</th>
                                    <th>Item Name</th>
                                    <th>Unit Price</th>
                                </tr>
                                <tbody id="grid_item_bundle">
                                <?php
                                if(isset($item->bundle->sub_items)):
                                    foreach($item->bundle->sub_items as $sub_item){
                                        ?>
                                        <tr>
                                            <td><a onclick='return deleteBundleSubItem(this, <?= $sub_item->id ?>);' ><i class="fa fa-trash-o fa fa-2x text-error"></i></a></td>
                                            <td class="modelname"><?php echo $sub_item->name;?></td>
                                            <td class="modelname"><?php echo $sub_item->price;?></td>
                                        </tr>
                                    <?php }
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    (function(){
        $('div[id*="promotionKit_"]>input[id*="_date"]').each(function(i,item){
            $('#'+$(item).attr('id')).datetimepicker({
                timepicker:false,
                format:'Y-m-d'
            });
        });

        $('div[id*="promotionKit_"]>input[id*="_time"]').each(function(i,item){
            $('#'+$(item).attr('id')).datetimepicker({
                datepicker:false,
                format:'H:i'
            });
        });

        $('body').delegate('.addmore','click',function(){
            var codetime = 'promotionKit_'+moment().unix();
            $('#promotion_time').append(
                '<div id="'+codetime+'">'+
                '<span>Start Date: </span><input type="text" class="start_date" style="width:100px;" readonly>'+
                    '<span>  End Date: </span><input type="text" class="end_date" style="width:100px;" readonly>'+
                    '<span>  Start Time: </span><input type="text" class="start_time" style="width:70px;" readonly>'+
                    '<span>  End Time: </span><input type="text" class="end_time" style="width:70px;" readonly>'+
//                    '<span>&nbsp;<input type="button" value="-" class="reducetime" style="width: 26px;"/></span>'+
                '</div>'
            );
            $('.reducetime').on('click', function(){
                console.log(this);
                $(this).parent().parent().remove();
            });
        });

    }());


    $("#last_appointment_ckb").click(function(){
        if($(this).is(":checked")){
            var branch_last_appointment = "<?php echo $item->last_appointment['branch_last_appointment'] ?>";
            $("#last_appointment").val(branch_last_appointment);
            $("#last_appointment").attr("disabled","disabled");
        }
        else if($(this).is(":not(:checked)")){
            var branch_last_appointment = "<?php echo $item->last_appointment['item_last_appointment'] ?>";
            $("#last_appointment").val(branch_last_appointment);
            $("#last_appointment").removeAttr("disabled");
        }
    });

    $('#btn_add_all_items').click(function(){
        var options = $('#item_kit_select option');
        var valid_values = $.map(options ,function(option) {
            return option.value;
        });

        options = $('#item_id option');
        var category_values = $.map(options ,function(option) {
            return option.value;
        });
        
        $.each(category_values, function( index, value ) {
            $.system_process({
                url : url + control + 'getItem/' + value,
                success:function(ret,more){
                    if ($.inArray(value, valid_values) != -1){
                        var data = $('#item_kit_ids').val();
                        data = JSON.parse(data);
                        if ($.inArray(value, data) != -1){}
                        else{
                            var result = ret['data'];
                            var html = "";
                            html += '<tr><td><a onclick="return deleteItemKitRow(this,'+result.id+');"><i class="fa fa-trash-o fa fa-2x text-error"></i></a></td>';
                            html += '<td class="modelname">'+result.name+'</td>';
                            html += '<td class="modelname">'+result.price+'</td></tr>';
                            $('#grid_item').append(html);

                            data.push(value);
                            $('#item_kit_ids').val(JSON.stringify(data));
                        }
                    }
                }
            });
        })
    });

    $(".btn-primary").click(function(){
        var show_date_from = $("#show_date_from").val();
        var show_date_to = $("#show_date_to").val();
        var show_time_from = $("#show_time_from").val();
        var show_time_to = $("#show_time_to").val();
        var mess_content = "";

        if(show_date_from != "" && show_date_to != "")
            if(show_date_from > show_date_to)
                mess_content = "Show To Date must equal or bigger than Show From Date";

        if(show_time_from != "" && show_time_to != "")
            if(show_time_from >= show_time_to)
                mess_content = "Show To Time must bigger than Show From Time";

        if(mess_content != ""){
            $.msgBox({
            title:"Message",
            type:"alert",
                content:mess_content,
                buttons:[{value:'Cancel'}]
            });

            return false;
        }
    });
</script>