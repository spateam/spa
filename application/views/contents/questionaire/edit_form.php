<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 11/1/2014
 * Time: 4:30 PM
 */
?>

<div class="widget-content">
    <form  method="post" accept-charset="utf-8" id="item_kit_form" class="form-horizontal">
        <input type="hidden" id="id" name="id" value="<?php echo isset($item->id)?$item->id:'0'; ?>"/>
        <div class="messagebox" style="z-index:6000000" id="messageBox"></div>
        <div class="form-group">
            <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label">Question:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <input type="text" name="title" value="<?php echo isset($item->title)?$item->title:''; ?>" id="title" class="insert form-control"  />
                <span id="confirmExist" class="confirmExist"></span>
            </div>
        </div>

        <div class="form-group">
            <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label">Type:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <select id="type" name="type" class="insert drop-style get-list-manager" style="display:none;">
                    <option value="1" <?= isset($item->type)&&$item->type=='1'?'selected="selected"':''?>>Yes/No</option>
                    <option value="2" <?= isset($item->type)&&$item->type=='2'?'selected="selected"':''?>>One Choice</option>
                    <option value="3" <?= isset($item->type)&&$item->type=='3'?'selected="selected"':''?>>Multiple Choice</option>
                </select>
            </div>
        </div>

        <div class="form-group  <?= !isset($item->type)||(isset($item->type)&&QUESTION_TYPE($item->type)=='y-n')?'hidden':''?> addition">
            <label for="type" class="col-sm-3 col-md-3 col-lg-2 control-label">Answers:</label>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <textarea id="type_values" name="type_values" class="insert form-control"><?= to_b($item->type_values)?implode("\n",json_decode($item->type_values)):''?></textarea>
            </div>
        </div>
    </form>
</div>