<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input max_month="1" type="text" id="start_date" name="start_date" class="datepicker" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input object_time="start_date" type="text" id="end_date" name="end_date" class="datepicker" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                </div>
            </div>


            <div  class="form-group category_group" >
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Category</label>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <input name="category_id" class="multi-select" id="category_id" data-source="<?php echo admin_url('categories/suggest?minimal=true')?>">
                </div>
            </div>

            <div  class="form-group item_group" >
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Item</label>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <input name="item_id" class="multi-select" id="item_id" data-source="<?php echo admin_url('items/suggest?minimal=true')?>">
                </div>
            </div>

            <?php if($current_system == 'admin'){ ?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Warehouse</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="warehouse" class="warehouse" name="warehouse_id" data-source="<?php echo admin_url('warehouse/suggest')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>

            <?php } else {?>

                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Warehouse</label>

                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="warehouse" class="warehouse" name="warehouse_id" data-source="<?php echo admin_url('warehouse/suggest')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>

            <?php } ?>

            <div class="form-actions">
                <button class="btn btn-primary" type="submit">Submit </button>
            </div>
        </form>
    </div>
</div>
