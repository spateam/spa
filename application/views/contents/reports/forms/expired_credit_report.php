<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">`
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date From</label>

                <div class="col-sm-4 col-md-4 col-lg-4">
                    <input max_month="3" type="text" id="start_datepicker" name="start_date" class="report_date datepicker form-control" value="<?= isset($input->start_date)?$input->start_date:date('Y-m-d') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Date To</label>

                <div class="col-sm-4 col-md-4 col-lg-4">
                    <input  type="text" id="end_datepicker" name="end_date" class="report_date datepicker form-control" value="<?= isset($input->end_date)?$input->end_date:date('Y-m-d') ?>">
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>

                <div class="col-sm-4 col-md-4 col-lg-4">
                    <select id="branch" class="branch form-control" name="branch_group_id" data-source="
                    <?php echo $current_system == 'admin'?
                        admin_url('branch_group/suggest'):
                        staff_url('branch_group/suggest')?>">
                        <option value="0">All</option>
                    </select>
                </div>
            </div>
            <div class="form-group" id="summary_section">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="summary">Summary</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input id="summary" name="summary" value="1" type="checkbox" checked>
                </div>
            </div>
            <!--
          <div class="form-group" id="summary_section">
               <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="summary">Summary</label>

              <div class="col-sm-9 col-md-9 col-lg-10">
                   <select class="js-example-basic-multiple" multiple="multiple" data-source='<?= site_url('categories/suggest?full=1')?>' data-value='["1","2"]' style="width:100%">
                   </select>
            </div>
           </div>-->
            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>

        </form>
    </div>
</div>


