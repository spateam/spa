<?php
$input = $this->session->userdata('report');
$current_system = $this->config->item('current_system');
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-align-justify"></i>
        </span>
    </div>
    <div class="widget-content no-padding">
        <form target="_blank" method="post" action="<?php echo $url;?>" id="receipt_report" class="form-horizontal form-horizontal-mobiles">
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">From</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="from_date" name="from_date" class="datepicker" value="<?= isset($input->month_distance)?$input->month_distance:date('Y-m-d') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">To</label>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <input type="text" id="to_date" name="to_date" class="datepicker" value="<?= isset($input->month_distance)?$input->month_distance:date('Y-m-d') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch Group</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select id="branch" class="branch" name="branch_group_id" data-source="
                    <?php echo $current_system == 'admin'?
                        admin_url('branch_group/suggest'):
                        staff_url('branch_group/suggest')?>">
                    </select>
                </div>
            </div>

            <?php if($current_system == 'admin'){ ?>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="range">Branch</label>
                    <div class="col-sm-9 col-md-9 col-lg-10">
                        <select id="branch" class="branch" name="branch_id" data-source="<?php echo admin_url('branch/suggest_by_branch_group')?>">
                            <option value="0">All</option>
                        </select>
                    </div>
                </div>
            <?php } ?>

            <div class="form-actions">
                <button class="btn btn-primary submit_button btn-large" id="generate_report"  type="submit"
                        name="generate_report">Submit </button>
            </div>
        </form>
    </div>
</div>


