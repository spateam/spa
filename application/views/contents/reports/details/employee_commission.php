<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 12/9/2014
 * Time: 4:04 PM
 */
?>
<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <?php if (isset($admin_system) && $admin_system && $summary): ?>
            <th>Employee Name</th>
            <th>Total Sales</th>
            <th>Total Commission</th>
        <?php elseif ($summary == false): ?>
            <th>Refno</th>
            <th>Date</th>
            <th>Item No</th>
            <th>Item Name</th>
            <th>Member ID</th>
            <th>Member Name</th>
            <!--            <th>Commission Type</th>-->
            <th>Total Sales</th>
            <th>Commission</th>
        <?php
        else: ?>
            <th>Employee Name</th>
            <th>Total Sales</th>
            <th>Total Commission</th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>
    <?php if (count($rows)): ?>
        <?php if (isset($admin_system) && $admin_system && $summary): ?>
            <?php foreach ($branch_map as $branch_id => $branch_field):
                $branch_data = isset($rows[$branch_id]) ? $rows[$branch_id] : null;
                if (!$branch_data) continue;
                ?>
                <tr>
                    <td colspan="3" style="font-weight: bold; color:red;"><?php echo $branch_field->name ?></td>
                </tr>
                <?php foreach ($branch_data as $key => $row): ?>
                <tr>
                    <td><?php echo $row->employee_name; ?></td>
                    <td><?php echo to_currency($row->subtotal); ?></td>
                    <td><?php echo to_currency($row->subcommission); ?></td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td  style="font-weight: bold; text-align: right"><?php echo "Total"?></td>
                    <td><?php echo to_currency($branch_total[$branch_field->id]->branch_total_sale); ?></td>
                    <td><?php echo to_currency($branch_total[$branch_field->id]->branch_total_commission); ?></td>
                </tr>
            <?php endforeach; ?>
            <tr class="sum_total">
                <td colspan="<?php echo $summary ? 1 : 6 ?>" style="text-align: right">Total:</td>
                <td><?php echo to_currency($total->total_sale) ?></td>
                <td><?php echo to_currency($total->total_commission) ?></td>
            </tr>
        <?php else : ?>
            <?php foreach ($rows as $key => $row): ?>
                <?php if ($summary == false): ?>
                    <tr row-toggle="off">
                        <td colspan="8" style="font-weight: bold; color:red;"><?php echo $row->employee_name ?></td>
                    </tr>
                    <?php foreach ($row->commissions as $commission_row): ?>
                        <tr>
                            <td><?php echo $commission_row->bill_code ?></td>
                            <td><?php echo $commission_row->date ?></td>
                            <td><?php echo $commission_row->item_id ?></td>
                            <td><?php echo $commission_row->item_name ?></td>
                            <td><?php echo $commission_row->member_id ?></td>
                            <td><?php echo $commission_row->customer_name ?></td>
                            <!--                        <td>-->
                            <?php //echo $commission_row->commission_name?><!--</td>-->
                            <td><?php echo to_currency($commission_row->total) ?></td>
                            <td><?php echo to_currency($commission_row->commission) ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td><?php echo $row->employee_name; ?></td>
                        <td><?php echo to_currency($row->subtotal); ?></td>
                        <td><?php echo to_currency($row->subcommission); ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($summary == false): ?>
                    <tr class="sum_total">
                        <td colspan="6" style="text-align: right; padding-right: 5px;">Subtotal:</td>
                        <td><?php echo to_currency($row->subtotal); ?></td>
                        <td><?php echo to_currency($row->subcommission); ?></td>
                    </tr>
                    <tr class="row_space">
                        <td colspan="8"></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr class="sum_total">
                <td colspan="<?php echo $summary ? 1 : 6 ?>" style="text-align: right">Total:</td>
                <td><?php echo to_currency($total->total_sale) ?></td>
                <td><?php echo to_currency($total->total_commission) ?></td>
            </tr>
        <?php endif; ?>
    <?php else: ?>
        <tr>
            <td colspan='8' style='color: red;'>No data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</div>
</div>