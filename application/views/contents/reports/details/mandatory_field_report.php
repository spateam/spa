<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Member ID</th>
        <th>First Name</th>
        <th>Nric</th>
        <th>Mobile Number</th>
        <th>Email</th>
        <th>Gender</th>
    </tr>
    </thead>
    <tbody>
    <tr style="color:red; font-weight: bolder"><td colspan="10">Member</td></tr>
    <?php foreach($customers['member'] as $customer_id=>$customer_fields):?>
        <tr>
            <td><?php echo $customer_fields->code ?></td>
            <td><?php echo $customer_fields->first_name ?></td>
            <td><?php echo $customer_fields->nric ?></td>
            <td><?php echo $customer_fields->mobile_number ?></td>
            <td><?php echo $customer_fields->email ?></td>
            <td><?php echo GENDER($customer_fields->gender) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Member ID</th>
        <th>First Name</th>
        <th>Mobile Number</th>
        <th>Gender</th>
    </tr>
    </thead>
    <tbody>
    <tr style="color:red; font-weight: bolder"><td colspan="10">Guest</td></tr>
    <?php foreach($customers['guest'] as $customer_id=>$customer_fields):?>
        <tr>
            <td><?php echo $customer_fields->code ?></td>
            <td><?php echo $customer_fields->first_name ?></td>
            <td><?php echo $customer_fields->mobile_number ?></td>
            <td><?php echo GENDER($customer_fields->gender) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>