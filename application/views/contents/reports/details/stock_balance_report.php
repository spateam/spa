<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Item No</th>
        <th>Item Name</th>
        <th>Quantity</th>
        <th>Last Transaction</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($stock_items)): foreach($stock_items as $warehouse_item_id=>$warehouse_item_fields):?>
        <tr>
            <td><?php echo $warehouse_item_fields->item_code ?></td>
            <td><?php echo $warehouse_item_fields->item_name ?></td>
            <td><?php echo $warehouse_item_fields->quantity ?></td>
            <td><?php echo $warehouse_item_fields->log_time ?></td>
        </tr>
    <?php endforeach; else: ?>
        <tr><td colspan="10">There is no record</td></tr>
    <?php endif; ?>
    </tbody>
</table>