<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Package Name</th>
        <th>Credit Remaining</th>
        <th>Credit Original</th>
        <th>Expired Date</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($customers as $customer_id => $customer_fields) :
        $customer_packages = $customers_packages[$customer_id];
        ?>
        <tr style="color:red; font-weight: bolder">
            <td colspan="10"><?php echo to_full_name($customer_fields->first_name, $customer_fields->last_name) ?></td>
        </tr>
        <?php foreach ($customer_packages as $customer_package): ?>
        <tr>
            <td><?php echo $customer_package->name ?></td>
            <td><?php echo $customer_package->credit ?></td>
            <td><?php echo $customer_package->credit_original ?></td>
            <td><?php echo to_user_format($customer_package->end_date) ?></td>
        </tr>
    <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>