<?php $cols = 4 + count($payments); ?>
<table class="table table-bordered table-striped table-hover data-table">
    <tbody>
    <?php foreach($customer_summary_data as $customer_id=>$customer_field) : $total = $customer_field['total']; ?>
        <tr><td colspan='<?php echo $cols?>' style='color: red;'><strong><?php echo $customer_field['name'] ?></strong></td></tr>
        <tr>
            <th>Date</th>
            <?php foreach($payments as $payment_id=>$payment_field) :?>
                <th><?php echo $payment_field->name ?></th>
            <?php endforeach; ?>
            <th>Redeem+</th>
            <th>Redeem-</th>
            <th>Revenues</th>
        </tr>
        <?php foreach($customer_field['rows'] as $date=>$row){
            ?>
            <tr>
                <td><?php echo $date?></td>
                <?php foreach($payments as $payment_id=>$payment_field) :?>
                    <td><?php echo isset($row['payments'][$payment_id]) && isset($row['payments'][$payment_id]['amount'])?
                            to_currency($row['payments'][$payment_id]['amount']):to_currency(0) ?></td>
                <?php endforeach; ?>
                <td><?php echo to_currency($row['credit_add']) ?></td>
                <td><?php echo to_currency($row['credit_used']) ?></td>
                <td><?php echo to_currency($row['sales_revenues']) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <th colspan="1" style="text-align: right">Total</th>
            <?php foreach($payments as $payment_id=>$payment_field) :?>
                <th><?php echo isset($total['payments'][$payment_id]) && isset($total['payments'][$payment_id]['amount'])?
                        to_currency($total['payments'][$payment_id]['amount']):to_currency(0) ?></th>
            <?php endforeach; ?>
            <th><?php echo to_currency($total['credit_add']) ?></th>
            <th><?php echo to_currency($total['credit_used']) ?></th>
            <th><?php echo to_currency($total['sales_revenues']) ?></th>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
