<?php if(isset($summary) && $summary == true): ?>
<table class="table table-bordered table-striped table-hover data-table tablesorter" id="sortable_table">
    <thead>
    <tr>
        <th>Date</th>
        <?php foreach($category_map as $key=>$value): ?>
        <th><?php echo $key ?></th>
        <?php endforeach ?>
        <th>Total</th>
    </tr>
    </thead>
    <?php if(isset($rows) && count($rows)): ?>
        <tbody>
        <?php foreach($rows as $branch_id=>$branch_rows):?>
            <tr class="table-row-toggle" row-toggle="off"><td colspan="13"> <?= $branch_map[$branch_id]->name  ?> </td></tr>
            <?php foreach($branch_rows as $date=>$row_value): ?>
                <tr>
                    <td><?php echo $date ?></td>
                    <?php foreach($category_map as $category_id=>$value): ?>
                        <td><?= to_currency(isset($row_value['categories'][$category_id])?$row_value['categories'][$category_id]->sale_value:0) ?></th>
                    <?php endforeach ?>
                    <td><b><?php echo to_currency($row_value['total'])?></b></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <th>Branch Total</th>
                <?php foreach($category_map as $key=>$value): ?>
                    <th><?php echo to_currency($total[$branch_id]['categories'][$key]) ?></th>
                <?php endforeach ?>
                <th><?php echo to_currency($total[$branch_id]['total']) ?></th>
            </tr>
        <?php endforeach?>
<!--        --><?php //foreach($rows_item as $item_id=>$item_total):?>
<!--            <tr>-->
<!--                <td>--><?php //echo $item_total->name ?><!--</td>-->
<!--                <td colspan="10">--><?php //echo to_currency($item_total->sale_value) ?><!--</td>-->
<!--            </tr>-->
<!--        --><?php //endforeach?>
        </tbody>
        <tfoot>
        <tr>
            <th>Total</th>
            <?php foreach($category_map as $key=>$value): ?>
                <th><?php echo to_currency(isset($total['categories'][$key])?$total['categories'][$key]:0) ?></th>
            <?php endforeach ?>
            <th><?php echo to_currency($total['total']) ?></th>
        </tr>
        </tfoot>
    <?php else:?>
        <tbody>
        <td colspan="13" style="text-decoration: underline;font-weight:bold"> <?= $branch_name  ?> </td>
        <tr><td colspan="13">There is no current bill</td></tr>
        </tbody>
    <?php endif; ?>
</table>
<?php else : ?>
    <table class="table table-bordered table-striped table-hover data-table tablesorter" id="sortable_table">
        <thead>
        <tr>
            <td>&nbsp;</td>
            <th>Item Name</th>
            <th>Value</th>
        </tr>
        </thead>
        <?php if(isset($rows) && count($rows)): ?>
            <tbody>
            <tr><td colspan="13" style="text-decoration: underline;font-weight:bold; color:red">Category: <?= $category_name  ?> </td></tr>
            <?php foreach($rows as $date=>$row_value):?>
                <tr><td colspan="13" style="text-decoration: underline;font-weight:bold"> <?= $date  ?> </td></tr>

                    <?php foreach($row_value['items'] as $key=>$value): ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td><?php echo $key ?></td>
                        <td><?php echo $value->sale_value ?></td>
                    </tr>
                    <?php endforeach ?>
                    <tr><td style="text-align: right">Total</td><td><b><?php echo to_currency($row_value['total'])?></b></td></tr>

            <?php endforeach?>
<!--            --><?php //foreach($rows_item as $item_id=>$item_total):?>
<!--                    <tr>-->
<!--                        <td>--><?php //echo $item_total->name ?><!--</td>-->
<!--                        <td colspan="10">--><?php //echo to_currency($item_total->sale_value) ?><!--</td>-->
<!--                    </tr>-->
<!--            --><?php //endforeach?>
            </tbody>
            <tfoot>
            <tr>
                <th style="text-align: right">Total</th>
                <th colspan="10"><?php echo to_currency($total['total']) ?></th>
            </tr>
            </tfoot>
        <?php else:?>
            <tbody>
            <td colspan="13" style="text-decoration: underline;font-weight:bold"> <?= $branch_name  ?> </td>
            <tr><td colspan="13">There is no current bill</td></tr>
            </tbody>
        <?php endif; ?>
    </table>
<?php endif; ?>