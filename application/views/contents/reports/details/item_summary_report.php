<?php
if(!isset($multi_branch) || !$multi_branch):
$cols = 10; ?>
<table class="table table-bordered table-striped table-hover data-table">
    <tbody>
    <?php foreach($item_summary_data as $item_id=>$item_fields) :?>
        <tr row-toggle="off"><td colspan='<?php echo $cols?>' style='color: red;'><strong><?php echo $item_fields['name'] ?></strong></td></tr>
        <tr>
            <th>Date</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Discount</th>
            <th>Sales Value</th>
            <th>Redeem-</th>
            <th>Redeem+</th>

        </tr>
        <?php foreach($item_fields['rows'] as $date=>$row){ ?>
            <tr>
                <td><?php echo $date?></td>
                <td><?php echo to_currency($row['price'])?></td>
                <td><?php echo $row['quantity']?></td>
                <td><?php echo to_currency($row['discount'])?></td>
                <td><?php echo to_currency($row['sales_value'])?></td>
                <td><?php echo $row['credit_used']?></td>
                <td><?php echo $row['credit_add']?></td>
            </tr>
        <?php } ?>
        <tr>
            <th colspan="2" style="text-align: right">Total</th>
            <td><?php echo $item_fields['total']['quantity']?></td>
            <td><?php echo to_currency($item_fields['total']['discount'])?></td>
            <td><?php echo to_currency($item_fields['total']['sales_value'])?></td>
            <td><?php echo $item_fields['total']['credit_used']?></td>
            <td><?php echo $item_fields['total']['credit_add']?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php else :
    $cols = count($item_summary_data) + 1;
    ?>

<table class="table table-bordered table-striped table-hover data-table">
    <thead>

        <tr>
            <th style="width:10%">&nbsp;</th>
            <?php foreach($item_summary_data as $item_id=>$item_field) :?>
            <th><?php echo $item_field['name'] ?></th>
            <?php endforeach; ?>
        </tr>

    </thead>
    <tbody>
        <?php foreach($branch_data as $branch_id=>$branch_field) : ?>
            <tr>
                <td class="title_cell"><?php echo $branch_field->name?></td>
                <?php foreach($item_summary_data as $item_id=>$item_field) :
                    $sale_fields = $item_field['branch_rows'][$branch_id];
                    ?>
                <td><?php echo to_currency($sale_fields['sales_value']) ?></td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
    </tbody>
    <tfoot>
    <tr>
        <td class="title_cell">Total</td>
    <?php foreach($item_summary_data as $item_id=>$item_field) :
        $total_field = $item_field['total'];
        ?>
        <td><?php echo to_currency($total_field['sales_value']) ?></td>
    <?php endforeach;?>
    </tr>
    </tfoot>
</table>

<?php endif; ?>