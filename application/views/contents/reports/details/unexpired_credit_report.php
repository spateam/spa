<table class="table table-bordered table-striped table-hover data-table">
    <thead>
    <tr>
        <th>Customer Name</th>
        <th>Client ID</th>
        <th>Credit name</th>
        <th>Credit Balance</th>
        <th>Customer Mobile Number</th>
        <th>Expiry Date</th>
    </tr>
    </thead>
    <tbody>
    <?php $month= -1;?>
    <?php $total_credit=0;?>
    <?php foreach($packages as $package):?>
        <?php if($month != $package->compute_months   ):?>
            <?php if($total_credit!=0): ?>
                <tr>
                    <td style="text-align: right; font-weight: bold">Total</td>
                    <td style="font-weight: bold" colspan="10"><?php echo $total_credit?></td>
                </tr>
            <?php endif; ?>
            <tr row-toggle="off">
                <td style="color: red; font-weight: bold" colspan="10"><?php echo $package->compute_months." months" ?></td>
            </tr>
            <?php $total_credit=0;?>
        <?php endif; ?>
        <?php $month = $package->compute_months;?>
        <tr>
            <td><?php echo $package->customer_name ?></td>
            <td><?php echo $package->code_name ?></td>
            <td><?php echo $package->name ?></td>
            <td><?php echo $package->credit ?></td>
            <td><?php echo $package->mobile_number ?></td>
            <td><?php echo $package->end_date ?></td>
        </tr>
        <?php $total_credit += $package->credit?>

    <?php endforeach; ?>
    <tr>
        <td style="text-align: right; font-weight: bold">Total</td>
        <td style="font-weight: bold" colspan="10"><?php echo $total_credit?></td>
    </tr>
    <tr>
        <td style="text-align: right; font-weight: bold">Total</td>
        <td style="font-weight: bold" colspan="10"><?php echo $total['unused_credit']?></td>
    </tr>
    </tbody>
</table>