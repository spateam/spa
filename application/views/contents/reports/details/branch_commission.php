<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Ref No.</th>
        <th>Date</th>
        <th>Customer ID</th>
        <th>Customer Name</th>
        <th>Discount</th>
        <th>Credit-</th>
        <th>Sales</th>
        <th>Item No.</th>
        <th>Item Name</th>
        <th>Qty</th>
        <th>Price</th>
        <th>Total</th>
        <th>Employee Name</th>
        <th>Commission Name</th>
        <th>Commission</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $GLOBALS['customer_map']    = $customer_map;
    $GLOBALS['employee_map']    = $employee_map;
    $GLOBALS['commission_map']  = $commission_map;
    $GLOBALS['item_map']        = $item_map;

    if(isset($admin_system) && $admin_system){
        foreach($results as $branch_id=>$branch_result){
            if(count($branch_result)){
                echo '<tr style="color:red;font-weight: bold"><td colspan="20">'.$branch_map[$branch_id]->name.'</td></tr>';
                echo_result($branch_result);
            }
        }
    }else{
        echo_result($results);
    }

    function echo_result($results){
        $customer_map = $GLOBALS['customer_map'];
        $employee_map = $GLOBALS['employee_map'];
        $commission_map = $GLOBALS['commission_map'];
        $item_map = $GLOBALS['item_map'];

        $array_bill = array();
        $bill_cols = array(
            'bill_id' => "",
            'date' => "",
            'customer_id' => "",
            'customer_name' => "",
            'discount' => "",
            'credit_used' => "",
            'sales' => "",
            'row_span' => 1
        );
        $item_cols = array(
            'item_id' => "",
            'item_name' => "",
            'qty' => "",
            'price' => "",
            'total' => "",
            'no_employee' => ""
        );
        $employee_cols = array(
            'employee_name' => "",
            'commission_name' => "",
            'commission_number' => ""
        );
        $exclude = array('row_span','no_employee');
        $sample_array = $bill_cols + $item_cols + $employee_cols;

        foreach($results as $bill_id => $row){
            $customer_id = "";
            $customer_name = "";
            if(isset($customer_map[$row['customer_id']])){
                $customer = $customer_map[$row['customer_id']];
                $customer_id = $customer->code;
                $customer_name = to_full_name($customer->first_name, $customer->last_name);
            }
            // Add bill information
            $bill_temp_array = $bill_cols;
            $bill_temp_array['bill_id'] = $row['bill_code'];
            $bill_temp_array['date'] = $row['created_date'];
            $bill_temp_array['customer_id'] = $customer_id;
            $bill_temp_array['customer_name'] = $customer_name;
            $bill_temp_array['discount'] = $row['total_discount'];
            $bill_temp_array['credit_used'] = $row['credit_used'];
            $bill_temp_array['sales'] = $row['sales'];
            $bill_temp_array['row_span'] = $row['row_span'];
            foreach($row['items'] as $bill_item_id => $item_row){
                $item_id = $item_row['item_id'];
                $item_name = "";
                if(isset($item_map[$item_id])){
                    $item_id = $item_map[$item_id]->id;
                    $item_name = $item_map[$item_id]->name;
                    $item_type = $item_map[$item_id]->type;
                }
                $qty = $item_row['qty'];
                $price = $item_row['price'];
                $total = $qty * $price;
                if($item_type == ITEM_TYPE('Bundle')){
                    $num_emp = count_deep($item_row['employees'],0,2);
                }else{
                    $num_emp = count($item_row['employees']);
                }
                // Add item information
                $item_temp_array = $item_cols;
                $item_temp_array['item_id'] = $item_id;
                $item_temp_array['item_name'] = $item_name;
                $item_temp_array['qty'] = $qty;
                $item_temp_array['price'] = $price;
                $item_temp_array['total'] = $total;
                $item_temp_array['no_employee'] = $num_emp;
                if($num_emp > 0) {
                    $is_first_employee = true;
                    if($item_type == ITEM_TYPE('Bundle')){
                        foreach ($item_row['employees'] as $sub_item_id => $sub_item_employees) {
                            foreach($sub_item_employees as $employee_id=>$employee_row){
                                $num_emp = count($item_row['employees'][$sub_item_id]);
                                $employee_name = "";
                                $commission_name = "";
                                $commission_number = "";
                                if(isset($employee_map[$employee_id])) {
                                    $employee = $employee_map[$employee_id];
                                    $employee_name = to_full_name($employee->first_name, $employee->last_name);
                                }
                                if(isset($commission_map[$employee_row['commission_id']][$item_row['item_id']])){
                                    $commission = $commission_map[$employee_row['commission_id']][$item_row['item_id']];
                                    $commission_name = $commission->commission_name;
                                    $commission_type = $employee_row['commission_type'];
                                    $commission_value = $employee_row['commission_value'];

                                    $commission_number = calculate_commission($commission_type, $commission_value, $total, $num_emp,$employee_row['sub_commission_value']);
                                }else {
                                    $commission_name = "Not Applied";
                                }
                                // Add employee information
                                $employee_temp_array = $employee_cols;
                                $employee_temp_array['employee_name'] = $employee_name;
                                $employee_temp_array['commission_name'] = $commission_name;
                                $employee_temp_array['commission_number'] = $commission_number;
                                //if didn't have bill id, create bill basic information
                                $temp_array = array();
                                if(!isset($array_bill[$bill_id])){
                                    $array_bill[$bill_id] = array();
                                    $temp_array = $bill_temp_array + $item_temp_array + $employee_temp_array;
                                    $is_first_employee = false;
                                }elseif($is_first_employee){
                                    $temp_array = $item_temp_array + $employee_temp_array;
                                    $is_first_employee = false;
                                }else{
                                    $temp_array = $employee_temp_array;
                                }
                                $array_bill[$bill_id][] = $temp_array;
                            }

                        }
                    }else{
                        foreach ($item_row['employees'] as $employee_id => $employee_row) {
                            $employee_name = "";
                            $commission_name = "";
                            $commission_number = "";
                            if(isset($employee_map[$employee_id])) {
                                $employee = $employee_map[$employee_id];
                                $employee_name = to_full_name($employee->first_name, $employee->last_name);
                            }
                            if(isset($commission_map[$employee_row['commission_id']][$item_row['item_id']])){
                                $commission = $commission_map[$employee_row['commission_id']][$item_row['item_id']];
                                $commission_name = $commission->commission_name;
                                $commission_type = $employee_row['commission_type'];
                                $commission_value = $employee_row['commission_value'];

                                $commission_number = calculate_commission($commission_type, $commission_value, $total, $num_emp,$employee_row['sub_commission_value']);
                            }else {
                                $commission_name = "Not Applied";
                            }
                            // Add employee information
                            $employee_temp_array = $employee_cols;
                            $employee_temp_array['employee_name'] = $employee_name;
                            $employee_temp_array['commission_name'] = $commission_name;
                            $employee_temp_array['commission_number'] = $commission_number;
                            //if didn't have bill id, create bill basic information
                            $temp_array = array();
                            if(!isset($array_bill[$bill_id])){
                                $array_bill[$bill_id] = array();
                                $temp_array = $bill_temp_array + $item_temp_array + $employee_temp_array;
                                $is_first_employee = false;
                            }elseif($is_first_employee){
                                $temp_array = $item_temp_array + $employee_temp_array;
                                $is_first_employee = false;
                            }else{
                                $temp_array = $employee_temp_array;
                            }
                            $array_bill[$bill_id][] = $temp_array;
                        }
                    }

                }else{
                    $temp_array = array();
                    if(!isset($array_bill[$bill_id])){
                        //if didn't have bill id, create bill basic information
                        $array_bill[$bill_id] = array();
                        $temp_array = $bill_temp_array + $item_temp_array + $employee_cols;
                    }else{
                        // if already has bill just fill item information with employee is null
                        $temp_array = $item_temp_array + $employee_cols;
                    }
                    $array_bill[$bill_id][] = $temp_array;
                }
            }
        }

        foreach($array_bill as $bill_id => $rows){
            foreach($rows as $row_detail){
                echo '<tr>';
                foreach($row_detail as $key => $value){
                    if(!in_array($key, $exclude)){
                        $row_span = "1";
                        if(isset($row_detail['row_span']) && $row_detail['row_span'] > 1 && isset($bill_cols[$key])){
                            $row_span = 'rowspan="'. $row_detail['row_span'] .'"';
                        }
                        if(isset($row_detail['no_employee']) && $row_detail['no_employee'] > 1 && isset($item_cols[$key])){
                            $row_span = 'rowspan="'. $row_detail['no_employee'] .'"';
                        }
                        if(in_array($key,array('commission_number','discount','sales')))
                            $value = to_currency($value);
                        echo "<td {$row_span}>{$value}</td>";
                    }
                }
                echo '</tr>';
            }
        }
    }

    ?>
    </tbody>
</table>