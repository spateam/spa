<div id="mainBlock" style="padding: 20px;">
    <div style="font-size: 18px;font-weight: bold; margin-bottom: 20px">
        Healing Touch - <?php echo $branch_name; ?><br/>
        <?php if(isset($department_name) && $department_name != "") echo $department_name. " Department <br/>"; ?>
        <?php echo $name; ?>
        <button id="print" class="fright btn btn-primary">Print</button>
        <button id="csv" class="fright btn btn-warning">Csv</button>
        <button id="excel" class="fright btn btn-info">Excel</button>
    </div>
    <?php if (isset($start_date) && isset($end_date)): ?>
        <div
            style="font-size: 12px;font-weight: bold;margin-bottom: 20px"><?php echo to_user_format($start_date, false, true) . ' - ' . to_user_format($end_date, false, true); ?></div>
    <?php elseif(isset($start_date)):
        if($start_date=='All Time'):?>
        <div
            style="font-size: 12px;font-weight: bold;margin-bottom: 20px"><?= $start_date ?></div>
        <?php else: ?>
        <div
            style="font-size: 12px;font-weight: bold;margin-bottom: 20px"><?php echo to_user_format($start_date, false, true); ?></div>
        <?php endif; ?>
    <?php endif; ?>
    <div id="grid_row">
        <?php echo $data_content; ?>
    </div>
</div>

<script>
    var post_back = <?php echo isset($post_back)?json_encode($post_back):json_encode(array())?>;
    var report_name = "<?php echo isset($key)?$key:0?>";
    var system = "<?php echo $this->config->item('current_system')=='admin'?'admin/':''?>";
</script>