<div id="box">
    <div class="box_title">Healing Touch</div>
    <div class="pos_login_form">
        <div class="login_form_header" style="">LOGIN</div>
        <form name="login_form">
            <div class="form_row">
                <div class="form_title">User Name:</div>
                <div class="form_input">
                    <input tabindex="1" type="text" id="account" name="user_session[login]"/>
                </div>
            </div>
            <div class="form_row">
                <div class="form_title">Password:</div>
                <div class="form_input">
                    <input tabindex="2" type="password" id="password" name="user_session[password]"/>
                </div>
            </div>
            <?php if(isset($branch_groups)): ?>
                <div class="form_row">

                    <div class="form_title">Area:</div>
                    <div class="form_input">
                        <select tabindex="3" id="branch_group_id" name="branch_group_id">
                            <?php foreach ($branch_groups as $branch_group): ?>
                                <option value="<?php echo $branch_group->id; ?>"><?php echo $branch_group->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            <?php endif; ?>

            <div class="form_row">
                <div class="form_title">Branch:</div>
                <div class="form_input">
                    <select tabindex="3" id="branch_id" name="branch_id">
                        <?php foreach ($branches as $branch): ?>
                            <option value="<?php echo $branch->id; ?>"><?php echo $branch->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

            </div>

            <div class="form_row">
                <div class="form_title">&nbsp;</div>
                <div class="form_input">
                    <button type="submit" id="submit">Login</button>
                </div>
            </div>
            <div class="clear"></div>
        </form>
    </div>
</div>
