<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 11/4/2014
 * Time: 3:47 PM
 */
?>


<input type="hidden" id="group_id" value="<?=isset($current_group_id)?$current_group_id:'0' ?>">
<ol id='tree' data-name='permission-data'>
    <?php foreach($allPermissions as $moduleName=>$values): ?>
    <li class='expanded'><?= $moduleName?>
        <ol>
            <?php foreach($values as $row):?>
            <li class="container" permit-type='<?= $row->permission_code ?>' data-value='<?= $row->id?>' <?= isset($groupPermissions)&&in_array($row->id,$groupPermissions)?"data-checked='1'":""?>
                <?php
                    if($row->module_permission_global == 1){
                        echo "disabled";
                    }
                ?>
                ><?=$row->permission_name?></li>
            <?php endforeach;?>
        </ol>
    </li>
    <?php endforeach; ?>
</ol>

