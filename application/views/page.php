<!DOCTYPE html>
<html>
    <head>
        <title>POS System</title>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('public/images/en/icon.ico'); ?>">
        <meta name="description" content="Amazing site">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <?= $this->assets->releaseCss(); ?>
    </head>
    <?php flush(); ?>
    <body>
        <div id="wf_page">
    		<?= $layout ?>
        </div>
        <div id="wf_page_dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
        <div id="wf_page_confirm" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"><p></p></div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
        <div id="wf_page_alert" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><span type="icon" class=""></span><span class="title"></span></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>
        <div id="wf_page_spinner" style="display: none">
            <i class="fa fa-spin fa-spinner fa-pulse"></i>
        </div>
        <script type="text/javascript">
            var REQUEST_FAIL = '<?php echo REQUEST_FAIL; ?>';
            var REQUEST_SUCCESS = '<?php echo REQUEST_SUCCESS; ?>';
            var url = '<?php echo site_url(); ?>';
            var booking_url = '<?php echo booking_url(); ?>';
            var admin_url = '<?php echo admin_url(); ?>';
            var BASE_URL = '<?php echo base_url(); ?>';
            var current_url = '<?php echo current_url()?>';
            var requestTimeout = 3600000;
            var msgBoxImagePath = '<?php echo $this->assets->coreImageURL ?>';
            var update_successfully = "<?php echo "update_successfully" ?>";
            var add_successfully = "<?php echo "add_successfully" ?>";
            var current_system = "<?php echo $this->config->item('current_system') ?>";
            var moment_date_format = 'YYYY-MM-DD HH:mm:ss';
            var moment_time_format = 'YYYY-MM-DD';
            <?php $query = $this->input->get()?>
            var query_params = <?php echo $query?json_encode($this->input->get()):'{}' ?>;
            <?php if(isset($pageCode) && strlen($pageCode)): ?>
            if(typeof right !== 'undefined')
            {
                right['i'] = <?= $this->user_permission->checkPermission('i', $pageCode)==true?1:0 ?>;
                right['e'] = <?= $this->user_permission->checkPermission('e', $pageCode)==true?1:0 ?>;
                right['d'] = <?= $this->user_permission->checkPermission('d', $pageCode)==true?1:0 ?>;
            }
            else{
                right = {
                    i : <?= $this->user_permission->checkPermission('i', $pageCode)==true?1:0 ?>,
                    e : <?= $this->user_permission->checkPermission('e', $pageCode)==true?1:0 ?>,
                    d : <?= $this->user_permission->checkPermission('d', $pageCode)==true?1:0 ?>
                };
            }
            <?php endif; ?>
        </script>
        <?= $this->assets->releaseJs(); ?>
    </body>
</html>
