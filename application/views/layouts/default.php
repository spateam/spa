<div data-color="grey" class="flat">
    <div id="wrapper">
        <!-- Header -->
        <div id="header">
            <?php echo $header ?>
        </div>
        <!-- Menu -->
        <div id="menu">
            <?php echo $menu ?>
        </div>
        <!-- Content -->
        <div id="content">
            <?php if(isset($content_title)): ?>
            <div id="content-header" class="hidden-print">
                <?php echo $content_title; ?>
            </div>
            <?php endif; ?>
            <?php if(isset($breadcrumb)): ?>
                <div id="breadcrumb" class="hidden-print"><?php echo $breadcrumb; ?></div>
                <div class="clear"></div>
            <?php endif; ?>
            <?php echo $content ?>
        </div>
    </div>
</div>

