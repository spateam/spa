
<div class='csslider1 autoplay '>
    <input name="cs_anchor1" id='cs_slide1_0' type="radio" class='cs_anchor slide' >
    <input name="cs_anchor1" id='cs_slide1_1' type="radio" class='cs_anchor slide' >
    <input name="cs_anchor1" id='cs_play1' type="radio" class='cs_anchor' checked>
    <input name="cs_anchor1" id='cs_pause1_0' type="radio" class='cs_anchor pause'>
    <input name="cs_anchor1" id='cs_pause1_1' type="radio" class='cs_anchor pause'>
    <ul>
        <div>
            <img src="<?php echo $this->assets->releaseProductImage($images[0]) ?>" style="width: 100%;">
        </div>
        <?php $i = 0; foreach($images as $image): ?>
        <li class="num<?php echo $i++?> img">
            <img title="Capture" src="<?php echo $this->assets->releaseProductImage($image) ?>">
        </li>
        <?php endforeach; ?>
    </ul>

    <div class='cs_play_pause'>
        <label class='cs_play' for='cs_play1'><span><i></i></span></label>
        <label class='cs_pause num0' for='cs_pause1_0'><span><i></i></span></label>
        <label class='cs_pause num1' for='cs_pause1_1'><span><i></i></span></label>

    </div>
    <div class='cs_arrowprev'>
        <label class='num0' for='cs_slide1_0'><span><i></i></span></label>
        <label class='num1' for='cs_slide1_1'><span><i></i></span></label>
    </div>
    <div class='cs_arrownext'>
        <label class='num0' for='cs_slide1_0'><span><i></i></span></label>
        <label class='num1' for='cs_slide1_1'><span><i></i></span></label>
    </div>

    <div class='cs_bullets'>
        <?php $i = 0; foreach($images as $image): ?>
        <label class='num<?php echo $i ?>' for='cs_slide1_0'>
            <span class='cs_point'></span>
            <span class='cs_thumb'><img src='<?php echo $this->assets->releaseProductImage($image) ?>' alt='Capture' title='Capture' /></span>
        </label>
        <?php endforeach; ?>
    </div>
</div>