<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class POS_Table_Model extends POS_Model
{

    protected $tableName;
    protected $fieldList;
    protected $fieldListRequirement;
    protected $tableType;
    protected $is_all;
    public $availableStatus;

    function __construct()
    {
        parent::__construct();
        $this->fieldListRequirement = array();
        $this->availableStatus = array(Status::Active);
        $this->is_all = false;
    }

    /** set the is_all property value
     * @param $value
     */
    public function set_is_all($value){
        $this->is_all = $value;
    }

    /** get the is_all property
     * @return bool
     */
    public function get_is_all(){
        return $this->is_all;
    }

    /**
     * @param $field_list
     * @param int $id
     * @return mixed
     */
    function checkDataConstraints($field_list, $id = 0){
        $checkConstraints = $this->table_constraints->checkDataConstraints($field_list, $this->fieldListRequirement, $this->tableName, $id);
        return $checkConstraints;
    }

    /** return all availableStatus property
     * @return string
     */
    function getAvailableStatus(){
        return $this->availableStatus;
    }

    /** reset the requirement rules of the fields
     * @param $value
     */
    public function setFieldListRequirement($value){
        $this->fieldListRequirement = $value;
    }

    /** return all the field list
     * @return mixed
     */
    function getFieldList()
    {
        return $this->fieldList;
    }

    /** get a record base on the ID of the entity
     * @param null $ID: the id of the entty
     * @param array $permission: the permission values of the entity
     * @param string $user_level: current loggin level
     * @param array $condition: another condition of the entity
     * @return null
     */
    function getByID($ID = null, $permission = array(), $user_level = "",$condition = array())
    {
        if(! $ID){
            return null;
        }
        $result = $this->get(array_merge(array('id' => $ID),$condition),'','','',$user_level,$permission);
        if(count($result) > 0){
            return $result[0];
        }
        return null;
    }

    /** Generate a set of info to select records
     * @param array $condition
     * @param string $user_level
     * @param array $permission
     * @return array
     */
    function generateBaseInfo($condition = array(), $user_level = "", $permission = array()){
        $info = array();
        $condition = $this->renderCondition($condition);
        $info['select'] = array($this->tableName => $this->fieldList);
        $info['from'] = $this->tableName;
        if ((is_array($condition) && count($condition) > 0) || is_string($condition)) {
            $info['where'] = $condition;
        }
        if($user_level !== ""){
            $info['user_level'] = $user_level;
        }
        $info['permission'] = $permission;
        return $info;
    }

    /** like get method, but only 1 params to be passed
     * @param array $param
     * @return mixed
     */
    function get_minify($param){
        $default = array(
            'condition'     => array(),
            'order_by'      => '',
            'limit'         => '',
            'offset'        => '',
            'user_level'    => '',
            'permission'    => array(),
            'no_permission' => array(),
        );
        $param = array_merge($default,$param);
        return $this->get(
            $param['condition'],
            $param['order_by'],
            $param['limit'],
            $param['offset'],
            $param['user_level'],
            $param['permission'],
            $param['no_permission']
        );
    }

    /**
     * @param array $condition
     * @param string $order_by
     * @param string $limit
     * @param string $offset
     * @param string $user_level
     * @param array $permission
     * @param bool $no_permission
     * @return mixed
     */
    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array(), $no_permission = false)
    {
        /* ----- Get Branch -----*/
        if(in_array('branch_id',$this->fieldList) && !isset($condition['branch_id'])){
            if($this->config->item('current_system') == 'staff'){
                $branch_id = $this->user_check->get('branch_id');
                $condition['branch_id'] = $branch_id;
            }else{
                $condition['branch_id']  = $this->user_permission->get_branch();
            }

        }
        $info = $this->generateBaseInfo($condition, $user_level, $permission);
        if ($limit != "") {
            $info['limit'] = $limit;
        }
        if ($offset != "") {
            $info['start'] = $offset;
        }
        if ($order_by != "") {
            $info['order'] = $order_by;
        }
        if($no_permission){
            $info['no_permission'] = $no_permission;
        }
        $info['count'] = true;
        return $this->select($info)->result();
    }

    /**
     * used to get total record when passing some condititon
     * @param array $condition
     * @param string $user_level
     * @param array $permission
     * @return int
     */
    function getTotal($condition = array(), $user_level = "", $permission = array()){
        $info = $this->generateBaseInfo($condition, $user_level, $permission);
        return $this->num_rows($info);
    }

    /** used to get some records base some information, after that it will convert data into mapping format
     * @param string $keyField
     * @param string $valueField
     * @param array $condition
     * @param bool $unique
     * @param string $user_level
     * @param array $permission
     * @param string $order_by
     * @return array
     */
    function getTableMap($keyField = 'id', $valueField = "",  $condition = array(), $unique = true, $user_level = "", $permission = array(),$order_by = "")
    {
        $table = $this->get($condition,$order_by, "", "",  $user_level, $permission);
        $ret = convert_to_array($table, $keyField, $valueField, $unique);
        return $ret;
    }

    /** used to render condition into valid condition, add more neccessary condition before being rendered to sql statement
     * @param $condition
     * @param string $for
     * @return array
     */
    function renderCondition($condition, $for = "select")
    {
        $new_condition = array();
        if(is_array($condition)){
            foreach($condition as $key => $value){
                if(is_string($key) && strpos($key, '.') === false){
                    $key = $this->tableName . "." . $key;
                }
                $new_condition[$key] = $value;
            }
        }elseif(is_string($condition)){
            $new_condition[] = $condition;
        }
        if($for == "select"){
            if ($this->tableType != 'connected' && isset($new_condition[$this->tableName . '.' . 'status']) == false && $this->availableStatus) {
                $new_condition[$this->tableName . '.' . 'status'] = $this->availableStatus;
            }
        }
        return $new_condition;
    }

    /** used to check data, rendered data into valid data before rendered to sql statement
     * @param $field_list
     * @return array
     */
    function renderDataForInsert($field_list)
    {
        if(is_object($field_list)){
            $field_list = (array) $field_list;
        }
        if(!isset($field_list['id']) || count($field_list) != 1){
            $checkConstraints = $this->table_constraints->checkDataConstraints($field_list, $this->fieldListRequirement, $this->tableName, 0);
            if ($checkConstraints !== true) {
                echo json_encode(array('status' => REQUEST_FAIL,'data'=>array('validate_error' => $checkConstraints)));
                exit;
            }
        }

        $willInsertFields = array();
        foreach ($field_list as $key => $value) {
            if (in_array($key, $this->fieldList))
                $willInsertFields[$key] = $value;
        }

        if(count($willInsertFields) > 0){
            if(in_array('creator', $this->fieldList)){
                if ($this->session->userdata('login') !== false) {
                    $login = $this->session->userdata('login');
                    if(isset($login->user_id))
                        $willInsertFields['creator'] = $login->user_id;
                }
            }
            if (in_array('created_date', $this->fieldList) && !isset($willInsertFields['created_date'])) {
                $willInsertFields['created_date'] = get_database_date();
            }
            if(in_array('status', $this->fieldList) && !isset($field_list['status'])){
                $willInsertFields['status'] = is_array($this->availableStatus)?$this->availableStatus[0]:$this->availableStatus;
            }
        }
        return $willInsertFields;
    }

    /** just like insert method, but only 1 param
     * @param array $param
     * @return bool|int
     */
    function insert_minify($param){
        $default = array(
            'field_list'    => array(),
            'user_level'    => array(),
            'permission'    => array(),
            'force_insert'  => false,
            'log_data'      => array()
        );
        $param = array_merge($default,$param);
        return $this->insert($param['field_list'], $param['user_level'], $param['permission'], $param['force_insert'], $param['log_data']);
    }

    /** used to insert a record
     * @param $field_list: data to be inserted
     * @param string $user_level: current logging level
     * @param array $checkValue: permission data of the record
     * @param bool $force_insert:  force insert if there is already have id or not
     * @param array $log_data: the log data which will override the default log data
     * @return bool|int
     */
    function insert($field_list, $user_level = "", $checkValue = array(), $force_insert = false, $log_data = array())
    {
        $ret = false;
        $id = 0;
        $willInsertFields = $this->renderDataForInsert($field_list);
        if(count($willInsertFields) > 0){
            $ret = true;
//            $this->db->trans_begin();
            if(isset($willInsertFields['id']) && $force_insert == false){
                $id = $willInsertFields['id'];
            }else{
                $id =  $this->insert_database($this->tableName, $willInsertFields);
            }
            if($id !== false && $id !== 0){
                $restriction_data = $this->get_data_restriction($user_level, $checkValue);
                if(count($restriction_data) > 0){
                    foreach($restriction_data as $key => $value){
                        if(isset($checkValue[$value['field']])){
                            $permission_table = $this->tableName . "_" . $value['table'];
                            $table_field = $this->tableName . "_id";
                            $info = array(
                                'from' => $permission_table,
                                'where' => array($table_field => $id, $value['field'] => $checkValue[$value['field']])
                            );
                            $num_rows = $this->num_rows($info);
                            if($num_rows == 0){
                                $permission_ret = $this->insert_database($permission_table, array($table_field => $id, $value['field'] => $checkValue[$value['field']]));
                                if($permission_ret === false){
                                    $ret = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if($ret !== false){
//                $this->db->trans_commit();
                $this->save_log($id, 'insert', $log_data);
            }else{
//                $this->db->trans_rollback();
            }
        }
        return ($id != 0 && $ret !== false) ? $id : $ret;
    }

    /**
     * used to check data, rendered data into valid data before rendered to sql statement
     * @param $id
     * @param $field_list
     * @param bool $constraint_check
     * @return array
     */
    function renderDataForUpdate($id, $field_list, $constraint_check = true)
    {
        if($constraint_check){
            $checkConstraints = $this->table_constraints->checkDataConstraints($field_list, $this->fieldListRequirement, $this->tableName, $id);
            if ($checkConstraints !== true) {
                echo json_encode(array('status' => REQUEST_FAIL,'data'=>array('validate_error' => $checkConstraints)));
                exit;
            }
        }


        $willUpdateFields = array();
        foreach ($field_list as $key => $value) {
            if (in_array($key, $this->fieldList))
                $willUpdateFields[$key] = $value;
        }

        if (in_array('updated_date', $this->fieldList) && !isset($willUpdateFields['update_date'])) {
            $willUpdateFields['updated_date'] = get_database_date();
        }
        return $willUpdateFields;
    }

    /** just like update method, but only 1 param
     * @param $param
     * @return bool
     */
    function update_minify($param){
        $default = array(
            'id'                    => '',
            'field_list'            => array(),
            'constraint_check'      => array(),
            'log_data'              => false,
        );
        $param = array_merge($default,$param);
        return $this->update($param['id'],$param['field_list'], $param['constraint_check'], $param['log_data']);
    }

    /** used to update a record base on the passed id
     * @param $id
     * @param $field_list
     * @param bool $constraint_check
     * @param array $log_data
     * @return bool
     */
    function update($id, $field_list, $constraint_check = true, $log_data = array())
    {
        $willInsertFields = $this->renderDataForUpdate($id, $field_list, $constraint_check);
        $ret = $this->update_database($this->tableName, $willInsertFields, array('id' => $id));
        if($ret !== false){
            $this->save_log($id, 'update', $log_data);
        }
        return $ret;
    }

    /** used to delete multiple records base on the passed condition
     * @param array $condition
     * @param string $user_level
     * @param array $checkValue
     * @param bool $force_delete
     * @return bool
     */

    function delete($condition = array(), $user_level = "", $checkValue = array(), $force_delete = false){
        $ret = false;
        if((is_array($condition) && count($condition) > 0) || is_string($condition)){
            $ret = true;
            $restriction_data = $this->get_data_restriction($user_level, $checkValue, false);
            if(count($restriction_data) > 0){
                $get_data = $this->get($condition);
                $ids = array();
                foreach($get_data as $row){
                    $ids[] = $row->id;
                }
                $last_info = array();
                if(count($ids) > 0){
                    foreach($restriction_data as $key => $value){
                        if(isset($checkValue[$value['field']])){
                            $permission_table = $this->tableName . "_" . $value['table'];
                            $table_field = $this->tableName . "_id";
                            $permission_ret = $this->delete_database($permission_table, array($table_field => $ids, $value['field'] => $checkValue[$value['field']]));
                            $last_info = $value;
                            if($permission_ret === false){
                                $ret = false;
                                break;
                            }
                        }
                    }
                }
                $restriction_source = $this->permissionValue[$this->tableName];
                if(count($restriction_data) == count($restriction_source)){
                    $permission_table = $this->tableName . "_" . $last_info['table'];
                    $table_field = $this->tableName . "_id";
                    $info_select = array();
                    $info_select['select'] = '*';
                    $info_select['from'] = $permission_table;
                    $info_select['where'] = array($table_field => $ids);
                    $query = $this->select($info_select);
                    if($query->num_rows()== 0){
                        if($this->tableType == 'main' && $force_delete !== true){
                            $ret = $this->update_database($this->tableName, array('status' => 0), array('id' => $ids));
                        }else{
                            $ret = $this->delete_database($this->tableName, $condition);
                        }
                    }
                }
            }

            $above_permission_checker = $this->get_data_restriction($user_level, $checkValue);
            if(count($above_permission_checker) == 0) {
                $is_delete = true;
                if ($this->tableType == 'main' && $force_delete !== true) {
                    $ret = $this->update_database($this->tableName, array('status' => 0), $condition);
                } else {
                    $ret = $this->delete_database($this->tableName, $condition);
                }
            }
            if ($ret !== false) {
//            $this->db->trans_commit();
            }else{

//            $this->db->trans_rollback();
            }
        }
        return $ret;
    }

    /** used to save log after a database transaction
     * @param $id
     * @param $type
     * @param array $override_data
     */

    function save_log($id, $type, $override_data = array())
    {
        if ($this->db->table_exists($this->tableName . '_log')) {
            // begin to write logs
            $row = $this->db->get_where($this->tableName, array($this->fieldList[0] => $id));
            $user = $this->session->userdata('login');
            if ($row->num_rows > 0) {
                $row = $row->result()[0];
                $row->log_user = isset($user->user_id)?$user->user_id:0;
                $row->log_time = get_database_date();
                $row->log_type = $type;

                $new_data = (array) $override_data + (array)$row;

                $this->db->insert($this->tableName . '_log', $new_data);
            }
        }

    }

    /** get the maximum value of a field name
     * @param $field
     * @return mixed
     */
    function getMaxValue($field)
    {
        $this->db->select_max($field);
        $result = $this->db->get($this->tableName)->row();
        return $result->$field;
    }

    function get_data_restriction($userLevel = "", &$checkValue = array(), $goDown = true){
        $ret = array();
        if (isset($this->permissionValue[$this->tableName])) {
            $login = new stdClass();
            if ($this->session->userdata('login') !== false) {
                $login = $this->session->userdata('login');
            }
            if ($userLevel == "") {
                $permissionValueKey = $this->config->item('permissionLevelKey');
                if (isset($login->$permissionValueKey)) {
                    $userLevel = $login->$permissionValueKey;
                }
            }
            if ($userLevel != "") {
                $permissionValue = $this->permissionValue[$this->tableName];
                if($goDown){
                    krsort($permissionValue);
                }else{
                    ksort($permissionValue);
                }
                foreach ($permissionValue as $key => $value) {
                    if($goDown){
                        if ($userLevel <= $key) {
                            if (!isset($checkValue[$value['field']])) {
                                if (isset($login->$value['field'])) {
                                    $checkValue[$value['field']] = $login->$value['field'];
                                }
                            }
                            $ret[$key] = $value;
                        }else{
                            break;
                        }
                    }else{
                        if ($userLevel >= $key) {
                            if (!isset($checkValue[$value['field']])) {
                                if (isset($login->$value['field'])) {
                                    $checkValue[$value['field']] = $login->$value['field'];
                                }
                            }
                            $ret[$key] = $value;
                        }else{
                            break;
                        }
                    }
                }
            }
        }
        return $ret;
    }

    /** generate some more condition in where statement and join with other neccessary table in from statement
     * @param array $checkValue
     * @param string $alias
     * @param string $userLevel
     * @return array
     */
    function get_join_restriction($checkValue = array(), $alias = "", $userLevel = "")
    {
        $ret = array();
        $permissionWhereCondition = "1";
        $selectMore = array();

        $restriction_data = $this->get_data_restriction($userLevel, $checkValue);
        if(count($restriction_data) > 0){
            if ($alias == "") {
                $alias = $this->tableName;
            }
            $global_conditions = array("(" . $this->tableName . ".`global` = 1)");
            $permission_conditions = array();
            $passed_objects = array();
            $lastKey = end(array_keys($restriction_data));
            foreach ($restriction_data as $key => $value) {
                if (isset($checkValue[$value['field']])) {
                    $permission_table = $this->tableName . "_" . $value['table'];
                    $fieldCompareWithRootTable = $this->tableName . "_id";
                    $fieldSelectMore = "{$permission_table}.{$value['table']}_id";
                    $fieldSelectMoreAlias = "{$permission_table}_id";

                    // Create join table information
                    $condition = array();
                    $condition[] = "{$alias}.id = {$permission_table}.{$fieldCompareWithRootTable}";
                    $condition[$permission_table . "." . $value['field']] = $checkValue[$value['field']];
                    $ret[$permission_table] = array('type' => 'left', 'table' => $permission_table, 'condition' => $condition);
                    // Check to add condition to check global or not
                    if($key != $lastKey){
                        $global_condition = "{$permission_table}.`global` = 1";
                        if(count($passed_objects) > 0){
                            foreach($passed_objects as $old_object){
                                $old_object_table = $this->tableName . "_" . $old_object['table'];
                                $global_condition .= " AND {$old_object_table}.{$old_object['field']} IS NOT NULL";
                            }
                        }
                        $global_conditions[] = "(" . $global_condition . ")";
                    }
                    $passed_objects[] = $value;
                    // Add condition to check has permission or not
                    $permission_conditions[] = "{$permission_table}.{$value['field']} IS NOT NULL";
                    $selectMore[] = "{$fieldSelectMore} as {$fieldSelectMoreAlias}";
                }
            }
            $permissionWhereArr = array();
            if(count($permission_conditions) > 0){
                $permission_conditions_string = implode(" AND ", $permission_conditions);
                $selectMore[] = "IF({$permission_conditions_string}, 1, 0) as {$this->tableName}_permission";
                $permissionWhereArr[] = "($permission_conditions_string)";
            }
            if(count($permission_conditions) > 0 && count($global_conditions) > 0){
                $global_conditions_string = implode(" OR ", $global_conditions);
                $selectMore[] = "IF({$global_conditions_string}, 1, 0) as {$this->tableName}_global";
                $permissionWhereArr[] = "($global_conditions_string)";
            }
            if(count($permissionWhereArr) > 0){
                $permissionWhereCondition = implode(" OR ", $permissionWhereArr );
            }
        }
        return array(
            'select' => count($selectMore) > 0 ? implode(',', $selectMore) : "",
            'where' => $permissionWhereCondition,
            'from' => $ret
        );
    }

    /** used to retrieve condition in an array with many condition of different tables
     * @param null $array
     * @param string $name
     * @return array
     */
    function to_condition($array = null,$name = ''){
        if(!$array){
            return array();
        }
        if(!isset($array[$name])){
            return array();
        }else{
            $array = $array[$name];
        }
        $temp = array();
        if(! $name){
            foreach($array as $table=>$condition){
                foreach($condition as $key=>$value){
                    if(strpos($key,$table.'.') === -1){
                        $temp[$key] = $value;
                    }else{
                        $temp[$table.'.'.$key] = $value;
                    }
                }
            }
        }else{
            foreach($array as $key=>$value){
                if(is_numeric($key)){
                    $temp[] = $value;
                }
                elseif(strpos($key,$name.'.') === -1){
                    $temp[$key] = $value;
                }else{
                    $temp[$name.'.'.$key] = $value;
                }

            }
        }

        return $temp;
    }

    /**
     * get branch group of the current loggin user
     * @param $id
     * @return array|null
     */
    function get_branch_group($id){
        if($this->db->table_exists($this->tableName. '_branch_group')){
            $result = $this->db->get_where($this->tableName. '_branch_group', array($this->tableName. '_id' => $id))->result();
            return convert_to_array($result,'','branch_group_id');
        }
        return null;
    }

    /**
     * get the brach of the current login user
     * @param $id
     * @return array|null
     */
    function get_branch($id){
        if($this->db->table_exists($this->tableName. '_branch')){
            $result = $this->db->get_where($this->tableName. '_branch', array($this->tableName. '_id' => $id))->result();
            return convert_to_array($result,'','branch_id');
        }
        return null;
    }

    function getConfig($code = ''){

    }
}

