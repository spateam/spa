<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class POS_Report_Model extends POS_Model {
    function __construct() {
        parent::__construct();
    }

    function get_form_data(){
        return new stdClass();
    }

    function get_detail_data($post_data){
        return new stdClass();
    }

    function get_export_data( $data){
        $this->load->library('export_original_data');
        $aaaa = $this->export_original_data->parse_export($data);
        return $aaaa;

    }

    /**
     * @param $array
     * @param array $level_sort = array('name' => '', 'function' = function)
     * @return array
     */
    function mapping_data($array, $level_sort = array(), $is_unique = false){
        $ret_array = array();
        foreach($array as $row){
            if(is_callable($level_sort['first_function'])){
                $level_sort['first_function']($ret_array,$row);
            }
            $each_level = &$ret_array['rows'];
            $current_level = 1;
            $max_level = count($level_sort['mapping_function']);
            foreach($level_sort['mapping_function'] as $name => $function){
                if(is_callable($function)){
                    $function($each_level[$row->$name],$row);
                }
                if($current_level == $max_level){
                    if($is_unique){
                        $each_level[$row->$name] = $row;
                    }
                    else{
                        $each_level[$row->$name]['rows'][] = $row;
                    }
                }else{
                    $each_level = &$each_level[$row->$name]['rows'];
                }
                $current_level++;
            }
        }

        return $ret_array;
    }

    function to_export_text($text = '',$level = '', $colspan = 1, $rowspan = 1, $export_type = ''){
        $report_stype = $this->config->item('report_style');
        if(isset($report_stype['level_'.$level])){
            $report_stype = $report_stype['level_'.$level];
        }else{
            $report_stype = array();
        }
        return array_merge(array(
            'export_text' => $text,
            'colspan' => $colspan,
            'rowspan' => $rowspan,
            'export_type' => $export_type
        ),$report_stype);
    }
}

