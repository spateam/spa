<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//$core_folder = "application/core/";
//foreach(glob($core_folder . "Controllers/*.php") as $file_name){
//    $include_file_name = str_replace($core_folder, "", $file_name);
//    include_once $include_file_name;
//}
// Huy Note: Autoload bug when Admin inherit staff controller (A of Admin is laoding first)
include_once "Controllers/Staff_Controller.php";
include_once "Controllers/Admin_Controller.php";
include_once "Controllers/Booking_Controller.php";

class POS_Controller extends CI_Controller {
    protected $dir;
    protected $system = "";
    protected $controller_name;
    protected $moduleName = "";
    protected $pageCode = "";
    protected $pageName = "";
    protected $pageIcon = "";
    protected $pageDescription = "";
    protected $defaultLayout = "default";
    protected $breadcrumbs = array();

    public function __construct() {
        parent::__construct();
        if($this->input->is_ajax_request()){
            /*-- If it is an ajax request and the data of ajax request 'current_system' has been set,
            set the current system = ajax request 'current_system' --*/
            if($this->input->post('current_system') !== false){
                $this->set_system($this->input->post('current_system'));
            }
        }
        /* get the controller name */
        $this->controller_name = $this->router->fetch_class();
        $this->dir = $this->router->fetch_directory() . $this->controller_name;
    }

    /**
     * This function will be executed before execute any controller's method.
     * For Example:
     * If the system call index function. Before the index function is called.
     * The _remap function will be called. And in the _remap function it will call the index function.
     * @param $method
     * @param array $params
     *
     */
    public function _remap($method,$params = array())
    {
        /* If method not exists show this page doesn't exists (404) */
        if (!method_exists($this, $method)){
            show_404();
        }

        if($this->input->is_ajax_request()) {
            /* If this is an ajax request, check if any error occur,
            it will return as an AFormat fail message */
            try{
                call_user_func_array(array($this, $method), $params);
            }catch(Exception $e){
                $this->ajax_content(REQUEST_FAIL,$e->getMessage());
            }
        }else{
            /* If this is not an ajax request, check if any error occur,
            it will return as a string message */
            try{
                call_user_func_array(array($this, $method), $params);
            }catch(Exception $e){
                echo $e->getMessage();
            }

        }

    }

    function set_system($system){
        /* A function called to set what is current system is */
        $this->system = $system;
        $this->config->set_item('current_system',$system);
    }

    /**
     * This function is used to render a page and output it in HTML. In this function it will automatically called all components of a layout and output them as html
     * @param $contentName: the content view's name which will be loaded
     * @param $vars: the variables of the content view's name which will be used on the content's view
     * @param string $layout: layout which will be loaded that contains the content view
     * @param bool $return: return as html or print on the screen
     * @return html content or a string which is a html content
     */
    protected function page($contentName, $vars, $layout = '', $return = false){
        /* if layout is not set, get the controller default layout */
        if($layout === ''){
            $layout = $this->defaultLayout;
        }
        /* all layout string should be in lower case */
        $layout = strtolower($layout);
        /* get layout data from the database including components*/
        $data = $this->load->controller_model('layout')->getLayoutData($layout);
        /* if the input $vars is null, set it as empty object,
        and if not convert it to an object if it is an array*/
        if($vars == null){
            $vars = new stdClass();
        }else{
            $vars = (object)$vars;
        }
        /* get component's html */
        $vars->layout_data = array();
        foreach ($data->components as $component) {
            $component_var = $this->get_data_component($component->code);
            $vars->layout_data[$component->code] = $this->load->component($component->path, $component_var, TRUE);
        }
        /* end of get content's html */
        if($this->system != ""){
            /* if system is set, assign the content name a prefix that is the folder name of the system */
            $systemContentName = $this->system . "/" . $contentName;
            $checkContent = $this->load->check_content_exists($systemContentName);
            if($checkContent !== false){
                /* if the content exist in the system folder, assign the content name with the system folder name*/
                $contentName = $systemContentName;
            }
        }
        return $this->load->page($contentName, $vars, $layout, $return);
    }

    /**
     * This function is used to gather data from components of specific type of controller,
     * you can override this function in another child controller to get different variable set you want
     * @param $code
     * @return stdClass
     */
    function get_data_component($code){
        $data = new stdClass();
        $user_level = $this->user_check->get('login_as_type');
        switch($code){
            case 'menu':
                if($user_level < 3){
                    $condition = array('type' => MENU_TYPE(array('Staff','All')));
                }else {
                    $condition = array('type' => MENU_TYPE(array('Admin','All')));
                }
                $this->load->library('menu_component');
                $data->menus = $this->menu_component->get_data($condition);
                $data->pageCode = $this->pageCode;
                $data->pageName = $this->pageName;
                break;
            case 'breadcrumb':
                $data->breadcrumbs = $this->breadcrumbs;
                break;
            case 'content_title':
                $data->pageName = $this->pageName;
                $data->pageIcon = $this->pageIcon;
                break;
        }
        return $data;
    }

    /**
     * Load content's view base on $contentName and output it as html
     * This is used seperately from function page(), page will load a layout but this doesn't
     * @param $contentName
     * @param array $data
     * @param bool $return
     * @return mixed
     */
    protected function content($contentName, $data = array(), $return = true){

        if($this->system != ""){
            /* if system is set, assign the content name a prefix that is the folder name of the system */
            $systemContentName = $this->system . "/" . $contentName;
            $checkContent = $this->load->check_content_exists($systemContentName);
            if($checkContent !== false){
                /* if the content exist in the system folder, assign the content name with the system folder name*/
                $contentName = $systemContentName;
            }
        }
        return $this->load->content($contentName, $data, $return);
    }

    /**
     * Used to make the result as an AFormat
     * @param $status
     * @param string $msg
     * @param string $data
     * @param bool $is_ret
     * @return array
     */
    function ajax_content($status, $msg = "", $data = "", $is_ret = false){
        /* If if is an ajax, return as json */
        if($this->input->is_ajax_request() && !$is_ret){
            echo json_encode(array(
                "status" => $status,
                "message" => $msg,
                "data" => $data
            ));
        }
        /* If is not an ajax request, return as an array */
        return array(
            "status" => $status,
            "message" => $msg,
            "data" => $data
        );
    }

    /**
     * Determine that a request need to be checked login or not
     * @return bool
     */
    function is_check_login()
    {
        $ret = false;
        /* get the attribute 'authorize_checker' in the config*/
        $login_checker = $this->config->item('authorize_checker');
        /* check if the current system have been defined to check or not
        if not return as false (meaning not to check login)
        */
        if (isset($login_checker) && isset($login_checker[$this->system])) {
            $system_checker = $login_checker[$this->system];
            /* if the system check is assign bool return the value*/
            if (is_bool($system_checker)) {
                return $system_checker;
            }
            /* if not, continue to check on controller defined or not */
            if (isset($system_checker['controller']) && isset($system_checker['controller'][$this->controller_name])) {
                $controller_checker = $system_checker['controller'][$this->controller_name];
                /* check if the controller checker is bool or not, if yes, return its value*/
                if (is_bool($controller_checker)) {
                    return $controller_checker;
                }
                /* check if in controller checker there is a method defined which is the method the request is point to or not */
                if (isset($system_checker['controller'][$this->controller_name]['method'][$this->router->method])) {
                    $method_checker = $system_checker['controller'][$this->controller_name]['method'][$this->router->method];
                    /* if the method_checker exists and its a bool value, return the bool value */
                    if (is_bool($method_checker)) {
                        return $method_checker;
                    }
                }
                /* check if in controller checker there is a default or not, if it has, return the default value */
                if (isset($system_checker['controller'][$this->controller_name]['default']) &&is_bool($system_checker['controller'][$this->controller_name]['default'])) {
                    return $system_checker['controller'][$this->controller_name]['default'];
                }
            }
            /* if controller is not defined, check if there is a default attribute in system checker or not,
            if not return as false */
            if (isset($system_checker['default'])) {
                /* if it is a bool value return its value */
                if (is_bool($system_checker['default'])) {
                    return $system_checker['default'];
                }
                /* check if there is a method checker in the default or not */
                if (isset($system_checker['default']['method'][$this->router->method])) {
                    $method_checker = $system_checker['default']['method'][$this->router->method];
                    if (is_bool($method_checker)) {
                        return $method_checker;
                    }
                }
                /* check if there is a default in default or not, if has return its value if it is a bool */
                if (isset($system_checker['default']['default']) && is_bool($system_checker['default']['default'])) {
                    if(is_bool($system_checker['default']['default'])){
                        return $system_checker['default']['default'];
                    }
                }
            }
        }
        return $ret;
    }
    function check_login(){
        /* first check if it need to check login or not */
        if($this->is_check_login()){
            /* get the login session in the system */
            $login = $this->session->userdata('login');
            /* check if the login session existed or not */
            if(!isset($login) || $login === false){
                /* check if this is a ajax request or not */
                if($this->input->is_ajax_request()){
                    /* return json AFormat  and redirect user to login page*/
                    $this->ajax_content(REQUEST_SUCCESS, "", array('type' => 'redirect', 'option' => array('url' => site_url($this->system)))); exit;
                }else{
                    /* redirect the user to login page */
                    redirect($this->system);
                }
            }
        }
    }
}
