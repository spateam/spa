<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_Controller extends POS_Controller {

    function __construct() {
        parent::__construct();
        $this->set_system("booking");
        $this->defaultLayout = "booking";
        $this->breadcrumbs = array();
        $this->load->library('cart');
    }

    function index(){
        $this->page($this->moduleName,array());
    }

    function get_data_component($code){
        $data = new stdClass();
        switch($code){
            case 'menu':
                $this->load->library('menu_component');
                $data->menus = $this->menu_component->get_categories();
                $data->pageCode = $this->pageCode;
                break;
            case 'breadcrumb':
                $data->breadcrumbs = $this->breadcrumbs;
                break;
        }
        return $data;
    }
}