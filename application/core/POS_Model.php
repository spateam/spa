<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//$core_folder = "application/core/";
//foreach(glob($core_folder . "Models/*.php") as $file_name){
//    $include_file_name = str_replace($core_folder, "", $file_name);
//    include_once $include_file_name;
//}

include_once "Models/Controller_Model.php";
include_once "Models/Admin_Controller_Model.php";
include_once "Models/Report_Model.php";
include_once "Models/Table_Model.php";

class POS_Model extends CI_Model {

    protected $modelName;
    protected $permissionValue;

    function __construct() {
        parent::__construct();
        /* automatically get the model name */
        $this->modelName = strtolower(get_class($this));
        /* get the permissionValue of the config */
        $this->permissionValue = $this->config->item('permissionValue');
    }

    function getNormalCaseString($value, $text=''){
        $ret = ($text!='') ? " ".$text." " : "";
        if(is_string($value)){
            $ret .= $value;
        }elseif(is_array($value)){
            $ret .= implode(', ', $value);
        }
        return $ret;
    }
    /**
     * selectSQLCreator
     *
     * Create sql string to run on query functions
     *
     * @param $info
     * @return string
     * @author Tuan
     */
    public function sql_creator($info){
        if(!is_array($info)){
            return (is_string($info)) ? $info : '';
        }
        $userLevel = "";
        if(isset($info['user_level'])){
            $userLevel = $info['user_level'];
        }
        $permissionCheck = "";
        if(isset($info['permission'])){
            $permissionCheck = $info['permission'];
        }
        if(!isset($info['no_permission'])){
            $info['no_permission'] = false;
        }
        //convert select to array to modify when create from command
        if(!isset($info['select'])){
            $info['select'] = array('*');
        }elseif(isset($info['select']) && is_string($info['select'])){
            $info['select'] = array($info['select']);
        }
        //create from command and add base conditions to where
        /** array structure
         * array(
         *  'alias'    => "table_name" / array('table'=>'table_name'),
         *  'alias1'   => array(
         *                      'type'=>'INNER/RIGHT/LEFT', (default is INNER)
         *                      'table'=>'table_name',
         *                      'condition'=>'string_compare',
         *                      'isQuery' => true/false
         *                     ),
         *  'alias2'   => ...
         * );
         */
        $baseConditions = "1";
        $from = "";
        if(is_string($info['from'])){
            //if just have only one table use this case
            $from .= $this->_from($info['from'], "", true);
            //check this table has value permission configuration or not
            if(isset($this->permissionValue[$info['from']]) && $info['no_permission'] == false){
                $modelName = $this->load->table_model($info['from']);
                $valuePermission = $modelName->get_join_restriction($permissionCheck, "", $userLevel);
                foreach($valuePermission['from'] as $key=>$value){
                    $from .= $this->_from($value, $key);
                }
                if($valuePermission['where'] != ""){
                    $baseConditions = "(" . $valuePermission['where'] . ")";
                }
                if($valuePermission['select'] != ""){
                    $info['select'][] = $valuePermission['select'];
                }
            }
        }elseif(is_array($info['from'])){
            //if have JOIN Or use UNION use this case
            $fromSql = "";
            foreach ($info['from'] as $key => $value) {
                if($fromSql == ""){
                    $fromSql = $this->_from($value, $key, true);
                }else{
                    $fromSql .= $this->_from($value, $key);
                }
                //check this table has value permission configuration or not
                if(isset($this->permissionValue[$value['table']]) && $info['no_permission'] == false){
                    $modelName = $this->load->table_model($value['table']);
                    $valuePermission = $modelName->get_join_restriction($permissionCheck, $key, $userLevel);
                    foreach($valuePermission['from'] as $permissionFromKey=>$permissionFromValue){
                        $fromSql .= $this->_from($permissionFromValue, $permissionFromKey);
                    }
                    if($valuePermission['where'] != "") {
                        if ($baseConditions !== "1") {
                            $baseConditions .= " AND ";
                        } else {
                            $baseConditions = "";
                        }
                        $baseConditions .= "(" . $valuePermission['where'] . ")";
                    }
                    if($valuePermission['select'] != ""){
                        $info['select'][] = $valuePermission['select'];
                    }
                }
            }
            $from .= $fromSql;
        }
        if($from == ""){
            return "";
        }
        //render select command
        $sql = $this->_select($info);
        //add from command
        $sql .= $from;
        //render where command
        $sql .= " WHERE ".$baseConditions." ";
        if(isset($info['where'])){
            $sql .= " AND (" . $this->_where($info['where']) . ") ";
        }
        //render group by command
        if(isset($info['group']) && $info['group']!=''){
            $sql .= $this->getNormalCaseString($info['group'], 'GROUP BY');
        }
        //render having
        if(isset($info['having']) && $info['having']!=''){
            $sql .= $this->getNormalCaseString($info['having'], 'HAVING');
        }
        //render order
        if(isset($info['order']) && $info['order']!=''){
            $sql .= $this->getNormalCaseString($info['order'], 'ORDER BY');
        }
        //render limit
        if(isset($info['limit']) && $info['limit'] != '') {
            $sql .= " LIMIT ";
            if(isset($info['start']) && $info['start'] != ''){
                $sql .= $info['start'] . ",";
            }
            $sql .= $info['limit'];
        }
        return $sql;
    }

    /**
     * Render select command, return sql select statement
     * @param $info
     * @return string
     */
    private function _select($info){
        /** array structure
         * array(
         *      'table_alias.field',
         *      'table_alias1' => 'field', //table_alias1.field
         *      'table_alias2' => array('field1', 'field2', ...), // table_alias2.field1, table_alias2.field2, ...
         *      'table_alias3' => array('field_alias3' => 'field3','field_alias4' => 'field4', ...); // table_alias3.field3 as field_alias3, table_alias4.field4 as field_alias4, ...
         *      ...
         * )
         */
        $ret = "SELECT ";
        if(isset($info['count']) && $info['count']){
            $ret .= " SQL_CALC_FOUND_ROWS ";
        }
        if(isset($info['select_type'])) {
            if(is_array($info['select_type'])){
                $info['select_type'] = implode(' ', $info['select_type']);
            }
            $ret .= " ". $info['select_type'] . " ";
        }
        $selectString = "";
        if(isset($info['select'])){
            if(is_string($info['select'])){
                $selectString .= $info['select'];
            }elseif(is_array($info['select'])){
                $arrSelect = array();
                foreach ($info['select'] as $key => $value) {
                    if(is_numeric($key)){
                        $arrSelect[] = $this->getNormalCaseString($value);
                    }else{
                        if(is_string($value)){
                            $arrSelect[] = "`".$key."`.".$value;
                        }elseif(is_array($value)){
                            foreach ($value as $vKey=>$vValue) {
                                $field = $vValue;
                                $fieldAlias = '';
                                if(is_string($vKey)){
                                    $fieldAlias = " as " . $vKey;
                                }
                                $arrSelect[] = "`".$key."`.`".$field."`".$fieldAlias;
                            }
                        }
                    }
                }
                $selectString .= implode(', ', $arrSelect);
            }
        }
        if($selectString != ""){
            $ret .= $selectString;
        }else{
            $ret .= "*";
        }
        return $ret;
    }

    /**
     * Render Single From command
     * @param $from table name or array has key table => table name
     * @param string $alias table alias
     * @param bool $isFirst is first table or not
     * @return string
     */
    private function _from($from, $alias = "", $isFirst = false){
        $ret = "";
        $setAlias = " " . $alias;
        if($alias == "" || $alias == $from['table']){
            $setAlias = "";
        }
        if($isFirst){
            if(is_string($from)){
                $ret = " FROM `".$from."`".$setAlias;
            }elseif(is_array($from)){
                $ret = " FROM `".$from['table']."`".$setAlias;
            }
        }else{
            $from['isQuery'] = (isset($from['isQuery']) && is_bool($from['isQuery'])) ? $from['isQuery'] : false;
            $from['type'] = (isset($from['type'])) ? " " . strtoupper($from['type']) : '';
            $ret = $from['type']." JOIN ".(($from['isQuery']) ? "(".$from['table'].")" : "`".$from['table']."`").$setAlias;
            $ret .= " ON ".$this->_where($from['condition']);
        }
        return $ret;
    }

    /**
     * Render Where Condition
     * @param $where
     * @return string
     */
    private function _where($where){
        /**
         * Array Structure
         * array(
         *  string / array(condition1, condition2,condition3,...) / others -> string / condition1 AND condition2 AND condition3 / 0
         *  'table_alias.field'    => string / number / array(1,2,3) / array('a','b','c') -> table_alias.field = 'string' / table_alias.field = number / table_alias.field IN (1,2,3) / table_alias.field IN ('a','b','c')
         * );
         */
        $ret = "1";
        if(is_string($where) && $where!=''){
            $ret = $where;
        }elseif(is_array($where) && count($where)>0){
            foreach ($where as $key => $value) {
                $simpleCondition = "";
                if(is_numeric($key)){
                    if(is_string($value)){
                        $simpleCondition .= ($value != '') ? $value : '1';
                    }elseif(is_array($value)){
                        $simpleCondition .= (sizeof($value) > 0) ? implode(" AND ",$value) : '1';
                    }else{
                        $simpleCondition .= '0';
                    }
                }else{
                    if(is_numeric($value)){
                        $simpleCondition .= $key."=".$value;
                    }elseif(is_string($value)){
                        $simpleCondition .= $key."='".$value."'";
                    }elseif(is_array($value)){
                        if(sizeof($value) > 0){
                            $first_value = array_shift(array_values($value));
                            if(is_string($first_value)){
                                $simpleCondition .= $key." IN ('".implode("','",$value)."')";
                            }elseif(is_numeric($first_value)){
                                $simpleCondition .= $key." IN ('".implode("','",$value)."')";
                            }else{
                                $simpleCondition .= '0';
                            }
                        }else{
                            $simpleCondition .= '0'; // when array is empty always return null
                        }
                    }else{
                        $simpleCondition .= '0';
                    }
                }
                if($simpleCondition != ""){
                    if($ret != "1"){
                        $ret .= " AND ";
                    }else{
                        $ret = "";
                    }
                    $ret .= $simpleCondition;
                }
            }
        }
        return $ret;
    }

    /**
     * this function is used to retrieve a set of record base the info
     * @param $info :
     * array('select' => array('{table_alias}' => array('{field_alias}' => '{field_name}',...),...),
     * 'from' => array('{table_alias}' = array('table_name' => string,'condition' => string),'type' => (LEFT|INNER|RIGHT),....),
     * 'where' => (array('field_name' => (array()|string|number))|string),...),
     * 'group by' => (string|array(),'order' => (string|array),'limit' => string, 'offset' => string))
     * @param string $db
     * @return null
     */
    public function select($info, $db=""){
        if($db === "" || $db === "db"){
            $db = $this->db;
        }
        $sql = $this->sql_creator($info);
        if($sql != ''){
            $query = $db->query($sql);
            if ($db->_error_number() > 0){
                $this->log($this->router->fetch_method(), $sql, $db);
            }else{
                return $query;
            }
        }
        return null;
    }


    /** used to retrieve number of records, the param $info is same like select() method
     * @param $info
     * @param string $db
     * @return int
     */
    public function num_rows($info, $db=""){
        $num = 0;
        if(isset($info['count']) && $info['count']){
            $sql = "SELECT FOUND_ROWS() as num";
        }else{
            $info['select'] = "COUNT(*) as num";
            $info['order'] = "";
            $info['start'] = "";
            $info['limit'] = "";
            $info['group'] = "";
            $info['having'] = "";
            $sql = $this->sql_creator($info);
        }
        $query = $this->select($sql);
        if($query !== null){
            $num = $query->result();
            return count($num)?$num[0]->num:0;
        }
        return $num;
    }

    /**
     * log the error message into log file
     * @param <type> $logType: error, warning, note
     * @param <type> $function: function name
     * @param <type> $query: sql query if any
     */
    function log($function, $query = "", $db = "") {
        $tmpMessage = ($db === "") ? $this->db->_error_message() : $db->_error_message();
        if ($query == "" || $query == null) {
            log_message("error", $this->modelName . ' - ' . $function . ":[" . $tmpMessage . "]");
        } else {
            log_message("error", $this->modelName . ' - ' . $function . ":[" . $tmpMessage . "]: SQL Syntax :[" . $query . "]");
        }
    }

    /**
     * @param $table
     * @param $data
     * @param string $db
     * @return bool|int
     */
    public function insert_database($table, $data, $db = ""){
        if($db === "" || $db === "db"){
            $db = $this->db;
        }
        $id = 0;
        if(is_array($data) && sizeof($data) > 0){
            $first_data = reset($data);
            if(isset($first_data) && (is_array($first_data) || is_object($first_data))){
                $db->insert_batch($table, $data);
            }else{
                $db->insert($table, $data);
                $id = $this->db->insert_id();
            }
        }elseif(is_object($data)){
            $db->insert($table, $data);
            $id = $this->db->insert_id();
        }
        if($db->_error_number() > 0){
            $this->log($this->modelName." ".$this->router->fetch_method(), "Insert ".$table." Data: ".json_encode($data), $db);
            return FALSE;
        }
        if($id!=0){ return $id; }
        return TRUE;
    }

    /**
     * @param $table
     * @param $condition
     * @param string $db
     * @return bool
     */
    function delete_database($table, $condition, $db=""){
        if($db === "" || $db === "db"){
            $db = $this->db;
        }
        $where = $this->_where($condition);
        if($where == '1'){
            return false;
        }
        $db->where($where, null, false);
        $db->delete($table);
        if($db->_error_number() > 0){
            $this->log($this->modelName." ".$this->router->fetch_method(), "Delete ".$table." Condition: ".json_encode($condition), $db);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @param $table
     * @param $data
     * @param $condition
     * @param string $db
     * @return bool
     */
    function update_database($table, $data, $condition, $db=""){
        if($db === ""  || $db === "db"){
            $db = $this->db;
        }
        $where = $this->_where($condition);
        if($where == '1'){
            return false;
        }
        $db->where($where, null, false);
        $db->update($table, $data);
        if($db->_error_number() > 0){
            $this->log($this->modelName." ".$this->router->fetch_method(), "Update ".$table." Data: ".json_encode($data)." Condition: ".json_encode($condition), $db);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @author: Huy Vu
     * Array structure:
     *  table       : table toto write log into
     *  condition   : condition to write log
     **/
    function write_log($info = array()){
        if(isset($info['table']) == false || isset($info['condition']) == false){
            throw new Exception('Missing parameter for write log');
        }

        $table_log = $info['table'].'_log';
        $log_routing = $this->config->item('logRoute');

        if($this->db->table_exists($table_log) == false || isset($log_routing[$info['table']]) == false)
            return true;

        $log_routing = $log_routing[$info['table']];
        $field_list = $this->db->list_fields($info['table']);
        $data = $this->select(array(
                'select'    => array(
                    $info['table']  => $field_list
                ),
                'from'      => array($info['table'] => array('table' => $info['table'])),
                'where'     => $info['condition']
            )
        )->result_array();


        if(count($data) == 0){
            throw new Exception('No master record to write log');
        }

        $data = $this->renderData($data[0], $field_list);
        $data = array_merge($data,$this->getRelationLog(array(
            'relation_tables'   => $log_routing,
            'parent_data'       => $data,
            'parent_key'        => $info['table'].'_id'
        )));
    }


    /**
     * @author: Huy Vu
     * Array structure:
     *  relation_tables       :
     *  parent_data           :
     *  parent_key            :
     **/
    function getRelationLog($info){
        $data = array();
        if(isset($info['relation_tables']) == false || count($info['relation_tables']) == 0){
            return $data;
        }
        foreach($info['relation_tables'] as $table=>$children){

            $field_list = $this->db->list_fields($table);
            $table_data = $this->select(array(
                    'select'    => array(
                        $table  => $field_list
                    ),
                    'from'      => array($table => array('table' => $table)),
                    'where'     => array($info['parent_key'] => $info['parent_data']['id'])
                )
            )->result_array();
            $data[$table.'_log'] = json_encode($table_data);
            $child_data = array();
            $data = array_merge($data, $this->getRelationLog($this->getRelationLog(array(
                        'relation_tables'   => $children,
                        'parent_data'       => $row,
                        'parent_key'        => $table.'_id'
                    ))
                )
            );

        }

        return $data;
    }

    function renderData($data, $field_list){
        $return_data = array();
        foreach($data as $key=>$value){
            if(in_array($key,$field_list)){
                $return_data[$key] = $value;
            }
        }
        return $return_data;
    }

}