<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 3/10/2015
 * Time: 11:30 AM
 */

function add_permission_to_all_places($module_id, $permission_codes = array()){
    $CI =& get_instance();
    if(is_string($permission_codes)){
        $permission_codes = array($permission_codes);
    }

    $branches = $CI->db->get('branch')->result();
    $branch_groups = $CI->db->get('branch_group')->result();

    foreach($permission_codes as $code){
        $CI->db->insert('module_permission',array('module_id' => $module_id, 'permission_code' => $code));
        $module_permission_id = $CI->db->insert_id();
        foreach($branch_groups as $branch_group){
            $CI->db->insert('module_permission_branch_group',array('branch_group_id' => $branch_group->id,'module_permission_id' => $module_permission_id));
        }
        foreach($branches as $branch){
            $CI->db->insert('module_permission_branch',array('branch_id' => $branch->id,'module_permission_id' => $module_permission_id));
        }
    }

    return true;

}