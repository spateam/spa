<?php
    // 1. Autoload the SDK Package. This will include all the files and classes to your autoloader
    require __DIR__ . '/autoload.php';

    // 2. Provide your Secret Key. Replace the given one with your app clientId, and Secret
    // https://developer.paypal.com/webapps/developer/applications/myapps
    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            'AXrr5UzlDvVY8EdNDDA_gdUsw2fKupcvISW79_9Fj5qa1HjKQFFUJi2skPqIyV8z5fIyWXFDXVMQuKW2',     // ClientID
            'EPX6Gpgl93rxPtVOzc9EKm3Ja2LOS1Bi6srIinN2j0VRUEY3HAiVi87yTtHUNxJGjCnAWHbBZxPJvaHF'      // ClientSecret
        )
    );

    // Step 2.1 : Between Step 2 and Step 3
    $apiContext->setConfig(
        array(
            'log.LogEnabled' => true,
            'log.FileName' => 'PayPal.log',
            'log.LogLevel' => 'DEBUG'
        )
    );