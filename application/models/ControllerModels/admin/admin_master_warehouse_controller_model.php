<?php

class Admin_Master_Warehouse_Controller_Model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "item";
        $this->suggestionSearch = array('code', 'name', 'description');
        $this->suggestionDisplay = array('code', 'name', 'description');
        $this->entity_status = array(Status::Active);
        $this->default_condition = array('item.type' => ITEM_TYPE('Product'));
    }

    function renderDataSet($data){
        $warehouse_quantity = $this->load->table_model('warehouse_item')->getTableMap('item_id','',array('item_id' => convert_to_array($data,'','id')),false);
        foreach($data as $row){
            $row->master_quantity = $row->product->master_quantity;
            $row->total_quantity = $row->master_quantity;
            if(isset($warehouse_quantity[$row->id])){
                foreach($warehouse_quantity[$row->id] as $each_quantity_row){
                    $row->total_quantity += $each_quantity_row->quantity;
                }
            }

        }
        return $data;
    }

    function getDataForQuantityViewForm($id){
        return $this->load->table_model('item')->getByID($id);
    }

    function update($id,$data){
        $is_exists = $this->load->table_model('item_product')->get(array('item_id'=>$id));
        if(count($is_exists)){
            $is_exists = $is_exists[0];
            $log_data = array();
            if($data['log_data']){
                $log_data = $data['log_data'];
            }
            $result = $this->load->table_model('item_product')->update($is_exists->id,$data,true,$log_data);
        }else{
            throw new Exception('The current item is not exists');
        }
        return $result;
    }

    function getDataForViewForm($id){
        $item_model = $this->load->table_model('item');

        $item = $item_model->getByID($id);
        if(!$item){
            throw new Exception('Item does not exists or managable to this account');
        }

        $warehouse_list = $this->load->table_model('warehouse')->get();
        $warehouse_ids = convert_to_array($warehouse_list,'','id');
        $warehouse_item_map = $this->load->table_model('warehouse_item')->getTableMap('warehouse_id','',array('item_id' => $id,'warehouse_id' => $warehouse_ids));
        foreach($warehouse_list as $warehouse){
            if(isset($warehouse_item_map[$warehouse->id])){
                $warehouse->quantity = $warehouse_item_map[$warehouse->id]->quantity;
            }else{
                $warehouse->quantity = 0;
            }

        }
        return array(
            'warehouse_list' => $warehouse_list,
            'item'           => $item,
            'ret_option'     => array(
                'title'     => $item->name
            ),
        );
    }

    function spread_item_to_warehouses($item_id,$data){
        $item_product_model     = $this->load->table_model('item_product');
        $warehouse_item_model   = $this->load->table_model('warehouse_item_model');

        $item_product = $this->load->table_model('item')->getByID($item_id);
        if(! isset($item_product->product)){
            throw new Exception('This item is not a product');
        }

        $warehouse_item_map = $warehouse_item_model->getTableMap('warehouse_id','',array('item_id' => $item_id));

        $will_change = array(
            'master_quantity'       => $item_product->product->master_quantity,
            'warehouse_quantity'    => array()
        );
        $this->db->trans_start();
        foreach($data['item_change'] as $warehouse_id=>$each_change){
            if(!isset($will_change['warehouse_quantity'][$warehouse_id])){
                $will_change['warehouse_quantity'][$warehouse_id] = array(
                    'quantity' => isset($warehouse_item_map[$warehouse_id])?$warehouse_item_map[$warehouse_id]->quantity:0,
                    'id'       => isset($warehouse_item_map[$warehouse_id])?$warehouse_item_map[$warehouse_id]->id:''
                );
            }
            $plus = $each_change['plus'];
            $minus = $each_change['minus'];

            if($will_change['warehouse_quantity'][$warehouse_id]['id']){
                $warehouse_item_id = $will_change['warehouse_quantity'][$warehouse_id]['id'];
                if($each_change['plus'] > 0){
                    $will_change['warehouse_quantity'][$warehouse_id]['quantity'] += $plus;
                    $will_change['master_quantity']                   -= $plus;
                    $warehouse_item_model->update_minify(array(
                        'id'        => $warehouse_item_id,
                        'field_list' => array(
                            'quantity'  => $will_change['warehouse_quantity'][$warehouse_id]['quantity'],
                        ),
                        'log_data'  => array(
                            'log_type' => 'import_pro'
                        ))
                    );
                }
                if($each_change['minus'] > 0){
                    $will_change['warehouse_quantity'][$warehouse_id]['quantity'] -= $minus;
                    $will_change['master_quantity']                   += $minus;
                    $warehouse_item_model->update_minify(array(
                        'id'        => $warehouse_item_id,
                        'field_list' => array(
                            'quantity'  => $will_change['warehouse_quantity'][$warehouse_id]['quantity'],
                        ),
                        'log_data'  => array(
                            'log_type' => 'export_pro'
                        ))
                    );
                }
            }else{
                $id = '';
                if($each_change['plus'] > 0){
                    $will_change['warehouse_quantity'][$warehouse_id]['quantity'] += $plus;
                    $will_change['master_quantity']                   -= $plus;
                    $id = $warehouse_item_model->insert_minify(array(
                        'field_list' => array(
                            'quantity'      => $will_change['warehouse_quantity'][$warehouse_id]['quantity'],
                            'warehouse_id'  => $warehouse_id,
                            'item_id'       => $item_id,
                        ),
                        'log_data'  => array(
                            'log_type' => 'import_pro'
                        )
                    ));
                }
                if($each_change['minus'] > 0){
                    $will_change['warehouse_quantity'][$warehouse_id]['quantity'] -= $minus;
                    $will_change['master_quantity']                   += $minus;
                    if($id){
                        $warehouse_item_model->update_minify(array(
                            'id'        => $warehouse_item_id,
                            'field_list' => array(
                                'quantity'  => $will_change['warehouse_quantity'][$warehouse_id]['quantity'],
                            ),
                            'log_data'  => array(
                                'log_type' => 'export_pro'
                            ))
                        );
                    }
                    else{
                        $will_change['warehouse_quantity'][$warehouse_id]['quantity'] -= $minus;
                        $will_change['master_quantity']                   += $minus;
                        $warehouse_item_model->insert_minify(array(
                            'field_list' => array(
                                'quantity'      => $will_change['warehouse_quantity'][$warehouse_id]['quantity'],
                                'warehouse_id'  => $warehouse_id,
                                'item_id'       => $item_id,
                            ),
                            'log_data'  => array(
                                'log_type' => 'export_pro'
                            )
                        ));
                    }
                }
            }
            if($will_change['warehouse_quantity'][$warehouse_id]  < 0 ){
                $warehouse = $this->load->table_model('warehouse')->getByID($warehouse_id);
                throw new Exception('There is not enough item on '.$warehouse->name.' warehouse for this transaction');
            }
            if($will_change['master_quantity'] < 0){
                throw new Exception('There is not enough item on master warehouse for this transaction');
            }
        }
        $item_product_model->update($item_product->product->id,array('master_quantity' => $will_change['master_quantity']));
        $this->db->trans_complete();

        return true;
    }

}