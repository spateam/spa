<?php

class Admin_Commission_Controller_Model extends Admin_Controller_model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "commission";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

    function getItemSuggestion($id,$keyword){
        $items = $this->select(array(
            'select' => array(
                'item' => array('item_id' => 'id','item_name' => 'name','item_type' => 'type','item_price' => 'price'),
                'commission_item' => array('value','type')
            ),
            'from' => array(
                'commission_item' => array('table' => 'commission_item'),
                'item' => array('table' => 'item', 'condition' => 'commission_item.item_id = item.id','LEFT')
            ),
            'where' => array(
                'commission_item.commission_id' => $id,
                'item.name LIKE("%'.$keyword.'%")'
            )
        ))->result();
        $suggestion_array = array();
        foreach($items as $item){
            $row = array('label' => $item->item_name, 'value' => $item->item_id, 'data' => $item);
            $suggestion_array[] = $row;
        }
        return $suggestion_array;
    }

    function getDataForViewForm($id = 0){
        return $this->load->controller_model('commission','staff')->getDataForViewForm($id);
    }

    function insert_extend($values){
        return $this->load->controller_model('commission','staff')->insert($values);
    }

    function update_extend($id, $values){
        return $this->load->controller_model('commission','staff')->update($id,$values);
    }
}