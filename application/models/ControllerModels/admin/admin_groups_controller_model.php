<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Admin_Groups_Controller_Model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "group";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

    function getGroupPermission($group_id){
        $model = $this->load->table_model('group');
        $data = new stdClass();
        $data->groupPermissions = $model->getPermissions($group_id);
        $data->allPermissions = $model->getFullPermissions();
        $data->current_group_id = $group_id;
        return $data;
    }

    function edit_permissions($id,$permissions,$remove_permissions){
        $model =  $this->load->table_model('module_permission');
        $this->db->trans_start();
        if(isset($remove_permissions) && $remove_permissions !== false && count($remove_permissions) > 0){
            $ret = $this->load->table_model('module_permission_group')->delete(array('module_permission_id' => $remove_permissions, 'group_id' => $id));
        }
        if(isset($permissions) && $permissions !== false && count($permissions) > 0){
            foreach($permissions as $permission){
                $ret = $model->insert(array('id' => $permission),1,array('group_id' => $id));
            }
        }
        $this->db->trans_complete();
        return $ret;
    }

    function get_report_permission_data($group_id){
        return $this->load->controller_model('groups','staff')->get_report_permission_data($group_id);
    }

    function edit_report_permission($id,$report_permission_list){
        return $this->load->controller_model('groups','staff')->edit_report_permission($id,$report_permission_list);
    }
}