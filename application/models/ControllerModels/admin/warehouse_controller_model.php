<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Warehouse_Controller_Model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "warehouse";
        $this->suggestionSearch = array('name','code');
        $this->suggestionDisplay = array('name');
    }

    function before_insert_to_branch_group($id,$branch_group_ids){
//        $branch_group_model = $this->load->table_model('warehouse_branch_group');
//        foreach($branch_group_ids as $branch_group_id){
//            $branch_group_model->delete(array('branch_group_id' => $branch_group_id));
//        }
    }

    function before_insert_to_branch($id,$branch_ids){
        $branch_model = $this->load->table_model('warehouse_branch');
        foreach($branch_ids as $branch_id){
            $branch_model->delete(array('branch_id' => $branch_id));
        }
    }

    function get_warehouse_item_data($warehouse_id){
        $ret = array ();
        $item_product_list = $this->load->table_model('item')->getTableMap('id','',array('type' => ITEM_TYPE('Product')));
        $warehouse_product_list = $this->load->table_model('warehouse_item')->getTableMap('item_id','',array('warehouse_id' => $warehouse_id));

        foreach($item_product_list as $item_id=>$item_product_fields){
            $ret[$item_id] = array(
                'master_quantity'   => $item_product_fields->product->master_quantity,
                'current_quantity'  => isset($warehouse_product_list[$item_id])?$warehouse_product_list[$item_id]->quantity:0,
                'item_name'         => $item_product_fields->name
            );
        }

        return $ret;
    }

    function update_warehouse_distribution($warehouse_id,$data){
        $item_product_model     = $this->load->table_model('item_product');
        $warehouse_item_model   = $this->load->table_model('warehouse_item_model');

        $item_product_list = $this->load->table_model('item')->getTableMap('id','',array('type' => ITEM_TYPE('Product')));
        $warehouse_item_list = $warehouse_product_list = $this->load->table_model('warehouse_item')->getTableMap('item_id','',array('warehouse_id' => $warehouse_id));
        $will_insert_data = array();
        $will_update_data = array();
        $will_update_master_data = array();
        $this->db->trans_start();
        foreach($data['item_change'] as $item_id=>$plus_minus){
            if($plus_minus['plus'] == 0 && $plus_minus['minus'] == 0){
                continue;
            }
            if(isset($warehouse_item_list[$item_id])){
                $quantity = $plus_minus['plus'] - $plus_minus['minus'];
                if($item_product_list[$item_id]->product->master_quantity - $quantity < 0){
                    throw new Exception('The item quantity in master warehouse can not be lower then 0');
                }
                if($warehouse_item_list[$item_id]->quantity + $quantity < 0){
                    throw new Exception('The item quantity in the warehouse can not be lower than 0');
                }
                $log_type = $quantity>0?'import_pro':($quantity<0?'export_pro':'');
                $will_update_data[$warehouse_item_list[$item_id]->id] = array('quantity' => $warehouse_item_list[$item_id]->quantity + $quantity, 'type' => $log_type);
                $will_update_master_data[$item_product_list[$item_id]->product->id] = $item_product_list[$item_id]->product->master_quantity - $quantity;
            }else{
                $quantity = $plus_minus['plus'] - $plus_minus['minus'];
                if($item_product_list[$item_id]->product->master_quantity - $quantity < 0){
                    throw new Exception('The item quantity in master warehouse can not be lower then 0');
                }
                if($quantity < 0){
                    throw new Exception('The item quantity in the warehouse can not be lower than 0');
                }
                $log_type = $quantity>0?'import_pro':($quantity<0?'export_pro':'');
                $will_insert_data[$item_id] = array('quantity' => $quantity, 'type' => $log_type);
                $will_update_master_data[$item_product_list[$item_id]->product->id] = $item_product_list[$item_id]->product->master_quantity - $quantity;
            }
        }
        /* Insert item */
        foreach($will_insert_data as $item_id => $item_fields){
            if(!$item_fields['type']){
                continue;
            }
            $warehouse_item_model->insert_minify(array(
                    'field_list' => array(
                        'item_id'   => $item_id,
                        'warehouse_id' => $warehouse_id,
                        'quantity'  => $item_fields['quantity'],
                    ),
                    'log_data'  => array(
                        'log_type' => $item_fields['type']
                    ))
            );
        }
        /* Update Item */
        foreach($will_update_data as $item_id => $item_fields){
            if(!$item_fields['type']){
                continue;
            }
            $warehouse_item_model->update_minify(array(
                    'id'         => $item_id,
                    'field_list' => array(
                        'quantity'  => $item_fields['quantity'],
                    ),
                    'log_data'  => array(
                        'log_type' => $item_fields['type']
                    ))
            );
        }
        /* Update master */
        foreach($will_update_master_data as $row_id=>$row_quantity){
            $item_product_model->update($row_id,array('master_quantity' => $row_quantity));
        }
        $this->db->trans_complete();
        return true;
    }

    function suggest_item($term, $id){
        return $this->select(array(
            'select' => array(
                'item' => array('id','label' => 'name','value' => 'id')
            ),
            'from' => array(
                'item' => array('table' => 'item')
            ),
            'where' => array(
                'type' => ITEM_TYPE('Product'),
                'item.name LIKE("%'.trim($term).'%")'
            )
        ))->result_array();
    }

}