<?php

class Items_Controller_model extends Admin_Controller_model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "item";
        $this->suggestionSearch = array('code', 'name', 'description');
        $this->suggestionDisplay = array('code', 'name', 'description');
        $this->entity_status = array(Status::Active,Status::Disable);
    }

    function getDataForViewFormExtend($data){
        if($data){
            $id = $data->item->id;
            $item_model = $this->load->table_model('item');
            if(isset($data->item) && $data->item->type == ITEM_TYPE('Package')){
                $item_ids = $this->load->table_model('package_item')->get(array('package_id' => $id));
                $package_items = array();
                foreach($item_ids as $item_id){
                    $package_items[] = $item_model->getByID($item_id->item_id,'','',array('status' => $this->entity_status));
                }
                $data->item->package = $package_items;

                $item_package = $this->load->table_model('item_package')->get(array('item_id' => $id));
                if(count($item_package)){
                    $data->item->expire_length = $item_package[0]->expire_length;
                }
            }
            $data->categories = $this->load->table_model('category')->getTableMap('id');
        }
        return $data;
    }

    function insert_extend($values){
        $id = $this->load->table_model($this->main_table)->insert($values,Permission_Value::ADMIN);
        $categories_items_model = $this->load->table_model('category_item');
        $packages_items_model = $this->load->table_model('package_item');
        $item_service_model = $this->load->table_model('item_service');
        $item_service_room_model = $this->load->table_model('item_service_room');
        $item_package_model = $this->load->table_model('item_package');
        if($values->type == ITEM_TYPE('Service')){
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $willInsert->duration = $values->duration;
            $willInsert->status = 1;
            $item_service_model->insert($willInsert);
        }else if($values->type == ITEM_TYPE('Package')){
            $willInsert = new stdClass();
            $willInsert->item_id = $id;
            $willInsert->expire_length = isset($values->expire_length)&&$values->expire_length?$values->expire_length:null;
            $willInsert->status = 1;
            $item_package_model->insert($willInsert);
        }
        $category_ids = isset($values->category_id)?$values->category_id:array();
        foreach($category_ids as $category_id){
            $category = new stdClass();
            $category->category_id = $category_id;
            $category->item_id     = $id;
            $categories_items_model->insert($category);
        }

        $item_ids = isset($values->item_kit_ids)?$values->item_kit_ids:array();
        foreach($item_ids as $item_id){
            $item_kit = new stdClass();
            $item_kit->package_id = $id;
            $item_kit->item_id = $item_id;
            $packages_items_model->insert($item_kit);
        }
        $room_ids = isset($values->room_ids)?$values->room_ids:array();
        foreach($room_ids as $room_id){
            $room_service = new stdClass();
            $room_service->room_id = $room_id;
            $room_service->item_id = $id;
            $item_service_room_model->insert($room_service);
        }
        return $id;
    }

    function update_extend($id,$values){
        $values = (object)$values;
        $this->load->table_model($this->main_table)->update($id,$values);
        $categories_items_model = $this->load->table_model('category_item');
        $packages_items_model = $this->load->table_model('package_item');
        $item_service_model = $this->load->table_model('item_service');
        $item_service_room_model = $this->load->table_model('item_service_room');
        $item_package_model = $this->load->table_model('item_package');
        if($values->type == ITEM_TYPE('Service')){
            $service = $item_service_model->get(array('item_id'=>$id));
            if(count($service)){
                $service = $service[0];
                $willInsert = new stdClass();
                $willInsert->duration = $values->duration;
                $item_service_model->update($service->id,$willInsert);
            }
            else{
                $willInsert = new stdClass();
                $willInsert->item_id = $id;
                $willInsert->duration = $values->duration;
                $willInsert->status = 1;
                $item_service_model->insert($willInsert);
            }
        }else if($values->type == ITEM_TYPE('Package')){
            $package = $item_package_model->get(array('item_id'=>$id));
            if(count($package)){
                $package = $package[0];
                $willInsert = new stdClass();
                $willInsert->expire_length =  isset($values->expire_length)&&$values->expire_length?$values->expire_length:null;
                $item_package_model->update($package->id,$willInsert);
            }
            else{
                $willInsert = new stdClass();
                $willInsert->item_id = $id;
                $willInsert->expire_length =  isset($values->expire_length)&&$values->expire_length?$values->expire_length:null;
                $willInsert->status = 1;
                $item_package_model->insert($willInsert);
            }
        }
        $categories_items_model->delete(array('item_id' => $id));
        $packages_items_model->delete(array('package_id' => $id));
        $item_service_room_model->delete(array('item_id' => $id));

        $category_ids = isset($values->category_id)?$values->category_id:array();
        foreach($category_ids as $category_id){
            $category = new stdClass();
            $category->category_id = $category_id;
            $category->item_id     = $id;
            $categories_items_model->insert($category);
        }

        $item_ids = isset($values->item_kit_ids)?$values->item_kit_ids:array();
        foreach($item_ids as $item_id){
            $item_kit = new stdClass();
            $item_kit->package_id = $id;
            $item_kit->item_id = $item_id;
            $packages_items_model->insert($item_kit);
        }
        $room_ids = isset($values->room_ids)?$values->room_ids:array();
        foreach($room_ids as $room_id){
            $room_service = new stdClass();
            $room_service->room_id = $room_id;
            $room_service->item_id = $id;
            $item_service_room_model->insert($room_service);
        }
        return true;
    }


}