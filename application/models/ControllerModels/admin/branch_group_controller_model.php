<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Branch_Group_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "branch_group";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

    function getDataForViewFormExtend($data){
        if(! isset($data->item)){
            return array('branch_group_config' => $this->load->table_model('config')->get(array(),array('user_level' => 3)));
        }
        $item = $data->item;

        if($this->user_permission->check_level(Permission_Value::ADMIN)){
            $item->employees = $this->select(array(
                'select' => array('employee' => '*'),
                'from'   => array(
                    'employee' => array('table' => 'employee'),
                    'employee_branch_group' => array('table' => 'employee_branch_group', 'condition' => 'employee_branch_group.employee_id = employee.id'),
                ),
                'where'  => array('employee.type = 3', 'employee_branch_group.branch_group_id' => $item->id)
            ))->result();
            $item->employees = convert_to_array($item->employees,'','id');
        }
        $ret = array(
            'branch_group_data'     => $item,
            'branch_group_config'   => $this->load->table_model('config')->get(array(),array('user_level' => 3, 'branch_group_id' => $item->id)),
        );
        return $ret;
    }


    function update($id, $fields){
        $branch_group_data = $fields['branch_group_data'];
        $values = $fields['value'];
        $this->db->trans_start();
        foreach($values as $config_id=>$config_value){
            $fields = new stdClass();
            $fields->value = $config_value;
            $this->load->table_model('config')->update($config_id, $fields, array('user_level' => 3,'branch_group_id' => $id));
        }
        if($branch_group_data){
            $this->load->table_model('branch_group')->update($id,$branch_group_data);
            if($this->user_permission->check_level(4)){
                if(isset($branch_group_data['employees'])){
                    foreach($branch_group_data['employees'] as $employee_id){
                        $is_exists = $this->load->table_model('employee_branch_group')->get(array('employee_id' => $employee_id, 'branch_group_id' => $id));
                        if(! count($is_exists)){
                            $this->load->table_model('employee_branch_group')->insert(array('employee_id' => $employee_id, 'branch_group_id' => $id));
                        }
                    }
                }
            }
        }
        $this->db->trans_complete();
        return true;
    }


    function renderDataSet($data){
        $temp = array();
        foreach($data as $row){
            if(! in_array($row->id,$this->user_check->get('branch_group_id'))){
                continue;
            }
            $temp[] = $row;
        }
        return $temp;
    }

    function insert($fields){
        $branch_group_data = $fields['branch_group_data'];
        $values = $fields['value'];
        $this->db->trans_start();
        $branch_id = $this->load->table_model('branch_group')->insert($branch_group_data);
        if($this->user_permission->check_level(4)){
            if(isset($branch_group_data['employees'])){
                foreach($branch_group_data['employees'] as $employee_id){
                    $is_exists = $this->load->table_model('employee_branch_group')->get(array('employee_id' => $employee_id, 'branch_group_id' => $branch_id));
                    if(! count($is_exists)){
                        $this->load->table_model('employee_branch_group')->insert(array('employee_id' => $employee_id, 'branch_group_id' => $branch_id));
                    }
                }
            }
        }
        foreach($values as $config_id=>$config_value){
            $fields = new stdClass();
            $fields->value = $config_value;
            $this->load->table_model('config')->update($config_id, $fields, array('user_level' => 3,'branch_group_id' => $branch_id));
        }

        $this->db->trans_complete();
        return $branch_id;
    }

    function get_suitable_employee(){
        $employees = $this->load->table_model('employee')->get(array('type' => 3));
        $ret = array();
        foreach($employees as $employee){
            $ret[] = array('value' => $employee->id, 'label' => to_full_name($employee->first_name,$employee->last_name));
        }
        return $ret;
    }

    function get_permission_data($branch_group_id){
        $available_permission = $this->select(array(
            'select' => array(
                'module_permission' => array('id','permission_code','global'),
                'module'     => array('module_name' => 'name'),
                'permission' => array('name')),
            'from'   => array(
                'module'                    => array('table' => 'module'),
                'module_permission'         => array('table' => 'module_permission', 'condition' => 'module.id = module_permission.module_id'),
                'permission'                => array('table' => 'permission','condition' => 'module_permission.permission_code = permission.code')
            )
        ))->result();
        $available_permission = convert_to_array($available_permission,'module_name','',false);


        $branch_group_permission = $this->select(array(
            'select' => array('module_permission' => array('id')),
            'from'   => array(
                'module_permission'                 => array('table' => 'module_permission'),
            ),
            'permission' => array(
                'branch_group_id' => $branch_group_id,
            ),
            'user_level' => Permission_Value::BRANCH_GROUP
        ))->result();
        $branch_group_permission = convert_to_array($branch_group_permission,'','id');

        return array('available_permission' => $available_permission, 'branch_group_permission' => $branch_group_permission, 'branch_group_data' => $this->load->table_model('branch_group')->getByID($branch_group_id));
    }

    function edit_permissions($branch_group_id,$permissions,$remove_permissions){
        $model =  $this->load->table_model('module_permission');
        $this->db->trans_start();
        if(isset($remove_permissions) && $remove_permissions !== false && count($remove_permissions) > 0){
            $ret = $this->load->table_model('module_permission_branch_group')->delete(array('module_permission_id' => $remove_permissions, 'branch_group_id' => $branch_group_id));
        }
        if(isset($permissions) && $permissions !== false && count($permissions) > 0){
            foreach($permissions as $permission){
                $ret = $model->insert(array('id' => $permission),Permission_Value::BRANCH_GROUP,array('branch_group_id' => $branch_group_id));
            }
        }
        $this->db->trans_complete();
        return true;
    }

}