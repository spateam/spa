<?php

class Admin_Items_Controller_model extends Admin_Controller_model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "item";
        $this->suggestionSearch = array('code', 'name', 'description');
        $this->suggestionDisplay = array('code', 'name', 'description');
        $this->entity_status = array(Status::Active,Status::Disable);
    }

    function insert_extend($values){
        return $this->load->controller_model('items','staff')->insert($values);
    }
    function update_extend($id,$values){
        return $this->load->controller_model('items','staff')->update($id,$values);
    }
    function disable_item($id){
        return $this->load->controller_model('items','staff')->disable_item($id);
    }
    function enable_item($id){
        return $this->load->controller_model('items','staff')->enable_item($id);
    }
    function renderDataSet($data){
        return $this->load->controller_model('items','staff')->renderDataSet($data);
    }

    function getItem($id){
        return $this->load->table_model('item')->getByID($id,'','',array('status'=>$this->entity_status));
    }

    function getItemSuggestion($keyword){
        $items = $this->load->table_model('item')->get(array('type' => 1));
        $suggestion_array = array();
        foreach($items as $item){
            $string = (strlen($item->code)>0?$item->code.' ':'').
                (strlen($item->name)>0?$item->name.',':'').
                (strlen($item->description)>0?$item->description:'');

            if (strpos(strtolower($string), strtolower($keyword)) !== false) {
                $row = array('label' => $string, 'value' => $item->id);
                $suggestion_array[] = $row;
            }
        }
        return $suggestion_array;
    }

    function getSuggestion($keyword, $condition = array())
    {
        $suggestion_array = array();
        if(count($this->suggestionSearch) > 0 && count($this->suggestionDisplay) > 0){
            $fields = "";
            if($keyword != ""){
                foreach($this->suggestionSearch as $field){
                    if($fields != ""){
                        $fields .= " OR ";
                    }
                    if(strpos($field, '.') === false){
                        $field = $this->main_table . "." . $field;
                    }
                    $fields .= $field . " LIKE '%{$keyword}%'";
                }
            }
            $condition[] = strlen($fields)?('('.$fields.')'):'';
//            $suggestions = $this->load->table_model($this->main_table)->get($condition);
            $suggestions = $this->select(array(
                'select' => array('item' => '*'),
                'from'   => array(
                    'item' => array('table' => 'item'),
                    'category_item' => array('table' => 'category_item', 'condition' => 'item.id = category_item.item_id', 'type' => 'left')
                ),
                'where' => $condition,
                'group' => 'item.id'
            ))->result();
            if(count($suggestions) > 0){
                foreach($suggestions as $suggestion){
                    $string = "";
                    foreach($this->suggestionDisplay as $displayField){
                        if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                            if($string != ""){
                                $string .= " ";
                            }
                            $string .= $suggestion->{$displayField};
                        }
                    }
                    $row = array('label' => $string, 'value' => $suggestion->{$this->suggestionKey});
                    $suggestion_array[] = $row;
                }
            }
        }
        $suggestion_array = array_merge($suggestion_array,$this->getSuggestionExtend());
        return $suggestion_array;
    }

    function get_service_by_category_employee($condition){
        $listservice = $this->select(array(
            'select' => array(
                'employee_service' => 'item_id'
            ),
            'from'  => array(
                'employee_service' => array('table' => 'employee_service'),
                'category_item'    => array('table' => 'category_item','condition' => 'employee_service.item_id = category_item.item_id')
            ),
            'where' => array(
                'employee_service.employee_id' => $condition['employee_id'],
                'category_item.category_id'  => $condition['category_id']
            )
        ))->result();
        return $listservice;
    }


}