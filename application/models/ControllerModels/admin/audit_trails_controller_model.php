<?php

class Audit_Trails_Controller_Model extends Admin_Controller_model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "audit_trails";
    }

    public function log($oldData,$data,$logData){
        $differ = array();
        if(!empty($oldData) && !empty($data)){
            foreach($oldData as $key => $prop){
                if(property_exists((object)$data,$key) &&
                    $prop != $data[$key]){
                    $differ[$key] = array(
                        'before' => $prop,
                        'after'  => $data[$key]
                    );
                }
            }
        }

        $log = array();
        // employee_id, createAt, action, module, object, object_detail
        $log['employee_id'] = $this->session->all_userdata()['staff']['login']->user_id;
        $log['createAt']   = gmdate('Y-m-d H:i:s',strtotime('NOW'));
        $log['action'] = (isset($logData['action']) ? $logData['action'] : '');
        $log['module'] = (isset($logData['module']) ? $logData['module'] : '');
        $log['object'] = (isset($logData['object']) ? $logData['object'] : '');

        $objectDetail = '';
        if(count($differ) > 0){
            foreach($differ as $key => $item){
                $objectDetail .= $key . '- before: ' . $item['before'].' - after: '.$item['after'].'<br>';
            }
        }
        else{
            $objectDetail = isset($logData['objectDetail']) ? $logData['objectDetail'] : '';
        }

        $log['object_detail'] = $objectDetail;

        $this->insert($log);

    }

}