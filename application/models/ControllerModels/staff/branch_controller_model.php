<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Branch_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "branch";
        $this->suggestionSearch = array('name', 'address', 'phone_number', 'email');
        $this->suggestionDisplay = array('name', 'address', 'phone_number', 'email');
        $this->minimalSuggestionDisplay = array('name');
        $modelName = $this->load->table_model($this->main_table);
        $modelName->set_is_all(true);

    }







}