<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Rooms_Controller_model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "room";
        $this->suggestionSearch = array('name','code');
        $this->suggestionDisplay = array('name');
        $this->entity_status = array(Status::Active,Status::Disable);
    }
    function getItem($id){
        return $this->load->table_model('room')->getByID($id);
    }

    function getRoom($id){
        return $this->select(array(
            'select' => array(
                'room' => array('id','code','name','bed_quantity','global','status','creator','created_date','updated_date','room_type_id')
            ),
            'from'   => array(
                'room' => array('table' => 'room')
            ),
            'where'  => array(
                'id' => $id
            )
        ))->result();
    }

    function renderDataSet($data){
        $room_type_map = $this->load->table_model('room_type')->getTableMap('id','',convert_to_array($data,'','room_type_id'));
        foreach($data as $row){
            if($row->status != Status::Disable){
                $row->is_not_disable = true;
            }
            $row->room_type_name = isset($room_type_map[$row->room_type_id])?$room_type_map[$row->room_type_id]->name:'';
        }
        return $data;
    }

    function disable_room($id){
        $item = $this->load->table_model('room')->getByID($id,'','',array('status' => $this->entity_status));
        if(!isset($item)){
            throw new Exception("Can't find the room");
        }
        $item->status = Status::Disable;
        $result = $this->load->table_model('room')->update($id,$item);
        if(!$result) {
            throw new Exception("Can't disable the room");
        }
    }

    function enable_room($id){
        $item = $this->load->table_model('room')->getByID($id,'','',array('status' => $this->entity_status));
        if(!isset($item)){
            throw new Exception("Can't find the room");
        }
        $item->status = Status::Active;
        $result = $this->load->table_model('room')->update($id,$item);
        if(!$result) {
            throw new Exception("Can't enable room");
        }
    }

}