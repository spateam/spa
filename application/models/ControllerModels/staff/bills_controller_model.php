<?php

class Bills_Controller_model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "bill";
        $this->suggestionSearch = array('bill.code', 'bill.description','bill.created_date','payment_type.name','customer.first_name','customer.last_name');
        $this->suggestionDisplay = array('code', 'description','created_date','payment_name','first_name','last_name');
    }

    function getSuggestion($keyword)
    {
        $suggestion_array = array();
        if(count($this->suggestionSearch) > 0 && count($this->suggestionDisplay) > 0){
            $fields = "";
            if($keyword != ""){
                foreach($this->suggestionSearch as $field){
                    if($fields != ""){
                        $fields .= " OR ";
                    }
                    if(strpos($field, '.') === false){
                        $field = $this->main_table . "." . $field;
                    }
                    $fields .= $field . " LIKE '%{$keyword}%'";
                }
            }
            $condition = $fields;
            $suggestions = $this->load->table_model($this->main_table)->select(array(
                'select'    => array(
                    'bill'          => array('id','code','created_date','status', 'description','amount_due'),
                    'payment_type'  => array('payment_name' => 'name'),
                    'customer'      => array('first_name','last_name'),
                    'bill_payment'  => array('amount')
                ),
                'from'      => array(
                    'bill' => array('table' => 'bill'),
                    'bill_payment' => array('table' => 'bill_payment', 'condition' => 'bill.id = bill_payment.bill_id','type' => 'left'),
                    'customer'     => array('table' => 'customer', 'condition' => 'customer.id = bill.customer_id'),
                    'payment_type' => array('table' => 'payment_type', 'condition' => 'payment_type.id = bill_payment.payment_id','type' => 'left')
                ),
                'where' => $condition
            ))->result();
            foreach($suggestions as $row){
                $row->created_date = date('Y-m-d',strtotime($row->created_date));
            }

            $suggestions = pos_render_array_no_dupplicate($suggestions,'',array('payment_name'));
            if(count($suggestions) > 0){
                foreach($suggestions as $suggestion){
                    $string = "";
                    foreach($this->suggestionDisplay as $displayField){
                        if(isset($suggestion->{$displayField}) && $suggestion->{$displayField} != "" && $suggestion->{$displayField} != null){
                            if($string != ""){
                                $string .= " ";
                            }
                            $string .= $suggestion->{$displayField};
                        }
                    }
                    $row = array('label' => $string, 'value' => $suggestion->{$this->suggestionKey});
                    $suggestion_array[] = $row;
                }
            }
        }
        return $suggestion_array;
    }

    function getDataSet($page = 1,$search = array()){
        $modelName = $this->load->table_model($this->main_table);
        $data = new stdClass();
        // get limit row number and start row index
        $limit = $this->config->item('item_per_page');
        $offset= ($page-1) * $limit;
        if(count($search) == 0){
            $start_date = get_database_date("",'',false);
            $end_date = $start_date . ' 23:59:59';
            $start_date = $start_date . ' 00:00:00';
            $search = array(
                "bill.created_date > '{$start_date}'",
                "bill.created_date < '{$end_date}'"
            );
        }
        if(isset($search['bill.status']) == false || count($search['bill.status']) == 0){
            $search['bill.status'] = $modelName->getAvailableStatus();
            $search['bill.status'][] = BILL_STATUS('Void');
        }

        $info = array(
            'select'    => array(
                'bill'          => array('id','code','created_date','status', 'description','amount_due','replace_bill_id'),
                'payment_type'  => array('payment_name' => 'name'),
                'customer'      => array('first_name','last_name'),
                'bill_payment'  => array('amount')
            ),
            'from'      => array(
                'bill' => array('table' => 'bill'),
                'bill_payment' => array('table' => 'bill_payment', 'condition' => 'bill.id = bill_payment.bill_id','type' => 'left'),
                'bill_item'    => array('table' => 'bill_item','condition' => 'bill_item.bill_id = bill.id', 'type' => 'left'),
                'bill_employee'=> array('table' => 'bill_employee', 'condition' => 'bill_employee.bill_item_id = bill_item.id', 'type' => 'left'),
                'customer'     => array('table' => 'customer', 'condition' => 'customer.id = bill.customer_id', 'type' => 'left'),
                'payment_type' => array('table' => 'payment_type', 'condition' => 'payment_type.id = bill_payment.payment_id','type' => 'left')
            ),
            'where' => $search,
            'start' => $offset,
            'limit' => $limit,
            'order' => 'bill.created_date desc, bill.code desc',
            'group' => 'bill.id',
            'count' => true
        );
        $bills = $modelName->select($info)->result();
        $data->total_rows = $modelName->num_rows($info);

        //Check if pay by payment exists
        $bill_item_map = $this->load->table_model('bill_item')->get(array('bill_id' => convert_to_array($bills,'','id')));
        $bill_item_credit_map = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bill_item_map,'','id')),false,Permission_Value::ADMIN,array());
        $bills = pos_render_array_no_dupplicate($bills,'id',array('payment_name','amount'));
        $bill_item_map = $this->load->table_model('bill_item')->getTableMap('bill_id','',array('bill_id' => convert_to_array($bills,'','id')),false);
        $replace_bill_ids = convert_to_array($bills,'','replace_bill_id');

        $replace_bills = $modelName->getTableMap('id','',array('id' => $replace_bill_ids));

        foreach($bills as $bill){
            $bill->replace_bill_code = isset($bill->replace_bill_id)?$replace_bills[$bill->replace_bill_id]->code:'';
            $bill_items = isset($bill_item_map[$bill->id]) ? $bill_item_map[$bill->id] : array();
            foreach($bill_items as $bill_item){
                if(isset($bill_item_credit_map[$bill_item->id])){
                    $bill->payment_name = strlen($bill->payment_name)?$bill->payment_name.", Credit":"Credit";
                    break;
                }
            }
        }

        //-------------------------------
        $data->dataset = array_values($bills);

        $data->dataset = $this->renderDataSet($data->dataset);
        //generate Paging information
        $data->page = new stdClass();
        $data->page->current_page = $page;
        $data->page->total_page   = ceil($data->total_rows/$limit)==0?1:ceil($data->total_rows/$limit);
        $data->page->item_per_page = $limit;
        return $data;
    }
    function renderDataSet($dataset){
        $edit_permission = $this->user_permission->checkPermission('e','bills');
        $backdate_permission = $this->user_permission->checkPermission('backdate','bills');
        foreach($dataset as $value){
            $bill_date = strtotime($value->created_date);
            $database_date = strtotime(get_database_date());

            $days_range = ($database_date - $bill_date) / (60*60*24);
            if($days_range < 7 && $value->status == BILL_STATUS('Complete') && $edit_permission){
                $value->edit_allow = true;
            }
            $value->total =  $value->amount - $value->amount_due; $value->total = isset($value->total) || $value->total != ''? $value->total : 0;
            $value->customer_name = to_full_name($value->first_name,$value->last_name);
            if($value->status == BILL_STATUS('Complete')){
                $value->is_active = true;
            }
            $value->status = BILL_STATUS($value->status);

            $current_date = new DateTime(get_database_date());
            $days_ago = $current_date->sub(date_interval_create_from_date_string($this->config->item('backdate_length')))->format('Y-m-d H:i:s');

            if($value->created_date > $days_ago && $backdate_permission){
                $value->is_in_7_days = true;
            }

            $value->created_date = get_user_date($value->created_date, '', '', true);
        }
        return $dataset;
    }

    function void($ids, $comment = ""){
        try{
            $modelName = $this->load->table_model($this->main_table);
            $credit_model = $this->load->table_model('credit');
            $this->db->trans_start();
            $result = 1;
            $rows = $modelName->select(array(
                'select' => array(
                    'bill'              => array('id','code','customer_id','created_date','status'),
                    'bill_item'         => array('item_id','credit_add'),
                    'bill_item_credit'  => array('credit_id','credit_value')
                ),
                'from'   => array(
                    'bill'              => array('table' => 'bill'),
                    'bill_item'         => array('table' => 'bill_item', 'condition' => 'bill_item.bill_id = bill.id'),
                    'bill_item_credit'  => array('table' => 'bill_item_credit','condition' => 'bill_item_credit.bill_item_id = bill_item.id', 'type' => 'left')
                ),
                'where' => array('bill.id' => $ids)
            ))->result();
            $item_ids = convert_to_array($rows,'','item_id');
            $credit_ids = convert_to_array($rows,'','credit_id');
            $item_package_map = $this->load->table_model('item_package')->getTableMap('item_id','',array('item_id' => $item_ids));
            $where = "item_id IN ('".implode("','",$item_ids)."')" . " OR credit.id IN ('".implode("','",$credit_ids)."')" ;
            $credits = $modelName->select(array(
                    'from' => 'credit',
                    'where'=> $where,
                    'user_level' => Permission_Value::ADMIN
                )
            )->result();
            $credits_raw = convert_to_array($credits,'id','',true);

            $item_ids = convert_to_array($credits,'','item_id');
            $items = $modelName->select(array(
                'from' => 'item',
                'where'=> array('item.id' => $item_ids)
            ))->result();
            $items = convert_to_array($items,'id');

            if(count($credits)){
                $credits = convert_to_array($credits,'customer_id','',false);
            }

            foreach($credits as $key=>$credit){
                $credits[$key] = convert_to_array($credit,'item_id');
            }

            $rows = convert_to_array($rows,'id','',false);
            foreach($rows as $bill_id=>$bill_value){
                if($bill_value[0]->status == BILL_STATUS('Void')){
                    return "This bill was already voided (Sale ID: {$bill_value[0]->code})";
                }
                foreach($bill_value as $value){
                    //Check if the credit is used
                    if(isset($credits[$value->customer_id]) == false)
                        continue;
                    if(isset($credits[$value->customer_id][$value->item_id])){
                        $credit_check = $credits[$value->customer_id][$value->item_id];
                    }else{
                        continue;
                    }

                    $check = $modelName->select(array(
                        'select' => array(
                            'bill'          => array('id','code')
                        ),
                        'from'   => array(
                            'bill' => array('table' => 'bill'),
                            'bill_item'    => array('table'=>'bill_item', 'condition' => 'bill_item.bill_id = bill.id'),
                            'bill_item_credit' => array('table' => 'bill_item_credit', 'condition' => 'bill_item_credit.bill_item_id = bill_item.id'),
                        ),
                        'where' => array("bill.created_date > '{$value->created_date}'", 'status = 1', 'bill_item_credit.credit_id' => $credit_check->id),
                        'group' => 'bill.id'
                    ))->result();
                    if(count($check)){
                        $return = $this->error_message_creator(array(
                            'bill_void' => $value->code,
                            'item_name' => $items[$value->item_id]->name,
                            'bill_used' => $check
                        ));
                        throw new Exception($return);
                    }

                }
                //If pass change bill status
                date_default_timezone_set("UTC");
                $this->db->update('bill',array('status' => BILL_STATUS('Void'), 'void_date' => get_database_date(),'description' => $comment),array('id' => $bill_id));

                $change_credit = array();
                foreach($bill_value as $value){
                    //Refund customer credit
                    if($value->credit_id){
                        $refund_credit = $credits_raw[$value->credit_id];
                        $refund_credit->credit += $value->credit_value;
                        $credits_raw[$value->credit_id] = $refund_credit;
                        if(in_array($value->credit_id,$change_credit) == false)
                            $change_credit[] = $value->credit_id;
                    }
                    //Retrieve customer credit back
                    if(isset($items[$value->item_id]) && ITEM_TYPE($items[$value->item_id]->type) == 'Package'){
                        foreach($credits_raw as $credit_id=>$credit){
                            if($credit->customer_id == $value->customer_id && $credit->item_id == $value->item_id){
                                $credit->credit -= $value->credit_add;
                                $credit->credit_original -= $value->credit_add;
                                if(in_array($credit_id,$change_credit) == false)
                                    $change_credit[] = $credit_id;
                            }
                        }
                    }
                }
                foreach($change_credit as $credit_id){
                    //Check if it is new create or not
                    $case = 1;
                    $bill_before = $modelName->select(array(
                        'select' => array('bill' => array('created_date')),
                        'from'   => array(
                            'bill' => array('table' => 'bill'),
                            'bill_item' => array('table' => 'bill_item', 'condition' => 'bill_item.bill_id = bill.id'),
                            'credit'    => array('table' => 'credit', 'condition' => 'credit.item_id = bill_item.item_id')
                        ),
                        'where'  => array('credit.id' => $credit_id, "bill.created_date < '{$value->created_date}'", 'bill.status = 1'),
                        'order'  => 'created_date desc'
                    ));
                    $bill_after = $modelName->select(array(
                        'select' => array('bill' => array('created_date')),
                        'from'   => array(
                            'bill' => array('table' => 'bill'),
                            'bill_item' => array('table' => 'bill_item', 'condition' => 'bill_item.bill_id = bill.id'),
                            'credit'    => array('table' => 'credit', 'condition' => 'credit.item_id = bill_item.item_id')
                        ),
                        'where'  => array('credit.id' => $credit_id, "bill.created_date > '{$value->created_date}'", 'bill.status = 1'),
                        'order'  => 'created_date asc'
                    ));
                    if($bill_before->num_rows == 0)
                        $case = 1;
                    else
                        $case = 2;

                    if($bill_after->num_rows == 0){
                        if($case == 1)
                            $case = 0;
                        if($case == 2)
                            $case = 3;
                    }
                    switch($case){
                        case 0:
                            $end_date = new DateTime($credits_raw[$credit_id]->start_date);
                            if(isset($item_package_map[$credits_raw[$credit_id]->item_id]) && $item_package_map[$credits_raw[$credit_id]->item_id]->expire_length){
                                $expire_length = $item_package_map[$credits_raw[$credit_id]->item_id]->expire_length;
                            }else {
                                $expire_length = $this->system_config->get('expire');
                            }
                            $end_date = date_add($end_date,date_interval_create_from_date_string("$expire_length months"))->format(DATABASE_DATE_FORMAT . ' ' .DATABASE_TIME_FORMAT);
                            $credit_model->update($credit_id,
                                array(
                                    'credit_original' => $credits_raw[$credit_id]->credit_original,
                                    'credit'          => $credits_raw[$credit_id]->credit,
                                    'end_date'        => $end_date,
                                ),false
                            );
                            break;
                        case 1:
                            $bill_after = $bill_after->result()[0];
                            $credit_model->update($credit_id,
                                array(
                                    'credit_original' => $credits_raw[$credit_id]->credit_original,
                                    'credit'          => $credits_raw[$credit_id]->credit,
                                    'start_date'      => $bill_after->created_date
                                ),false
                            );
                            break;
                        case 2:
                            $credit_model->update($credit_id,
                                array(
                                    'credit_original' => $credits_raw[$credit_id]->credit_original,
                                    'credit'          => $credits_raw[$credit_id]->credit),false
                            );
                            break;
                        case 3:
                            $bill_before = $bill_before->result()[0];
                            if(isset($item_package_map[$credits_raw[$credit_id]->item_id]) && $item_package_map[$credits_raw[$credit_id]->item_id]->expire_length){
                                $expire_length = $item_package_map[$credits_raw[$credit_id]->item_id]->expire_length;
                            }else {
                                $expire_length = $this->system_config->get('expire');
                            }

                            $end_date = new DateTime($bill_before->created_date);
                            $add_months = new DateInterval("P{$expire_length}M");
                            $end_date->add($add_months);
                            $credit_model->update($credit_id,
                                array(
                                    'credit_original' => $credits_raw[$credit_id]->credit_original,
                                    'credit'          => $credits_raw[$credit_id]->credit,
                                    'end_date'        => $end_date->format('Y-m-d H:i:s')
                                )
                            );
                            break;
                    }
                }
            }
            //Add the item to stock when completely void
            $bill_items_product = $this->select(array(
                'select' => array('bill_item' => '*','item' => array('item_name' => 'name')),
                'from' => array(
                    'bill'      => array('table' => 'bill'),
                    'bill_item' => array('table' => 'bill_item','condition' => 'bill_item.bill_id = bill.id'),
                    'item'      => array('table' => 'item', 'condition' => 'bill_item.item_id = item.id'),
                ),
                'where' => array('item.type' => ITEM_TYPE('Product'),'bill.id' => $ids)
            ))->result();

            if(count($bill_items_product)){
                $warehouse_management_model = $this->load->controller_model('warehouse_management');
                $warehouse_items = $this->load->table_model('warehouse_item')->get_warehouse_quantity(null,convert_to_array($bill_items_product,'','item_id'),false);

                foreach($bill_items_product as $product){
                    if(!isset($warehouse_items[$product->item_id])){
                        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse();
                        if(!$warehouse_id){
                            if(isset($product->warehouse_id)){
                                $warehouse_item = $this->load->table_model('warehouse_item')->get_warehouse_quantity($product->warehouse_id,$product->item_id,false);
                                $qty =  $product->quantity;
                                if($warehouse_item){
                                    $qty += $warehouse_item->qty;
                                }
                                $warehouse_management_model->insert(array(
                                    'warehouse_id' => $product->warehouse_id,
                                    'quantity'     => $qty,
                                    'item_id'      => $product->item_id,
                                ));
                            }else{
                                throw new Exception("Do not have any warehouse to contain this item: ",$product->item_name);
                            }

                        }else{
                            $warehouse_management_model->insert(array(
                                'warehouse_id' => $warehouse_id,
                                'quantity'     => $product->quantity,
                                'item_id'      => $product->item_id,
                            ));
                        }
                    }else{
                        $warehouse_item = $warehouse_items[$product->item_id];
                        $warehouse_id   = $warehouse_item->warehouse_id;
                        $qty            = $warehouse_item->quantity + $product->quantity;
                        $warehouse_items[$product->item_id]->quantity += $product->quantity;
                        $warehouse_management_model->insert(array(
                            'warehouse_id' => $warehouse_id,
                            'quantity'     => $qty,
                            'item_id'      => $product->item_id,
                        ));
                    }

                }

            }

            $this->db->trans_complete();
            return $result;
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    function error_message_creator($info = array()){
        $message = "Can not void bill: <b>{$info['bill_void']}</b> because the item <b>\"{$info['item_name']}\"</b> was used in these bills: </br>";
        foreach($info['bill_used'] as $bill){
            $message .= "Bill ID : <b>{$bill->code}</b> </br>";
        }
        return $message;
    }

    function getDataForViewFormExtend($data){
        $bill = $data->item;
        $limited_date = new DateTime($bill->created_date);
        $limited_date = date_sub($limited_date,date_interval_create_from_date_string($this->config->item('backdate_length')))->format(DATABASE_DATE_FORMAT . ' ' .DATABASE_TIME_FORMAT);
        $condition = array(
            "created_date < '$bill->created_date'",
            "created_date > '$limited_date'",
            'status' => BILL_STATUS('Void'),
            'replace_bill_id is null',
        );

        $void_bills = $this->load->table_model('bill')->getTableMap('id','',$condition);
        $employees  = $this->load->table_model('employee')->getTableMap('id','',array('id' => convert_to_array($void_bills,'','creator')),true,4);

        /*-- Check if the bill was used to replace or not ---*/
        $replaced = $this->load->table_model('bill')->get(array('replace_bill_id' => $bill->id));
        if(count($replaced)){
            $bill->replaced = count($replaced);
        }
        $data->void_bills = $void_bills;
        $data->employees = $employees;
        $data->item = $bill;
        return $data;
    }

    function update($id, $values){
        try{
            $bill_model = $this->load->table_model('bill');
            $credit_model = $this->load->table_model('credit');

            $this->db->trans_begin();
            $replacing_bill_id = $id;
            $replaced_bill_id = $values['replace_bill_id'];

            $replaced_bill  = $bill_model->getByID($replaced_bill_id);
            $replacing_bill = $bill_model->getByID($replacing_bill_id);
            $replacing_bill_items = $this->load->table_model('bill_item')->get(array('id' => $replacing_bill->id));
            $replacing_bill_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($replacing_bill_items, '','id')),false);

            if(isset($replacing_bill->customer_id)){
                $newer_bills = $bill_model->getTableMap('id','',array(
                    "created_date >  '{$replaced_bill->created_date}'",
                    "bill.id != {$replacing_bill_id}",
                    "status" => BILL_STATUS('Complete'),
                    "customer_id" => $replacing_bill->customer_id
                ),true,'',array(),'created_date desc');

                $bills_credit_data = array();
                /*---- Get customer credit ----*/
                $customer_ids = array();
                if(isset($replaced_bill->customer_id)) $customer_ids[] = $replaced_bill->customer_id;
                if(isset($replacing_bill->customer_id)) $customer_ids[] = $replacing_bill->customer_id;
                $c_credits = $credit_model->getTableMap('id','',array(
                    "customer_id" => $customer_ids
                ),true,4);
                /*-------------------------------*/

                /*---- Get bill arrange by credit ----*/
                $newer_bills_items = $this->load->table_model('bill_item')->getTableMap('bill_id','',array(
                    'bill_id' => convert_to_array($newer_bills,'','id'),
                ),false);

                $newer_bill_item_ids = array();
                foreach($newer_bills_items as $bill_id=>$newer_bill_items){
                    $credit_add = array();
                    foreach($newer_bill_items as $row){
                        $newer_bill_item_ids[] = $row->id;
                        if($row->credit_add){
                            foreach($c_credits as $credit_id=>$credit_field){
                                if($credit_field->customer_id == $newer_bills[$bill_id]->customer_id && $credit_field->item_id == $row->item_id){
                                    $credit_add[$credit_id] = $row->credit_add;
                                }
                            }
                        }
                    }
                    $bills_credit_data[$bill_id]['credit_add'] = $credit_add;
                }

                $newer_bills_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => $newer_bill_item_ids),false);

                foreach($newer_bills_items as $bill_id=>$newer_bill_items){
                    $credit_used = array();
                    foreach($newer_bill_items as $row){
                        $bill_item_credits = isset($newer_bills_items_credits[$row->id])?$newer_bills_items_credits[$row->id]:array();
                        foreach($bill_item_credits as $bill_item_credit){
                            $credit_used[$bill_item_credit->credit_id] = $bill_item_credit->credit_value;
                        }
                    }
                    $bills_credit_data[$bill_id]['credit_used'] = $credit_used;
                }
                krsort($bills_credit_data);
                /*----------------------------------------*/

                /*------ Go down bills ------*/
                foreach($bills_credit_data as $bill_id=>$bill_credit_fields){
                    $credit_add = $bill_credit_fields['credit_add'];
                    $credit_used = $bill_credit_fields['credit_used'];
                    /*--- Add credit used ---*/
                    foreach($credit_used as $credit_id=>$credit_value){
                        $c_credits[$credit_id]->credit += $credit_value;
                    }
                    /*--- Minus credit add ---*/
                    foreach($credit_add as $credit_id=>$credit_value){
                        $c_credits[$credit_id]->credit -= $credit_value;
                        if($c_credits[$credit_id]->credit < 0){
                            throw new Exception('Unable to replace this bill. Transaction fail because there is not enough credit <b>before</b> bill: '.$newer_bills[$bill_id]->code);
                        }
                    }
                }
                /*----------------------------*/
            }

            $bill_replaced_before = $bill_model->get(array('replace_bill_id' => $replacing_bill->id, 'status' => BILL_STATUS('Void')));
            if(count($bill_replaced_before)){
                foreach($bill_replaced_before as $row){
                    $bill_model->update($row->id, array('replace_bill_id' => null), array('replace_bill_date' => null));
                }
            }
            $bill_model->update($replacing_bill->id,array('created_date'    => $replaced_bill->created_date,'raw_created_date' => $replacing_bill->created_date));
            $bill_model->update($replaced_bill->id,array('replace_bill_id'  => $replacing_bill->id,'replace_bill_date' => get_database_date()));
            if($this->db->trans_status()){
                $this->db->trans_commit();
            }else{
                throw new Exception('Can not update the bill');
            }
        }catch(Exception $e) {
            $this->db->trans_rollback();
            return $e->getMessage();
        }

        return true;
    }

    function getDataForReportForm(){
        $reports = array(
            'item_summary_report' => array('text' => 'Item Summary'),
        );
        return array('reports'=>$reports);
    }

    function getDataForBackDate($bill_id){
        $bill_data = $this->load->table_model('bill')->getByID($bill_id);
        if($bill_data){
            $current_date = new DateTime(get_database_date());
            $bill_data->max_date = get_user_date($bill_data->created_date,'','Y-m-d H:i:s');
            $days_ago = $current_date->sub(date_interval_create_from_date_string($this->config->item('backdate_length')))->format('Y-m-d H:i:s');
            $bill_data->min_date = $days_ago;
        }
        return array('bill' => $bill_data);
    }

    function backdate_bill($bill_id,$date_back_dated){
        $this->db->trans_start();
        $date_back_dated = get_database_date($date_back_dated);
        $bill_model = $this->load->table_model('bill');
        $bill_data = $bill_model->getByID($bill_id);
        $bill_item_data = $this->load->table_model('bill_item')->getTableMap('id','',array(
            'bill_id' => $bill_data->id,
        ));
        $bill_item_credit_data =  $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' =>convert_to_array($bill_item_data,'','id')),false);
        $bill_credit_used_data = array();
        foreach($bill_item_credit_data as $bill_item_id=>$rows){
            foreach($rows as $row){
                if(isset($bill_credit_used_data[$row->credit_id])){
                    $bill_credit_used_data[$row->credit_id] += $row->credit_value;
                }else{
                    $bill_credit_used_data[$row->credit_id] = $row->credit_value;
                }
            }


        }
        if(!$bill_data){
            throw new Exception('The bill is no longer exists');
        }
        $temp_date = new DateTime(get_database_date());
        $current_date = $temp_date->format('Y-m-d H:i:s');
        $days_ago = $temp_date->sub(date_interval_create_from_date_string($this->config->item('backdate_length')))->format('Y-m-d H:i:s');
        if($date_back_dated < $days_ago ){
            throw new Exception('The date you backdated is more than 7 days');
        }
        if($date_back_dated > $current_date){
            throw new Exception('The date you backdated is later than current time');
        }

        if($bill_data->customer_id){
            $newer_bills = $bill_model->getTableMap('id','',array(
                "created_date >  '{$date_back_dated}'",
                "status" => BILL_STATUS('Complete'),
                "bill.id != {$bill_id}",
                "customer_id" => $bill_data->customer_id
            ),true,'',array(),'created_date desc');

            $bills_credit_data = array();
            /*---- Get customer credit ----*/
            $c_credits = $this->load->table_model('credit')->getTableMap('id','',array(
                "customer_id" => $bill_data->customer_id
            ),true,Permission_Value::ADMIN);
            /*-------------------------------*/

            /*---- Get bill arrange by credit ----*/
            $newer_bills_items = $this->load->table_model('bill_item')->getTableMap('bill_id','',array(
                'bill_id' => convert_to_array($newer_bills,'','id'),
            ),false);

            $credit_adjustment_log = $this->load->table_model('credit_adjustment_log')->get(array('credit_id' => convert_to_array($c_credits,'','id')),'log_time desc');

            $newer_bill_item_ids = array();
            foreach($newer_bills_items as $bill_id=>$newer_bill_items){
                $credit_add = array();
                foreach($newer_bill_items as $row){
                    $newer_bill_item_ids[] = $row->id;
                    if($row->credit_add){
                        foreach($c_credits as $credit_id=>$credit_field){
                            if($credit_field->customer_id == $newer_bills[$bill_id]->customer_id && $credit_field->item_id == $row->item_id){
                                $credit_add[$credit_id] = $row->credit_add;
                            }
                        }
                    }
                }
                $bills_credit_data[$bill_id]['credit_add'] = $credit_add;
            }

            $newer_bills_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => $newer_bill_item_ids),false);

            foreach($newer_bills_items as $bill_id=>$newer_bill_items){
                $credit_used = array();
                foreach($newer_bill_items as $row){
                    $bill_item_credits = isset($newer_bills_items_credits[$row->id])?$newer_bills_items_credits[$row->id]:array();
                    foreach($bill_item_credits as $bill_item_credit){
                        $credit_used[$bill_item_credit->credit_id] = $bill_item_credit->credit_value;
                    }
                }
                $info = array(
                    'bill_id' => $bill_id,
                    'credit_used' => $credit_used,
                    'created_date' => $newer_bills[$bill_id]->created_date
                );
                if(isset($bills_credit_data[$bill_id])){
                    $bills_credit_data[$bill_id] = array_merge($bills_credit_data[$bill_id],$info);
                }else{
                    $bills_credit_data[$bill_id] = $info;
                }

            }
            usort($bills_credit_data,function($a,$b){
                if($a['created_date']==$b['created_date']) return 0;
                return $a['created_date'] < $b['created_date']?1:-1;
            });
            $bills_credit_data = array_sort_asc($bills_credit_data,$credit_adjustment_log,'created_date','log_time','desc','array');

            /*----------------------------------------*/

            /*------ Go down bills ------*/
            foreach($bills_credit_data as $bill_credit_fields){
                if(isset($bill_credit_fields['bill_id'])){
                    $bill_id = $bill_credit_fields['bill_id'];
                    $credit_add = $bill_credit_fields['credit_add'];
                    $credit_used = $bill_credit_fields['credit_used'];
                    /*--- Add credit used ---*/
                    foreach($credit_used as $credit_id=>$credit_value){
                        $c_credits[$credit_id]->credit += $credit_value;
                    }
                    /*--- Minus credit add ---*/
                    foreach($credit_add as $credit_id=>$credit_value){
                        $c_credits[$credit_id]->credit -= $credit_value;
                        if($c_credits[$credit_id]->credit < 0){
                            throw new Exception('Unable to backdate this bill. Transaction fail because there is not enough credit <b>before</b> bill: '.$newer_bills[$bill_id]->code);
                        }
                    }
                }else{
                    $credit_id = $bill_credit_fields['credit_id'];
                    $c_credits[$credit_id]->credit += -$bill_credit_fields['credit_change'];

//                    if($c_credits[$credit_id]->credit < 0){
//                        if(isset($bill_credit_used_data[$credit_id]) && ($c_credits[$credit_id]->credit + $bill_credit_used_data[$credit_id] >= 0)){
//                            continue;
//                        }else{
//                            throw new Exception('Unable to backdate this bill. Transaction fail because there is not enough credit for credit adjustment at '.get_user_date($bill_credit_fields['log_time']));//.get_user_date($bill_credit_fields['log_time'],'',true));
//                        }
//                    }
                }
            }
        }

        if($bill_data->raw_created_date){
            $bill_model->update($bill_data->id,array('created_date' => $date_back_dated,'raw_created_date' => $bill_data->raw_created_date),false,array('log_type' => 'backdate'));
        }else{
            $bill_model->update($bill_data->id,array('created_date' => $date_back_dated,'raw_created_date' => $bill_data->created_date),false,array('log_type' => 'backdate'));
        }

        $this->db->trans_complete();

        return true;
    }
}

