<?php

class Categories_Controller_Model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "category";
        $this->suggestionSearch = array('code', 'name', 'description');
        $this->suggestionDisplay = array('code', 'name');
    }

    function getByID($id){
        return $this->load->table_model('category')->getByID($id);
    }

    function renderDataSet($data){
        foreach($data as $each_category){
            $each_category->room_type_string = implode(', ',convert_to_array($each_category->room_types,'','name'));
        }
        return $data;
    }

    function update($id, $values,$constraint_check = true){
        $values = (array)$values;
        $this->db->trans_start();
        parent::update($id,$values,$constraint_check);
        $category_item_model = $this->load->table_model('category_item_model');
        $current_item_id = $category_item_model->getTableMap('','item_id',array('category_id' => $id));
        $category_item_model->delete(array('category_id' => $id,'item_id' => $current_item_id));
        if(isset($values['item_id_list'])){
            if(is_string($values['item_id_list'])){
                $values['item_id_list'] = array($values['item_id_list']);
            }
            foreach($values['item_id_list'] as $item_id){
                $category_item_model->insert(array('item_id' => $item_id, 'category_id' => $id));
            }
        }
        $this->update_booking_category_data($id, $values);
        $this->db->trans_complete();
        return true;
    }

    function insert($values,$user_level = '', $permission = array(),$force_insert = false){
        $values = (array)$values;
        $this->db->trans_start();
        $id = parent::insert($values);
        $category_item_model = $this->load->table_model('category_item_model');
        if(isset($values['item_id_list'])){
            if(is_string($values['item_id_list'])){
                $values['item_id_list'] = array($values['item_id_list']);
            }
            foreach($values['item_id_list'] as $item_id){
                $category_item_model->insert(array('item_id' => $item_id, 'category_id' => $id));
            }
        }
        $this->insert_booking_category_data($id,$values);
        $this->db->trans_complete();
        return true;
    }

    function insert_booking_category_data($id,$values){
        $booking_category_model = $this->load->table_model('category_booking');
        $current_system = $this->config->item('current_system');
        $branch_list = $this->select(array(
            'select' => array('branch' => '*'),
            'from'   => array(
                'branch' => array('table' => 'branch')
            ),
            'where' => array('status' => 1)
        ))->result();

        $show_in_ui = array();
        if(isset($values["show_in_ui"])){
            foreach($values["show_in_ui"] as $value)
                array_push($show_in_ui, $value);
        }
        $show_price_in_ui = array();
        if(isset($values["show_price_in_ui"])){
            foreach($values["show_price_in_ui"] as $value)
                array_push($show_price_in_ui, $value);
        }

        $booking_category = new stdClass();
        foreach($branch_list as $branch){
            $cat = $this->select(array(
                        'select' => array('category_booking' => '*'),
                        'from'   => array(
                            'category_booking' => array('table' => 'category_booking')
                        ),
                        'where' => array('branch_id' => $branch->id,'category_id' => $id)
                    ))->result();
            if(!count($cat)){
                $booking_category->branch_id    = $branch->id;
                $booking_category->category_id  = $id;
                if($current_system == "admin"){
                    $booking_category->is_show = 1;
                    $booking_category->is_price_show = 1;
                }
                else{
                    if(in_array($branch->id, $show_in_ui))
                        $booking_category->is_show = 1;
                    else
                        $booking_category->is_show = 0;

                    if(in_array($branch->id, $show_price_in_ui))
                        $booking_category->is_price_show = 1;
                    else
                        $booking_category->is_price_show = 0;
                }
                $booking_category_model->insert($booking_category);
            }
        }
    }

    function update_booking_category_data($id,$values){
        $current_system = $this->config->item('current_system');
        if($current_system == "staff"){
            $booking_items_model = $this->load->table_model('category_booking');
            $current_system = $this->config->item('current_system');
            $booking_items_model->delete(array('category_id' => $id));
            $branch_list = $this->select(array(
                'select' => array('branch' => '*'),
                'from'   => array(
                    'branch' => array('table' => 'branch')
                ),
                'where' => array('status' => 1)
            ))->result();

            $show_in_ui = array();
            if(isset($values["show_in_ui"])){
                foreach($values["show_in_ui"] as $value){
                    array_push($show_in_ui, $value);
                }
            }
            $show_price_in_ui = array();
            if(isset($values["show_price_in_ui"])){
                foreach($values["show_price_in_ui"] as $value)
                    array_push($show_price_in_ui, $value);
            }

            $booking_category = new stdClass();
            foreach($branch_list as $branch){
                $cat = $this->select(array(
                            'select' => array('category_booking' => '*'),
                            'from'   => array(
                                'category_booking' => array('table' => 'category_booking')
                            ),
                            'where' => array('branch_id' => $branch->id,'category_id' => $id)
                        ))->result();
                if(!count($cat)){
                    $booking_category->branch_id   = $branch->id;
                    $booking_category->category_id     = $id;

                    if(in_array($branch->id, $show_in_ui))
                        $booking_category->is_show = 1;
                    else
                        $booking_category->is_show = 0;

                    if(in_array($branch->id, $show_price_in_ui))
                        $booking_category->is_price_show = 1;
                    else
                        $booking_category->is_price_show = 0;

                    $booking_items_model->insert($booking_category);
                }
            }
        }
    }

    function getBookingBranch($id,$getall=false,$is_price=false){
        if($getall){
            $branch_list = $this->select(array(
                'select' => array('branch' => '*'),
                'from'   => array(
                    'branch' => array('table' => 'branch')
                ),
                'where' => array('status' => 1)
            ))->result();
            $show_in_ui = array();
            foreach($branch_list as $branch){
                $temp = new stdClass();
                $temp->id = $branch->id;
                array_push($show_in_ui, $temp);
            }
            return $show_in_ui;
        }

        $category_booking_branch = $this->select(array(
                            'select' => array('category_booking' => '*'),
                            'from'   => array(
                                'category_booking' => array('table' => 'category_booking')
                            ),
                            'where' => array('category_id' => $id)
                        ))->result();
        $show_in_ui = array();
        foreach($category_booking_branch as $value){
            if($is_price){
                if($value->is_price_show){
                    $temp = new stdClass();
                    $temp->id = $value->branch_id;
                    array_push($show_in_ui, $temp);
                }
            }
            else{
                if($value->is_show){
                    $temp = new stdClass();
                    $temp->id = $value->branch_id;
                    array_push($show_in_ui, $temp);
                }
            }
        }
        return $show_in_ui;
    }
}