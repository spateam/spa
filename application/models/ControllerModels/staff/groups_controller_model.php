<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Groups_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "group";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

    function getGroupPermission($group_id){
        $model = $this->load->table_model('group');
        $data = new stdClass();
        $data->groupPermissions = $model->getPermissions($group_id);
        $data->allPermissions = $model->getFullDetailUserPermissions();
        $data->current_group_id = $group_id;
        return $data;
    }

    function edit_permissions($id,$permissions,$remove_permissions){
        $model =  $this->load->table_model('module_permission');
        $this->db->trans_start();
        if(isset($remove_permissions) && $remove_permissions !== false && count($remove_permissions) > 0){
            $ret = $this->load->table_model('module_permission_group')->delete(array('module_permission_id' => $remove_permissions, 'group_id' => $id));
        }
        if(isset($permissions) && $permissions !== false && count($permissions) > 0){
            foreach($permissions as $permission){
                $ret = $model->insert(array('id' => $permission),1,array('group_id' => $id));
            }
        }
        $this->db->trans_complete();
        return $ret;
    }

    function get_report_permission_data($group_id){
        $group = $this->load->table_model($this->main_table)->getByID($group_id);
        $group_report_permission = convert_to_array($group->report_list,'','id');
        $all_report_permission = REPORT();
        return array(
            'group_report_permission' => $group_report_permission,
            'all_report_permission'   => $all_report_permission,
            'group_id'                => $group_id
        );
    }

    function edit_report_permission($id,$report_permission_list){
        $this->db->trans_start();
        $model = $this->load->table_model('report_group');
        $model->delete(array('group_id' => $id));
        if(isset($report_permission_list) && $report_permission_list !== false && count($report_permission_list) > 0){
            foreach($report_permission_list as $report_permission_id){
                $ret = $model->insert(array('report_id' => $report_permission_id,'group_id' => $id));
            }
        }
        $this->db->trans_complete();
        return $ret;
    }
}