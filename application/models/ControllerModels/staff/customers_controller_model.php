<?php

class Customers_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "customer";
        $this->suggestionSearch = array('code','mobile_number','nric','first_name','last_name','email');
        $this->suggestionDisplay = array('first_name', 'last_name', 'code','mobile_number');
        $this->noPermissionDisplay = array('first_name', 'last_name', 'code');
        $this->suggestionOrder = 'id desc ,first_name asc, last_name asc';
        $this->minimalSuggestionDisplay = array('first_name', 'last_name');
        $this->extendNoPermissionDisplay = array('no_permission_display');
        $this->custom_display = array('customer_type' => array(CUSTOMER_TYPE('Guest') => 'Guest', CUSTOMER_TYPE('Member') => 'Member'));
    }

    function no_permission_display($id){
        $branch_groups =  $this->load->table_model('customer_branch_group')->select(
            array(
                'select' => array('branch_group' => array('name')),
                'from'  => array(
                    'customer_branch_group' => array('table' => 'customer_branch_group','condition' => 'customer.id = customer_branch_group.customer_id'),
                    'branch_group' => array('table' => 'branch_group','condition' => 'customer_branch_group.branch_group_id = branch_group.id')
                ),
                'where' => array('customer_id' => $id))
        )->result();
        $str = 'This customer only belong to these branch group: ';
        $branch_names = array();
        foreach($branch_groups as $row){
            $branch_names[] = $row->name;
        }
        return '<span style="color:red">'.$str . implode(' - ',$branch_names).'</span>';
    }


    function getDataForQuestionForm($id = 0)
    {
        $data = new stdClass();
        $questionaire_model = $this->load->table_model('question');
        $customer_model = $this->load->table_model('customer');
        $data->questions = $questionaire_model->get();
        foreach ($data->questions as $question) {
            $question->type_values = json_decode($question->type_values);
        }
        if ($id == 0) {


        } else {
            $data->answers = $customer_model->getCustomerAnswers($id);
        }

        $data->customer_id = $id;
        return $data;
    }

    function insert($values,$user_level = null)
    {
        $customers_model = $this->load->table_model('customer');
        $survey_model = $this->load->table_model('customer_question');
        $question_data = isset($values->survey) ? json_decode($values->survey) : array();
        while (1) {
            $values->code = 'SPA' . mt_rand(100001, 900000);
            $this->db->where('code',$values->code);
            $query = $this->db->get($this->main_table);
            $check = $query->result_array();
            
            if (count($check) == 0)
                break;
        }

        $this->db->trans_start();
        $values->created_date = get_database_date();

        $customer_id = $customers_model->insert($values,$user_level);
        if ($customer_id) {
            foreach ($question_data as $answer) {
                $answer->customer_id = $customer_id;
                if (is_array($answer->answer))
                    $answer->answer = json_encode($answer->answer);
                $survey_model->insert($answer);
            }
        }
        if ($this->db->trans_complete()) {
            return $customer_id;
        }
        return false;
    }

    function update($id, $values)
    {
        $customer_model = $this->load->table_model('customer');
        $this->db->trans_start();

        $values->updated_date = get_database_date();
        $customer_model->update($id, $values);
        if ($this->db->trans_complete()) {
            return true;
        }
        return false;
    }

    function editSurvey($id, $answers)
    {
        $survey_model = $this->load->table_model('customer_question');
        $this->db->trans_start();
        $survey_model->delete(array('customer_id' => $id));
        foreach ($answers as $answer) {
            $answer = (object) $answer;
            $answer->customer_id = $id;
            if (is_array($answer->answer))
                $answer->answer = json_encode($answer->answer);
            $survey_model->insert($answer);
        }
        if($this->db->trans_complete()){
            return true;
        }
        return false;
    }

    function insert_to_branch($id, $identity, $type)
    {
        $customer_model = $this->load->table_model('customer');

        if($type == CUSTOMER_TYPE('Member')){
            $result = $customer_model->get(array('id' => $id, 'pin' => $identity));
        }else{
            $result = $customer_model->get(array('id' => $id, 'mobile_number' => $identity));
        }


        if (count($result)) {
            $this->db->trans_start();
            $customer_model->insert(array('id' => $id));
            $this->db->trans_complete();
            return true;
        } else {
            return $type==CUSTOMER_TYPE('Member')?'Pin is incorrect':'Mobile Number is incorrect';
        }
    }

    function renderDataSet($data)
    {
        foreach ($data as $row) {
            if($row->customer_type != CUSTOMER_TYPE('Member')){
                $row->isMember = false;
            }else{
                $row->isMember = true;
            }
            $row->customer_type = CUSTOMER_TYPE($row->customer_type);
        }
        return $data;
    }

    function getCustomerPin($id)
    {
        $customer_model = $this->load->table_model('customer');
        $result = $customer_model->get(array('id' => $id));
        return $result[0]->pin;
    }

    function setCustomerPin($id, $pin, $opin)
    {
        $customer_model = $this->load->table_model('customer');
        $check = $customer_model->get(array('id' => $id, 'pin' => $opin));
        if(count($check) == 0){
            return array(
                'opin' => 'Incorrect Pin'
            );
        }
        $result = $customer_model->update($id, (object)array('pin' => $pin));
        return $result;
    }

    function getCustomerCreditHistory($id){
        $ret = array();
        $customer_credits = $this->load->table_model('credit')->get(array('customer_id' => $id),"","","","",array(),true);

        $credits_adjustment = $this->load->table_model('credit_adjustment_log')->get_detail(array('credit_adjustment_log.credit_id' => convert_to_array($customer_credits,'','id')),'log_time desc');

        if(count($customer_credits) > 0){
            $credit_ids = array();
            $customer_packages = array();
            $package_ids = array();
            foreach($customer_credits as $credit){
                $customer_packages[$credit->item_id] = $credit;
                $package_ids[] = $credit->item_id;
                $credit_ids[] = $credit->id;
                $credit->detail = array();
                $ret[$credit->id] = $credit;
            }

            foreach($ret as $ret_item){
                $ret_item->change_time_log = $this->select(array(
                    'select' => array('start_date,end_date,credit,log_time'),
                    'from'   => array('credit_log' => array('table'    =>'credit_log')),
                    'where'  => array('id' => $ret_item->id,'customer_id' => $id, 'item_id' => $ret_item->item_id , 'log_type' => array('update','insert')),
                    'group'=> 'end_date',
                    'order'=> 'log_time ASC'
                ))->result();
                if(count($ret_item->change_time_log) == 0){
                    $ret_item->change_time_log[] = (object) array(
                        'start_date' => $ret_item->start_date,
                        'end_date'   => $ret_item->end_date,
                        'credit'     => $ret_item->credit,
                        'log_time'   => $ret_item->end_date);
                }
            }

            $customer_credit_bills = $this->load->table_model('bill_item_credit')->get_bill_item_credit($id, $credit_ids, $package_ids, 'bill.created_date DESC');

            $customer_credit_transactions = array_sort_asc($customer_credit_bills,$credits_adjustment,'created_date','log_time','asc');

            $item_ids = $package_ids;
            $bill_item_ids = array();
            $employee_ids = array();
            foreach($customer_credit_transactions as $trans){
                $item_ids[] = $trans->item_id;
                if(isset($trans->created_date)){
                    $trans_ids[] = $trans->bill_item_id;
                    $employee_ids[$trans->creator] = $trans->creator;
                    if(isset($trans->credit_id) && $trans->credit_id != 0){
                        if(isset($ret[$trans->credit_id])){
                            $ret[$trans->credit_id]->detail[] = array(
                                'type' => 0,
                                'dateUnix' => strtotime($trans->created_date),
                                'date' => get_user_date($trans->created_date, "", "", true),
                                'value' => $trans->credit_value,
                                'item_id' => $trans->item_id,
                                'bill_item_id' => $trans->bill_item_id,
                                'cashier_id' => $trans->creator,
                                'cashier_name' => "",
                                'employee' => array(),
                                'branch_id' => $trans->branch_id,
                                "branch_name" => "",
                                'bill_code' => $trans->bill_code
                            );
                        }
                    }
                    else if(isset($trans->credit_add_id) && $trans->credit_add_id != 0){
                        if(isset($ret[$trans->credit_add_id])){
                            $ret[$trans->credit_add_id]->detail[] = array(
                                'type' => 1,
                                'dateUnix' => strtotime($trans->created_date),
                                'date' => get_user_date($trans->created_date, "", "", true),
                                'value' => $trans->credit_add,
                                'item_id' => $trans->item_id,
                                'bill_item_id' => $trans->bill_item_id,
                                'cashier_id' => $trans->creator,
                                'cashier_name' => "",
                                'employee' => array(),
                                'branch_id' => $trans->branch_id,
                                "branch_name" => "",
                                'bill_code' => $trans->bill_code
                            );
                        }
                    }elseif(isset($trans->item_id) && $trans->item_id != 0){
                        if(isset($customer_packages[$trans->item_id])){
                            $credit = $customer_packages[$trans->item_id];
                            $ret[$credit->id]->detail[] = array(
                                'type' => 1,
                                'dateUnix' => strtotime($trans->created_date),
                                'date' => get_user_date($trans->created_date, "", "", true),
                                'value' => $trans->credit_add,
                                'item_id' => $trans->item_id,
                                'bill_item_id' => $trans->bill_item_id,
                                'cashier_id' => $trans->creator,
                                'cashier_name' => "",
                                'employee' => array(),
                                'branch_id' => $trans->branch_id,
                                'branch_name' => "",
                                'bill_code' => $trans->bill_code
                            );
                        }
                    }
                    $bill_item_ids[] = $trans->bill_item_id;
                }elseif(isset($trans->log_time)){
                    $log_reason = 'Admin Adjustment';
                    if(isset($trans->credit_id_transfer) && $trans->credit_id_transfer){
                        $log_reason = 'Credit Transfer';
                    }
                    $ret[$trans->id]->detail[] = array(
                        'type' => 2,
                        'dateUnix' => strtotime($trans->log_time),
                        'date' => get_user_date($trans->log_time, "", "", true),
                        'value' => $trans->credit_change,
                        'item_id' => $trans->item_id,
                        'bill_code' => $log_reason,
                    );
                }
            }

            $item_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $item_ids), true, 4);
            $branch_map = $this->load->table_model('branch')->getTableMap('id', 'name');
            $bill_employee_map = $this->load->table_model('bill_employee')->getTableMap('bill_item_id', '', array('bill_item_id' => $bill_item_ids), false);
            foreach($bill_employee_map as $bill_item_id => $bill_employees){
                $employee_ids += convert_to_array($bill_employees, "employee_id", "employee_id");
            }
            $employee_map = $this->load->table_model('employee')->getTableMap('id', '', array('id' => $employee_ids), true, 4);
            foreach($ret as $credit_id => &$value){
                if(isset($item_map[$value->item_id])){
                    unset($item_map[$value->item_id]->package);
                    $value->item = $item_map[$value->item_id];
                }
                else{
                    $value->item = null;
                }
                $value->employee = (isset($item_map[$value->item_id])) ? $item_map[$value->item_id] : null;
                foreach($value->detail as &$detail){
                    if(isset($item_map[$value->item_id])){
                        unset($item_map[$detail['item_id']]->package);
                        unset($item_map[$detail['item_id']]->categories);
                        unset($item_map[$detail['item_id']]->service);
                        $detail['item'] = $item_map[$detail['item_id']];
                    }
                    else{
                        $detail['item'] = null;
                    }
                    $detail['item'] = (isset($item_map[$detail['item_id']])) ? $item_map[$detail['item_id']] : null;
                    if(isset($detail['type']) && $detail['type'] != 2){
                        if(isset($bill_employee_map[$detail['bill_item_id']])){
                            foreach($bill_employee_map[$detail['bill_item_id']] as $bill_employee){
                                if(isset($employee_map[$bill_employee->employee_id])){
                                    $employee = $employee_map[$bill_employee->employee_id];
                                    $detail['employee'][] = to_full_name($employee->first_name, $employee->last_name);
                                }
                            }
                        }
                        if(isset($employee_map[$detail['cashier_id']])){
                            $cashier = $employee_map[$detail['cashier_id']];
                            $detail['cashier_name'] = to_full_name($cashier->first_name, $cashier->last_name);
                        }
                        if(isset($branch_map[$detail['branch_id']])){
                            $detail['branch_name'] = $branch_map[$detail['branch_id']];
                        }
                    }

                }
            }
        }
        return $ret;
    }

    function get_credit_adjustment_data($customer_id){
        $customer_credits = $this->load->table_model('credit')->get(array('customer_id' => $customer_id));
        $items = $this->load->table_model('item')->getTableMap('id', '', array('id' => convert_to_array($customer_credits,'','item_id')));
        foreach($customer_credits as $item_id=>$credit_field){
            foreach($items as $key => $item){
                if($credit_field->item_id == $item->id){
                    $customer_credits[$item_id]->item_detail = $item;
                    break;
                }
            }
        }
        return array('customer_credits' => $customer_credits,'customer_id' => $customer_id);
    }

    function adjust_credit($credit_data){
        $user_id    = $this->user_check->get('user_id');
        $log_time   = get_database_date(null,null,true);

        $credit_model                       = $this->load->table_model('credit');
        $credit_adjustment_log_model        = $this->load->table_model('credit_adjustment_log');

        $this->db->trans_start();
        $customer_credits = $credit_model->getTableMap('id','',array('id' => array_keys($credit_data)));
        $processed_id = array();
        foreach($credit_data as $credit_id=>$credit_fields){
            $credit_record = $customer_credits[$credit_id];
            $credit_value = $credit_fields['credit'];
            $update_fields = array();
            if(isset($credit_fields['expiry'])){
                $update_fields['end_date'] =  get_database_date($credit_fields['expiry'] . ' 23:59:59');
            }
            if($credit_value != $credit_record->credit){
                if($credit_value < 0){
                    throw new Exception('No credit is allowed lower than zero');
                }
                $credit_change = $credit_value - $credit_record->credit;
                $update_fields['credit_original'] =  $credit_record->credit_original + $credit_change;
                $update_fields['credit'] = $credit_value;
                $credit_adjustment_log_model->insert(array(
                    'credit_id'             => $credit_id,
                    'credit_change'         => $credit_change,
                    'credit_value_before'   => $credit_record->credit,
                    'log_time'              => $log_time,
                    'creator'               => $user_id
                ));
            }
            $credit_model->update($credit_id,$update_fields);
            $processed_id[] = $credit_id;
        }
        $this->db->trans_complete();
        return true;
    }


    function reset_pin($id){
        $result = $this->load->table_model('customer')->update($id,array('pin'=>rand(100000,999999)),false);
        return $result;
    }

    function convert_customer($id, $pin){
        return $this->load->table_model('customer')->update($id,array('pin' => $pin,'customer_type' => CUSTOMER_TYPE('Member')));
    }

    function delete($ids){
        $modelName = $this->load->table_model($this->main_table);
        $this->db->trans_start();
        $result = $modelName->delete(array('id' => $ids),Permission_Value::BRANCH_GROUP);
        $this->db->trans_complete();
        return $result;
    }

    function InsertTemp($customer_info){
        $customerTempId = $this->load->table_model('customer_temp')->insert($customer_info);
        return $customerTempId;
    }

    function UpdateTemp($id,$customer_info){
        $customerTempId = $this->load->table_model('customer_temp')->update($id,$customer_info);
        return $customerTempId;
    }

    function update_first_login($cid){
        return $this->load->table_model('customer')->update($cid,array('is_new' => 0));
    }


    function getFirstlogin_status($cid)
    {
        $customer_model = $this->load->table_model('customer');
        $result = $customer_model->get(array('id' => $cid));
        return $result[0]->is_new;
    }
    function getExcelData(){

        $branchGroupId = $this->session->all_userdata()['staff']['login']->branch_group_id;

        $res = $this->select(array(
            'select' => array(
                'customer' => array('Code' => 'code','Name' => 'first_name','Email' => 'email' ,'NRIC' => 'nric','Mobile' => 'mobile_number'),
                'item' => array('"Package Name"' => 'name'),
                'credit' => array('"Package Credits"' => 'credit')
            ),
            'from' => array(
                'customer' => array('table' => 'customer'),
                'credit' => array('table' => 'credit', 'condition' => 'customer.id = credit.customer_id', 'LEFT'),
                'item' => array('table' => 'item', 'condition' => 'credit.item_id = item.id', 'LEFT')
            ),
            'where' => array(
                'customer.customer_type' => CUSTOMER_TYPE('Member'),
                'customer.status' => CUSTOMER_STATUS('Active'),
                'credit.status' => 1 // ACTIVE
            ),
            'orderby' => 'customer.id'
        ))->result_array();

        return $res;

    }

    function getCreditInfo($creditId){
        return $this->select(array(
            'select' => array(
                'customer' => array('code','first_name','last_name'),
                'item' => array('itemName' => 'name')
            ),
            'from' => array(
                'credit' => array('table' => 'credit'),
                'customer' => array('table' => 'customer', 'condition' => 'credit.customer_id=customer.id','LEFT'),
                'item' => array('table' => 'item', 'condition' => 'credit.item_id=item.id')
            ),
            'where' => array(
                'credit.id' => $creditId
            )
        ))->row();
    }

    function validationCustomer($validFields){
        return $this->load->table_model('customer')->validationCustomer($validFields);
    }
}