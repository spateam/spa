<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Show_In_Booking_Controller_model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "show_in_booking";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

}