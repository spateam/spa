<?php

class Commission_Controller_Model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
        $this->main_table = "commission";
        $this->suggestionSearch = array('name');
        $this->suggestionDisplay = array('name');
    }

    function getDataForViewForm($id = 0){
        $item_model = $this->load->table_model('commission');
        $data = parent::getDataForViewForm($id);
        $commission_categories = array();
        $commission_items = array();
        if(isset($data->item)){
            $commission_category_result = $this->load->table_model('commission_category')->get(array('commission_id' => $id));
            foreach($commission_category_result as $commission_category){
                if(!isset($commission_categories[$commission_category->category_id])){
                    $commission_categories[$commission_category->category_id] = array();
                    $commission_categories[$commission_category->category_id]['category_name'] = $commission_category->category_name;
                    $commission_categories[$commission_category->category_id]['rules'] = array();
                }
                $commission_categories[$commission_category->category_id]['rules'][] = $commission_category;
            }
            $commission_items = $this->load->table_model('commission_item')->get(array('commission_id' => $id),'commission_item.item_id desc');
        }else{
            $data->item = new stdClass();
        }
        $data->item->commission_categories = $commission_categories;
        $data->item->commission_items = $commission_items;
        return $data;
    }

    function insert($values){
        $values = (object)$values;
        $this->db->trans_start();
        $id = parent::insert($values);
        // Insert Commission Categories
        $commission_categories = isset($values->commission_categories)?$values->commission_categories:array();
        if(count($commission_categories) > 0){
            $commission_category_model  = $this->load->table_model('commission_category');
            foreach($commission_categories as $commission_category){
                $c_category = (object) $commission_category;
                if(!isset($c_category->category_id) || !isset($c_category->value)
                    || !is_numeric($c_category->value) || $c_category->value == 0){
                    continue;
                }
                $c_category->commission_id = $id;
                if(!isset($c_category->from) || !is_numeric($c_category->from)){
                    $c_category->from = 0;
                }
                if(!isset($c_category->to) || !is_numeric($c_category->to)){
                    $c_category->to = null;
                }
                $commission_category_model->insert($c_category);
            }
        }
        // Insert Commission Items
        $commission_items = isset($values->commission_items)?$values->commission_items:array();
        if(count($commission_items) > 0){
            $commission_item_model = $this->load->table_model('commission_item');
            $bundle_item_model          = $this->load->table_model('item_bundle_item');
            foreach($commission_items as $commission_item){
                $c_item = (object) $commission_item;
                if(is_numeric($c_item->value) && $c_item->value != 0){
                    $c_item->commission_id = $id;
                    $commission_item_model->insert($c_item);
                }
                if(isset($c_item->sub_items)){
                    //Check exists
                    foreach($c_item->sub_items as $sub_item){
                        $is_exists = $bundle_item_model->get(array('sub_item_id' => $sub_item->id,'item_id' => $c_item->item_id));
                        if(count($is_exists)){
                            $bundle_item_model->update($is_exists[0]->id,array('commission_value' => $sub_item->value));
                        }else{
                            throw new Exception("There is missing item on a bundle item. Developer Code: 'I:'$c_item->item_id - S:$sub_item->value");
                        }
                    }
                }
            }
        }
        if($this->db->trans_complete()){
            return $id;
        }
        return false;
    }

    function update($id, $values){
        if(is_object($values)){
            $values = (array)$values;
        }
        $this->db->trans_start();
        $result = parent::update($id, $values);

        $commission_category_model = $this->load->table_model('commission_category');
        // Delete Old Commission Item
        $delete_commission_categories = $commission_category_model->delete(array('commission_id' => $id));
        // Insert Commission Categories
        if(isset($values['commission_categories'])) {
            $commission_categories = $values['commission_categories'];
            if (count($commission_categories) > 0) {
                $commission_category_model = $this->load->table_model('commission_category');
                foreach ($commission_categories as $commission_category) {
                    $c_category = (object)$commission_category;
                    if (!isset($c_category->category_id) || !isset($c_category->value)
                        || !is_numeric($c_category->value) || $c_category->value == 0
                    ) {
                        continue;
                    }
                    $c_category->commission_id = $id;
                    if (!isset($c_category->from) || !is_numeric($c_category->from)) {
                        $c_category->from = 0;
                    }
                    if (!isset($c_category->to) || !is_numeric($c_category->to)) {
                        $c_category->to = null;
                    }
                    $commission_category_model->insert($c_category);
                }
            }
        }

        $commission_item_model = $this->load->table_model('commission_item');
        $bundle_item_model     = $this->load->table_model('item_bundle_item');
        // Delete Old Commission Item
        $delete_commission_items = $commission_item_model->delete(array('commission_id' => $id));
        // Insert Commission Item
        if(isset($values['commission_items'])) {
            $commission_items = $values['commission_items'];
            if (count($commission_items) > 0) {
                foreach ($commission_items as $commission_item) {
                    $c_item = (object)$commission_item;
                    if ((is_numeric($c_item->value) && $c_item->value != 0) || $c_item->type == COMMISSION_BUNDLE_TYPE('Inherit')) {
                        $c_item->commission_id = $id;
                        $commission_item_model->insert($c_item);
                    }
                    if(isset($c_item->sub_items) && $c_item->type != COMMISSION_BUNDLE_TYPE('Inherit')){
                        //Check exists
                        foreach($c_item->sub_items as $sub_item){
                            $sub_item = (object)$sub_item;
                            $is_exists = $bundle_item_model->get(array('sub_item_id' => $sub_item->id,'item_id' => $c_item->item_id));
                            if(count($is_exists)){
                                $bundle_item_model->update($is_exists[0]->id,array('commission_value' => $sub_item->value));
                            }else{
                                throw new Exception("There is missing item on a bundle item. Developer Code: 'I:'$c_item->item_id - S:$sub_item->value");
                            }
                        }
                    }
                }
            }
        }
        if($this->db->trans_complete()){
            return true;
        }
        return false;
    }
}