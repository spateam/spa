<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:57 PM
 */

class Employees_Controller_Model extends POS_Controller_Model
{

    function __construct()
    {
        parent::__construct();
        $this->main_table = "employee";
        $this->suggestionSearch = array('first_name', 'last_name', 'phone_number', 'email');
        $this->suggestionDisplay = array('first_name', 'last_name', 'phone_number', 'email');
        $this->minimalSuggestionDisplay = array('first_name', 'last_name');
        $modelName = $this->load->table_model($this->main_table);
        $modelName->set_is_all(true);
    }


    function getDataSet($page = 1, $condition = array()){
        $modelName = $this->load->table_model($this->main_table);
        $modelName->set_is_all(true);
        $data = parent::getDataSet($page, $condition);
        return $data;
    }

    function renderDataSet($data){
        $therapist_department_id_list = $this->load->table_model('department')->get_department_serve_booking();
        foreach($data as $row){
            $row->is_therapist = in_array($row->department_id,$therapist_department_id_list)?1:0;
        }
        return $data;
    }

    function getDataForViewForm($id = 0){
        $data = parent::getDataForViewForm($id);
        $data->branchs = $this->load->table_model('branch')->get();
        $data->departments = $this->load->table_model('department')->get();
        $data->commissions = $this->load->table_model('commission')->getTableMap('id', 'name');
        return $data;
    }

    function insert($values){
        $values = (array)$values;
        $employee_model = $this->load->table_model('employee');

        $checkDuplicateEmail = $employee_model->get(array('email' => $values['email']));
        if(count($checkDuplicateEmail)){
            $checkDuplicateEmail = $checkDuplicateEmail[0];
            if(is_null($checkDuplicateEmail->employee_permission))
                return json_encode(array('duplicate_email' => $checkDuplicateEmail->id));
        }
        $this->db->trans_begin();
        $record = new stdClass();
        foreach($values as $key=>$value){
            $record->$key = $value;
        }
        if(isset($record->password)){
            if($record->password== ''){
                unset($record->password);
            }else{
                $record->password = md5($record->password);
            }
        }
        $therapist_id = $this->load->table_model('department')->get_department_serve_booking();
        if(in_array($record->department_id,$this->load->table_model('department')->get_department_serve_booking())){
            $record->ordering = count($employee_model->get(array('department_id' => $therapist_id)))+1;
        }

        $id = $employee_model->insert($record);
        if(isset($values['calendar'])){
            $result = $this->changeCalendar($id,$values['calendar']);
            if(!is_numeric($result)){
                return $result;
            }
            if(!$result){
                return 'Can not change calendar';
            }
        }
        $this->db->trans_complete();
        return $id;
    }

    function update($id, $values){
        $emplyoee_model = $this->load->table_model('employee');
        $record = new stdClass();
        foreach($values as $key=>$value){
            $record->$key = $value;
        }
        if(isset($record->password)){
            if($record->password== ''){
                unset($record->password);
            }else{
                $record->password = md5($record->password);
            }
        }
        $this->db->trans_start();
        $result = $emplyoee_model->update($id,$record);
        $this->db->trans_complete();
        return $result;
    }

    function insert_to_branch($id){
        $this->db->trans_start();
        $result = $this->load->table_model($this->main_table)->insert_to_branch($id);
        $this->db->trans_complete();
        return $result;
    }

    function getDataForCalendar($id){
        $employee_calendar = $this->load->table_model('employee_calendar')->get(array('employee_id' => $id));
        $data = new stdClass();
        if(count($employee_calendar)){
            $data->item = convert_to_array($employee_calendar,'day_id','',true);
        }else{
            $data->item = null;
        }
        return $data;
    }

    function autoFixEmployeeTime($id){
        $listRemove = $this->select(array(
            'select' => array(
                'employee_calendar' => array('id')
            ),
            'from' => array(
                'employee_branch' => array('table' => 'employee_branch'),
                'employee_calendar' => array('table' => 'employee_calendar', 'condition' => 'employee_branch.employee_id = employee_calendar.employee_id', 'LEFT')
            ),
            'where' => array(
                'employee_calendar.employee_id' => $id,
                'employee_calendar.branch_id != employee_branch.branch_id'
            )
        ))->result();
        $employee_calendar_model = $this->load->table_model('employee_calendar');
        foreach($listRemove as $item){
            $employee_calendar_model->delete(array('id'=>$item->id));
        }
        return;
    }

    function checkEmployeeAppointment($id,$data,$custom_days=""){
        $full_array = array(0,1,2,3,4,5,6);

        foreach($data as $key => $val){
            $day_id = substr($key,0,1);
            if(in_array($day_id,$full_array)){
                unset($full_array[$day_id]);
            }
        }

        $check = $this->select(array(
            'select' => array(
                'booking' => array('id'),
                'booking_service' => array('start_time','end_time')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id','LEFT')
            ),
            'where' => array(
                'booking.status' => array(BOOKING_STATUS('HOLD'),BOOKING_STATUS('COMPLETE')),
                'employee_id' => $id,
                'start_time > NOW()'
            )
        ))->result_array();

        $conflick_num = 0;
        foreach($check as $key => $item){
            if(in_array(date('w',strtotime($item['start_time'])),$full_array)){
                $conflick_num++;
            }
        }
        $timezone = $this->system_config->get('timezone');
        if($custom_days != ""){
            $temp_array = rtrim($custom_days, "|");
            $temp_array = explode("|", $temp_array);
            foreach($temp_array as $values){
                $values = explode('_',$values);
                foreach($check as $key => $item){
                    $tempDate  = get_user_date($item['start_time'],$timezone,'Y-m-d H:i:s',true);
                    $tempDate2 = get_user_date($item['end_time'],$timezone,'Y-m-d H:i:s',true);
                    if(date('Y-m-d',strtotime($tempDate)) == $values[0]){
                        if(date('H:i', $tempDate) > $values[1] && date('H:i', $tempDate) < $values[2]){
                            $conflick_num++;
                            break;
                        }
                        if(date('H:i', $tempDate2) > $values[1] && date('H:i', $tempDate2) < $values[2]){
                            $conflick_num++;
                            break;
                        }
                        if(date('H:i', $tempDate) < $values[1] && date('H:i', $tempDate2) > $values[2]){
                            $conflick_num++;
                            break;
                        }
                    }
                }
            }
        }

        if($conflick_num > 0){
            return 'This employee still has '.$conflick_num.' booking appointment on scheduler.';
        }
        else{
            return TRUE;
        }
    }

    function changeCalendar($id,$data,$custom_days=""){
        $branch_id = $this->load->table_model('branch')->get_branch_by_employee_id($id);
        $willInsert = array();
        $cdays = "";
        if($data != ""){
            foreach($data as $key=>$value){
                $day_id = substr($key,0,1);
                if(!isset($willInsert[$day_id])){
                    $willInsert[$day_id] = [];
                }
                $type = substr($key,2);
                $willInsert[$day_id][$type] = $value;
                if($type=='end'){
                    if(isset($willInsert[$day_id]['start'])){
                        if($willInsert[$day_id]['start'] > $willInsert[$day_id]['end']){
                            return 'Start time is later than End time on '.WORKING_DAYS($day_id);
                        }
                    }
                }else{
                    if(isset($willInsert[$day_id]['end'])){
                        if($willInsert[$day_id]['start'] > $willInsert[$day_id]['end']){
                            return 'Start time is later than End time on '.WORKING_DAYS($day_id);
                        }
                    }
                }
            }
        }
        $this->db->trans_begin();
        $employee_calendar_model = $this->load->table_model('employee_calendar');
        $employee_calendar_model->delete(array('employee_id'=>$id));
        foreach($branch_id as $branch){
            foreach($willInsert as $key=>$value){
                $employee_calendar_model->insert(array(
                    'employee_id' => $id,
                    'day_id'      => $key,
                    'start_time'  => $value['start'],
                    'end_time'    => $value['end'],
                    'branch_id'   => $branch->branch_id
                ));
            }
        }
        if($custom_days != ""){
            $custom_array = array();
            $employee_calendar_customize_model = $this->load->table_model('employee_calendar_customize');
            $employee_calendar_customize_model->delete(array('employee_id'=>$id));
            $temp_array = rtrim($custom_days, "|");
            $temp_array = explode("|", $temp_array);
            foreach($temp_array as $values){
                $temp = explode("_", $values);
                $custom_array[$temp[0]] = array();
                $custom_array[$temp[0]][0] = $temp[1];
                $custom_array[$temp[0]][1] = $temp[2];
                $custom_array[$temp[0]][2] = $temp[3];
            }
         
            foreach($branch_id as $branch){
                foreach($custom_array as $key => $value){
                    $dayofweek = date('w', strtotime(trim($key)));
                    if(isset($willInsert[$dayofweek])){
                        $default = $willInsert[$dayofweek];
                        $custom_start_time = $value[0];
                        $custom_end_time = $value[1];
                        $custom_status = $value[2];
                        $set_default = 0;                        

                        if(!preg_match("#([0-1]{1}[0-9]{1}|[2]{1}[0-3]{1}):[0-5]{1}[0-9]{1}#", $custom_start_time))
                            $set_default = 1;

                        if(!preg_match("#([0-1]{1}[0-9]{1}|[2]{1}[0-3]{1}):[0-5]{1}[0-9]{1}#", $custom_end_time))
                            $set_default = 1;

                        if($custom_start_time >= $custom_end_time)
                            $set_default = 1;

                        if($set_default){
                            $custom_start_time = $default["start"];
                            $custom_end_time = $default["end"];
                            $custom_status = 1;
                        }
                        
                        $employee_calendar_customize_model->insert(array(
                            'employee_id' => $id,
                            'day_id'      => $dayofweek,
                            'branch_id'   => $branch->branch_id,
                            'start_time'  => $custom_start_time,
                            'end_time'    => $custom_end_time,
                            'date'        => $key,
                            'working'     => $custom_status,
                        ));
                    }
                }
            }
        }
        $this->db->trans_complete();
        return true;
    }

    function changeAbsent($id,$data){
        $date_start = $data->absent_from;
        $date_end   = $data->absent_to;
        $st = get_database_date($date_start);
        $et = get_database_date($date_end);
        $employee_absent_model = $this->load->table_model('employee_absent');
        $employee_absent = $employee_absent_model->get(array('employee_id' => $id,
                "((start_time < '{$st}' AND end_time > '{$st}')
                OR (start_time > '{$st}' AND start_time < '{$et}' AND end_time > '{$st}'))")
        );

        $this->db->trans_begin();
        $employee_absent_model->delete(array('id' => convert_to_array($employee_absent,'','id'),'employee_id' => $id));

        $result = $employee_absent_model->insert(array(
            'employee_id'   => $id,
            'start_time'    => $st,
            'end_time'      => $et,
            'type'          => $data->type,
            'description'   => $data->description
        ));

        $this->db->trans_complete();

        $booked_date = $this->load->table_model('booking_service')->get_booking_range(array('start_time' => $st, 'end_time' => $et, 'condition' => array('booking_service.employee_id' => $id)));

        if(count($booked_date)){
            $customers = $this->load->table_model('customer')->get(array('id'=>convert_to_array($booked_date,'','customer_id')));
            $customers = convert_to_array($customers,'id');
            $result = "This employee have some appointment on this time. Please report to the following customer: <br>";
            foreach($booked_date as $row){
                $customer = $customers[$row->customer_id];
                $result .= to_full_name($customer->first_name,$customer->last_name)." {$customer->mobile_number} on {$row->start_time} <br>";
            }
        }
        return $result;
    }

    function getDataForAbsent($id,$data=array())
    {
        $data = (object)$data;
        $employee_absents = $this->select(array(
            'select' => array(
                'booking' => array('id','type' => 'status'),
                'booking_service' => array('start_time','end_time')
            ),
            'from'   => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id')
            ),
            'where'  => array(
                'booking.status' => array(BOOKING_STATUS('Away'),BOOKING_STATUS('Offline')),
                'booking_service.employee_id' => $id
            ),
            'order' => 'booking_service.start_time desc'
        ))->result();
        $data->absent_data = $employee_absents;
        return $data;
    }

    function getCurrentAbsent($id)
    {
        $employee_absents = $this->load->table_model('employee_absent')->get(array('id' => $id));
        foreach($employee_absents as $row){
            $starttime = strtotime($row->start_time) + (8*60*60);
            $endtime = strtotime($row->end_time) + (8*60*60);
            $starttime = date('d-m-Y H:i:s',$starttime);
            $endtime = date('d-m-Y H:i:s',$endtime);

            $row->start_time= $starttime;
            $row->end_time= $endtime;
        }

        return $employee_absents;
    }

    function deleteAbsentDate($id){
        $result = $this->load->controller_model('scheduler')->delete_booking($id);
        if($result){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function get_assigned_service_form_data($id){
        $services = $this->load->table_model('employee')->get_assigned_service($id,false);
        return array(
            'assigned_service_detail' => $services
        );
    }

    function assign_service($employee_id,$service_id_list){
        $existing_service = $this->load->table_model('employee')->get_assigned_service($employee_id);
        $employee_service_model = $this->load->table_model('employee_service');
        $this->db->trans_start();
        $employee_service_model->delete(array('item_id' => $existing_service,'employee_id' => $employee_id));
        if (!empty($service_id_list)){
            foreach($service_id_list as $each_service_id){
                $employee_service_model->insert(array('employee_id' => $employee_id, 'item_id' => $each_service_id));
            }
        }
        $this->db->trans_complete();
        return true;
    }
}