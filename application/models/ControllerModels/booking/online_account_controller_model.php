<?php

class Online_Account_Controller_model extends POS_Controller_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function getSectionBookingData($customer){
        $listBooking = $this->db->select('GROUP_CONCAT(booking.id) as list_id',FALSE)
                                ->where(array('booking.customer_id' => $customer['id']))
                                ->get('booking')->row_array();
        if(!empty($listBooking) && $listBooking['list_id'] !== NULL){
            return $this->db->select('booking.id,booking.code as booking_code,booking.status ,branch.name as branch_name, employee.first_name as therapist,
                                              item.name as service,booking_service.start_time, booking_service.end_time')
                                    ->join('booking_service','booking.id = booking_service.booking_id','LEFT')
                                    ->join('branch','booking.branch_id = branch.id','LEFT')
                                    ->join('employee','booking_service.employee_id = employee.id','LEFT')
                                    ->join('item','booking_service.service_id = item.id','LEFT')
                                    ->where('booking.id IN ('.$listBooking['list_id'].')')
                                    ->order_by('booking.created_date DESC')
                                    ->get('booking')->result_array();
        }
        else{
            return array();
        }
    }

    function getNextAppt($customerId){
        $bookingId = $this->db->select('booking.id,booking.code as booking_code,booking.status ,branch.name as branch_name, employee.first_name as therapist,
                                                                  item.name as service,booking_service.start_time, booking_service.end_time')
                                ->join('booking_service','booking.id = booking_service.booking_id','LEFT')
                                ->join('branch','booking.branch_id = branch.id','LEFT')
                                ->join('employee','booking_service.employee_id = employee.id','LEFT')
                                ->join('item','booking_service.service_id = item.id','LEFT')
                                ->where(array(
                                    'customer_id' => $customerId,
                                    'booking_service.start_time >' => get_database_date()
                                ))
                                ->where('booking.status IN ('.BOOKING_STATUS('Complete').')')
                                ->get('booking')->row_array();
        if(!empty($bookingId)){
            $bookingId['start_time'] = get_user_date($bookingId['start_time'],'','d-m-Y H:i');
        }
        return $bookingId;
    }
}