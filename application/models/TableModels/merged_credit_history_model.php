<?php
class Merged_Credit_History_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "merged_credit_history";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

}