<?php

class Customer_Branch_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "customer_branch";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }
}