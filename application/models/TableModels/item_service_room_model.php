<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Item_Service_Room_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "item_service_room";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = array();
        $this->tableType = "connected";
    }
}
