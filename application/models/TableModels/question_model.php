<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Question_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "question";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }
}


