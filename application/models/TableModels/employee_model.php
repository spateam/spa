<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Employee_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "employee";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array(
            'username'      => array(Constraints::Required, Constraints::Unique),
            'first_name'    => array(Constraints::Required),
            'email'         => array(Constraints::Required,Constraints::Email, Constraints::Unique),
            'password'      => array(Constraints::Required)
        );
        $this->tableType = "main";
    }

    function generateBaseInfo($condition = array(), $user_level = "", $permission = array()){
        if($user_level !== ""){
            $condition[] = "employee.type <= $user_level";
        }else{
            if($this->session->userdata('login') != false){
                $login = $this->session->userdata('login');
                $permission_key = $this->config->item('permissionLevelKey');
                if(isset($login->{$permission_key})){
                    $current_user_level = $login->{$permission_key};
                    $condition[] = "employee.type <= $current_user_level";
                }
            }
        }
        return parent::generateBaseInfo($condition, $user_level, $permission);
    }

    function getCurrentUserPermissions($permission_fields = array()){
        $permission = isset($permission_fields['permission'])?$permission_fields['permission']:array();
        $user_level = isset($permission_fields['user_level'])?$permission_fields['user_level']:'';
        $employee_permissions = $this->select(
            array(
                'select'    => array('module_permission' => array('id')),
                'from'      => array(
                    'module_permission'                     =>  array('table' => 'module_permission'),
                ),
                'permission' => $permission,
                'user_level' => $user_level
            )
        )->result();
        $employee_permissions = convert_to_array($employee_permissions, '', 'id');
        return $employee_permissions;
    }

    function getEmployeePermissions($user_id){
        $user = $this->getByID($user_id);
        return $this->load->table_model('department')->getDepartmentPermissions($user->department_id);
    }

    function update($id,$field_list,$constraint_check = true){
        //Remove password constraints
        $this->fieldListRequirement = (object) array(
            'username'      => array(Constraints::Required, Constraints::Unique),
            'first_name'    => array(Constraints::Required),
            'email'         => array(Constraints::Required,Constraints::Email)
        );

        return parent::update($id,$field_list,$constraint_check);
    }

    function insert_to_branch($id){
        $ret = $this->insert(array('id'=>$id));
        return $ret;
    }

    public function get_join_restriction($checkValue = array(), $alias = "", $userLevel = ""){
        $ret = parent::get_join_restriction($checkValue, $alias, $userLevel);
        if($this->is_all){
            $ret['where'] = "";
        }
        return $ret;
    }

    function get_branch_group($id){
        $employee = $this->getByID($id,array(),Permission_Value::ADMIN);
        if($employee->type == Permission_Value::ADMIN){
            return $this->load->table_model('branch_group')->getTableMap('','id');
        }

        if($this->db->table_exists($this->tableName. '_branch_group')){
            $result = $this->db->get_where($this->tableName. '_branch_group', array($this->tableName. '_id' => $id))->result();
            return convert_to_array($result,'','branch_group_id');
        }
        return null;
    }

    function get_branch($id){
        $employee = $this->getByID($id,array(),Permission_Value::ADMIN);
        if($employee->type == Permission_Value::ADMIN){
            return $this->load->table_model('branch')->getTableMap('','id');
        }
        if($employee->type == Permission_Value::BRANCH_GROUP){
            return $this->load->table_model('branch')->getTableMap('','id',array('branch_group_d' => $this->get_branch_group($id)));
        }
        if($this->db->table_exists($this->tableName. '_branch')){
            $result = $this->db->get_where($this->tableName. '_branch', array($this->tableName. '_id' => $id))->result();
            return convert_to_array($result,'','branch_id');
        }
        return null;
    }

    function get_assigned_service($employee_id = 0, $ret_array = true){
        if(!$employee_id){
            return null;
        }
        if(array($employee_id) && empty($employee_id)){
            return array();
        }
        $service = $this->load->table_model('employee_service')->get(array('employee_id' => $employee_id));
        $service_detail = $this->load->table_model('item')->get(array('id' => convert_to_array($service,'','item_id')));
        if($ret_array){
            if(is_array($employee_id)){
                return convert_to_array($service,'employee_id','item_id',false);
            }else{
                return convert_to_array($service,'','item_id');
            }
        }else{
            $employee_service_id = convert_to_array($service,'employee_id','item_id',false);
            if(is_array($employee_id)){
                $ret = array();
                foreach($employee_id as $each_id){
                    $ret[$each_id] = array();
                }
                foreach($service_detail as $service){
                    foreach($employee_id as $each_employee_id){
                        if(in_array($service->id,$employee_service_id[$each_employee_id])){
                            $ret[$each_employee_id][$service->id] = $service;
                        }
                    }
                }
                return $ret;
            }else{
                return convert_to_array($service_detail,'id');
            }
        }
    }

    function get_employee_serve_service($service_id, $branchId){
        $result = $this->select(array(
            'select' => array(
                'employee_branch' => array('employee_id')
            ),
            'from' => array(
                'employee_branch' => array('table' => 'employee_branch'),
                'employee_service' => array('table' => 'employee_service', 'condition' => 'employee_branch.employee_id = employee_service.employee_id', 'LEFT')
            ),
            'where' => array(
                'employee_branch.branch_id' => $branchId,
                'employee_service.item_id' => $service_id
            )
        ))->result();
       // $result = $this->load->table_model('employee_service')->get(array('item_id' => $service_id));
        return convert_to_array($result,'','employee_id');
    }

    function getSimpleData(){
        $result = $this->select(array(
            'select' => array(
                'employee' => array('id','first_name')
            ),
            'from' => array(
                'employee' => array('table' => 'employee')
            ),
            'where' => array(
                'status' => 1
            )
        ))->result();
        $result = convert_to_array($result,'id');
        return $result;
    }
}