<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Room_model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "room";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array(
            'code' => array(Constraints::Required, Constraints::Unique),
            'name' => array(Constraints::Required),
            'bed_quantity' => array(Constraints::Required)
        );
        $this->availableStatus = Status::Active;
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array(), $no_permission = false){
        $result = parent::get($condition,$order_by,$limit,$offset,$user_level,$permission,$no_permission);

        $service_map = $this->select(array(
            'select'    => array(
                'item'              => array('id','name'),
                'room'              => array('room_id' => 'id'),
            ),
            'from'      => array(
                'item'              => array('table' => 'item'),
                'category_item'     => array('table' => 'category_item', 'condition' => 'item.id = category_item.item_id'),
                'room_type_category'=> array('table' => 'room_type_category', 'condition' => 'room_type_category.category_id = category_item.category_id'),
                'room'              => array('table' => 'room', 'condition' => 'room.room_type_id = room_type_category.room_type_id')
            ),
            'where'     => array('room.id' => convert_to_array($result,'','id'))
        ))->result();
        $service_map = convert_to_array($service_map,'room_id','',false);

        foreach($result as $row){
            if(isset($service_map[$row->id])){
                $row->services = convert_to_array($service_map[$row->id],'id');
            }else{
                $row->services = array();
            }

        }
        return $result;
    }
}


