<?php

class Deposit_Payment_Item_List_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "deposit_payment_item_list";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }
}