<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Module_Permission_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "module_permission";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }
}