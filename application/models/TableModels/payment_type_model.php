<?php

class Payment_Type_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "payment_type";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

}