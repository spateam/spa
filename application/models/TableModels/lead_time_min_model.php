<?php

class Lead_Time_Min_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "lead_time_min";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

}