<?php

class Branch_Group_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "branch_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function get_branch_and_branch_group($branch_group_id){
        $infoSql = array(
            'select' => array('branch' => array('id', 'name', 'branch_group_id')),
            'from' =>
                array(
                    'branch' => array('table' => 'branch'),
                    'branch_group' => array('table' => 'branch_group', 'condition' => 'branch_group.id = branch.branch_group_id'),
                ),
            'where' => array('branch_group_id' => $branch_group_id)
        );
        $query = $this->select($infoSql);
        if($query !== null){
            return $query->result();
        }
        return array();
    }

    function get_branch_group_by_branch_id($branch_id = array(), $array_id = false){
        $branches = $this->load->table_model('branch')->get(array('id' => $branch_id));
        $branch_group_id = convert_to_array($branches,'','branch_group_id');
        if($array_id){
            return $branch_group_id;
        }else{
            return $this->get(array('branch_group_id' => $branch_group_id));
        }
    }
}

