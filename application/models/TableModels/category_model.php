<?php

/**
 * @author
 * @copyright 2014
 */

class Category_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "category";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = array(
            'name' => array(Constraints::Required, Constraints::Unique),
            'code' => array(Constraints::Required, Constraints::Unique)
        );
        $this->tableType = "main";
    }


    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array()){
        $categories     = parent::get($condition, $order_by, $limit, $offset, $user_level, $permission);
        $category_id_list = convert_to_array($categories,'','id');
        $room_type_map   = $this->select(array(
            'select' => array(
                'room_type_category' => array('category_id'),
                'room_type'          => array('name','code','id'),
            ),
            'from'   => array(
                'room_type_category' => array('table' => 'room_type_category'),
                'room_type'          => array('table' => 'room_type','condition' => 'room_type.id = room_type_category.room_type_id')
            ),
            'where'  => array('room_type_category.category_id' => $category_id_list)
        ))->result();

        $item_map = $this->select(array(
            'select' => array(
                'category_item'      => array('category_id'),
                'item'               => array('name','code','id'),
            ),
            'from'   => array(
                'category_item' => array('table' => 'category_item'),
                'item'          => array('table' => 'item','condition' => 'item.id = category_item.item_id')
            ),
            'where'  => array('category_item.category_id' => $category_id_list)
        ))->result();

        $room_type_map = convert_to_array($room_type_map,'category_id','',false);
        $item_map      = convert_to_array($item_map,'category_id','',false);
        foreach($categories as $category){
            $category->room_types = isset($room_type_map[$category->id])?$room_type_map[$category->id]:array();
            $category->item_list  = isset($item_map[$category->id])?$item_map[$category->id]:array();
        }
        return $categories;

    }

}