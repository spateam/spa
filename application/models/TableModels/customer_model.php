<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */

class Customer_model extends POS_Table_Model
{

    function __construct()
    {
        parent::__construct();
        $this->tableName = "customer";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->fieldListRequirement = array(
            'email'         => array(Constraints::Unique),
            'nric'          => array(Constraints::Unique)
        );
        $this->is_all = true;
    }

    function getListCustID(){
        $listCust = $this->select(array(
            'select' => array(
                'customer' => array('id')
            ),
            'from' => array(
                'customer' => array(
                    'table' => 'customer'
                )
            ),
            'no_permission' => true
        ))->result_array();
        return $listCust;
    }

    function getCustomerAnswers($id){
        $answers = $this->load->table_model('customer_question')->get(array('customer_id' => $id));
        $result = array();
        foreach($answers as $answer){
            $result[$answer->question_id] = $answer->answer;
        }

        return $result;
    }

    function getPackagesItems($customer_id){
        $credit_model = $this->load->table_model('credit');
        $end_date = get_database_date(date('Y-m-d H:i:s'));
        $items = $credit_model->select(
            array(
                'select'    =>  array(
                    'package_item'  => 'item_id',
                ),
                'from'      => array(
                    'credit'    => array('table'    => 'credit'),
                    'package_item'   => array('table'    => 'package_item', 'condition' => 'credit.item_id = package_item.package_id'),
                    'customer'              => array('table' => 'customer', 'condition' => 'credit.customer_id = customer.id')
                ),
                'where'         => array('credit.customer_id' => $customer_id,"credit.status" => $credit_model->availableStatus,"end_date > '$end_date'", 'customer.customer_type' => CUSTOMER_TYPE('Member')),
                'user_level'=> Permission_Value::ADMIN,
            )
        )->result();

        return convert_to_array($items, '', 'item_id');
    }

    function getGiftCardDetail($customer_id){
        $data = $this->select(array(
            'select' => array(
                'giftcard_list_detail' => '*'
            ),
            'from' => array(
                'giftcard_list_detail' => array('table' => 'giftcard_list_detail')
            ),
            'where' => array(
                'customer_id' => $customer_id
            )
        ))->result();
        return $data;
    }

    function getCreditDetail($customer_id){
        $branch_group_id = $this->user_check->get('branch_group_id');
        $credit_model = $this->load->table_model('credit');
        $end_date = get_database_date(date('Y-m-d H:i:s'));
        $packages = $credit_model->select(array(
            'select'        => array(
                'credit'        => '*',
                'credit_branch_group'  => array('branch_group_id' => 'branch_group_id')
            ),
            'from'          => array(
                'credit'                => array('table' => 'credit'),
                'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id'),
                'customer'              => array('table' => 'customer', 'condition' => 'credit.customer_id = customer.id')
            ),
            'user_level'    => Permission_Value::ADMIN,
            'where'         => array('credit.customer_id' => $customer_id,"credit.status" => $credit_model->availableStatus,"end_date > '$end_date'", 'customer.customer_type' => CUSTOMER_TYPE('Member')),
            'group'         => 'credit.id'
        ))->result();
        $branch_group = $this->load->table_model('branch_group')->getTableMap('id','',array('id' => convert_to_array($packages,'','branch_group_id')));
        $packages_id = convert_to_array($packages, '', 'item_id');
        $items = $this->load->table_model('item')->getTableMap('id','',array('id' => $packages_id),true,Permission_Value::ADMIN);
        if(is_array($customer_id)){
            $packages = convert_to_array($packages,'customer_id','',false);
            $customer_package = array();
            foreach($packages as $customer_id=>$package_rows){
                if(! isset($customer_package[$customer_id])){
                    $customer_package[$customer_id] = array();
                }
                foreach($package_rows as $package){
                    $item = $items[$package->item_id];
                    $package->name  = $item->name;
                    if($branch_group_id && $branch_group_id != $branch_group[$package->branch_group_id]->id){
                        $package->name  .=" ({$branch_group[$package->branch_group_id]->name})";
                    }
                    $package->package_id    = $item->id;
                    $package->branch_group  = $branch_group[$package->branch_group_id];
                    $package->items = $this->load->table_model('package_item')->get(array('package_id' => $package->package_id));
                    if(count($package->items) == 0)
                        $package->items = array();
                    else
                        $package->items = convert_to_array($package->items, '', 'item_id');
                    $customer_package[$customer_id][] = $package;
                }
            }
            return $customer_package;
        }else if(is_numeric($customer_id)){
            foreach($packages as $package){
                $item = $items[$package->item_id];
                $package->name  = $item->name;
                if($branch_group_id && $branch_group_id != $branch_group[$package->branch_group_id]->id){
                    $package->name  .=" ({$branch_group[$package->branch_group_id]->name})";
                }
                $package->package_id    = $item->id;
                $package->branch_group  = $branch_group[$package->branch_group_id];
                $package->items = $this->load->table_model('package_item')->get(array('package_id' => $package->package_id));
                if(count($package->items) == 0)
                    $package->items = array();
                else
                    $package->items = convert_to_array($package->items, '', 'item_id');
            }
            return $packages;
        }
    }

    public function get_join_restriction($checkValue = array(), $alias = "", $userLevel = ""){
        $ret = parent::get_join_restriction($checkValue, $alias, $userLevel);
        if($this->is_all){
            $ret['where'] = "";
        }
        return $ret;
    }

    function getByCode($code, $permission = array(), $user_level = ""){
        $query = $this->select(array(
            'select'    => array(
                $this->tableName            => $this->fieldList
            ),
            'from'      => $this->tableName,
            'where'     => array($this->tableName.'.code' => $code),
            'permission' => $permission,
            'user_level' => $user_level
        ));

        if($query !== null){
            return $query->first_row();
        }
        return null;
    }

    function getByID($ID, $permission = array(), $user_level = ""){
        $query = $this->select(array(
            'select'    => array(
                $this->tableName            => $this->fieldList
            ),
            'from'      => $this->tableName,
            'where'     => array($this->tableName.'.id' => $ID),
            'permission' => $permission,
            'user_level' => $user_level
        ));

        if($query !== null){
            return $query->first_row();
        }
        return null;
    }

    function is_guest($id){
        $customer = $this->getByID($id);
        if($customer){
            if($customer->customer_type == CUSTOMER_TYPE('Guest')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function getCustomerBirthdays()
    {
        $this->db->select('customer.id,customer.code,first_name,last_name,email, if(gender=1,"Mr","Ms") as gender, branch_group.name as branch_group_name', false);
        $this->db->from('customer');
        $this->db->join('customer_branch_group', 'customer.id = customer_branch_group.customer_id');
        $this->db->join('branch_group', 'customer_branch_group.branch_group_id = branch_group.id');
        $today = date("Y-m-d");
        $month = date('m', strtotime($today));
        $day = date('d', strtotime($today));
        //$this->db->where('birthday',$today);

        $this->db->where('month(birthday)',$month);
        $this->db->where('day(birthday)',$day);
        $this->db->where(array('email !=' => ''));
        $query = $this->db->get();
        return $query->result();
    }

    function getCustomerExpiryThreeMonths()
    {
        $sql = "SELECT c.id,c.code,c.first_name,c.last_name,c.email, if(c.gender=1,'Mr','Ms') as gender, cd.id as credit_id
                FROM customer c
                    JOIN bill b on b.customer_id = c.id
                    LEFT JOIN bill_item bi on b.id = bi.bill_id
                    LEFT JOIN credit cd on cd.id = bi.credit_id
                WHERE DATEDIFF(cd.end_date,NOW()) = 90
                    AND c.email != ''
                    AND cd.status = 1
                    AND cd.credit > 0
                    AND cd.email_three_months = 0
                group by c.email";
        $query = $this->db->query($sql);
        $result= $query->result();
        return $result;
    }

    function getCustomerExpiryOneMonth()
    {
        $sql = "SELECT c.id,c.code,c.first_name,c.last_name,c.email, if(c.gender=1,'Mr','Ms') as gender, cd.id as credit_id
                FROM customer c
                    JOIN bill b on b.customer_id = c.id
                    LEFT JOIN bill_item bi on b.id = bi.bill_id
                    LEFT JOIN credit cd on cd.id = bi.credit_id
                WHERE DATEDIFF(cd.end_date,NOW()) = 30
                    AND c.email != ''
                    AND cd.status = 1
                    AND cd.credit > 0
                    AND cd.email_one_month = 0
                group by c.email";
        $query = $this->db->query($sql);
        $result= $query->result();
        return $result;
    }

    function sendEmailInviteReview()
    {
        $sql = 'SELECT bill.id as bid, bill.created_date, customer.id as cid ,customer.first_name, customer.last_name, customer.email, customer.recovery_email, branch_group.name as branch_group_name,branch.id as branch_id, branch.name as branch_name
                FROM bill LEFT JOIN customer ON bill.customer_id = customer.id
                          LEFT JOIN bill_branch_group ON bill.id = bill_branch_group.bill_id
                          LEFT JOIN branch_group ON bill_branch_group.branch_group_id = branch_group.id
                          LEFT JOIN bill_branch ON bill.id = bill_branch.bill_id
                          LEFT JOIN branch ON bill_branch.branch_id = branch.id
                WHERE bill.email_review = 0
                AND DATEDIFF(DATE(NOW()),DATE(bill.created_date)) IN (1,2,3)
                AND bill.status = 1
                AND customer.email != ""';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getCustomerListReminder12(){
        $dateTime = new DateTimeZone($this->system_config->get('timezone'));
        $timezoneFormat = ($dateTime->getOffset(new DateTime('GMT')))/60;
        if($timezoneFormat > 0){
            $timezoneFormat = '+' . str_pad((int)($timezoneFormat/60),2,0,STR_PAD_LEFT) . ':' . str_pad(($timezoneFormat%60),2,0,STR_PAD_LEFT);
        }
        else{
            $timezoneFormat = '-' . str_pad((int)($timezoneFormat/60),2,0,STR_PAD_LEFT) . ':' . str_pad(($timezoneFormat%60),2,0,STR_PAD_LEFT);
        }
        $getDbDate = get_database_date();//  get_user_date(get_database_date(), $this->system_config->get('timezone'),'Y-m-d H:i:s');

        define('TIME_REMIND', 11);

        $cust = $this->db->select('booking.id as booking_id,c.id,c.first_name,c.last_name,c.email,
                                   branch.id as branch_id,branch.name as branch_name, 
                                   customer.first_name as customer_name, customer.mobile_number,
                                   item.name as service_name, booking_service.start_time, booking_service.end_time,
                                   TIMESTAMPDIFF(MINUTE,booking_service.start_time,booking_service.end_time) as duration', FALSE)
                         ->join('booking_service', 'booking.id = booking_service.booking_id', 'LEFT')
                         ->join('customer c', 'booking.customer_id = c.id', 'LEFT')
                         ->join('item','booking_service.service_id = item.id', 'LEFT')
                         ->join('branch','booking.branch_id = branch.id','LEFT')
                         ->where(array(
                             'booking.status' => 1,
                             'c.email !=' => '',
                             'booking_service.start_time >' => $getDbDate,
                             'booking.email_12' => 0,
                         ))
                         ->where('HOUR(TIMEDIFF(convert_tz(booking_service.start_time,"+00:00","'.$timezoneFormat.'"), "'.$getDbDate.'")) = ' . TIME_REMIND)
                         ->get('booking')->result();
        $custTemp = $this->db->select('booking.id as booking_id,c.id,c.first_name,c.last_name,c.email,
                                       branch.id as branch_id,branch.name as branch_name, 
                                       customer.first_name as customer_name, customer.mobile_number,
                                       item.name as service_name, booking_service.start_time, booking_service.end_time,
                                       TIMESTAMPDIFF(MINUTE,booking_service.start_time,booking_service.end_time) as duration',FALSE)
                         ->join('customer_temp c', 'booking.customer_id = c.id', 'LEFT')
                         ->join('booking_service', 'booking.id = booking_service.booking_id', 'LEFT')
                         ->join('item','booking_service.service_id = item.id', 'LEFT')
                         ->join('branch','booking.branch_id = branch.id','LEFT')
                         ->where(array(
                             'booking.status' => 1,
                             'c.email !=' => '',
                             'booking_service.start_time >' => $getDbDate,
                             'booking.email_12' => 0,
                         ))
                        ->where('HOUR(TIMEDIFF(convert_tz(booking_service.start_time,"+00:00","'.$timezoneFormat.'"), "'.$getDbDate.'")) = ' . TIME_REMIND)
                         ->get('booking')->result();
        $listCust = array_merge($cust,$custTemp);
        return $listCust;
    }

    function getCustomerSMSReminder(){
        $this->db->_protect_identifiers=false;

        $query_1 = 'SELECT booking.id, booking.customer_id, booking.status
                    FROM (booking) LEFT JOIN booking_service ON booking.id = booking_service.booking_id
                    WHERE DATEDIFF(DATE(NOW()), DATE(booking_service.start_time)) IN (- 1)
                        AND booking.sms_reminder = 0
                        AND booking.status NOT IN (0 , 4, 5, 6, 7, 8, 9, 3)';
        $query_1 = $this->db->query($query_1);


        $listBooking = $query_1->result();

        $custSearch = array();
        $custTempSearch = array();
        $bookingArr = array();
        foreach($listBooking as $item){
            $bookingArr[] = $item->id;
            if($item->status == BOOKING_STATUS('Complete')) {
                $custSearch[] = $item->customer_id;
            }
            elseif($item->status == BOOKING_STATUS('Hold')){
                $custTempSearch[] = $item->customer_id;
            }
        }

        if(!empty($custSearch)) {
            $custSearch = 'SELECT booking.id, branch.id AS branch_id, branch.name AS branch_name, customer.first_name AS customer_name,
                                  customer.mobile_number, item.name AS service_name, start_time, end_time, TIMESTAMPDIFF(MINUTE,start_time, end_time) AS duration
                        FROM (booking) LEFT JOIN customer ON booking.customer_id = customer.id
                                       LEFT JOIN booking_service ON booking.id = booking_service.booking_id
                                       LEFT JOIN item ON booking_service.service_id = item.id
                                       LEFT JOIN branch ON booking.branch_id = branch.id
                        WHERE  customer.mobile_number IS NOT NULL
                               AND customer.mobile_number != ""
                               AND CHAR_LENGTH(customer.mobile_number) = 10
                               AND booking.id in (' . implode(',', $bookingArr) . ')
                               AND customer.id in (' . implode(',',$custSearch) . ')';
            $custSearch = $this->db->query($custSearch);
            $custSearch = $custSearch->result();
        }
        if(!empty($custTempSearch)){
            $custTempSearch = 'SELECT booking.id, branch.id AS branch_id, branch.name AS branch_name, customer_temp.first_name AS customer_name,
                                  customer_temp.mobile_number, item.name AS service_name, start_time, end_time, TIMESTAMPDIFF(MINUTE,start_time, end_time) AS duration
                        FROM (booking) LEFT JOIN customer_temp ON booking.customer_id = customer_temp.id
                                       LEFT JOIN booking_service ON booking.id = booking_service.booking_id
                                       LEFT JOIN item ON booking_service.service_id = item.id
                                       LEFT JOIN branch ON booking.branch_id = branch.id
                        WHERE customer_temp.mobile_number IS NOT NULL
                               AND customer_temp.mobile_number != ""
                               AND CHAR_LENGTH(customer_temp.mobile_number) = 10
                               AND booking.id in (' . implode(',', $bookingArr) . ')
                               AND customer_temp.id in (' . implode(',',$custTempSearch) . ')';
            $custTempSearch = $this->db->query($custTempSearch);
            $custTempSearch = $custTempSearch->result();
        }

        return array_merge($custSearch,$custTempSearch);
    }

    function validationCustomer($validFields){
        // check mobile number empty
        if(trim($validFields->mobile_number) == ''){
            return array('status' => FALSE, 'message' => 'Mobile Number should be not empty');
        }
        // check mobile number is number only or not
        if(!is_numeric(trim($validFields->mobile_number))){
            return array('status' => FALSE, 'message' => 'Mobile Number should only contain number.');
        }

        if(isset($validFields->code) && $validFields->code != ''){
            $currentCust = $this->db->get_where('customer', array('code' => trim($validFields->code)))->row_array();
            if(trim($validFields->email) != $currentCust['email']){
                $customers = $this->db->get_where('customer', array('email' => trim($validFields->email)))->result_array();
                if(count($customers) > 0){
                    return array('status' => FALSE, 'message' => 'The Email address already exists in system');
                }
            }
            if(trim($validFields->mobile_number) != $currentCust['mobile_number']){
                $customers = $this->db->get_where('customer', array('mobile_number' => trim($validFields->mobile_number)))->result_array();
                if(count($customers) > 0) {
                    return array('status' => FALSE, 'message' => 'The Mobile Number already exists in system.');
                }
            }
        }
        else{
            // check email
            if($validFields->email != ''){
                $customers = $this->db->get_where('customer', array('email' => trim($validFields->email)))->result_array();
                if(count($customers) > 0){
                //    return array('status' => FALSE, 'message' => 'The Email address already exists in system');
                }
            }

            // check mobile number dupliate
            $customers = $this->db->get_where('customer', array('mobile_number' => trim($validFields->mobile_number)))->result_array();
            if(count($customers) > 0) {
            //    return array('status' => FALSE, 'message' => 'The Mobile Number already exists in system.');
            }
        }
        return array('status' => TRUE);
    }

    function importBatchCustomer($importData){
        $result = $this->db->insert_batch('customer',$importData);
        return $result;
    }
}