<?php

class Customer_Reviews_model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "customer_reviews";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array();
        $this->tableType = "main";
    }

    function getData(){
        return $this->select(array(
            'select' => array(
                'customer_reviews' => array('id','createAt', 'review_author', 'review_title', 'review_body', 'review_rating'),
                'bill'  => array('bill_code' => 'code')
            ),
            'from' => array(
                'customer_reviews' => array('table' => 'customer_reviews'),
                'bill' => array('table' => 'bill', 'condition' => 'customer_reviews.bill_id = bill.id', 'type' => 'LEFT')
            ),
            'no_permission' => true
        ))->result_array();
    }
}