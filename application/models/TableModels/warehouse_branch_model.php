<?php

/**
 * @author
 * @copyright 2014
 */

class Warehouse_Branch_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "warehouse_branch";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}
