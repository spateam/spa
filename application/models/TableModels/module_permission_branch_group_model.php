<?php

class Module_Permission_Branch_Group_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "module_permission_branch_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }
}