<?php

class Commission_Item_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "commission_item";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function generateBaseInfo($condition = array()){
        $info = parent::generateBaseInfo($condition);
        $info['select'] = array(
            $this->tableName => $this->fieldList,
            'item' => array('item_id' => 'id','item_name' => 'name','item_type' => 'type','item_price' => 'price'),
            'commission' => array('commission_name' => 'name')
        );
        $info['from'] = array(
            $this->tableName => array('table' => $this->tableName),
            'item' => array('table' => 'item', 'condition' => "item.id = {$this->tableName}.item_id"),
            'commission' => array('table' => 'commission', 'condition' => "commission.id = {$this->tableName}.commission_id")
        );
        return $info;
    }

    function get($condition = array(), $order_by = "", $limit = "", $offset = "", $user_level = "", $permission = array(), $no_permission = false){
        $result = parent::get($condition,$order_by,$limit,$offset,$user_level,$permission,$no_permission);

        $bundle_map = $this->select(array(
            'select' => array(
                'item_bundle'       => array('parent_item_id' => 'item_id'),
                'item_bundle_item'  => array('commission_value'),
                'item'              => array('id','name','price','code','description','type')
            ),
            'from'   => array(
                'item_bundle'           => array('table' => 'item_bundle'),
                'item_bundle_item'      => array('table' => 'item_bundle_item', 'condition' => 'item_bundle.item_id = item_bundle_item.item_id'),
                'item'                  => array('table' => 'item', 'condition' => 'item.id = item_bundle_item.sub_item_id')
            ),
            'where'  => array('item_bundle.item_id' => convert_to_array($result,'','item_id'))
        ))->result();
        $bundle_map = convert_to_array($bundle_map,'parent_item_id','',false);

        foreach($result as $row){
            if($row->item_type == ITEM_TYPE('Bundle')){
                if($bundle_map[$row->item_id]){
                    $row->sub_item_commission  = $bundle_map[$row->item_id];
                }
            }
        }
        return $result;
    }
}