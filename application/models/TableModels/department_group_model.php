<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Department_Group_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "department_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function get_group($condition){
        $data = $this->select(array(
            'select' => 'group_id',
            'from' => $this->tableName,
            'where' => $condition
        ))->result();

        $result = array();

        foreach($data as $row){
            $result[] = $row->group_id;
        }
        return $result;
    }
}