<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/30/2014
 * Time: 4:46 PM
 */


class Credit_Adjustment_Log_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "credit_adjustment_log";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connective";
        $this->fieldListRequirement = array();
        $this->availableStatus = null;
    }

    function get_detail($condition = array(),$order_by = ''){
        $result = $this->select(array(
            'select' => array(
                'credit'                => array('id','credit','credit_original','item_id','customer_id'),
                'credit_adjustment_log' => array('log_time','credit_change','credit_value_before','credit_id_transfer'),
            ),
            'from' => array(
                'credit'                => array('table' => 'credit'),
                'credit_adjustment_log' => array('table' => 'credit_adjustment_log','condition' => 'credit.id = credit_adjustment_log.credit_id')
            ),
            'where' => $condition,
            'order' => $order_by
        ))->result();
        return $result;
    }

    function write_transfer_log($credit_log_data){
        $to     = $credit_log_data['to_credit'];
        $from   = $credit_log_data['from_credit'];
        $this->insert($credit_log_data['to_credit']);
        $this->insert($credit_log_data['from_credit']);
        return true;
    }
}


