<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Employee_Calendar_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "employee_calendar";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array();
        $this->tableType = "connected";
    }
}