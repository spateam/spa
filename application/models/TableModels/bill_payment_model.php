<?php

class Bill_Payment_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "bill_payment";
        $this->fieldList = $this->db->list_fields($this->tableName);

        $this->fieldListRequirement = (object) array(
            'bill_id'           => array(Constraints::Required),
            'payment_id'        => array(Constraints::Required),
            'amount'            => array(Constraints::Required)
        );
        $this->tableType = "connected";
    }

}