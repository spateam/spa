<?php

/**
 * @author
 * @copyright 2014
 */

class Warehouse_Branch_group_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "warehouse_branch_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}
