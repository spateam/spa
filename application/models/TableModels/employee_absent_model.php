<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Employee_Absent_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "employee_absent";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array();
        $this->tableType = "connected";
    }

    function get_absent_range($params){
        $default_value = array(
            'start_time' => '1970-01-01 00:00:00',
            'end_time' => '2100-12-30 23:59:59',
            'condition'   => array(
                'employee_id' => array()
            ),
            'permission'  => array(
                'user_level' => '',
                'field'      => '',
            )
        );
        $params = $params + $default_value;
        if(isset($params['permission']['field']['branch_id'])){
//            $params['condition']['branch_id'] = $params['permission']['field']['branch_id'];
        }
        $result = $this->select(array(
            'select' => array('employee_absent' => array('employee_id','id')),
            'from'   => array('employee_absent' => array('table' => 'employee_absent')),
            'where'  => array_merge(array("((employee_absent.start_time <= '{$params['start_time']}' AND employee_absent.end_time > '{$params['start_time']}')
            OR (employee_absent.start_time > '{$params['start_time']}' AND employee_absent.start_time < '{$params['end_time']}}' AND employee_absent.end_time > '{$params['start_time']}'))"),$params['condition']),
        ))->result();
        return $result;
    }
}