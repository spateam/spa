<?php

class Menu_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "menu";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

    function getMenuToShow($condition = array()){
        $return_data = array();
        $parent_condition = array(
            array('menu.module_permission_id = 0 OR menu.module_permission_id is NULL')
        );
        $new_condition = array();
        if(isset($condition['type'])){
            $parent_condition['type'] = $condition['type'];
            $new_condition = array('module.type' => $condition['type'], 'menu.type' => $condition['type']);
        }
        $info_group = array(
            'select'    =>  array(
                'menu'      => array('id','title','link','parent')
            ),
            'from'      =>  'menu',
            'where'     =>  $parent_condition,
            'order'     =>  'menu.ordering asc'
        );
        $query_group = $this->select($info_group);
        if($query_group !== null){
            $return_data = $query_group->result();
        }
        $info = array(
            'select'    =>  array(
                'menu'      => array('id','title','link','parent')
            ),
            'from'      =>  array(
                'menu'              =>  array('table'   =>  'menu'),
                'module_permission' =>  array('table'   =>  'module_permission','condition' => "menu.module_permission_id = module_permission.id", 'type' => 'left'),
                'module'            =>  array('table'   =>  'module','condition' => 'module.id = module_permission.module_id')
            ),
            'where'     =>  $new_condition,
            'order'     =>  'menu.ordering asc'
        );
        $query = $this->select($info);
        if($query !== null){
            $return_data = array_merge($return_data, $query->result());
        }
        return $return_data;
    }
}


