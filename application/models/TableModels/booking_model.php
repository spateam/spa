<?php

class Booking_model extends POS_Table_Model
{

    function __construct()
    {
        parent::__construct();
        $this->tableName = "booking";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object)array();
        $this->tableType = "main";
    }


    function get_detail_scheduler($condition = array())
    {
        // get booking information for booking
        $result = array();

        if (isset($condition['filter_date'])) {
            $tar_date = $condition['filter_date'];
            unset($condition['filter_date']);
            $condition = $this->to_condition($condition, 'booking');
            $condition['DATE(booking_service.start_time) <'] = $tar_date;
            $condition['DATE(booking_service.end_time) >'] = $tar_date;
        } else {
            $condition = $this->to_condition($condition, 'booking');
        }

        $source = $this->select(array(
            'select' => array(
                'booking' => array('id', 'code', 'comment', 'reserve_comment', 'bundle_id', 'customer_id', 'created_date', 'updated_date', 'status', 'type', 'no_preference', 'bill_id', 'is_new', 'bill_id', 'old_status','is_bypass'),
                'booking_recurring' => array('repeat_times', 'repeat_on', 'repeat_to', 'repeat_to_data', 'recurring_type'),
                'branch' => array('branch_name' => 'name')
            ),
            'from' => array(
                'booking' => array('table' => 'booking'),
                'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id'),
                'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id = branch.id'),
                'booking_recurring' => array('table' => 'booking_recurring', 'condition' => 'booking.id=booking_recurring.booking_id', 'type' => 'LEFT')
            ),
            'no_permission' => true,
            'where' => $condition
        ))->result();

        $result = $source;

        $customerTempKey = $this->select(array(
            'select' => array('customer_temp' => array('id','first_name', 'last_name', 'email', 'mobile_number')),
            'from' => array('customer_temp' => array('table' => 'customer_temp'))
        ))->result_array();
        $temp = array();
        foreach($customerTempKey as $item){
            $temp[$item['id']] = $item;
        }
        $customerTempKey = $temp;

        $customerKey = $this->select(array(
            'select' => array('customer' => array('id','first_name', 'last_name', 'email', 'mobile_number')),
            'from' => array('customer' => array('table' => 'customer'))
        ))->result_array();
        $temp = array();
        foreach($customerKey as $item){
            $temp[$item['id']] = $item;
        }
        $customerKey = $temp;

        foreach($result as $key => $item){
            if($item->status == BOOKING_STATUS('Hold') && isset($customerTempKey[$item->customer_id])){
                $result[$key]->customer_first_name = $customerTempKey[$item->customer_id]['first_name'];
                $result[$key]->customer_customer_name = $customerTempKey[$item->customer_id]['last_name'];
                $result[$key]->email = $customerTempKey[$item->customer_id]['email'];
                $result[$key]->customer_mobile_number = $customerTempKey[$item->customer_id]['mobile_number'];
            }
            elseif($item->status != BOOKING_STATUS('Hold') && isset($customerKey[$item->customer_id])){
                $result[$key]->customer_first_name = $customerKey[$item->customer_id]['first_name'];
                $result[$key]->customer_customer_name = $customerKey[$item->customer_id]['last_name'];
                $result[$key]->email = $customerKey[$item->customer_id]['email'];
                $result[$key]->customer_mobile_number = $customerKey[$item->customer_id]['mobile_number'];
            }
        }

        foreach ($result as $key => $item) {
            if ($item->bill_id != '' && $item->bill_id != 0) {
                $result[$key]->bill_code = $this->select(array(
                    'select' => array('bill' => array('bill_code' => 'code')),
                    'from' => array('bill' => array('table' => 'bill') ),
                    'where' => array('bill.id' => $item->bill_id )
                ))->result();
                if (count($item->bill_code) > 0) {
                    $result[$key]->bill_code = $result[$key]->bill_code[0]->bill_code;
                } else {
                    $result[$key]->bill_code = '';
                }
            }
        }

        $booking_service_ids = array();
        $bundle_ids = array();

        foreach ($result as $row) {
            $booking_service_ids[] = $row->id;
            if (isset($row->bundle_id) && $row->bundle_id) {
                $bundle_ids[] = $row->bundle_id;
            }
        }

        $booking_service_map = $this->select(array(
            'select' => array(
                'booking_service' => array('booking_id', 'start_time', 'end_time'),
                'room' => array('room_id' => 'id', 'room_name' => 'name'),
                'employee' => array('employee_id' => 'id', ' employee_first_name' => 'first_name', 'employee_last_name' => 'last_name'),
                'item' => array('service_id' => 'id', 'item_name' => 'name', 'item_type' => 'type')
            ),
            'from' => array(
                'booking_service' => array('table' => 'booking_service'),
                'room' => array('table' => 'room', 'condition' => 'room.id = booking_service.room_id'),
                'employee' => array('table' => 'employee', 'condition' => 'employee.id = booking_service.employee_id'),
                'item' => array('table' => 'item', 'condition' => 'item.id = booking_service.service_id')
            ),
            'where' => array_merge($this->to_condition($condition, 'booking_service'), array('booking_id' => $booking_service_ids))
        ))->result();
        $bundle_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $bundle_ids));

        if (count($booking_service_map) == 0) return array();
        $booking_service_map = convert_to_array($booking_service_map, 'booking_id', '', false);
        foreach ($result as $key => $row) {
            if (!isset($booking_service_map[$row->id]) || !count($booking_service_map[$row->id])) {
                unset($result[$key]);
                continue;
            }
            if ($row->bundle_id) {
                $row->services = array();
                $start_time = '9999-12-30 23:59:59';
                foreach ($booking_service_map[$row->id] as $booking_service) {
                    $row->services[] = (object)array(
                        'room_name' => $booking_service->room_name,
                        'room_id' => $booking_service->room_id,
                        'employee_id' => $booking_service->employee_id,
                        'employee_first_name' => $booking_service->employee_first_name,
                        'employee_last_name' => $booking_service->employee_last_name,
                        'service_id' => $booking_service->service_id,
                        'item_name' => $booking_service->item_name,
                        'start_time' => $booking_service->start_time,
                        'end_time' => $booking_service->end_time
                    );
                    if ($booking_service->start_time < $start_time) {
                        $start_time = $booking_service->start_time;
                    }
                }
                $row->start_time = $start_time;
                $row->bundle = $bundle_map[$row->bundle_id];
            } else {
                $booking_service = $booking_service_map[$row->id][0];
                $row->room_name = $booking_service->room_name;
                $row->room_id = $booking_service->room_id;
                $row->employee_id = $booking_service->employee_id;
                $row->employee_first_name = $booking_service->employee_first_name;
                $row->employee_last_name = $booking_service->employee_last_name;
                $row->service_id = $booking_service->service_id;
                $row->item_name = $booking_service->item_name;
                $row->start_time = $booking_service->start_time;
                $row->end_time = $booking_service->end_time;
            }
        }
        return $result;
    }

    function get_detail($condition = array(), $type = 2, $new_booking = 0)
    {
        // get booking information for booking
        $result = array();
        if ($type == 1) {
            $logged_user = $this->session->userdata('login');

            if(!$logged_user){
                $table = 'customer_temp';
            }
            else{
                $table = 'customer';
            }
            $result = $this->select(array(
                'select' => array(
                    'booking' => array('id', 'code', 'comment', 'reserve_comment', 'bundle_id', 'customer_id', 'created_date', 'updated_date', 'status', 'type', 'no_preference', 'is_new', 'bill_id', 'old_status','is_bypass'),
                    'booking_recurring' => array('repeat_times', 'repeat_on', 'repeat_to', 'repeat_to_data', 'recurring_type'),
                    $table => array('customer_first_name' => 'first_name', 'customer_last_name' => 'last_name', 'email' => 'email', 'customer_mobile_number' => 'mobile_number'),
                    'branch' => array('branch_name' => 'name')
                ),
                'from' => array(
                    'booking' => array('table' => 'booking'),
                    $table => array('table' => $table, 'condition' => $table.'.id = booking.customer_id'),
                    'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id = branch.id'),
                    'booking_recurring' => array('table' => 'booking_recurring', 'condition' => 'booking.id=booking_recurring.booking_id', 'type' => 'LEFT')
                ),
                'where' => $this->to_condition($condition, 'booking')
            ))->result();

        } // get booking information for scheduler
        else if ($type == 2) {
            if (isset($condition['filter_date']) && $new_booking == 0) {
                $tar_date = $condition['filter_date'];
                unset($condition['filter_date']);
                $condition = $this->to_condition($condition, 'booking');
                $condition['DATE(booking_service.start_time) <'] = $tar_date;
                $condition['DATE(booking_service.end_time) >'] = $tar_date;
            } else {
                $condition = $this->to_condition($condition, 'booking');
            }

            $source = $this->select(array(
                'select' => array(
                    'booking' => array('id', 'code', 'comment', 'reserve_comment', 'bundle_id', 'customer_id', 'created_date', 'updated_date', 'status', 'type', 'no_preference', 'bill_id', 'is_new', 'bill_id', 'old_status','is_bypass'),
                    'booking_recurring' => array('repeat_times', 'repeat_on', 'repeat_to', 'repeat_to_data', 'recurring_type'),
                    'branch' => array('branch_name' => 'name')
                ),
                'from' => array(
                    'booking' => array('table' => 'booking'),
                    'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id'),
                    'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id = branch.id'),
                    'booking_recurring' => array('table' => 'booking_recurring', 'condition' => 'booking.id=booking_recurring.booking_id', 'type' => 'LEFT')
                ),
                'no_permission' => true,
                'where' => $condition
            ))->result();

            $result = $source;

            $customerKey = $this->select(array(
                'select' => array('customer' => array('id','first_name', 'last_name', 'email', 'mobile_number')),
                'from' => array('customer' => array('table' => 'customer_temp'))
            ))->result_array();
            $temp = array();
            foreach($customerKey as $item){
                $temp[$item['id']] = $item;
            }
            $customerKey = $temp;

            foreach($result as $key => $item){
                if(isset($customerKey[$item->customer_id])){
                    $result[$key]->customer_first_name = $customerKey[$item->customer_id]['first_name'];
                    $result[$key]->customer_customer_name = $customerKey[$item->customer_id]['last_name'];
                    $result[$key]->email = $customerKey[$item->customer_id]['email'];
                    $result[$key]->customer_mobile_number = $customerKey[$item->customer_id]['mobile_number'];
                }
            }

            if (!$new_booking && count($result) > 1) {
                $list_exept = array();
                foreach ($result as $item) {
                    $list_exept[] = $item->id;
                }

                $result2 = $this->select(array(
                    'select' => array(
                        'booking' => array('id', 'code', 'comment', 'reserve_comment', 'bundle_id', 'customer_id', 'created_date', 'updated_date', 'status', 'type', 'no_preference', 'bill_id', 'is_new', 'bill_id', 'old_status','is_bypass'),
                        'booking_recurring' => array('repeat_times', 'repeat_on', 'repeat_to', 'repeat_to_data', 'recurring_type'),
                        'branch' => array('branch_name' => 'name')
                    ),
                    'from' => array(
                        'booking' => array('table' => 'booking'),
                        'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id'),
                        'branch' => array('table' => 'branch', 'condition' => 'booking.branch_id = branch.id'),
                        'booking_recurring' => array('table' => 'booking_recurring', 'condition' => 'booking.id=booking_recurring.booking_id', 'type' => 'LEFT')
                    ),
                    'no_permission' => true,
                    'where' => $condition
                ))->result();

                $customerKey = $this->select(array(
                    'select' => array('customer' => array('id','first_name', 'last_name', 'email', 'mobile_number')),
                    'from' => array('customer' => array('table' => 'customer'))
                ))->result_array();
                $temp = array();
                foreach($customerKey as $item){
                    $temp[$item['id']] = $item;
                }
                $customerKey = $temp;

                foreach($result2 as $key => $item){
                    if(isset($customerKey[$item->customer_id])){
                        $result2[$key]->customer_first_name = $customerKey[$item->customer_id]['first_name'];
                        $result2[$key]->customer_customer_name = $customerKey[$item->customer_id]['last_name'];
                        $result2[$key]->email = $customerKey[$item->customer_id]['email'];
                        $result2[$key]->customer_mobile_number = $customerKey[$item->customer_id]['mobile_number'];
                    }
                }
                foreach ($result2 as $item) {
                    if (!isset($list_exept[$item->id])) {
                        $result[] = $item;
                    }
                }
            }

            foreach ($result as $key => $item) {
                if ($item->bill_id != '' && $item->bill_id != 0) {
                    $result[$key]->bill_code = $this->select(array(
                        'select' => array('bill' => array('bill_code' => 'code')),
                        'from' => array('bill' => array('table' => 'bill') ),
                        'where' => array('bill.id' => $item->bill_id )
                    ))->result();
                    if (count($item->bill_code) > 0) {
                        $result[$key]->bill_code = $result[$key]->bill_code[0]->bill_code;
                    } else {
                        $result[$key]->bill_code = '';
                    }
                }
            }
        }

        $booking_service_ids = array();
        $bundle_ids = array();

        foreach ($result as $row) {
            $booking_service_ids[] = $row->id;
            if (isset($row->bundle_id) && $row->bundle_id) {
                $bundle_ids[] = $row->bundle_id;
            }
        }

        $booking_service_map = $this->select(array(
            'select' => array(
                'booking_service' => array('booking_id', 'start_time', 'end_time'),
                'room' => array('room_id' => 'id', 'room_name' => 'name'),
                'employee' => array('employee_id' => 'id', ' employee_first_name' => 'first_name', 'employee_last_name' => 'last_name'),
                'item' => array('service_id' => 'id', 'item_name' => 'name', 'item_type' => 'type')
            ),
            'from' => array(
                'booking_service' => array('table' => 'booking_service'),
                'room' => array('table' => 'room', 'condition' => 'room.id = booking_service.room_id'),
                'employee' => array('table' => 'employee', 'condition' => 'employee.id = booking_service.employee_id'),
                'item' => array('table' => 'item', 'condition' => 'item.id = booking_service.service_id')
            ),
            'where' => array_merge($this->to_condition($condition, 'booking_service'), array('booking_id' => $booking_service_ids))
        ))->result();

        $bundle_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $bundle_ids));

        if (count($booking_service_map) == 0) return array();
        $booking_service_map = convert_to_array($booking_service_map, 'booking_id', '', false);
        foreach ($result as $key => $row) {
            if (!isset($booking_service_map[$row->id]) || !count($booking_service_map[$row->id])) {
                unset($result[$key]);
                continue;
            }
            if ($row->bundle_id) {
                $row->services = array();
                $start_time = '9999-12-30 23:59:59';
                foreach ($booking_service_map[$row->id] as $booking_service) {
                    $row->services[] = (object)array(
                        'room_name' => $booking_service->room_name,
                        'room_id' => $booking_service->room_id,
                        'employee_id' => $booking_service->employee_id,
                        'employee_first_name' => $booking_service->employee_first_name,
                        'employee_last_name' => $booking_service->employee_last_name,
                        'service_id' => $booking_service->service_id,
                        'item_name' => $booking_service->item_name,
                        'start_time' => $booking_service->start_time,
                        'end_time' => $booking_service->end_time
                    );
                    if ($booking_service->start_time < $start_time) {
                        $start_time = $booking_service->start_time;
                    }
                }
                $row->start_time = $start_time;
                $row->bundle = $bundle_map[$row->bundle_id];
            } else {
                $booking_service = $booking_service_map[$row->id][0];
                $row->room_name = $booking_service->room_name;
                $row->room_id = $booking_service->room_id;
                $row->employee_id = $booking_service->employee_id;
                $row->employee_first_name = $booking_service->employee_first_name;
                $row->employee_last_name = $booking_service->employee_last_name;
                $row->service_id = $booking_service->service_id;
                $row->item_name = $booking_service->item_name;
                $row->start_time = $booking_service->start_time;
                $row->end_time = $booking_service->end_time;
            }
        }
        return $result;
    }

    function get_detail_for_employee($condition = array(), $type = 2)
    {
        $customer = $this->select(array(
            'select' => array('customer' => array('id','customer_first_name' => 'first_name', 'customer_last_name' => 'last_name', 'email' => 'email', 'customer_mobile_number' => 'mobile_number')),
            'from' => array('customer' => array('table' => 'customer')),
            'no_permission' => true
        ))->result();
        $temp = array();
        foreach($customer as $item){ $temp[$item->id] = $item; }
        $customer = $temp;

        $customer_temp = $this->select(array(
            'select' => array('customer_temp' => array('id','customer_first_name' => 'first_name', 'customer_last_name' => 'last_name', 'email' => 'email', 'customer_mobile_number' => 'mobile_number')),
            'from' => array('customer_temp' => array('table' => 'customer_temp')),
            'no_permission' => true
        ))->result();
        $temp = array();
        foreach($customer_temp as $item){ $temp[$item->id] = $item; }
        $customer_temp = $temp;

        $branch = $this->select(array(
            'select' => array('branch' => array('id','branch_name' =>'name')),
            'from' => array('branch' => array('table' => 'branch')),
            'no_permission' => true
        ))->result();
        $temp = array();
        foreach($branch as $item){ $temp[$item->id] = $item->branch_name; }
        $branch = $temp;

        $result = array();
        if ($type == 1) {

        }
        else if ($type == 2) {
            $week_start = date('Y-m-d h:i:s', strtotime('monday this week',strtotime($condition['filter_date'])) +" 00:00:00");
            $week_end = date('Y-m-d h:i:s', strtotime('sunday this week',strtotime($condition['filter_date'])) + " 23:59:59");
            $condition['booking_service']['start_time >'] = $week_start;
            $condition['booking_service']['end_time <'] = $week_end;

            $result = $this->select(array(
                'select' => array(
                    'booking' => array('id', 'code', 'comment', 'reserve_comment','branch_id', 'bundle_id', 'customer_id', 'created_date', 'updated_date', 'status', 'type', 'no_preference', 'bill_id','is_bypass'),
                    'booking_recurring' => array('repeat_times', 'repeat_on', 'repeat_to', 'repeat_to_data', 'recurring_type'),
                ),
                'from' => array(
                    'booking' => array('table' => 'booking'),
                    'booking_service' => array('table' => 'booking_service', 'condition' => 'booking.id = booking_service.booking_id', 'type' => 'LEFT'),
                    'booking_recurring' => array('table' => 'booking_recurring', 'condition' => 'booking.id=booking_recurring.booking_id', 'type' => 'LEFT')
                ),
                'where' => array_merge($this->to_condition($condition, 'booking'),$this->to_condition($condition, 'booking_service'))
            ))->result();

            foreach($result as $key => $item){
                if($item->status == 2){
                    $result[$key]->customer_first_name = $customer_temp[$item->customer_id]->customer_first_name;
                    $result[$key]->customer_last_name = $customer_temp[$item->customer_id]->customer_last_name;
                    $result[$key]->email = $customer_temp[$item->customer_id]->email;
                    $result[$key]->customer_mobile_number = $customer_temp[$item->customer_id]->customer_mobile_number;
                }
                else{
                    $result[$key]->customer_first_name = $customer[$item->customer_id]->customer_first_name;
                    $result[$key]->customer_last_name  = $customer[$item->customer_id]->customer_last_name;
                    $result[$key]->email               = $customer[$item->customer_id]->email;
                    $result[$key]->customer_mobile_number = $customer[$item->customer_id]->customer_mobile_number;
                }
            }
        }

        $booking_service_ids = array();
        $bundle_ids = array();
        foreach ($result as $row) {
            $booking_service_ids[] = $row->id;
            if (isset($row->bundle_id) && $row->bundle_id) {
                $bundle_ids[] = $row->bundle_id;
            }
        }


        $booking_service_map = $this->select(array(
            'select' => array(
                'booking_service' => array('booking_id', 'start_time', 'end_time'),
                'room' => array('room_id' => 'id', 'room_name' => 'name'),
                'employee' => array('employee_id' => 'id', ' employee_first_name' => 'first_name', 'employee_last_name' => 'last_name'),
                'item' => array('service_id' => 'id', 'item_name' => 'name', 'item_type' => 'type')
            ),
            'from' => array(
                'booking_service' => array('table' => 'booking_service'),
                'room' => array('table' => 'room', 'condition' => 'room.id = booking_service.room_id'),
                'employee' => array('table' => 'employee', 'condition' => 'employee.id = booking_service.employee_id'),
                'item' => array('table' => 'item', 'condition' => 'item.id = booking_service.service_id')
            ),
            'where' => array_merge($this->to_condition($condition, 'booking_service'), array('booking_id' => $booking_service_ids))
        ))->result();
    //    $bundle_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $bundle_ids));
        if (count($booking_service_map) == 0) return array();
        $booking_service_map = convert_to_array($booking_service_map, 'booking_id', '', false);
        $temp[] = array();
        foreach ($result as $key => $row) {
            if (!isset($booking_service_map[$row->id]) || !count($booking_service_map[$row->id])) {
                unset($result[$key]);
                continue;
            }
            if ($row->bundle_id) {
//                $row->services = array();
//                $start_time = '9999-12-30 23:59:59';
//                foreach ($booking_service_map[$row->id] as $booking_service) {
//                    $row->services[] = (object)array(
//                        'room_name' => $booking_service->room_name,
//                        'room_id' => $booking_service->room_id,
//                        'employee_id' => $booking_service->employee_id,
//                        'employee_first_name' => $booking_service->employee_first_name,
//                        'employee_last_name' => $booking_service->employee_last_name,
//                        'service_id' => $booking_service->service_id,
//                        'item_name' => $booking_service->item_name,
//                        'start_time' => $booking_service->start_time,
//                        'end_time' => $booking_service->end_time
//                    );
//                    if ($booking_service->start_time < $start_time) {
//                        $start_time = $booking_service->start_time;
//                    }
//                }
//                $row->start_time = $start_time;
//                $row->bundle = $bundle_map[$row->bundle_id];
            } else {
                $booking_service = $booking_service_map[$row->id][0];
                $row->room_name = $booking_service->room_name;
                $row->room_id = $booking_service->room_id;
                $row->employee_id = $booking_service->employee_id;
                $row->employee_first_name = $booking_service->employee_first_name;
                $row->employee_last_name = $booking_service->employee_last_name;
                $row->service_id = $booking_service->service_id;
                $row->item_name = $booking_service->item_name;
                $row->start_time = $booking_service->start_time;
                $row->end_time = $booking_service->end_time;
            }
        }
        return $result;
    }


    function check_booking_by_bill_id($ids)
    {
        $check_booking = $this->select(array(
            'select' => array(
                'booking' => array('id', 'comment')
            ),
            'from' => array(
                'booking' => array('table' => 'booking')
            ),
            'where' => array(
                'booking.bill_id' => $ids
            )
        ))->result();

        if ($check_booking && count($check_booking) > 0) {
            $check_booking = $check_booking[0];
            $update = $this->db->update('booking', array('status' => 1, 'comment' => $check_booking->comment . '<br>VOIDED', 'bill_id' => 'NULL'), array('id' => $check_booking->id));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_employee_working_hours($data)
    {
        $check_date = date('Y-m-d', strtotime($data['date_time']));
        $convert_to_day = date('w', strtotime($data['date_time']));

        $calendar = $this->select(array(
            'select' => array(
                'employee_calendar' => array('id', 'employee_id', 'start_time', 'end_time')
            ),
            'from' => array(
                'employee_calendar' => array('table' => 'employee_calendar')
            ),
            'where' => array(
                'employee_id' => $data['staff_id'],
                'branch_id' => $data['branch_id'],
                'day_id' => $convert_to_day
            )
        ))->result_array();

        if (count($calendar) > 0) {
            $time = strtotime($data['date_time']);
            $end_time = $time + $data['duration'] * 60;
            $start_work_time = strtotime($check_date . ' ' . $calendar[0]['start_time'] . ':00');
            $end_work_time = strtotime($check_date . ' ' . $calendar[0]['end_time'] . ':00');
            if ($start_work_time <= $time && $time <= $end_work_time &&
                $start_work_time <= $end_time && $end_time <= $end_work_time
            ) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    function check_customize_employee_working_hours($data){
        $calendar_customize = $this->select(array(
            'select' => array(
                'employee_calendar_customize' => array('id','employee_id','start_time','end_time','date','working')
            ),
            'from' => array(
                'employee_calendar_customize' => array('table' => 'employee_calendar_customize')
            ),
            'where' => array(
                'employee_id' => $data['staff_id'],
                'branch_id'  => $data['branch_id'],
                'date' => date('Y-m-d', strtotime($data['date_time']))
            )
        ))->result_array();

        if(count($calendar_customize) > 0){
            if($calendar_customize[0]['working'] == 1){ // check offday
                $time = strtotime($data['date_time']);
                $end_time = $time + $data['duration']*60;
                $start_work_time = strtotime($calendar_customize[0]['date'].' '.$calendar_customize[0]['start_time'].':00');
                $end_work_time = strtotime($calendar_customize[0]['date'].' '.$calendar_customize[0]['end_time'].':00');
                if($start_work_time <= $time && $time <= $end_work_time &&
                   $start_work_time <= $end_time && $end_time <= $end_work_time){
                    return TRUE;
                }
                else{
                    return FALSE;
                }
            }
            else{
                return FALSE;
            }
        }
        else{
            return TRUE;
        }
    }

    function check_wrong_bill_tagging(){
        $list_wrongly = $this->select(array(
            'select' => array(
                'booking' => array('id','bill_id')
            ),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'booking' => array('table' => 'booking', 'condition' => 'booking.bill_id = bill.id', 'LEFT')
            ),
            'where' => 'bill.customer_id != booking.customer_id'
        ))->result();

        if(!empty($list_wrongly)){
            foreach($list_wrongly as $item){
                $this->db->update('booking',array('status' => 1, 'bill_id' => NULL),array('id' => $item->id));
            }
        }
    }

    function get_number_of_new_appointment($branch_id){
        $num_appointment = $this->select(array(
            'select' => array(
                'booking' => array('id')
            ),
            'from' => array(
                'booking' => array('table' => 'booking')
            ),
            'where' => array(
                'branch_id' => $branch_id,
                'is_new' => 1,
                'status' => BOOKING_STATUS('Hold'),
                'type'   => BOOKING_TYPE('Online'),
            )
        ))->num_rows();
        return $num_appointment;
    }

    function getAppointmentByBillId($id, $status){
        return $this->select(array(
            'select' => array('id'),
            'from' => array(
                'booking' => array('table' => 'booking')
            ),
            'where' => array(
                'bill_id' => $id,
                'status' => $status
            )
        ))->result();
    }

    function updateEmailReminder12($bookingId){
        if(!empty($bookingId)) {
            $this->db->update('booking', array('email_12' => 1), array('id' => $bookingId));
        }
    }
}

