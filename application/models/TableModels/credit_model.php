<?php

class Credit_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "credit";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = array();
        $this->tableType = "main";
    }

    function getCreditPackageDetail($condition, $include_outer_branch = true){
        if($include_outer_branch){
            $credit = $this->select(array(
                'select'        => array(
                    'credit'        => '*',
                    'branch_group'  => array('branch_group_name' => 'name')
                ),
                'from'          => array(
                    'credit'                => array('table'    =>'credit'),
                    'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit_branch_group.credit_id = credit.id'),
                    'branch_group'          => array('table' => 'branch_group', 'condition' => 'branch_group.id = credit_branch_group.branch_group_id')
                ),
                'where'         => $condition,
                'user_level'    => Permission_Value::ADMIN
            ))->result();
        }else{
            $credit = $this->select(array(
                'from'          => array('credit' => array('table'=>'credit')),
                'where'         => $condition,
                'order'         => 'created_date DESC'
            ))->result();
        }

        if(count($credit)){
            $credit = $credit[0];
            $package = $this->load->table_model('item')->getByID($credit->item_id,array(),Permission_Value::ADMIN);
            $package->credit = $credit->credit;
            $branch_group_name = isset($credit->branch_group_name)?$credit->branch_group_name:"";
            $package->name = $package->name . " ({$branch_group_name})";

            return $package;
        }
        return null;
    }

    function getCreditBranch($ids = 0){
        $ret = array();
        $info = array(
            'select' => array(
                'credit' => array('credit_id' => 'id'),
                'bill_item' => array('credit_add'),
                'item' => array('item_name' => 'name'),
                'bill_branch' => array('branch_id'),
                'branch' => array('branch_name' => 'name')
            ),
            'from' => array(
                'credit' => array('table' => $this->tableName),
                'bill' => array('table' => 'bill', 'condition' => 'credit.customer_id = bill.customer_id'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id AND credit.item_id = bill_item.item_id'),
                'item' => array('table' => 'item', 'condition' => 'item.id = bill_item.item_id'),
                'bill_branch' => array('table' => 'bill_branch', 'condition' => 'bill_branch.bill_id = bill.id'),
                'branch' => array('table' => 'branch', 'condition' => 'bill_branch.branch_id = branch.id')
            ),
            'where' => array('credit.id' => $ids, 'bill.status' => BILL_STATUS('Complete')),
            'user_level' => 4
        );
        $query = $this->select($info);
        if($query != null){
            foreach($query->result() as $row){
                if(!isset($ret[$row->credit_id])){
                    $ret[$row->credit_id] = array();
                    $ret[$row->credit_id]['name'] = $row->item_name;
                    $ret[$row->credit_id]['branch'] = array();
                }
                if(isset($ret[$row->credit_id]['branch'][$row->branch_id])){
                    $ret[$row->credit_id]['branch'][$row->branch_id]['credit'] += floatval($row->credit_add);
                }else{
                    $ret[$row->credit_id]['branch'][$row->branch_id] = array(
                        'branch_name' => $row->branch_name,
                        'credit' => floatval($row->credit_add)
                    );
                }
            }
        }
        return $ret;
    }

    function updateEmailExpiryThreeMonths($credit_id){
        if(!empty($credit_id)){
            $this->db->update('credit', array('email_three_months' => 1), array('id' => $credit_id));
        }
    }

    function updateEmailExpiryOneMonth($credit_id){
        if(!empty($credit_id)){
            $this->db->update('credit', array('email_one_month' => 1), array('id' => $credit_id));
        }
    }
}


