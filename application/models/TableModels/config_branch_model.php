<?php

/**
 * @author
 * @copyright 2014
 */

class Config_Branch_Model extends POS_Table_Model
{
    var $permission;
    function __construct()
    {
        parent::__construct();
        $this->tableName = "config";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->permission =  array(
            2 => array('table' => 'config_branch', 'field' => 'branch_id', 'id_field' => 'config_id'),
            3 => array('table' => 'config_branch_group', 'field' => 'branch_group_id', 'id_field' => 'config_id'),
            4 => array('table' => 'config', 'id_field' => 'id')
        );
    }

    function get($condition = array(),$permission_fields=array()){
        $ret = array();
        $results = parent::get($condition);
        foreach ($results as $result) {
            if (strlen($result->type_value) > 0){
                $type = json_decode($result->type_value);
                if($type){
                    $result->type_value = json_decode($result->type_value);
                }
            }
            $user = $this->session->userdata('login');
            $user_level = null;
            if(!empty($permission_fields['user_level'])){
                $user_level = $permission_fields['user_level'];
            }else if($user && isset($user->login_as_type)){
                $user_level = $user->login_as_type;
            }else{
                $user_level = Permission_Value::ADMIN;
            }

            if($user_level && $user_level!= Permission_Value::ADMIN){
                $value = $result->value;
                $i = Permission_Value::ADMIN - 1;
                while($i >= $user_level){
                    if(! isset($this->permission[$i])){
                        $i--;
                        continue;
                    }
                    $permission = $this->permission[$i];
                    if(isset($permission_fields[$permission['field']])){
                        $field_value = $permission_fields[$permission['field']];
                    }else if($user){
                        if(!isset($user->{$permission['field']})){
                            $result->inherit = true;
                            $i--; continue;
                        }
                        $field_value = $user->{$permission['field']};
                    }
                    if($this->session->userdata('booking_state')){
                        $field_value = 3;
                    }
                    $id_field = isset($permission['id_field'])?$permission['id_field']:'config_id';
                    $temp = $this->db->get_where($permission['table'],array($id_field => $result->id,$permission['field']=>$field_value))->result();
                    if(count($temp)){
                        if($temp[0]->value != 'inherit'){
                            $value = $temp[0]->value;
                            $result->inherit = false;
                        }else{
                            $result->inherit = true;
                        }
                    }else{
                        $result->inherit = true;
                    }
                    $i--;
                }
                $result->value = $value;
            }
            $ret[] = $result;
        }
        return $ret;
    }
}