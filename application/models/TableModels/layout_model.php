<?php

/**
 * @author Vũ Hoàng Huy
 * @copyright 2014
 */
class Layout_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "layout";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

}