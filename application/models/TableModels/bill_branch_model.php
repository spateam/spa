<?php

class Bill_Branch_Model extends POS_Table_Model
{
    function __construct(){
        parent::__construct();
        $this->tableName = "bill_branch";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

    function getBranchIdNumber($billId){
        $res = $this->select(array(
            'select' => array(
                'branch' => array('id','name','address','phone_number','email')
            ),
            'from' => array(
                $this->tableName => array('table' => $this->tableName),
                'branch' => array('table' => 'branch', 'condition' => $this->tableName.'.branch_id = branch.id')
            ),
            'where' => array(
                'bill_id' => $billId
            )
        ))->row_array();
        if(count($res) > 0){
            return $res;
        }
        else{
            return '';
        }
    }
}

