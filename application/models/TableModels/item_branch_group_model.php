<?php

/**
 * @author
 * @copyright 2014
 */

class Item_Branch_Group_model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "item_branch_group";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }

}
