<?php

class Show_In_Booking_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "show_in_booking";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

}