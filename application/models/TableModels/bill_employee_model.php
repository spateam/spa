<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Bill_Employee_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "bill_employee";
        $this->fieldList = $this->db->list_fields($this->tableName);

        $this->fieldListRequirement = (object) array(
            'bill_item_id'          => array(Constraints::Required),
            'employee_id'           => array(Constraints::Required)
        );
        $this->tableType = "connected";
    }

}