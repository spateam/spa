<?php

class Branch_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "branch";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->fieldListRequirement = (object) array(
            'name' => array(Constraints::Required, Constraints::Unique),
            'sale_id_prefix' => array(Constraints::Required, Constraints::Unique)
        );
        $this->tableType = "main";
    }

    function get_branch_by_employee_id($id){
        $infoSql = array(
            'select' => array('employee_branch' => array('branch_id')),
            'from' =>
                array(
                    'employee_branch' => array('table' => 'employee_branch'),
                ),
            'where' => array('employee_id' => $id)
        );
        $query = $this->select($infoSql);
        if($query !== null){
            return $query->result();
        }
        return array();
    }

    function getSimpleData(){
        $result = $this->select(array(
            'select' => array(
                'branch' => array('id','name')
            ),
            'from' => array(
                'branch' => array('table' => 'branch')
            ),
            'where' => array(
                'status' => 1
            )
        ))->result();
        $result = convert_to_array($result,'id');
        return $result;
    }
}

