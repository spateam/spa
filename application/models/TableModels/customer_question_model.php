<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 11/1/2014
 * Time: 3:37 PM
 */

class Customer_Question_Model extends POS_Table_Model
{

    function __construct(){
        parent::__construct();
        $this->tableName = "customer_question";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "connected";
    }
}

