<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class customer_temp_model extends POS_Table_Model
{

    function __construct()
    {
        parent::__construct();
        $this->tableName = "customer_temp";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
        $this->is_all = true;
    }

    function getByID($ID, $permission = array(), $user_level = ""){
        $query = $this->select(array(
            'select'    => array(
                $this->tableName            => $this->fieldList
            ),
            'from'      => $this->tableName,
            'where'     => array($this->tableName.'.id' => $ID),
            'permission' => $permission,
            'user_level' => $user_level
        ));

        if($query !== null){
            return $query->first_row();
        }
        return null;
    }

}