<?php

class Commission_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "commission";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }
}