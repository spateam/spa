<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 2:59 PM
 */


class Department_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "department";
        $this->fieldList = $this->db->list_fields($this->tableName);
        $this->tableType = "main";
    }

    function getDepartmentPermissions($department_id){
        $groups = $this->load->table_model('department_group')->get(array('department_id' => $department_id));
        //Get groups permission
        $DepartmentPermissions = array();
        foreach($groups as $group){
            $data = $this->load->table_model('group_permission')->getPermissions($group->group_id);
            foreach($data as $row){
                if(in_array($row,$DepartmentPermissions) == false){
                    $DepartmentPermissions[] = $row;
                }
            }
        }
        return $DepartmentPermissions;
    }

    function get_department_with_account_not_require($ret_arr = true){
        $result = $this->get(array('is_account_required' => 0));
        if($ret_arr){
            return convert_to_array($result,'','id');
        }else{
            return $result;
        }
    }

    function get_department_serve_booking($ret_arr = true){
        $result = $this->get(array('is_serve_booking' => 1));
        if($ret_arr){
            return convert_to_array($result,'','id');
        }else{
            return $result;
        }
    }

}