<?php
/**
 * Created by PhpStorm.
 * User: Vu Huy
 * Date: 10/28/2014
 * Time: 8:31 AM
 */

class Item_Bundle_Model extends POS_Table_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "item_bundle";
        $this->fieldList = $this->db->list_fields($this->tableName);

        $this->fieldListRequirement = (object) array();
        $this->tableType = "connected";
    }

}
