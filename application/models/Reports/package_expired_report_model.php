<?php
class Package_expired_report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $branch_group_id = $data['branch_group_id'];
        $info = array(
            'select'        => array(
                'credit'        => '*',
                'credit_branch_group'  => array('branch_group_id' => 'branch_group_id')
            ),
            'from'          => array(
                'credit'                => array('table' => 'credit')
            ),
            'where'         => array("credit.end_date < '{$start_date}'")
        );

        if(!$branch_group_id){
            $info['user_level'] = Permission_Value::BRANCH_GROUP;
            $info['permission'] = array('branch_group_id' => $this->user_permission->get_all_branch_group());
        }else{
            $info['user_level'] = Permission_Value::BRANCH_GROUP;
            $info['permission'] = array('branch_group_id' => $this->user_permission->check_branch_group($branch_group_id));
        }
        $packages = $this->load->table_model('credit')->select($info)->result();
        $branch_group = $this->load->table_model('branch_group')->getTableMap('id','',array('id' => convert_to_array($packages,'','branch_group_id')));
        $packages_id = convert_to_array($packages, '', 'item_id');
        $items = $this->load->table_model('item')->getTableMap('id','',array('id' => $packages_id),true,Permission_Value::ADMIN);
        $packages = convert_to_array($packages,'customer_id','',false);
        $customers_packages = array();
        $customer_ids = array();
        foreach($packages as $customer_id=>$package_rows){
            $customer_ids[] = $customer_id;
            if(! isset($customer_package[$customer_id])){
                $customers_packages[$customer_id] = array();
            }
            foreach($package_rows as $package){
                $item = $items[$package->item_id];
                $package->name  = $item->name;
                $package->name  .=" ({$branch_group[$package->branch_group_id]->name})";
                $package->package_id    = $item->id;
                $package->branch_group  = $branch_group[$package->branch_group_id];
                $package->items = $this->load->table_model('package_item')->get(array('package_id' => $package->package_id));
                if(count($package->items) == 0)
                    $package->items = array();
                else
                    $package->items = convert_to_array($package->items, '', 'item_id');
                $customers_packages[$customer_id][] = $package;
            }
        }
        $ret_data = array(
            'customers' => $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_ids)),
            'customers_packages' => $customers_packages
        );
        return $ret_data;
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $packages = $this->load->table_model('credit')->select(array(
            'select'        => array(
                'credit'        => '*',
            ),
            'from'          => array(
                'credit'                => array('table' => 'credit'),
            ),
            'where'         => array("credit.end_date < '{$start_date}'")
        ))->result();
        $packages_id = convert_to_array($packages, '', 'item_id');
        $items = $this->load->table_model('item')->getTableMap('id','',array('id' => $packages_id),true,Permission_Value::ADMIN);
        $packages = convert_to_array($packages,'customer_id','',false);
        $customers_packages = array();
        $customer_ids = array();
        foreach($packages as $customer_id=>$package_rows){
            $customer_ids[] = $customer_id;
            if(! isset($customer_package[$customer_id])){
                $customers_packages[$customer_id] = array();
            }
            foreach($package_rows as $package){
                $item = $items[$package->item_id];
                $package->name  = $item->name;
                $package->package_id    = $item->id;
                $package->items = $this->load->table_model('package_item')->get(array('package_id' => $package->package_id));
                if(count($package->items) == 0)
                    $package->items = array();
                else
                    $package->items = convert_to_array($package->items, '', 'item_id');
                $customers_packages[$customer_id][] = $package;
            }
        }
        $ret_data = array(
            'customers' => $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_ids)),
            'customers_packages' => $customers_packages
        );
        return $ret_data;
    }
/*
    function get_export_data($format, $data){
        $content = array();
        $content[] = array('Package Name', 'Credit Remaining', 'Credit Original', 'Expired Date');
        foreach($data['customers'] as $customer_id=>$customer_fields) {
            $customer_packages = $data['customers_packages'][$customer_id];
            foreach ($customer_packages as $customer_package) {
                $content[] = array(
                    $customer_package->name,
                    $customer_package->credit,
                    $customer_package->credit_original,
                    to_user_format($customer_package->end_date),
                );
            }
        }
        return $content;
    }
    */
    function get_original_data($data,$type = false){
        $content = array();
        $content[] = array(
            array('export_text' => 'Package Name'),
            array('export_text' =>'Credit Remaining'),
            array('export_text' => 'Credit Original'),
            array('export_text' => 'Expired Date')
          );
        foreach($data['customers'] as $customer_id=>$customer_fields) {
            $customer_packages = $data['customers_packages'][$customer_id];
            $content[] = array(array('export_text' => to_full_name($customer_fields->first_name, $customer_fields->last_name),
                'style'=>'color:red; font-weight: bolder',
                'colspan'=>'10'));
            foreach ($customer_packages as $customer_package) {
                $content[] = array(
                    array('export_text' => $customer_package->name),
                    array('export_text' =>$customer_package->credit),
                    array('export_text' => $customer_package->credit_original),
                    array('export_text' => to_user_format($customer_package->end_date))

                );
            }
        }
        return $content;
    }
}