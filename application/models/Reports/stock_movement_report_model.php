<?php
class Stock_Movement_Report_Model extends POS_Report_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function admin_get_detail_data($post_data)
    {

        $data = new stdClass();
        $start_date = $post_data['start_date'];
        $end_date = $post_data['end_date'];
        $item_id = isset($post_data['item_id']) ? $post_data['item_id'] : null;
        $category_id = isset($post_data['category_id']) ? $post_data['category_id'] : null;


        $branch_id = $this->user_check->get_branch_id($post_data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('', 'branch_group_id', array('id' => $branch_id));
        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse(array(), Permission_Value::BRANCH, array('branch_id' => $branch_id, 'branch_group_id' => $branch_group_id));
        $condition = array(
            "warehouse_item_log.log_time >= '{$start_date}'",
            "warehouse_item_log.log_time <= '{$end_date}'",

        );

        if ($item_id) {
            $condition['warehouse_item_log.item_id'] = $item_id;
        }
        if ($category_id) {
            $category_item_id = $this->load->table_model('category_item')->getTableMap('', 'item_id', array('category_id' => $category_id));
            if (is_array($item_id)) {
                $category_item_id = array_merge($category_item_id, $item_id);
            }
            $condition['warehouse_item_log.item_id'] = $category_item_id;
        }
        if ($warehouse_id) {
            $condition['warehouse_item_log.warehouse_id'] = $warehouse_id;
        }


        $model = $this->load->table_model('warehouse_item_log');
        $info = array(
            'select' => array(
                'warehouse_item_log' => array('warehouse_item_id' => 'id', 'warehouse_id', 'item_id', 'quantity', 'created_date', 'log_time', 'log_type'),
                'warehouse_branch' => array('branch_id'),
            ),
            'from' => array(
                'warehouse_item_log' => array('table' => 'warehouse_item_log'),
                'warehouse_branch' => array('table' => 'warehouse_branch', 'condition' => 'warehouse_item_log.warehouse_id = warehouse_branch.warehouse_id'),
            ),
            'where' => $condition,
            'order' => 'log_time desc',
        );


        $results = $model->select($info)->result();
        //
        $tem_id = array();
        foreach ($results as $key => $value) {
            $tem_id[] = $value->item_id;
        }
        $data->branch_map = $this->load->table_model('branch')->getTableMap('id', '');
        $data->item_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $tem_id));
        $user = $this->select(array(
            'select' => array(
                'employee' => array('id','first_name','last_name')
            ),
            'from'   => array(
                'employee' => array('table' => 'employee')
            )
        ))->result_array();
        $new_user = array();
        foreach($user as $u){
            $new_user[$u['id']][] = $u;
        }
        ////
        $results = $this->mapping_data($results, array(
            'first_function' => function (&$entity, $row_data) {

            },
            'mapping_function' => array(
                'branch_id' => function (&$entity, $row_data) {
                },
                'item_id' => function (&$entity, &$row_data) {
                    $row_data->unit_add = 0;
                    $row_data->unit_sold = 0;
                    $row_data->unit_export = 0;
                },
            )
        ), false);
        if(count($results)!=0) {
            foreach ($results['rows'] as $key => $branch) {
                foreach ($branch['rows'] as $key1 => $item) {
                    //$count = count($branch['rows']);
                    $time = $item['rows'][0]->log_time;
                    $condition1 = array(
                        "warehouse_item_log.log_time < '{$time}'",
                        "warehouse_item_log.item_id = '{$key1}'",
                        "warehouse_item_log.warehouse_id = '{$item['rows'][0]->warehouse_id}'"
                    );
                    $temp = array(
                        'select' => array(
                            'warehouse_item_log' => array('warehouse_item_id' => 'id', 'warehouse_id', 'item_id', 'quantity', 'created_date', 'log_time', 'log_type', 'log_user'),
                        ),
                        'from' => array(
                            'warehouse_item_log' => array('table' => 'warehouse_item_log'),
                        ),
                        'where' => $condition1,
                        'limit' => '1',
                        'order' => 'log_time DESC',
                    );
                    $temp_array = $model->select($temp)->result();
                    $i = 0;
                    $temp_quantity = 0;

                    foreach ($item['rows'] as $key4 => $bill) {
                        if(isset($temp_array[0]->log_user)) {
                            $bill->user_name = isset($new_user[$temp_array[0]->log_user]) ? $new_user[$temp_array[0]->log_user][0]['first_name'] : '';
                        }
                        if(isset($data->item_map[$bill->item_id]->product->unit_cost)){
                            $unit_cost = $data->item_map[$bill->item_id]->product->unit_cost;
                            $bill->unit_cost = ($unit_cost == null? 0:$unit_cost);
                        }
                        else{
                            $bill->unit_cost = 0;
                        }
                        $i++;
                        if ($i == 1) {
                            if ($bill->log_type == 'sale') {
                                if (!isset($temp_array[0]->quantity)) {
                                    $bill->unit_sold = $bill->quantity;
                                } else {
                                    $bill->unit_sold = $temp_array[0]->quantity - $bill->quantity;
                                }
                            }
                            else if ($bill->log_type == 'import_pro') {
                                if (!isset($temp_array[0]->quantity)) {
                                    $bill->unit_add = $bill->quantity;
                                } else {
                                    $bill->unit_add = intval($bill->quantity) - intval($temp_array[0]->quantity);
                                }
                            }
                            else if($bill->log_type == 'export_pro'){
                                if (!isset($temp_array[0]->quantity)) {
                                    $bill->unit_export = $bill->quantity;
                                } else {
                                    $bill->unit_export =  intval($temp_array[0]->quantity) - intval($bill->quantity);
                                }
                            }
                        } else {
                            if ($bill->log_type == 'sale') {
                                $bill->unit_sold = intval($temp_quantity) - intval($bill->quantity);
                            }
                            else if ($bill->log_type == 'import_pro') {
                                $bill->unit_add = intval($bill->quantity) - intval($temp_quantity);
                            }

                            else if ($bill->log_type == 'export_pro') {
                                $bill->unit_export = intval($temp_quantity) - intval($bill->quantity);
                            }
                        }
                        $temp_quantity = $bill->quantity;
                    }
                }
            }
            ///////////
            foreach ($results['rows'] as $key => $branch) {
                foreach ($branch['rows'] as $key1 => $item) {
                    foreach ($item['rows'] as $key4 => $bill) {
                        $date = date_create($bill->log_time);
                        $date = date_format($date, 'Y-m-d');
                        $bill->log_time = $date;
                    }
                }
            }
        }
        /////////
        $data->results = $results;
        return $data;
    }


    function get_original_data($data,$type = false)
    {
        $content = array();
        if(count($data->results)!=0) {
            foreach ($data->results['rows'] as $key => $branch) {
                if (isset($data->branch_map[$key])) {
                    $content[] = array(
                        array('export_text' => $data->branch_map[$key]->name,
                            'style' => 'color:red; font-weight:bold',
                            'colspan' => '10'),
                    );
                }

                foreach ($branch['rows'] as $key1 => $item) {
                    if (isset($data->item_map[$key1])) {
                        $content[] = array(
                            array('export_text' => $data->item_map[$key1]->name,
                                'colspan' => '10',
                                'html_class' => 'text-danger',

                            ),
                        );
                    }
                    $content[] = array(
                        array('export_text' => 'Date',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Units Added',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Units Export',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Unit Cost',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Unit sold',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Amount sold',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Balance',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Amount balance',
                            'style' => 'font-weight:bold'),
                        array('export_text' => 'Staff',
                            'style' => 'font-weight:bold'),
                    );

                    $temp_sold = 0;
                    $temp_add = 0;
                    $temp_date = 0;
                    $temp_quantity = 0;
                    $temp_export  = 0;
                    $i = 0;
                    $count = count($item['rows']) - 1;

                    foreach ($item['rows'] as $key4 => $bill) {
                        if ($count == 0) {
                            $content[] = array(
                                array('export_text' => $bill->log_time),
                                array('export_text' => $bill->unit_add),
                                array('export_text' => $bill->unit_export),
                                array('export_text' => $bill->unit_cost,'export_type' => 'currency'),
                                array('export_text' => $bill->unit_sold),
                                array('export_text' => $bill->unit_sold*$bill->unit_cost,'export_type' => 'currency'),
                                array('export_text' => $bill->quantity),
                                array('export_text' => $bill->quantity*$bill->unit_cost,'export_type' => 'currency'),
                                array('export_text' => isset($bill->user_name) ? $bill->user_name : '')
                            );
                        } else {
                            $i++;
                            if (($bill->log_time != $temp_date && $i != 1)) {
                                $content[] = array(
                                    array('export_text' => $temp_date),
                                    array('export_text' => $temp_add),
                                    array('export_text' => $temp_export),
                                    array('export_text' => $bill->unit_cost,'export_type' => 'currency'),
                                    array('export_text' => $temp_sold),
                                    array('export_text' => $bill->unit_cost*$temp_sold,'export_type' => 'currency'),
                                    array('export_text' => $temp_quantity),
                                    array('export_text' => $bill->unit_cost*$temp_quantity,'export_type' => 'currency'),
                                    array('export_text' => isset($bill->user_name) ? $bill->user_name : '')
                                );
                                $temp_sold = 0;
                                $temp_add = 0;
                                $flag = 1;
                            }
                            if ($key4 == $count) {
                                if ($flag == 1) {
                                    $content[] = array(
                                        array('export_text' => $bill->log_time),
                                        array('export_text' => $bill->unit_add),
                                        array('export_text' => $bill->unit_export),
                                        array('export_text' => $bill->unit_cost,'export_type' => 'currency'),
                                        array('export_text' => $bill->unit_sold),
                                        array('export_text' => $bill->unit_sold*$bill->unit_cost,'export_type' => 'currency'),
                                        array('export_text' => $bill->quantity),
                                        array('export_text' => $bill->quantity*$bill->unit_cost,'export_type' => 'currency'),
                                        array('export_text' => isset($bill->user_name) ? $bill->user_name : '')
                                    );
                                } else {
                                    $temp_date = $bill->log_time;
                                    $temp_sold += $bill->unit_sold;
                                    $temp_add += $bill->unit_add;
                                    $temp_export += $bill->unit_export;
                                    $temp_quantity = $bill->quantity;
                                    $content[] = array(
                                        array('export_text' => $temp_date),
                                        array('export_text' => $temp_add),
                                        array('export_text' => $temp_export),
                                        array('export_text' => $bill->unit_cost,'export_type' => 'currency'),
                                        array('export_text' => $temp_sold),
                                        array('export_text' => $bill->unit_cost*$temp_sold,'export_type' => 'currency'),
                                        array('export_text' => $temp_quantity),
                                        array('export_text' => $bill->unit_cost*$temp_quantity,'export_type' => 'currency'),
                                        array('export_text' => isset($bill->user_name) ? $bill->user_name : '')
                                    );
                                    $temp_sold = 0;
                                    $temp_add = 0;
                                    $temp_export = 0;
                                }
                            }
                            $flag = 0;
                            $temp_date = $bill->log_time;
                            $temp_export = $bill->unit_export;
                            $temp_sold += $bill->unit_sold;
                            $temp_add += $bill->unit_add;
                            $temp_quantity = $bill->quantity;
                        }
                    }
                    $content[] = array(
                        array('export_text' => '',
                            'colspan' => '10',
                            'style' => 'background-color:white'
                        ),

                    );

                }
            }
        }
        return $content;
    }
}