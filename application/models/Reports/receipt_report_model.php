<?php
class Receipt_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data)
    {
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];

        $type = $data['summary_type'];
        $status = $data['status'];
        $raw_branch_id = $data['branch_id'];
        $branch_id = $this->user_check->get_branch_id($data['branch_id']);

        if(empty($data['branch_group_id']) || $data['branch_group_id'] == 0){
            $branch_group_id = $this->load->table_model('branch')->getTableMap('', 'branch_group_id', array('id' => $branch_id));
        }
        else{
            $branch_group_id = $data['branch_group_id'];
        }

        $bill_model = $this->load->table_model('bill');
        $data = new stdClass();
        $data->payment_types = $this->load->table_model('payment_type')->get();
        $data->start_date = get_user_date($start_date);
        $data->end_date = get_user_date($end_date);
        $data->type = $type;
        $data->status = $status;
        $order = "bill.created_date desc";
        if ($type == 'none') {
            $order = "bill.created_date desc";
        }
        $bills = $bill_model->select(array(
            'select' => array(
                'bill' => array('id', 'code', 'discount', 'gst', 'created_date', 'amount_due'),
                'customer' => array('first_name', 'last_name', 'customer_code' => 'code')
            ),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'customer' => array('table' => 'customer', 'condition' => 'bill.customer_id = customer.id', 'type' => 'left'),
            ),
            'where' => array("bill.status = {$status}", "bill.created_date > '{$start_date}'", "bill.created_date < '{$end_date}'"),
            'order' => $order,
            'user_level' => Permission_Value::BRANCH,
            'permission' => array('branch_id' => $branch_id, 'branch_group_id' => $branch_group_id)
        ))->result();
        $bill_branch = array();
        $bill_id_temp = array();
        foreach ($bills as $item) {
            $bill_id_temp[] = $item->id;
        }
        if (count($bills) == 0) {
            return $data;
        }
        $bill_items = $bill_model->select(array(
            'select' => array(
                'bill_item' => array('bill_item_id' => 'id', 'bill_id', 'item_id', 'discount_value', 'discount_type', 'quantity', 'price', 'credit_add', 'belong_bundle'),
                'item' => array('id', 'type')
            ),
            'from' => array(
                'bill_item' => array('table' => 'bill_item'),
                'item' => array('table' => 'item', 'condition' => 'item.id = bill_item.item_id')
            ),
            'where' => array('bill_id' => $bill_id_temp),//convert_to_array($bills,'','id'))),
            'user_level' => Permission_Value::ADMIN,
        ))->result();
        $bill_payments = $bill_model->select(array(
            'select' => array(
                'bill_payment' => array('bill_id', 'amount'),
                'payment_type' => array('payment_id' => 'id', 'name'),
            ),
            'from' => array(
                'bill_payment' => array('table' => 'bill_payment'),
                'payment_type' => array('table' => 'payment_type', 'condition' => 'payment_type.id = bill_payment.payment_id')
            ),
            'where' => array('bill_id' => $bill_id_temp)//convert_to_array($bills,'','id'))//)
        ))->result();
        $bill_item_temp = array();
        foreach ($bill_items as $item2) {
            $bill_item_temp[] = $item2->bill_item_id;
        }
        $bill_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id', '', array('bill_item_id' => $bill_item_temp), false);
        $bills = convert_to_array($bills, 'id');

        foreach ($bill_items as $bill_item) {
            $bill_item->credit_value = 0;
            if (isset($bills[$bill_item->bill_id]) === false) {
                continue;
            }
            if (isset($bill_items_credits[$bill_item->bill_item_id])) {
                foreach ($bill_items_credits[$bill_item->bill_item_id] as $bill_item_credit) {
                    $bill_item->credit_value += round($bill_item_credit->credit_value, 2);
                }
            }

            $bills[$bill_item->bill_id]->belong_bundle = $bill_item->belong_bundle;
            $bill = $bills[$bill_item->bill_id];

            //   $bills = pos_add_object($bills, $bill_item, 'bill_id', 'items');

            $object_key = $bill_item->bill_id;
            if (isset($bills[$object_key]) != false) {
                if (isset($bills[$object_key]->items) && is_array($bills[$object_key]->items)) {
                    $bills[$object_key]->items[] = $object_key;
                } else {
                    $bills[$object_key]->items = array();
                    $bills[$object_key]->items[] = $object_key;
                }
            }

            $item_discount = calculate_discount($bill_item->discount_type, $bill_item->discount_value, $bill_item->price * $bill_item->quantity);

            if (isset($bills[$bill_item->bill_id]->items_discount)) {
                $bills[$bill_item->bill_id]->items_discount += round($item_discount, 2);
            } else {
                $bills[$bill_item->bill_id]->items_discount = $item_discount;
            }
            if (isset($bill->redeem_minus)) {
                $bill->redeem_minus -= round($bill_item->credit_value, 2);
            } else {
                $bill->redeem_minus = -$bill_item->credit_value;
            }
            if (isset($bill_item->credit_add) && $bill_item->credit_add > 0) {
                if (isset($bill->redeem_add)) {
                    if (($bill_item->belong_bundle == 0 && $bill_item->type != 4) || ($bill_item->belong_bundle == 1 && $bill_item->type == 2)) // fixed
                        $bill->redeem_add += round($bill_item->quantity * $bill_item->credit_add, 2);
                } else {
                    $bill->redeem_add = round($bill_item->quantity * $bill_item->credit_add, 2);
                }
            }
            if (isset($bill->sale_value)) {
                if ($bill_item->belong_bundle == 0)
                    $bill->sale_value += round($bill_item->price * $bill_item->quantity - $item_discount - $bill_item->credit_value, 2);
            } else {
                if ($bill_item->belong_bundle == 0) {
                    $bill->sale_value = round($bill_item->price * $bill_item->quantity - $item_discount - $bill_item->credit_value - $bill->discount, 2);
                } else {
                    $bill->sale_value = 0;
                }
            }
        }

        $cash_config = $this->system_config->get('cash');
        foreach ($bill_payments as $bill_payment) {
            if (isset($bills[$bill_payment->bill_id]) == false) {
                continue;
            }
            $bill = $bills[$bill_payment->bill_id];
            //$bills = pos_add_object($bills, $bill_payment, 'bill_id', 'payments');

            $add_object_key = $bill_payment->bill_id;
            if (isset($bills[$add_object_key]->payments) && is_array($bills[$add_object_key]->payments)) {
                $bills[$add_object_key]->payments[] = $bill_payment;
            } else {
                $bills[$add_object_key]->payments = array();
                $bills[$add_object_key]->payments[] = $bill_payment;
            }
            if ($bill_payment->payment_id === $cash_config) {
                $bill->sale_total_round = true;
            }
            $bill->payments = pos_render_array_no_dupplicate($bill->payments, 'name', 'amount');
        }

        $branch_total = array();
        if (!to_b($raw_branch_id)) {
            $branches = $this->load->table_model('branch')->getTableMap('id', '', array('id' => $branch_id));
            foreach ($branches as $branch_id => $field) {
                $branch_total[$branch_id] = (object)array(
                    'branch_group_id' => $field->branch_group_id,
                    'branch_name' => $field->name,
                    'gst' => 0,
                    'grand' => 0,
                    'sale_value' => 0,
                    'payments' => new stdClass(),
                    'items_discount' => 0,
                    'redeem_add' => 0,
                    'redeem_minus' => 0,
                    'redeem' => 0,
                    'amount_due' => 0,
                    'discount' => 0
                );
            }
        }

        if ($type == 'none') {
            foreach ($bills as $bill) {
                $bill = $this->build_data_for_bill($bill);

                $bill->created_date = date('d/m/Y h:i A', strtotime($bill->created_date) + 28800);

                if (!to_b($raw_branch_id)) {
                    $branch_total = $this->build_data_for_branch_total($branch_total,$bill);
                }
            }

        } else {
            if ($type == 'daily' || $type == 'daily_branch') {
                $date_format = BASE_DATE_FORMAT; // d/m/Y
            } else if ($type == 'monthly' || $type == 'monthly_branch') {
                $date_format = BASE_DATE_MONTH__FORMAT; // m/Y
            }
            $daily_bills = array();
            $daily_branch_bills = array();
            foreach ($bills as $bill) {
                $bill = $this->build_data_for_bill($bill);

                $timezone = $this->user_check->get('timezone');
                $bill_date = get_user_date($bill->created_date,$timezone);                
                $date = $bill_date;

                if (!to_b($raw_branch_id)) {
                    $branch_total = $this->build_data_for_branch_total($branch_total,$bill);
                }


                if (isset($daily_bills[$date])) {
                    $daily_bill = $daily_bills[$date];
                    $daily_bill->gst += $bill->gst;
                    $daily_bill->discount += $bill->discount;
                    $daily_bill->items_discount += $bill->items_discount;
                    $daily_bill->redeem_add += $bill->redeem_add;
                    $daily_bill->redeem_minus += $bill->redeem_minus;
                    $daily_bill->redeem += $bill->redeem;
                    $daily_bill->amount_due += $bill->amount_due;
                    foreach ($bill->payments as $key => $payment) {
                        if (isset($daily_bill->payments[$key])) {
                            $daily_bill->payments[$key]->amount += $payment->amount;
                            $daily_bill->grand += round($payment->amount, 2);
                        } else {
                            $daily_bill->payments[$key] = new stdClass();
                            $daily_bill->payments[$key]->amount = $payment->amount;
                            $daily_bill->grand += round($payment->amount, 2);
                        }
                    }
                    $daily_bill->grand -= $bill->amount_due;

                    // $daily_bill->sale_value = round($daily_bill->grand,2) - round($daily_bill->gst,2);
                    $daily_bill->sale_value += $bill->sale_value;
                } else {
                    $daily_bills[$date] = new stdClass();
                    $daily_bill = $daily_bills[$date];
                    $daily_bill->gst = $bill->gst;
                    $daily_bill->discount = $bill->discount;
                    $daily_bill->items_discount = $bill->items_discount;
                    $daily_bill->redeem_add = $bill->redeem_add;
                    $daily_bill->redeem_minus = $bill->redeem_minus;
                    $daily_bill->redeem = $bill->redeem;
                    $daily_bill->payments = array();
                    $daily_bill->amount_due = $bill->amount_due;
                    $daily_bill->grand = 0;
                    foreach ($bill->payments as $key => $payment) {
                        $daily_bill->payments[$key] = new stdClass();
                        $daily_bill->payments[$key]->amount = $payment->amount;
                        $daily_bill->grand += round($payment->amount, 2);
                    }
                    $daily_bill->grand -= $bill->amount_due;
                    // $daily_bill->sale_value = round($daily_bill->grand,2) - round($daily_bill->gst,2);
                    $daily_bill->sale_value = $bill->sale_value;
                }

                if (isset($daily_branch_bills[$bill->bill_branch_id])) {
                    if (isset($daily_branch_bills[$bill->bill_branch_id][$date])) {
                        $daily_bill2 = $daily_branch_bills[$bill->bill_branch_id][$date];
                        $daily_bill2->gst += $bill->gst;
                        $daily_bill2->discount += $bill->discount;
                        $daily_bill2->items_discount += $bill->items_discount;
                        $daily_bill2->redeem_add += $bill->redeem_add;
                        $daily_bill2->redeem_minus += $bill->redeem_minus;
                        $daily_bill2->redeem += $bill->redeem;
                        $daily_bill2->amount_due += $bill->amount_due;
                        foreach ($bill->payments as $key => $payment) {
                            if (isset($daily_bill2->payments[$key])) {
                                $daily_bill2->payments[$key]->amount += $payment->amount;
                                $daily_bill2->grand += round($payment->amount, 2);
                            } else {
                                $daily_bill2->payments[$key] = new stdClass();
                                $daily_bill2->payments[$key]->amount = $payment->amount;
                                $daily_bill2->grand += round($payment->amount, 2);
                            }
                        }
                        $daily_bill2->grand -= $bill->amount_due;
                        // $daily_bill2->sale_value = round($daily_bill2->grand,2) - round($daily_bill2->gst,2);
                        $daily_bill2->sale_value += $bill->sale_value;
                    } else {
                        $daily_branch_bills[$bill->bill_branch_id][$date] = new stdClass();
                        $daily_bill2 = $daily_branch_bills[$bill->bill_branch_id][$date];
                        $daily_bill2->gst = $bill->gst;
                        $daily_bill2->discount = $bill->discount;
                        $daily_bill2->items_discount = $bill->items_discount;
                        $daily_bill2->redeem_add = $bill->redeem_add;
                        $daily_bill2->redeem_minus = $bill->redeem_minus;
                        $daily_bill2->redeem = $bill->redeem;
                        $daily_bill2->payments = array();
                        $daily_bill2->amount_due = $bill->amount_due;
                        $daily_bill2->grand = 0;
                        foreach ($bill->payments as $key => $payment) {
                            $daily_bill2->payments[$key] = new stdClass();
                            $daily_bill2->payments[$key]->amount = $payment->amount;
                            $daily_bill2->grand += round($payment->amount, 2);
                        }
                        $daily_bill2->grand -= $bill->amount_due;
                        // $daily_bill2->sale_value = round($daily_bill2->grand,2) - round($daily_bill2->gst,2);
                        $daily_bill2->sale_value = $bill->sale_value;
                    }
                } else {
                    $daily_branch_bills[$bill->bill_branch_id][$date] = new stdClass();
                    $daily_bill2 = $daily_branch_bills[$bill->bill_branch_id][$date];
                    $daily_bill2->gst = $bill->gst;
                    $daily_bill2->discount = $bill->discount;
                    $daily_bill2->items_discount = $bill->items_discount;
                    $daily_bill2->redeem_add = $bill->redeem_add;
                    $daily_bill2->redeem_minus = $bill->redeem_minus;
                    $daily_bill2->redeem = $bill->redeem;
                    $daily_bill2->payments = array();
                    $daily_bill2->amount_due = $bill->amount_due;
                    $daily_bill2->grand = 0;
                    foreach ($bill->payments as $key => $payment) {
                        $daily_bill2->payments[$key] = new stdClass();
                        $daily_bill2->payments[$key]->amount = $payment->amount;
                        $daily_bill2->grand += round($payment->amount, 2);
                    }
                    $daily_bill2->grand -= $bill->amount_due;
                    // $daily_bill2->sale_value = round($daily_bill2->grand,2) - round($daily_bill2->gst,2);
                    $daily_bill2->sale_value = $bill->sale_value;
                }
            }
            $bill_branch = $daily_branch_bills;
            $bills = $daily_bills;
        }
        $total = (object)(array(
            'grand' => 0,
            'gst' => 0,
            'sale_value' => 0,
            'items_discount' => 0,
            'discount' => 0,
            'redeem' => 0,
            'payments' => new stdClass(),
            'amount_due' => 0,
        )
        );
        if ($type == 'daily' || $type == 'monthly' || $type == 'none') {
            $total->redeem_minus = 0;
            $total->redeem_add = 0;
        }
        foreach ($bills as $bill) {
            $total->gst += $bill->gst;
            $total->items_discount += isset($bill->items_discount) ? $bill->items_discount : 0;
            $total->discount += $bill->discount;
            $total->redeem += $bill->redeem;
            $total->amount_due += isset($bill->amount_due) ? $bill->amount_due : 0;
            if (isset($bill->payments)) {
                foreach ($bill->payments as $key => $value) {
                    if (isset($total->payments->$key)) {
                        $total->payments->$key += $value->amount;
                        $total->grand += $value->amount;
                    } else {
                        $total->payments->$key = $value->amount;
                        $total->grand += $value->amount;
                    }
                }
            }
            $total->grand -= $bill->amount_due;
            $total->sale_value = $total->grand - $total->gst;
            if ($type == 'daily' || $type == 'monthly' || $type == 'none') {
                $total->redeem_minus += $bill->redeem_minus;
                $total->redeem_add += $bill->redeem_add;
            }
        }

        foreach($branch_total as $key => $branch){
            if(is_array($branch_group_id)){
                if(!in_array($branch->branch_group_id,$branch_group_id)){
                    unset($branch_total[$key]);
                }
            }
            elseif(is_numeric(intval($branch_group_id))){
                if($branch->branch_group_id != $branch_group_id){
                    unset($branch_total[$key]);
                }
            }
        }

        $data->bills = $bills;
        $data->total = $total;
        $data->bill_branch = $bill_branch;
        $data->branch_total = $branch_total;
        return $data;
    }

    function build_data_for_bill($bill) {
        $bill->sale_value = isset($bill->sale_value) ? $bill->sale_value : 0;
        $bill->gst = round($bill->sale_value * ($bill->gst / 100), 2);
        $bill->grand = round($bill->sale_value + $bill->gst, 2);
        if (isset($bill->sale_total_round) && $bill->sale_total_round == true) {
            $bill->grand = round(sale_total_round($bill->grand),2);
        }
        $bill->payments = isset($bill->payments) ? $bill->payments : array();
        $bill->items_discount = isset($bill->items_discount) ? $bill->items_discount : 0;
        $bill->redeem_add = isset($bill->redeem_add) ? $bill->redeem_add : 0;
        $bill->redeem_minus = isset($bill->redeem_minus) ? $bill->redeem_minus : 0;
        $bill->redeem = $bill->redeem_add + $bill->redeem_minus;
        return $bill;
    }

    function build_data_for_branch_total($branch_total, $bill){
        $branch_total[$bill->bill_branch_id]->items_discount += $bill->items_discount;
        $branch_total[$bill->bill_branch_id]->redeem_add += $bill->redeem_add;
        $branch_total[$bill->bill_branch_id]->redeem_minus += $bill->redeem_minus;
        $branch_total[$bill->bill_branch_id]->redeem += $bill->redeem;
        $branch_total[$bill->bill_branch_id]->gst += $bill->gst;
        $branch_total[$bill->bill_branch_id]->amount_due += $bill->amount_due;
        $branch_total[$bill->bill_branch_id]->discount += $bill->discount;
        foreach ($bill->payments as $key => $payment) {
            if (isset($branch_total[$bill->bill_branch_id]->payments->$key)) {
                $branch_total[$bill->bill_branch_id]->payments->$key += $payment->amount;
                $branch_total[$bill->bill_branch_id]->grand += round($payment->amount, 2);
            } else {
                $branch_total[$bill->bill_branch_id]->payments->$key = $payment->amount;
                $branch_total[$bill->bill_branch_id]->grand += round($payment->amount, 2);
            }
        }
        $branch_total[$bill->bill_branch_id]->grand -= $bill->amount_due;
        $branch_total[$bill->bill_branch_id]->sale_value = round($branch_total[$bill->bill_branch_id]->grand,2) - round($branch_total[$bill->bill_branch_id]->gst,2);
        return $branch_total;
    }

    function get_original_data($data,$type = false)
    {
        $type = $data->type;
        $payment_types = $data->payment_types;
        $total = isset($data->total)?$data->total:array();
        if($type == 'none'){
            $headers = array(
                array('export_text' => 'Ref no.'),
                array('export_text' => 'Date'),
                array('export_text' => 'Member ID.'),
                array('export_text' => 'Customer Name')
            );
        }else{
            $headers = array(array('export_text' => 'Date'));
        }
        foreach($payment_types as $payment_type){
            $headers[] = array('export_text' => $payment_type->name);
            $sum_payment[$payment_type->name] = 0;
        }
        $headers[] = array('export_text' => 'Discount'); $headers[] = array('export_text' => 'Voucher');
        if($type == 'daily' || $type == 'monthly' || $type == 'none'){
            $headers[] = array('export_text' => 'Redeem+'); $headers[] = array('export_text' => 'Redeem-');
        }
        $headers[] = array('export_text' => 'Redeem');
        $headers[] = array('export_text' => 'Sales'); $headers[] = array('export_text' => 'GST');
        $headers[] = array('export_text' => 'Amount Due');$headers[] = array('export_text' => 'Grand');
        $content[] = $headers;
        if(isset($data->bills) && count($data->bills)){
            if($type == 'daily' || $type == 'monthly' || $type == 'none') {
                $show_month = "";
                $show_date = "";
                $sum_discount = 0;
                $sum_voucher = 0;
                $sum_add_redeem = 0;
                $sum_minus_redeem = 0;
                $sum_redeem = 0;
                $sum_sales = 0;
                $sum_gst = 0;
                $sum_amount_due = 0;
                $sum_grand= 0;

                $m_sum_discount = 0;
                $m_sum_voucher = 0;
                $m_sum_add_redeem = 0;
                $m_sum_minus_redeem = 0;
                $m_sum_redeem = 0;
                $m_sum_sales = 0;
                $m_sum_gst = 0;
                $m_sum_amount_due = 0;
                $m_sum_grand= 0;
                foreach ($data->bills as $key => $bill) {
                    if($type == 'monthly'){
                        $new_date = explode("/", $key);
                        $new_date = $new_date[0]."-".$new_date[1]."-".$new_date[2];
                        $new_date = date("F, Y", strtotime($new_date));
                        if($show_month != $new_date){
                            if($show_month != ""){
                                $temp_array = array();
                                $temp = array('export_text' => $show_month);
                                array_push($temp_array, $temp);
                                foreach ($payment_types as $payment){
                                    $temp = array('export_text' => $sum_payment[$payment->name],'export_type' => 'currency');
                                    array_push($temp_array, $temp);
                                    $sum_payment[$payment->name] = 0;
                                }
                                $temp = array('export_text' => $sum_discount,'export_type' => 'currency');
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => $sum_voucher,'export_type' => 'currency');
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => number_format($sum_add_redeem, 2));
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => number_format($sum_minus_redeem, 2));
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => number_format($sum_redeem, 2));
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => $sum_sales,'export_type' => 'currency');
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => $sum_gst,'export_type' => 'currency');
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => $sum_amount_due,'export_type' => 'currency');
                                array_push($temp_array, $temp);
                                $temp = array('export_text' => $sum_grand,'export_type' => 'currency');
                                array_push($temp_array, $temp);

                                $m_sum_discount += $sum_discount;
                                $m_sum_voucher += $sum_voucher;
                                $m_sum_add_redeem += $sum_add_redeem;
                                $m_sum_minus_redeem += $sum_minus_redeem;
                                $m_sum_redeem += $sum_redeem;
                                $m_sum_sales += $sum_sales;
                                $m_sum_gst += $sum_gst;
                                $m_sum_amount_due += $sum_amount_due;
                                $m_sum_grand += $sum_grand;

                                $content[] = $temp_array;

                                $sum_discount = 0;
                                $sum_voucher = 0;
                                $sum_add_redeem = 0;
                                $sum_minus_redeem = 0;
                                $sum_redeem = 0;
                                $sum_sales = 0;
                                $sum_gst = 0;
                                $sum_amount_due = 0;
                                $sum_grand= 0;
                            }
                        }
                        $show_month = $new_date;
                    }

                    $content_row = array();
                    if ($type == 'none') {
                        $content_row[] = array('export_text' => $bill->code);
                        $content_row[] = array('export_text' => $bill->created_date);
                        $content_row[] = array('export_text' => $bill->customer_code);
                        $content_row[] = array('export_text' => to_full_name($bill->first_name, $bill->last_name));
                    } else {
                        if($type != 'monthly')
                            $content_row[] = array('export_text' => $key);
                    }
                    foreach ($payment_types as $payment) {
                        $sum_payment[$payment->name] += isset($bill->payments[$payment->name]) ? $bill->payments[$payment->name]->amount : 0;
                        if($type != 'monthly')
                            $content_row[] = array('export_text' => isset($bill->payments[$payment->name]) ? $bill->payments[$payment->name]->amount : 0,
                                'export_type' => 'currency');
                    }
                    if($type != 'monthly'){
                        $content_row[] = array('export_text' => $bill->items_discount,'export_type' => 'currency');
                        $content_row[] = array('export_text' => $bill->discount,'export_type' => 'currency');
                    }
                    if ($type == 'daily' || $type == 'monthly' || $type == 'none') {
                        if($type != 'monthly'){
                            $content_row[] = array('export_text' => number_format($bill->redeem_add, 2));
                            $content_row[] = array('export_text' => number_format($bill->redeem_minus, 2));
                        }
                        $sum_add_redeem += $bill->redeem_add;
                        $sum_minus_redeem += $bill->redeem_minus;
                    }
                    if($type != 'monthly'){
                        $content_row[] = array('export_text' => number_format($bill->redeem, 2));
                        $content_row[] = array('export_text' => $bill->sale_value,'export_type' => 'currency');
                        $content_row[] = array('export_text' => $bill->gst,'export_type' => 'currency');
                        $content_row[] = array('export_text' => $bill->amount_due,'export_type' => 'currency');
                        $content_row[] = array('export_text' => $bill->grand,'export_type' => 'currency');
                        $content[] = $content_row;
                    }

                    $sum_discount += $bill->items_discount;
                    $sum_voucher += $bill->discount;
                    $sum_redeem += $bill->redeem;
                    $sum_sales += $bill->sale_value;
                    $sum_gst += $bill->gst;
                    $sum_amount_due += $bill->amount_due;
                    $sum_grand += $bill->grand;
                }


                $m_sum_discount += $sum_discount;
                $m_sum_voucher += $sum_voucher;
                $m_sum_add_redeem += $sum_add_redeem;
                $m_sum_minus_redeem += $sum_minus_redeem;
                $m_sum_redeem += $sum_redeem;
                $m_sum_sales += $sum_sales;
                $m_sum_gst += $sum_gst;
                $m_sum_amount_due += $sum_amount_due;
                $m_sum_grand += $sum_grand;

                if($type == 'monthly'){
                    if($show_month != ""){
                        $temp_array = array();
                        $temp = array('export_text' => $show_month);
                        array_push($temp_array, $temp);
                        foreach ($payment_types as $payment){
                            $temp = array('export_text' => $sum_payment[$payment->name],'export_type' => 'currency');
                            array_push($temp_array, $temp);
                        }
                        $temp = array('export_text' => $sum_discount,'export_type' => 'currency');
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => $sum_voucher,'export_type' => 'currency');
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => number_format($sum_add_redeem, 2));
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => number_format($sum_minus_redeem, 2));
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => number_format($sum_redeem, 2));
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => $sum_sales,'export_type' => 'currency');
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => $sum_gst,'export_type' => 'currency');
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => $sum_amount_due,'export_type' => 'currency');
                        array_push($temp_array, $temp);
                        $temp = array('export_text' => $sum_grand,'export_type' => 'currency');
                        array_push($temp_array, $temp);

                        $content[] = $temp_array;
                        $show_month = "";
                    }
                }
            }

            if(isset($data->branch_total)){
                foreach($data->branch_total as $branch_id => $row){
                    $branch_total_row = array();
                    if($type == 'none'){
                        $branch_total_row[] = array('export_text' => $row->branch_name,
                            'html_text' => $row->branch_name,
                            'style' => 'text-align:right',
                            'colspan' =>'4'
                        );
                    }else{
                        $branch_total_row[] = array('export_text' => $row->branch_name);
                    }
                    foreach($payment_types as $payment){
                        if(isset($row->payments->{$payment->name})){
                            $branch_total_row[] = array('export_text' => number_format($row->payments->{$payment->name},2),
                                'html_text' => number_format($row->payments->{$payment->name},2),
                                'label_text' => $payment->name,
                                'label_class' => 'under');

                        }else{
                            $branch_total_row[] = array('export_text' => number_format(0,2),
                                'html_text' => number_format(0,2),
                                'label_text' => $payment->name,
                                'label_class' => 'under');
                        }
                    }
                    $branch_total_row[] = array('export_text' => number_format($row->items_discount, 2),
                        'html_text' => number_format($row->items_discount, 2),
                        'label_text' => 'Discount',
                        'label_class' => 'under');
                    $branch_total_row[] = array('export_text' => number_format($row->discount, 2),
                        'html_text' => number_format($row->discount, 2),
                        'label_text' => 'Voucher',
                        'label_class' => 'under');
                    if($type == 'daily' || $type == 'monthly' || $type == 'none'){
                        $branch_total_row[] = array('export_text' => number_format($row->redeem_add, 2),
                            'html_text' => number_format($row->redeem_add, 2),
                            'label_text' => 'Redeem+',
                            'label_class' => 'under');
                        $branch_total_row[] = array('export_text' => number_format($row->redeem_minus, 2),
                            'html_text' => number_format($row->redeem_minus, 2),
                            'label_text' => 'Redeem-',
                            'label_class' => 'under');
                    }
                    $branch_total_row[] = array('export_text' => number_format($row->redeem,2),
                        'html_text' => number_format($row->redeem,2),
                        'label_text' => 'Redeem',
                        'label_class' => 'under');

                    $branch_total_row[] = array('export_text' => number_format($row->sale_value,2),
                        'html_text' => number_format($row->sale_value,2),
                        'label_text' => 'Sales',
                        'label_class' => 'under');
                    $branch_total_row[] = array('export_text' => number_format($row->gst,2),
                        'html_text' => number_format($row->gst,2),
                        'label_text' => 'GST',
                        'label_class' => 'under');
                    $branch_total_row[] = array('export_text' => number_format($row->amount_due,2),
                        'html_text' => number_format($row->amount_due,2),
                        'label_text' => 'Amount Due',
                        'label_class' => 'under');
                    $branch_total_row[] = array('export_text' => number_format($row->grand,2),
                        'html_text' => number_format($row->grand,2),
                        'label_text' => 'Grand',
                        'label_class' => 'under');
                    $content[] = $branch_total_row;

                    if($type == 'daily_branch' || $type == 'monthly_branch'){
                        if(isset($data->bill_branch[$branch_id])) {
                            $show_month = "";
                            $show_date = "";
                            $sum_discount = 0;
                            $sum_voucher = 0;
                            $sum_add_redeem = 0;
                            $sum_minus_redeem = 0;
                            $sum_redeem = 0;
                            $sum_sales = 0;
                            $sum_gst = 0;
                            $sum_amount_due = 0;
                            $sum_grand= 0;
                            foreach ($data->bill_branch[$branch_id] as $key => $bill) {
                                if($type == 'monthly_branch'){
                                    $new_date = explode("/", $key);
                                    $new_date = $new_date[0]."-".$new_date[1]."-".$new_date[2];
                                    $new_date = date("F, Y", strtotime($new_date));

                                    if($show_month != $new_date){
                                        if($show_month != ""){
                                            $content_row = array();
                                            $content_row[] = array('export_text' => $show_month);
                                            foreach ($payment_types as $payment){
                                                $content_row[] = array('export_text' => $sum_payment[$payment->name],'export_type' => 'currency');
                                                $sum_payment[$payment->name] = 0;
                                            }
                                            $content_row[] = array('export_text' => $sum_discount,'export_type' => 'currency');
                                            $content_row[] = array('export_text' => $sum_voucher,'export_type' => 'currency');
                                            $content_row[] = array('export_text' => number_format($sum_redeem, 2));
                                            $content_row[] = array('export_text' => $sum_sales,'export_type' => 'currency');
                                            $content_row[] = array('export_text' => $sum_gst,'export_type' => 'currency');
                                            $content_row[] = array('export_text' => $sum_amount_due,'export_type' => 'currency');
                                            $content_row[] = array('export_text' => $sum_grand,'export_type' => 'currency');
                                            
                                            $content[] = $content_row;
                                            $sum_discount = 0;
                                            $sum_voucher = 0;
                                            $sum_add_redeem = 0;
                                            $sum_minus_redeem = 0;
                                            $sum_redeem = 0;
                                            $sum_sales = 0;
                                            $sum_gst = 0;
                                            $sum_amount_due = 0;
                                            $sum_grand= 0;
                                        }
                                    }
                                    $show_month = $new_date;
                                }

                                $content_row = array();
                                if($type != 'monthly_branch')
                                    $content_row[] = array('export_text' => $key);
                                foreach ($payment_types as $payment) {
                                    $sum_payment[$payment->name] += isset($bill->payments[$payment->name]) ? $bill->payments[$payment->name]->amount : 0;
                                    if($type != 'monthly_branch')
                                        $content_row[] = array('export_text' => isset($bill->payments[$payment->name]) ? $bill->payments[$payment->name]->amount : 0,
                                            'export_type' => 'currency');
                                }
                                if($type != 'monthly_branch'){
                                    $content_row[] = array('export_text' => $bill->items_discount,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $bill->discount,'export_type' => 'currency');
                                }
                                if ($type == 'daily' || $type == 'monthly' || $type == 'none') {
                                    $content_row[] = array('export_text' => number_format($bill->redeem_add, 2));
                                    $content_row[] = array('export_text' => number_format($bill->redeem_minus, 2));
                                }
                                if($type != 'monthly_branch'){
                                    $content_row[] = array('export_text' => number_format($bill->redeem, 2));
                                    $content_row[] = array('export_text' => $bill->sale_value,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $bill->gst,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $bill->amount_due,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $bill->grand,'export_type' => 'currency');
                                }
                                $content[] = $content_row;

                                $sum_discount += $bill->items_discount;
                                $sum_voucher += $bill->discount;
                                $sum_redeem += $bill->redeem;
                                $sum_sales += $bill->sale_value;
                                $sum_gst += $bill->gst;
                                $sum_amount_due += $bill->amount_due;
                                $sum_grand += $bill->grand;
                            }
                            if($type == 'monthly_branch'){
                                if($show_month != ""){
                                    $content_row = array();
                                    $content_row[] = array('export_text' => $show_month);
                                    foreach ($payment_types as $payment){
                                        $content_row[] = array('export_text' => $sum_payment[$payment->name],'export_type' => 'currency');
                                        $sum_payment[$payment->name] = 0;
                                    }
                                    $content_row[] = array('export_text' => $sum_discount,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $sum_voucher,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => number_format($sum_redeem, 2));
                                    $content_row[] = array('export_text' => $sum_sales,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $sum_gst,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $sum_amount_due,'export_type' => 'currency');
                                    $content_row[] = array('export_text' => $sum_grand,'export_type' => 'currency');
                                    
                                    $content[] = $content_row;
                                    $show_month = "";
                                }
                            }
                        }
                    }

                }
            }

            if($type == 'none'){
                $footer[] = array('export_text' =>'Total',
                    'html_text'=>'Total',
                    'colspan' =>'4',
                    'style' => 'text-align:right'
                );
            }else{
                $footer[] = array('export_text' =>'Total');
            }
            foreach($payment_types as $payment){
                if(isset($total->payments->{$payment->name})){
                    $footer[] = array('export_text' => number_format($total->payments->{$payment->name},2),
                        'html_text' => number_format($total->payments->{$payment->name},2),
                        'label_text' => $payment->name,
                        'label_class' => 'under');

                }else{
                    $footer[] = array('export_text' => '0',
                        'html_text' => '0',
                        'label_text' => $payment->name,
                        'label_class' => 'under');
                }
            }


            if($type =='monthly'){
                $sum_discount = $m_sum_discount;
                $sum_voucher = $m_sum_voucher;
                $sum_redeem = $m_sum_redeem;
                $sum_sales = $m_sum_sales;
                $sum_gst = $m_sum_gst;
                $sum_amount_due = $m_sum_amount_due;
                $sum_grand = $m_sum_grand;
            }


            $footer[] = array('export_text' => number_format($sum_discount, 2),
                'html_text' => number_format($sum_discount, 2),
                'label_text' => 'Discount',
                'label_class' => 'under');
            $footer[] = array('export_text' => number_format($sum_voucher, 2),
                'html_text' => number_format($sum_voucher, 2),
                'label_text' =>  'Voucher',
                'label_class' => 'under');
            if($type == 'daily' || $type == 'monthly' || $type == 'none'){
                $footer[] = array('export_text' => number_format($total->redeem_add, 2),
                    'html_text' => number_format($total->redeem_add, 2),
                    'label_text' => 'Redeem+',
                    'label_class' => 'under');
                $footer[] = array('export_text' => number_format($total->redeem_minus, 2),
                    'html_text' => number_format($total->redeem_minus, 2),
                    'label_text' => 'Redeem-',
                    'label_class' => 'under');
            }
            $footer[] = array('export_text' => number_format($sum_redeem,2),
                'html_text' => number_format($sum_redeem,2),
                'label_text' => 'Redeem',
                'label_class' => 'under');

            $footer[] = array('export_text' => number_format($sum_sales,2),
                'html_text' => number_format($sum_sales,2),
                'label_text' => 'Sales',
                'label_class' => 'under');
            $footer[] = array('export_text' => number_format($sum_gst, 2),
                'html_text' => number_format($sum_gst, 2),

                'label_text' => 'GST',
                'label_class' => 'under'
            );
            $footer[] = array('export_text' => number_format($sum_amount_due,2),
                'html_text' => number_format($sum_amount_due,2),

                'label_text' => 'Amount Due',
                'label_class' => 'under');
            $footer[] = array('export_text' => number_format($sum_grand,2),
                'html_text' => number_format($sum_grand,2),
                'label_text' => 'Grand',
                'label_class' => 'under'
            );
            $content[] = $footer;
        }else{

            $content[] = array(
                array('export_text' => 'No Content',
                    'colspan'=>'20'),
            );
        }
        return $content;

    }
}