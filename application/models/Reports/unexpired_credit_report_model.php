<?php
class Unexpired_Credit_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        //$start_date = $data['start_date'];
        //$end_date = $data['end_date'];
        $month_distance=$data['month_distance'];
        $start_date = get_database_date();

       // $start_date = '2016-05-24 15:59:59';

        $end_date = $month_distance;
        $branch_group_id = $data['branch_group_id'];
        $condition = array();
        if($branch_group_id){
            $condition['credit_branch_group.branch_group_id'] = $branch_group_id;
        }

        $packages = $this->load->table_model('credit')->select(array(
            'select'        => array(
                'credit'        => '*',
                'credit_branch_group'  => array('branch_group_id' => 'branch_group_id')
            ),
            'from'          => array(
                'credit'                => array('table' => 'credit'),
                'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id')
            ),
            'no_permission' => true,
            'where'         => array_merge($condition,array("credit.end_date <= '{$end_date}'", "credit.end_date >= '{$start_date}'")),
            'order'         => 'credit.end_date asc',
        ))->result();

        $branch_group = $this->load->table_model('branch_group')->getTableMap('id','',array('id' => convert_to_array($packages,'','branch_group_id')));
        $packages_id = convert_to_array($packages, '', 'item_id');
        $items = $this->load->table_model('item')->getTableMap('id','',array('id' => $packages_id),true,Permission_Value::ADMIN);
        $customers = $this->load->table_model('customer')->getTableMap('id','',array('id' => convert_to_array($packages,'','customer_id')));
        $unused_credit = 0;
        $current_date = get_database_date();
        foreach($packages as $key=>$package){
            $item = $items[$package->item_id];
            if(! isset($customers[$package->customer_id])){
                unset($packages[$key]);
                continue;
            }
            $package->customer_name = to_full_name($customers[$package->customer_id]->first_name,$customers[$package->customer_id]->last_name);
            $package->code_name = $customers[$package->customer_id]->code;
            $package->customer_email    = $customers[$package->customer_id]->email;
            $package->mobile_number     = $customers[$package->customer_id]->mobile_number;
            $package->name  = $item->name;
            if(!$branch_group_id){
                $package->name  .=" ({$branch_group[$package->branch_group_id]->name})";
            }
            $package->package_id    = $item->id;
            $package->branch_group  = $branch_group[$package->branch_group_id];
            $unused_credit += $package->credit;
            //////
            $diff = abs(strtotime($package->end_date) - strtotime($current_date));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
            $package->compute_months=($years*12)+$months;
            if($days!=0){ $package->compute_months=$package->compute_months+1;}
            else if($hours!=0){ $package->compute_months=$package->compute_months+1;}
            /////
        }
        $total = array('unused_credit' => $unused_credit);

        return array('packages' => $packages, 'total' => $total, 'end_date' => $month_distance);

    }
    function get_original_data($data,$type = false){
        $total_credit=0;
        $content = array();
        $content[] = array(
            array('export_text' =>  'Customer Name'),
            array('export_text' => 'Client ID'),
            array('export_text' => 'Credit Name'),
            array('export_text' => 'Credit Balance'),
            array('export_text' => 'Mobile Number'),
            array('export_text' => 'Expiry Date'),
        );
        $content[] = array(
                        array('export_text' =>   'End Date: '.$data['end_date'],
                            'style'=>'color: red; font-weight: bold',
                            'colspan'=>'10'),
                    );
        foreach($data['packages'] as $package){
            $link = base_url().'customers/credit_history_report/'.$package->customer_id;
            $tempa = round(floatval($package->credit),2);
            $content[] = array(
                array('export_text' =>  $package->customer_name),
                array('export_text' => '<a target="_blank" href="'.$link.'">'.$package->code_name.'</a>'),
                array('export_text' => $package->name),
                array('export_text' =>  number_format($tempa,2)),
                array('export_text' =>  $package->mobile_number),
                array('export_text' =>  $package->end_date),
            );
            $total_credit += floatval($tempa);
        }
        /*$content[] = array(
            array('export_text' =>  'Total',
                'style'=>'text-align: right; font-weight: bold'),
            array('export_text' => $total_credit,
                'style'=>'font-weight: bold',
                'colspan'=>'10'),
           );*/

        $content[] = array(
            array('export_text' =>  'Total Credits',
                'style'=>'text-align: right; font-weight: bold'),
            array('export_text' => number_format($total_credit, 2),
                'style'=>'font-weight: bold',
                'colspan'=>'10'),
           );
        return $content;
    }
}