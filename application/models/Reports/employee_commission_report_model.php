<?php
class Employee_Commission_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($post_data)
    {
        $data = new stdClass();
        $data->start_date = $post_data['start_date'];
        $data->end_date = $post_data['end_date'];
        $data->summary = isset($post_data['summary']) ? $post_data['summary'] : false;
        $data->summary_type = isset($post_data['summary_type']) ? $post_data['summary_type'] : false;
        if($post_data['ec_branch_group_id'] == 0)
            $branch_group_id = $this->load->table_model('branch')->getTableMap('', 'branch_group_id');
        else
            $branch_group_id = $post_data['ec_branch_group_id'];
        if(isset($post_data['branch_id']))
            $branch_id = $this->user_check->get_branch_id($post_data['branch_id']);
        else{
            $branch_id = $this->load->table_model('branch')->getTableMap('', 'id');
            $branch_id = $this->user_check->get_branch_id($branch_id);
        }
        $department_id = isset($post_data['department_id']) ? $post_data['department_id'] : null;
        $employee = isset($post_data['employee']) ? $post_data['employee'] : null;
        $condition = array("bill.created_date > '{$data->start_date}'", "bill.created_date < '{$data->end_date}'");

        if($data->summary_type == "monthly-branch-total"){
            $data->summary = false;
        }
        if ($employee != null) {
            $condition['employee.id'] = $employee;
        }
        if ($department_id != null) {
            if($department_id > 0)
                $condition['employee.department_id'] = $department_id;
        }
        $condition['bill.status'] = BILL_STATUS('Complete');
      //  $condition['item.status'] = array(1,4);
        $bill_model = $this->load->table_model('bill');
        $employees = $bill_model->select(array(
            'select'    => array(
                'employee'      => array('employee_first_name' => 'first_name','employee_last_name' => 'last_name','id','commission_id'),
                'bill_employee' => array('commission_type','commission_value','sub_item_id','sub_commission_value'),
                'bill_item'     => array('bill_item_id' => 'id','item_id','quantity','price','discount_value','discount_type','belong_bundle'),
                'bill'          => array('bill_code' => 'code','created_date','customer_id'),
                'item'          => array('item_type'=> 'type'),
            ),
            'from'      => array(
                'bill_employee'     => array('table' => 'bill_employee'),
                'bill_item'         => array('table' => 'bill_item', 'condition' => 'bill_item.id = bill_employee.bill_item_id'),
                'item'              => array('table' => 'item', 'condition' => 'bill_item.item_id = item.id'),
                'bill'              => array('table' => 'bill', 'condition' => 'bill.id = bill_item.bill_id'),
                'employee'          => array('table' => 'employee','condition' => 'bill_employee.employee_id = employee.id'),
            ),
            'where'     => $condition,
            'order'     => 'bill.created_date DESC',
            'group'     => 'bill_item.id,employee.id',
            'user_level'=> Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)
        ))->result();

        foreach($employees as $employee){
            $commission_true = 0;
            $list_commission = $bill_model->select(array(
                'select' => array(
                    'bill_employee' => array('employee_id','commission_type','commission_value','sub_item_id','sub_commission_value')
                ),
                'from' => array(
                    'bill_employee' => array('table' => 'bill_employee')
                ),
                'where' => array(
                    'bill_item_id' => $employee->bill_item_id,
                    'sub_item_id IS NOT NULL',
                )
            ))->result_array();
            foreach($list_commission as $key => $item){
                $list_commission[$key]['num_employee'] = 1;
                foreach($list_commission as $item2){
                    if($item['sub_item_id'] == $item2['sub_item_id'] && $item['employee_id'] != $item2['employee_id']){
                        $list_commission[$key]['num_employee']++;
                    }
                }
            }
            foreach($list_commission as $item){
                if($item['employee_id'] == $employee->id){
                    $commission_true += ($item['sub_commission_value']/$item['num_employee']);
                }
            }
            $employee->sub_commission_value = $commission_true;
        }

        $commission_id = array();
        $item_id = array();
        $customer_id = array();
        $bill_item_id = array();
        foreach($employees as $row){
            $item_id[] = $row->item_id;
            $customer_id[] = $row->customer_id;
            $commission_id[] = $row->commission_id;
            $bill_item_id[] = $row->bill_item_id;
        }

        $commission_map = $this->load->table_model('commission')->getTableMap('id','',array('id' => $commission_id));
        $item_map = $this->load->table_model('item')->getTableMap('id','',array('id' => $item_id,'status' => array(1,0,4)));
        $customer_map = $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_id, 'status' => array(CUSTOMER_STATUS('Active'),CUSTOMER_STATUS('Hold'),CUSTOMER_STATUS('Delete'))));
        $bill_item_credit_map  = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => $bill_item_id),false);
        foreach($employees as $key=>&$row){
            if(!isset($item_map[$row->item_id])){
                unset($employees[$key]);
                continue;
            }
            $row->item_name = $item_map[$row->item_id]->name;
            $row->commission_name = $commission_map[$row->commission_id]->name;
            if($row->customer_id){
                $row->customer_first_name = $customer_map[$row->customer_id]->first_name;
                $row->customer_last_name = $customer_map[$row->customer_id]->last_name;
                $row->member_id = $customer_map[$row->customer_id]->code;
            }else{
                $row->customer_first_name = 'GUEST';
                $row->customer_last_name = 'GUEST';
                $row->member_id = 'GUEST';
            }
        }

        $all_branch_total = new stdClass();
        $all_branch_total->total_sale = 0;
        $all_branch_total->total_commission = 0;

        $branch_list = $this->load->table_model('branch')->get(array('id' => $branch_id));
        $branch_bills = array();
        foreach($branch_list as $branch) {
            $branch_bills[$branch->id] = array();
            $branch_bills[$branch->id]["employee_list"] = array();
            $branch_bills[$branch->id]["branch_name"] = $branch->name;
            foreach($employees as $bills_list) {
                if($bills_list->bill_branch_id == $branch->id)
                    $branch_bills[$branch->id]["employee_list"][] = $bills_list;
            }
            $employee_by_branch = convert_to_array($branch_bills[$branch->id]["employee_list"],'bill_item_id','',false);

            $num_employees = array();
            foreach($employee_by_branch as $employee_id=>$employee_rows) {
                foreach($employee_rows as $fields){
                    if($fields->sub_item_id){
                        if(! isset($num_employees[$fields->bill_item_id][$fields->sub_item_id] )){
                            $num_employees[$fields->bill_item_id][$fields->sub_item_id] = 0;
                        }
                        $num_employees[$fields->bill_item_id][$fields->sub_item_id]++;
                    }else{
                        if(! isset($num_employees[$fields->bill_item_id])){
                            $num_employees[$fields->bill_item_id] = 0;
                        }
                        $num_employees[$fields->bill_item_id]++;
                    }
                }
            }
            $result = array();
            foreach($employee_by_branch as $bill_item_id => $bill_item_list){

                foreach($bill_item_list as $employee){
                        $key = $employee->id;
                        if($employee->sub_item_id){
                            $num_employee = $num_employees[$bill_item_id][$employee->sub_item_id];
                        }else{
                            $num_employee = $num_employees[$bill_item_id];
                        }

                        if(isset($result[$key])==false){
                            $result[$key] = new stdClass();
                            $result[$key]->employee_name = to_full_name($employee->employee_first_name,$employee->employee_last_name);
                            $result[$key]->subtotal = 0;
                            $result[$key]->subcommission = 0;
                            $result[$key]->commissions = array();
                        }
                        $credit_used = 0;
                        if(isset($bill_item_credit_map[$employee->bill_item_id])){
                            foreach($bill_item_credit_map[$employee->bill_item_id] as $each_bill_item_credit){
                                $credit_used += $each_bill_item_credit->credit_value;
                            }
                        }

                        $total              = $employee->quantity * $employee->price - $credit_used - calculate_discount($employee->discount_type,$employee->discount_value,$employee->quantity * $employee->price);
                        $total              = ($total > 0)? $total : 0 ; // total sales alway > 0
                        $commission_value   = calculate_commission($employee->commission_type,$employee->commission_value,$employee->quantity * $employee->price,$num_employee,$employee->sub_commission_value,$employee->sub_item_id);

                        $result[$key]->commissions[] = (object)array(
                            'total'         => $total,
                            'commission'    => $commission_value,
                            'item_name'     => $employee->item_name,
                            'customer_name' => to_full_name($employee->customer_first_name, $employee->customer_last_name),
                            'member_id'     => $employee->member_id,
                            'commission_name' => $employee->commission_name,
                            'date'            => get_user_date($employee->created_date),
                            'item_id'         => $employee->item_id,
                            'bill_item_id'    => $employee->bill_item_id,
                            'bill_code'       => $employee->bill_code,
                            'bill_branch_id'  => $employee->bill_branch_id
                        );
                        $result[$key]->subtotal         += $total;
                        $result[$key]->subcommission    += $commission_value;
                }
            }

            usort($result,function($a,$b){
                if($a->employee_name==$b->employee_name) return 0;
                return $a->employee_name > $b->employee_name?1:-1;
            });

            if($data->summary){
                $data->admin_system = true;
                $temp = array();
                $branch_total = array();
                $total = (object)array('total_sale' => 0, 'total_commission' => 0);
                foreach($result as $key=>$employee_list){
                    foreach($employee_list->commissions as $commission){
                        if(!isset($temp[$commission->bill_branch_id][$key])) {
                            $temp[$commission->bill_branch_id][$key] = (object)array(
                                'subtotal' => 0,
                                'subcommission' => 0,
                                'employee_name' => '',
                                'commission' => array()
                            );
                        }
                        if(!isset($branch_total[$commission->bill_branch_id])){
                            $branch_total[$commission->bill_branch_id] = (object)array(
                                'branch_total_sale' => 0,
                                'branch_total_commission' => 0
                            );
                        }
                        $temp[$commission->bill_branch_id][$key]->commission[] = $commission;
                        $temp[$commission->bill_branch_id][$key]->subcommission += $commission->commission;
                        $temp[$commission->bill_branch_id][$key]->subtotal += $commission->total;
                        $temp[$commission->bill_branch_id][$key]->employee_name = $employee_list->employee_name;
                        $branch_total[$commission->bill_branch_id]->branch_total_sale += $commission->total;
                        $branch_total[$commission->bill_branch_id]->branch_total_commission += $commission->commission;
                    }
                    $total->total_sale += $employee_list->subtotal;
                    $total->total_commission += $employee_list->subcommission;
                }
                $result = $temp;
                $data->branch_map = $this->load->table_model('branch')->getTableMap('id');
                $data->branch_total = $branch_total;
            }else{
                $total = new stdClass();
                $total->total_sale = 0;
                $total->total_commission = 0;
                foreach($result as $key=>$employee_list){
                    $total->total_sale += $employee_list->subtotal;
                    $total->total_commission += $employee_list->subcommission;
                }
            }

            
            $branch_bills[$branch->id]["employee_list"] = $result;
            $branch_bills[$branch->id]["total_sale"] = $total->total_sale;
            $branch_bills[$branch->id]["total_commission"] = $total->total_commission;
            $all_branch_total->total_sale += $total->total_sale;
            $all_branch_total->total_commission += $total->total_commission;
            //$branch_bills[$branch->id][0] = $branch->name;
        }
        $data->total = $all_branch_total;
        $data->rows = $branch_bills;
        return $data;
    }

    function get_detail_data($post_data){
        $data = new stdClass();
        $data->start_date = $post_data['start_date'];
        $data->end_date = $post_data['end_date'];
        $data->summary = isset($post_data['summary'])?$post_data['summary']:false;
        $employee = isset($post_data['employee'])?$post_data['employee']:0;
        $condition = array("bill.created_date > '{$data->start_date}'","bill.created_date < '{$data->end_date}'");
        if($employee)
            $condition['employee.id'] = $employee;
        $condition['bill.status'] = BILL_STATUS('Complete');
        $bill_model = $this->load->table_model('bill');
        $employees = $bill_model->select(array(
            'select'    => array(
                'employee'      => array('employee_first_name' => 'first_name','employee_last_name' => 'last_name','id'),
                'bill_employee' => array('commission_type','commission_value','sub_item_id','sub_commission_value'),
                'bill_item'     => array('bill_item_id' => 'id','quantity','price','discount_value','discount_type'),
                'item'          => array('item_name'      => 'name', 'item_id' => 'id'),
                'commission'    => array('commission_name' => 'name'),
                'bill'          => array('bill_code' => 'code','created_date'),
                'customer'      => array('customer_first_name' => 'first_name','customer_last_name' => 'last_name','member_id' => 'code')
            ),
            'from'      => array(
                'bill_employee'     => array('table' => 'bill_employee'),
                'bill_item'         => array('table' => 'bill_item', 'condition' => 'bill_item.id = bill_employee.bill_item_id'),
                'bill'              => array('table' => 'bill', 'condition' => 'bill.id = bill_item.bill_id'),
                'employee'          => array('table' => 'employee', 'condition' => 'employee.id = bill_employee.employee_id'),
                'commission'        => array('table' => 'commission', 'condition' => 'commission.id = employee.commission_id'),
                'item'              => array('table' => 'item', 'condition' => 'item.id = bill_item.item_id'),
                'customer'          => array('table' => 'customer', 'condition' => 'customer.id = bill.customer_id'),
            ),
            'where'     => $condition,
            'order'     => 'bill.created_date desc'
        ))->result();

        $num_employees = array();
        foreach($employees as $fields){
            if($fields->sub_item_id){
                if(! isset($num_employees[$fields->bill_item_id][$fields->sub_item_id] )){
                    $num_employees[$fields->bill_item_id][$fields->sub_item_id] = 0;
                }
                $num_employees[$fields->bill_item_id][$fields->sub_item_id]++;
            }else{
                if(! isset($num_employees[$fields->bill_item_id])){
                    $num_employees[$fields->bill_item_id] = 0;
                }
                $num_employees[$fields->bill_item_id]++;
            }

        }
        $employees = convert_to_array($employees,'bill_item_id','',false);

        $result = array();
        foreach($employees as $bill_item_id => $bill_item_list){
            foreach($bill_item_list as $employee){
                $key = $employee->id;
                if($employee->sub_item_id){
                    $num_employee = $num_employees[$bill_item_id][$employee->sub_item_id];
                }else{
                    $num_employee = $num_employees[$bill_item_id];
                }

                if(isset($result[$key])==false){
                    $result[$key] = new stdClass();
                    $result[$key]->employee_name = to_full_name($employee->employee_first_name,$employee->employee_last_name);
                    $result[$key]->subtotal = 0;
                    $result[$key]->subcommission = 0;
                    $result[$key]->commissions = array();
                }
                $total              = $employee->quantity * $employee->price;
                $commission_value   = calculate_commission($employee->commission_type,$employee->commission_value,$total,$num_employee,$employee->sub_commission_value,$employee->sub_item_id);
                $result[$key]->commissions[] = (object)array(
                    'total'         => $total,
                    'commission'    => $commission_value,
                    'item_name'     => $employee->item_name,
                    'customer_name' => to_full_name($employee->customer_first_name, $employee->customer_last_name),
                    'member_id'     => $employee->member_id,
                    'commission_name' => $employee->commission_name,
                    'date'            => get_user_date($employee->created_date),
                    'item_id'         => $employee->item_id,
                    'bill_item_id'    => $employee->bill_item_id,
                    'bill_code'       => $employee->bill_code
                );
                $result[$key]->subtotal         += $total;
                $result[$key]->subcommission    += $commission_value;
            }
        }
        $total = new stdClass();
        $total->total_sale = 0;
        $total->total_commission = 0;
        foreach($result as $key=>$employee_list){
            $total->total_sale += $employee_list->subtotal;
            $total->total_commission += $employee_list->subcommission;
        }

        $data->total = $total;
        $data->rows = $result;

        return $data;
    }

    function get_original_data($data,$type = false){
        $total = $data->total;
        $summary = $data->summary;
        $summary_type = $data->summary_type;
        $results = $data->rows;
        $check_data = 0;
        $branches_list = array();
        //$admin_system = $data->admin_system;
        $content = array();
        if (isset($admin_system) && $admin_system && $summary){
            $content[] = array(
                array('export_text' => 'Employee name'),
                array('export_text' => 'Total Sales'),
                array('export_text' => 'Total Commission'),
            );

        }
        elseif($summary == false){
            $content[] = array(
                array('export_text' => 'Ref No'),
                array('export_text' => 'Date'),
                array('export_text' => 'Item No'),
                array('export_text' => 'Item Name'),
                array('export_text' =>  'Member ID'),
                array('export_text' => 'Member Name'),
                array('export_text' => 'Commission Type'),
                array('export_text' =>  'Total Sales'),
                array('export_text' => 'Commission')
            );
        }else{
            $content[] = array(
                array('export_text' => 'Employee name'),
                array('export_text' => 'Total Sales'),
                array('export_text' => 'Total Commission'),
            );
        }

        foreach($results as $result) {
            $rows = $result["employee_list"];
            if(count($rows)){
                $check_data++;
                if($summary_type != 'monthly-branch-total')
                    $content [] = array(
                        array('export_text' => $result["branch_name"],
                            'html_text' => $result["branch_name"],
                            'style' => 'font-weight: bold; color:blue',
                            'colspan' => '9'),
                    );
                if(!isset($branches_list[$result["branch_name"]])){
                    $branches_list[$result["branch_name"]] = array();
                }

                if(isset($admin_system) && $admin_system && $summary){
                    $branch_map = $data->branch_map;
                    $branch_total = $data->branch_total;
                    foreach ($branch_map as $branch_id => $branch_field){
                        $branch_data = isset($rows[$branch_id]) ? $rows[$branch_id] : null;
                        if (!$branch_data) continue;
                        $content[] = array(
                            array('export_text' => $branch_field->name,
                            'html_text' => $branch_field->name,
                                'style' => 'font-weight: bold; color:red',
                            'colspan'=>'3'),
                        );

                        foreach ($branch_data as $key => $row){
                            $content[] = array(
                                array('export_text' => $row->employee_name),
                                array('export_text' => $row->subtotal),
                                array('export_text' => $row->subcommission),
                            );
                        }
                        $content[] = array(
                            array('export_text' => 'Total',
                            'style'=>'text-align:right'),
                            array('export_text' => $branch_total[$branch_field->id]->branch_total_sale),
                            array('export_text' => $branch_total[$branch_field->id]->branch_total_commission),
                        );

                    }
                    $content[] = array(
                        array('export_text' => 'Total',
                            'style'=>'text-align:right'),
                        array('export_text' => $total->total_sale),
                        array('export_text' => $total->total_commission),
                    );

                }else {
                    if ($summary == false) {
                        foreach ($rows as $key => $row) {
                            if($summary_type != 'monthly-branch-total')
                                $content [] = array(
                                    array('export_text' => $row->employee_name,
                                        'html_text' => $row->employee_name,
                                        'style' => 'font-weight: bold; color:red',
                                        'colspan' => '9'),
                                );

                            if($summary_type == "none"){
                                foreach ($row->commissions as $commission_row) {
                                    $content[] = array(
                                        array('export_text' => $commission_row->bill_code),
                                        array('export_text' => $commission_row->date),
                                        array('export_text' => $commission_row->item_id),
                                        array('export_text' => $commission_row->item_name),
                                        array('export_text' => $commission_row->member_id),
                                        array('export_text' => $commission_row->customer_name),
                                        array('export_text' => $commission_row->commission_name),
                                        array('export_text' => $commission_row->total,
                                            'html_text' => $commission_row->total,
                                            'export_type' => 'currency'),
                                        array('export_text' => $commission_row->commission,
                                            'html_text' => $commission_row->commission,
                                            'export_type' => 'currency',
                                        ),
                                    );
                                }
                            }
                            else{
                                if($summary_type == "daily"){
                                    $show_date = "";
                                    $day_total_sales = 0;
                                    $day_total_commission = 0;
                                    foreach ($row->commissions as $commission_row) {
                                        if($show_date != $commission_row->date){
                                            if($show_date != ""){
                                                $content[] = array(
                                                    array('export_text' => "Sub Total",'style' => 'color: #337AB7'),
                                                    array('export_text' => ''/*$show_date*/,'colspan' => 6,'style' => 'color: #337AB7'),
                                                    array('export_text' => $day_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                    array('export_text' => $day_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                );
                                                $day_total_sales = 0;
                                                $day_total_commission = 0;
                                            }
                                        }
                                        $show_date = $commission_row->date;
                                        $content[] = array(
                                            array('export_text' => $commission_row->bill_code),
                                            array('export_text' => $commission_row->date),
                                            array('export_text' => $commission_row->item_id),
                                            array('export_text' => $commission_row->item_name),
                                            array('export_text' => $commission_row->member_id),
                                            array('export_text' => $commission_row->customer_name),
                                            array('export_text' => $commission_row->commission_name),
                                            array('export_text' => $commission_row->total,
                                                'html_text' => $commission_row->total,
                                                'export_type' => 'currency'),
                                            array('export_text' => $commission_row->commission,
                                                'html_text' => $commission_row->commission,
                                                'export_type' => 'currency',
                                            ),
                                        );
                                        $day_total_sales += $commission_row->total;
                                        $day_total_commission += $commission_row->commission;
                                    }
                                    if($show_date != ""){
                                        $content[] = array(
                                            array('export_text' => "SubTotal",'style' => 'color: #337AB7'),
                                            array('export_text' => ''/*$show_date*/,'colspan' => 6,'style' => 'color: #337AB7'),
                                            array('export_text' => $day_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                            array('export_text' => $day_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                        );
                                        $show_date = "";
                                    }
                                }
                                else{
                                    $show_date = "";
                                    $day_total_sales = 0;
                                    $day_total_commission = 0;
                                    $show_month = "";
                                    $month_total_sales = 0;
                                    $month_total_commission = 0;
                                    foreach ($row->commissions as $commission_row) {
                                        if($show_date != $commission_row->date){
                                            if($show_date != ""){
                                                if($summary_type != 'monthly-branch-total')
                                                    $content[] = array(
                                                        array('export_text' => $show_date,'colspan' => 7),
                                                        array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                                        array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                                    );
                                                $month_total_sales += $day_total_sales;
                                                $month_total_commission += $day_total_commission;
                                                $day_total_sales = 0;
                                                $day_total_commission = 0;
                                            }
                                        }
                                        $show_date = $commission_row->date;
                                        $day_total_sales += $commission_row->total;
                                        $day_total_commission += $commission_row->commission;

                                        $new_date = explode("/", $show_date);
                                        $new_date = $new_date[0]."-".$new_date[1]."-".$new_date[2];
                                        $new_date = date("F, Y", strtotime($new_date));
                                        if($show_month != $new_date){
                                            if($show_month != ""){
                                                if($summary_type != 'monthly-branch-total')
                                                    $content[] = array(
                                                        array('export_text' => 'SubTotal','colspan' => 7,'style' => 'color: #337AB7;text-align:right'),
                                                        array('export_text' => $month_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                        array('export_text' => $month_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                    );
                                                $branches_list[$result["branch_name"]][$show_month]["sale"] += $month_total_sales;
                                                $branches_list[$result["branch_name"]][$show_month]["commission"] += $month_total_commission;
                                                $branches_list[$result["branch_name"]][$show_month]["month"] = $show_month;
                                                $month_total_sales = 0;
                                                $month_total_commission = 0;
                                            }
                                        }
                                        $show_month = $new_date;
                                        if(!isset($branches_list[$result["branch_name"]][$show_month])){
                                            $branches_list[$result["branch_name"]][$show_month] = array();
                                            $branches_list[$result["branch_name"]][$show_month]["sale"] = 0;
                                            $branches_list[$result["branch_name"]][$show_month]["commission"] = 0;
                                            $branches_list[$result["branch_name"]][$show_month]["month"] = $show_month;
                                        }
                                    }
                                    if($show_date != ""){
                                        if($summary_type != 'monthly-branch-total')
                                            $content[] = array(
                                                array('export_text' => $show_date,'colspan' => 7),
                                                array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                                array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                            );
                                        $show_date = "";
                                    }
                                    if($show_month != ""){
                                        if($summary_type != 'monthly-branch-total')
                                            $content[] = array(
                                                array('export_text' => 'SubTotal','colspan' => 7,'style' => 'color: #337AB7;text-align:right'),
                                                array('export_text' => $month_total_sales+$day_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                array('export_text' => $month_total_commission+$day_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                            );
                                        $branches_list[$result["branch_name"]][$show_month]["sale"] += $month_total_sales+$day_total_sales;
                                        $branches_list[$result["branch_name"]][$show_month]["commission"] += $month_total_commission+$day_total_commission;
                                        $branches_list[$result["branch_name"]][$show_month]["month"] = $show_month;
                                        $show_month = "";
                                    }
                                }
                            }
                            if($summary_type != 'monthly-branch-total'){
                                $content[] = array(
                                    array('export_text' => 'Total',
                                        'colspan' => '7',
                                        'style' => 'text-align:right'),
                                    array('export_text' => $row->subtotal,
                                        'html_text' => $row->subtotal,
                                        'export_type' => 'currency'),
                                    array('export_text' => $row->subcommission,
                                        'html_text' => $row->subcommission,
                                        'export_type' => 'currency'),
                                );
                                $content[] = array(
                                    array('export_text' => '',
                                        'colspan' => '10',
                                        'style' => 'background-color:white'
                                       ),
                                );
                            }
                            $content[] = array();
                        }
                    }
                    else {
                        foreach ($rows as $key => $row){
                            if($summary_type == "none"){
                                foreach($row as $key2=>$test)
                                    $content[] = array(
                                        array('export_text' => $test->employee_name,
                                            ),
                                        array('export_text' => $test->subtotal,
                                            'export_type' => 'currency'),
                                        array('export_text' => $test->subcommission,
                                            'export_type' => 'currency'),
                                    );
                            }
                            else{                                
                                foreach($row as $key2=>$test){
                                    $content[] = array(
                                        array('export_text' => $test->employee_name,'style' => 'color: red;','colspan' => '10'),
                                    );
                                    
                                    $show_date = "";
                                    $day_total_sales = 0;
                                    $day_total_commission = 0;
                                    foreach ($test->commission as $commission_row) {
                                        if($summary_type == "daily")
                                            $new_date = $commission_row->date;
                                        else{
                                            $new_date = explode("/", $commission_row->date);
                                            $new_date = $new_date[0]."-".$new_date[1]."-".$new_date[2];
                                            $new_date = date("F, Y", strtotime($new_date));
                                        }
                                        if($show_date != $new_date){
                                            if($show_date != ""){
                                                $content[] = array(
                                                    array('export_text' => $show_date),
                                                    array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                                    array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                                );
                                                $day_total_sales = 0;
                                                $day_total_commission = 0;
                                            }
                                        }
                                        $show_date = $new_date;
                                        $day_total_sales += $commission_row->total;
                                        $day_total_commission += $commission_row->commission;
                                    }
                                    if($show_date != ""){
                                        $content[] = array(
                                            array('export_text' => $show_date),
                                            array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                            array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                        );
                                        $show_date = "";
                                    }
                                    
                                    $content[] = array(
                                        array('export_text' => "SubTotal",'style' => 'color: #337AB7'),
                                        array('export_text' => $test->subtotal,
                                            'style' => 'color: #337AB7',
                                            'export_type' => 'currency'),
                                        array('export_text' => $test->subcommission,
                                            'style' => 'color: #337AB7',
                                            'export_type' => 'currency'),
                                    );
                                }
                            }
                        }
                    }

                    if($summary == false){
                        if($summary_type != 'monthly-branch-total')
                            $content[] = array(
                                array('export_text' =>  'Branch Total',
                                    'style'=>'text-align:right',
                                    'colspan' => '7'),
                                array('export_text' => $result["total_sale"],
                                'html_text' => $result["total_sale"],
                                        'export_type' => 'currency'),
                                array('export_text' =>  $result["total_commission"],
                                'html_text' => $result["total_commission"],
                                        'export_type' => 'currency'),
                            );

                    }else{
                        $content[] = array(
                            array('export_text' =>  'Branch Total',
                                'style'=>'text-align:right'),
                            array('export_text' => $result["total_sale"],
                                'html_text' => $result["total_sale"],
                                'export_type' => 'currency'),
                            array('export_text' =>  $result["total_commission"],
                                'html_text' => $result["total_commission"],
                                'export_type' => 'currency'),
                        );
                    }
                }
            }else{
                /*if($check_data) {
                    $content[] = array(
                        array('export_text' =>'No Data',
                            'colspan'=>'9'),

                    );
                }*/
            }
        }

        if($summary_type == 'monthly-branch-total'){
            $new_branches_list = array();
            foreach($branches_list as $key => $branch){                
                usort($branch,function($a,$b){
                    $temp1 = explode(",", $a['month']);
                    $temp2 = explode(",", $b['month']);
                    $temp1 = trim($temp1[1]);
                    $temp2 = trim($temp2[1]);
                    if($temp1 > $temp2) return -1;
                    if($temp1 < $temp2) return 1;
                                        
                    $d1 = strtotime($a['month']);
                    $d2 = strtotime($b['month']);
                    if($d1==$d2) return 0;
                    return $d1 > $d2?-1:1;
                });
                $new_branches_list[$key] = $branch;
            }
            foreach($new_branches_list as $key => $branch){
                $content[] = array($this->to_export_text($key,2,10));
                $branch_sale = 0;
                $branch_commission = 0;
                foreach($branch as $month => $value){
                    $content[] = array(
                        array('export_text' => $value["month"],'colspan' => 7),
                        array('export_text' => $value["sale"],'export_type' => 'currency'),
                        array('export_text' => $value["commission"],'export_type' => 'currency'),
                    );
                    $branch_sale += $value["sale"];
                    $branch_commission += $value["commission"];
                }

                $content[] = array(
                    $this->to_export_text('Branch Total',3,7),
                    $this->to_export_text($branch_sale,3,1,1,'currency'),
                    $this->to_export_text($branch_commission,3,1,1,'currency'),
                );
                $content[] = array(
                    $this->to_export_text('',0,10),
                );
            }
        }

        if($check_data) {
            if($check_data > 1) {
                if($summary == false){
                    $content[] = array(
                        array('export_text' =>  'All Branch Total',
                            'style'=>'text-align:right',
                            'colspan' => '7'),
                        array('export_text' => $total->total_sale,
                        'html_text' => $total->total_sale,
                                'export_type' => 'currency'),
                        array('export_text' =>  $total->total_commission,
                        'html_text' => $total->total_commission,
                                'export_type' => 'currency'),
                    );

                }else{
                    $content[] = array(
                        array('export_text' =>  'All Branch Total',
                            'style'=>'text-align:right'),
                        array('export_text' => $total->total_sale,
                            'html_text' => $total->total_sale,
                            'export_type' => 'currency'),
                        array('export_text' => $total->total_commission,
                            'html_text' => $total->total_commission,
                            'export_type' => 'currency'),
                    );
                }
            }
        }
        else {
            $content[] = array(
                array('export_text' =>'No Data',
                    'colspan'=>'9'),
            );
        }

        return $content;
    }
}