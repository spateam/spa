<?php
class Mandatory_Field_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $customers_guest = $this->load->table_model('customer')->getTableMap('id','',array('customer_type' => CUSTOMER_TYPE('Guest')));
        $customers_member = $this->load->table_model('customer')->getTableMap('id','',array('customer_type' => CUSTOMER_TYPE('Member')));
        if(!$this->user_permission->check_level(Permission_Value::ADMIN)){
            foreach($customers_guest as $customer_id=>$customer_fields){
                if(!$customer_fields->customer_permission){
                    unset($customers_guest[$customer_id]);
                }
            }
            foreach($customers_member as $customer_id=>$customer_fields){
                if(!$customer_fields->customer_permission){
                    unset($customers_member[$customer_id]);
                }
            }
        }


        return array('customers' => array(
            'guest' => $customers_guest,
            'member'=> $customers_member
        ));
    }

    function get_detail_data($data){
        $customers_guest = $this->load->table_model('customer')->getTableMap('id','',array('customer_type' => CUSTOMER_TYPE('Guest')));
        foreach($customers_guest as $customer_id=>$customer_fields){
            if(!$customer_fields->customer_permission){
                unset($customers_guest[$customer_id]);
            }
        }
        $customers_member = $this->load->table_model('customer')->getTableMap('id','',array('customer_type' => CUSTOMER_TYPE('Member')));
        foreach($customers_member as $customer_id=>$customer_fields){
            if(!$customer_fields->customer_permission){
                unset($customers_member[$customer_id]);
            }
        }

        return array('customers' => array(
            'guest' => $customers_guest,
            'member'=> $customers_member
        ));
    }
/*
    function get_export_data($format, $data){
        $customers = $data['customers'];
        $content = array();
        $content[] = array('Member');
        $content[] = array('Member ID','First Name', 'Nric', 'Mobile Number', 'Email','Gender');
        foreach($customers['member'] as $customer_id=>$customer_fields){
            $content[] = array(
                $customer_fields->code,
                $customer_fields->first_name,
                $customer_fields->nric,
                $customer_fields->mobile_number,
                $customer_fields->email,
                GENDER($customer_fields->gender),
            );
        }
        $content[] = array('Guest');
        $content[] = array('Member ID','First Name', 'Mobile Number', 'Gender');
        foreach($customers['guest'] as $customer_id=>$customer_fields){
            $content[] = array(
                $customer_fields->code,
                $customer_fields->first_name,
                $customer_fields->mobile_number,
                GENDER($customer_fields->gender),
            );
        }
        return $content;
    }
    */
    function get_original_data($data,$type = false){
        $customers = $data['customers'];
        $content = array();
        $content[] = array(
            array('export_text' => 'Member ID'),
            array('export_text' => 'First Name'),
            array('export_text' => 'Nric'),
            array('export_text' => 'Mobile Number'),
            array('export_text' => 'Email'),
            array('export_text' => 'Gender'),
        );
        $content[] = array(
            array('export_text' => 'Member',
                'style'=>'color:red; font-weight: bolder',
                'colspan'=>'10'));

        foreach($customers['member'] as $customer_id=>$customer_fields){
            $content[] = array(
                array('export_text' => $customer_fields->code),
                array('export_text' => $customer_fields->first_name),
                array('export_text' => $customer_fields->nric),
                array('export_text' => $customer_fields->mobile_number),
                array('export_text' =>  $customer_fields->email),
                array('export_text' => GENDER($customer_fields->gender)),

            );
        }
        $content[] = array(
            array('export_text' => '',
                'style'=>'background-color:white',
                'colspan'=>'10'));
        $content[] = array(
            array('export_text' => 'Member ID',
                'style'=>'font-weight:bold; background-color:white'),
            array('export_text' => 'First Name',
                'style'=>'font-weight:bold; background-color:white'),
            array('export_text' => 'Mobile Number',
                'style'=>'font-weight:bold; background-color:white'),
            array('export_text' => 'Gender',
                'style'=>'font-weight:bold; background-color:white'),
        );
        $content[] = array(
            array('export_text' => 'Guest',
                'style'=>'color:red; font-weight: bolder',
                'colspan'=>'4'));

        foreach($customers['guest'] as $customer_id=>$customer_fields){
            $content[] = array(
                array('export_text' => $customer_fields->code),
                array('export_text' => $customer_fields->first_name),
                array('export_text' => $customer_fields->mobile_number),
                array('export_text' => GENDER($customer_fields->gender)),

            );
        }
        return $content;
    }
}