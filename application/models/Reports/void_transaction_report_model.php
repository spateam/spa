<?php
class Void_Transaction_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];

        $branch_id = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        $replaced_bills = $this->load->table_model('bill')->get(array(
            "created_date > '{$start_date}'",
            "created_date < '{$end_date}'",
            "status" => BILL_STATUS('Void')
        ),'created_date desc','','',Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));

        $replacing_bills = $this->load->table_model('bill')->getTableMap('id','',array(
            'id' => convert_to_array($replaced_bills,'','replace_bill_id')
        ),true,Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));

        foreach($replaced_bills as $bill){
            if($bill->replace_bill_id){
                $bill->replacing_bill = (empty($replacing_bills[$bill->replace_bill_id])?'':$replacing_bills[$bill->replace_bill_id]);
                $bill->replace_bill_date    = (empty($bill->replace_bill_date)?'':get_user_date($bill->replace_bill_date,$this->system_config->get('timezone'),'',true));
            }else{
                $bill->replace_bill_id = '';
                $bill->replacing_bill = (object)array(
                    'code' => ''
                );
            }
            $bill->created_date         = get_user_date($bill->created_date,$this->system_config->get('timezone'),'',true);
            $bill->void_date            = get_user_date($bill->void_date,$this->system_config->get('timezone'),'',true);
        }

        return array(
            'bills' => $replaced_bills
        );
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];

        $replaced_bills = $this->load->table_model('bill')->get(array(
            "created_date > '{$start_date}'",
            "created_date < '{$end_date}'",
            "status" => BILL_STATUS('Void')
        ),'code desc');
        $replacing_bills = $this->load->table_model('bill')->getTableMap('id','',array(
            'id' => convert_to_array($replaced_bills,'','replace_bill_id')
        ));

        foreach($replaced_bills as $bill){
            if($bill->replace_bill_id){
                $bill->replacing_bill = $replacing_bills[$bill->replace_bill_id];
                $bill->replace_bill_date    = get_user_date($bill->replace_bill_date,$this->system_config->get('timezone'),'',true);
            }else{
                $bill->replace_bill_id = '';
                $bill->replacing_bill = (object)array(
                    'code' => ''
                );
            }
            $bill->created_date         = get_user_date($bill->created_date,$this->system_config->get('timezone'),'',true);
            $bill->void_date            = get_user_date($bill->void_date,$this->system_config->get('timezone'),'',true);
        }

        return array(
            'bills' => $replaced_bills
        );
    }

    function get_original_data($data,$type = false){
    	/*********** Tran Minh Thao added - Begin ***********/
        $user_data = $this->session->userdata('login');
        /*********** Tran Minh Thao added - End ***********/
        $bills = $data['bills'];
        $content = array();
        $content[] = array(
            array('export_text' => 'Bill Code'),
            array('export_text' => 'Created Time'),
            array('export_text' => 'Void Time'),
            array('export_text' => 'Replaced by Bill Code'),
            array('export_text' => 'Replaced Time'),
        );
        foreach($bills as $bill){
        	/*********** Tran Minh Thao added - Begin **********
        	//	Compare user's branch id with bill id to allow user can only view bills in current he/she's branch
        	*/
        	$link = $bill->code;
        	if(isset($user_data->branch_id)) {
	        	if($user_data->branch_id == $bill->bill_branch_id) {
		            $link_void_bill = site_url('sales/receipt/'.$bill->id);
		            $link = "<a target='_blank' href='".$link_void_bill."'>".$bill->code."</a>";
		        }
	    	}
	        /*********** Tran Minh Thao added - End ***********/

            $content[] = array(
                array('export_text' => $link),
                array('export_text' => $bill->created_date),
                array('export_text' => $bill->void_date),
                array('export_text' => (empty($bill->replacing_bill->code)?'':$bill->replacing_bill->code)),
                array('export_text' => $bill->replace_bill_date),

            );
        }
        return $content;
    }
}