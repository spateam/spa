<?php
class Manual_Credit_Adjustment_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $fromDate = get_database_date($data['from_date']);
        $toDate = $data['to_date'].' 15:59:59';
        $branch_group_id = $data['branch_group_id'];
        $branch_id = $data['branch_id'];

        $res = $this->select(array(
            'select' => array(
                'credit_adjustment_log' => '*'
            ),
            'from' => array(
                'credit_adjustment_log' => array('table' => 'credit_adjustment_log', 'condition' => 'customer.id = credit_adjustment_log.credit_id','type' => 'LEFT'),
                'credit_branch_group' => array('table' => 'credit_branch_group', 'condition' => 'credit_adjustment_log.credit_id = credit_branch_group.credit_id', 'type' => 'LEFT'),
            ),
            'where' => array(
                'credit_branch_group.branch_group_id' => $branch_group_id,
                'credit_adjustment_log.log_time <' => $toDate,
                'credit_adjustment_log.log_time >' => $fromDate
            ),
            'order' => 'credit_adjustment_log.log_time'
        ))->result_array();

        $condition = array();
        if($branch_id){
            $condition['bill_branch.branch_id'] = $branch_id;
        }

        $creditInfor = $this->select(array(
            'select' => array(
                'credit' => array('credit_id' => 'id','customer_id' => 'customer_id'),
                'item' => array('item_name' => 'name'),
                'bill_branch' => array('branch_id')
            ),
            'from' => array(
                'credit' => array('table' => 'credit'),
                'item' => array('table' => 'item', 'condition' => 'credit.item_id = item.id','type' => 'LEFT'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'credit.id = bill_item.credit_id', 'type' => 'LEFT'),
                'bill_branch' => array('table' => 'bill_branch', 'condition' => 'bill_item.bill_id = bill_branch.bill_id', 'type' => 'LEFT')
            ),
            'where' => 'bill_item.credit_add is not null',
            'where' => $condition,
            'group' => 'credit_id',
            'no_permission' => true
        ))->result_array();

        $newCreditList = array();
        foreach($creditInfor as $item){
            $newCreditList[$item['credit_id']] = $item;
        }

        foreach($res as $key => $item){
            if(isset($newCreditList[$item['credit_id']])){
                $customerInfo = $this->select(array(
                    'select' => array('customer' =>array('customer_id' => 'id','customer_name' => 'first_name', 'customer_code' => 'code')),
                    'from' => array('customer' => array('table' => 'customer')),
                    'where' => array('id' => $newCreditList[$item['credit_id']]['customer_id'])
                ))->result_array();

                if(!empty($customerInfo)){
                    $item['customer_id'] = $customerInfo[0]['customer_id'];
                    $item['customer_name'] = $customerInfo[0]['customer_name'];
                    $item['customer_code'] = $customerInfo[0]['customer_code'];
                }
                $item['item_name_from'] = $newCreditList[$item['credit_id']]['item_name'];
                $item['log_time'] = get_user_date($item['log_time'],'','Y-m-d H:i:s');
                $res[$key] = $item;
            }
            else{
                unset($res[$key]);
            }
        }

        return array('data' => $res);

    }
    function get_original_data($res,$type = false){
        $data = $res['data'];
        $total_credit=0;
        $content = array();
        $content[] = array(
            array('export_text' => 'Customer Name'),
            array('export_text' => 'Client ID'),
            array('export_text' => 'Credit Adjustment'),
            array('export_text' => 'Credit Before Adjustment'),
            array('export_text' => 'Credit Change'),
            array('export_text' => 'Credit After Adjustment'),
            array('export_text' => 'Log Time'),
        );
        $totalBefore = 0;
        $totalChange = 0;
        $totalAfter  = 0;
        $system = $this->config->item('current_system');
        foreach($data as $item){
            if($system == 'admin'){
                $link = base_url().'admin/customers/credit_history_report/'.$item['customer_id'];
            }
            else{
                $link = base_url().'customers/credit_history_report/'.$item['customer_id'];
            }

            if($type == 'export'){
                $fill = $item['customer_code'];
            }
            else{
                $fill = '<a target="_blank" href="'.$link.'">'.$item['customer_code'].'</a>';
            }

            $content[] = array(
                array('export_text' => isset($item['customer_name'])?$item['customer_name']: ''),
                array('export_text' => $fill ),
                array('export_text' => isset($item['item_name_from'])?$item['item_name_from']:''),
                array('export_text' => $item['credit_value_before']),
                array('export_text' => $item['credit_change']),
                array('export_text' => number_format(floatval(($item['credit_value_before'])) + floatval($item['credit_change']),2,'.',',')),
                array('export_text' => $item['log_time']),
            );
            $totalBefore += ($item['credit_value_before']);
            $totalChange += ($item['credit_change']);
            $totalAfter  += (floatval(($item['credit_value_before'])) + (floatval($item['credit_change'])));
        }

        $content[] = array(
            array('export_text' =>  'Total:',
                'style'=>'text-align: right; font-weight: bold',
                'colspan'=>'3'),
            array('export_text' => number_format($totalBefore,2,'.',','), 2,
                'style'=>'font-weight: bold'),
            array('export_text' => number_format($totalChange,2,'.',','), 2,
                'style'=>'font-weight: bold'),
            array('export_text' => number_format($totalAfter,2,'.',','), 2,
                'style'=>'font-weight: bold'),
            array('export_text' => ''),
        );

        return $content;
    }
}