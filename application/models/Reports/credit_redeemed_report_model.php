<?php
class Credit_Redeemed_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $fromDate = get_database_date($data['from_date'], '', 'Y-m-d H:i:s');
        $toDate = date('Y-m-d',strtotime($data['to_date'])).' 15:59:59';

        if($fromDate > $toDate){
            return array();
        }

        $branch_group_id = $data['branch_group_id'];
        $branch_id = $data['branch_id'];

        $conditions = array(
            'bill.status' => BILL_STATUS('Complete'),
            'bill.created_date >' => $fromDate,
            'bill.created_date <' => $toDate,
        );

        if($branch_id){
            $conditions['bill_branch.branch_id'] = $branch_id;
        }


        // GET BALANCE TODAY
        $listBill = $this->select(array(
            'select'        => array(
                'bill'        => array('bill_code' => 'code','customer_id','created_date'),
                'bill_item_credit' => array('credit_id','credit_value'),
                'bill_branch' => array('branch_id')
            ),
            'from' => array(
                'bill'                  => array('table' => 'bill'),
                'bill_item'             => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id'),
                'bill_item_credit'      => array('table' => 'bill_item_credit', 'condition' => 'bill_item.id = bill_item_credit.bill_item_id'),
                'bill_branch'     => array('table' => 'bill_branch', 'condition' => 'bill.id = bill_branch.bill_id')
            ),
            'no_permission' => true,
            'where'         => $conditions
        ))->result_array();

        $listCredit = $this->select(array(
            'select' => array(
                'credit' => array('credit_id' => 'id','item_id','end_date')
            ),
            'from' => array(
                'credit' => array('table' => 'credit'),
                'credit_branch_group' => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id')
            ),
            'no_permission' => true,
            'where' => array(
                'credit_branch_group.branch_group_id' => $branch_group_id
            )
        ))->result_array();

        foreach($listCredit as $it){
            $temp[$it['credit_id']] = $it;
        }
        $listCredit = $temp; unset($temp);

        $customer = $this->select(array(
            'select' => array('customer' => array('customer_id' => 'id','customer_code' => 'code','customer_name' => 'first_name')),
            'from' => array('customer' => array('table' => 'customer')),
            'no_permission' => true
        ))->result_array();
        foreach ($customer as $cust){
            $temp[$cust['customer_id']] = $cust;
        }
        $customer = $temp; unset($temp);

        $items = $this->select(array(
            'select' => array('item' => array('item_id' => 'id','item_name' => 'name')),
            'from' => array('item' => array('table' => 'item')),
            'no_permission' => true
        ))->result_array();
        foreach ($items as $item){
            $temp[$item['item_id']] = $item;
        }
        $items = $temp; unset($temp);

        // get list branches
        $branches = $this->select(array(
            'select' => array('branch' => array('branch_id' => 'id','branch_name' => 'name')),
            'from' => array('branch' => array('table' => 'branch')),
            'no_permission' => true
        ))->result_array();
        foreach ($branches as $ie){
            $temp[$ie['branch_id']] = $ie;
        }
        $branches = $temp; unset($temp);

        $final = array();

        foreach($listBill as $item){
            if(isset($listCredit[$item['credit_id']])){
                if(!isset($final[$item['credit_id']]['expired_date'])){
                    $final[$item['credit_id']]['expired_date'] = get_user_date($listCredit[$item['credit_id']]['end_date'], $this->config->item('timezone'), 'Y-m-d H:i:s');
                }
                if(!isset($final[$item['credit_id']]['customer_code'])){
                    $final[$item['credit_id']]['customer_id'] = $customer[$item['customer_id']]['customer_id'];
                    $final[$item['credit_id']]['customer_code'] = $customer[$item['customer_id']]['customer_code'];
                }
                if(!isset($final[$item['credit_id']]['item_name'])){
                    $final[$item['credit_id']]['item_name'] = $items[$listCredit[$item['credit_id']]['item_id']]['item_name'];
                }
                $final[$item['credit_id']]['place_redeemed'][] = array($branches[$item['branch_id']]['branch_name'],$item['credit_value']);
                if(isset($final[$item['credit_id']]['credit_redeemed'])){
                    $final[$item['credit_id']]['credit_redeemed'] += $item['credit_value'];
                }
                else{
                    $final[$item['credit_id']]['credit_redeemed'] = $item['credit_value'];
                }
            }
        }

        return array('data' => $final, 'params' => $data);
    }

    function get_original_data($res,$type = false){
        $data = $res['data'];
        $total_credit=0;
        $content = array();
        $content[] = array(
            array('export_text' =>   'From: '.$res['params']['from_date'].' - To: '. $res['params']['to_date'],
                'style'=>'color: red; font-weight: bold',
                'colspan'=>'10'),
        );
        $content[] = array(
            array('export_text' => 'Expired Date'),
            array('export_text' => 'Client ID'),
            array('export_text' => 'Package Name'),
            array('export_text' => 'Locations Redeemed'),
            array('export_text' => 'Total credits redeemed'),
        );
        $system = $this->config->item('current_system');
        $branchTotal = array();
        foreach($data as $item){
            $branchName = array();
            if($system == 'admin'){
                $link = base_url().'admin/customers/credit_history_report/'.$item['customer_id'];
            }
            else{
                $link = base_url().'customers/credit_history_report/'.$item['customer_id'];
            }
            foreach($item['place_redeemed'] as $is){
                $branchName[] = $is[0];
            }
            $content[] = array(
                array('export_text' => $item['expired_date']),
                array('export_text' => '<a target="_blank" href="'.$link.'">'.$item['customer_code'].'</a>'),
                array('export_text' => $item['item_name']),
                array('export_text' => implode(',',$branchName)),
                array('export_text' => number_format($item['credit_redeemed'],2,'.',','))
            );
            foreach($item['place_redeemed'] as $as){
                if(isset($branchTotal[$as[0]])){
                    $branchTotal[$as[0]] += floatval($as[1]);
                }
                else{
                    $branchTotal[$as[0]] = floatval($as[1]);
                }
            }

            $total_credit += floatval($item['credit_redeemed']);
        }
        foreach($branchTotal as $key => $item){
            $content[] = array(
                array('export_text' =>  'Credit Redeemed At '.ucfirst($key),
                    'style'=>'text-align: right; font-weight: bold',
                    'colspan'=>'4'),
                array('export_text' => number_format($item,2,'.',','), 2,
                    'style'=>'font-weight: bold'),
            );
        }
        $content[] = array(
            array('export_text' =>  'Total Credits Redeemed',
                'style'=>'text-align: right; font-weight: bold',
                'colspan'=>'4'),
            array('export_text' => number_format($total_credit,2,'.',','), 2,
                'style'=>'font-weight: bold'),
        );
        return $content;
    }
}