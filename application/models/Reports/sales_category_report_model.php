<?php
class Sales_Category_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $branch_id = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        $category_id = isset($data['category_id'])?$data['category_id']:null;
        $summary = isset($category_id)&&count($category_id)==1?false:true;
        $condition = array();
        if(isset($category_id) && $category_id){
            $condition['category.id'] = $category_id;
        }
        $model = $this->load->table_model('category');
        $branch_model = $this->load->table_model('branch');
        $data = new stdClass();
        $data->category_map = $model->select(array(
                'select'=> array('name'),
                'from' => array('category' => array('table'=>'category')),
                'where' => $condition,
                'user_level'=> Permission_Value::BRANCH,
                'permission'=> array('branch_id' => $branch_id)
            )
        )->result();

        $data->category_map = pos_render_array_no_dupplicate($data->category_map,'name');
        $data->start_date   = get_user_date($start_date);
        $data->end_date     = get_user_date($end_date);

        $bill_items = $model->select(array(
            'select'    =>  array(
                'bill'     => array('created_date'),
                'bill_item'=> array('id','discount_value','discount_type','quantity','price'),
                'item'     => array('item_id' => 'id','item_name' => 'name'),
                'category' => array('category_name' => 'name','category_id' => 'id')
            ),
            'from'      => array(
                'bill'      => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id'),
                'item'      => array('table' => 'item', 'condition' => 'item.id = bill_item.item_id'),
                'category_item' => array('table' => 'category_item', 'condition' => 'item.id = category_item.item_id'),
                'category' => array('table' => 'category','condition' => 'category_item.category_id = category.id')
            ),
            'where'     => array_merge($condition,array("bill.created_date > '{$start_date}'", "bill.created_date < '{$end_date}'")),
            'order'     => 'bill.created_date DESC',
            'group'     => 'bill_item.bill_id,bill_item.item_id,category_item.category_id',
            'user_level'=> Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)
        ))->result();
        $bill_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bill_items,'','id')),false);
        if(count($bill_items) == 0){
            return $data;
        }

        foreach($bill_items as $bill_item){
            $bill_item->credit_value = 0;
            if(isset($bill_items_credits[$bill_item->id])){
                foreach($bill_items_credits[$bill_item->id] as $bill_item_credit){
                    $bill_item->credit_value += $bill_item_credit->credit_value;
                }
            }
            $bill_item->sale_value = $bill_item->quantity * $bill_item->price - $bill_item->credit_value - calculate_discount($bill_item->discount_type,$bill_item->discount_value,$bill_item->quantity * $bill_item->price);
            $bill_item->created_date = get_user_date($bill_item->created_date);
        }
        $item_sale_map = convert_to_array($bill_items,'item_id','',false);
        if($summary){
            $bill_items = convert_to_array($bill_items,'bill_branch_id', "", false);
            foreach($bill_items as $branch_id=>$rows){
                $bill_items[$branch_id] = convert_to_array($rows,'created_date',"",false);
            }
        }else{
            $bill_items = convert_to_array($bill_items,'created_date', "", false);
        }

        $item_total = array();
        foreach($item_sale_map as $item_id=>$rows){
            $item_total[$item_id] = (object)array(
                'name' => $rows[0]->item_name,
                'sale_value' => 0,
            );
            foreach($rows as $row)
            {
                $item_total[$item_id]->sale_value += $row->sale_value;
            }
        }
        $data->rows_item = $item_total;

        if($summary){
            $category_data = array();
            foreach($bill_items as $branch_id=>$branch_rows){
                $category_data[$branch_id] = array();
                foreach($branch_rows as $date=>$date_rows){
                    $category_data[$branch_id][$date]['categories'] = pos_render_array_no_dupplicate($date_rows,'category_name', array('sale_value'));
                }

            }

            $total = array('total' => 0,'categories' => array());
            $data->branch_map = $this->load->table_model('branch')->getTableMap('id','');

            foreach($data->branch_map as $branch_id=>$branch_field) {
                $total[$branch_id] = array('total' => 0, 'categories' => array());
            }
            foreach($data->category_map as $category_name=>$category_field){
                $total['categories'][$category_name] = 0;
                foreach($data->branch_map as $branch_id=>$branch_field) {
                    $total[$branch_id]['categories'][$category_name] = 0;
                }
            }

            foreach($category_data as $branch_id=>$branch_rows){
                foreach($branch_rows as $date=>$date_value){
                    $total_value = 0;
                    foreach($date_value['categories'] as $category_name=>$values){
                        $total_value += $values->sale_value;
                        $total['categories'][$category_name] += $values->sale_value;
                        $total[$branch_id]['categories'][$category_name] += $values->sale_value;
                    }
                    $category_data[$branch_id][$date]['total'] = $total_value;
                    $total[$branch_id]['total'] += $total_value;
                    $total['total'] += $total_value;
                }
            }
            $data->rows = $category_data;
        }else{
            $item_data = array();
            $data->category_name = $this->load->table_model('category')->getByID($category_id)->name;
            foreach($bill_items as $key=>$value){
                $item_data[$key]['items'] = pos_render_array_no_dupplicate($value,'item_name', array('sale_value'));
            }
            $total = array('total' => 0);
            foreach($item_data as $date=>$date_value){
                $total_value = 0;
                foreach($date_value['items'] as $item_name=>$values){
                    $total_value += $values->sale_value;
                    if(isset($total[$item_name])){
                        $total[$item_name] += $values->sale_value;
                    }else{
                        $total[$item_name] = $values->sale_value;
                    }
                }
                $item_data[$date]['total'] = $total_value;
                $total['total'] += $total_value;
            }
            $data->rows = $item_data;
        }

        $data->summary = $summary;
        $data->total = $total;
        return $data;
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $category_id = isset($data['category_id'])?$data['category_id']:null;
        $summary = isset($category_id)&&count($category_id)==1?false:true;
        $condition = array();
        if(isset($category_id) && $category_id){
            $condition['category.id'] = $category_id;
        }
        $model = $this->load->table_model('category');
        $branch_model = $this->load->table_model('branch');
        $data = new stdClass();
        $data->category_map = $model->select(array(
                'select'=> array('name'),
                'from' => array('category' => array('table'=>'category')),
                'where' => $condition
            )
        )->result();

        $data->category_map = pos_render_array_no_dupplicate($data->category_map,'name');
        $data->start_date   = get_user_date($start_date);
        $data->end_date     = get_user_date($end_date);

        $bill_items = $model->select(array(
            'select'    =>  array(
                'bill'     => array('created_date'),
                'bill_item'=> array('id','discount_value','discount_type','quantity','price'),
                'item'     => array('item_id' => 'id','item_name' => 'name'),
                'category' => array('category_name' => 'name','category_id' => 'id')
            ),
            'from'      => array(
                'bill'      => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id'),
                'item'      => array('table' => 'item', 'condition' => 'item.id = bill_item.item_id'),
                'category_item' => array('table' => 'category_item', 'condition' => 'item.id = category_item.item_id'),
                'category' => array('table' => 'category','condition' => 'category_item.category_id = category.id')
            ),
            'where'     => array_merge($condition,array("bill.created_date > '{$start_date}'", "bill.created_date < '{$end_date}'")),
            'order'     => 'bill.created_date DESC',
            'group'     => 'bill_item.bill_id,bill_item.item_id,category_item.category_id'
        ))->result();
        $bill_items_credits = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => convert_to_array($bill_items,'','id')),false);
        if(count($bill_items) == 0){
            return $data;
        }

        foreach($bill_items as $bill_item){
            $bill_item->credit_value = 0;
            if(isset($bill_items_credits[$bill_item->id])){
                foreach($bill_items_credits[$bill_item->id] as $bill_item_credit){
                    $bill_item->credit_value += $bill_item_credit->credit_value;
                }
            }
            $bill_item->sale_value = $bill_item->quantity * $bill_item->price - $bill_item->credit_value - calculate_discount($bill_item->discount_type,$bill_item->discount_value,$bill_item->quantity * $bill_item->price);
            $bill_item->created_date = get_user_date($bill_item->created_date);
        }
        $item_sale_map = convert_to_array($bill_items,'item_id','',false);
        if($summary){
            $bill_items = convert_to_array($bill_items,'bill_branch_id', "", false);
            foreach($bill_items as $branch_id=>$rows){
                $bill_items[$branch_id] = convert_to_array($rows,'created_date',"",false);
            }
        }else{
            $bill_items = convert_to_array($bill_items,'created_date', "", false);
        }

        $item_total = array();
        foreach($item_sale_map as $item_id=>$rows){
            $item_total[$item_id] = (object)array(
                'name' => $rows[0]->item_name,
                'sale_value' => 0,
            );
            foreach($rows as $row)
            {
                $item_total[$item_id]->sale_value += $row->sale_value;
            }
        }
        $data->rows_item = $item_total;

        if($summary){
            $category_data = array();
            foreach($bill_items as $branch_id=>$branch_rows){
                $category_data[$branch_id] = array();
                foreach($branch_rows as $date=>$date_rows){
                    $category_data[$branch_id][$date]['categories'] = pos_render_array_no_dupplicate($date_rows,'category_name', array('sale_value'));
                }

            }

            $total = array('total' => 0,'categories' => array());
            $data->branch_map = $this->load->table_model('branch')->getTableMap('id','');

            foreach($data->branch_map as $branch_id=>$branch_field) {
                $total[$branch_id] = array('total' => 0, 'categories' => array());
            }
            foreach($data->category_map as $category_name=>$category_field){
                $total['categories'][$category_name] = 0;
                foreach($data->branch_map as $branch_id=>$branch_field) {
                    $total[$branch_id]['categories'][$category_name] = 0;
                }
            }

            foreach($category_data as $branch_id=>$branch_rows){
                foreach($branch_rows as $date=>$date_value){
                    $total_value = 0;
                    foreach($date_value['categories'] as $category_name=>$values){
                        $total_value += $values->sale_value;
                        $total['categories'][$category_name] += $values->sale_value;
                        $total[$branch_id]['categories'][$category_name] += $values->sale_value;
                    }
                    $category_data[$branch_id][$date]['total'] = $total_value;
                    $total[$branch_id]['total'] += $total_value;
                    $total['total'] += $total_value;
                }
            }
            $data->rows = $category_data;
        }else{
            $item_data = array();
            $data->category_name = $this->load->table_model('category')->getByID($category_id)->name;
            foreach($bill_items as $key=>$value){
                $item_data[$key]['items'] = pos_render_array_no_dupplicate($value,'item_name', array('sale_value'));
            }
            $total = array('total' => 0);
            foreach($item_data as $date=>$date_value){
                $total_value = 0;
                foreach($date_value['items'] as $item_name=>$values){
                    $total_value += $values->sale_value;
                    if(isset($total[$item_name])){
                        $total[$item_name] += $values->sale_value;
                    }else{
                        $total[$item_name] = $values->sale_value;
                    }
                }
                $item_data[$date]['total'] = $total_value;
                $total['total'] += $total_value;
            }
            $data->rows = $item_data;
        }

        $data->summary = $summary;
        $data->total = $total;
        return $data;
    }

    function get_original_data($data,$type = false){
        $content = array();
        $header = array(
            array('export_text' => 'Date')
            );
        if(isset($data->total)){ $total = $data->total;}
        else {$total = '';}
        if(isset($data->branch_name)){ $branch_name = $data->branch_name;}
        else {$branch_name = '';}

        if(isset($data->summary) && $data->summary == true){
            $category_map = $data->category_map;
            foreach($category_map as $key=>$value){
                $header[] = array('export_text' => $key);
            }
            $header[] = array('export_text' => 'Total');
            $content[] = $header;
            if(isset($data->rows) && count($data->rows)){
                foreach($data->rows as $branch_id=>$branch_rows){
                    $content[] = array(
                        array('export_text' => $data->branch_map[$branch_id]->name,
                            'colspan'=>'13',
                            'style'=>'text-decoration: underline;font-weight:bold; color:red'
                        ),

                    );
                    foreach($branch_rows as $date=>$row_value){
                        $content_row = array(
                            array('export_text' =>$date)
                        );
                        foreach($category_map as $key=>$value){
                            $sales_value = isset($row_value['categories'][$key])?$row_value['categories'][$key]->sale_value:0;
                            $content_row[] = array( 'export_text' =>$sales_value,
                                            'export_type' => 'currency',);
                        }
                        $content_row[] = array('export_text' => $row_value['total'],
                            'style'=>'font-weight:bold',
                             'export_type' => 'currency'
                        );
                        $content[] = $content_row;
                    }
                    $content_row = array(array('export_text' =>'Branch Total',
                        'style'=>'font-weight:bold'));
                    foreach($category_map as $key=>$value){
                        $content_row[] = array('export_text' =>$total[$branch_id]['categories'][$key],
                            'style'=>'font-weight:bold',
                            'export_type' => 'currency');
                    }
                    $content_row[] = array('export_text' =>$total[$branch_id]['total'],
                        'style'=>'font-weight:bold',
                        'export_type' => 'currency');
                    $content[] = $content_row;
                }

                $footer = array(array('export_text' =>'Total',
                    'style'=>'font-weight:bold'));
                foreach($category_map as $key=>$value){
                    $footer[] = array(
                        'export_text' =>isset($total['categories'][$key])?$total['categories'][$key]:0,
                        'style'=>'font-weight:bold',
                        'export_type' => 'currency'
                    );
                }
                $footer[] = array('export_text' =>$total['total'],
                    'style'=>'font-weight:bold',
                    'export_type' => 'currency');
                $content[] = $footer;
            }else{
                if($branch_name!='') {
                    $content[] = array(
                        array('export_text' =>$branch_name,
                            'colspan'=>'10'),
                    );
                }
                $content[] = array(
                    array('export_text' =>'There is no current bill',
                        'colspan'=>'10'),
                );
            }
        }else{
            //////////////
            $content[] = array(
                array('export_text' => ''),
                array('export_text' => 'Item Name'),
                array('export_text' =>'Value'),
                );
            if(isset($data->rows) && count($data->rows)){
                $content[] = array(
                    array('export_text' =>$data->category_name,
                        'html_text' => $data->category_name,
                        'style'=>'text-decoration: underline;font-weight:bold; color:red',
                        'colspan'=>'13'
                        ));
                foreach($data->rows as $date=>$row_value){
                    $content[] = array(array('export_text' => $date));
                    foreach($row_value['items'] as $key=>$value){
                        $content[] = array(
                            array('export_text' => ''),
                            array('export_text' => $key),
                            array('export_text' =>$value->sale_value,
                                'export_type' => 'currency')
                            );
                    }
                    $content[] = array(
                        array('export_text' => ''),
                        array('export_text' => 'Total'),
                        array('export_text' =>$row_value['total'],
                            'export_type' => 'currency')
                       );
                }
                $content[] = array(
                    array('export_text' => 'Total'),
                    array('export_text' => $data->total['total'],
                    'export_type' => 'currency')
                   );
            }else{
                if($branch_name!='') {
                    $content[] = array(
                        array('export_text' =>$branch_name,
                            'colspan'=>'10'),
                    );
                }
                $content[] = array(
                    array('export_text' =>'There is no current bill',
                        'colspan'=>'10'),
                    );
            }
        }
        return $content;
    }
}