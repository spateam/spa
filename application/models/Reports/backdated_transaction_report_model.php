<?php
class Backdated_Transaction_report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $branch_id = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));

        $bills = $this->load->table_model('bill')->getTableMap('id','',array('raw_created_date IS NOT NULL'),true,Permission_Value::BRANCH,array('branch_id'=>$branch_id,'branch_group_id' => $branch_group_id));

        $bill_id = convert_to_array($bills,'','id');

        $bill_log_model = $this->load->table_model('bill_log');

        //$backdated_bills = $bill_log_model->getTableMap('id','',array('id' => $bill_id,"log_time <= '{$end_date}'", "log_time >= '{$start_date}'", "log_type = 'backdate'"),false);
        $backdated_bills = $bill_log_model->getTableMap('id','',array('id' => $bill_id,"log_time <= '{$end_date}'", "log_time >= '{$start_date}'", "log_type = 'backdate'"),false,Permission_Value::BRANCH,array()," created_date desc");

        $ret_data = array();
        foreach($backdated_bills as $bill_id=>$bill_logs){
            // Tran Minh Thao -- Added begin
            $ret_data[$bill_id]['bill_id'] = $bills[$bill_id]->id;
            $ret_data[$bill_id]['bill_branch_id'] = $bills[$bill_id]->bill_branch_id;
            // Tran Minh Thao -- Added end
            $ret_data[$bill_id]['code'] = $bills[$bill_id]->code;
            $last_date = '';
            foreach($bill_logs as $log){
                if($last_date == ''){
                    if(isset($log->raw_created_date)){
                        $last_date = $log->raw_created_date;
                    }else{
                        $older_log = $bill_log_model->get(array("log_type < '{$start_date}'", "log_time desc"));
                        if(count($older_log)){
                            $last_date = $older_log->created_date;
                        }
                    }

                }
                $backdate_from = new DateTime($last_date);
                $backdate_to = new DateTime($log->created_date);
                $diff = $backdate_to->diff($backdate_from);
                $range = $diff->format("%d day(s) %h hour(s) %i minutes() %s seconds()");
                $ret_data[$bill_id]['log'][] = array(
                    'backdate_from' => get_user_date($last_date,null,null,true),
                    'backdate_to'   => get_user_date($log->created_date,null,null,true),
                    'backdate_range'=> $range
                );
                $last_date = $log->created_date;
            }
        }

        return array(
            'bills' => $ret_data
        );
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $bills = $this->load->table_model('bill')->getTableMap('id','',array('raw_created_date IS NOT NULL'));

        $bill_id = convert_to_array($bills,'','id');

        $bill_log_model = $this->load->table_model('bill_log');

        //$backdated_bills = $bill_log_model->getTableMap('id','',array('id' => $bill_id,"log_time <= '{$end_date}'", "log_time >= '{$start_date}'", "log_type = 'backdate'"),false);
        $backdated_bills = $bill_log_model->getTableMap('id','',array('id' => $bill_id,"log_time <= '{$end_date}'", "log_time >= '{$start_date}'", "log_type = 'backdate'"),false,Permission_Value::BRANCH,array()," created_date desc");

        $ret_data = array();
        foreach($backdated_bills as $bill_id=>$bill_logs){
            $ret_data[$bill_id]['code'] = $bills[$bill_id]->code;
            $last_date = '';
            foreach($bill_logs as $log){
                if($last_date == ''){
                    if(isset($log->raw_created_date)){
                        $last_date = $log->raw_created_date;
                    }else{
                        $older_log = $bill_log_model->get(array("log_type < '{$start_date}'", "log_time desc"));
                        if(count($older_log)){
                            $last_date = $older_log->created_date;
                        }
                    }

                }
                $backdate_from = new DateTime($last_date);
                $backdate_to = new DateTime($log->created_date);
                $diff = $backdate_to->diff($backdate_from);
                $range = $diff->format("%d day(s) %h hour(s) %i minutes() %s seconds()");
                $ret_data[$bill_id]['log'][] = array(
                    'backdate_from' => get_user_date($last_date,null,null,true),
                    'backdate_to'   => get_user_date($log->created_date,null,null,true),
                    'backdate_range'=> $range
                );
                $last_date = $log->created_date;
            }
        }

        return array(
            'bills' => $ret_data
        );
    }
/*
    function get_export_data($format, $data){
        $bills = $data['bills'];
        $content = array();
        $content[] = array(
            'Backdate From',
            'Backdate To',
            'Backdate Range'
        );
        foreach($bills as $bill_id=>$logs){
            $content[] = array($logs['code']);
            foreach($logs['log'] as $log){
                $content[] = array(
                    $log['backdate_from'],
                    $log['backdate_to'],
                    $log['backdate_range']
                );
            }
        }
        return $content;
    }
*/
    function get_original_data($data,$type = false){
        // Tran Minh Thao added - Begin
        $user_data = $this->session->userdata('login');
        // Tran Minh Thao added - End
        $bills = $data['bills'];
        $content = array();
        $content[] = array(
            array('export_text' => 'Backdate From'),
            array('export_text' => 'Backdate To'),
            array('export_text' => 'Backdate Range'),

        );
        foreach($bills as $bill_id=>$logs){
            /* Tran Minh Thao added - Begin
                Compare user's branch id with bill id to allow user can only view bills in current he/she's branch
             */
            $link = $logs['code'];
            if(isset($user_data->branch_id)) {
                if($user_data->branch_id == $logs['bill_branch_id']) {
                    $link_backdate_bill = site_url('sales/receipt/'.$logs['bill_id']);
                    $link = "<a target='_blank' href='".$link_backdate_bill."'>".$logs['code']."</a>";
                }
            }
            // Tran Minh Thao added - End

            $content[] = array(
                array('export_text' => $link,
                    'style' => 'color:red; font-weight: bold',
                    'colspan'=>'5'));
            foreach($logs['log'] as $log){
                $content[] = array(
                    array('export_text' => $log['backdate_from']),
                    array('export_text' =>  $log['backdate_to']),
                    array('export_text' => $log['backdate_range']),

                );
            }
        }
        return $content;
    }
}