<?php
class Branch_Commission_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($post_data){
        if(isset($post_data['type'])){
            $type = $post_data['type'];
        }else{
            $type = 'detail';
        }
        $summary = isset($post_data['summary']) ? $post_data['summary'] : false;
        $data = new stdClass();
        $start_date     = $post_data['start_date'];
        $end_date       = $post_data['end_date'];
        $employee_id    = isset($post_data['employee_id'])?$post_data['employee_id']:null;
        $item_id        = isset($post_data['item_id'])?$post_data['item_id']:null;
        $category_id    = isset($post_data['category_id'])?$post_data['category_id']:null;
        if($type == 'monthly-branch-total')
            $summary = 1;

        $branch_id = $this->user_check->get_branch_id($post_data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        $condition = array(
            "bill.created_date >= '{$start_date}'",
            "bill.created_date <= '{$end_date}'",
            "bill.status" => BILL_STATUS('Complete'),
            "bill_item.belong_bundle" => 0
        );
        if($employee_id){
            $condition['bill_employee.employee_id'] = $employee_id;
        }
        if($item_id){
            $condition['bill_item.item_id'] = $item_id;
        }
        if($category_id){
            $category_item_id = $this->load->table_model('category_item')->getTableMap('','item_id',array('category_id' => $category_id));
            if(is_array($item_id)){
                $category_item_id = array_merge($category_item_id,$item_id);
            }
            $condition['bill_item.item_id'] = $category_item_id;
            $condition['category_item.category_id'] = $category_id;
        }

        $model = $this->load->table_model('bill');
        $info = array(
            'select' => array(
                'bill'          => array('bill_code' => 'code', 'customer_id', 'status', 'created_date','amount_due','discount','creator','description'),
                'bill_item'     => array('bill_item_id'=>'id', 'bill_id', 'item_id', 'quantity', 'price', 'discount_type', 'discount_value','belong_bundle'),
                'bill_employee' => array('employee_id', 'commission_id', 'commission_type', 'commission_value','sub_item_id','sub_commission_value'),
                'category_item' => array('category_id','item_category_id'=>'item_id'),
            ),
            'from'   => array(
                'bill'          => array('table' => 'bill'),
                'bill_item'     => array('table' => 'bill_item','condition' => 'bill.id = bill_item.bill_id','type' => 'left'),
                'item'          => array('table' => 'item', 'condition' => 'bill_item.item_id = item.id', 'type' => 'left'),
                'bill_employee' => array('table' => 'bill_employee', 'condition' => 'bill_employee.bill_item_id = bill_item.id','type' => 'left'),
                'category_item' => array('table' => 'category_item', 'condition' => 'bill_item.item_id = category_item.item_id','type' => 'left')
            ),
            'where'     => $condition,
            'group'     => 'bill_item.id',//'group'     => 'bill_code',
            'order'     => 'bill.created_date DESC',
            'user_level'=> Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)
        );
        $results = $model->select($info)->result();

        $em_id = array();
        $cust_id = array();
        $tem_id = array();
        $bill_item_ids = array();
        foreach ($results as $key=>$value){
            $bill_item_ids[] = $value->bill_item_id;
            $cust_id[] = $value->customer_id;
            $em_id[] = $value->employee_id;
            $tem_id[] = $value->item_id;
        }
        $bill_credit_map  = $this->load->table_model('bill_item_credit')->getTableMap('bill_item_id','',array('bill_item_id' => $bill_item_ids),false);
        foreach ($results as $key=>$value){
            $value->credit_used = 0;
            if(isset($bill_credit_map[$value->bill_item_id])){
                $value->credit_used = 0;
                foreach($bill_credit_map[$value->bill_item_id] as $row){
                    $value->credit_used += $row->credit_value;
                }
            }
            $results[$key] = $value;
        }
        $data->branch_map = $this->load->table_model('branch')->getTableMap('id','');
        $data->commission_map = $this->load->table_model('commission')->getTableMap('id','');
        $data->category_map = $this->load->table_model('category')->getTableMap('id','');
        $data->employee_map = $this->load->table_model('employee')->getTableMap('id','',array(),true,Permission_Value::ADMIN);
        $data->customer_map = $this->load->table_model('customer')->getTableMap('id','',array('id' => $cust_id));
        $user_session = $this->session->userdata('login');

        if($user_session->type == 4) {
            $data->item_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $tem_id, 'status' => array(1,4)), true, Permission_Value::ADMIN);
        }
        else {
            $data->item_map = $this->load->table_model('item')->getTableMap('id', '', array('id' => $tem_id, 'status' => array(1,4)));
        }

        /** @Tran Minh Thao added - Begin
            Declare item list to get infomation about item
        */
        $branch_list = array();
        /* ------------- @Tran Minh Thao added - End --------------- */
        //$data->results = array();
        $results = $this->mapping_data($results,array(
            'first_function' => function(&$entity,$row_data){
                if(!isset($entity['price'] )){
                    $entity['price'] = 0;
                }
                if(!isset($entity['discount'] )){
                    $entity['discount'] = 0;
                }
                if(!isset($entity['credit_used'] )){
                    $entity['credit_used'] = 0;
                }
                $entity['price'] += $row_data->price*$row_data->quantity;
                $entity['discount'] += $row_data->discount;
                $entity['credit_used'] += $row_data->credit_used;

            },
            'mapping_function'=> array(
                'category_id'    => function(&$entity,$row_data){
                    if(!isset($entity['price'] )){
                        $entity['price'] = 0;
                    }
                    if(!isset($entity['discount'] )){
                        $entity['discount'] = 0;
                    }
                    if(!isset($entity['credit_used'] )){
                        $entity['credit_used'] = 0;
                    }
                    $entity['price'] += $row_data->price*$row_data->quantity;
                    $entity['discount'] += $row_data->discount;
                    $entity['credit_used'] += $row_data->credit_used;
                },
                'bill_branch_id' => function(&$entity,$row_data){
                    if(!isset($entity['price'] )){
                        $entity['price'] = 0;
                    }
                    if(!isset($entity['discount'] )){
                        $entity['discount'] = 0;
                    }
                    if(!isset($entity['credit_used'] )){
                        $entity['credit_used'] = 0;
                    }
                    $entity['price'] += $row_data->price*$row_data->quantity;
                    $entity['discount'] += $row_data->discount;
                    $entity['credit_used'] += $row_data->credit_used;
                },
                'employee_id'    => function(&$entity,$row_data){
                    if(!isset($entity['price'] )){
                        $entity['price'] = 0;
                    }
                    if(!isset($entity['discount'] )){
                        $entity['discount'] = 0;
                    }
                    if(!isset($entity['credit_used'] )){
                        $entity['credit_used'] = 0;
                    }
                    $entity['price'] += $row_data->price*$row_data->quantity;
                    $entity['discount'] += $row_data->discount;
                    $entity['credit_used'] += $row_data->credit_used;

                },
                'bill_id' => function(&$entity,&$row_data){
                    if(!isset($entity['price'] )){
                        $entity['price'] = 0;
                    }
                    if(!isset($entity['discount'] )){
                        $entity['discount'] = 0;
                    }
                    if(!isset($entity['credit_used'] )){
                        $entity['credit_used'] = 0;
                    }
                    $entity['price'] += $row_data->price*$row_data->quantity;
                    $entity['discount'] += $row_data->discount;
                    $entity['credit_used'] += $row_data->credit_used;
                },
                'bill_item_id' => function(&$entity,&$row_data){
                    if(!isset($entity['price'] )){
                        $entity['price'] = 0;
                    }
                    if(!isset($entity['discount'] )){
                        $entity['discount'] = 0;
                    }
                    if(!isset($entity['credit_used'] )){
                        $entity['credit_used'] = 0;
                    }
                    //if(!isset($entity['']))
                    $entity['price'] += $row_data->price*$row_data->quantity;
                    $entity['discount'] += $row_data->discount;
                    $entity['credit_used'] += $row_data->credit_used;
                },
            )
        ),false);
        if(count($results) != 0)
        {
            foreach ($results['rows'] as $key=>$category){
                foreach ($category['rows'] as $key1=>$branch){
                    /** @Tran Minh Thao added - Begin */
                    if(!isset($branch_list[$key1]))
                        $branch_list[$key1] = array();
                    /* ------------- @Tran Minh Thao added - End --------------- */

                    foreach ($branch['rows'] as $key2=>$staff){
                        /** @Tran Minh Thao added - Begin */
                        if(!isset($branch_list[$key1][$key2]))
                            $branch_list[$key1][$key2] = array();
                        /* ------------- @Tran Minh Thao added - End --------------- */

                        foreach ($staff['rows'] as $key3=>$bill_id){
                            /** @Tran Minh Thao added - Begin */
                            if(!isset($branch_list[$key1][$key2][$key3]))
                                $branch_list[$key1][$key2][$key3] = array();
                            /* ------------- @Tran Minh Thao added - End --------------- */

                            foreach($bill_id['rows'] as $key6=>$bill_item) {
                                $emp_num = 0;
                                $em_array = array();
                                foreach ($bill_item['rows'] as $key4 => $bill) {
                                    if(isset($data->item_map[$bill->item_id])) {
                                        $bill->item_type = $data->item_map[$bill->item_id]->type;
                                    }
                                    $emp_num++;
                                    $em_array[] = $bill->employee_id;
                                }
                                foreach ($bill_item['rows'] as $key4 => $bill) {
                                    $total = $bill->quantity * $bill->price;
                                    if(isset($bill->sub_item_id) && $bill->sub_item_id > 0){
                                        if($bill->commission_value > 0){
                                            $bill->commission_value = 0;
                                        }
                                        $bill->commission_value += $bill->sub_commission_value;
                                    }
                                    else {
                                        $bill->commission_value = calculate_commission($bill->commission_type, $bill->commission_value, $total, $emp_num, $bill->sub_commission_value, $bill->sub_item_id);
                                    }
                                    $bill->employee_array = $em_array;
                                }

                                /** @Tran Minh Thao added - Begin
                                    If isset bill items have the same bill id and do not duplicate, push it to array
                                */

                                $temp = '';
                                foreach($bill_item['rows'] as $item){
                                    if($temp == ''){
                                        $temp = $item;
                                    }
                                    else{
                                        if(isset($item->sub_commission_value) && isset($item->sub_item_id)){
                                            $temp->commission_value += $item->sub_commission_value;
                                        }
                                    }
                                }
                                $bill_item['rows'][0] = $temp;
                                if(!isset($branch_list[$key1][$key2][$key3][$key6]))
                                    $branch_list[$key1][$key2][$key3][$key6] = $bill_item;
                                /* ------------- @Tran Minh Thao added - End --------------- */
                            }
                        }
                    }
                }
            }
        }
        /** @Tran Minh Thao added - Begin
            Change branch list to as format as $result to export data
        */
        if(!empty($branch_list)) {
            $temp_list = $branch_list;
            $branch_list = array();
            $branch_list["price"] = 0;
            $branch_list["discount"] = 0;
            $branch_list["credit_used"] = 0;
            $branch_list["rows"][0]["price"] = 0;
            $branch_list["rows"][0]["discount"] = 0;
            $branch_list["rows"][0]["credit_used"] = 0;
            $branch_list["rows"][0]["rows"] = array();

            foreach ($temp_list as $key => $branch) {
                $branch_list["rows"][0]["rows"][$key]["price"] = 0;
                $branch_list["rows"][0]["rows"][$key]["discount"] = 0;
                $branch_list["rows"][0]["rows"][$key]["credit_used"] = 0;
                $branch_list["rows"][0]["rows"][$key]["rows"] = array();

                foreach ($branch as $key1 => $staff) {
                    $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["price"] = 0;
                    $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["discount"] = 0;
                    $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["credit_used"] = 0;
                    $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"] = array();

                    foreach ($staff as $key2 => $bill) {
                        $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["price"] = 0;
                        $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["discount"] = 0;
                        $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["credit_used"] = 0;
                        $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["rows"] = array();

                        foreach ($bill as $key3 => $bill_item) {
                            $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["rows"][$key3] = $bill_item;

                            // Calculate sum of the bill
                            $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["price"] += $bill_item["price"];
                            $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["discount"] += $bill_item["discount"];
                            $branch_list["rows"][0]["rows"][$key]["rows"][$key1]["rows"][$key2]["credit_used"] += $bill_item["credit_used"];
                        }
                    }
                }
            }
        }
        /* ------------- @Tran Minh Thao added - End --------------- */

        $data->types  = $type;
        $data->summary = $summary;
        $data->results = $results;

        /** @Tran Minh Thao added - Begin
            If type is Daily Summary Report, use new data from $branch_list
        */
        if($data->types == 'item')
            $data->results = $branch_list;
        /* ------------- @Tran Minh Thao added - End --------------- */

        return $data;
    }


    function get_original_data($data,$type = false){
        $content = array();
        $test = count($data->results);
        $timezone = $this->user_check->get('timezone');
        if($test != 0) {
            $total_commission = 0;
            $total_sale = 0;
            $total_quantity = 0;
            if (($data->types == 'detail' && $data->summary == false) || $data->types == 'daily' || $data->types == 'monthly' || $data->types == 'monthly-branch-total') {
                $branches_list = array();
                $content[] = array(
                    array('export_text' => 'Bill No.'),
                    array('export_text' => 'Date'),
                    array('export_text' => 'Item No.'),
                    array('export_text' => 'Description'),
                    array('export_text' => 'Customer ID'),
                    array('export_text' => 'Customer Name'),
                    array('export_text' => 'Quantity'),
                    array('export_text' => 'Sale'),
                    array('export_text' => 'Commission'));
                foreach ($data->results['rows'] as $key => $category) {
                    if (isset($data->category_map[$key])) {
                        if($data->types != 'monthly-branch-total')
                            $content[] = array($this->to_export_text($data->category_map[$key]->name,1,10));
                    }
                    $commission_category = 0;
                    $sale_category = 0;
                    $quantity_category = 0;
                    foreach ($category['rows'] as $key1 => $branch) {
                        if (isset($data->branch_map[$key1])) {
                            if($data->types != 'monthly-branch-total')
                                $content[] = array($this->to_export_text($data->branch_map[$key1]->name,2,10));
                            if(!isset($branches_list[$data->branch_map[$key1]->name])){
                                $branches_list[$data->branch_map[$key1]->name] = array();
                            }
                        }
                        $commission_branch = 0;
                        $sale_branch = 0;
                        $quantity_branch = 0;
                        foreach ($branch['rows'] as $key2 => $staff) {
                            if (isset($data->employee_map[$key2])) {
                                if($data->types != 'monthly-branch-total')
                                    $content[] = array($this->to_export_text(to_full_name($data->employee_map[$key2]->first_name, $data->employee_map[$key2]->last_name),3,10));
                            }
                            $show_date = "";
                            $day_total_quantity = 0;
                            $day_total_sales = 0;
                            $day_total_commission = 0;
                            $show_month = "";
                            $new_date = "";
                            $month_total_quantity = 0;
                            $month_total_sales = 0;
                            $month_total_commission = 0;

                            $commission_staff = 0;
                            $sale_staff = 0;
                            $quantity_staff = 0;
                            foreach ($staff['rows'] as $key3 => $bill_id) {
                                $commission_bill = 0;
                                $sale_bill = 0;
                                $quantity_bill = 0;
                                foreach ($bill_id['rows'] as $key4 => $item_bill) {
                                    $commission_item = 0;
                                    $sale_item = 0;

                                    $old_bill_id = 0;
                                    $old_item_id = 0;
                                    $old_bill_item_id = 0;

                                    foreach ($item_bill['rows'] as $key5 => $bill) {
                                        $bill_date = get_user_date($bill->created_date,$timezone);
                                        $bill_month = explode("/", $bill_date);
                                        $bill_month = $bill_month[0]."-".$bill_month[1]."-".$bill_month[2];
                                        $bill_month = date("F, Y", strtotime($bill_month));
                                        if(isset($bill->item_type) && $bill->item_type == 4 && $old_bill_id == $bill->bill_id && $old_item_id == $bill->item_id && $old_bill_item_id == $bill->bill_item_id)
                                            break;

                                        if (isset($data->customer_map[$bill->customer_id])) {
                                            $customer_code = $data->customer_map[$bill->customer_id]->code;
                                            $customer_name = to_full_name($data->customer_map[$bill->customer_id]->first_name, $data->customer_map[$bill->customer_id]->last_name);
                                        }
                                        if (isset($data->commission_map[$bill->commission_id])) {
                                            $commission_name = $data->commission_map[$bill->commission_id]->name;
                                        }
                                        if (isset($data->item_map[$bill->item_id])) {
                                            $item_name = $data->item_map[$bill->item_id]->name;
                                            $item_code = $data->item_map[$bill->item_id]->code;
                                        }
                                        $count = count($bill->employee_array);
                                        if ($count == 1) {
                                            $emp_name = '';
                                            if (isset($data->employee_map[$bill->employee_array[0]])) {
                                                $emp_name = to_full_name($data->employee_map[$bill->employee_array[0]]->first_name, $data->employee_map[$bill->employee_array[0]]->last_name);
                                            }
                                        } else {
                                            $emp_name = '';
                                            foreach ($bill->employee_array as $value => $emp) {
                                                if (isset($data->employee_map[$bill->employee_array[$value]])) {
                                                    $emp_name .= to_full_name($data->employee_map[$bill->employee_array[$value]]->first_name, $data->employee_map[$bill->employee_array[$value]]->last_name);
                                                    $emp_name .= ', ';
                                                }
                                            }
                                        }

                                        $commission_item += $bill->commission_value;
                                        $sale_item += $bill->quantity * $bill->price  - $item_bill['credit_used'] - calculate_discount($bill->discount_type,$bill->discount_value,$bill->quantity * $bill->price);
                                        
                                        if($show_date != "" && $show_date != $bill_date){
                                            if($data->summary == false){
                                                if($data->types == "daily"){
                                                    $content[] = array(
                                                        array('export_text' => 'SubTotal','colspan' => 6,'style' => 'color: #337AB7;text-align: right'),
                                                        array('export_text' => $day_total_quantity,'style' => 'color: #337AB7'),
                                                        array('export_text' => $day_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                        array('export_text' => $day_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                    );
                                                }
                                                else{
                                                    if($data->types == "monthly"){
                                                        $content[] = array(
                                                            array('export_text' => $show_date,'colspan' => 6),
                                                            array('export_text' => $day_total_quantity),
                                                            array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                                            array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                                        );
                                                    }
                                                }
                                            }
                                            else{
                                                if($data->types == "daily"){
                                                    $content[] = array(
                                                        array('export_text' => $show_date,'colspan' => 6),
                                                        array('export_text' => $day_total_quantity),
                                                        array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                                        array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                                    );
                                                }
                                            }

                                            $month_total_quantity += $day_total_quantity;
                                            $month_total_sales += $day_total_sales;
                                            $month_total_commission += $day_total_commission;
                                            $day_total_quantity = 0;
                                            $day_total_sales = 0;
                                            $day_total_commission = 0;
                                        }

                                        if($show_month != "" && $show_month != $bill_month){
                                            if($data->types == "monthly"){
                                                if($data->types != 'monthly-branch-total'){
                                                    if($data->summary == false){
                                                        $content[] = array(
                                                            array('export_text' => 'SubTotal','colspan' => 6,'style' => 'color: #337AB7;text-align: right'),
                                                            array('export_text' => $month_total_quantity,'style' => 'color: #337AB7'),
                                                            array('export_text' => $month_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                            array('export_text' => $month_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                                        );
                                                    }
                                                    else{
                                                        $content[] = array(
                                                            array('export_text' => $show_month,'colspan' => 6),
                                                            array('export_text' => $month_total_quantity),
                                                            array('export_text' => $month_total_sales,'export_type' => 'currency'),
                                                            array('export_text' => $month_total_commission,'export_type' => 'currency'),
                                                        );
                                                    }
                                                }
                                            }

                                            $branches_list[$data->branch_map[$key1]->name][$show_month]["quantity"] += $month_total_quantity;
                                            $branches_list[$data->branch_map[$key1]->name][$show_month]["sale"] += $month_total_sales;
                                            $branches_list[$data->branch_map[$key1]->name][$show_month]["commission"] += $month_total_commission;
                                            $month_total_quantity = 0;
                                            $month_total_sales = 0;
                                            $month_total_commission = 0;
                                        }

                                        if($data->summary == false){
                                            if($data->types == "daily" || $data->types == "detail"){
                                                $content[] = array(
                                                    array('export_text' => $bill->bill_code),
                                                    array('export_text' => get_user_date($bill->created_date,$timezone)),
                                                    array('export_text' => $item_code),
                                                    array('export_text' => $item_name),
                                                    array('export_text' => $customer_code),
                                                    array('export_text' => $customer_name),
                                                    array('export_text' => $bill->quantity),
                                                    array('export_text' => $sale_item,
                                                        'export_type' => 'currency'),
                                                    array('export_text' => $bill->commission_value,
                                                        'export_type' => 'currency'),
                                                );
                                            }
                                        }

                                        $day_total_quantity += $bill->quantity;
                                        $day_total_sales += $sale_item;
                                        $day_total_commission += $bill->commission_value;
                                        $show_date = $bill_date;
                                        $show_month = $bill_month;
                                        if(!isset($branches_list[$data->branch_map[$key1]->name][$show_month])){
                                            $branches_list[$data->branch_map[$key1]->name][$show_month] = array();
                                            $branches_list[$data->branch_map[$key1]->name][$show_month]["quantity"] = 0;
                                            $branches_list[$data->branch_map[$key1]->name][$show_month]["sale"] = 0;
                                            $branches_list[$data->branch_map[$key1]->name][$show_month]["commission"] = 0;

                                        }

                                        $old_bill_id = $bill->bill_id;
                                        $old_item_id = $bill->item_id;
                                        $old_bill_item_id = $bill->bill_item_id;
                                    }
                                    $commission_bill += $commission_item;
                                    $sale_bill += $sale_item;
                                    $quantity_bill += $bill->quantity;
                                }
                                $commission_staff += $commission_bill;
                                $sale_staff += $sale_bill;
                                $quantity_staff += $quantity_bill;
                            }
                            if($show_date != ""){
                                if($data->summary == false){
                                    if($data->types == "daily"){
                                        $content[] = array(
                                            array('export_text' => 'SubTotal','colspan' => 6,'style' => 'color: #337AB7;text-align: right'),
                                            array('export_text' => $day_total_quantity,'style' => 'color: #337AB7'),
                                            array('export_text' => $day_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                            array('export_text' => $day_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                        );
                                    }
                                    else{
                                        if($data->types == "monthly"){
                                            $content[] = array(
                                                array('export_text' => $show_date,'colspan' => 6),
                                                array('export_text' => $day_total_quantity),
                                                array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                                array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                            );
                                        }
                                    }
                                }
                                else{
                                    if($data->types == "daily"){
                                        $content[] = array(
                                            array('export_text' => $show_date,'colspan' => 6),
                                            array('export_text' => $day_total_quantity),
                                            array('export_text' => $day_total_sales,'export_type' => 'currency'),
                                            array('export_text' => $day_total_commission,'export_type' => 'currency'),
                                        );
                                    }
                                    else{
                                        if($data->types != 'monthly-branch-total'){
                                            $content[] = array(
                                                array('export_text' => $show_month,'colspan' => 6),
                                                array('export_text' => $month_total_quantity+$day_total_quantity),
                                                array('export_text' => $month_total_sales+$day_total_sales,'export_type' => 'currency'),
                                                array('export_text' => $month_total_commission+$day_total_commission,'export_type' => 'currency'),
                                            );
                                        }
                                    }
                                }
                                $show_date = "";
                            }

                            if($show_month != ""){
                                if($data->types == "monthly" && $data->summary == false){
                                    $content[] = array(
                                        array('export_text' => 'SubTotal','colspan' => 6,'style' => 'color: #337AB7;text-align: right'),
                                        array('export_text' => $month_total_quantity+$day_total_quantity,'style' => 'color: #337AB7'),
                                        array('export_text' => $month_total_sales+$day_total_sales,'export_type' => 'currency','style' => 'color: #337AB7'),
                                        array('export_text' => $month_total_commission+$day_total_commission,'export_type' => 'currency','style' => 'color: #337AB7'),
                                    );
                                }
                                $branches_list[$data->branch_map[$key1]->name][$show_month]["quantity"] += $month_total_quantity+$day_total_quantity;
                                $branches_list[$data->branch_map[$key1]->name][$show_month]["sale"] += $month_total_sales+$day_total_sales;
                                $branches_list[$data->branch_map[$key1]->name][$show_month]["commission"] += $month_total_commission+$day_total_commission;
                                $show_month = "";
                            }

                            if($data->types != 'monthly-branch-total'){
                                $content[] = array(
                                    $this->to_export_text('Staff Total',3,6),
                                    $this->to_export_text($quantity_staff,3,1,1),
                                    $this->to_export_text($sale_staff,3,1,1,'currency'),
                                    $this->to_export_text($commission_staff,3,1,1,'currency'),
                                );
                                $content[] = array($this->to_export_text('',0,10));
                            }
                            $commission_branch += $commission_staff;
                            $sale_branch += $sale_staff;
                            $quantity_branch +=$quantity_staff;
                        }
                        $commission_category += $commission_branch;
                        $sale_category += $sale_branch;
                        $quantity_category += $quantity_branch;
                    }
                    if($data->types != 'monthly-branch-total'){
                        $content[] = array(
                            $this->to_export_text('',0,10),
                        );
                    }
                    $total_commission += $commission_category;
                    $total_sale += $sale_category;
                    $total_quantity += $quantity_category;
                }
                if($data->types == 'monthly-branch-total'){
                    foreach($branches_list as $key => $branch){
                        $content[] = array($this->to_export_text($key,2,10));
                        $branch_quantity = 0;
                        $branch_sale = 0;
                        $branch_commission = 0;
                        foreach($branch as $month => $value){
                            $content[] = array(
                                array('export_text' => $month,'colspan' => 6),
                                array('export_text' => $value["quantity"]),
                                array('export_text' => $value["sale"],'export_type' => 'currency'),
                                array('export_text' => $value["commission"],'export_type' => 'currency'),
                            );
                            $branch_quantity += $value["quantity"];
                            $branch_sale += $value["sale"];
                            $branch_commission += $value["commission"];
                        }

                        $content[] = array(
                            $this->to_export_text('Branch Total',3,6),
                            $this->to_export_text($branch_quantity,3,1,1),
                            $this->to_export_text($branch_sale,3,1,1,'currency'),
                            $this->to_export_text($branch_commission,3,1,1,'currency'),
                        );
                        $content[] = array(
                            $this->to_export_text('',0,10),
                        );
                    }
                }
                $content[] = array(
                    $this->to_export_text('Grand Total',0,6),
                    $this->to_export_text($total_quantity,0,1,1),
                    $this->to_export_text($total_sale,0,1,1,'currency'),
                    $this->to_export_text($total_commission,0,1,1,'currency'),
                );
            }
            else {
                if($data->types == 'item'){
                    /** @Tran Minh Thao edited 2015/08/07 - Refortmat daily summary report form - Begin */
                    $content[] = array(
                        $this->to_export_text("",3,5,1),
                    );
                    /* ------------- @Tran Minh Thao edited - End --------------- */
                }else{
                    $content[] = array(
                        $this->to_export_text('Employee Name',0),
                        $this->to_export_text('Quantity',0),
                        $this->to_export_text('Sale',0),
                        $this->to_export_text('Commission',0));
                }
                $grand_total = 0;
                foreach ($data->results['rows'] as $key => $category) {
                    if (isset($data->category_map[$key])) {
                        $content[] = array(
                            $this->to_export_text($data->category_map[$key]->name,1,4),
                        );
                    }
                    $commission_category = 0;
                    $sale_category = 0;
                    $quantity_category = 0;
                    $flag = 1;
                    foreach ($category['rows'] as $key1 => $branch) {
                        if (isset($data->branch_map[$key1])) {
                            // Tran Minh Thao added - 2015/08/07 - reformat the daily summary report form -- begin
                            if($data->types == 'item') {
                                if($flag) {
                                    $content[] = array(
                                        $this->to_export_text($data->branch_map[$key1]->name,2,1,1),
                                        $this->to_export_text('Item',2,1,1),
                                        $this->to_export_text('Sale',2,1,1),
                                        $this->to_export_text('Quantity',2,1,1),
                                        $this->to_export_text('Commission',2,1,1));

                                    $flag = 0;
                                }
                                else {
                                    $content[] = array(
                                        $this->to_export_text($data->branch_map[$key1]->name,2,5,1));
                                }
                            }
                            else {
                                $content[] = array(
                                    $this->to_export_text($data->branch_map[$key1]->name,2,4,1));
                            }
                            // -- End
                        }
                        $commission_branch = 0;
                        $sale_branch = 0;
                        $quantity_branch = 0;
                        foreach ($branch['rows'] as $key2 => $staff) {
                            $daily_summary_data = array();
                            $commission_staff = 0;
                            $sale_staff = 0;
                            $quantity_staff = 0;
                            foreach ($staff['rows'] as $key3 => $bill_id) {
                                $commission_bill = 0;
                                $sale_bill = 0;
                                $quantity_bill = 0;
                                foreach ($bill_id['rows'] as $key4 => $item_bill) {
                                    $commission_item = 0;
                                    $sale_item = 0;
                                    $quantity_item = 0;

                                    $old_bill_id = 0;
                                    $old_item_id = 0;
                                    $old_bill_item_id = 0;
                                    foreach ($item_bill['rows'] as $key5 => $bill) {
                                        if(isset($bill->item_type) && $bill->item_type == 4 && $old_bill_id == $bill->bill_id && $old_item_id == $bill->item_id && $old_bill_item_id == $bill->bill_item_id)
                                            break;

                                        $commission_item += (isset($bill->commission_value) ? $bill->commission_value : 0);
                                        $sale_item += $bill->quantity * $bill->price - $item_bill['credit_used'] - calculate_discount($bill->discount_type,$bill->discount_value,$bill->quantity * $bill->price);
                                        $quantity_item += $bill->quantity;

                                        /** @Tran Minh Thao added - Begin
                                            Declare item list to get infomation about item
                                        */
                                        if(isset($data->item_map[$bill->item_id])) {
                                            $bill_item["bill_code"] = $bill->bill_code;
                                            $bill_item["item_code"] = $data->item_map[$bill->item_id]->code;
                                            $bill_item["name"] = $data->item_map[$bill->item_id]->name;
                                            $bill_item["sales"] = $sale_item;
                                            $bill_item["quantity"] = $bill->quantity;
                                            $bill_item["commission"] = $bill->commission_value;
                                        }
                                        /* ------------- @Tran Minh Thao added - End --------------- */

                                        $old_bill_id = $bill->bill_id;
                                        $old_item_id = $bill->item_id;
                                        $old_bill_item_id = $bill->bill_item_id;
                                    }
                                    $commission_bill += $commission_item;
                                    $sale_bill += $sale_item;
                                    $quantity_bill += $quantity_item;
                                    if($data->types == 'item'){
                                        $user_created_date = get_user_date($bill->created_date,$timezone);
                                        if(!isset($daily_summary_data[$user_created_date])){
                                            $daily_summary_data[$user_created_date] = array('commission' => 0, 'sale' => 0);
                                        }
                                        $daily_summary_data[$user_created_date]['commission'] += $commission_item;
                                        $daily_summary_data[$user_created_date]['sale'] += $sale_item;
                                        /** @Tran Minh Thao added - Begin */
                                        //$daily_summary_data[$user_created_date]['item_list'][] = $bill_item;
                                        if(!isset($daily_summary_data['item_list'][$bill_item["item_code"]])) {
                                            $daily_summary_data['item_list'][$bill_item["item_code"]] = $bill_item;
                                            $daily_summary_data['item_list'][$bill_item["item_code"]]["item_quantity"] = $bill->quantity;
                                            $daily_summary_data['item_list'][$bill_item["item_code"]]["item_sales"] = $bill_item["sales"];
                                            $daily_summary_data['item_list'][$bill_item["item_code"]]["item_commission"] = $bill_item["commission"];
                                        }
                                        else {
                                            $daily_summary_data['item_list'][$bill_item["item_code"]]["item_quantity"] += $bill->quantity;
                                            $daily_summary_data['item_list'][$bill_item["item_code"]]["item_sales"] += $bill_item["sales"];
                                            $daily_summary_data['item_list'][$bill_item["item_code"]]["item_commission"] += $bill_item["commission"];
                                        }
                                        /* ------------- @Tran Minh Thao added - End --------------- */
                                    }
                                }
                                $commission_staff += $commission_bill;
                                $sale_staff += $sale_bill;
                                $quantity_staff += $quantity_bill;
                            }

                            if (isset($data->employee_map[$key2])) {
                                if($data->types == 'item'){
                                    //ksort($daily_summary_data);
                                    $sum_total = 0;
                                    $subflag = 1;

                                    /** @Tran Minh Thao added - Begin
                                        Loop array to get item of bills
                                        Edited - 2015/08/07 - reformat the daily summary report form
                                    */
                                    $sum_quantity = 0;
                                    foreach($daily_summary_data['item_list'] as $item) {
                                        if($subflag) {
                                            $first_name = isset($data->employee_map[$key2]->first_name)?$data->employee_map[$key2]->first_name:"";
                                            $last_name = isset($data->employee_map[$key2]->last_name)?$data->employee_map[$key2]->last_name:"";
                                            $content[] = array(
                                                $this->to_export_text(to_full_name($first_name, $last_name),3,1,1),
                                                $this->to_export_text($item["name"],'',1,1),
                                                $this->to_export_text($item['item_sales'],'',1,1,'currency'),
                                                $this->to_export_text($item['item_quantity'],'',1,1),
                                                $this->to_export_text($item['item_commission'],'',1,1,'currency'),
                                            );

                                            $subflag = 0;
                                        }
                                        else {
                                            $content[] = array(
                                                $this->to_export_text("",'',1,1),
                                                $this->to_export_text($item["name"],'',1,1),
                                                $this->to_export_text($item['item_sales'],'',1,1,'currency'),
                                                $this->to_export_text($item['item_quantity'],'',1,1),
                                                $this->to_export_text($item['item_commission'],'',1,1,'currency'),
                                            );
                                        }

                                        $sum_quantity += $item['item_quantity'];
                                    }
                                    $sum_total += $sum_quantity;
                                    /* ------------- @Tran Minh Thao added - End --------------- */

                                    $content[] = array(
                                        $this->to_export_text('Sub Total','',2,1),
                                        $this->to_export_text($sale_staff,3,1,1,'currency'),
                                        $this->to_export_text($sum_total,3,1,1),
                                        $this->to_export_text($commission_staff,3,1,1,'currency'),
                                    );
                                    $grand_total += $sum_total;
                                    $content[] = array($this->to_export_text(''));
                                }else{
                                    $content[] = array(
                                        $this->to_export_text(to_full_name($data->employee_map[$key2]->first_name, $data->employee_map[$key2]->last_name)),
                                        $this->to_export_text($quantity_staff,5,1,1),
                                        $this->to_export_text($sale_staff,5,1,1,'currency'),
                                        $this->to_export_text($commission_staff,5,1,1,'currency'),
                                    );
                                }
                            }
                            $commission_branch += $commission_staff;
                            $sale_branch += $sale_staff;
                            $quantity_branch += $quantity_staff;
                        }
                        $commission_category += $commission_branch;
                        $sale_category += $sale_branch;
                        $quantity_category += $quantity_branch;
                    }
                    $total_commission += $commission_category;
                    $total_sale += $sale_category;
                    $total_quantity += $quantity_category;
                }

                if($data->types == 'item') {
                    $content[] = array(
                        $this->to_export_text('Grand Total',0,2,1),
                        $this->to_export_text($total_sale,0,0,1,'currency'),
                        $this->to_export_text($grand_total,0,1,1),
                        $this->to_export_text($total_commission,0,0,1,'currency'),
                    );
                }
                else {
                    $content[] = array(
                        $this->to_export_text('Grand Total',0,1,1),
                        $this->to_export_text($total_quantity,0,0,1),
                        $this->to_export_text($total_sale,0,0,1,'currency'),
                        $this->to_export_text($total_commission,0,0,1,'currency'),
                    );
                }
            }
        }
        else {
            $content[] = array(
                array('export_text' => 'No Data',
                    'colspan' => '20'),);
        }

        return $content;
    }
}