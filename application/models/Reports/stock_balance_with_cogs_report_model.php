<?php
class Stock_Balance_With_Cogs_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    /**
     * @param $data
     * @return array
     */
    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $item_id    = $data['item_id'];
        $sortby     = isset($data['sortby'])?$data['sortby']:'date';
        $branch_id  = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        $condition = array();
        $item_condition['type'] = ITEM_TYPE('Product');
        if($item_id != 0){
            $condition['item_id'] = $item_id;
            $item_condition['id'] = $item_id;
        }

        $warehouse_id = $this->load->table_model('warehouse_item')->get_warehouse(array(),Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));
        $ret_data = array();
        if($warehouse_id){
            $condition['warehouse_id']  = $warehouse_id;
            $condition[]                = "log_time < '$start_date'";
            $log_data = $this->load->table_model('warehouse_item_log')->get($condition,'log_time desc');
            foreach($log_data as $row){
                if(!isset($ret_data[$row->id])){
                    $ret_data[$row->id] = $row;
                }else{
                    continue;
                }
            }
        }
        $items = $this->load->table_model('item')->getTableMap('id','',$item_condition,true,array());

        $ret_data_2 = array();
        if(count($ret_data)){
            foreach($ret_data as $row_id=>$row_values){
                if(!isset($items[$row_values->item_id])){
                    continue;
                }
                $item = $items[$row_values->item_id];
                if(isset($ret_data_2[$item->id])){
                    $ret_data_2[$item->id]->quantity += $row_values->quantity;
                }else{
                    $ret_data[$row_id]->item_name = $item->name;
                    $ret_data[$row_id]->item_code = $item->code;
                    $ret_data[$row_id]->product_cost = $item->price;
                    $ret_data[$row_id]->unit_cost = $item->product->unit_cost;
                    $ret_data[$row_id]->log_time = get_user_date($row_values->log_time,'','',true);
                    $ret_data_2[$item->id] = $ret_data[$row_id];
                }

            }
        }else if($item_id){
            $item = $items[$item_id];
            $ret_data_2[$item_id] = (object)array(
                'item_name'     => $item->name,
                'item_code'     => $item->code,
                'product_cost'  => $item->price,
                'unit_cost'     => $item->product->unit_cost,
                'quantity'      => 0,
                'log_time'      => 'There is no transaction for this item'
            );
        }


        $item_product_list = $this->load->table_model('item_product')->getTableMap('item_id','',array());

        foreach($ret_data_2 as $item){
            if(isset($item_product_list[$item->item_id]->master_quantity)) {
                $item->master_quantity = $item_product_list[$item->item_id]->master_quantity;
            }
            else{
                $item->master_quantity = 'ERROR';
            }
        }

        if($sortby == "name")
            usort($ret_data_2, array($this, "cmp_obj_name"));
        elseif($sortby == "code")
            usort($ret_data_2, array($this, "cmp_obj_code"));

        return array(
            'stock_items' => $ret_data_2
        );
    }
    function get_original_data($data,$type = false){
        $stock_items = $data['stock_items'];
        $content = array();
        $content[] = array(
            array('export_text' => 'Item Code'),
            array('export_text' => 'Item Name'),
            array('export_text' => 'Unit Cost'),
            array('export_text' => 'Unit Price'),
            array('export_text' => 'Master Quantity'),
            array('export_text' => 'Quantity'),
            array('export_text' => 'Total Unit Cost'),
            array('export_text' => 'Total Unit Price'),
            array('export_text' => 'Last Transaction')

        );
        if(count($stock_items)){
            $total = 0;
            $grand_total = 0;
            $grand_total_product = 0;
            $quantity_grandtotal = 0;
            $master_quantity_grandtotal = 0;

            foreach($stock_items as $warehouse_item_id=>$warehouse_item_fields){
                //$unit_cost = isset($warehouse_item_fields->unit_cost)?$warehouse_item_fields->unit_cost* UNIT_COGS_MULTIPLE('Value'):0;
                $unit_cost = isset($warehouse_item_fields->unit_cost)?$warehouse_item_fields->unit_cost:0;
                $total = $unit_cost!=0?$unit_cost*$warehouse_item_fields->quantity:0;
                $product_cost = isset($warehouse_item_fields->product_cost)?$warehouse_item_fields->product_cost:0;
                $total_product_cost = $product_cost!=0?$product_cost*$warehouse_item_fields->quantity:0;
                $grand_total += $total;
                $grand_total_product += $total_product_cost;
                $quantity_grandtotal += $warehouse_item_fields->quantity;
                $master_quantity_grandtotal += $warehouse_item_fields->master_quantity;

                $content[] = array(
                    array('export_text' =>  $warehouse_item_fields->item_code),
                    array('export_text' => $warehouse_item_fields->item_name),
                    array('export_text' => $unit_cost,'export_type' => 'currency'),
                    array('export_text' => $warehouse_item_fields->product_cost,'export_type' => 'currency'),
                    array('export_text' =>$warehouse_item_fields->master_quantity),
                    array('export_text' =>$warehouse_item_fields->quantity),
                    array('export_text' => $total,'export_type' => 'currency'),
                    array('export_text' => $total_product_cost,'export_type' => 'currency'),
                    array('export_text' =>  $warehouse_item_fields->log_time)
                );
            }
            $content[] = array(
                $this->to_export_text('Grand Total',1,4),
                $this->to_export_text($master_quantity_grandtotal,3,1,1),
                $this->to_export_text($quantity_grandtotal,3,1,1),
                $this->to_export_text($grand_total,3,1,1,'currency'),
                $this->to_export_text($grand_total_product,3,2,1,'currency'),
            );
        }else{
            $content[] = array( array('export_text' => 'There is no record',
                'colspan'=>'12'),
            );
        }
        return $content;
    }

    function cmp_obj_name($a, $b){
        return strcmp($a->item_name, $b->item_name);
    }

    function cmp_obj_code($a, $b){
        return strcmp($a->item_code, $b->item_code);
    }
}