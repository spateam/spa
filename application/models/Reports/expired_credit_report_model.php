<?php
class Expired_Credit_Report_Model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $summary = isset($data['summary'])?$data['summary']:false;
        $branch_group_id = $data['branch_group_id'];
        $condition = array();
        if($branch_group_id){
            $condition['credit_branch_group.branch_group_id'] = $branch_group_id;
        }

        $packages = $this->load->table_model('credit')->select(array(
            'select'        => array(
                'credit'        => '*',
                'credit_branch_group'  => array('branch_group_id' => 'branch_group_id')
            ),
            'from'          => array(
                'credit'                => array('table' => 'credit'),
                'credit_branch_group'   => array('table' => 'credit_branch_group', 'condition' => 'credit.id = credit_branch_group.credit_id')
            ),
            'user_level'    => Permission_Value::ADMIN,
            'where'         => array_merge($condition,array("credit.end_date <= '{$end_date}'", "credit.end_date > '{$start_date}'")),
            'order'         => 'credit.end_date desc',
        ))->result();
        $branch_group = $this->load->table_model('branch_group')->getTableMap('id','',array('id' => convert_to_array($packages,'','branch_group_id')));
        $packages_id = convert_to_array($packages, '', 'item_id');

        $items = $this->db->select('id, name')->get('item')->result();
        $temp = array();
        foreach($items as $item){
            $temp[$item->id] = $item;
        }
        $items = $temp;

     //   $items = $this->load->table_model('item')->getTableMap('id','',array('id' => $packages_id),true,Permission_Value::ADMIN);
        $customers = $this->load->table_model('customer')->getTableMap('id','',array('id' => convert_to_array($packages,'','customer_id'), 'customer.status' => array(1,0)));
        $unused_credit = 0;
        $date_map = array();
        $timezone = $this->system_config->get('timezone');
        foreach($packages as $key=>$package){
            $item = $items[$package->item_id];
            if(! isset($customers[$package->customer_id])){
                continue;
            }
            $package->customer_name = to_full_name($customers[$package->customer_id]->first_name,$customers[$package->customer_id]->last_name);
            $package->code_name = $customers[$package->customer_id]->code;
            $package->customer_email    = $customers[$package->customer_id]->email;
            $package->mobile_number     = $customers[$package->customer_id]->mobile_number;
            $package->name  = $item->name;
            if(!$branch_group_id){
                $package->name  .=" ({$branch_group[$package->branch_group_id]->name})";
            }
            $package->package_id    = $item->id;
            $package->branch_group  = $branch_group[$package->branch_group_id];
            $unused_credit += $package->credit;
            $user_package_end_date = get_user_date($package->end_date,$timezone);
            $package->end_date = get_user_date($package->end_date,'','Y-m-d H:i:s');
            if(!isset($date_map[$user_package_end_date])){
                $date_map[$user_package_end_date] = array();
            }
            $date_map[$user_package_end_date][] = $package;
        }
        $total = array('unused_credit' => number_format($unused_credit,2,'.',','));

        return array('packages' => $date_map, 'total' => $total, 'summary' => $summary);

    }
    function get_original_data($data,$type = false){
        $summary = $data['summary'];
        $content = array();
        if(!$summary){
            $content[] = array(
                array('export_text' => 'Customer Name'),
                array('export_text' => 'Client ID'),
                array('export_text' => 'Credit Name'),
                array('export_text' => 'Credit Balance'),
                array('export_text' => 'Mobile Number'),
                array('export_text' => 'Expiry Date'),
            );
        }else{
            $content[] = array(
                array('export_text' => 'Date'),
                array('export_text' => 'Credit Balance'),
            );
        }

        foreach($data['packages'] as $date=>$package_list){
            $total_credit=0;
            foreach($package_list as $package){
                if(!$summary){
                    $content[] = array(
                        array('export_text' =>  $package->customer_name),
                        array('export_text' => '<a target="_blank" href="'.base_url().'customers/credit_history_report/'.$package->customer_id.'">'.$package->code_name.'</a>'),
                        array('export_text' => $package->name),
                        array('export_text' => number_format($package->credit,2,'.',',')),
                        array('export_text' =>  $package->mobile_number),
                        array('export_text' =>  $package->end_date),

                    );
                }
                $total_credit += number_format($package->credit,2,'.',',');
            }
            if($total_credit != 0 && $summary){
                $content[] = array(
                    array('export_text' => $date),
                    array('export_text' => number_format($total_credit,2,'.',',')),
                );
            }
        }
        $content[] = array(
            array('export_text' =>  'Total',
                'style'=>'text-align: right; font-weight: bold'),
            array('export_text' => $data['total']['unused_credit'],
                'style'=>'font-weight: bold',
                'colspan'=>'10'),
        );
        return $content;
    }
}