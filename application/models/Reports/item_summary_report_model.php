<?php
class Item_Summary_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

//    function admin_get_detail_data($post_data){
//        $temp_data = $post_data;
//        $raw_branch_id = isset($post_data['branch_id']) ? $post_data['branch_id'] : '';
//        $branch_id = ($this->user_check->get_branch_id($post_data['branch_id']));
//        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
//        $branch_data = $this->load->table_model('branch')->getTableMap('id','',array('id' => $branch_id));
//
//        $post_data = array(
//            "bill.created_date > '{$temp_data['start_date']}'",
//            "bill.created_date < '{$temp_data['end_date']}'",
//        );
//        if(isset($temp_data['item_id']) && $temp_data['item_id']){
//            $post_data['bill_item.item_id'] = $temp_data['item_id'];
//        }else{
//            $post_data['bill_item.item_id'] = $this->load->table_model('item')->getTableMap('','id');
//        }
//
//        $list_item_id = isset($post_data['bill_item.item_id'])?$post_data['bill_item.item_id']:null;
//        $list_item_id = is_array($list_item_id)?$list_item_id:array($list_item_id);
//        $bills = $this->load->table_model('bill')->get_detail($post_data,array(
//            'item'          => true,
//            'customer'      => true,
//        ),Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));
//
//        $item_detail_data = array();
//        $item_summary_data = array();
//        foreach($bills as $bill_id => $bill_fields){
//            foreach($bill_fields->bill_items as $bill_item){
//                if($list_item_id && ! in_array($bill_item->item_id,$list_item_id)){
//                    continue;
//                }
//                if(! isset($item_detail_data[$bill_item->item_id])){
//                    $item_detail_data[$bill_item->item_id] = array();
//                }
//                $credit_used = 0;
//                foreach($bill_item->credit_used as $credit_used_row){
//                    $credit_used += $credit_used_row->credit_value;
//                }
//                $item_detail_data[$bill_item->item_id][] = array(
//                    'bill_code'     => $bill_fields->code,
//                    'branch_id'     => $bill_fields->bill_branch_id,
//                    'created_date'  => $bill_fields->created_date,
//                    'item_name'     => $bill_item->item_detail->name,
//                    'price'         => $bill_item->price,
//                    'credit_used'   => $credit_used,
//                    'credit_add'    => $bill_item->credit_add,
//                    'quantity'      => $bill_item->quantity,
//                    'discount'      => calculate_discount($bill_item->discount_type,$bill_item->discount_value,$bill_item->quantity*$bill_item->price),
//                    'sales_value'   => $bill_item->quantity*$bill_item->price
//                );
//            }
//        }
//
//        if($raw_branch_id){
//            foreach($item_detail_data as $item_id=>$item_detail){
//                if(! isset($item_summary_data[$item_id]))
//                {
//                    $item_summary_data[$item_id] = array(
//                        'rows' => array(),
//                        'name' => $item_detail[0]['item_name'],
//                    );
//                }
//
//                foreach($item_detail as $row){
//                    $format = BASE_DATE_FORMAT;
//                    $key = get_user_date($row['created_date'],'',$format);
//                    if(isset($item_summary_data[$item_id]['rows'][$key])){
//                        $row_summary = $item_summary_data[$item_id]['rows'][$key];
//                        $row_summary['quantity']    += $row['quantity'];
//                        $row_summary['discount']    += $row['discount'];
//                        $row_summary['credit_used'] += $row['credit_used'];
//                        $row_summary['credit_add']  += $row['credit_add'];
//                        $row_summary['sales_value'] += $row['sales_value'];
//                        $item_summary_data[$item_id]['rows'][$key] = $row_summary;
//                    }else{
//                        $row_summary = array();
//                        $row_summary['quantity']    = $row['quantity'];
//                        $row_summary['discount']    = $row['discount'];
//                        $row_summary['credit_used'] = $row['credit_used'];
//                        $row_summary['credit_add']  = $row['credit_add'];
//                        $row_summary['sales_value'] = $row['sales_value'];
//                        $row_summary['price']       = $row['price'];
//                        $item_summary_data[$item_id]['rows'][$key] = $row_summary;
//                    }
//                }
//            }
//
//            foreach($item_summary_data as $item_id=>$summary_field){
//                $total = array(
//                    'quantity' => 0,
//                    'discount' => 0,
//                    'credit_used' => 0,
//                    'credit_add' => 0,
//                    'sales_value' => 0,
//                );
//                foreach($summary_field['rows'] as $row){
//                    $total['quantity']    += $row['quantity'];
//                    $total['discount']    += $row['discount'];
//                    $total['credit_used'] += $row['credit_used'];
//                    $total['credit_add']  += $row['credit_add'];
//                    $total['sales_value'] += $row['sales_value'];
//                }
//                $item_summary_data[$item_id]['total'] = $total;
//            }
//        }else{
//            foreach($item_detail_data as $item_id=>$item_detail){
//                if(! isset($item_summary_data[$item_id]))
//                {
//                    $item_summary_data[$item_id] = array(
//                        'branch_rows'   => array(),
//                        'total'         => array(
//                            'quantity'      => 0,
//                            'discount'      => 0,
//                            'credit_used'   => 0,
//                            'credit_add'    => 0,
//                            'sales_value'   => 0
//                        ),
//                        'name'          => $item_detail[0]['item_name'],
//                    );
//                    foreach($branch_id as $each){
//                        $item_summary_data[$item_id]['branch_rows'][$each] = array(
//                            'quantity'      => 0,
//                            'discount'      => 0,
//                            'credit_used'   => 0,
//                            'credit_add'    => 0,
//                            'sales_value'   => 0
//                        );
//                    }
//                }
//
//                foreach($item_detail as $row){
//                    /* ----- By Branch ---- */
//                    $key = $row['branch_id'];
//                    $row_summary = &$item_summary_data[$item_id]['branch_rows'][$key];
//                    $row_summary['quantity']    += $row['quantity'];
//                    $row_summary['discount']    += $row['discount'];
//                    $row_summary['credit_used'] += $row['credit_used'];
//                    $row_summary['credit_add']  += $row['credit_add'];
//                    $row_summary['sales_value'] += $row['sales_value'];
//                    /* ------ By Item ------*/
//                    $total_summary = &$item_summary_data[$item_id]['total'];
//                    $total_summary['quantity']    += $row['quantity'];
//                    $total_summary['discount']    += $row['discount'];
//                    $total_summary['credit_used'] += $row['credit_used'];
//                    $total_summary['credit_add']  += $row['credit_add'];
//                    $total_summary['sales_value'] += $row['sales_value'];
//                }
//            }
//            return array('item_summary_data' => $item_summary_data, 'branch_data' => $branch_data , 'multi_branch' => true);
//        }
//
//        return array('item_summary_data' => $item_summary_data);
//    }

    function admin_get_detail_data($post_data){
        if(isset($post_data['search_condition'])){
            $post_data = (array)json_decode($post_data['search_condition']);
        }else{
            $temp_data = $post_data;
            $post_data = array(
                "bill.created_date > '{$temp_data['start_date']}'",
                "bill.created_date < '{$temp_data['end_date']}'",
            );
            if(isset($temp_data['item_id']) && $temp_data['item_id']){
                $post_data['bill_item.item_id'] = $temp_data['item_id'];
            }else{
                $post_data['bill_item.item_id'] = $this->load->table_model('item')->getTableMap('','id');
            }
        }

        $list_item_id = isset($post_data['bill_item.item_id'])?$post_data['bill_item.item_id']:null;
        $bills = $this->load->table_model('bill')->get_detail($post_data,array(
            'item'          => true,
            'customer'      => true,
        ));

        $item_detail_data = array();
        $item_summary_data = array();
        foreach($bills as $bill_id => $bill_fields){
            foreach($bill_fields->bill_items as $bill_item){
                if($list_item_id && ! in_array($bill_item->item_id,$list_item_id)){
                    continue;
                }
                if(! isset($item_detail_data[$bill_item->item_id])){
                    $item_detail_data[$bill_item->item_id] = array();
                }
                $credit_used = 0;
                foreach($bill_item->credit_used as $credit_used_row){
                    $credit_used += $credit_used_row->credit_value;
                }
                $item_detail_data[$bill_item->item_id][] = array(
                    'bill_code'     => $bill_fields->code,
                    'created_date'  => $bill_fields->created_date,
                    'item_name'     => $bill_item->item_detail->name,
                    'price'         => $bill_item->price,
                    'credit_used'   => $credit_used,
                    'credit_add'    => $bill_item->credit_add,
                    'quantity'      => $bill_item->quantity,
                    'discount'      => calculate_discount($bill_item->discount_type,$bill_item->discount_value,$bill_item->quantity*$bill_item->price),
                    'sales_value'   => $bill_item->quantity*$bill_item->price
                );
            }
        }
        foreach($item_detail_data as $item_id=>$item_detail){
            if(! isset($item_summary_data[$item_id]))
            {
                $item_summary_data[$item_id] = array(
                    'rows' => array(),
                    'name' => $item_detail[0]['item_name'],
                );
            }

            foreach($item_detail as $row){
                $format = BASE_DATE_FORMAT;
                $key = get_user_date($row['created_date'],'',$format);
                if(isset($item_summary_data[$item_id]['rows'][$key])){
                    $row_summary = $item_summary_data[$item_id]['rows'][$key];
                    $row_summary['quantity']    += $row['quantity'];
                    $row_summary['discount']    += $row['discount'];
                    $row_summary['credit_used'] += $row['credit_used'];
                    $row_summary['credit_add']  += $row['credit_add'];
                    $row_summary['sales_value'] += $row['sales_value'];
                    $item_summary_data[$item_id]['rows'][$key] = $row_summary;
                }else{
                    $row_summary = array();
                    $row_summary['quantity']    = $row['quantity'];
                    $row_summary['discount']    = $row['discount'];
                    $row_summary['credit_used'] = $row['credit_used'];
                    $row_summary['credit_add']  = $row['credit_add'];
                    $row_summary['sales_value'] = $row['sales_value'];
                    $row_summary['price']       = $row['price'];
                    $item_summary_data[$item_id]['rows'][$key] = $row_summary;
                }
            }
        }

        foreach($item_summary_data as $item_id=>$summary_field){
            $total = array(
                'quantity' => 0,
                'discount' => 0,
                'credit_used' => 0,
                'credit_add' => 0,
                'sales_value' => 0,
            );
            foreach($summary_field['rows'] as $row){
                $total['quantity']    += $row['quantity'];
                $total['discount']    += $row['discount'];
                $total['credit_used'] += $row['credit_used'];
                $total['credit_add']  += $row['credit_add'];
                $total['sales_value'] += $row['sales_value'];
            }
            $item_summary_data[$item_id]['total'] = $total;
        }

        return array('item_summary_data' => $item_summary_data);
    }
/*
    function get_export_data($format, $data){
        $item_summary_data = $data['item_summary_data'];
        $content = array();
        if(count($item_summary_data)){
            if(!isset($data['multi_branch']) || !$data['multi_branch']){
                foreach($item_summary_data as $item_id=>$item_fields) {
                    $content[] = array($item_fields['name']);
                    $content[] = array('Date', 'Price', 'Quantity', 'Discount', 'Sales', 'Redeem-', 'Redeem+');
                    foreach ($item_fields['rows'] as $date => $row) {
                        $content[] = array($date, $row['price'], $row['quantity'], $row['discount'], $row['sales_value'], $row['credit_used'], $row['credit_add']);
                    }
                    $total = $item_fields['total'];
                    $content[] = array('', 'Total', $total['quantity'], $total['discount'], $total['sales_value'], $total['credit_used'], $total['credit_add']);
                }
            }else{
                $branch_data = $data['branch_data'];
                $header = array('');
                foreach($item_summary_data as $item_id=>$item_field){
                    $header[] = $item_field['name'];
                }
                $content[] = $header;
                foreach($branch_data as $branch_id=>$branch_field){
                    $content_row = array($branch_field->name);
                    foreach($item_summary_data as $item_id=>$item_field){
                        $content_row[] = $item_field['branch_rows'][$branch_id]['sales_value'];
                    }
                    $content[] = $content_row;
                }
                $footer = array('Total');
                foreach($item_summary_data as $item_id=>$item_field){
                    $footer[] = $item_field['total']['sales_value'];
                }
                $content[] = $footer;
            }
        }else{
            $content[] = array('No content');
        }

        return $content;
    }
*/
    function get_original_data($data,$type = false){
        $item_summary_data = $data['item_summary_data'];
        $content = array();
        if(count($item_summary_data)){
            if(!isset($data['multi_branch']) || !$data['multi_branch']){
                $cols = 10;
                foreach($item_summary_data as $item_id=>$item_fields) {
                    $content[] = array(
                        array('export_text' => $item_fields['name'],
                            'colspan'=> $cols,
                            'style'=>'color: red; font-weight:bold'),
                    );
                    $content[] = array(
                        array('export_text' => 'Date',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Price',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Quantity',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Discount',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Sales',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Redeem-',
                            'style'=>'font-weight:bold'),
                        array('export_text' => 'Redeem+',
                            'style'=>'font-weight:bold'),
                    );
                    foreach ($item_fields['rows'] as $date => $row) {
                        $content[] = array(
                            array('export_text' => $date),
                            array('export_text' => $row['price'],
                                'export_type'=>'currency'),
                            array('export_text' => $row['quantity']),
                            array('export_text' => $row['discount'],
                                'export_type'=>'currency'),
                            array('export_text' => $row['sales_value'],
                                'export_type'=>'currency'),
                            array('export_text' =>  $row['credit_used']),
                            array('export_text' =>  $row['credit_add']),
                        );
                    }
                    $total = $item_fields['total'];
                    $content[] = array(
                        array('export_text' => 'Total',
                            'colspan'=>'2',
                            'style'=>'text-align: right'),
                        array('export_text' => $total['quantity']),
                        array('export_text' => $total['discount'],
                            'export_type'=>'currency'),
                        array('export_text' =>  $total['sales_value'],
                            'export_type'=>'currency'),
                        array('export_text' => $total['credit_used']),
                        array('export_text' =>  $total['credit_add']),

                    );
                }
            }else{
                $cols = count($item_summary_data) + 1;
                $branch_data = $data['branch_data'];
                $header = array(
                    array('export_text' =>'',
                        'style'=>'width:10%'));
                foreach($item_summary_data as $item_id=>$item_field){
                    $header[] = array('export_text' =>$item_field['name']);
                }
                $content[] = $header;
                foreach($branch_data as $branch_id=>$branch_field){
                    $content_row = array(array('export_text' =>$branch_field->name,
                        'html_class'=>'title_cell'));
                    foreach($item_summary_data as $item_id=>$item_field){
                        $content_row[] = array('export_text' =>$item_field['branch_rows'][$branch_id]['sales_value'],
                            'export_type'=>'currency');
                    }
                    $content[] = $content_row;
                }
                $footer = array(array('export_text' =>'Total',
                    'html_class'=>'title_cell'));
                foreach($item_summary_data as $item_id=>$item_field){
                    $footer[] = array('export_text' =>$item_field['total']['sales_value'],
                        'export_type'=>'currency');
                }
                $content[] = $footer;
            }
        }else{
            $content[] = array(array('export_text' =>'No content',
                'colspan'=>'10'),
                );
        }

        return $content;
    }

}