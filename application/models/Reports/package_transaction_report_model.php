<?php
class Package_Transaction_Report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $customer_id  = isset($data['customer_id'])?$data['customer_id']:null;

        $condition = array("bill.created_date > '{$start_date}'");
        $branch_id          = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));
        if($customer_id!=null){
            $condition['bill.customer_id'] = $customer_id;
        }

        $bills = $this->load->table_model('bill')->get_detail($condition,array('item' => true,'employee' => true),Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));
        $bills_no_key = array();
        foreach($bills as $key=>$bill){
            $bills_no_key[] = (object)$bill;
        }
        $credits = $this->load->table_model('credit')->getTableMap('id',null,array('customer_id'=>convert_to_array($bills,'','customer_id')),true,Permission_Value::ADMIN);
        $credit_adjustment = $this->load->table_model('credit_adjustment_log')->get_detail(array(
            "log_time < '{$end_date}'",
            'credit_adjustment_log.credit_id' => convert_to_array($credits,'','id')),'log_time desc');
        $credit_transaction = array();
        $items      = $this->load->table_model('item')->getTableMap('id','',array('id' => convert_to_array($credits,'','item_id')));
        $customers  = $this->load->table_model('customer')->getTableMap('id','',array('id' => convert_to_array($credits,'','customer_id')));

        $credit_flow = array_sort_asc($credit_adjustment,$bills_no_key,'log_time','created_date','desc','object');
        $timezone = $this->system_config->get('timezone');
        foreach($credits as $credit_id=>$credit_fields){
            if(!isset($customers[$credit_fields->customer_id])){
                unset($credits[$credit_id]); continue;
            }
            $credit_transaction[$credit_id] = array(
                'package_name' => $items[$credit_fields->item_id]->name,
                'customer_name'=> to_full_name($customers[$credit_fields->customer_id]->first_name,$customers[$credit_fields->customer_id]->last_name),
                'customer_code'=> $customers[$credit_fields->customer_id]->code,
                'credit_value' => $credit_fields->credit,
                'start_date'   => get_user_date($credit_fields->start_date,$timezone,'',true),
                'end_date'     => get_user_date($credit_fields->end_date,$timezone,'',true),
                'rows' => array()
            );
        }

        $bill_credits = array();
        $i = 0;
        foreach($credit_flow as $credit_flow_fields){
            if(isset($credit_flow_fields->created_date)){
                $bill_id = $credit_flow_fields->id;
                $bill_credits[$bill_id]['created_date'] = $credit_flow_fields->created_date;
                foreach($credit_flow_fields->bill_items as $bill_item){
                    $employee_name = array();
                    foreach($bill_item->employees as $employee){
                        $employee_name[] = to_full_name($employee->employee_detail->first_name,$employee->employee_detail->last_name);
                    }

                    if(isset($bill_item->credit_add_id)){
                        $redeem = 0;
                        if($bill_item->credit_add_id>0){
                            $redeem = $bill_item->price * $bill_item->quantity;
                        }

                        $bill_credits[$bill_id]['credit_add'] = array(
                            'credit_id'     => $bill_item->credit_add_id,
                            'credit_value'  =>  $redeem,
                            'item_name'     => $bill_item->item_detail->name,
                            'employee_name' => implode(', ',$employee_name),
                            'bill_code'     => $credit_flow_fields->code,
                            'price'         => $bill_item->price,
                            'qty'           => $bill_item->quantity
                        );
                    }else if(isset($bill_item->credit_used) && count($bill_item->credit_used)){
                        foreach($bill_item->credit_used as $credit_fields){
                            $bill_credits[$bill_id]['credit_used'][] = array(
                                'credit_id'     => $credit_fields->credit_id,
                                'credit_value'  => $credit_fields->credit_value,
                                'item_name'     => $bill_item->item_detail->name,
                                'employee_name' => implode(', ',$employee_name),
                                'bill_code'     => $credit_flow_fields->code,
                                'price'         => $bill_item->price,
                                'qty'           => $bill_item->quantity
                            );
                        }
                    }
                }
            }else if(isset($credit_flow_fields->log_time)){
                $j = $i+1;
                $n = count($credit_flow);
                if($j < $n){
                    while(isset($credit_flow[$j]->created_date) == false && $j < $n - 2){
                        $j++;
                    }
                }else{
                    $j = $n - 1;
                }
                $log_reason = 'Admin Adjustment';
                if(isset($credit_flow_fields->credit_id_transfer) && $credit_flow_fields->credit_id_transfer){
                    $log_reason = 'Credit Transfer';
                }
                $bill_id = $credit_flow[$j]->id;
                $bill_credits[$bill_id]['code'] = $log_reason;
                $bill_credits[$bill_id]['created_date'] = $credit_flow_fields->log_time;
                if($credit_flow_fields->credit_change > 0){
                    $bill_credits[$bill_id]['credit_add'] = array(
                        'credit_id'     => $credit_flow_fields->id,
                        'credit_value'  => $credit_flow_fields->credit_change,
                        'item_name'     => $log_reason,
                        'employee_name' => $log_reason,
                        'bill_code'     => $log_reason,
                        'price'         => $log_reason,
                        'qty'           => $log_reason,
                    );
                }else{
                    $bill_credits[$bill_id]['credit_used'][] = array(
                        'credit_id'     => $credit_flow_fields->id,
                        'credit_value'  => -$credit_flow_fields->credit_change,
                        'item_name'     => $log_reason,
                        'employee_name' => $log_reason,
                        'bill_code'     => $log_reason,
                        'price'         => $log_reason,
                        'qty'           => $log_reason,
                    );
                }
            }
            $i++;
        }

        foreach($bill_credits as $bill_id=>$bill_fields){
            if(isset($bill_fields['credit_used'])){
                foreach($bill_fields['credit_used'] as $credit_fields){
                    if(!isset($credits[$credit_fields['credit_id']])){
                        continue;
                    }
                    $after  = $credits[$credit_fields['credit_id']]->credit;
                    $before = $credits[$credit_fields['credit_id']]->credit + $credit_fields['credit_value'];
                    $credits[$credit_fields['credit_id']]->credit = $before;
                    if($bill_fields['created_date'] < $end_date) {
                        if(!isset($credit_fields['price'])){
                            $a = 1;
                        }
                        $credit_transaction[$credit_fields['credit_id']]['rows'][] = array(
                            'after'         => $after,
                            'before'        => $before,
                            'code'          => $credit_fields['bill_code'],
                            'created_date'  => get_user_date($bill_fields['created_date'],$timezone,'',true),
                            'item_name'     => $credit_fields['item_name'],
                            'employee_name' => $credit_fields['employee_name'],
                            'redeem+'       => '0',
                            'redeem-'       => $credit_fields['credit_value'],
                            'price'         => $credit_fields['price'],
                            'qty'           => $credit_fields['qty']

                        );
                    }
                }
            }
            if(isset($bill_fields['credit_add'])){
                if(!isset($credits[$bill_fields['credit_add']['credit_id']])){
                    continue;
                }
                $after  = ($credits[$bill_fields['credit_add']['credit_id']]->credit);
                $temp = floatval($bill_fields['credit_add']['credit_value']);
                $before = round($after - $temp, 2);
                $credits[$bill_fields['credit_add']['credit_id']]->credit = $before;
                if($bill_fields['created_date'] < $end_date){
                    $credit_transaction[$bill_fields['credit_add']['credit_id']]['rows'][] = array(
                        'after'         => $after,
                        'before'        => $before,
                        'code'          => $bill_fields['credit_add']['bill_code'],
                        'created_date'  => get_user_date($bill_fields['created_date'],$timezone,'',true),
                        'item_name'     => $bill_fields['credit_add']['item_name'],
                        'employee_name' => $bill_fields['credit_add']['employee_name'],
                        'redeem+'       => $bill_fields['credit_add']['credit_value'],
                        'redeem-'       => '0',
                        'price'         => $bill_fields['credit_add']['price'],
                        'qty'           => $bill_fields['credit_add']['qty']
                    );
                }
            }
        }

        foreach($credit_transaction as $credit_id=>$credit_fields){
            $summary_transaction = array(
                'before' => 0,
                'after'  => 0,
            );
            if(count($credit_fields['rows'])){
                $summary_transaction['after']  = $credit_fields['rows'][0]['after'];
                $summary_transaction['before']   = $credit_fields['rows'][count($credit_fields['rows'])-1]['before'];
            }
            $credit_transaction[$credit_id]['summary'] = $summary_transaction;
        }
        $ret_data = array('credit_transaction' => convert_to_array($credit_transaction,'customer_code','',false));
        return $ret_data;
    }

    function get_detail_data($data){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $customer_id = $data['customer_id'];
        $condition = array("bill.created_date > '{$start_date}'");
        if($customer_id){
            $condition['bill.customer_id'] = $customer_id;
        }
        $bills = $this->load->table_model('bill')->get_detail($condition,array('item' => true,'employee' => true));
        $bills_no_key = array();
        foreach($bills as $key=>$bill){
            $bills_no_key[] = (object)$bill;
        }
        $credits = $this->load->table_model('credit')->getTableMap('id',null,array('customer_id'=>convert_to_array($bills,'','customer_id')),true,Permission_Value::ADMIN);
        $credit_adjustment = $this->load->table_model('credit_adjustment_log')->get_detail(array(
            "log_time < '{$end_date}'",
            'credit_adjustment_log.credit_id' => convert_to_array($credits,'','id')),'log_time desc');
        $credit_transaction = array();
        $items      = $this->load->table_model('item')->getTableMap('id','',array('id' => convert_to_array($credits,'','item_id')));
        $customers  = $this->load->table_model('customer')->getTableMap('id','',array('id' => convert_to_array($credits,'','customer_id')));

        $credit_flow = array_sort_asc($credit_adjustment,$bills_no_key,'log_time','created_date','desc','object');

        foreach($credits as $credit_id=>$credit_fields){
            if(!isset($customers[$credit_fields->customer_id])){
                unset($credits[$credit_id]); continue;
            }
            $credit_transaction[$credit_id] = array(
                'package_name' => $items[$credit_fields->item_id]->name,
                'customer_name'=> to_full_name($customers[$credit_fields->customer_id]->first_name,$customers[$credit_fields->customer_id]->last_name),
                'customer_code'=> $customers[$credit_fields->customer_id]->code,
                'credit_value' => $credit_fields->credit,
                'start_date'   => get_user_date($credit_fields->start_date,$this->system_config->get('timezone'),'',true),
                'end_date'     => get_user_date($credit_fields->end_date,$this->system_config->get('timezone'),'',true),
                'rows' => array()
            );
        }

        $bill_credits = array();
        $i = 0;
        foreach($credit_flow as $credit_flow_fields){
            if(isset($credit_flow_fields->created_date)){
                $bill_id = $credit_flow_fields->id;
                $bill_credits[$bill_id]['created_date'] = $credit_flow_fields->created_date;
                foreach($credit_flow_fields->bill_items as $bill_item){
                    $employee_name = array();
                    foreach($bill_item->employees as $employee){
                        $employee_name[] = to_full_name($employee->employee_detail->first_name,$employee->employee_detail->last_name);
                    }
                    if(isset($bill_item->credit_add_id)){
                        $bill_credits[$bill_id]['credit_add'] = array(
                            'credit_id'     => $bill_item->credit_add_id,
                            'credit_value'  => $bill_item->credit_add,
                            'item_name'     => $bill_item->item_detail->name,
                            'employee_name' => implode(', ',$employee_name),
                            'bill_code'     => $credit_flow_fields->code
                        );
                    }else if(isset($bill_item->credit_used) && count($bill_item->credit_used)){
                        foreach($bill_item->credit_used as $credit_fields){
                            $bill_credits[$bill_id]['credit_used'][] = array(
                                'credit_id'     => $credit_fields->credit_id,
                                'credit_value'  => $credit_fields->credit_value,
                                'item_name'     => $bill_item->item_detail->name,
                                'employee_name' => implode(', ',$employee_name),
                                'bill_code'     => $credit_flow_fields->code
                            );
                        }
                    }
                }
            }else if(isset($credit_flow_fields->log_time)){
                $j = $i+1;
                while(isset($credit_flow[$j]->created_date) == false ){
                    $j++;
                }
                $log_reason = 'Admin Adjustment';
                if(isset($credit_flow_fields->credit_id_transfer) && $credit_flow_fields->credit_id_transfer){
                    $log_reason = 'Credit Transfer';
                }
                $bill_id = $credit_flow[$j]->id;
                $bill_credits[$bill_id]['code'] = $log_reason;
                $bill_credits[$bill_id]['created_date'] = $credit_flow_fields->log_time;
                if($credit_flow_fields->credit_change > 0){
                    $bill_credits[$bill_id]['credit_add'] = array(
                        'credit_id'     => $credit_flow_fields->id,
                        'credit_value'  => $credit_flow_fields->credit_change,
                        'item_name'     => $log_reason,
                        'employee_name' => $log_reason,
                        'bill_code'     => $log_reason,
                        'price'         => $log_reason,
                        'qty'           => $log_reason,
                    );
                }else{
                    $bill_credits[$bill_id]['credit_used'][] = array(
                        'credit_id'     => $credit_flow_fields->id,
                        'credit_value'  => -$credit_flow_fields->credit_change,
                        'item_name'     => $log_reason,
                        'employee_name' => $log_reason,
                        'bill_code'     => $log_reason,
                        'price'         => $log_reason,
                        'qty'           => $log_reason
                    );
                }
            }
            $i++;
        }

        foreach($bill_credits as $bill_id=>$bill_fields){
            if(isset($bill_fields['credit_used'])){
                foreach($bill_fields['credit_used'] as $credit_fields){
                    if(!isset($credits[$credit_fields['credit_id']])){
                        continue;
                    }
                    $after  = $credits[$credit_fields['credit_id']]->credit;
                    $before = $credits[$credit_fields['credit_id']]->credit + $credit_fields['credit_value'];
                    $credits[$credit_fields['credit_id']]->credit = $before;
                    if($bill_fields['created_date'] < $end_date) {
                        $credit_transaction[$credit_fields['credit_id']]['rows'][] = array(
                            'after'         => $after,
                            'before'        => $before,
                            'code'          => $credit_fields['bill_code'],
                            'created_date'  => get_user_date($bill_fields['created_date'],$this->system_config->get('timezone'),'',true),
                            'item_name'     => $credit_fields['item_name'],
                            'employee_name' => $credit_fields['employee_name'],
                        );
                    }
                }
            }
            if(isset($bill_fields['credit_add'])){
                if(!isset($credits[$bill_fields['credit_add']['credit_id']])){
                    continue;
                }
                $after  = ($credits[$bill_fields['credit_add']['credit_id']]->credit);
                $temp = floatval($bill_fields['credit_add']['credit_value']);
                $before = round($after - $temp, 2);
                $credits[$bill_fields['credit_add']['credit_id']]->credit = $before;
                if($bill_fields['created_date'] < $end_date){
                    $credit_transaction[$bill_fields['credit_add']['credit_id']]['rows'][] = array(
                        'after'         => $after,
                        'before'        => $before,
                        'code'          => $bill_fields['credit_add']['bill_code'],
                        'created_date'  => get_user_date($bill_fields['created_date'],$this->system_config->get('timezone'),'',true),
                        'item_name'     => $bill_fields['credit_add']['item_name'],
                        'employee_name' => $bill_fields['credit_add']['employee_name']
                    );
                }
            }
        }

        foreach($credit_transaction as $credit_id=>$credit_fields){
            $summary_transaction = array(
                'before' => 0,
                'after'  => 0,
            );
            if(count($credit_fields['rows'])){
                $summary_transaction['after']  = $credit_fields['rows'][0]['after'];
                $summary_transaction['before']   = $credit_fields['rows'][count($credit_fields['rows'])-1]['before'];
            }
            $credit_transaction[$credit_id]['summary'] = $summary_transaction;
        }
        $ret_data = array('credit_transaction' => convert_to_array($credit_transaction,'customer_code','',false));
        return $ret_data;
    }
    /*
        function get_export_data($format, $data){
            $content = array();
            $credit_transaction = $data['credit_transaction'];
            foreach($credit_transaction as $customer_code => $credit_trans_fields){
                $content[] = array('Customer Name:',$credit_trans_fields[0]['customer_name'],'Customer ID',$customer_code);
                foreach($credit_trans_fields as $package_fields){
                    $content[] = array(
                        'Date',
                        'Bill Code',
                        'Item',
                        'Redeem',
                        'Balance',
                        'Employees'
                    );
                    $content[] = array($package_fields['package_name']);
                    foreach($package_fields['rows'] as $row){
                        $content[] = array(
                            $row['created_date'],
                            $row['code'],
                            $row['item_name'],
                            $row['after']>$row['before']?('+'.$row['after']-$row['before']):$row['after']-$row['before'],
                            $row['after'],
                            $row['employee_name']
                        );
                    }
                    $change = $package_fields['summary']['after'] - $package_fields['summary']['before'];
                    $content[] = array("Package Points before: {$package_fields['summary']['before']} + Points: {$change} = Available Points: {$package_fields['summary']['after']}");
                }
                $content[] = array('');
            }
            return $content;
        }
        */
    function get_original_data($data,$type = false){
        $content = array();
        $credit_transaction = $data['credit_transaction'];
        foreach($credit_transaction as $customer_code => $credit_trans_fields){
            $content[] = array(
                array('export_text' => 'Customer Name: '.$credit_trans_fields[0]['customer_name'],
                    'html_class'=>'text-primary',
                    'style'=>'font-weight:bold; background-color:white',
                    'colspan'=>'2'
                ),

                array('export_text' => 'Customer ID: '.$customer_code,
                    'html_class'=>'text-primary',
                    'style'=>'font-weight:bold; background-color:white',
                    'colspan'=>'8')

            );

            foreach($credit_trans_fields as $package_fields){
                $content[] = array(
                    array('export_text' => 'Date',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Bill Code',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Item',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Quantity',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Price',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Employees',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Balance',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Redeem+',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'Redeem-',
                        'style'=>' font-weight:bold; background-color:white'),
                    array('export_text' => 'New Balance',
                        'style'=>' font-weight:bold; background-color:white'),


                );
                $content[] = array(array('export_text' =>$package_fields['package_name'],
                    'style'=>'color:red; font-weight: bold',
                    'colspan'=>'10'));
                foreach($package_fields['rows'] as $row){
                    $content[] = array(
                        array('export_text' => $row['created_date']),
                        array('export_text' => $row['code']),
                        array('export_text' => $row['item_name']),
                        array('export_text' => $row['qty']),
                        array('export_text' => $row['price']),
                        array('export_text' =>$row['employee_name']),
                        array('export_text' =>  $row['before']),
                        array('export_text' => $row['redeem+']),
                        array('export_text' => $row['redeem-']),
                        array('export_text' =>  $row['after']),


                    );
                }


            }
            $content[]=array(array('export_text' =>'',
                'colspan'=>'10',
                'style'=>' background-color:white'
            ));

        }
        return $content;
    }
}