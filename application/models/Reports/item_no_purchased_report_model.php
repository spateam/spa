<?php
class Item_no_purchased_report_model extends POS_Report_Model{
    function __construct(){
        parent::__construct();
    }

    function admin_get_detail_data($data){
        $category_id = isset($data['category_id']) ? $data['category_id'] : 0;
        $item_id = $data['item_id'];

        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $branch_id = $this->user_check->get_branch_id($data['branch_id']);
        $branch_group_id = $this->load->table_model('branch')->getTableMap('','branch_group_id',array('id' => $branch_id));

        if($item_id == 0){
            //Get all item in the category
            // $item = $this->load->table_model('category')->getByID($category_id);
            $item = $this->load->table_model('category')->getTableMap('id','',array('id' => $category_id));
            /*$item_ids = $this->select(array(
                'select' => array('item' => array('id')),
                'from'   => array(
                    'category_item' => array('table' => 'category_item'),
                    'item'     => array('table' => 'item', 'condition' => 'category_item.item_id = item.id')
                ),
                'where' => array('category_item.category_id' => $category_id),
                'user_level'=> Permission_Value::BRANCH,
                'permission'=> array('branch_id' => $branch_id, 'branch_group_id' => $branch_group_id)
            ))->result();
            $item_id = convert_to_array($item_ids,'','id');*/
        }else{
            $item = $this->load->table_model('item')->getByID($item_id);
        }

        /*$bills_before = $this->select(array(
            'select' => array('bill' => array('customer_id')),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
            ),
            'where' => array('bill_item.item_id' => $item_id,"bill.created_date < '{$start_date}'"),
            'user_level'=> Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id, 'branch_group_id' => $branch_group_id)
        ))->result();

        $customer_ids_before = convert_to_array($bills_before,'','customer_id');
        sort($customer_ids_before);

        $bills_after = $this->select(array(
            'select' => array('bill' => array('customer_id')),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
            ),
            'where' => array('bill_item.item_id' => $item_id,"bill.created_date >= '{$start_date}'", "bill.created_date <= '{$end_date}'"),
            'user_level'=> Permission_Value::BRANCH,
            'permission'=> array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id)
        ))->result();
        $customer_ids_after = convert_to_array($bills_after,'','customer_id');
        sort($customer_ids_after);
        $customer_ids = array();
        foreach($customer_ids_before as $customer_id_before){
            if(!in_array($customer_id_before,$customer_ids_after)){
                $customer_ids[] = $customer_id_before;
            }
        }

        $customers = $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_ids),true,Permission_Value::BRANCH,array('branch_id' => $branch_id,'branch_group_id' => $branch_group_id));*/

        return array('item' => $item, 'start_date' => $start_date, 'end_date' => $end_date, 'branch_id' => $branch_id, 'branch_group_id' => $branch_group_id);
    }

    function get_detail_data($data){
        $category_id = $data['category_id'];
        $item_id = $data['item_id'];

        $start_date = $data['start_date'];
        $end_date = $data['end_date'];

        if($item_id == 0){
            //Get all item in the category
            $item = $this->load->table_model('category')->getByID($category_id);
            $item_ids = $this->select(array(
                'select' => array('item' => array('id')),
                'from'   => array(
                    'category_item' => array('table' => 'category_item'),
                    'item'     => array('table' => 'item', 'condition' => 'category_item.item_id = item.id')
                ),
                'where' => array('category_item.category_id' => $category_id)
            ))->result();
            $item_id = convert_to_array($item_ids,'','id');
        }else{
            $item = $this->load->table_model('item')->getByID($item_id);
        }

        $bills_before = $this->select(array(
            'select' => array('bill' => array('customer_id')),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
            ),
            'where' => array('bill_item.item_id' => $item_id,"bill.created_date < '{$start_date}'")
        ))->result();

        $customer_ids_before = convert_to_array($bills_before,'','customer_id');
        sort($customer_ids_before);

        $bills_after = $this->select(array(
            'select' => array('bill' => array('customer_id')),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
            ),
            'where' => array('bill_item.item_id' => $item_id,"bill.created_date >= '{$start_date}'", "bill.created_date <= '{$end_date}'")
        ))->result();
        $customer_ids_after = convert_to_array($bills_after,'','customer_id');
        sort($customer_ids_after);
        $customer_ids = array();
        foreach($customer_ids_before as $customer_id_before){
            if(!in_array($customer_id_before,$customer_ids_after)){
                $customer_ids[] = $customer_id_before;
            }
        }

        $customers = $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_ids));

        return array('customers' => $customers, 'item' => $item);
    }
/*
    function get_export_data($format, $data){
        $content = array();
        $content[] = array('First Name', 'Last Name', 'Mobile Number', 'Email');
        $content[] = array($data['item']->name);
        if(isset($data['item'])){
            foreach($data['customers'] as $customer_id=>$customer_fields){
                $content[] = array(
                    $customer_fields->first_name,
                    $customer_fields->last_name,
                    $customer_fields->mobile_number,
                    $customer_fields->email,
                );
            }
        }else{
            $content[] = array('No record');
        }

        return $content;
    }
    */
    function get_original_data($data,$type = false){
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $branch_id = $data['branch_id'];
        $branch_group_id = $data['branch_group_id'];
        $item = $data['item'];
        $customers = null;
        $item_ids = null;
        $item_id = null;
        $content = array();
        $content[] = array(
            array('export_text' => 'First Name'),
            array('export_text' => 'Last Name'),
            array('export_text' => 'Mobile Number'),
            array('export_text' => 'Email')
        );
        // $content[] = array(array('export_text' =>isset($data['item']) ? $data['item']->name : "",
        //     'style'=>'color:red; font-weight: bolder',
        //     'colspan'=>'10'));

        if(isset($item)){
            foreach ($item as $key => $value) {
                $content[] = array(array('export_text' =>isset($item[$key]->name) ? $item[$key]->name : "",
                    'style'=>'color:red; font-weight: bolder',
                    'colspan'=>'10'));
                $item_ids = $this->select(array(
                    'select' => array('item' => array('id')),
                    'from'   => array(
                        'category_item' => array('table' => 'category_item'),
                        'item'     => array('table' => 'item', 'condition' => 'category_item.item_id = item.id')
                    ),
                    'where' => array('category_item.category_id' => $key)
                ))->result();
                $item_id = convert_to_array($item_ids,'','id');

                $bills_before = $this->select(array(
                    'select' => array('bill' => array('customer_id')),
                    'from' => array(
                        'bill' => array('table' => 'bill'),
                        'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
                    ),
                    'where' => array('bill_item.item_id' => $item_id,"bill.created_date < '{$start_date}'")
                ))->result();

                $customer_ids_before = convert_to_array($bills_before,'','customer_id');
                sort($customer_ids_before);

                $bills_after = $this->select(array(
                    'select' => array('bill' => array('customer_id')),
                    'from' => array(
                        'bill' => array('table' => 'bill'),
                        'bill_item' => array('table' => 'bill_item', 'condition' => 'bill.id = bill_item.bill_id')
                    ),
                    'where' => array('bill_item.item_id' => $item_id,"bill.created_date >= '{$start_date}'", "bill.created_date <= '{$end_date}'")
                ))->result();
                $customer_ids_after = convert_to_array($bills_after,'','customer_id');
                sort($customer_ids_after);
                $customer_ids = array();
                foreach($customer_ids_before as $customer_id_before){
                    if(!in_array($customer_id_before,$customer_ids_after)){
                        $customer_ids[] = $customer_id_before;
                    }
                }

                $customers = $this->load->table_model('customer')->getTableMap('id','',array('id' => $customer_ids));
                if(isset($customers)){
                    foreach($customers as $customer_id=>$customer_fields){
                        $content[] = array(
                            array('export_text' => $customer_fields->first_name),
                            array('export_text' => $customer_fields->last_name),
                            array('export_text' => $customer_fields->mobile_number),
                            array('export_text' =>  $customer_fields->email)

                        );
                    }
                }
            }
            
        }else{
            $content[] = array(array('export_text' =>'No record',
                'colspan'=>'10'));
        }

        return $content;
    }
}