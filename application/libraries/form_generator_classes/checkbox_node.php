<?php
/**
 * Created by PhpStorm.
 * User: Anne
 * Date: 5/7/2015
 * Time: 9:18 AM
 */

class Checkbox_Node extends Node{
    function pre_parse(){
        $this->node_attribute['type'] = 'checkbox';
        $this->node_attribute['style'] = 'width:auto';
        if(isset($this->node_attribute['value']) && $this->node_attribute['value'] == 1){
            $this->node_attribute['checked'] = 'checked';
        }else{
            $this->node_attribute['value'] = 1;
        }
    }
}