<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 6/5/2015
 * Time: 9:08 AM
 */

class Node {
    public $node_name = '';
    public $node_auto_id = 1;
    public $node_attribute = array();
    public $child_nodes = array();
    static $ID_COLLECTION = array();
    static $NO_TAIL_TAG = array('input');

    public function __construct($param = array()){
        $df = array(
            'node_name' => '',
            'node_attribute' => array(),
            'child_nodes' => array()
        );
        while(in_array($this->node_auto_id,Node::$ID_COLLECTION)){
            $this->node_auto_id = rand(1,1000000);
        }
        Node::$ID_COLLECTION[] = $this->node_auto_id;
        $param                  = array_merge($df,$param);
        $this->node_name        = $param['node_name'];
        $this->node_attribute   = $param['node_attribute'];
        $this->child_nodes      = $param['child_nodes'];
    }

    function append_node($node){
        if(!in_array($this->node_name, Node::$NO_TAIL_TAG)){
            $this->child_nodes[] = $node;
        }
    }

    protected function pre_parse(){

    }

    function parse(){
        $this->pre_parse();
        $inner_node = '';
        if(in_array($this->node_name,Node::$NO_TAIL_TAG)){
            $html = "<{$this->node_name} %s/>%s";
        }else{
            $html = "<{$this->node_name} %s>%s</{$this->node_name}>";
            if(isset($this->node_attribute['text'])){
                $inner_node = $this->node_attribute['text'];
            }
            foreach($this->child_nodes as $each_child_node){
                $inner_node .= $each_child_node->parse();
            }
        }
        $attributes = $this->node_attribute; if(isset($attributes['text'])) unset($attributes['text']);
        $attribute_html = array();
        foreach($attributes as $key=>$value){
            if(is_array($value)){
                $value = json_encode($value);
                $attribute_html[] = "{$key}='{$value}'";
            }else{
                $attribute_html[] = "{$key}='{$value}'";
            }

        }
        $attribute_html = implode(' ', $attribute_html);
        return sprintf($html,$attribute_html,$inner_node);
    }
}