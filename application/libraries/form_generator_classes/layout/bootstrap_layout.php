<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 6/5/2015
 * Time: 9:25 AM
 */
require "node_layout.php";

class Bootstrap_Layout extends Node_Layout{

    function __construct(){

    }

    static function LABEL_CLASS_LAYOUT(){
        return "col-sm-3 col-md-3 col-lg-2";
    }

    static function CONTROL_CLASS_LAYOUT(){
        return "col-sm-4 col-md-4 col-lg-4";
    }

    static function CONTROL_CLASS(){
        return "form-control";
    }

    static function LABEL_CLASS(){
        return "control-label";
    }

    static function FORM_GROUP_CLASS(){
        return "form-group";
    }

}