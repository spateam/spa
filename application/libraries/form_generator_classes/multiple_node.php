<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 6/5/2015
 * Time: 10:53 AM
 */

class Multiple_Node extends Node {

    function __cosntruct($param){
        parent::__construct($param);
    }

    function pre_parse(){
        if(isset($this->node_attribute['value'])){
            $this->node_attribute['data-value'] = $this->node_attribute['value'];
            unset($this->node_attribute['value']);
        }

        if(isset($this->node_attribute['data-source'])){
            if(is_array($this->node_attribute['data-source'])){
                foreach($this->node_attribute['data-source'] as $key=>$value){
                    $node_param = array(
                        'node_name' => 'option',
                        'node_attribute' => array(
                            'text' => $value,
                            'value'=> $key
                        ),
                    );
                    if(isset($this->node_attribte['value']) && $key == $this->node_attribte['value']){
                        $node_param['attribute']['selected'] = '1';
                    }
                    $this->child_nodes[] = new Node($node_param);
                }
                unset($this->node_attribute['data-source']);
            }
        }
        if(isset($this->node_attribute['default-source'])){
            if(is_array($this->node_attribute['default-source'])){
                foreach($this->node_attribute['default-source'] as $key=>$value){
                    $node_param = array(
                        'node_name' => 'option',
                        'node_attribute' => array(
                            'text' => $value,
                            'value'=> $key
                        ),
                    );
                    if(isset($this->node_attribte) && $key == $this->node_attribte['value']){
                        $node_param['attribute']['selected'] = '1';
                    }
                    $this->child_nodes[] = new Node($node_param);
                }
                unset($this->node_attribute['default-source']);
            }
        }
    }

}