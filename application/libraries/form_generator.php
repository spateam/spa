<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 6/5/2015
 * Time: 9:07 AM
 */

include 'form_generator_classes/node.php';
include 'form_generator_classes/select_node.php';
include 'form_generator_classes/multiple_node.php';
include 'form_generator_classes/radio_node.php';
include 'form_generator_classes/checkbox_node.php';

class Form_Generator {

    static $NODE_MAP = array(
        'select'        => array('class'=>'Select_Node', 'node_name' => 'select'),
        'multiselect'   => array('class'=>'Node', 'node_name' => 'input'),
        'input'         => array('class'=>'Node', 'node_name' => 'input'),
        'checkbox'      => array('class'=>'Checkbox_Node', 'node_name' => 'input'),
        'radio'         => array('class'=>'Radio_Node', 'node_name' => 'input'),
        'multiple'      => array('class'=>'Multiple_Node', 'node_name' => 'select'),
    );

    static $LAYOUT_MAP = array(
        'bootstrap' => array(
            'LABEL_LAYOUT'      => "col-sm-3 col-md-3 col-lg-2",
            'LABEL_CLASS'       => "control-label",
            'CONTROL_LAYOUT'    => "col-sm-9 col-md-9 col-lg-10",
            'CONTROL_CLASS'     => "form-control",
            'FORM_GROUP_CLASS'  => "form-group"
        )
    );

    static function CREATE_FORM_GROUP($param){
        $df = array(
            'label'             => array(
                'text'  => '',
                'class' => '',
            ),
            'control'           => array(
                'type'      => '',
                'value'     => '',
                'attribute' => array(),
                'id'        => '',
                'class'     => ''
            ),
            'layout' => 'bootstrap'
        );

        $param = array_merge($df,$param);
        if(!isset($param['control']['attribute']['value']) && isset($param['control']['value'])){
            $param['control']['attribute']['value'] = $param['control']['value'];
        }
        if(!isset($param['control']['attribute']['id']) && isset($param['control']['id'])){
            $param['control']['attribute']['id'] = $param['control']['id'];
        }
        if(!isset($param['control']['attribute']['class']) && isset($param['control']['class'])){
            $param['control']['attribute']['class'] = $param['control']['class'];
        }
        if(!isset($param['control']['attribute']['name']) && isset($param['control']['id'])){
            $param['control']['attribute']['name'] = $param['control']['id'];
        }
        if(!isset($param['control']['attribute']['class'])){
            $param['control']['attribute']['class'] = '';
        }
        $param['control']['attribute']['class'] .= ' '.self::$LAYOUT_MAP[$param['layout']]['CONTROL_CLASS'];

        $form_node = new Node(array(
            'node_name'         => 'div',
            'node_attribute'    =>  array('class' => self::$LAYOUT_MAP[$param['layout']]['FORM_GROUP_CLASS'])
        ));

        $label_node_info = array(
            'node_name' => 'label',
            'node_attribute' => array(
                'class' => self::$LAYOUT_MAP[$param['layout']]['LABEL_LAYOUT'] . ' ' .self::$LAYOUT_MAP[$param['layout']]['LABEL_CLASS'] . (isset($param['label']['class'])?' ' . $param['label']['class']:''),
                'text'  => $param['label']['text']
            )
        );
        if(isset($param['control']['attribute']['id'])){
            $label_node_info['node_attribute']['for'] = $param['control']['attribute']['id'];
        }

        $label_node = new Node($label_node_info);
        $control_node = new Node(array(
            'node_name'         => 'div',
            'node_attribute'    => array('class' => self::$LAYOUT_MAP[$param['layout']]['CONTROL_LAYOUT']),
            'child_nodes'       => array(
                new self::$NODE_MAP[$param['control']['type']]['class'](array(
                    'node_name' => self::$NODE_MAP[$param['control']['type']]['node_name'],
                    'node_attribute' => $param['control']['attribute']
                ))
            ),
        ));

        $form_node->append_node($label_node);
        $form_node->append_node($control_node);
        return $form_node->parse();
    }



}