<?php

class POS_Email extends CI_Email
{
    protected $_CI;

    public function __construct($config = array())
    {
        $this->_CI = &get_instance();
        $config['protocol'] = AWS_SES::protocol;
        $config['smtp_host'] = AWS_SES::smtp_host;
        $config['smtp_user'] = AWS_SES::smtp_user;
        $config['smtp_pass'] = AWS_SES::smtp_pass;
        $config['smtp_port'] = AWS_SES::smtp_port;
        $config['smtp_timeout'] = AWS_SES::smtp_timeout;
        $config['smtp_crypto'] = AWS_SES::smtp_crypto;
        $config['newline'] = "\r\n";
        $config['wordwrap'] = false;
        $config['mailtype'] = 'html';
        parent::__construct($config);
        $this->from($this->_CI->config->item('system_email'), $this->_CI->config->item('system_email_name'));
    }


    public function clear($clear_attachments = FALSE)
    {
        parent::clear($clear_attachments);
        $this->from($this->_CI->config->item('system_email'), $this->_CI->config->item('system_email_name'));
    }
}