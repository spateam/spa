<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Smslib{
    
    const ID = 'joshua@healingtouchspa.com';
    const PWD = 'JH160510';
    const APP_ID = 1528;
    const APP_SECRET = '6eb549b8-ca8e-481a-b447-a8d3167dc6e8';
    const SEND_SMS_API = 'http://www.smsdome.com/api/http/sendsms.aspx';
    const RESPONSE_FORMAT = 'JSON';

    public function sendSMS($number, $content){
        $checkData = $this->_validSMSParams(array('receivers' => $number, 'content' => $content));
        if($checkData['status']){
            $this->_sendSMS(array('receivers' => $number, 'content' => $content));
        }
        return TRUE;
    }

    private function _validSMSParams($params)
    {
        if (!isset($params['receivers'])) {
            return array('status' => FALSE, 'message' => 'Sending number was missing.');
        }
        if (empty($params['receivers'])) {
            return array('status' => FALSE, 'message' => 'Sending number can not empty');
        }

        if (!isset($params['content'])) {
            return array('status' => FALSE, 'message' => 'Message content was missing.');
        }
        if (empty($params['content'])) {
            return array('status' => FALSE, 'message' => 'Message can not empty');
        }

        if (is_array($params['receivers'])) {
            foreach ($params['receivers'] as $key => $num) {
                $params['receivers']['key'] = trim($num);
            }
        } else {
            $params['receivers'] = trim($params['receivers']);
        }

        $params['receivers'] = trim($params['receivers']);
        $params['content'] = rawurldecode($params['content']);
        return array('status' => TRUE, 'data' => $params);
    }

    private function _sendSMS($sendingParams)
    {
        $receivers = '';
        if (is_array($sendingParams['receivers'])) {
            $receivers = implode(',', $sendingParams['receivers']);
        } else {
            $receivers = $sendingParams['receivers'];
        }

        $curlPostFields = array(
            // app id : provide by customer
            'appid' => Smslib::APP_ID,
            // appsecret : provide by customer
            'appsecret' => Smslib::APP_SECRET,
            // receivers : customer sending numbers, get from our database, (maximum 100 per sending), seperate by ','.
            'receivers' => $receivers,
            // content : message content, provide by our system.
            'content' => $sendingParams['content'],
            // responseformat: XML or JSON, default is XML.
            'responseformat' => Smslib::RESPONSE_FORMAT
        );

        // init the resource
        $ch = curl_init();

        // set curl options
        curl_setopt($ch, CURLOPT_URL, Smslib::SEND_SMS_API);
        curl_setopt($ch, CURLOPT_POST, 1); // 1 for POST request, 0 for GET request
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($curlPostFields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // execute curl job
        $response = curl_exec($ch);

        // freeup curl resource
        curl_close($ch);

        $this->_logSMSResponse($response);
        return TRUE;
    }

    private function _logSMSResponse($response){
        $CI =& get_instance();
        $response = (array)json_decode($response);
        $insertData = array(
            'result' => $response['result']->status,
            'error'  => $response['result']->error,
            'test_mode' => $response['result']->testmode,
            'content' => $response['content']->value,
            'parts' => $response['content']->parts,
            'credit_balance' => $response['credit']->balance,
            'credit_use' => $response['credit']->required,
            'receivers' => json_encode($response['receivers'])
        );
        $CI->db->insert('sms_response',$insertData);
        return TRUE;
    }

    // SEND SMS REMINDER TO BOOKING CUSTOMER BEFORE 1 DAY
    public function sendSMSReminder(){
        $CI =& get_instance();
        $listCust = $CI->load->table_model('customer')->getCustomerSMSReminder();
        $sendTime = $CI->db->select('config_branch.branch_id,config.code,config.value as defaul_value, config_branch.value as branch_value')
                           ->join('config_branch','config.id = config_branch.config_id','LEFT')
                           ->where('config.code IN ("sms_rm1","timezone")')
                           ->get('config')->result_array();
        $temp = array();
        foreach($sendTime as $item){
            $temp[$item['branch_id']][$item['code']] = ($item['branch_value'] != 'inherit' ? $item['branch_value'] : $item['defaul_value'] );
        }
        $sendTime = $temp;

        $currentTime = get_database_date();

        if(!empty($listCust)){
            foreach($listCust as $customer){
                if(isset($sendTime[$customer->branch_id]['sms_rm1'])){
                    if(get_user_date($currentTime,$sendTime[$customer->branch_id]['timezone'],'G:i') == $sendTime[$customer->branch_id]['sms_rm1']){
                        $startTime = get_user_date($customer->start_time,'','g:ia');
                        $endTime = get_user_date($customer->end_time,'','g:ia');
                        $date = get_user_date($customer->start_time,'','d M');
                        $branch = trim(strstr($customer->branch_name,'Branch',true));
                        $content = 'Healing Touch, '.$branch.' branch appointment, ' . $date . ' ' . $startTime . ' - ' . $endTime . '. Arrive 10 mins earlier.';
                     //   $content = 'Your appointment at Healing Touch, '.$branch.' - ' . $date . ' ' . $startTime . ' to ' . $endTime . '. Please arrive 15mins earlier, any changes, call 67151515. Please Do Not Reply';
                        //echo strlen($content).' - '.$content.'<br>';
                        $this->sendSMS(trim($customer->mobile_number),$content);
                        $CI->db->update('booking',array('sms_reminder' => 1), array('id' => $customer->id));
                    }
                }
            }
        }
    }

    public function sendResetPin($customerId,$pin){
        $CI =& get_instance();
        if(empty($customerId)){
            return FALSE;
        }
        if(empty($pin)){
            return FALSE;
        }
        $customer = $CI->db->get_where('customer',array('id' => $customerId))->row_array();
        if(empty($customer)){
            return FALSE;
        }

        $branchName = $CI->session->all_userdata()['staff']['login']->branch_name;
        $branchGroup = $CI->db->get_where('branch_group',array('id' => $CI->session->all_userdata()['staff']['login']->branch_group_id))->row_array();
        if(!empty($branchGroup)){
            $branchGroup = $branchGroup['name'].', ';
        }
        else{
            $branchGroup = '';
        }

        $mobileNumber = $customer['mobile_number'];
        $content = $branchGroup . $branchName . ' - Dear ' . ucwords(mb_strtolower($customer['first_name'])) . ' , Your new package pin is ' . $pin;
        $this->sendSMS($mobileNumber, $content);
    }

    public function sendConfirmEmailOTP($customer,$OTP){
        $CI =& get_instance();
        if(empty($customer)){
            return FALSE;
        }
        if(empty($OTP)){
            return FALSE;
        }

        $branchName = $CI->session->all_userdata()['staff']['login']->branch_name;
        $branchGroup = $CI->db->get_where('branch_group',array('id' => $CI->session->all_userdata()['staff']['login']->branch_group_id))->row_array();
        if(!empty($branchGroup)){
            $branchGroup = $branchGroup['name'].', ';
        }
        else{
            $branchGroup = '';
        }

        $mobileNumber = $customer['mobile_number'];
        $content = $branchGroup . $branchName . ' - Your OTP code: '.$OTP.' use to verify '.$customer['email'];
        $this->sendSMS($mobileNumber, $content);
    }

    public function sendBillSms($customerId){
        $CI =& get_instance();
        if(empty($customerId)){
            return FALSE;
        }
        $customer = $CI->db->get_where('customer',array('id' => $customerId))->row_array();
        if(empty($customer)){
            return FALSE;
        }
        $mobileNumber = $customer['mobile_number'];
        $content = 'Pls review us on Facebook http://goo.gl/SG3uEk or TripAdvisor http://goo.gl/1YxR8s & stand a chance to win 1hr free massage. Tq! Unsub: Reply U437 to 90933211';
        $this->sendSMS($mobileNumber, $content);
    }

}
