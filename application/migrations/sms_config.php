CREATE TABLE `spa_report`.`sms_config` (
`id` INT NOT NULL,
`name` VARCHAR(255) NOT NULL,
`value` VARCHAR(45) NOT NULL,
`createAt` DATETIME NULL,
`updateAt` DATETIME NULL,
`status` INT(11) NOT NULL DEFAULT 1,
PRIMARY KEY (`id`));


INSERT INTO `spa_report`.`config` (`name`, `code`, `value`, `type`, `required`, `status`)
VALUES ('Max Number of booking', 'max_num_booking', '3', '2', '1', '1');
