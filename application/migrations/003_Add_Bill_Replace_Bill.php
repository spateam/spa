<?php
class Migration_Add_Bill_Replace_Bill extends CI_Migration{
    public function up(){
        $fields = array(
            'replace_bill_id' => array(
                'type' => 'int',
                'unsigned' => TRUE,
                'null' => true
            )
        );
        $this->dbforge->add_column('bill',$fields, 'amount_due');
        $this->db->query('ALTER TABLE bill ADD CONSTRAINT `bill_ibfk_3` FOREIGN KEY (`replace_bill_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE  ON UPDATE CASCADE');
    }

    public function down(){
        $this->dbforge->drop_column('bill','replace_bill_id');
    }
}