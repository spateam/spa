<?php
class Migration_Admin_Employee_Module extends CI_Migration{
    public function up(){
        $employee_module_id = $this->db->get_where('module',array('code' => 'employees'))->result()[0]->id;

        $employee_module_permission_id = $this->db->get_where('module_permission',array('module_id' => $employee_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $this->db->update('module',array('type' => MENU_TYPE('All')),array('code' => 'employees'));

        $this->db->insert('menu',array(
            'title'     => 'Employees',
            'link'      => 'admin/employees',
            'type'      => MENU_TYPE('Admin'),
            'module_permission_id' => $employee_module_permission_id
        ));
    }

    public function down(){

    }
}