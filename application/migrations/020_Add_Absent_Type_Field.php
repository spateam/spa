<?php
class Migration_Add_Absent_Type_Field extends CI_Migration{
    public function up(){
        $fields = array(
            'type' => array(
                'type' => 'tinyint',
                'constraint' => 100,
                'default'    => 1,
                'null' => true
            )
        );
        $this->dbforge->add_column('employee_absent',$fields, 'end_time');
    }
}