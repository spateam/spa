<?php
class Migration_Add_Booking_Updated extends CI_Migration{
    public function up(){
        $fields = array(
            'updated_date' => array(
                'type' => 'datetime',
                'null' => true
            )
        );
        $this->dbforge->add_column('booking',$fields, 'created_date');
    }

    public function down(){
    }
}