<?php
class Migration_Employee_Absent_Note extends CI_Migration{
    public function up(){
        $fields = array(
            'description' => array(
                'type' => 'text',
                'null' => true
            )
        );
        $this->dbforge->add_column('employee_absent',$fields, 'end_time');
    }
    public function down(){}
}