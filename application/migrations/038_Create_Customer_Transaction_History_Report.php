<?php
class Migration_Create_Customer_Transaction_History_Report extends CI_Migration
{
    public function up(){
        $this->db->trans_start();

        $this->db->insert('report',array(
            'parent'            => 13,
            'condition_form'    => 1,
            'code'              => 'customer_transaction_history_report',
            'name'              => 'Customer Transaction History',
            'system'            => 3,
            'status'            => 1));
        $report_id = $this->db->select('MAX(id) as id')->get('report')->result();

        $this->db->insert('report_group',array('group_id' => 1, 'report_id' => $report_id[0]->id));
        $this->db->insert('report_group',array('group_id' => 2, 'report_id' => $report_id[0]->id));

        $this->db->trans_complete();

    }
}