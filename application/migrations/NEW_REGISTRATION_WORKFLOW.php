// create one time password table
CREATE TABLE `spa_report`.`customer_onetime_passwd` (
`id` INT NOT NULL AUTO_INCREMENT,
`customer_id` INT(11) NOT NULL,
`customer_email` VARCHAR(255) NOT NULL,
`one_time_pwd` VARCHAR(100) NOT NULL,
`created_date` DATETIME NULL,
`valid_date` DATETIME NULL,
`is_valid` INT(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`id`));



INSERT INTO `gentopos2`.`config` (`id`, `name`, `code`, `value`, `type`, `required`, `order`, `status`) VALUES ('21', 'Booking popup message', 'popup_sms', '1', '1', '0', '12', '1');


INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '1', '');
INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '2', '');
INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '3', '');
INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '4', '');
INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '5', '');
INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '6', '');
INSERT INTO `gentopos2`.`config_branch` (`config_id`, `branch_id`, `value`) VALUES ('21', '7', '');


INSERT INTO `gentopos2`.`config_branch_group` (`config_id`, `branch_group_id`, `value`) VALUES ('21', '1', '');
INSERT INTO `gentopos2`.`config_branch_group` (`config_id`, `branch_group_id`, `value`) VALUES ('21', '2', '');
INSERT INTO `gentopos2`.`config_branch_group` (`config_id`, `branch_group_id`, `value`) VALUES ('21', '3', '');
INSERT INTO `gentopos2`.`config_branch_group` (`config_id`, `branch_group_id`, `value`) VALUES ('21', '8', '');
INSERT INTO `gentopos2`.`config_branch_group` (`config_id`, `branch_group_id`, `value`) VALUES ('21', '10', '');
