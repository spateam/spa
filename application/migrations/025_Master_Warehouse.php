<?php
class Migration_Master_Warehouse extends CI_Migration{
    public function up(){
        $this->db->trans_start();

        $this->create_item_product_table();

        $this->db->trans_complete();
    }

    function create_item_product_table(){
        $this->db->query('DROP TABLE IF EXISTS `item_product`;');
        $this->db->query('DROP TABLE IF EXISTS `item_product_log`;');
        $this->db->query('CREATE TABLE `item_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned DEFAULT NULL,
  `master_quantity` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `item_product_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
        $result = $this->db->get_where('item',array('type' => ITEM_TYPE('Product')))->result();
        foreach($result as $row){
            $this->db->insert('item_product',array('item_id' => $row->id,'master_quantity' => 0));
        }
        $this->db->query('CREATE TABLE `item_product_log` (
  `id` int(10) unsigned DEFAULT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `master_quantity` int(10) unsigned DEFAULT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `log_user` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
    }


    public function down(){
    }
}