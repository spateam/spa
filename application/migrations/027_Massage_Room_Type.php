<?php
class Migration_Massage_Room_Type extends CI_Migration{
    public function up(){
        $this->db->trans_start();

        $this->edit_some_field();
        $this->create_room_type();
        $this->remove_room_service();
        $this->add_data();
        $this->add_module();
        $this->db->trans_complete();
    }

    function edit_some_field(){
        $fields = array(
            'updated_date' => array(
                'name' => 'updated_date',
                'type' => 'datetime',
            ),
        );
        $this->dbforge->modify_column('room', $fields);
        if ($this->db->field_exists('room_type_id', 'room'))
        {
            $this->db->query('update room set room.room_type_id = NULL');
        }
    }

    function create_room_type(){
        $this->db->query('SET FOREIGN_KEY_CHECKS=0;');
        $this->db->query('DROP TABLE IF EXISTS `room_type_category`;');
        $this->db->query('DROP TABLE IF EXISTS `room_type`;');
        $this->db->query('CREATE TABLE `room_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(15) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        $this->db->query('CREATE TABLE `room_type_category` (
  `room_type_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  KEY `room_type_id` (`room_type_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `room_type_category_ibfk_1` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `room_type_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        $this->db->query('SET FOREIGN_KEY_CHECKS=1;');
    }

    function remove_room_service(){
        if (!$this->db->field_exists('room_type_id', 'room'))
        {
            $this->db->query('ALTER TABLE room
  ADD COLUMN room_type_id int UNSIGNED,
  ADD FOREIGN KEY room_room_type_ibfk_1(room_type_id) REFERENCES room_type(id) ON DELETE CASCADE ON UPDATE CASCADE;');
        }

        $this->db->query('DROP TABLE IF EXISTS `item_service_room`');
    }

    function add_data(){
        $room_type = array(
            'MR' => array(
                'name' => 'Massage Room',
                'categories' => array(1,2,5,10)
            ),
            'SLR' => array(
                'name' => 'Slimming Room',
                'categories' => array(1,2,5,10,3,4,11,13,15)
            ),
            'SAR' => array(
                'name' => 'Sauna Room',
            ),
            'FR' => array(
                'name' => 'Facial Room',
                'categories' =>  array(1,2,3,4,5,6,7,8,10,11,12,14)
            ),
        );

        $room_model = $this->load->table_model('room');
        $room_type_model = $this->load->table_model('room_type');
        $room_type_category_model = $this->load->table_model('room_type_category');

        $this->db->query('delete from room_type');

        $rooms = $this->db->get_where('room',"code like '%BR%'")->result();
        foreach($rooms as $room){
            $room->code = str_replace('BR','FR',$room->code);
            $room_model->update($room->id, array('code' => $room->code), false);
        }

        foreach($room_type as $code => $fields){
            $type_id = $room_type_model->insert(array('code' => $code, 'name' => $fields['name']));
            if(isset($fields['categories']))
            foreach($fields['categories'] as $category_id){
                $room_type_category_model->insert(array(
                    'room_type_id' => $type_id,
                    'category_id'  => $category_id
                    ));
            }
            $rooms = $this->db->get_where('room',"code LIKE '%$code%'")->result_array();
            foreach($rooms as $room){
                $room_model->update($room['id'], array('room_type_id' => $type_id), false);
            }
        }
    }

    function add_module(){
        $module_array = array(
            array(
                'name' => 'Room Type',
                'code' => 'room_type',
                'type' => 3,
            )
        );

        $this->db->insert_batch('module',$module_array);
        foreach($module_array as $module){
            $id = $this->db->get_where('module',$module)->result();
            $id = $id[0]->id;
            $this->load->helper('migration');
            add_permission_to_all_places($id,array('v','i','e','d'));
        }

        $room_type_module_id       = $this->db->get_where('module',$module_array[0])->result()[0]->id;
        $room_type_permission_id = $this->db->get_where('module_permission',array('module_id' => $room_type_module_id, 'permission_code' => 'v'))->result()[0]->id;
        

        $this->db->insert('menu',array(
            'title'     => 'Room Type',
            'link'      => 'admin/room_type',
            'type'      => MENU_TYPE('Admin'),
            'module_permission_id' => $room_type_permission_id
        ));

        $this->db->insert('menu',array(
            'title'     => 'Room Type',
            'link'      => 'staff/room_type',
            'type'      => MENU_TYPE('Staff'),
            'module_permission_id' => $room_type_permission_id
        ));

    }

}