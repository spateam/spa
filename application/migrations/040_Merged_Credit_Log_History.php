CREATE TABLE `spa_dev_local`.`merged_credit_history` (
`id` INT NOT NULL AUTO_INCREMENT,
`credit_id` INT NOT NULL,
`customer_id_from` INT NOT NULL,
`customer_id_to` INT NOT NULL,
`log_time` DATETIME NULL,
PRIMARY KEY (`id`));

CREATE TABLE `spa_dev_local`.`merged_credit_log_history` (
`id` INT NOT NULL AUTO_INCREMENT,
`merged_credit_id` INT NOT NULL,
`credit_log_id` INT NOT NULL,
`customer_id_from` INT NOT NULL,
`customer_id_to` INT NOT NULL,
`log_time` DATETIME NOT NULL,
PRIMARY KEY (`id`));

CREATE TABLE `spa_dev_local`.`merged_bill_history` (
`id` INT NOT NULL AUTO_INCREMENT,
`bill_id` INT NOT NULL,
`customer_id_from` INT NOT NULL,
`customer_id_to` INT NOT NULL,
`log_time` DATETIME NOT NULL,
PRIMARY KEY (`id`));

CREATE TABLE `spa_dev_local`.`merged_bill_log_history` (
`id` INT NOT NULL AUTO_INCREMENT,
`bill_log_id` INT NOT NULL,
`customer_id_from` INT NOT NULL,
`customer_id_to` INT NOT NULL,
`log_time` DATETIME NOT NULL,
PRIMARY KEY (`id`));
