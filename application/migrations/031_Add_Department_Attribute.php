<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 16/6/2015
 * Time: 1:02 PM
 */

class Migration_Add_Department_Attribute extends CI_Migration {

    public function up(){
        $fields = array(
            'is_account_required' => array(
                'type' => 'tinyint',
                'constraint' => 100,
                'default'    => 1,
                'null' => true
            )
        );
        $this->dbforge->add_column('department',$fields, 'name');

        $fields = array(
            'is_serve_booking' => array(
                'type' => 'tinyint',
                'constraint' => 100,
                'default'    => 0,
                'null' => true
            )
        );
        $this->dbforge->add_column('department',$fields, 'is_account_required');

        $this->db->update('department',array('is_serve_booking' => 1,'is_account_required' => 0),array('id' => 7));
        $this->db->update('department',array('is_serve_booking' => 1,'is_account_required' => 0),array('id' => 8));
    }

}