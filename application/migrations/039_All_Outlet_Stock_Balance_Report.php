<?php
class Migration_All_Outlet_Stock_Balance_Report extends CI_Migration
{
    public function up(){
        $this->db->trans_start();

        $this->db->insert('report',array(
            'parent'            => 23,
            'condition_form'    => 1,
            'code'              => 'all_outlet_stock_balance_report',
            'name'              => 'All Outlet Stock Balance',
            'system'            => 2,
            'status'            => 1));
        $report_id = $this->db->select('MAX(id) as id')->get('report')->result();

        $this->db->insert('report_group',array('group_id' => 1, 'report_id' => $report_id[0]->id));
        $this->db->insert('report_group',array('group_id' => 2, 'report_id' => $report_id[0]->id));

        $this->db->trans_complete();

    }
}

/*
 ALTER TABLE `gentopos2`.`item`
ADD COLUMN `fullcash_payment` INT NOT NULL DEFAULT 1 COMMENT '' AFTER `global`;

 * */