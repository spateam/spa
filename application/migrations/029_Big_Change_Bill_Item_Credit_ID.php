<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 2/6/2015
 * Time: 1:38 PM
 */

class Migration_Big_Change_Bill_Item_Credit_ID extends CI_Migration{
    public function up(){
        if(!$this->db->field_exists('credit_id','bill_item')) {
            $fields = array(
                'credit_id' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'null' => true
                )
            );
            $this->dbforge->add_column('bill_item',$fields, 'discount_value');
            $this->db->query('ALTER TABLE bill_item ADD CONSTRAINT `bill_item_ibfk_4` FOREIGN KEY (`credit_id`) REFERENCES `credit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE');
        }
        $this->db->trans_start();
        $this->add_credit_id();
        $this->db->trans_complete();
    }

    function add_credit_id(){
        $CI =& get_instance();
        $bills = $CI->load->table_model('bill')->select(array(
            'select' => array(
                'bill_item' => array('bill_item_id' => 'id','item_id','credit_add'),
                'credit' => array('credit_id' => 'id'),
                'credit_branch_group' => array('branch_group_id')
            ),
            'from' => array(
                'bill' => array('table' => 'bill'),
                'bill_item' => array('table' => 'bill_item',  'condition' => 'bill.id = bill_item.bill_id'),
                'credit' => array('table' => 'credit', 'condition' => 'credit.customer_id = bill.customer_id and credit.item_id = bill_item.item_id'),
                'bill_branch_group' => array('table' => 'bill_branch_group', 'condition' => 'bill_branch_group.bill_id = bill.id', 'type' => 'left'),
                'credit_branch_group' => array('table' => 'credit_branch_group',
                    'condition' => 'credit_branch_group.credit_id = credit.id and bill_branch_group.branch_group_id = credit_branch_group.branch_group_id', 'type' => 'left')
            ),
            'where' => array('bill_item.credit_add > 0'),
            'user_level' => 4,
        ))->result();
        $bills = convert_to_array($bills,'bill_item_id','',false);
        $this->db->update('bill_item',array('credit_id' => null));
        foreach($bills as $list_credit){
            $credit = null;
            if(count($list_credit) > 1){
                foreach($list_credit as $each_credit){
                    if(isset($each_credit->branch_group_id)){
                        $credit = $each_credit;
                    }
                }
            }
            if(!$credit){
                $credit = $list_credit[0];
            }
            $this->db->update('bill_item',array('credit_id' => $credit->credit_id),array('id' => $credit->bill_item_id));
            echo "Updated $credit->bill_item_id with credit_id: $credit->credit_id<br>";
        }

    }

    public function down(){

    }
}