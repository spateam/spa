<?php
class Migration_Item_Module extends CI_Migration{
    function item_module(){

        $parent_id = $this->db->get_where('menu',array('title' => 'Inventory'))->result()[0]->id;

        $this->db->update('menu',array('type' => MENU_TYPE('All')),array('title' => 'Inventory'));

        $item_module_id = $this->db->get_where('module',array('code' => 'items'))->result()[0]->id;

        $item_permission_id = $this->db->get_where('module_permission',array('module_id' => $item_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $this->db->update('module',array('type' => MENU_TYPE('All')),array('code' => 'items'));

        $this->db->insert('menu',array(
            'title'     => 'Items',
            'link'      => 'admin/items',
            'type'      => MENU_TYPE('Admin'),
            'parent'    => $parent_id,
            'module_permission_id' => $item_permission_id
        ));
    }
    
    function category_module(){

        $parent_id = $this->db->get_where('menu',array('title' => 'Inventory'))->result()[0]->id;

        $category_module_id = $this->db->get_where('module',array('code' => 'categories'))->result()[0]->id;

        $category_permission_id = $this->db->get_where('module_permission',array('module_id' => $category_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $this->db->update('module',array('type' => MENU_TYPE('All')),array('code' => 'categories'));

        $this->db->insert('menu',array(
            'title'     => 'Categories',
            'link'      => 'admin/categories',
            'type'      => MENU_TYPE('Admin'),
            'parent'    => $parent_id,
            'module_permission_id' => $category_permission_id
        ));
    }

    public function up(){
        $this->db->trans_start();

        $this->item_module();
        $this->category_module();

        $this->db->trans_complete();
    }

    public function down(){

    }
}