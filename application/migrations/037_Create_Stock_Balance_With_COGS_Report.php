<?php
class Migration_Create_Stock_Balance_With_COGS_Report extends CI_Migration
{
    public function up(){
        $this->db->trans_start();

        $this->db->insert('report',array(
            'parent'            => 23,
            'condition_form'    => 1,
            'code'              => 'stock_balance_with_cogs_report',
            'name'              => 'Stock Balance With COGS',
            'system'            => 3,
            'status'            => 1));
        $report_id = $this->db->select('MAX(id) as id')->get('report')->result();

        $this->db->insert('report_group',array('group_id' => 1, 'report_id' => $report_id[0]->id));
        $this->db->insert('report_group',array('group_id' => 2, 'report_id' => $report_id[0]->id));

        $this->db->trans_complete();

    }
}