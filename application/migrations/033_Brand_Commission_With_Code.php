<?php
/**
 * Created by PhpStorm.
 * User: gento
 * Date: 16/6/2015
 * Time: 1:02 PM
 */

class Migration_Brand_Commission_With_Code extends CI_Migration {

    public function up(){
       $fields = array(
            'unit_cost' => array(
                'type' => 'float',
                'constraint' => 11,
                'null' => true
            )
        );

        $this->dbforge->add_column('item_product',$fields, 'master_quantity');
        $this->dbforge->add_column('item_product_log',$fields,'log_user');
        $this->dbforge->add_column('bill_item', $fields, 'quantity');

        $this->db->trans_start();

        $this->db->insert('report',
            array('parent'          => 1,
                  'condition_form'  => 1,
                  'code'            => 'branch_commission_with_code',
                  'name'            => 'Branch Commission With Code',
                  'system'          => 3,
                  'status'          => 1));

        $report_id = $this->db->get_where('report',array('code' => 'branch_commission_with_code'))->result()[0]->id;

        $groups = $this->db->select('id')->get('group')->result();
        foreach($groups as $group) {
            $this->db->insert('report_group', array('group_id' => $group->id, 'report_id' => $report_id));
        }

        $this->db->trans_complete();
    }

}