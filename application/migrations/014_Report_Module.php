<?php
class Migration_Report_Module extends CI_Migration{
    public function up(){
        $report_module_id = $this->db->get_where('module',array('code' => 'reports'))->result()[0]->id;

        $report_module_permission_id = $this->db->get_where('module_permission',array('module_id' => $report_module_id, 'permission_code' => 'v'))->result()[0]->id;

        $this->db->update('module',array('type' => MENU_TYPE('All')),array('code' => 'reports'));

        $this->db->insert('menu',array(
            'title'     => 'Reports',
            'link'      => 'admin/reports',
            'type'      => MENU_TYPE('Admin'),
            'module_permission_id' => $report_module_permission_id
        ));
    }

    public function down(){

    }
}