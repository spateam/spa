<?php
class Migration_Bill_Log_Table extends CI_Migration{
    public function up(){

        $this->db->query("DROP TABLE IF EXISTS `bill_log`;");
        $this->db->query("CREATE TABLE `bill_log` (
  `id` int(11) unsigned NOT NULL,
  `code` varchar(20) NOT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `discount` float(11,2) unsigned NOT NULL DEFAULT '0.00',
  `gst` float(11,2) unsigned NOT NULL DEFAULT '0.00',
  `amount_due` float(11,2) unsigned NOT NULL DEFAULT '0.00',
  `replace_bill_id` int(10) unsigned DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `void_date` datetime DEFAULT NULL,
  `raw_created_date` datetime DEFAULT NULL,
  `replace_bill_date` datetime DEFAULT NULL,
  `global` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `log_type` varchar(10) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `log_user` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
    }

    public function down(){

    }
}