CREATE TABLE `spa_report`.`deposit_payment_item_list` (
`id` INT NOT NULL AUTO_INCREMENT,
`bill_id` INT(25) NOT NULL,
`customer_id` INT(25) NOT NULL,
`deposit_payment_item_id` INT(25) NOT NULL,
`remain_payment_times` INT(25) NOT NULL,
`total_remain_amount` FLOAT(11,2) NOT NULL,
`status` INT(1) NOT NULL,
PRIMARY KEY (`id`));


CREATE TABLE `spa_report`.`deposit_paypal_return` (
`id` INT NOT NULL AUTO_INCREMENT,
`deposit_payment_item_id` INT(25) NOT NULL,
`paypal_payment_id` VARCHAR(45) NOT NULL,
`paypal_payment_token` VARCHAR(45) NOT NULL,
`paypal_payment_payer_id` VARCHAR(45) NOT NULL,
`time_no` INT(11) NOT NULLL
`amount_payment` FLOAT(11,2) NOT NULLL
PRIMARY KEY (`id`));
