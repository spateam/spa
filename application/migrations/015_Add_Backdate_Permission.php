<?php
class Migration_Add_backdate_Permission extends CI_Migration{
    public function up(){
        $this->db->trans_start();
        $permission_id = $this->db->get_where('permission',array('code' => 'backdate'))->result();
        if(!count($permission_id)){
            $this->db->insert('permission',array('code' => 'backdate','name' => 'Backdate'));
        }
        $permission_code = 'backdate';
        $bill_module_id = $this->db->get_where('module',array('code' => 'bills'))->result();
        if(count($bill_module_id)){
            $bill_module_id = $bill_module_id[0]->id;
        }else{
            throw new Exception('where the heck is your bill module');
        }

        $this->load->helper('migration');

        add_permission_to_all_places($bill_module_id,array($permission_code));

        $this->db->trans_complete();
    }

    public function down(){

    }
}